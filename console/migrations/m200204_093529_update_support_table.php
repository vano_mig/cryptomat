<?php

use yii\db\Migration;

/**
 * Class m200204_093529_update_support_table
 */
class m200204_093529_update_support_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%support}}', 'client_name');
        $this->dropColumn('{{%support}}', 'email');
        $this->dropColumn('{{%support}}', 'support_id');
        $this->addColumn('{{%support}}', 'answerer_id', $this->string()->null()->defaultValue(null));
        $this->alterColumn('{{%support}}', 'answerer_id', $this->string()->null()->defaultValue(null)->after('client_id'));
        $this->alterColumn('{{%support}}', 'status', $this->string()->null()->defaultValue(null)->after('ticket_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%support}}', 'client_name', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%support}}', 'email', $this->string()->null()->defaultValue(null));
        $this->dropColumn('{{%support}}', 'answerer_id');
        $this->addColumn('{{%support}}', 'support_id', $this->string()->null()->defaultValue(null));
    }

}
