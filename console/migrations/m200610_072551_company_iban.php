<?php

use yii\db\Migration;

/**
 * Class m200610_072551_company_iban
 */
class m200610_072551_company_iban extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #$this->addColumn('company', 'iban', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200610_072551_company_iban cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_072551_company_iban cannot be reverted.\n";

        return false;
    }
    */
}
