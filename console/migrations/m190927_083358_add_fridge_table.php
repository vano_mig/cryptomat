<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m190927_083358_add_fridge_table
 */
class m190927_083358_add_fridge_table extends Migration
{
    public $tableName = 'freezer';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'unique_id'=>$this->string()->null()->defaultValue(null),
            'country'=>$this->string()->null()->defaultValue(null),
            'city'=>$this->string()->null()->defaultValue(null),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        //доступ к пользователям
        $userCreate = $auth->createPermission('freezer.create');
        $userCreate->description = 'Создание холодильника';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('freezer.update');
        $userUpdate->description = 'Редактирование холодильника';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('freezer.delete');
        $userDelete->description = 'Удаление холодильника';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('freezer.view');
        $userView->description = 'Просмотр холодильника';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('freezer.listview');
        $userListview->description = 'Просмотр списка холодильников';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable($this->tableName);

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('freezer.listview');
        $view = $auth->getPermission('freezer.view');
        $create = $auth->getPermission('freezer.create');
        $update = $auth->getPermission('freezer.update');
        $delete = $auth->getPermission('freezer.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
