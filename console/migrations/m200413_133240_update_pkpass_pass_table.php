<?php

use yii\db\Migration;

/**
 * Class m200413_133240_update_pkpass_pass_table
 */
class m200413_133240_update_pkpass_pass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%pkpass_pass}}', 'user_id', $this->string()->null()->defaultValue(null));
        $this->alterColumn('{{%pkpass_pass}}', 'authentication_token', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%pkpass_pass}}', 'user_id', $this->float()->null()->defaultValue(null));
        $this->alterColumn('{{%pkpass_pass}}', 'authentication_token', $this->float()->null()->defaultValue(null));

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200413_133240_update_pkpass_pass_table cannot be reverted.\n";

        return false;
    }
    */
}
