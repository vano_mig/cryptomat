<?php

use yii\db\Migration;

/**
 * Class m191021_135440_support_table_update
 */
class m191021_135440_support_table_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('support', 'ticket_id', $this->integer()->null()->defaultValue(null)->after('id'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('support', 'ticket_id');
    }
}
