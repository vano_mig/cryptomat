<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colum_in_passcreator_push}}`.
 */
class m191206_102934_create_colum_in_passcreator_push_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%passcreator_push}}', 'date_sending', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%passcreator_push}}', 'push_id', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%passcreator_push}}', 'date_sending', $this->date()->null()->defaultValue(null));
        $this->dropColumn('{{%passcreator_push}}', 'push_id');
    }
}
