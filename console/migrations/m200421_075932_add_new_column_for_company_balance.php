<?php

use yii\db\Migration;

/**
 * Class m200421_075932_add_new_column_for_company_balance
 */
class m200421_075932_add_new_column_for_company_balance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company_balance}}', 'transaction', $this->string(50)->null()->defaultValue(null)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%company_balance}}', 'transaction');
    }
}
