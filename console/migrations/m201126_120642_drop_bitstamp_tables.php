<?php

use yii\db\Migration;

/**
 * Class m201126_120642_drop_bitstamp_tables
 */
class m201126_120642_drop_bitstamp_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%bitstamp_api}}');
        $this->dropTable('{{%bitstamp_crypto_withdrawal}}');
        $this->dropTable('{{%bitstamp_transactions}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201126_120642_drop_bitstamp_tables cannot be reverted.\n";

        return false;
    }
}
