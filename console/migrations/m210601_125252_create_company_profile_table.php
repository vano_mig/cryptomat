<?php

use common\models\Company;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_profile}}`.
 */
class m210601_125252_create_company_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company_profile}}', 'kys_url', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company_profile}}', 'kys_client_id', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company_profile}}', 'kys_secret_key', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%company_profile}}', 'kys_url');
        $this->dropColumn('{{%company_profile}}', 'kys_client_id');
        $this->dropColumn('{{%company_profile}}', 'kys_secret_key');
    }
}
