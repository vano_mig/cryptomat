<?php

use yii\db\Migration;

/**
 * Class m191023_150950_create_frifge_monitiring
 */
class m191023_150950_create_frifge_monitiring extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fridge_monitoring}}', [
            'id' => $this->primaryKey(),
            'fridge_id'=>$this->integer()->null()->defaultValue(null),
            'content'=>$this->text()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%fridge_monitoring}}');
    }
}
