<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colum_in_passcreator_template}}`.
 */
class m191210_150339_create_colum_in_passcreator_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%passcreator_template}}', 'template_fields_links', $this->text()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%passcreator_template}}', 'template_fields_links');
    }
}
