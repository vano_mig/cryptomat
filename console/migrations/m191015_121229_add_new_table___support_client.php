<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m191015_121229_add_new_table___support_client
 */
class m191015_121229_add_new_table___support_client extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('support', [
            'id' => $this->primaryKey(),
            'client_id' => $this->string(32)->null()->defaultValue(null),
            'client_name' => $this->string(32)->null()->defaultValue(null),
            'email' => $this->string(32)->null()->defaultValue(null),
            'technical_mark' => $this->string(32)->defaultValue(null),
            'text' => $this->string(1000)->null()->defaultValue(null),
            'state' => $this->string(100)->null()->defaultValue('active'),
            'result' => $this->string(1000)->null()->defaultValue(null),
            'created_at' => $this->dateTime()->null(),
        ]);

        $created_at = date('Y-m-d H:i:s');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        //доступ к тикетам
        $supportCreate = $auth->createPermission('support.create');
        $supportCreate->description = 'Добавление тикета';
        $auth->add($supportCreate);
        $auth->addChild($admin, $supportCreate);

        $supportUpdate = $auth->createPermission('support.update');
        $supportUpdate->description = 'Редактирование тикета';
        $auth->add($supportUpdate);
        $auth->addChild($admin, $supportUpdate);

        $supportDelete = $auth->createPermission('support.delete');
        $supportDelete->description = 'Удаление тикета';
        $auth->add($supportDelete);
        $auth->addChild($admin, $supportDelete);

        $supportView = $auth->createPermission('support.view');
        $supportView->description = 'Посмотреть обращения';
        $auth->add($supportView);
        $auth->addChild($admin, $supportView);

        $supportListview = $auth->createPermission('support.listview');
        $supportListview->description = 'Список обращений';
        $auth->add($supportListview);
        $auth->addChild($admin, $supportListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('support');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('support.listview');
        $view = $auth->getPermission('support.view');
        $create = $auth->getPermission('support.create');
        $update = $auth->getPermission('support.update');
        $delete = $auth->getPermission('support.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
