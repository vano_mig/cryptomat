<?php

use yii\db\Migration;

/**
 * Class m210216_083023_create_table_market_transaction
 */
class m210216_083023_create_table_market_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{market_transaction}}', [
			'id' => $this->primaryKey(),
			'price' => $this->decimal(64, 8)->null()->defaultValue(null),
			'amount' => $this->decimal(64, 8)->null()->defaultValue(null),
			'transaction_id' => $this->string(),
			'type' => $this->integer(),
			'market_id' => $this->string()->null()->defaultValue(null),
			'datetime' => $this->dateTime(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{market_transaction}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210216_083023_create_table_market_transaction cannot be reverted.\n";

        return false;
    }
    */
}
