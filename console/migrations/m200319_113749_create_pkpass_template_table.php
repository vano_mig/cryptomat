<?php

use yii\db\Migration;
use common\models\User;
/**
 * Handles the creation of table `{{%pkpass_template}}`.
 */
class m200319_113749_create_pkpass_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pkpass_template}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->null()->defaultValue(null),
            'description' => $this->string()->null()->defaultValue(null),
            'organization_name' => $this->string()->null()->defaultValue(null),
            'pass_type_identifier' => $this->string()->null()->defaultValue(null),
            'team_identifier' => $this->string()->null()->defaultValue(null),
            'relevant_date' => $this->dateTime()->null()->defaultValue(null),
            'expiration_date' => $this->dateTime()->null()->defaultValue(null),
            'style_key' => $this->string()->null()->defaultValue(null),
            'voided' => $this->boolean()->null()->defaultValue(null),
            'beacons' => $this->text()->null()->defaultValue(null),
            'locations' => $this->text()->null()->defaultValue(null),
            'barcode' => $this->text()->null()->defaultValue(null),
            'backgroundColor' => $this->string()->null()->defaultValue(null),
            'foregroundColor' => $this->string()->null()->defaultValue(null),
            'labelColor' => $this->string()->null()->defaultValue(null),
            'logoText' => $this->string()->null()->defaultValue(null),
            'suppressStripShine' => $this->boolean()->null()->defaultValue(null),
            'nfc' => $this->text()->null()->defaultValue(null),
            'auxiliaryFields' => $this->text()->null()->defaultValue(null),
            'backFields' => $this->text()->null()->defaultValue(null),
            'headerFields' => $this->text()->null()->defaultValue(null),
            'primaryFields' => $this->text()->null()->defaultValue(null),
            'secondaryFields' => $this->text()->null()->defaultValue(null),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pkpass_template}}');

    }
}
