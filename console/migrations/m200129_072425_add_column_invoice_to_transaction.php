<?php

use yii\db\Migration;

/**
 * Class m200129_072425_add_column_invoice_to_transaction
 */
class m200129_072425_add_column_invoice_to_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction}}', 'invoice', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%transaction}}', 'invoice');
    }
}
