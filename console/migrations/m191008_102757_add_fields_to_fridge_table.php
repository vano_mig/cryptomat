<?php

use yii\db\Migration;

/**
 * Class m191008_102757_add_fields_to_fridge_table
 */
class m191008_102757_add_fields_to_fridge_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%freezer}}', 'ip_address', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%freezer}}', 'port', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%freezer}}', 'ip_address');
       $this->dropColumn('{{%freezer}}', 'port');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_102757_add_fields_to_fridge_table cannot be reverted.\n";

        return false;
    }
    */
}
