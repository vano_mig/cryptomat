<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_balance}}`.
 */
class m200330_101729_create_company_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_balance}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'service'=>$this->string(50)->null()->defaultValue(null),
            'cost'=>$this->decimal(10,2)->null()->defaultValue(null),
            'quantity'=>$this->integer()->null()->defaultValue(null),
            'vat'=>$this->decimal()->null()->defaultValue(null),
            'message'=>$this->text()->null()->defaultValue(null),
            'date'=>$this->date()->null()->defaultValue(null),
            'status'=>$this->string(50)->null()->defaultValue(null),
            'type'=>$this->string(50)->null()->defaultValue(null),
            'invoice'=>$this->string()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_balance}}');
    }
}
