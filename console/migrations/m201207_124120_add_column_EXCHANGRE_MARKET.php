<?php

use yii\db\Migration;

/**
 * Class m201207_124120_add_column_EXCHANGRE_MARKET
 */
class m201207_124120_add_column_EXCHANGRE_MARKET extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%replenish_wallet}}', 'exchange_wallet', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%replenish_wallet}}', 'exchange_wallet');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201207_124120_add_column_EXCHANGRE_MARKET cannot be reverted.\n";

        return false;
    }
    */
}
