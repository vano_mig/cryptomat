<?php

use yii\db\Migration;

/**
 * Class m200615_115430_create_atm_group_and_atm_profile_added
 */
class m200615_115430_create_atm_group_and_atm_profile_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%atm_groups}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
        ]);
        // take average fee blockchain customer
//        $this->addColumn('atm_profile', 'take_average_fee_blockchain_customer', $this->integer()->null()->defaultValue(null));
//        $this->addColumn('atm_profile', 'paper_waller', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%atm_groups}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200615_115430_create_atm_group_and_atm_profile_added cannot be reverted.\n";

        return false;
    }
    */
}
