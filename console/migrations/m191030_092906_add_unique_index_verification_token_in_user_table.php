<?php

use yii\db\Migration;

/**
 * Class m191030_092906_add_unique_index_verification_token_in_user_table
 */
class m191030_092906_add_unique_index_verification_token_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('verification_token','{{%user}}', 'verification_token', $unique = true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('verification_token','{{%user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191030_092906_add_unique_index_verification_token_in_user_table cannot be reverted.\n";

        return false;
    }
    */
}
