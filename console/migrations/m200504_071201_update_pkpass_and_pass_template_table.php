<?php

use yii\db\Migration;

/**
 * Class m200504_071201_update_pkpass_and_pass_template_table
 */
class m200504_071201_update_pkpass_and_pass_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%pkpass_pass}}', 'operating_system');
        $this->dropColumn('{{%pkpass_template}}', 'voided');
        $this->addColumn('{{%pkpass_template}}', 'update', $this->boolean()->defaultValue(null)->after('status'));
        $this->renameColumn('{{%pkpass_pass}}', 'voided', 'status');
        $this->createTable('{{%pkpass_device}}', [
            'id' => $this->primaryKey(),
            'pass_id' => $this->integer()->defaultValue(null),
            'application' => $this->string()->null()->defaultValue(null),
            'device_token' => $this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%pkpass_template}}', 'voided', $this->boolean()->null()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'operating_system', $this->text()->defaultValue(null));
        $this->dropColumn('{{%pkpass_template}}', 'update');
        $this->renameColumn('{{%pkpass_pass}}', 'status', 'voided');
        $this->dropTable('{{%pkpass_device}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200504_071201_update_pkpass_and_pass_template_table cannot be reverted.\n";

        return false;
    }
    */
}
