<?php

use yii\db\Migration;

/**
 * Class m201124_113718_alter_atm_table
 */
class m201124_113718_alter_atm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%atm}}', 'group_id', $this->integer()->null()->defaultValue(null));
		$this->addColumn('{{%atm}}', 'profile_id', $this->integer()->null()->defaultValue(null));
		$this->renameColumn('{{%identification_profile}}', 'user_id', 'company_id');
		$this->createTable('{{%identification_wallets}}', [
			'id' => $this->primaryKey(),
			'profile_id' => $this->integer()->null()->defaultValue(null),
			'replenish_id' => $this->integer()->null()->defaultValue(null),
			'exchange_id' => $this->integer()->null()->defaultValue(null),
			'type' => $this->integer()->null()->defaultValue(null),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%atm}}', 'group_id');
		$this->dropColumn('{{%atm}}', 'profile_id');
		$this->renameColumn('{{%identification_profile}}', 'company_id', 'user_id');
		$this->dropTable('{{%atm_wallets}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_113718_alter_atm_table cannot be reverted.\n";

        return false;
    }
    */
}
