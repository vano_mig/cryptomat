<?php

use yii\db\Migration;

/**
 * Class m191104_135616_create_user_profile_column
 */
class m191104_135616_create_user_profile_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'privacy_policy', $this->boolean()->null()->defaultValue(false));
        $this->addColumn('{{%user_profile}}', 'terms_of_service', $this->boolean()->null()->defaultValue(false));
        $this->alterColumn('{{%user_profile}}', 'mailing', $this->boolean()->null()->defaultValue(false));
        $this->addColumn('{{%user_profile}}', 'pkpass_card_url', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'privacy_policy');
        $this->dropColumn('{{%user_profile}}', 'terms_of_service');
        $this->alterColumn('{{%user_profile}}', 'mailing', $this->string()->null()->defaultValue(null));
        $this->dropColumn('{{%user_profile}}', 'pkpass_card_url');
    }

}
