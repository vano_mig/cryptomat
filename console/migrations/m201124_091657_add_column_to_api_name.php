<?php

use backend\modules\admin\models\ApiName;
use yii\db\Migration;

/**
 * Class m201124_091657_add_column_to_api_name
 */
class m201124_091657_add_column_to_api_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%api_name}}', 'type', $this->tinyInteger()->null()->defaultValue(null));
        $this->addColumn('{{%api_name}}', 'form', $this->string()->null()->defaultValue(null));
        ApiName::updateAll(['type'=>ApiName::TYPE_EXCHANGE]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%api_name}}', 'type');
    }
}
