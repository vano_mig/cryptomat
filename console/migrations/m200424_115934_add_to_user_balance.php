<?php

use backend\modules\user\models\User;
use yii\db\Migration;

/**
 * Class m200424_115934_add_to_user_balance
 */
class m200424_115934_add_to_user_balance extends Migration
{
    public $tableName ='{{%user_balance}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $operator = $auth->getRole(User::ROLE_USER);



        $view = $auth->createPermission('user_balance.view');
        $view->description = 'View advert data';
        $auth->add($view);
        $auth->addChild($user, $view);
        $auth->addChild($operator, $view);

        $listview = $auth->createPermission('user_balance.listview');
        $listview->description = 'View user_balance list';
        $auth->add($listview);
        $auth->addChild($user, $listview);
        $auth->addChild($operator, $listview);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $operator = $auth->getRole(User::ROLE_USER);
        $view = $auth->getPermission('user_balance.view');
        $listview = $auth->getPermission('user_balance.listview');
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->removeChild($operator, $view);
        $auth->removeChild($operator, $listview);
        $auth->remove($view);
        $auth->remove($listview);
    }

}
