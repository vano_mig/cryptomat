<?php

use yii\db\Migration;

/**
 * Class m201127_091838_alter_currency_cash_id_field_in_coinmarketcap_table
 */
class m201127_091838_alter_currency_cash_id_field_in_coinmarketcap_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%coinmarketcap}}', 'currency_cash_id', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('{{%transaction}}', 'api_id', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201127_091838_alter_currency_cash_id_field_in_coinmarketcap_table cannot be reverted.\n";

        return false;
    }
}
