<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_passport}}`.
 */
class m200514_053847_create_user_passport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_passport}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'file'=>$this->text()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_passport}}');
    }
}
