<?php

use yii\db\Migration;
use backend\modules\user\models\User;

/**
 * Handles the creation of table `{{%atm_profile}}`.
 */
class m200612_081455_create_atm_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%atm_profile}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'use_sms'=>$this->integer()->null()->defaultValue(null),
            'use_phone'=>$this->integer()->null()->defaultValue(null),
            'use_pin'=>$this->integer()->null()->defaultValue(null),
            'currency'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('atm.create');
        $create->description = 'Add virtual atm device';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('atm.update');
        $update->description = 'Update virtual atm device';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('atm.delete');
        $delete->description = 'Delete virtual atm device';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('atm.view');
        $view->description = 'View virtual atm device';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('atm.list');
        $listview->description = 'View virtual atm device list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%atm_profile}}');

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('atm.create');
        $update = $auth->getPermission('atm.update');
        $delete = $auth->getPermission('atm.delete');
        $view = $auth->getPermission('atm.view');
        $list = $auth->getPermission('atm.list');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $list);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($list);
    }
}
