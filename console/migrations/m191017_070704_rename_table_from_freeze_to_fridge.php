<?php

use yii\db\Migration;

/**
 * Class m191017_070704_rename_table_from_freeze_to_fridge
 */
class m191017_070704_rename_table_from_freeze_to_fridge extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('freezer', 'fridge');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameTable('fridge', 'freezer');
    }
}
