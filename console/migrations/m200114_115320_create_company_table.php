<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company}}`.
 */
class m200114_115320_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'vat_number'=>$this->integer()->null()->defaultValue(null),
            'street'=>$this->string()->null()->defaultValue(null),
            'city'=>$this->string()->null()->defaultValue(null),
            'country_id'=>$this->integer()->null()->defaultValue(null),
            'zip_code'=>$this->integer()->null()->defaultValue(null),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'image'=>$this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company}}');
    }
}
