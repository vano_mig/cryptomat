<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitstamp}}`.
 */
class m200625_113241_create_bitstamp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitstamp_api}}', [
            'id' => $this->primaryKey(),
            'client_id'=>$this->string()->null()->defaultValue(null),
            'api_key'=>$this->string()->null()->defaultValue(null),
            'api_secret'=>$this->string()->null()->defaultValue(null),
            'user_id'=>$this->integer()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitstamp_api}}');
    }
}
