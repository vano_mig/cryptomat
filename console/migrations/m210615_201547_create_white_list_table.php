<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%white_list}}`.
 */
class m210615_201547_create_white_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%white_list}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'ip_address' => $this->text()->null()->defaultValue(null),
            'atm_id' => $this->integer()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%white_list}}');
    }
}
