<?php

use backend\modules\user\models\User;
use yii\db\Migration;

/**
 * Class m200611_070044_permission_virtual_terminal
 */
class m200611_070044_permission_virtual_terminal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('virtual_terminal.create');
        $create->description = 'Add virtual virtual_terminal device';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('virtual_terminal.update');
        $update->description = 'Update virtual virtual_terminal device';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('virtual_terminal.delete');
        $delete->description = 'Delete virtual virtual_terminal device';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('virtual_terminal.view');
        $view->description = 'View virtual virtual_terminal device';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('virtual_terminal.list');
        $listview->description = 'View virtual virtual_terminal device list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        $createCard = $auth->getPermission('card.create');
        $deleteCard = $auth->getPermission('card.delete');
        $viewCard = $auth->getPermission('card.view');
        $auth->removeChild($user, $createCard);
        $auth->removeChild($user, $deleteCard);
        $auth->removeChild($user, $viewCard);
        $auth->remove($createCard);
        $auth->remove($deleteCard);
        $auth->remove($viewCard);

        $createCategory = $auth->getPermission('category.create');
        $deleteCategory = $auth->getPermission('category.delete');
        $viewCategory = $auth->getPermission('category.view');
        $updateCategory = $auth->getPermission('category.update');
        $listCategory = $auth->getPermission('category.listview');
        $auth->removeChild($user, $createCategory);
        $auth->removeChild($user, $deleteCategory);
        $auth->removeChild($user, $viewCategory);
        $auth->removeChild($user, $updateCategory);
        $auth->removeChild($user, $listCategory);
        $auth->remove($createCategory);
        $auth->remove($deleteCategory);
        $auth->remove($viewCategory);
        $auth->remove($updateCategory);
        $auth->remove($listCategory);

        $createFridge = $auth->getPermission('fridge.create');
        $deleteFridge = $auth->getPermission('fridge.delete');
        $viewFridge = $auth->getPermission('fridge.view');
        $updateFridge = $auth->getPermission('fridge.update');
        $listFridge = $auth->getPermission('fridge.listview');
        $auth->removeChild($user, $createFridge);
        $auth->removeChild($user, $deleteFridge);
        $auth->removeChild($user, $viewFridge);
        $auth->removeChild($user, $updateFridge);
        $auth->removeChild($user, $listFridge);
        $auth->remove($createFridge);
        $auth->remove($deleteFridge);
        $auth->remove($viewFridge);
        $auth->remove($updateFridge);
        $auth->remove($listFridge);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('virtual_terminal.create');
        $update = $auth->getPermission('virtual_terminal.update');
        $delete = $auth->getPermission('virtual_terminal.delete');
        $view = $auth->getPermission('virtual_terminal.view');
        $list = $auth->getPermission('virtual_terminal.list');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $list);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($list);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200611_070044_permission_virtual_terminal cannot be reverted.\n";

        return false;
    }
    */
}
