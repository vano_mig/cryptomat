<?php

use yii\db\Migration;

/**
 * Class m200325_123233_add_model_id_to_fridge
 */
class m200325_123233_add_model_id_to_fridge extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%fridge}}', 'model_id', $this->integer()->null()->defaultValue(null)->after('unique_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%fridge}}', 'model_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200325_123233_add_model_id_to_fridge cannot be reverted.\n";

        return false;
    }
    */
}
