<?php

use yii\db\Migration;

/**
 * Class m201123_085331_add_atm_haccp_table
 */
class m201123_085331_add_atm_haccp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%atm_haccp}}', [
            'id' => $this->primaryKey(),
            'terminal_id'=>$this->integer()->null()->defaultValue(null),
            'content'=>$this->text()->null()->defaultValue(null),
            'status'=>$this->integer()->null()->defaultValue(null),
            'code'=>$this->tinyInteger()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('{{%atm_haccp}}');
    }
}
