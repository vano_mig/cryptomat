<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m191211_090159_delete_passcreator_service_permissions
 */
class m191211_090159_delete_passcreator_service_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $view = $auth->getPermission('passcreator_service.view');
        $auth->removeChild($user, $view);
        $auth->remove($view);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $view = $auth->createPermission('passcreator_service.view');
        $view->description = 'Доступ к Passcreator Service';
        $auth->add($view);
        $auth->addChild($user, $view);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191211_090159_delete_passcreator_service_permissions cannot be reverted.\n";

        return false;
    }
    */
}
