<?php

use yii\db\Migration;

/**
 * Class m191026_084147_remove_unique_index_username_in_user_table
 */
class m191026_084147_remove_unique_index_username_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('username','{{%user}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('username','{{%user}}', 'username', $unique = true);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191026_084147_remove_unique_index_username_in_user_table cannot be reverted.\n";

        return false;
    }
    */
}
