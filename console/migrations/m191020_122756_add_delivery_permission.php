<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m191020_122756_add_delivery_permission
 */
class m191020_122756_add_delivery_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        //доступ к просмотру поставщика
        $translationCreate = $auth->createPermission('delivery.view');
        $translationCreate->description = 'Просмотр поставщика';
        $auth->add($translationCreate);
        $auth->addChild($admin, $translationCreate);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('delivery.view');
        $auth->removeChild($admin, $list);
        $auth->remove($list);
    }

}
