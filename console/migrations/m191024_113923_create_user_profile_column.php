<?php

use yii\db\Migration;

/**
 * Class m191024_113923_create_user_profile_column
 */
class m191024_113923_create_user_profile_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'user_id', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'first_name', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'last_name', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'phone_number', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'country', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'city', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'street_house', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'post_code', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'internal_marker', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'ref_card_no', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'ccno', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'card_type', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'expiry_date', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'client_from', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'mailing', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'user_id');
        $this->dropColumn('{{%user_profile}}', 'first_name');
        $this->dropColumn('{{%user_profile}}', 'last_name');
        $this->dropColumn('{{%user_profile}}', 'street_house');
        $this->dropColumn('{{%user_profile}}', 'post_code');
        $this->dropColumn('{{%user_profile}}', 'city');
        $this->dropColumn('{{%user_profile}}', 'country');
        $this->dropColumn('{{%user_profile}}', 'internal_marker');
        $this->dropColumn('{{%user_profile}}', 'phone_number');
        $this->dropColumn('{{%user_profile}}', 'ref_card_no');
        $this->dropColumn('{{%user_profile}}', 'ccno');
        $this->dropColumn('{{%user_profile}}', 'card_type');
        $this->dropColumn('{{%user_profile}}', 'expiry_date');
        $this->dropColumn('{{%user_profile}}', 'client_from');
        $this->dropColumn('{{%user_profile}}', 'mailing');
    }
}
