<?php

use yii\db\Migration;

/**
 * Class m200916_081843_add_3rd_party_api_table
 */
class m200916_081843_add_3rd_party_api_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%3rd_party_api}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->null()->defaultValue(null),
			'user_id' => $this->integer()->null()->defaultValue(null),
			'provider_id' => $this->integer()->null()->defaultValue(null),
			'account_sid' => $this->string()->null()->defaultValue(null),
			'auth_token' => $this->string()->null()->defaultValue(null),
			'messaging_service_sid' => $this->string()->null()->defaultValue(null),
			'from_number' => $this->string()->null()->defaultValue(null),
			'cost' => $this->float()->null()->defaultValue(null),
			'currency_id' => $this->integer()->null()->defaultValue(null),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%3rd_party_api}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200916_081843_add_3rd_party_api_table cannot be reverted.\n";

        return false;
    }
    */
}
