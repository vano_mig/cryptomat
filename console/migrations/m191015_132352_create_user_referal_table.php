<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_referal}}`.
 */
class m191015_132352_create_user_referal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_referal}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'user_referal_id'=>$this->integer()->null()->defaultValue(null),
            'level'=>$this->integer()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_referal}}');
    }
}
