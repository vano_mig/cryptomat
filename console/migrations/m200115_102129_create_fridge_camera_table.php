<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fridge_camera}}`.
 */
class m200115_102129_create_fridge_camera_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fridge_camera}}', [
            'id' => $this->primaryKey(),
            'fridge_id'=>$this->integer()->null()->defaultValue(null),
            'camera_1'=>$this->string()->null()->defaultValue(null),
            'camera_2'=>$this->string()->null()->defaultValue(null),
            'camera_3'=>$this->string()->null()->defaultValue(null),
            'camera_4'=>$this->string()->null()->defaultValue(null),
            'camera_5'=>$this->string()->null()->defaultValue(null),
            'camera_6'=>$this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%fridge_camera}}');
    }
}
