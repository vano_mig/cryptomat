<?php

use yii\db\Migration;

/**
 * Class m201124_132953_add_service_email_to_company_table
 */
class m201124_132953_add_service_email_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'service_email', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('{{%company}}', 'service_email');
    }
}
