<?php

use yii\db\Migration;

/**
 * Class m200410_091250_update_pkpass_pass_table
 */
class m200410_091250_update_pkpass_pass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%pkpass_pass}}', 'beacons');
        $this->dropColumn('{{%pkpass_pass}}', 'locations');
        $this->dropColumn('{{%pkpass_pass}}', 'webServiceURL');
        $this->dropColumn('{{%pkpass_pass}}', 'auxiliaryFields');
        $this->dropColumn('{{%pkpass_pass}}', 'backFields');
        $this->dropColumn('{{%pkpass_pass}}', 'headerFields');
        $this->dropColumn('{{%pkpass_pass}}', 'primaryFields');
        $this->dropColumn('{{%pkpass_pass}}', 'secondaryFields');
        $this->dropColumn('{{%pkpass_pass}}', 'authenticationToken');
        $this->addColumn('{{%pkpass_pass}}', 'user_id', $this->float()->defaultValue(null)->after('id'));
        $this->addColumn('{{%pkpass_pass}}', 'authentication_token', $this->float()->defaultValue(null)->after('voided'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_pass}}', 'authentication_token');
        $this->dropColumn('{{%pkpass_pass}}', 'user_id');
        $this->addColumn('{{%pkpass_pass}}', 'authenticationToken', $this->float()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'beacons', $this->float()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'locations', $this->float()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'webServiceURL', $this->dateTime()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'auxiliaryFields', $this->float()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'backFields', $this->float()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'headerFields', $this->dateTime()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'primaryFields', $this->dateTime()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'secondaryFields', $this->dateTime()->defaultValue(null));
    }

}
