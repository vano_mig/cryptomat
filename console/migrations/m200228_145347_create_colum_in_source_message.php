<?php

use yii\db\Migration;

/**
 * Class m200228_145347_create_colum_in_source_message
 */
class m200228_145347_create_colum_in_source_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%source_message}}', 'comment', $this->text()->null()->defaultValue(null));
        $this->addColumn('{{%source_message}}', 'status', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%source_message}}', 'comment');
        $this->dropColumn('{{%source_message}}', 'status');
    }
}
