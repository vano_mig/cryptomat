<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m191017_120833_add_permissions_to_translation
 */
class m191017_120833_add_permissions_to_translation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        //доступ к тикетам
        $translationCreate = $auth->createPermission('translation.create');
        $translationCreate->description = 'Добавление перевода';
        $auth->add($translationCreate);
        $auth->addChild($admin, $translationCreate);

        $translationUpdate = $auth->createPermission('translation.update');
        $translationUpdate->description = 'Редактирование перевода';
        $auth->add($translationUpdate);
        $auth->addChild($admin, $translationUpdate);

        $translationDelete = $auth->createPermission('translation.delete');
        $translationDelete->description = 'Удаление перевода';
        $auth->add($translationDelete);
        $auth->addChild($admin, $translationDelete);

        $translationView = $auth->createPermission('translation.view');
        $translationView->description = 'Посмотреть перевод';
        $auth->add($translationView);
        $auth->addChild($admin, $translationView);

        $translationListview = $auth->createPermission('translation.listview');
        $translationListview->description = 'Список переводов';
        $auth->add($translationListview);
        $auth->addChild($admin, $translationListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('translation.listview');
        $view = $auth->getPermission('translation.view');
        $create = $auth->getPermission('translation.create');
        $update = $auth->getPermission('translation.update');
        $delete = $auth->getPermission('translation.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
