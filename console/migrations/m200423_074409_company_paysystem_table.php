<?php

use common\models\Company;
use common\models\Paysystem;
use yii\db\Migration;

/**
 * Class m200423_074409_company_paysystem_table
 */
class m200423_074409_company_paysystem_table extends Migration
{
    public $tablename = '{{%company_paysystem}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tablename, [
           'id'=>$this->primaryKey(),
           'company_id'=>$this->integer()->null()->defaultValue(null),
           'paysystem_id'=>$this->integer()->null()->defaultValue(null),
           'token'=>$this->string()->null()->defaultValue(null),
           'key'=>$this->string()->null()->defaultValue(null),
        ]);

        $list = Company::find()->all();
        if(!empty($list)) {
            $paysystam = Paysystem::find()->one();
            foreach ($list as $item) {
                $this->insert($this->tablename,[
                    'company_id'=>$item->id,
                    'paysystem_id'=>$paysystam->id,
                    'token'=>$paysystam->identificator,
                ]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropTable($this->tablename);
    }
}
