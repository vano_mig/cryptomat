<?php

use yii\db\Migration;

/**
 * Class m201123_080715_edit_transaction_table
 */
class m201123_080715_edit_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%transaction}}');
        $this->createTable('{{%transaction}}', [
            'id' => $this->bigPrimaryKey(),
            'terminal_id'=>$this->integer()->null()->defaultValue(null),
            'widget_id'=> $this->integer()->null()->defaultValue(null),
            'type'=>$this->integer()->null()->defaultValue(null),
            'amount_receive'=>$this->decimal(64,8)->null()->defaultValue(null),
            'currency_receive'=> $this->string(10)->null()->defaultValue(null),
            'amount_sent'=>$this->decimal(64,8)->null()->defaultValue(null),
            'currency_sent'=> $this->string(10)->null()->defaultValue(null),
            'transaction_id'=>$this->string()->null()->defaultValue(null),
            'status'=>$this->integer()->null()->defaultValue(null),
            'address'=> $this->string()->null()->defaultValue(null),
            'address_coin'=> $this->string()->null()->defaultValue(null),
            'phone_number'=> $this->string()->null()->defaultValue(null),
            'return_payment'=> $this->integer()->null()->defaultValue(null),
            'coinmarketcap_id'=> $this->integer()->null()->defaultValue(null),
            'coinmarketcap_id_2'=> $this->integer()->null()->defaultValue(null),
            'transaction_begin'=> $this->integer()->null()->defaultValue(null),
            'endtime'=>$this->integer()->null()->defaultValue(null),
            'confirm_needed'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "not down";
    }
}
