<?php

use yii\db\Migration;

/**
 * Class m201126_093253_delete_currency_id_from_atm
 */
class m201126_093253_delete_currency_id_from_atm extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%atm}}', 'currency_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%atm}}', 'currency_id', $this->tinyInteger()->null()->defaultValue(null));
    }
}
