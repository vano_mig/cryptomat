<?php

use yii\db\Migration;

class m191224_094710_create_colum_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'last_name', $this->string()->null()->defaultValue(null)->after('username'));
        $this->alterColumn('{{%user}}', 'verification_token', $this->string()->null()->defaultValue(null)->after('password_reset_token'));
        $this->alterColumn('{{%user}}', 'image', $this->text()->null()->defaultValue(null)->after('verification_token'));
        $this->alterColumn('{{%user}}', 'email', $this->string()->null()->defaultValue(null)->after('id'));
        $this->alterColumn('{{%user}}', 'role', $this->string()->null()->defaultValue(null)->after('last_name'));
        $this->alterColumn('{{%user}}', 'status', $this->string()->null()->defaultValue(null)->after('role'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%user}}', 'image', $this->string()->null()->defaultValue(null));
    }
}
