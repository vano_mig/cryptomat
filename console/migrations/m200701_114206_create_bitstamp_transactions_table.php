<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitstamp_transactions}}`.
 */
class m200701_114206_create_bitstamp_transactions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->createTable('{{%bitstamp_withdrawal}}', [
//            'id' => $this->primaryKey(),
//            'datetime' => $this->dateTime()->null()->defaultValue(null),
//            'type' => $this->integer()->null()->defaultValue(null),
//            'transaction_id' => $this->string()->null()->defaultValue(null),
//            'address' => $this->string()->null()->defaultValue(null),
//            'amount' => $this->float()->null()->defaultValue(null),
//            'status' => $this->integer()->null()->defaultValue(null),
//            'currency' => $this->string()->null()->defaultValue(null),
//            'user_id' => $this->string()->null()->defaultValue(null)
//        ]);

        $this->createTable('{{%bitstamp_transactions}}', [
            'id' => $this->primaryKey(),
            'limit_order' => $this->integer()->null()->defaultValue(null),
            'market_order' => $this->integer()->null()->defaultValue(null),
            'instant_order' => $this->integer()->null()->defaultValue(null),
            'type' => $this->integer()->null()->defaultValue(null),
            'price' => $this->integer()->null()->defaultValue(null),
            'amount' => $this->float()->null()->defaultValue(null),
            'datetime' => $this->dateTime()->null()->defaultValue(null),
            'order_id' => $this->integer()->null()->defaultValue(null),
            'user_id' => $this->string()->null()->defaultValue(null)
        ]);

        $this->createTable('{{%bitstamp_crypto_withdrawal}}', [
            'id' => $this->primaryKey(),
            'datetime' => $this->dateTime()->null()->defaultValue(null),
            'transaction_id' => $this->string()->null()->defaultValue(null),
            'address' => $this->string()->null()->defaultValue(null),
            'amount' => $this->float()->null()->defaultValue(null),
            'currency' => $this->string()->null()->defaultValue(null),
            'user_id' => $this->string()->null()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitstamp_transactions}}');
//        $this->dropTable('{{%bitstamp_withdrawal}}');
        $this->dropTable('{{%bitstamp_crypto_withdrawal}}');
    }
}
