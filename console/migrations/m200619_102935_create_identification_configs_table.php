<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%identification_configs}}`.
 */
class m200619_102935_create_identification_configs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%identification_configs}}', [
            'id' => $this->primaryKey(),
            'profile_id'=>$this->integer()->null()->defaultValue(null),
            'currency_id'=>$this->integer()->null()->defaultValue(null),
            'buy'=>$this->integer()->null()->defaultValue(null),
            'sell'=>$this->integer()->null()->defaultValue(null),
            'value'=>$this->integer()->null()->defaultValue(0),
            'common_sum_scan'=>$this->integer()->null()->defaultValue(0),
            'common_sum_photo_id'=>$this->integer()->null()->defaultValue(0),
            'common_sum_email'=>$this->integer()->null()->defaultValue(0),
            'type'=>$this->integer()->null()->defaultValue(0),
        ]);

        $this->dropColumn('identification_profile', 'buy_sell');
        $this->dropColumn('identification_profile', 'common_sum_scan');
        $this->dropColumn('identification_profile', 'common_sum_photo_id');
        $this->dropColumn('identification_profile', 'common_sum_email');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%identification_configs}}');
        $this->addColumn('{{%identification_profile}}', 'buy_sell', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%identification_profile}}', 'common_sum_scan', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%identification_profile}}', 'common_sum_photo_id', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%identification_profile}}', 'common_sum_email', $this->integer()->null()->defaultValue(null));
    }
}
