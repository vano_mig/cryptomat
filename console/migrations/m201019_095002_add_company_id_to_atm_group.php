<?php

use yii\db\Migration;

/**
 * Class m201019_095002_add_company_id_to_atm_group
 */
class m201019_095002_add_company_id_to_atm_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%atm_groups}}', 'company_id', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%atm_groups}}', 'company_id');
    }
}
