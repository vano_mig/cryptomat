<?php

use yii\db\Migration;

/**
 * Class m200908_132409_add_column_crypto_currency
 */
class m200908_132409_add_column_crypto_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%crypto_currency}}', 'active', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%crypto_currency}}', 'active');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200908_132409_add_column_crypto_currency cannot be reverted.\n";

        return false;
    }
    */
}
