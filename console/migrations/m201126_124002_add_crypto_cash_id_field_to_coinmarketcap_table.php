<?php

use yii\db\Migration;

/**
 * Class m201126_124002_add_crypto_cash_id_field_to_coinmarketcap_table
 */
class m201126_124002_add_crypto_cash_id_field_to_coinmarketcap_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%coinmarketcap}}', 'currency_cash_id', $this->tinyInteger()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%coinmarketcap}}', 'currency_cash_id');
    }
}