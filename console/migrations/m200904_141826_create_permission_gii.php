<?php

use backend\modules\user\models\User;
use yii\db\Migration;

/**
 * Class m200904_141826_create_permission_gii
 */
class m200904_141826_create_permission_gii extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$auth = Yii::$app->authManager;
		$user = $auth->getRole(User::ROLE_ADMIN);

		$create = $auth->createPermission('gii.create');
		$create->description = 'Add virtual gii device';
		$auth->add($create);
		$auth->addChild($user, $create);

		$update = $auth->createPermission('gii.update');
		$update->description = 'Update virtual gii device';
		$auth->add($update);
		$auth->addChild($user, $update);

		$delete = $auth->createPermission('gii.delete');
		$delete->description = 'Delete virtual gii device';
		$auth->add($delete);
		$auth->addChild($user, $delete);

		$view = $auth->createPermission('gii.view');
		$view->description = 'View virtual gii device';
		$auth->add($view);
		$auth->addChild($user, $view);

		$listview = $auth->createPermission('gii.list');
		$listview->description = 'View virtual gii device list';
		$auth->add($listview);
		$auth->addChild($user, $listview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$auth = Yii::$app->authManager;
		$user = $auth->getRole(User::ROLE_ADMIN);
		$create = $auth->getPermission('gii.create');
		$update = $auth->getPermission('gii.update');
		$delete = $auth->getPermission('gii.delete');
		$view = $auth->getPermission('gii.view');
		$list = $auth->getPermission('gii.list');
		$auth->removeChild($user, $create);
		$auth->removeChild($user, $update);
		$auth->removeChild($user, $delete);
		$auth->removeChild($user, $view);
		$auth->removeChild($user, $list);
		$auth->remove($create);
		$auth->remove($update);
		$auth->remove($delete);
		$auth->remove($view);
		$auth->remove($list);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200904_141826_create_permission_gii cannot be reverted.\n";

        return false;
    }
    */
}
