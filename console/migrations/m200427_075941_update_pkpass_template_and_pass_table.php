<?php

use yii\db\Migration;

/**
 * Class m200427_075941_update_pkpass_template_table
 */
class m200427_075941_update_pkpass_template_and_pass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pkpass_template}}', 'icon', $this->string()->defaultValue(null));
        $this->addColumn('{{%pkpass_template}}', 'logo', $this->string()->defaultValue(null));
        $this->addColumn('{{%pkpass_template}}', 'strip', $this->string()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'operating_system', $this->text()->defaultValue(null));
        $this->addColumn('{{%pkpass_pass}}', 'created_at', $this->dateTime()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_template}}', 'icon');
        $this->dropColumn('{{%pkpass_template}}', 'logo');
        $this->dropColumn('{{%pkpass_template}}', 'strip');
        $this->dropColumn('{{%pkpass_pass}}', 'operating_system');
        $this->dropColumn('{{%pkpass_pass}}', 'created_at');
    }
}
