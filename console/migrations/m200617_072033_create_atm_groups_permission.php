<?php

use backend\modules\user\models\User;
use yii\db\Migration;

/**
 * Class m200617_072033_create_atm_groups_permission
 */
class m200617_072033_create_atm_groups_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('atm_groups.create');
        $create->description = 'Add atm_groups';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('atm_groups.update');
        $update->description = 'Update atm_groups';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('atm_groups.delete');
        $delete->description = 'Delete atm_groups';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('atm_groups.view');
        $view->description = 'View atm_groups';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('atm_groups.list');
        $listview->description = 'View atm_groups list';
        $auth->add($listview);
        $auth->addChild($user, $listview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('atm_groups.create');
        $update = $auth->getPermission('atm_groups.update');
        $delete = $auth->getPermission('atm_groups.delete');
        $view = $auth->getPermission('atm_groups.view');
        $list = $auth->getPermission('atm_groups.list');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $list);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($list);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200617_072033_create_atm_groups_permission cannot be reverted.\n";

        return false;
    }
    */
}
