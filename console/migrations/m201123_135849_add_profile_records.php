<?php

use backend\modules\user\models\CompanyProfile;
use common\models\Company;
use yii\db\Migration;

/**
 * Class m201123_135849_add_profile_records
 */
class m201123_135849_add_profile_records extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $list = Company::find()->all();
        if(!empty($list)) {
            foreach ($list as $item) {
                if(($profile = CompanyProfile::findOne($item->id)) === null) {
                    $model = new CompanyProfile();
                    $model->company_id = $item->id;
                    $model->save(false);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201123_135849_add_profile_records cannot be reverted.\n";

        return false;
    }
}
