<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction}}`.
 */
class m191003_070557_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'fridge_unique_id'=>$this->string()->null()->defaultValue(null),
            'transaction_id'=>$this->string()->null()->defaultValue(null),
            'wallet'=>$this->string()->null()->defaultValue(null),
            'operation'=>$this->text()->null()->defaultValue(null),
            'status_transaction'=>$this->string()->null()->defaultValue(null),
            'status_system'=>$this->string()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transaction}}');
    }
}
