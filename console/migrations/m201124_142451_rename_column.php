<?php

use yii\db\Migration;

/**
 * Class m201124_142451_rename_column
 */
class m201124_142451_rename_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->renameColumn('{{%replenish_wallet}}', 'user_id', 'company_id');
		$this->renameColumn('{{%exchange_wallet}}', 'user_id', 'company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->renameColumn('{{%replenish_wallet}}', 'company_id', 'user_id');
		$this->renameColumn('{{%exchange_wallet}}', 'company_id', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_142451_rename_column cannot be reverted.\n";

        return false;
    }
    */
}
