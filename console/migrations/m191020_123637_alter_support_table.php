<?php

use yii\db\Migration;

/**
 * Class m191020_123637_alter_support_table
 */
class m191020_123637_alter_support_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%support}}');
        $this->createTable('{{%support}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->null()->defaultValue(null),
            'client_name' => $this->string(255)->null()->defaultValue(null),
            'email' => $this->string(100)->null()->defaultValue(null),
            'category' => $this->string(50)->defaultValue(null),
            'title' => $this->string(255)->defaultValue(null),
            'text' => $this->text()->null()->defaultValue(null),
            'status' => $this->string(50)->null()->defaultValue('active'),
            'support_id' => $this->integer()->null()->defaultValue(null),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%support}}');
        $this->createTable('{{%support}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->string(32)->null()->defaultValue(null),
            'client_name' => $this->string(32)->null()->defaultValue(null),
            'email' => $this->string(32)->null()->defaultValue(null),
            'technical_mark' => $this->string(32)->defaultValue(null),
            'text' => $this->string(1000)->null()->defaultValue(null),
            'state' => $this->string(100)->null()->defaultValue('active'),
            'result' => $this->string(1000)->null()->defaultValue(null),
            'created_at' => $this->dateTime()->null(),
        ]);
    }
}
