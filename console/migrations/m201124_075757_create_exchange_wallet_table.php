<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%exchange_wallet}}`.
 */
class m201124_075757_create_exchange_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%exchange_wallet}}', [
            'id' => $this->primaryKey(),
            'provider_name'=>$this->string()->null()->defaultValue(null),
            'provider_id'=>$this->tinyInteger()->null()->defaultValue(null),
            'api_key'=>$this->string()->null()->defaultValue(null),
            'api_secret'=>$this->string()->null()->defaultValue(null),
            'block'=>$this->tinyInteger()->null()->defaultValue(null),
            'user_id'=>$this->tinyInteger()->null()->defaultValue(null),
            'client_id'=>$this->string()->null()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%exchange_wallet}}');
    }
}
