<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m191205_084205_create_passcreator_push_table_and_passcreator_permissions
 */
class m191205_084205_create_passcreator_push_table_and_passcreator_permissions extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%passcreator_push_notification}}');

        $this->addColumn('{{%passcreator_template}}', 'template_status', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%passcreator_template}}', 'template_fields_status', $this->integer()->null()->defaultValue(null));

        $this->createTable('{{%passcreator_push}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->null()->defaultValue(null),
            'date_sending' => $this->date()->null()->defaultValue(null),
            'status' => $this->string()->null()->defaultValue(null),
            'template_id' => $this->string()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $view = $auth->createPermission('passcreator_service.view');
        $view->description = 'Доступ к Passcreator Service';
        $auth->add($view);
        $auth->addChild($user, $view);

    }


    public function safeDown()
    {
        $this->dropTable('{{%passcreator_push}}');

        $this->dropColumn('{{%passcreator_template}}', 'template_status');
        $this->dropColumn('{{%passcreator_template}}', 'template_fields_status');

        $this->createTable('{{%passcreator_push_notification}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->null()->defaultValue(null),
            'date_sending' => $this->date()->null()->defaultValue(null),
            'status' => $this->string()->null()->defaultValue(null),
            'template_id' => $this->string()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $view = $auth->getPermission('passcreator_service.view');
        $auth->removeChild($user, $view);
        $auth->remove($view);
    }
}
