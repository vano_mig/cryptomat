<?php

use yii\db\Migration;

/**
 * Class m200506_104830_update_pkpass_pass_table
 */
class m200506_104830_update_pkpass_pass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pkpass_pass}}', 'pass_type_identifier', $this->string()->defaultValue(null)->after('template_id'));
        $this->addColumn('{{%pkpass_device}}', 'pass_type_identifier', $this->string()->defaultValue(null)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_pass}}', 'pass_type_identifier');
        $this->dropColumn('{{%pkpass_device}}', 'pass_type_identifier');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200506_104830_update_pkpass_pass_table cannot be reverted.\n";

        return false;
    }
    */
}
