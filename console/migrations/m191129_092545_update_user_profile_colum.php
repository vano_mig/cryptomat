<?php

use yii\db\Migration;

/**
 * Class m191129_092545_update_user_profile_colum
 */
class m191129_092545_update_user_profile_colum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user_profile}}', 'first_name');
        $this->dropColumn('{{%user_profile}}', 'last_name');
        $this->addColumn('{{%user_profile}}', 'first_name_card', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'last_name_card', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user}}', 'last_name', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user_profile}}', 'first_name', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'last_name', $this->string()->null()->defaultValue(null));
        $this->dropColumn('{{%user_profile}}', 'first_name_card');
        $this->dropColumn('{{%user_profile}}', 'last_name_card');
        $this->dropColumn('{{%user}}', 'last_name');
    }
}
