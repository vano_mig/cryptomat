<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m191017_120405_freeze_to_fridge_renaming
 */
class m191017_120405_freeze_to_fridge_renaming extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('freezer.listview');
        $view = $auth->getPermission('freezer.view');
        $create = $auth->getPermission('freezer.create');
        $update = $auth->getPermission('freezer.update');
        $delete = $auth->getPermission('freezer.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);

        //доступ к пользователям
        $userCreate = $auth->createPermission('fridge.create');
        $userCreate->description = 'Создание холодильника';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('fridge.update');
        $userUpdate->description = 'Редактирование холодильника';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('fridge.delete');
        $userDelete->description = 'Удаление холодильника';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('fridge.view');
        $userView->description = 'Просмотр холодильника';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('fridge.listview');
        $userListview->description = 'Просмотр списка холодильников';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('fridge.listview');
        $view = $auth->getPermission('fridge.view');
        $create = $auth->getPermission('fridge.create');
        $update = $auth->getPermission('fridge.update');
        $delete = $auth->getPermission('fridge.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);

        //доступ к пользователям
        $userCreate = $auth->createPermission('freezer.create');
        $userCreate->description = 'Создание холодильника';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('freezer.update');
        $userUpdate->description = 'Редактирование холодильника';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('freezer.delete');
        $userDelete->description = 'Удаление холодильника';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('freezer.view');
        $userView->description = 'Просмотр холодильника';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('freezer.listview');
        $userListview->description = 'Просмотр списка холодильников';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }
}
