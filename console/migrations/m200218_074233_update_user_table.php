<?php

use yii\db\Migration;

/**
 * Class m200218_074233_update_user_table
 */
class m200218_074233_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'agent_id', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('{{%user}}', 'agent_id', $this->integer()->null()->defaultValue(null)->after('email'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'agent_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200218_074233_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
