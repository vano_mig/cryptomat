<?php

use yii\db\Migration;
use \common\models\User;

/**
 * Class m200514_075128_create_pkpass_permission
 */
class m200514_075128_create_pkpass_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('pkpass_device.create');
        $create->description = 'Add pkpass device';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('pkpass_device.update');
        $update->description = 'Update pkpass device';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('pkpass_device.delete');
        $delete->description = 'Delete pkpass device';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('pkpass_device.view');
        $view->description = 'View pkpass device';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('pkpass_device.listview');
        $listview->description = 'View pkpass device list';
        $auth->add($listview);
        $auth->addChild($user, $listview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('pkpass_device.create');
        $update = $auth->getPermission('pkpass_device.update');
        $delete = $auth->getPermission('pkpass_device.delete');
        $view = $auth->getPermission('pkpass_device.view');
        $listview = $auth->getPermission('pkpass_device.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }

}
