<?php

use yii\db\Migration;

/**
 * Class m200511_083151_update_pkpass_pass_and_template_table
 */
class m200511_083151_update_pkpass_pass_and_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%pkpass_template}}', 'update');
        $this->addColumn('{{%pkpass_pass}}', 'update', $this->boolean()->defaultValue(null)->after('template_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%pkpass_template}}', 'update', $this->boolean()->defaultValue(null)->after('status'));
        $this->dropColumn('{{%pkpass_pass}}', 'update');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200511_083151_update_pkpass_pass_and_template_table cannot be reverted.\n";

        return false;
    }
    */
}
