<?php

use backend\modules\user\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%configuration}}`.
 */
class m200415_102825_create_configuration_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%configuration}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'value'=>$this->string()->null()->defaultValue(null),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);

        foreach ($this->getParams() as $item) {
            $this->insert('{{%configuration}}', [
                'name' => $item['name'],
                'value' => $item['value'],
                'user_id' => $item['user'],
                'created_at' => $item['date'],
                'updated_at' => $item['date'],
            ]);
        }

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        //доступ к пользователям
        $userCreate = $auth->createPermission('configuration.create');
        $userCreate->description = 'Create config param';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userListview = $auth->createPermission('configuration.listview');
        $userListview->description = 'Config params list';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);

        $userUpdate = $auth->createPermission('configuration.update');
        $userUpdate->description = 'Update config param';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('configuration.delete');
        $userDelete->description = 'Delete config param';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('configuration.view');
        $userView->description = 'View config param';
        $auth->add($userView);
        $auth->addChild($admin, $userView);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%configuration}}');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('configuration.listview');
        $view = $auth->getPermission('configuration.view');
        $create = $auth->getPermission('configuration.create');
        $update = $auth->getPermission('configuration.update');
        $delete = $auth->getPermission('configuration.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }

    public function getParams():array
    {
        return [
            ['name'=>'sms_token', 'value'=>'I8YutB7pfi0LvocMFysphykW7nFQNDudORVhA2rz', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'sms_url_backup', 'value'=>'https://api2.smsapi.com/sms.do', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'sms_url', 'value'=>'https://api.smsapi.com/sms.do', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'email_public_key', 'value'=>'0aeeb26ffae5b58fc538ebf6354982fa', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'email_private_key', 'value'=>'ebc7fffd37153729401f383975120d1e', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'email_version', 'value'=>'v3.1', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'email_version_template', 'value'=>'v3', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
            ['name'=>'company_ms', 'value'=>'3', 'user'=>16, 'date'=>date('Y-m-d H:i:s')],
        ];
    }
}
