<?php

use yii\db\Migration;

/**
 * Class m201211_091319_add_advert_table
 */
class m201211_091319_add_advert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advert}}', [
            'id'=> $this->primaryKey(),
            'advert_name'=>$this->string(255)->null()->defaultValue(null),
            'unique_id'=>$this->string(255)->null()->defaultValue(null),
            'country'=>$this->integer()->null()->defaultValue(null),
            'city'=>$this->string(255)->null()->defaultValue(null),
            'company_id'=>$this->integer(11)->null()->defaultValue(null),
            'status'=>$this->string(255)->null()->defaultValue(null),
            'status_error'=>$this->string(255)->null()->defaultValue(null),
            'key'=>$this->string(255)->null()->defaultValue(null),
            'key_active'=>$this->integer(11)->null()->defaultValue(0),
            'ip_address'=>$this->integer(11)->null()->defaultValue(0),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),

        ]);

        $tableName ='{{%advert_video}}';
        /**
         * {@inheritdoc}
         */

        $this->createTable($tableName, [
            'id' => $this-> primaryKey(),
            'advert_id'=>$this->integer()->null()->defaultValue(null),
            'advertisement_id'=>$this->integer()->null()->defaultValue(null),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('{{%advert}}');
       $this->dropTable('{{%advert_video}}');
    }
}
