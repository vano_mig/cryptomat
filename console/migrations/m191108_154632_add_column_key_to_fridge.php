<?php

use yii\db\Migration;

/**
 * Class m191108_154632_add_column_key_to_fridge
 */
class m191108_154632_add_column_key_to_fridge extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%fridge}}', 'key', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%fridge}}', 'key_active', $this->integer()->null()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->dropColumn('{{%fridge}}', 'key');
      $this->dropColumn('{{%fridge}}', 'key_active');
    }
}
