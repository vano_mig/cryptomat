<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%coinbase}}`.
 */
class m200707_065924_create_coinbase_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%coinbase_transaction}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null()->defaultValue(null),
            'buy_id' => $this->string()->null()->defaultValue(null),
            'status' => $this->string()->null()->defaultValue(null),
            'payment_method_id' => $this->string()->null()->defaultValue(null),
            'transaction_id' => $this->string()->null()->defaultValue(null),
            'amount' => $this->float()->null()->defaultValue(null),
            'currency' => $this->string()->null()->defaultValue(null),
            'total_amount' => $this->float()->null()->defaultValue(null),
            'total_currency' => $this->string()->null()->defaultValue(null),
            'subtotal_amount' => $this->float()->null()->defaultValue(null),
            'subtotal_currency' => $this->string()->null()->defaultValue(null),
            'fee_amount' => $this->float()->null()->defaultValue(null),
            'fee_currency' => $this->string()->null()->defaultValue(null),
            'resource' => $this->string()->null()->defaultValue(null),
            'committed' => $this->integer()->null()->defaultValue(null),
            'instant' => $this->integer()->null()->defaultValue(null),
            'payout_at' => $this->string()->null()->defaultValue(null),
            'created_at' => $this->dateTime()->null()->defaultValue(null),
            'updated_at' => $this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%coinbase_transaction}}');
    }
}
