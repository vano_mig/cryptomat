<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m191223_095922_add_transaction_permissions
 */
class m191223_095922_add_transaction_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $listview = $auth->createPermission('transaction.listview');
        $listview->description = 'Transaction list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        $view = $auth->createPermission('transaction.view');
        $view->description = 'View transaction data';
        $auth->add($view);
        $auth->addChild($user, $view);

        $create = $auth->createPermission('transaction.create');
        $create->description = 'Create transaction';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('transaction.update');
        $update->description = 'Update transaction data';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('transaction.delete');
        $delete->description = 'delete transaction';
        $auth->add($delete);
        $auth->addChild($user, $delete);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('transaction.create');
        $update = $auth->getPermission('transaction.update');
        $delete = $auth->getPermission('transaction.delete');
        $view = $auth->getPermission('transaction.view');
        $listview = $auth->getPermission('transaction.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }
}
