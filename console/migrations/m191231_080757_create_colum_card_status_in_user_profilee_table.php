<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colum_card_status_in_user_profilee}}`.
 */
class m191231_080757_create_colum_card_status_in_user_profilee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'card_status', $this->string()->null()->defaultValue(null)->after('ref_card_no'));
        $this->alterColumn('{{%user_profile}}', 'mailing', $this->string()->null()->defaultValue(null)->after('user_id'));
        $this->alterColumn('{{%user_profile}}', 'privacy_policy', $this->string()->null()->defaultValue(null)->after('mailing'));
        $this->alterColumn('{{%user_profile}}', 'terms_of_service', $this->string()->null()->defaultValue(null)->after('privacy_policy'));
        $this->alterColumn('{{%user_profile}}', 'client_from', $this->string()->null()->defaultValue(null)->after('post_code'));
        $this->alterColumn('{{%user_profile}}', 'first_name_card', $this->string()->null()->defaultValue(null)->after('card_status'));
        $this->alterColumn('{{%user_profile}}', 'last_name_card', $this->string()->null()->defaultValue(null)->after('first_name_card'));
        $this->alterColumn('{{%user_profile}}', 'reg_person', $this->string()->null()->defaultValue(null)->after('card_status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'card_status');
    }
}
