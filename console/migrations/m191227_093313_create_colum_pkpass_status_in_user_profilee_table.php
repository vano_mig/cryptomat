<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colum_pkpass_status_in_user_profilee}}`.
 */
class m191227_093313_create_colum_pkpass_status_in_user_profilee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'pkpass_status', $this->boolean()->null()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'pkpass_status');
    }
}
