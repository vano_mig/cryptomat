<?php

use yii\db\Migration;

/**
 * Class m191209_114248_alter_domain_field_paysystem
 */
class m191209_114248_alter_domain_field_paysystem extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%paysystem}}', 'domain', 'identificator');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%paysystem}}', 'identificator', 'domain');
    }
}
