<?php

use yii\db\Migration;

/**
 * Class m201126_152250_edit_fee_in_atm_table
 */
class m201126_152250_edit_fee_in_atm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%atm}}', 'fee_max');
        $this->renameColumn('{{%atm}}', 'fee_min', 'fee');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%atm}}', 'fee', 'fee_min');
        $this->addColumn('{{atm}}', 'fee_max', $this->decimal(10,2)->null()->defaultValue(null)->after('fee_min'));
    }
}
