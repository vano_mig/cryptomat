<?php

use yii\db\Migration;


class m200117_102555_create_colum_birthday_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'birthday', $this->date()->null()->defaultValue(null)->after('last_name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'birthday');
    }
}
