<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m190925_122734_add_permissions
 */
class m190925_122734_add_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $publisher = $auth->createRole(User::ROLE_USER);
        $publisher->description = 'user';
        $auth->add($publisher);

        $manager = $auth->createRole(User::ROLE_MANAGER);
        $manager->description = 'manager';
        $auth->add($manager);

        $auth = Yii::$app->authManager;
        $customer = $auth->createRole(User::ROLE_CUSTOMER);
        $customer->description = 'customer';
        $auth->add($customer);

        $auth = Yii::$app->authManager;
        $admin = $auth->createRole(User::ROLE_ADMIN);
        $admin->description = 'administrator';
        $auth->add($admin);

        //Создаем права доступа для ролей

        //доступ к пользователям
        $userCreate = $auth->createPermission('user.create');
        $userCreate->description = 'Создание пользователя';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('user.update');
        $userUpdate->description = 'Редактирование пользователя';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('user.delete');
        $userDelete->description = 'Удаление пользователя';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('user.view');
        $userView->description = 'Просмотр пользователя';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('user.listview');
        $userListview->description = 'Просмотр списка пользователей';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);

        //доступ к языкам
        $langCreate = $auth->createPermission('language.create');
        $langCreate->description = 'Создание языка';
        $auth->add($langCreate);
        $auth->addChild($admin, $langCreate);
        $auth->addChild($manager, $langCreate);

        $langUpdate = $auth->createPermission('language.update');
        $langUpdate->description = 'Редактирование языка';
        $auth->add($langUpdate);
        $auth->addChild($admin, $langUpdate);
        $auth->addChild($manager, $langUpdate);

        $langDelete = $auth->createPermission('language.delete');
        $langDelete->description = 'Удаление языка';
        $auth->add($langDelete);
        $auth->addChild($admin, $langDelete);
        $auth->addChild($manager, $langDelete);

        $langView = $auth->createPermission('language.view');
        $langView->description = 'Просмотр языка';
        $auth->add($langView);
        $auth->addChild($admin, $langView);
        $auth->addChild($manager, $langView);

        $langListview = $auth->createPermission('language.listview');
        $langListview->description = 'Просмотр списка языков';
        $auth->add($langListview);
        $auth->addChild($admin, $langListview);
        $auth->addChild($manager, $langListview);

        //доступ к странам
        $countryCreate = $auth->createPermission('country.create');
        $countryCreate->description = 'Создание страны';
        $auth->add($countryCreate);
        $auth->addChild($admin, $countryCreate);
        $auth->addChild($manager, $countryCreate);

        $countryUpdate = $auth->createPermission('country.update');
        $countryUpdate->description = 'Редактирование страны';
        $auth->add($countryUpdate);
        $auth->addChild($admin, $countryUpdate);
        $auth->addChild($manager, $countryUpdate);

        $countryDelete = $auth->createPermission('country.delete');
        $countryDelete->description = 'Удаление страны';
        $auth->add($countryDelete);
        $auth->addChild($admin, $countryDelete);
        $auth->addChild($manager, $countryDelete);

        $countryView = $auth->createPermission('country.view');
        $countryView->description = 'Просмотр страны';
        $auth->add($countryView);
        $auth->addChild($admin, $countryView);
        $auth->addChild($manager, $countryView);

        $countryListview = $auth->createPermission('country.listview');
        $countryListview->description = 'Просмотр списка стран';
        $auth->add($countryListview);
        $auth->addChild($admin, $countryListview);
        $auth->addChild($manager, $countryListview);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->authManager->removeAll();
    }

}
