<?php

use yii\db\Migration;

/**
 * Class m200826_120512_add_reload_field_to_virtual_terminal
 */
class m200826_120512_add_reload_field_to_virtual_terminal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%virtual_terminal}}', 'reload', $this->dateTime()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%virtual_terminal}}', 'reload');
    }
}
