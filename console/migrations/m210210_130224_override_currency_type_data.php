<?php

use yii\db\Migration;

/**
 * Class m210210_130224_override_currency_type_data
 */
class m210210_130224_override_currency_type_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->dropColumn('transaction', 'currency_sent');
		$this->dropColumn('transaction', 'currency_receive');
		$this->addColumn('transaction', 'currency_sent', $this->integer()->defaultValue(null)->null());
		$this->addColumn('transaction', 'currency_receive', $this->integer()->defaultValue(null)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('transaction', 'currency_sent');
		$this->dropColumn('transaction', 'currency_receive');
		$this->addColumn('transaction', 'currency_sent', $this->string()->defaultValue(null)->null());
		$this->addColumn('transaction', 'currency_receive', $this->string()->defaultValue(null)->null());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210210_130224_override_currency_type_data cannot be reverted.\n";

        return false;
    }
    */
}
