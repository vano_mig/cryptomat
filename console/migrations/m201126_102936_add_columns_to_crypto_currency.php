<?php

use yii\db\Migration;

/**
 * Class m201126_102936_add_columns_to_crypto_currency
 */
class m201126_102936_add_columns_to_crypto_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%crypto_currency}}', 'icon', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%crypto_currency}}', 'address_regex', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%crypto_currency}}', 'min_price', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%crypto_currency}}', 'full_name', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%crypto_currency}}', 'icon');
        $this->dropColumn('{{%crypto_currency}}', 'address_regex');
        $this->dropColumn('{{%crypto_currency}}', 'min_price');
        $this->dropColumn('{{%crypto_currency}}', 'full_name');
    }
}
