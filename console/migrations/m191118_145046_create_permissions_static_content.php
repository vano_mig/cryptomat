<?php

use yii\db\Migration;

use common\models\User;

/**
 * Class m191118_145046_create_permissions_static_content
 */
class m191118_145046_create_permissions_static_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('static_content.create');
        $create->description = 'Добавление контента';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('static_content.update');
        $update->description = 'Редактирование контента';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('static_content.delete');
        $delete->description = 'Удаление контента';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('static_content.view');
        $view->description = 'Просмотр контента';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('static_content.listview');
        $listview->description = 'Просмотр списка имеющегося контента';
        $auth->add($listview);
        $auth->addChild($user, $listview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('static_content.create');
        $update = $auth->getPermission('static_content.update');
        $delete = $auth->getPermission('static_content.delete');
        $view = $auth->getPermission('static_content.view');
        $listview = $auth->getPermission('static_content.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191118_145046_create_permissions_static_content cannot be reverted.\n";

        return false;
    }
    */
}
