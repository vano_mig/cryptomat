<?php

use yii\db\Migration;

/**
 * Class m200908_081542_add_column_to_replenish_wallet
 */
class m200908_081542_add_column_to_replenish_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%replenish_wallet}}', 'client_id', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%replenish_wallet}}', 'client_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200908_081542_add_column_to_replenish_wallet cannot be reverted.\n";

        return false;
    }
    */
}
