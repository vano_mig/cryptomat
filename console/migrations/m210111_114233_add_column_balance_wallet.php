<?php

use yii\db\Migration;

/**
 * Class m210111_114233_add_column_balance_wallet
 */
class m210111_114233_add_column_balance_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%replenish_wallet}}', 'balance', $this->decimal(64,8)->null()->defaultValue(null));
		$this->addColumn('{{%exchange_wallet}}', 'balance', $this->decimal(64,8)->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%replenish_wallet}}', 'balance');
        $this->dropColumn('{{%exchange_wallet}}', 'balance');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_114233_add_column_balance_wallet cannot be reverted.\n";

        return false;
    }
    */
}
