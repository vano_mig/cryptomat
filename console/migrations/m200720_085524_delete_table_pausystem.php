<?php

use yii\db\Migration;

/**
 * Class m200720_085524_delete_table_pausystem
 */
class m200720_085524_delete_table_pausystem extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%paysystem}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_085524_delete_table_pausystem cannot be reverted.\n";

        return false;
    }
}
