<?php

use yii\db\Migration;

/**
 * Class m191002_095630_add_store_id_field_to_freezer_table
 */
class m191002_095630_add_store_id_field_to_freezer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('freezer', 'store_id', $this->integer()->null()->defaultValue(null)->after('user_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('freezer', 'store_id');
    }
}
