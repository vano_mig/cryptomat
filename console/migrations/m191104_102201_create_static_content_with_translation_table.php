<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%static_page_with_translation}}`.
 */
class m191104_102201_create_static_content_with_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%static_content}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%static_content_language}}', [
            'id' => $this->primaryKey(),
            'content_id' => $this->integer()->null()->defaultValue(null),
            'language' => $this->string()->null()->defaultValue(null),
            'content' => $this->text()->null()->defaultValue(null),
        ]);
        $this->insert('{{%static_content}}', ['name' => 'termsOfService',]);
        $this->insert('{{%static_content}}', ['name' => 'privacyPolicy',]);
        $this->insert('{{%static_content}}', ['name' => 'mailing',]);

        $this->insert('{{%static_content_language}}', ['content_id' => '1', 'language' => 'de', 'content' => '
    <h1>Allgemeine Geschäftsbedingungen (AGB)</h1>

<p>Allgemeine Geschäftsbedingungen der My Smart Store 24 Automatisierte Shopsysteme im Zusammenhang mit der Nutzung von
    MS- Store24-Produkten</p>


<h2>§ 1 Geltungsbereich; Änderung der AGB</h2>
<ol>
    <li>Die My Smart Store 24 Automatisierte Shopsysteme hat Ihrem Unternehmen einen MS- Store 24-Automaten (nachfolgend
        auch „Automat“) zur Verfügung gestellt, über den registrierte Nutzer via Handy-App oder Kundenkarte und nicht
        registrierte Nutzer mit Hilfe Ihrer Kreditkarte Getränke, Snacks und frische Mahlzeiten beziehen können.
    </li>

    <li>My Smart Store 24 Automatisierte Shopsysteme stellt den Nutzern eine Möglichkeit zur Registrierung unter
        https://www.my-smart-store24.de/registrierung (nachfolgend auch „Registrierungsportal“)<br>
        Sämtliche Services von My Smart Store 24 im Zusammenhang mit der Registrierung, der Nutzung der Website sowie
        der Nutzung der Automaten werden nachfolgend zusammenfassend als „Services“ bezeichnet.
    </li>
    <li>Abweichende, entgegenstehende oder ergänzende allgemeine Geschäftsbedingungen des Nutzers werden nicht
        Vertragsbestandteil.
    </li>
    <li>Wir behalten uns das Recht vor, diese AGB mit Wirkung für die Zukunft nach folgendem Verfahren zu ändern: Hierzu
        werden wir Ihnen vorher die vorgesehenen Änderungen mitteilen und Sie auf Ihr Widerspruchsrecht hinweisen. Die
        Änderungen gelten als angenommen, wenn Sie nicht binnen zwei Monaten nach der Änderungsmitteilung widersprechen.
        Falls Sie der Änderung widersprechen, behalten wir uns bei Angebotsvarianten mit unbestimmter Laufzeit bzw.
        Anzahl von Lieferungen die ordentliche Kündigung vor.
    </li>
</ol>

<h2>§ 2 Auftragserteilung, Vertragsinhalt</h2>
<ol>
    <li>
        Diese Allgemeinen Verkaufs- und Lieferbedingungen gelten sowohl gegenüber Käufern, die Unternehmer im Sinne des
        § 14
        BGB sind, als auch sämtlichen Käufern (registriert oder nicht registriert) die unsere Dienstleistungen in
        Anspruch
        nehmen. Für die gegenwärtigen und künftigen Geschäftsbeziehungen mit dem Käufer gelten diese Allgemeinen
        Verkaufs- und
        Lieferbedingungen sowie zusätzlich für die Lieferung von Getränkeprodukten und für die Aufstellung von Geräten
        die auf
        den Preislisten bzw. Aufstellvereinbarungen abgedruckten Besonderen Verkaufs-/ Vertragsbedingungen, soweit nicht
        im
        Einzelfall schriftlich etwas abweichendes vereinbart ist. Der Käufer erkennt diese Bedingungen spätestens durch
        die
        teilweise oder gänzliche Abnahme der gelieferten Ware an. Abweichende Geschäftsbedingungen des Käufers werden
        selbst
        dann nicht Vertragsbestandteil, wenn dieser einen Auftrag zu seinen Bedingungen bestätigt und wir dem nicht
        ausdrücklich
        widersprechen oder wenn wir in Kenntnis entgegenstehender Bedingungen des Käufers die Lieferung an den Käufer
        vorbehaltlos ausführen. Selbst wenn wir auf ein Schreiben Bezug nehmen, das Geschäftsbedingungen des Käufers
        oder eines
        Dritten enthält oder auf solche verweist, liegt darin kein Einverständnis mit der Geltung jener
        Geschäftsbedingungen.
    </li>
    <li>Die von uns auf unserer Website, in Produktkatalogen oder auf andere Art und Weise dargestellten Produkte
        verstehen
        sich stets freibleibend, d.h. sie stellen lediglich die Aufforderung an den Käufer dar, eine verbindliche
        Bestellung
        aufzugeben. Angebote des Käufers werden erst mit unserer ausdrücklichen Annahme verbindlich, die auch durch
        Lieferung
        oder Rechnungserteilung erfolgen kann.
    </li>
    <li>Nebenabreden oder spätere Änderungen der getroffenen Vereinbarungen einschließlich dieser Allgemeinen Verkaufs-
        und
        Lieferbedingungen – inklusive dieser Schriftformklausel – bedürfen zu ihrer Wirksamkeit der Schriftform.
    </li>
    <li>Aus Gründen der Frische- und Qualitätssicherung für unsere Produkte behalten wir uns vor, Aufträge nicht
        anzunehmen,
        die über den durchschnittlichen Bedarf für den Zeitraum einer Normalbelieferung oder einer Promotion
        hinausgehen, der
        sich aus dem planmäßigen Belieferungsrhythmus ergibt. Bei Aufträgen, die über diesen genannten Bedarf
        hinausgehen, ist
        unsere Belieferung freibleibend. Unabhängig davon gelten die jeweiligen in den Besonderen Verkaufsbedingungen
        für die
        Getränkeprodukte angegebenen Mindestabnahmemengen.
    </li>
</ol>


<h2>§ 3 Registrierung; Allgemeine Pflichten des Nutzers; Löschung des Accounts</h2>
<ol>
    <li>Um Zugang zu unseren Services zu erhalten, können Sie sich in unserem Registrierungsportal als Nutzer
        registrieren.
        Ihre MS-Store24-Kundenkarte erhalten Sie danach an dem Serviceschalter in Ihrem Unternehmen oder per Post.
        Alternativ
        können Sie die Bedienung des Automaten und den Bezahlvorgang mit Hilfe der Handy-App bewerkstelligen. Die
        Registrierung
        ist kostenlos. Falls Sie unter 18 Jahre sind, ist für die Registrierung die Mitwirkung eines
        Erziehungsberechtigten
        erforderlich.
    </li>

    <li>Eine Registrierung erfolgt für die Nutzung der Ihrem Unternehmen und anderen frei zugänglich bereitgestellten
        Automaten.
    </li>

    <li>Alle Angaben, die Sie im Zusammenhang mit der Registrierung machen, insbesondere Ihre Kreditkartendaten, müssen
        aktuell und wahrheitsgemäß sein. Ihr Passwort dürfen Sie nicht an Dritte weitergeben, sie müssen es für
        Unberechtigte
        unzugänglich sicher verwahren und uns ein Abhandenkommen oder eine Weitergabe unverzüglich in Textform anzeigen.
    </li>

    <li>Mit der Registrierung kommt noch kein Vertrag zwischen Ihnen und MS- Store 24 zustande. Vielmehr schließen Sie
        erst
        am Automaten nach Maßgabe des § 4 dieser AGB einzelne Kaufverträge mit uns ab.
    </li>

    <li>Sie können Ihren Account jederzeit löschen. Bitte beachten Sie, dass Sie unsere Services dann nicht mehr in
        Anspruch
        nehmen können.
    </li>
</ol>
<h2>§ 4 Kauf von Automatenprodukten</h2>
<ol>
    <li>Unser Angebot ist stets auf den Vorrat des jeweiligen Automaten beschränkt. MS- Store 24 ist nicht verpflichtet,
        dem
        Nutzer eine bestimmte Ware zur Verfügung zu stellen, wenn der Warenvorrat im betreffenden Automaten erschöpft
        ist. Unser
        Angebot steht zudem unter dem Vorbehalt der technischen Funktionsfähigkeit des Automaten.
    </li>

    <li>Entnehmen Sie ein oder mehrere Produkte aus einem der Kühlschränke kommt ein gültiger Kaufvertrag zwischen MS
        Store
        24 und dem Nutzer durch Annahme unseres Angebots durch den Nutzer zustande. Nach Identifizierung des Nutzers am
        Automaten kann der Nutzer die gewünschten Produkte in der gewünschten Menge aus dem Automaten entnehmen. Sobald
        der
        Kühlschrank geschlossen wird, ist der Kaufvertrag abgeschlossen und der Kaufvorgang beendet.<br>
        Der Automat kann nun auf Ihre Kreditkarteninformationen zugreifen. Nach Abschluss des Kaufvorgangs wird Ihre
        Kreditkarte
        automatisch mit dem Kaufpreis belastet.
    </li>

    <li>Sie haben sicherzustellen, dass Ihre Kreditkarte bei Inanspruchnahme unserer Automatenservices gedeckt ist.
        Etwaige
        Rückbuchungsgebühren werden wir von Ihnen erstattet verlangen.
    </li>

    <li>Sollten die am Automaten erworbenen Produkte mangelhaft sein, stehen dem Nutzer die gesetzlichen
        Gewährleistungsrechte zu.
    </li>

    <li>Ein Widerrufsrecht kommt Ihnen hinsichtlich der am Automaten gekauften Produkte nicht zu (§ 312 Abs. 2 Nr. 9
        BGB).
    </li>
</ol>

<h2>§ 6 Preise, Zahlungsbedingungen</h2>
<ol>

    <li>Die Rechnungserteilung erfolgt auf der Grundlage der am Tage der Lieferung angezeigten Preise auf dem Display
        des
        Automaten.
    </li>

    <li>Alle Preise verstehen sich zuzüglich der jeweils gültigen gesetzlichen Mehrwertsteuer.</li>

    <li>Sie erhalten 2% Skonto, wenn Sie am Lastschriftverfahren mit 8 Tagen Zahlungsfrist teilnehmen. Oder nutzen Sie
        einfach die bequeme Abbuchung durch uns ohne Abzüge mit 30 Tagen Zahlungsfrist.<br>
        Möchten Sie am SEPA-Lastschriftverfahren mit Abbuchungsauftrag teilnehmen, dann lassen Sie uns bitte den Ihnen
        zur
        Verfügung gestellten Abbuchungsauftrag ausgefüllt und unterzeichnet zukommen.
    </li>

    <li>Die Aufrechnung mit Gegenansprüchen des Käufers oder die Zurückbehaltung von Zahlungen wegen solcher Ansprüche
        ist
        nur zulässig, soweit diese Gegenansprüche des Käufers unbestritten oder rechtskräftig festgestellt sind.
    </li>

    <li>Unsere Mitarbeiter sind nicht zum Inkasso berechtigt.</li>

    <li>Haben wir der Einschaltung eines Inkassokontors zugestimmt, so erlischt die Zahlungsverpflichtung des Käufers
        nicht
        mit der Zahlung an das Kontor, sondern erst mit Zahlungseingang bei uns.
    </li>

    <li>Bei Zahlungsverzug des Käufers sind wir berechtigt, ihm für die Dauer des Verzuges Zinsen in Höhe von 9
        Prozentpunkten über dem Basiszinssatz (§ 247 BGB) zu berechnen. Die Geltendmachung weitergehenden Schadens
        bleibt
        vorbehalten.
    </li>
</ol>

<h2>§ 7 Kundendienst</h2>
<ol>

    <li>
        Bei jeglichen Störfällen, bitten wir Sie, sich umgehend bei uns oder Ihrem Unternehmen zu melden. Sie erreichen
        uns
        unter XXX
    </li>
</ol>

<h2>§ 8 Eigentumsvorbehalt</h2>

<ol>
    <li>Die gelieferte Ware bleibt bis zur vollständigen Erfüllung sämtlicher, auch künftiger Forderungen aus der
        Geschäftsbeziehung mit dem Käufer unser Eigentum. Bei laufender Rechnung gilt das vorbehaltene Eigentum als
        Sicherung
        unserer Saldoforderung.
    </li>

    <li>Der Käufer darf die Vorbehaltsware im gewöhnlichen Geschäftsverkehr veräußern, solange er nicht mit seinen uns
        gegenüber bestehenden Verpflichtungen in Verzug ist und solange sich seine Vermögensverhältnisse nicht
        wesentlich
        verschlechtern. Sämtliche aus der Warenveräußerung entstehenden Forderungen, einschließlich etwaiger
        Sicherheiten, tritt
        der Käufer hiermit sicherheitshalber in Höhe unserer Kaufpreisforderung an uns ab. Erfolgt die Veräußerung
        zusammen mit
        anderen, uns nicht gehörenden Waren oder im Zusammenhang mit anderen Leistungen, gilt die Abtretung der
        Forderung aus
        der Weiterveräußerung in Höhe des Wertes der Vorbehaltsware.
    </li>

    <li>Der Käufer ist berechtigt, die abgetretenen Forderungen aus der Weiterveräußerung einzuziehen, solange er seinen
        Zahlungsverpflichtungen ordnungsgemäß nachkommt. Wir sind berechtigt, diese Ermächtigung zu widerrufen, Dritte
        von der
        Abtretung zu benachrichtigen und die Forderungen selbst einzuziehen, sobald der Käufer mit
        Zahlungsverpflichtungen in
        Verzug gerät oder in seinen Vermögensverhältnissen eine wesentliche Verschlechterung eintritt. In diesem Fall
        können wir
        verlangen, dass der Käufer uns die abgetretenen Forderungen und deren Schuldner bekannt gibt, alle zum Einzug
        erforderlichen Angaben macht, die dazugehörigen Unterlagen aushändigt und den Schuldnern (Dritten) die Abtretung
        mitteilt. Die Geltendmachung des Eigentumsvorbehalts gilt nur dann als Rücktritt vom Vertrag, wenn wir dies
        ausdrücklich
        schriftlich erklären.
    </li>

    <li>Der Käufer ist zur sachgemäßen Behandlung der Vorbehaltsware verpflichtet. Die Verpfändung oder
        Sicherungsübereignung von Vorbehaltsware an Dritte ist ausgeschlossen. Bei Pfändungen hat der Käufer
        ausdrücklich auf
        unsere Eigentumsrechte hinzuweisen und uns unverzüglich zu benachrichtigen.
    </li>

    <li>Der Käufer gestattet uns hiermit unwiderruflich Zutritt während der üblichen Geschäftszeiten zu seinen
        Geschäftsräumen sowie zu seinen Lagern zur Feststellung der in unserem Eigentum stehenden Waren. Erfüllt der
        Käufer
        seine Verpflichtungen aus den mit uns bestehenden Geschäftsverbindungen nicht, so sind wir berechtigt, die
        Vorbehaltsware jederzeit an uns zu nehmen. Das Gleiche gilt, wenn eine wesentliche Verschlechterung in den
        Vermögensverhältnissen des Käufers eintritt oder droht.
    </li>

    <li>Übersteigt der realisierbare Wert der uns zustehenden Sicherung unsere Gesamtforderung gegenüber dem Käufer um
        mehr
        als 10%, so sind wir auf Verlangen des Käufers insoweit zur Freigabe von Sicherheiten, die wir auswählen werden,
        verpflichtet.
    </li>
    </li>
</ol>

<h2>§ 9 Mängelhaftung</h2>
<ol>
    <li>Für die Rechte des Käufers bei Sach- und Rechtsmängeln gelten die gesetzlichen Vorschriften, soweit nachfolgend
        und
        in § 6 nichts anderes bestimmt ist. In allen Fällen unberührt bleiben die gesetzlichen Sondervorschriften bei
        Endlieferung der Ware an einen Verbraucher (Lieferantenregress gem. §§ 478, 479 BGB); die Haftung auf
        Schadensersatz
        besteht jedoch auch in diesem Falle ausschließlich im Rahmen des § 6.
    </li>

    <li>Die Gewährleistungsfrist beträgt ein Jahr ab Lieferung.</li>

    <li>Bei Lieferung erkennbare Mängel und Beschädigungen der Ware oder Verpackung sowie Mengenabweichungen und
        Fehllieferungen sind dem abliefernden Spediteur bei Warenempfang auf unserer Lieferquittung zu vermerken und uns
        daneben
        unverzüglich schriftlich mitzuteilen. Unterbleibt eine fristgerechte Mängelrüge, können aus solchen Mängeln
        keine
        Ansprüche gegen uns hergeleitet werden.
    </li>

    <li>Bei jeder Mängelrüge steht uns das Recht zur Besichtigung und Prüfung der beanstandeten Ware in unverändertem
        Zustand zu.
    </li>

    <li>Mängel eines Teils der Lieferung berechtigen den Käufer nicht zur Beanstandung der weitergehenden Lieferung,
        sofern
        die Brauchbarkeit der Gesamtlieferung nicht unzumutbar eingeschränkt ist.
    </li>

    <li>Ist die gelieferte Ware mangelhaft, beschränkt sich der Nacherfüllungsanspruch des Käufers auf Ersatzlieferung.
        Schlägt die Ersatzlieferung fehl oder ist sie innerhalb angemessener Frist nicht möglich oder verstreicht eine
        vom
        Käufer gesetzte angemessene Frist, ohne dass der Mangel behoben ist, oder wird die Mängelbeseitigung schuldhaft
        verzögert, so kann der Käufer nach seiner Wahl vom Vertrag zurücktreten oder zur angemessenen Herabsetzung des
        Kaufpreises (Minderung) verlangen.
    </li>

    <li>Der Ablauf des Mindesthaltbarkeitsdatums (MHD) beim Käufer berechtigt den Käufer nicht zur Rückgabe der Ware
        oder
        zur Geltendmachung anderer Ansprüche wie Nacherfüllung, Rücktritt oder Minderung.
    </li>

    <li>Der GTIN-Code oder die Darstellung als Strich-Code auf unseren Produkten bedeutet nur die Zuordnung zur
        Europäischen
        Artikelnummer. Die Nichtlesbarkeit des Codes berechtigt nur dann zu einer Mängelrüge, wenn die nach dem
        jeweiligen Stand
        der Technik hinnehmbare Fehlerquote überschritten wird und dies auf grober Fahrlässigkeit oder Vorsatz
        unsererseits
        beruht.
    </li>

    <li>Soweit Angaben über die zu liefernde Ware Vertragsbestandteil werden, enthalten sie nur insoweit eine
        Beschaffenheits-, Haltbarkeits- oder sonstige Garantie, als wir eine solche Garantie ausdrücklich übernehmen.
        Die
        Übernahme einer Garantie ist nur mit unserer schriftlichen Bestätigung wirksam.
    </li>
</ol>

<h2>§ 10 Haftung</h2>
<ol>
    <li>Soweit die getroffenen Vereinbarungen keine abweichenden Regelungen enthalten, sind alle Schadensersatzansprüche
        des
        Käufers (z.B. aus Nichterfüllung, Unmöglichkeit, sonstigem Ausschluss der Leistungspflicht, Verzug, Sachmängeln,
        Rechtsmängeln, Verletzung von vertraglichen Pflichten, Verletzung von Pflichten bei Vertragsverhandlungen,
        Ausgleich
        unter Gesamtschuldnern, unerlaubter Handlung und Delikt etc.) gegen uns sowie gegen unsere gesetzlichen
        Vertreter und
        Erfüllungsgehilfen ausgeschlossen. Die Haftungsausschlüsse gelten jedoch nicht für Schäden aus der Verletzung
        des
        Lebens, des Körpers oder der Gesundheit, die von uns, unseren gesetzlichen Vertretern oder Erfüllungsgehilfen
        vorsätzlich oder fahrlässig verursacht werden, für sonstige Schäden, die von uns, unseren gesetzlichen
        Vertretern, oder
        Erfüllungsgehilfen vorsätzlich oder grob fahrlässig verursacht werden, sowie für Schäden wegen der Verletzung
        wesentlicher Vertragspflichten. Wesentliche Vertragspflichten sind die Verpflichtung zur Lieferung einer
        mangelfreien
        Sache sowie Obhuts- und Schutzpflichten, auf deren Einhaltung der Kunde regelmäßig vertrauen darf.
    </li>

    <li>Unsere Haftung ist, außer im Falle von Vorsatz und grober Fahrlässigkeit, auf den vertragstypischen,
        voraussehbaren
        Schaden begrenzt.
    </li>

    <li>Soweit unsere Haftung ausgeschlossen oder eingeschränkt ist, gilt dies auch im Hinblick auf die persönliche
        Schadensersatzhaftung unserer Angestellten, Arbeitnehmer, Mitarbeiter, Vertreter und Erfüllungsgehilfen. Die
        Begrenzungen nach diesem § 6 gelten auch, soweit der Käufer anstelle eines Anspruchs auf Ersatz des Schadens
        statt der
        Leistung den Ersatz vergeblicher Aufwendungen verlangt.
    </li>

    <li>Die Haftung aus dem Produkthaftungsgesetz bleibt unberührt.</li>

    <li>Der Nutzer ist verpflichtet Produkt-, Verzehr– und Warnhinweise zu den Produkten vor Verwendung sorgfältig zu
        lesen
        und zu beachten.
    </li>

</ol>
<h2>§ 11 Beendigung des Zugangs zur Nutzung; Zugangsstörungen</h2>
<ol>
    <li>Wir behalten uns das Recht vor, den Zugang zu unseren Services jederzeit, auch ohne Grund und ohne vorherige
        Ankündigung, zu beenden. Insbesondere sind wir berechtigt, unsere Automaten jederzeit von ihrem Standort
        abzuziehen.
    </li>

    <li>Es besteht kein Anspruch auf jederzeitigen, ungestörten Zugang zu unseren Services. Insbesondere gewährleistet
        MS –
        Store 24 keinen jederzeit uneingeschränkten und ungestörten Zugang zu den Automaten.
    </li>
</ol>

<h2>§ 12 Datenschutz</h2>
    <h/>
    <ol>
        <li>
            Wir sind berechtigt, Daten des Käufers zum Zwecke der Begründung und Durchführung der rechtsgeschäftlichen
            Schuldverhältnisse zwischen uns und dem Käufer sowie in Einklang mit den geltenden Datenschutzbestimmungen
            zu erheben,
            verarbeiten und zu nutzen.
        </li>
    </ol>
    <h2>§ 13 Gerichtsstand, Rechtswahl</h2>
    <ol>
        <li>
            Erfüllungsort für Warenbestellungen ist dasjenige Werk/Lager, an dem wir die bestellte Ware an den
            Beförderer
            übergeben, sofern sich aus unserer Auftragsbestätigung nichts anderes ergibt.
        </li>

        <li>Beiderseitiger Gerichtsstand für alle Streitigkeiten aus den Beziehungen zwischen uns und dem Käufer ist
            Ulm, wenn
            der Käufer Kaufmann, eine juristische Person des öffentlichen Rechts oder ein öffentlich-rechtliches
            Sondervermögen ist
            oder im Inland keinen allgemeinen Gerichtsstand hat. Wir sind jedoch berechtigt, den Käufer auch an einem
            anderen
            gesetzlich zuständigen Gerichtsstand zu verklagen.
        </li>

        <li>Es gilt ausschließlich das Recht der Bundesrepublik Deutschland. Die Anwendung des Übereinkommens der
            Vereinten
            Nationen über Verträge über den internationalen Warenkauf (CISG) ist ausgeschlossen.
        </li>
    </ol>',]);
        $this->insert('{{%static_content_language}}', ['content_id' => '2', 'language' => 'de', 'content' => '
<h1>My Smart Store 24 Datenschutzbestimmungen</h1>
<h2>Einführung</h2>
<p>In diesen Datenschutzbestimmungen stellt My Smart Store 24 Automatisierte Shopsysteme („wir“, „unser“ oder „das
    Unternehmen“) seine Vorgehensweise bezüglich der von Benutzern erfassten Daten, die auf unsere Webseite unter
    https://my-smart-store24.de („Webseite“) zugreifen oder uns auf andere Weise personenbezogene Daten bereitstellen
    (gemeinsam: „Benutzer“), dar.</p>

<p>Zuständige Behörde im Sinne der Datenschutz-Grundverordnung (DSGVO): Der Landesbeauftragte für den Datenschutz
    Baden-Württemberg</p>
<h2>Benutzerrechte</h2>
<p>Sie haben folgende Rechte:</p>
<ol>
    <li>Eine Bestätigung, ob und inwieweit Ihre personenbezogenen Daten verwendet und verarbeitet werden, sowie den
        Zugriff auf die über Sie gespeicherten personenbezogenen Daten und zusätzliche Informationen anfordern
    </li>
    <li>Eine Kopie der personenbezogenen Daten, die Sie uns freiwillig bereitgestellt haben, in einem strukturierten,
        gängigen und maschinenlesbaren Format anfordern
    </li>
    <li>Eine Berichtigung der personenbezogenen Daten, die wir von Ihnen gespeichert haben, anfordern</li>
    <li>Das Löschen Ihrer personenbezogenen Daten beantragen</li>
    <li>Der Verarbeitung Ihrer personenbezogenen Daten durch uns widersprechen</li>
    <li>Die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten durch uns beantragen</li>
    <li>Eine Beschwerde bei einer Aufsichtsbehörde einreichen</li>
</ol>
<p>Beachten Sie jedoch, dass diese Rechte nicht uneingeschränkt gelten, sondern unseren eigenen rechtmäßigen Interessen
    sowie behördlichen Vorschriften unterliegen.</p>

<p>Wenn Sie eines der hier aufgeführten Rechte in Anspruch nehmen möchten oder weitere Informationen wünschen, wenden
    Sie sich an unseren Datenschutzbeauftragten unter:</p>
<p>Herrn Uli Mayer, privacy@my-smart-store24.de.</p>
<h2>Speicherung</h2>
<p>Wir speichern Ihre personenbezogenen Daten so lange, wie es für die Bereitstellung unserer Services, die Einhaltung
    rechtlicher Verpflichtungen sowie die Beilegung von Streitigkeiten und die Durchsetzung unserer Richtlinien
    erforderlich ist. Die Aufbewahrungsfristen richten sich nach der Art der erfassten Daten und dem Zweck, für den
    diese Daten erfasst wurden, wobei sowohl die fallspezifischen Gegebenheiten als auch die Notwendigkeit
    berücksichtigt werden, veraltete, nicht genutzte Informationen baldmöglichst zu löschen. Datensätze mit
    personenbezogenen Daten von Kunden, Dokumente über die Kontoeinrichtung, Mitteilungen und andere Daten speichern wir
    gemäß geltender Gesetze und Vorschriften.</p>

<p>Wir können jederzeit und in unserem alleinigen Ermessen unvollständige oder unrichtige Daten korrigieren,
    vervollständigen oder entfernen.</p>
<h2>Grundlage für die Datenerfassung</h2>
<p>Die Verarbeitung Ihrer personenbezogenen Daten (d. h. jegliche Daten, die mit vertretbaren Mitteln eine
    Identifizierung Ihrer Person zulassen; „personenbezogene Daten“) ist erforderlich, um unseren vertraglichen
    Verpflichtungen Ihnen gegenüber nachzukommen und damit wir Ihnen unsere Services bereitstellen, unser legitimes
    Interesse schützen sowie rechtlichen und finanziellen Regulierungsverpflichtungen, denen wir unterworfen sind,
    nachkommen können.</p>

<p>Durch die Nutzung dieser Webseite stimmen Sie der Erfassung, Speicherung, Verwendung, Offenlegung und sonstigen
    Nutzung Ihrer personenbezogenen Daten wie in diesen Datenschutzbestimmungen beschrieben zu.</p>

<p>Bitte lesen Sie sich die Datenschutzbestimmungen sorgfältig durch, bevor Sie Entscheidungen treffen.</p>
<h2>Welche Daten werden erfasst?</h2>
<p>Wir erfassen zwei Arten von Daten und Informationen von Benutzern.</p>

<p>Zur ersten Kategorie gehören nicht identifizierende und nicht identifizierbare Benutzerdaten, die durch die Nutzung
    der Webseite bereitgestellt oder erfasst werden („Nicht personenbezogene Daten”). Wir kennen die Identität des
    Benutzers nicht, von dem nicht personenbezogene Daten erfasst wurden. Zu den nicht personenbezogenen Daten, die
    erfasst werden können, gehören aggregierte Nutzungsdaten und technische Daten, die von Ihrem Gerät übermittelt
    werden, einschließlich bestimmter Informationen bezüglich Software und Hardware (z. B. auf dem Gerät verwendeter
    Browser und verwendetes Betriebssystem, Spracheinstellung, Zugriffszeit usw.). Anhand dieser Daten verbessern wir
    die Funktionalität unserer Webseite. Wir können auch Daten über Ihre Aktivität auf der Webseite erfassen (z. B.
    aufgerufene Seiten, Surfverhalten, Klicks, Aktionen usw.).</p>

<p>Zur zweiten Kategorie gehören personenbezogene Daten , also Daten, die eine Einzelperson identifizieren oder durch
    angemessene Maßnahmen identifizieren können. Zu solchen Daten gehören:</p>
<ul>
    <li>Gerätedaten: Wir erfassen personenbezogene Daten von Ihrem Gerät. Solche Daten umfassen Geolocation-Daten,
        IP-Adresse, eindeutige Kennungen (z. B. MAC-Adresse und UUID) sowie andere Daten, die sich aus Ihrer Aktivität
        auf der Webseite ergeben.
    </li>
    <li>Registrierungsdaten: Wenn Sie sich auf unserer Webseite registrieren, werden Sie um die Angabe bestimmter
        Informationen gebeten, z. B.: Vor- und Nachname, E-Mail-Adresse oder Anschrift bzw. andere Information.
    </li>
</ul>
<h2>Wie erhalten wir Daten über Sie?</h2>
<p>Wir beziehen Ihre personenbezogenen Daten aus verschiedenen Quellen:</p>
<ul>
    <li>Sie stellen uns solche Daten freiwillig bereit, zum Beispiel bei der Registrierung auf unserer Webseite.</li>
    <li>Wir erhalten solche Daten, wenn Sie unsere Webseite nutzen oder in Verbindung mit einem unserer Services darauf
        zugreifen.
    </li>
    <li>Wir erhalten solche Daten von anderen Anbietern, Services und aus öffentlichen Registern (zum Beispiel von
        Datentraffic-Analyseanbietern).
    </li>
</ul>
<h2>Wie werden die Daten genutzt? An wen werden die Daten weitergegeben?</h2>
<p>Wir geben Benutzerdaten nicht an Dritte weiter, außer wie in diesen Datenschutzbestimmungen beschrieben.</p>

<p>Wir verwenden Daten für folgende Zwecke:</p>
<ul>
    <li>Zur Kommunikation mit Ihnen (Senden von Hinweisen bezüglich unserer Services, Bereitstellen von technischen
        Informationen und ggf. Bearbeiten von Kundendienstanfragen)
    </li>
    <li>Zur Information über neue Updates und Services</li>
    <li>Zur Anzeigenschaltung im Rahmen der Nutzung unserer Webseite (weitere Informationen unter „Werbung“)</li>
    <li>Zur Vermarktung unserer Webseiten und Produkte (weitere Informationen unter „Marketing“)</li>
    <li>Zu statistischen und Analysezwecken, die der Verbesserung der Webseite dienen</li>
</ul>
<p>Neben den verschiedenen, oben aufgeführten Verwendungszwecken können wir personenbezogene Daten auch an unsere
    Tochtergesellschaften, verbundenen Unternehmen und Subunternehmer weitergeben.</p>

<p>Zusätzlich zu den in diesen Datenschutzbestimmungen aufgeführten Zwecken können wir personenbezogene Daten aus
    folgenden Gründen an unsere vertrauenswürdigen externen Anbieter weiterleiten, die ihren Sitz in unterschiedlichen
    Rechtsprechungen auf der ganzen Welt haben:</p>
<ul>
    <li>Hosten und Betreiben unserer Webseite
    <li>Bereitstellen unserer Services, einschließlich der personalisierten Anzeige unserer Webseite</li>
    <li>Speichern und Verarbeiten solcher Daten in unserem Namen</li>
    <li>Schalten von Anzeigen sowie die Möglichkeit, den Erfolg unserer Werbekampagnen zu beurteilen, Retargeting von
        Benutzern
    </li>
    <li>Bereitstellen von Marketingangeboten und Werbematerialien in Zusammenhang mit unserer Webseite und unseren
        Services
    </li>
    <li>Durchführen von Studien, technischen Diagnosen oder Analysen</li>
</ul>
<p>Wir können Daten auch offenlegen, wenn wir im guten Glauben sind, dies ist hilfreich oder angemessen, um: (i)
    geltenden Gesetzen, Vorschriften, Gerichtsverfahren oder behördlichen Anfragen zu entsprechen; (ii) unsere
    Richtlinien (einschließlich unserer Vereinbarung) durchzusetzen und ggf. diesbezügliche mögliche Verletzungen zu
    untersuchen; (iii) illegale Aktivitäten oder anderes Fehlverhalten, Betrugsverdacht oder Sicherheitsprobleme zu
    untersuchen, zu erkennen, zu verhindern oder Maßnahmen dagegen zu ergreifen; (iv) eigene Rechtsansprüche geltend zu
    machen oder durchzusetzen bzw. uns gegen die Ansprüche anderer zu verteidigen; (v) die Rechte, das Eigentum oder
    unsere Sicherheit, die Sicherheit unserer Benutzer, Ihre Sicherheit oder die Sicherheit von Dritten zu schützen;
    oder um (vi) mit Strafverfolgungsbehörden zusammenzuarbeiten und/oder geistiges Eigentum oder andere Rechtsansprüche
    zu schützen.</p>
<h2>Cookies</h2>
<p>Für die Bereitstellung entsprechender Services verwenden wir und unsere Partner Cookies. Dies gilt auch, wenn Sie
    unsere Webseite besuchen oder auf unsere Services zugreifen.</p>

<p>Bei einem „Cookie“ handelt es sich um ein kleines Datenpaket, das Ihrem Gerät beim Besuch einer Webseite von dieser
    Webseite zugeordnet wird. Cookies sind nützlich und können für unterschiedliche Zwecke eingesetzt werden. Dazu
    gehören z. B. die erleichterte Navigation zwischen verschiedenen Seiten, die automatische Aktivierung bestimmter
    Funktionen, das Speichern Ihrer Einstellungen sowie ein optimierter Zugriff auf unsere Services. Die Verwendung von
    Cookies ermöglicht uns außerdem, Ihnen relevante, auf Ihre Interessen abgestimmte Werbung einzublenden und
    statistische Informationen zu Ihrer Nutzung unserer Services zu sammeln.</p>

<p>Diese Webseite verwendet folgende Arten von Cookies:</p>
<ol type="a">
    <li>„Sitzungscookies“ , die für eine normale Systemnutzung sorgen. Sitzungscookies werden nur für begrenzte Zeit
        während einer Sitzung gespeichert und von Ihrem Gerät gelöscht, sobald Sie Ihren Browser schließen.
    </li>

    <li>„Permanente Cookies“, die nur von der Webseite gelesen und beim Schließen des Browserfensters nicht gelöscht,
        sondern für eine bestimmte Dauer auf Ihrem Computer gespeichert werden. Diese Art von Cookie ermöglicht uns, Sie
        bei Ihrem nächsten Besuch zu identifizieren und beispielsweise Ihre Einstellungen zu speichern.
    </li>

    <li>„Drittanbieter-Cookies“ , die von anderen Online-Diensten gesetzt werden, die mit eigenen Inhalten auf der von
        Ihnen besuchten Seite vertreten sind. Dies können z. B. externe Web-Analytics-Unternehmen sein, die den Zugriff
        auf unsere Webseite erfassen und analysieren.
    </li>
</ol>
<p>Cookies enthalten keine personenbezogenen Daten, die Sie identifizieren, doch die von uns gespeicherten
    personenbezogenen Daten werden möglicherweise von uns mit den in den Cookies enthaltenen Daten verknüpft. Sie können
    Cookies über die Geräteeinstellungen Ihres Gerät entfernen. Folgen Sie dabei den entsprechenden Anweisungen.
    Beachten Sie, dass die Deaktivierung von Cookies zur Einschränkung bestimmter Funktionen bei der Nutzung unserer
    Webseite führen kann.</p>

<p>Das von uns verwendete Tool basiert auf der Technologie von Snowplow Analytics . Zu den von uns erfassten Daten zur
    Nutzung unserer Webseite gehören beispielsweise, wie häufig Benutzer die Webseite besuchen oder welche Bereiche
    aufgerufen werden. Das von uns verwendete Tool erfasst keine personenbezogenen Daten und wird von unserem
    Webhosting-Anbieter und Service-Provider ausschließlich zur Verbesserung des eigenen Angebots verwendet.</p>
<h2>Verwendung von Skriptbibliotheken (Google Web Fonts)</h2>
<p>Damit unsere Inhalte in jedem Browser korrekt und grafisch ansprechend dargestellt werden, verwenden wir für diese
    Webseite Skript- und Schriftbibliotheken wie Google Web Fonts (https://www.google.com/webfonts). Google Web Fonts
    werden in den Cache Ihres Browsers übertragen, sodass sie nur einmal geladen werden müssen. Wenn Ihr Browser Google
    Web Fonts nicht unterstützt oder den Zugriff verweigert, werden die Inhalte in einer Standardschriftart
    dargestellt.</p>
<ul>
    <li>Beim Aufrufen von Skript- oder Schriftbibliotheken wir automatisch eine Verbindung zum Betreiber der Bibliothek
        hergestellt. Es besteht hierbei theoretisch die Möglichkeit für diesen Betreiber, Daten zu erfassen. Derzeit ist
        nicht bekannt, ob und zu welchem Zweck die Betreiber der entsprechenden Bibliotheken tatsächlich Daten erfassen.
    </li>

    <li>Hier finden Sie die Datenschutzbestimmungen des Betreibers der Google-Bibliothek:
        https://www.google.com/policies/privacy.
    </li>
</ul>
<h2>Erfassung von Daten durch Dritte</h2>
<p>In dieser Richtlinie werden nur die Nutzung und Offenlegung von Daten, die wir von Ihnen erfassen, behandelt. Wenn
    Sie Daten auf anderen Webseiten veröffentlichen oder Dritten im Internet offenlegen, gelten möglicherweise andere
    Bestimmungen. Lesen Sie sich daher die allgemeinen Geschäftsbedingungen und Datenschutzbestimmungen immer sorgfältig
    durch, wenn Sie Daten offenlegen.</p>

<p>Diese Datenschutzbestimmungen beziehen sich nicht auf Geschäftspraktiken von Unternehmen, die sich nicht in unserem
    Besitz befinden bzw. nicht unter unserer Kontrolle stehen, oder auf andere Personen als unsere Angestellten und
    Mitarbeiter, einschließlich Dritter, denen wir diese Daten wie in diesen Datenschutzbestimmungen beschrieben
    offenlegen.</p>
<h2>Wie schützen wir Ihre Daten?</h2>
<p>Wir setzen die Sicherheitsmaßnahmen auf der Webseite mit großer Sorgfalt um und schützen Ihre Daten. Wir verwenden in
    der Branche übliche Verfahren und Richtlinien, um den Schutz der erfassten und gespeicherten Daten sicherzustellen,
    und verhindern die unbefugte Verwendung solcher Daten. Wir verlangen außerdem von Dritten, dass sie sich gemäß
    diesen Datenschutzbestimmungen an ähnliche Sicherheitsanforderungen halten. Obwohl wir angemessene Schritte für den
    Schutz von Daten unternehmen, können wir nicht verantwortlich gemacht werden für Handlungen von jenen, die sich
    unbefugten Zugang zu unserer Webseite verschafft haben oder diese missbräuchlich verwenden, und wir geben keine
    ausdrückliche noch stillschweigende Gewähr, dass wir einen solchen Zugriff verhindern können.</p>
<h2>Übermittlung von Daten außerhalb des Europäischen Wirtschaftsraums</h2>
<p>Beachten Sie, dass einige Empfänger ihren Sitz möglicherweise nicht im Europäischen Wirtschaftsraum haben. Ist dies
    der Fall, werden wir Ihre Daten nur in von der Europäischen Kommission genehmigte Länder mit angemessenem
    Datenschutzniveau übermitteln oder durch eine rechtliche Vereinbarung ein angemessenes Datenschutzniveau
    sicherstellen.</p>
<h2>Werbung</h2>
<p>Wenn Sie auf unsere Webseite zugreifen, schalten wir möglicherweise Anzeigen mithilfe von Anzeigentechnologie
    externer Anbieter. Diese Technologie verwendet für die Anzeigenschaltung Ihre Nutzungsdaten der Services (z. B.
    durch die Platzierung von Drittanbieter-Cookies in Ihrem Webbrowser).</p>

<p>Sie können sich von zahlreichen Anzeigennetzwerken Dritter abmelden, auch von Netzwerken, die von Mitgliedern der
    Network Advertising Initiative („NAI“) und der Digital Advertising Alliance („DAA“) betrieben werden. Informationen
    über die Vorgehensweise von NAI- und DAA-Mitgliedern, über Ihre Optionen, die Sie bezüglich der Verwendung solcher
    Daten von diesen Unternehmen haben, und darüber, wie Sie sich aus Anzeigennetzwerken Dritter, die von NAI- und
    DAA-Mitgliedern betrieben werden, abmelden können, finden Sie auf der jeweiligen Webseite:
    http://optout.networkadvertising.org/#!/ und http://optout.aboutads.info/#!/.</p>

<h2>Marketing</h2>
<p>Wir können Ihre personenbezogenen Daten wie Ihren Namen, Ihre E-Mail-Adresse, Ihre Telefonnummer usw. selbst
    verwenden oder an einen externen Untervertragsnehmer weiterleiten, um Ihnen Werbematerialien bezüglich unserer
    Services bereitzustellen, die Sie möglicherweise interessieren.</p>

<p>Wir respektieren Ihr Recht auf Privatsphäre. Daher erhalten Sie in diesen Marketingmaterialien stets die Möglichkeit,
    sich von weiteren Zusendungen abzumelden. Wenn Sie sich abmelden, wird Ihre E-Mail-Adresse oder Telefonnummer aus
    unseren Marketing-Verteilerlisten entfernt.</p>

<p>Beachten Sie, dass wir Ihnen auch nach einer Abmeldung vom Erhalt unserer Marketing-E-Mails weiterhin E-Mails mit
    wichtigen Informationen senden, die keine Abmeldeoption enthalten. Dazu gehören Wartungsmeldungen oder
    administrative Benachrichtigungen.,</p>
<h2>Unternehmenstransaktion</h2>
<p>Wir können Daten im Fall einer Unternehmenstransaktion (z. B. beim Verkauf wesentlicher Unternehmensteile, bei einer
    Fusion, einer Konsolidierung oder einem Anlagenverkauf) weitergeben. Falls ein oben genannter Fall eintritt,
    übernimmt der Erwerber oder das entsprechende Unternehmen die in dieser Datenschutzerklärung dargelegten Rechte und
    Pflichten.</p>
<h2>Minderjährige</h2>
<p>Der Schutz der Daten von Kindern ist insbesondere im Online-Bereich sehr wichtig. Die Webseite ist nicht für Kinder
    konzipiert und richtet sich nicht an diese. Die Nutzung unserer Services durch Minderjährige ist nur mit der
    vorherigen Einwilligung oder Autorisierung von einem Elternteil oder Erziehungsberechtigten zulässig. Wir erfassen
    personenbezogene Daten von Minderjährigen nicht wissentlich. Wenn ein Elternteil oder Erziehungsberechtigter
    Kenntnis davon erlangt, dass sein oder ihr Kind uns personenbezogene Daten ohne deren Einwilligung bereitgestellt
    hat, kann er/sie sich unter uli.mayer@stuewer.de an uns wenden.</p>
<h2>Aktualisierungen oder Änderungen dieser Datenschutzbestimmungen</h2>
<p>Wir behalten uns das Recht vor, diese Datenschutzbestimmungen von Zeit zu Zeit zu ändern oder zu prüfen. Sie finden
    das Datum der aktuellen Version unter „Zuletzt geändert am“. Ihre fortgesetzte Nutzung der Plattform nach der
    Bekanntmachung solcher Änderungen auf unserer Webseite stellt Ihre Zustimmung zu solchen Änderungen an den
    Datenschutzbestimmungen dar und gilt als Ihr Einverständnis der Bindung an die geänderten Bestimmungen.</p>
<h2>So erreichen Sie uns</h2>
<p>Wenden Sie sich bei allgemeinen Fragen zur Webseite, zu den von uns über Sie erfassten Daten oder der Verwendung
    dieser Daten unter privacy@my-smart-store24.de an uns.</p>

<p>Zuletzt geändert am 16.10.2019</p>',]);
        $this->insert('{{%static_content_language}}', ['content_id' => '3', 'language' => 'de', 'content' => '<h2>Nutzungsbedingungen</h2>
<p>Meine E-Mail-Adresse wird nicht an andere Unternehmen weitergegeben. Diese Einwilligung zur Nutzung meiner E-Mail-Adresse für Werbezwecke kann ich jederzeit mit Wirkung für die Zukunft widerrufen, indem ich den Link "Abmelden" am Ende des Newsletters anklicke.</p>',]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%static_content}}');
        $this->dropTable('{{%static_content_language}}');
    }
}
