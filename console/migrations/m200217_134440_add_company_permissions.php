<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m200217_134440_add_company_permissions
 */
class m200217_134440_add_company_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('company.create');
        $create->description = 'Add company';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('company.update');
        $update->description = 'Update company data';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('company.delete');
        $delete->description = 'Deleted company data';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('company.view');
        $view->description = 'View company data';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('company.listview');
        $listview->description = 'View company list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        $this->dropColumn('{{%company}}', 'user_id');
        $this->addColumn('{{%user}}', 'company_id', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%user}}', 'company_role', $this->string(50)->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('company.create');
        $update = $auth->getPermission('company.update');
        $delete = $auth->getPermission('company.delete');
        $view = $auth->getPermission('company.view');
        $listview = $auth->getPermission('company.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);

        $this->addColumn('{{%company}}', 'user_id', $this->integer()->null()->defaultValue(null));
        $this->dropColumn('{{%user}}', 'company_id');
        $this->dropColumn('{{%user}}', 'company_role');
    }
}
