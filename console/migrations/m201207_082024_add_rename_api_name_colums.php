<?php

use yii\db\Migration;

/**
 * Class m201207_082024_add_rename_api_name_colums
 */
class m201207_082024_add_rename_api_name_colums extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->renameColumn('{{%api_name}}', 'replenish', 'sell');
		$this->addColumn('{{%api_name}}', 'buy', $this->string()->null()->defaultValue(null));
		$this->createTable('{{%api_name_currency}}', [
			'id' => $this->bigPrimaryKey(),
			'currency'=>$this->integer()->null()->defaultValue(null),
			'api_id'=>$this->integer()->null()->defaultValue(null),
			'active'=>$this->integer()->null()->defaultValue(null),
			'created_at'=>$this->dateTime()->null()->defaultValue(null),
			'updated_at'=>$this->dateTime()->null()->defaultValue(null),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->renameColumn('{{%api_name}}', 'sell', 'replenish');
		$this->dropColumn('{{%api_name}}', 'buy');
		$this->dropTable('api_name_currency');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201207_082024_add_rename_api_name_colums cannot be reverted.\n";

        return false;
    }
    */
}
