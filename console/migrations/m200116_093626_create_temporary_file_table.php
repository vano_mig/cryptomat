<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%temporary_file}}`.
 */
class m200116_093626_create_temporary_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%temporary_file}}', [
            'id' => $this->primaryKey(),
            'file'=>$this->text()->null()->defaultValue(null),
            'date'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%temporary_file}}');
    }
}
