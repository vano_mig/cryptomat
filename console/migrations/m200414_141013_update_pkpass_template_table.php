<?php

use yii\db\Migration;

/**
 * Class m200414_141013_update_pkpass_template_table
 */
class m200414_141013_update_pkpass_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%pkpass_template}}', 'suppressStripShine');
        $this->renameColumn('{{%pkpass_template}}', 'backgroundColor','background_color');
        $this->renameColumn('{{%pkpass_template}}', 'foregroundColor','foreground_color');
        $this->renameColumn('{{%pkpass_template}}', 'labelColor','label_color');
        $this->renameColumn('{{%pkpass_template}}', 'logoText','logo_text');
        $this->renameColumn('{{%pkpass_template}}', 'auxiliaryFields','auxiliary_fields');
        $this->renameColumn('{{%pkpass_template}}', 'backFields','back_fields');
        $this->renameColumn('{{%pkpass_template}}', 'headerFields','header_fields');
        $this->renameColumn('{{%pkpass_template}}', 'primaryFields','primary_fields');
        $this->renameColumn('{{%pkpass_template}}', 'secondaryFields','secondary_fields');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%pkpass_template}}', 'suppressStripShine', $this->float()->defaultValue(null));
        $this->renameColumn('{{%pkpass_template}}', 'background_color','backgroundColor');
        $this->renameColumn('{{%pkpass_template}}', 'foreground_color','foregroundColor');
        $this->renameColumn('{{%pkpass_template}}', 'label_color','labelColor');
        $this->renameColumn('{{%pkpass_template}}', 'logo_text','logoText');
        $this->renameColumn('{{%pkpass_template}}', 'auxiliary_fields','auxiliaryFields');
        $this->renameColumn('{{%pkpass_template}}', 'back_fields','backFields');
        $this->renameColumn('{{%pkpass_template}}', 'header_fields','headerFields');
        $this->renameColumn('{{%pkpass_template}}', 'primary_fields','primaryFields');
        $this->renameColumn('{{%pkpass_template}}', 'secondary_fields','secondaryFields');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_141013_update_pkpass_template_table cannot be reverted.\n";

        return false;
    }
    */
}
