<?php

use yii\db\Migration;

/**
 * Class m201125_140727_add_column_api_name
 */
class m201125_140727_add_column_api_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%api_name}}', 'replenish', $this->integer()->null()->defaultValue(0));
		$this->addColumn('{{%api_name}}', 'exchange', $this->integer()->null()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%api_name}}', 'replenish');
		$this->dropColumn('{{%api_name}}', 'exchange');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_140727_add_column_api_name cannot be reverted.\n";

        return false;
    }
    */
}
