<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m191108_132022_create_permissions_payment_card
 */
class m191108_132022_create_permissions_payment_card extends Migration
{
    /**
     * @return bool|void
     * @throws mixed
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_USER);

        $create = $auth->createPermission('card.create');
        $create->description = 'Добавление платежной карты';
        $auth->add($create);
        $auth->addChild($user, $create);

//        $update = $auth->createPermission('card.update');
//        $update->description = 'Редактирование платежной карты';
//        $auth->add($update);
//        $auth->addChild($user, $update);

        $delete = $auth->createPermission('card.delete');
        $delete->description = 'Удаление платежной карты';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('card.view');
        $view->description = 'Просмотр платежной карты';
        $auth->add($view);
        $auth->addChild($user, $view);

//        $listview = $auth->createPermission('card.listview');
//        $listview->description = 'Просмотр списка платежных карт';
//        $auth->add($listview);
//        $auth->addChild($user, $listview);
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_USER);
        $create = $auth->getPermission('card.create');
//        $update = $auth->getPermission('card.update');
        $delete = $auth->getPermission('card.delete');
        $view = $auth->getPermission('card.view');
//        $listview = $auth->getPermission('card.listview');
        $auth->removeChild($user, $create);
//        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
//        $auth->removeChild($user, $listview);
        $auth->remove($create);
//        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
//        $auth->remove($listview);
    }
}
