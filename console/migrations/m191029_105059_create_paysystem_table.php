<?php

use common\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%paysystem}}`.
 */
class m191029_105059_create_paysystem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%paysystem}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'vpn_address'=>$this->string()->null()->defaultValue(null),
            'domain'=>$this->string()->null()->defaultValue(null),
            'host'=>$this->string()->null()->defaultValue(null),
            'port'=>$this->integer()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        $orderCreate = $auth->createPermission('paysystem.create');
        $orderCreate->description = 'Create paysystem';
        $auth->add($orderCreate);
        $auth->addChild($admin, $orderCreate);

        $orderCreate = $auth->createPermission('paysystem.delete');
        $orderCreate->description = 'Delete paysystem';
        $auth->add($orderCreate);
        $auth->addChild($admin, $orderCreate);

        $orderCreate = $auth->createPermission('paysystem.update');
        $orderCreate->description = 'Edit paysystem';
        $auth->add($orderCreate);
        $auth->addChild($admin, $orderCreate);

        $orderCreate = $auth->createPermission('paysystem.listview');
        $orderCreate->description = 'Paysystem list';
        $auth->add($orderCreate);
        $auth->addChild($admin, $orderCreate);

        $orderCreate = $auth->createPermission('paysystem.view');
        $orderCreate->description = 'View paysystem data';
        $auth->add($orderCreate);
        $auth->addChild($admin, $orderCreate);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%paysystem}}');
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('paysystem.create');
        $list = $auth->getPermission('paysystem.listview');
        $update = $auth->getPermission('paysystem.update');
        $delete = $auth->getPermission('paysystem.delete');
        $view = $auth->getPermission('paysystem.view');
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->removeChild($admin, $view);
        $auth->remove($list);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
    }
}
