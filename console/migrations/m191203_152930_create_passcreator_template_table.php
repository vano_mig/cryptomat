<?php

use yii\db\Migration;
use common\models\User;

/**
 * Handles the creation of table `{{%passcreator_template}}`.
 */
class m191203_152930_create_passcreator_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%passcreator_template}}', [
            'id' => $this->primaryKey(),
            'template_id' => $this->string()->null()->defaultValue(null),
            'template_name' => $this->string()->null()->defaultValue(null),
            'total_pass' => $this->string()->null()->defaultValue(null),
            'template_fields' => $this->text()->null()->defaultValue(null),
            'template_locations' => $this->text()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('pkpass_template.create');
        $create->description = 'Добавление pkpass шаблона';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('pkpass_template.update');
        $update->description = 'Редактирование pkpass шаблона';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('pkpass_template.delete');
        $delete->description = 'Удаление pkpass шаблона';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('pkpass_template.view');
        $view->description = 'Просмотр pkpass шаблона';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('pkpass_template.listview');
        $listview->description = 'Просмотр списка pkpass шаблонов';
        $auth->add($listview);
        $auth->addChild($user, $listview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%passcreator_template}}');

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('pkpass_template.create');
        $update = $auth->getPermission('pkpass_template.update');
        $delete = $auth->getPermission('pkpass_template.delete');
        $view = $auth->getPermission('pkpass_template.view');
        $listview = $auth->getPermission('pkpass_template.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }
}
