<?php

use yii\db\Migration;

/**
 * Class m201019_112017_add_culumn_company
 */
class m201019_112017_add_culumn_company extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->addColumn('{{%company}}', 'uid', $this->string()->null()->defaultValue(null));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('{{%company}}', 'uid');
	}


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201019_112017_add_culumn_company cannot be reverted.\n";

        return false;
    }
    */
}
