<?php

use backend\modules\virtual_terminal\models\VirtualTerminal;
use yii\db\Migration;

/**
 * Class m200807_120848_add_columns_to_v_t
 */
class m200807_120848_add_columns_to_v_t extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%virtual_terminal}}', 'mode', $this->string(20)->null()->defaultValue(null));
        $this->addColumn('{{%virtual_terminal}}', 'currency_id', $this->integer(20)->null()->defaultValue(null));
        VirtualTerminal::updateAll(['mode'=>VirtualTerminal::MODE_TEST]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%virtual_terminal}}', 'mode');
        $this->dropColumn('{{%virtual_terminal}}', 'currency_id');
    }
}
