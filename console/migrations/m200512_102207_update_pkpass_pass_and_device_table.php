<?php

use yii\db\Migration;

/**
 * Class m200512_102207_update_pkpass_pass_and_device_table
 */
class m200512_102207_update_pkpass_pass_and_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pkpass_device}}', 'update', $this->boolean()->defaultValue(null)->after('id'));
        $this->dropColumn('{{%pkpass_pass}}', 'update');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_device}}', 'update');
        $this->addColumn('{{%pkpass_pass}}', 'update', $this->boolean()->defaultValue(null)->after('template_id'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200512_102207_update_pkpass_pass_and_device_table cannot be reverted.\n";

        return false;
    }
    */
}
