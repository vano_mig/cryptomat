<?php

use yii\db\Migration;

/**
 * Class m200417_095658_update_pkpass_template_table
 */
class m200417_095658_update_pkpass_template_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%pkpass_template}}', 'status', $this->string()->defaultValue(null)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_template}}', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200417_095658_update_pkpass_template_table cannot be reverted.\n";

        return false;
    }
    */
}
