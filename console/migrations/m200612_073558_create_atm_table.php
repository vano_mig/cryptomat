<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%atm}}`.
 */
class m200612_073558_create_atm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%atm}}', [
            'id' => $this->primaryKey(),
            'uid'=>$this->string()->null()->defaultValue(null),
            'name'=>$this->string()->null()->defaultValue(null),
            'ip'=>$this->string()->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'blocked'=>$this->integer()->null()->defaultValue(null),
            'daily_limit'=>$this->integer()->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'country_id'=>$this->integer()->null()->defaultValue(null),
            'state'=>$this->string()->null()->defaultValue(null),
            'city'=>$this->string()->null()->defaultValue(null),
            'address'=>$this->string()->null()->defaultValue(null),
            'longitude'=>$this->decimal(10,6)->null()->defaultValue(null),
            'latitude'=>$this->decimal(10,6)->null()->defaultValue(null),
            'fee_min'=>$this->integer()->null()->defaultValue(null),
            'fee_max'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);

        $this->addColumn('{{%virtual_terminal}}','country_id', $this->integer()->null()->defaultValue(null));
        $this->addColumn('{{%virtual_terminal}}','state', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%virtual_terminal}}','city', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%virtual_terminal}}','address', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%atm}}');

        $this->dropColumn('{{%virtual_terminal}}', 'country_id');
        $this->dropColumn('{{%virtual_terminal}}', 'state');
        $this->dropColumn('{{%virtual_terminal}}', 'city');
        $this->dropColumn('{{%virtual_terminal}}', 'address');
    }
}
