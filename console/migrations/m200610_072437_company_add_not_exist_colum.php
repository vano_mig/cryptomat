<?php

use yii\db\Migration;

/**
 * Class m200610_072437_company_add_not_exist_colum
 */
class m200610_072437_company_add_not_exist_colum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #$this->addColumn('company', 'bank_name', 'string');
        #$this->addColumn('company', 'bic', 'string');
        #$this->addColumn('company', 'sixt_number', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200610_072437_company_add_not_exist_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_072437_company_add_not_exist_colum cannot be reverted.\n";

        return false;
    }
    */
}
