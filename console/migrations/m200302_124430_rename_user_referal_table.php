<?php

use yii\db\Migration;

/**
 * Class m200302_124430_rename_user_referal_table
 */
class m200302_124430_rename_user_referal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('{{%user_referal}}', 'company_referral');
        $this->renameColumn('{{%company_referral}}', 'user_id', 'company_id');
        $this->renameColumn('{{%company_referral}}', 'user_referal_id', 'company_referral_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameTable('{{%company_referral}}', 'user_referal');
        $this->renameColumn('{{%user_referal}}', 'company_id', 'user_id');
        $this->renameColumn('{{%user_referal}}', 'company_referral_id', 'user_referal_id');
    }
}
