<?php

use yii\db\Migration;

/**
 * Class m200610_082935_virtual_terminal_init
 */
class m200610_082935_virtual_terminal_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%virtual_terminal}}', [
            'id' => $this->primaryKey(),
            'uid'=>$this->string()->null()->defaultValue(null),
            'name'=>$this->string()->null()->defaultValue(null),
            'ip'=>$this->string()->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%virtual_terminal}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_082935_virtual_terminal_init cannot be reverted.\n";

        return false;
    }
    */
}
