<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m200110_115143_add_category_permissions
 */
class m200110_115143_add_category_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $listview = $auth->createPermission('category.listview');
        $listview->description = 'Categories list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        $view = $auth->createPermission('category.view');
        $view->description = 'View categories data';
        $auth->add($view);
        $auth->addChild($user, $view);

        $create = $auth->createPermission('category.create');
        $create->description = 'Create category';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('category.update');
        $update->description = 'Update category';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('category.delete');
        $delete->description = 'Delete category';
        $auth->add($delete);
        $auth->addChild($user, $delete);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('category.create');
        $update = $auth->getPermission('category.update');
        $delete = $auth->getPermission('category.delete');
        $view = $auth->getPermission('category.view');
        $listview = $auth->getPermission('category.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }
}
