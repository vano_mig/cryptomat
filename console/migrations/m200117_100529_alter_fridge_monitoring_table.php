<?php

use yii\db\Migration;

/**
 * Class m200117_100529_alter_fridge_monitoring_table
 */
class m200117_100529_alter_fridge_monitoring_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%fridge_monitoring}}', 'door');
        $this->addColumn('{{%fridge_monitoring}}', 'mode', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%fridge_monitoring}}', 'action', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%fridge_monitoring}}', 'critical_errors', $this->text()->null()->defaultValue(null));
        $this->addColumn('{{%fridge_monitoring}}', 'uncritical_errors', $this->text()->null()->defaultValue(null));
        $this->addColumn('{{%fridge_monitoring}}', 'status', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%fridge_monitoring}}', 'type', $this->tinyInteger()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%fridge_monitoring}}', 'door', $this->string()->null()->defaultValue(null));
        $this->dropColumn('{{%fridge_monitoring}}', 'mode');
        $this->dropColumn('{{%fridge_monitoring}}', 'action');
        $this->dropColumn('{{%fridge_monitoring}}', 'critical_errors');
        $this->dropColumn('{{%fridge_monitoring}}', 'uncritical_errors');
        $this->dropColumn('{{%fridge_monitoring}}', 'status');
        $this->dropColumn('{{%fridge_monitoring}}', 'type');
    }
}
