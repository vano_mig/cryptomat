<?php

use yii\db\Migration;

/**
 * Class m190925_081442_add_field_to_user_table
 */
class m190925_081442_add_field_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->string()->null()->defaultValue(null)->after('status'));
        $this->alterColumn('user', 'created_at', $this->dateTime()->null()->defaultValue(null));
        $this->alterColumn('user', 'updated_at', $this->dateTime()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'role');
        $this->alterColumn('user', 'created_at', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('user', 'updated_at', $this->integer()->null()->defaultValue(null));
    }
}
