<?php

use yii\db\Migration;
use backend\modules\user\models\User;

/**
 * Class m200618_065547_create_currency
 */
class m200618_065547_create_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crypto_currency}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'code'=>$this->string()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%identification_profile}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'currency_id'=>$this->integer()->null()->defaultValue(null),
            'buy_sell'=>$this->integer()->null()->defaultValue(null),
            'common_sum_scan'=>$this->integer()->null()->defaultValue(0),
            'common_sum_photo_id'=>$this->integer()->null()->defaultValue(0),
            'common_sum_email'=>$this->integer()->null()->defaultValue(0),
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'btc',
            'name' => 'Bitcoin',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'bch',
            'name' => 'Bitcoin Cash',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'dash',
            'name' => 'Dash',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'eth',
            'name' => 'Ethereum',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'ltc',
            'name' => 'Litecoin',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'neo',
            'name' => 'NEO',
        ]);

        $this->insert('{{%crypto_currency}}', [
            'code' => 'xrp',
            'name' => 'Ripple',
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        // v

        $create = $auth->createPermission('crypto_currency.create');
        $create->description = 'Add crypto_currency';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('crypto_currency.update');
        $update->description = 'Update crypto_currency';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('crypto_currency.delete');
        $delete->description = 'Delete crypto_currency';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('crypto_currency.view');
        $view->description = 'View crypto_currency';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('crypto_currency.list');
        $listview->description = 'View crypto_currency list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        // identification_profile

        $createIndent = $auth->createPermission('identification_profile.create');
        $createIndent->description = 'Add identification_profile';
        $auth->add($createIndent);
        $auth->addChild($user, $createIndent);

        $updateIndent = $auth->createPermission('identification_profile.update');
        $updateIndent->description = 'Update identification_profile';
        $auth->add($updateIndent);
        $auth->addChild($user, $updateIndent);

        $deleteIndent = $auth->createPermission('identification_profile.delete');
        $deleteIndent->description = 'Delete identification_profile';
        $auth->add($deleteIndent);
        $auth->addChild($user, $deleteIndent);

        $viewIndent = $auth->createPermission('identification_profile.view');
        $viewIndent->description = 'View identification_profile';
        $auth->add($viewIndent);
        $auth->addChild($user, $viewIndent);

        $listviewIndent = $auth->createPermission('identification_profile.list');
        $listviewIndent->description = 'View identification_profile list';
        $auth->add($listviewIndent);
        $auth->addChild($user, $listviewIndent);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crypto_currency}}');
        $this->dropTable('{{%identification_profile}}');

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('crypto_currency.create');
        $update = $auth->getPermission('crypto_currency.update');
        $delete = $auth->getPermission('crypto_currency.delete');
        $view = $auth->getPermission('crypto_currency.view');
        $list = $auth->getPermission('crypto_currency.list');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $list);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($list);

        $createIndent = $auth->getPermission('identification_profile.create');
        $updateIndent = $auth->getPermission('identification_profile.update');
        $deleteIndent = $auth->getPermission('identification_profile.delete');
        $viewIndent = $auth->getPermission('identification_profile.view');
        $listIndent = $auth->getPermission('identification_profile.list');
        $auth->removeChild($user, $createIndent);
        $auth->removeChild($user, $updateIndent);
        $auth->removeChild($user, $deleteIndent);
        $auth->removeChild($user, $viewIndent);
        $auth->removeChild($user, $listIndent);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($list);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200618_065547_create_currency cannot be reverted.\n";

        return false;
    }
    */
}
