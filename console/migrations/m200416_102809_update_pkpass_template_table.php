<?php

use yii\db\Migration;

/**
 * Class m200416_102809_update_pkpass_template_table
 */
class m200416_102809_update_pkpass_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pkpass_template}}', 'template_name', $this->string()->defaultValue(null)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pkpass_template}}', 'template_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_102809_update_pkpass_template_table cannot be reverted.\n";

        return false;
    }
    */
}
