<?php

use yii\db\Migration;
use common\models\User;

/**
 * Handles the creation of table `{{%passcreator_push_notification}}`.
 */
class m191203_121630_create_passcreator_push_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%passcreator_push_notification}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->null()->defaultValue(null),
            'date_sending' => $this->date()->null()->defaultValue(null),
            'status' => $this->string()->null()->defaultValue(null),
            'template_id' => $this->string()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $create = $auth->createPermission('pkpass_push.create');
        $create->description = 'Создание Push-уведомления';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('pkpass_push.update');
        $update->description = 'Редактирование Push-уведомления';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('pkpass_push.delete');
        $delete->description = 'Удаление Push-уведомления';
        $auth->add($delete);
        $auth->addChild($user, $delete);

        $view = $auth->createPermission('pkpass_push.view');
        $view->description = 'Просмотр Push-уведомления';
        $auth->add($view);
        $auth->addChild($user, $view);

        $listview = $auth->createPermission('pkpass_push.listview');
        $listview->description = 'Просмотр списка Push-уведомлений';
        $auth->add($listview);
        $auth->addChild($user, $listview);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%passcreator_push_notification}}');

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('pkpass_push.create');
        $update = $auth->getPermission('pkpass_push.update');
        $delete = $auth->getPermission('pkpass_push.delete');
        $view = $auth->getPermission('pkpass_push.view');
        $listview = $auth->getPermission('pkpass_push.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }
}
