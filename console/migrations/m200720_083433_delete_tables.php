<?php

use yii\db\Migration;

/**
 * Class m200720_083433_delete_tables
 */
class m200720_083433_delete_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%fridge}}');
        $this->dropTable('{{%fridge_camera}}');
        $this->dropTable('{{%fridge_monitoring}}');
        $this->dropTable('{{%passcreator_template}}');
        $this->dropTable('{{%passcreator_push}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_083433_delete_tables cannot be reverted.\n";

        return false;
    }
}
