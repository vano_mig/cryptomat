<?php

use yii\db\Migration;

/**
 * Class m191211_214816_add_column_to_transaction
 */
class m191211_214816_add_column_to_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction}}', 'receipt', $this->text()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%transaction}}', 'receipt');
    }
}
