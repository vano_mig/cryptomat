<?php

use yii\db\Migration;

/**
 * Class m191202_084247_add_columns_to_fridge_monitoring
 */
class m191202_084247_add_columns_to_fridge_monitoring extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%fridge_monitoring}}', 'content');
        $this->addColumn('{{%fridge_monitoring}}', 'door', $this->integer()->null()->defaultValue(null)->after('fridge_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%fridge_monitoring}}', 'door');
        $this->addColumn('{{%fridge_monitoring}}', 'content', $this->text()->null()->defaultValue(null));
    }
}
