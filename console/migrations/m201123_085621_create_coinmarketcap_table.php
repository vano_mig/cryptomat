<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%coinmarketcap}}`.
 */
class m201123_085621_create_coinmarketcap_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%coinmarketcap}}', [
            'id' => $this->primaryKey(),
            'price' => $this->decimal(16, 8)->null()->defaultValue(null),
            'volume_24h' => $this->decimal(16, 8)->null()->defaultValue(null),
            'percent_change_1h' => $this->decimal(16, 8)->null()->defaultValue(null),
            'percent_change_24h' => $this->decimal(16, 8)->null()->defaultValue(null),
            'percent_change_7d' => $this->decimal(16, 8)->null()->defaultValue(null),
            'currency_id'=>$this->tinyInteger()->null()->defaultValue(null),
            'last_updated' => $this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%coinmarketcap}}');
    }
}
