<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%colum_fridge_name_status_in_fridge}}`.
 */
class m200108_104605_create_colum_fridge_name_status_in_fridge_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%fridge}}', 'fridge_name', $this->string()->null()->defaultValue(null)->after('unique_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%fridge}}', 'fridge_name');
    }
}
