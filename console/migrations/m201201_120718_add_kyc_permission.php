<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m201201_120718_add_kyc_permission
 */
class m201201_120718_add_kyc_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $customer = $auth->getRole(User::ROLE_CUSTOMER);
        $listview = $auth->createPermission('kyc.list');
        $listview->description = 'View KYC list';
        $auth->add($listview);
        $auth->addChild($admin, $listview);
        $auth->addChild($customer, $listview);
        $view = $auth->createPermission('kyc.update');
        $view->description = 'Update KYC data';
        $auth->add($view);
        $auth->addChild($admin, $view);
        $auth->addChild($customer, $view);
        $this->addColumn('{{%user}}', 'kyc', $this->tinyInteger()->null()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $customer = $auth->getRole(User::ROLE_CUSTOMER);
        $list = $auth->getPermission('kyc.list');
        $update = $auth->getPermission('kyc.update');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $update);
        $auth->removeChild($customer, $list);
        $auth->removeChild($customer, $update);
        $auth->remove($list);
        $auth->remove($update);
        $this->dropColumn('{{%user}}', 'kyc');
    }
}
