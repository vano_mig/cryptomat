<?php

use yii\db\Migration;

/**
 * Class m200512_151008_update_pkpass_table
 */
class m200512_151008_update_pkpass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%pkpass_pass}}', 'company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%pkpass_pass}}', 'company_id', $this->integer()->defaultValue(null));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200512_151008_update_pkpass_table cannot be reverted.\n";

        return false;
    }
    */
}
