<?php

use yii\db\Migration;

/**
 * Class m200903_090035_create_api_currency_wallet
 */
class m200903_090035_create_api_currency_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%api_name_crypto_currency}}', [
			'id' => $this->primaryKey(),
			'api_id' => $this->string()->null()->defaultValue(null),
			'crypto_currency' => $this->integer()->null()->defaultValue(null),
			'active' => $this->integer()->null()->defaultValue(null),
		]);
		$this->createTable('{{%api_name}}', [
			'id' => $this->primaryKey(),
			'api_name' => $this->string()->null()->defaultValue(null),
			'api_url' => $this->string()->null()->defaultValue(null),
			'active' => $this->integer()->null()->defaultValue(null),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('{{%api_name_crypto_currency}}');
       $this->dropTable('{{%api_name}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200903_090035_create_api_currency_wallet cannot be reverted.\n";

        return false;
    }
    */
}
