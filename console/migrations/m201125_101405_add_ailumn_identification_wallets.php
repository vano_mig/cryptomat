<?php

use yii\db\Migration;

/**
 * Class m201125_101405_add_ailumn_identification_wallets
 */
class m201125_101405_add_ailumn_identification_wallets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%identification_wallets}}', 'currency_id', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('{{%identification_wallets}}', 'currency_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201125_101405_add_ailumn_identification_wallets cannot be reverted.\n";

        return false;
    }
    */
}
