<?php

use yii\db\Migration;

/**
 * Class m200519_082530_add_column_reg_number_to_company
 */
class m200519_082530_add_column_reg_number_to_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'reg_number', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company}}', 'bank_name', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company}}', 'bic', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company}}', 'iban', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%company}}', 'sixt_number', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%company}}', 'reg_number');
       $this->dropColumn('{{%company}}', 'bank_name');
       $this->dropColumn('{{%company}}', 'bic');
       $this->dropColumn('{{%company}}', 'iban');
       $this->dropColumn('{{%company}}', 'sixt_number');
    }
}
