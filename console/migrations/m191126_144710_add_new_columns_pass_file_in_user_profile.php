<?php

use yii\db\Migration;

/**
 * Class m191126_144710_add_new_columns_pass_file_in_user_profile
 */
class m191126_144710_add_new_columns_pass_file_in_user_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'ios_pass', $this->text()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'android_pass', $this->text()->null()->defaultValue(null));
        $this->alterColumn('{{%user_profile}}', 'pkpass_qr_url', $this->text()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'ios_pass');
        $this->dropColumn('{{%user_profile}}', 'android_pass');
        $this->alterColumn('{{%user_profile}}', 'pkpass_qr_url', $this->string()->null()->defaultValue(null));
    }
}
