<?php

use yii\db\Migration;

/**
 * Class m200907_120711_create_replenish_wallet
 */
class m200907_120711_create_replenish_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%replenish_wallet}}', [
			'id' => $this->primaryKey(),
			'provider_name' => $this->string()->null()->defaultValue(null),
			'provider_id' => $this->integer()->null()->defaultValue(null),
			'api_key' => $this->string()->null()->defaultValue(null),
			'api_secret' => $this->string()->null()->defaultValue(null),
			'block' => $this->integer()->null()->defaultValue(null),
			'user_id' => $this->integer()->null()->defaultValue(null),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%replenish_wallet}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_120711_create_replenish_wallet cannot be reverted.\n";

        return false;
    }
    */
}
