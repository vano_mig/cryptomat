<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m190930_092035_add_currency_table
 */
class m190930_092035_add_currency_table extends Migration
{
    public $tableName = '{{%currency}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->null()->defaultValue(null),
            'code' => $this->string(255)->null()->defaultValue(null),
            'system_currency' => $this->string(255)->null()->defaultValue(null),
            'status' => $this->string(255)->null()->defaultValue(null),
        ]);
        foreach ($this->currencies() as $currency) {
            $this->insert($this->tableName, [
                'name'=>$currency['name'],
                'code'=>$currency['code'],
                'system_currency'=>$currency['system_currency'],
                'status'=>$currency['status'],
            ]);
        }

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        //доступ к Валютам
        $currencyCreate = $auth->createPermission('currency.create');
        $currencyCreate->description = 'Создание валюты';
        $auth->add($currencyCreate);
        $auth->addChild($admin, $currencyCreate);

        $currencyUpdate = $auth->createPermission('currency.update');
        $currencyUpdate->description = 'Редактирование валюты';
        $auth->add($currencyUpdate);
        $auth->addChild($admin, $currencyUpdate);

        $currencyDelete = $auth->createPermission('currency.delete');
        $currencyDelete->description = 'Удаление валюты';
        $auth->add($currencyDelete);
        $auth->addChild($admin, $currencyDelete);

        $currencyView = $auth->createPermission('currency.view');
        $currencyView->description = 'Просмотр валюты';
        $auth->add($currencyView);
        $auth->addChild($admin, $currencyView);

        $currencyListview = $auth->createPermission('currency.listview');
        $currencyListview->description = 'Просмотр списка валют';
        $auth->add($currencyListview);
        $auth->addChild($admin, $currencyListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('currency.listview');
        $view = $auth->getPermission('currency.view');
        $create = $auth->getPermission('currency.create');
        $update = $auth->getPermission('currency.update');
        $delete = $auth->getPermission('currency.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
    
    public function currencies()
    {
        return [
            ['code' => 'AFN','status' => 'inactive','name' => 'Afghani', 'system_currency'=>'inactive'],
            ['code' => 'DZD','status' => 'inactive','name' => 'Algerian Dinar', 'system_currency'=>'inactive'],
            ['code' => 'ARS','status' => 'inactive','name' => 'Argentine Peso', 'system_currency'=>'inactive'],
            ['code' => 'AMD','status' => 'inactive','name' => 'Armenian Dram', 'system_currency'=>'inactive'],
            ['code' => 'AWG','status' => 'inactive','name' => 'Aruban Guilder', 'system_currency'=>'inactive'],
            ['code' => 'AUD','status' => 'inactive','name' => 'Australian Dollar', 'system_currency'=>'inactive'],
            ['code' => 'AZN','status' => 'inactive','name' => 'Azerbaijanian Manat', 'system_currency'=>'inactive'],
            ['code' => 'BSD','status' => 'inactive','name' => 'Bahamian Dollar', 'system_currency'=>'inactive'],
            ['code' => 'BHD','status' => 'inactive','name' => 'Bahraini Dinar', 'system_currency'=>'inactive'],
            ['code' => 'THB','status' => 'inactive','name' => 'Baht', 'system_currency'=>'inactive'],
            ['code' => 'PAB','status' => 'inactive','name' => 'Balboa', 'system_currency'=>'inactive'],
            ['code' => 'BBD','status' => 'inactive','name' => 'Barbados Dollar', 'system_currency'=>'inactive'],
            ['code' => 'BYN','status' => 'inactive','name' => 'Belarussian Ruble', 'system_currency'=>'inactive'],
            ['code' => 'BZD','status' => 'inactive','name' => 'Belize Dollar', 'system_currency'=>'inactive'],
            ['code' => 'BMD','status' => 'inactive','name' => 'Bermudian Dollar', 'system_currency'=>'inactive'],
            ['code' => 'VES','status' => 'inactive','name' => 'Bolivar Soberano', 'system_currency'=>'inactive'],
            ['code' => 'BOB','status' => 'inactive','name' => 'Boliviano', 'system_currency'=>'inactive'],
            ['code' => 'BRL','status' => 'inactive','name' => 'Brazilian Real', 'system_currency'=>'inactive'],
            ['code' => 'BND','status' => 'inactive','name' => 'Brunei Dollar', 'system_currency'=>'inactive'],
            ['code' => 'BGN','status' => 'inactive','name' => 'Bulgarian Lev', 'system_currency'=>'inactive'],
            ['code' => 'BIF','status' => 'inactive','name' => 'Burundi Franc', 'system_currency'=>'inactive'],
            ['code' => 'CAD','status' => 'inactive','name' => 'Canadian Dollar', 'system_currency'=>'inactive'],
            ['code' => 'CVE','status' => 'inactive','name' => 'Cape Verde Escudo', 'system_currency'=>'inactive'],
            ['code' => 'KYD','status' => 'inactive','name' => 'Cayman Islands Dollar', 'system_currency'=>'inactive'],
            ['code' => 'XOF','status' => 'inactive','name' => 'CFA Franc BCEAO', 'system_currency'=>'inactive'],
            ['code' => 'XAF','status' => 'inactive','name' => 'CFA Franc BEAC', 'system_currency'=>'inactive'],
            ['code' => 'XPF','status' => 'inactive','name' => 'CFP Franc', 'system_currency'=>'inactive'],
            ['code' => 'CLP','status' => 'inactive','name' => 'Chilean Peso', 'system_currency'=>'inactive'],
            ['code' => 'COP','status' => 'inactive','name' => 'Colombian Peso', 'system_currency'=>'inactive'],
            ['code' => 'KMF','status' => 'inactive','name' => 'Comoro Franc', 'system_currency'=>'inactive'],
            ['code' => 'CDF','status' => 'inactive','name' => 'Congolese Franc', 'system_currency'=>'inactive'],
            ['code' => 'BAM','status' => 'inactive','name' => 'Convertible Mark', 'system_currency'=>'inactive'],
            ['code' => 'NIO','status' => 'inactive','name' => 'Cordoba Oro', 'system_currency'=>'inactive'],
            ['code' => 'CRC','status' => 'inactive','name' => 'Costa Rican Colon', 'system_currency'=>'inactive'],
            ['code' => 'CUP','status' => 'inactive','name' => 'Cuban Peso', 'system_currency'=>'inactive'],
            ['code' => 'CZK','status' => 'inactive','name' => 'Czech Koruna', 'system_currency'=>'inactive'],
            ['code' => 'GMD','status' => 'inactive','name' => 'Dalasi', 'system_currency'=>'inactive'],
            ['code' => 'DKK','status' => 'inactive','name' => 'Danish Krone', 'system_currency'=>'inactive'],
            ['code' => 'MKD','status' => 'inactive','name' => 'Denar', 'system_currency'=>'inactive'],
            ['code' => 'DJF','status' => 'inactive','name' => 'Djibouti Franc', 'system_currency'=>'inactive'],
            ['code' => 'STN','status' => 'inactive','name' => 'Dobra', 'system_currency'=>'inactive'],
            ['code' => 'DOP','status' => 'inactive','name' => 'Dominican Peso', 'system_currency'=>'inactive'],
            ['code' => 'VND','status' => 'inactive','name' => 'Dong', 'system_currency'=>'inactive'],
            ['code' => 'XCD','status' => 'inactive','name' => 'East Caribbean Dollar', 'system_currency'=>'inactive'],
            ['code' => 'EGP','status' => 'inactive','name' => 'Egyptian Pound', 'system_currency'=>'inactive'],
            ['code' => 'SVC','status' => 'inactive','name' => 'El Salvador Colon', 'system_currency'=>'inactive'],
            ['code' => 'ETB','status' => 'inactive','name' => 'Ethiopian Birr', 'system_currency'=>'inactive'],
            ['code' => 'EUR','status' => 'active','name' => 'Euro', 'system_currency'=>'active'],
            ['code' => 'FKP','status' => 'inactive','name' => 'Falkland Islands Pound', 'system_currency'=>'inactive'],
            ['code' => 'FJD','status' => 'inactive','name' => 'Fiji Dollar', 'system_currency'=>'inactive'],
            ['code' => 'HUF','status' => 'inactive','name' => 'Forint', 'system_currency'=>'inactive'],
            ['code' => 'GHS','status' => 'inactive','name' => 'Ghana Cedi', 'system_currency'=>'inactive'],
            ['code' => 'GIP','status' => 'inactive','name' => 'Gibraltar Pound', 'system_currency'=>'inactive'],
            ['code' => 'HTG','status' => 'inactive','name' => 'Gourde', 'system_currency'=>'inactive'],
            ['code' => 'PYG','status' => 'inactive','name' => 'Guarani', 'system_currency'=>'inactive'],
            ['code' => 'GNF','status' => 'inactive','name' => 'Guinea Franc', 'system_currency'=>'inactive'],
            ['code' => 'GYD','status' => 'inactive','name' => 'Guyana Dollar', 'system_currency'=>'inactive'],
            ['code' => 'HKD','status' => 'inactive','name' => 'Hong Kong Dollar', 'system_currency'=>'inactive'],
            ['code' => 'UAN','status' => 'inactive','name' => 'Hryvnia', 'system_currency'=>'inactive'],
            ['code' => 'ISK','status' => 'inactive','name' => 'Iceland Krona', 'system_currency'=>'inactive'],
            ['code' => 'INR','status' => 'inactive','name' => 'Indian Rupee', 'system_currency'=>'inactive'],
            ['code' => 'IRR','status' => 'inactive','name' => 'Iranian Rial', 'system_currency'=>'inactive'],
            ['code' => 'IQD','status' => 'inactive','name' => 'Iraqi Dinar', 'system_currency'=>'inactive'],
            ['code' => 'JMD','status' => 'inactive','name' => 'Jamaican Dollar', 'system_currency'=>'inactive'],
            ['code' => 'JOD','status' => 'inactive','name' => 'Jordanian Dinar', 'system_currency'=>'inactive'],
            ['code' => 'KES','status' => 'inactive','name' => 'Kenyan Shilling', 'system_currency'=>'inactive'],
            ['code' => 'PGK','status' => 'inactive','name' => 'Kina', 'system_currency'=>'inactive'],
            ['code' => 'LAK','status' => 'inactive','name' => 'Kip', 'system_currency'=>'inactive'],
            ['code' => 'HRK','status' => 'inactive','name' => 'Kuna', 'system_currency'=>'inactive'],
            ['code' => 'KWD','status' => 'inactive','name' => 'Kuwaiti Dinar', 'system_currency'=>'inactive'],
            ['code' => 'MWK','status' => 'inactive','name' => 'Kwacha', 'system_currency'=>'inactive'],
            ['code' => 'AOA','status' => 'inactive','name' => 'Kwanza', 'system_currency'=>'inactive'],
            ['code' => 'MMK','status' => 'inactive','name' => 'Kyat', 'system_currency'=>'inactive'],
            ['code' => 'GEL','status' => 'inactive','name' => 'Lari', 'system_currency'=>'inactive'],
            ['code' => 'LBP','status' => 'inactive','name' => 'Lebanese Pound', 'system_currency'=>'inactive'],
            ['code' => 'ALL','status' => 'inactive','name' => 'Lek', 'system_currency'=>'inactive'],
            ['code' => 'HNL','status' => 'inactive','name' => 'Lempira', 'system_currency'=>'inactive'],
            ['code' => 'SLL','status' => 'inactive','name' => 'Leone', 'system_currency'=>'inactive'],
            ['code' => 'LRD','status' => 'inactive','name' => 'Liberian Dollar', 'system_currency'=>'inactive'],
            ['code' => 'LYD','status' => 'inactive','name' => 'Libyan Dinar', 'system_currency'=>'inactive'],
            ['code' => 'SZL','status' => 'inactive','name' => 'Lilangeni', 'system_currency'=>'inactive'],
            ['code' => 'LSL','status' => 'inactive','name' => 'Loti', 'system_currency'=>'inactive'],
            ['code' => 'MGA','status' => 'inactive','name' => 'Malagasy Ariary', 'system_currency'=>'inactive'],
            ['code' => 'MYR','status' => 'inactive','name' => 'Malaysian Ringgit', 'system_currency'=>'inactive'],
            ['code' => 'MUR','status' => 'inactive','name' => 'Mauritius Rupee', 'system_currency'=>'inactive'],
            ['code' => 'MXN','status' => 'inactive','name' => 'Mexican Peso', 'system_currency'=>'inactive'],
            ['code' => 'MDL','status' => 'inactive','name' => 'Moldovan Leu', 'system_currency'=>'inactive'],
            ['code' => 'MAD','status' => 'inactive','name' => 'Moroccan Dirham', 'system_currency'=>'inactive'],
            ['code' => 'MZN','status' => 'inactive','name' => 'Mozambique Metical', 'system_currency'=>'inactive'],
            ['code' => 'NGN','status' => 'inactive','name' => 'Naira', 'system_currency'=>'inactive'],
            ['code' => 'ERN','status' => 'inactive','name' => 'Nakfa', 'system_currency'=>'inactive'],
            ['code' => 'NAD','status' => 'inactive','name' => 'Namibia Dollar', 'system_currency'=>'inactive'],
            ['code' => 'NPR','status' => 'inactive','name' => 'Nepalese Rupee', 'system_currency'=>'inactive'],
            ['code' => 'ANG','status' => 'inactive','name' => 'Netherlands Antillean Guilder', 'system_currency'=>'inactive'],
            ['code' => 'TWD','status' => 'inactive','name' => 'New Taiwan Dollar', 'system_currency'=>'inactive'],
            ['code' => 'NZD','status' => 'inactive','name' => 'New Zealand Dollar', 'system_currency'=>'inactive'],
            ['code' => 'ILS','status' => 'inactive','name' => 'New Israeli Sheqel', 'system_currency'=>'inactive'],
            ['code' => 'BTN','status' => 'inactive','name' => 'Ngultrum', 'system_currency'=>'inactive'],
            ['code' => 'KPW','status' => 'inactive','name' => 'North Korean Won', 'system_currency'=>'inactive'],
            ['code' => 'NOK','status' => 'inactive','name' => 'Norwegian Krone', 'system_currency'=>'inactive'],
            ['code' => 'PEN','status' => 'inactive','name' => 'Nuevo Sol', 'system_currency'=>'inactive'],
            ['code' => 'MRU','status' => 'inactive','name' => 'Ouguiya', 'system_currency'=>'inactive'],
            ['code' => 'TOP','status' => 'inactive','name' => 'Pa’anga', 'system_currency'=>'inactive'],
            ['code' => 'PKR','status' => 'inactive','name' => 'Pakistan Rupee', 'system_currency'=>'inactive'],
            ['code' => 'MOP','status' => 'inactive','name' => 'Pataca', 'system_currency'=>'inactive'],
            ['code' => 'UYU','status' => 'inactive','name' => 'Peso Uruguayo', 'system_currency'=>'inactive'],
            ['code' => 'PHP','status' => 'inactive','name' => 'Philippine Peso', 'system_currency'=>'inactive'],
            ['code' => 'GBP','status' => 'inactive','name' => 'Pound Sterling', 'system_currency'=>'inactive'],
            ['code' => 'BWP','status' => 'inactive','name' => 'Pula', 'system_currency'=>'inactive'],
            ['code' => 'QAR','status' => 'inactive','name' => 'Qatari Rial', 'system_currency'=>'inactive'],
            ['code' => 'GTQ','status' => 'inactive','name' => 'Quetzal', 'system_currency'=>'inactive'],
            ['code' => 'ZAR','status' => 'inactive','name' => 'Rand', 'system_currency'=>'inactive'],
            ['code' => 'OMR','status' => 'inactive','name' => 'Rial Omani', 'system_currency'=>'inactive'],
            ['code' => 'KHR','status' => 'inactive','name' => 'Riel', 'system_currency'=>'inactive'],
            ['code' => 'RON','status' => 'inactive','name' => 'Romanian Leu', 'system_currency'=>'inactive'],
            ['code' => 'MVR','status' => 'inactive','name' => 'Rufiyaa', 'system_currency'=>'inactive'],
            ['code' => 'IDR','status' => 'inactive','name' => 'Rupiah', 'system_currency'=>'inactive'],
            ['code' => 'RUB','status' => 'inactive','name' => 'Russian Ruble', 'system_currency'=>'inactive'],
            ['code' => 'RWF','status' => 'inactive','name' => 'Rwanda Franc', 'system_currency'=>'inactive'],
            ['code' => 'SHP','status' => 'inactive','name' => 'Saint Helena Pound', 'system_currency'=>'inactive'],
            ['code' => 'SAR','status' => 'inactive','name' => 'Saudi Riyal', 'system_currency'=>'inactive'],
            ['code' => 'RSD','status' => 'inactive','name' => 'Serbian Dinar', 'system_currency'=>'inactive'],
            ['code' => 'SCR','status' => 'inactive','name' => 'Seychelles Rupee', 'system_currency'=>'inactive'],
            ['code' => 'SGD','status' => 'inactive','name' => 'Singapore Dollar', 'system_currency'=>'inactive'],
            ['code' => 'SBD','status' => 'inactive','name' => 'Solomon Islands Dollar', 'system_currency'=>'inactive'],
            ['code' => 'KGS','status' => 'inactive','name' => 'Som', 'system_currency'=>'inactive'],
            ['code' => 'SOS','status' => 'inactive','name' => 'Somali Shilling', 'system_currency'=>'inactive'],
            ['code' => 'TJS','status' => 'inactive','name' => 'Somoni', 'system_currency'=>'inactive'],
            ['code' => 'KRW','status' => 'inactive','name' => 'South Korean Won', 'system_currency'=>'inactive'],
            ['code' => 'SSP','status' => 'inactive','name' => 'South Sudanese Pound', 'system_currency'=>'inactive'],
            ['code' => 'LKR','status' => 'inactive','name' => 'Sri Lanka Rupee', 'system_currency'=>'inactive'],
            ['code' => 'SDG','status' => 'inactive','name' => 'Sudanese Pound', 'system_currency'=>'inactive'],
            ['code' => 'SRD','status' => 'inactive','name' => 'Surinam Dollar', 'system_currency'=>'inactive'],
            ['code' => 'SEK','status' => 'inactive','name' => 'Swedish Krona', 'system_currency'=>'inactive'],
            ['code' => 'CHF','status' => 'inactive','name' => 'Swiss Franc', 'system_currency'=>'inactive'],
            ['code' => 'SYP','status' => 'inactive','name' => 'Syrian Pound', 'system_currency'=>'inactive'],
            ['code' => 'BDT','status' => 'inactive','name' => 'Taka', 'system_currency'=>'inactive'],
            ['code' => 'WST','status' => 'inactive','name' => 'Tala', 'system_currency'=>'inactive'],
            ['code' => 'TZS','status' => 'inactive','name' => 'Tanzanian Shilling', 'system_currency'=>'inactive'],
            ['code' => 'KZT','status' => 'inactive','name' => 'Tenge', 'system_currency'=>'inactive'],
            ['code' => 'TTD','status' => 'inactive','name' => 'Trinidad and Tobago Dollar', 'system_currency'=>'inactive'],
            ['code' => 'MNT','status' => 'inactive','name' => 'Tugrik', 'system_currency'=>'inactive'],
            ['code' => 'TND','status' => 'inactive','name' => 'Tunisian Dinar', 'system_currency'=>'inactive'],
            ['code' => 'TRY','status' => 'inactive','name' => 'Turkish Lira', 'system_currency'=>'inactive'],
            ['code' => 'TMT','status' => 'inactive','name' => 'Turkmenistan New Manat', 'system_currency'=>'inactive'],
            ['code' => 'AED','status' => 'inactive','name' => 'UAE Dirham', 'system_currency'=>'inactive'],
            ['code' => 'UGX','status' => 'inactive','name' => 'Uganda Shilling', 'system_currency'=>'inactive'],
            ['code' => 'USD','status' => 'inactive','name' => 'US Dollar', 'system_currency'=>'inactive'],
            ['code' => 'UZS','status' => 'inactive','name' => 'Uzbekistan Sum', 'system_currency'=>'inactive'],
            ['code' => 'VUV','status' => 'inactive','name' => 'Vatu', 'system_currency'=>'inactive'],
            ['code' => 'YER','status' => 'inactive','name' => 'Yemeni Rial', 'system_currency'=>'inactive'],
            ['code' => 'JPY','status' => 'inactive','name' => 'Yen', 'system_currency'=>'inactive'],
            ['code' => 'CNY','status' => 'inactive','name' => 'Yuan Renminbi', 'system_currency'=>'inactive'],
            ['code' => 'ZMW','status' => 'inactive','name' => 'Zambian Kwacha', 'system_currency'=>'inactive'],
            ['code' => 'ZWL','status' => 'inactive','name' => 'Zimbabwe Dollar', 'system_currency'=>'inactive'],
            ['code' => 'PLN','status' => 'inactive','name' => 'Zloty', 'system_currency'=>'inactive'],
        ];
    }
}
