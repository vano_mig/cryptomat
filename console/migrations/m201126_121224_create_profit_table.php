<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profit}}`.
 */
class m201126_121224_create_profit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_profit}}', [
            'id' => $this->bigPrimaryKey(),
            'terminal_id'=>$this->integer()->null()->defaultValue(null),
            'widget_id'=>$this->integer()->null()->defaultValue(null),
            'currency'=>$this->string(20)->null()->defaultValue(null),
            'amount'=>$this->decimal(64,8)->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'transaction_id'=>$this->bigInteger()->null()->defaultValue(null),
            'mode'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'updated_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_profit}}');
    }
}
