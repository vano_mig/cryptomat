<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m201127_142955_edit_permissions
 */
class m201127_142955_edit_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $customer = $auth->getRole(User::ROLE_CUSTOMER);
        $listview = $auth->getPermission('caterer.listview');
        $view = $auth->getPermission('caterer.view');
        $viewDelivery = $auth->getPermission('delivery.view');
        $create = $auth->getPermission('caterer.create');
        $update = $auth->getPermission('caterer.update');
        $delete = $auth->getPermission('caterer.delete');
        if ($listview) {
            $auth->removeChild($admin, $listview);
            $auth->removeChild($admin, $view);
            $auth->removeChild($admin, $viewDelivery);
            $auth->removeChild($admin, $create);
            $auth->removeChild($admin, $update);
            $auth->removeChild($admin, $delete);
            $auth->removeChild($customer, $listview);
            $auth->removeChild($customer, $view);
            $auth->removeChild($customer, $viewDelivery);
            $auth->removeChild($customer, $create);
            $auth->removeChild($customer, $update);
            $auth->removeChild($customer, $delete);
            $auth->remove($listview);
            $auth->remove($view);
            $auth->remove($viewDelivery);
            $auth->remove($create);
            $auth->remove($update);
            $auth->remove($delete);
        }

        $listview = $auth->getPermission('paysystem.listview');
        $view = $auth->getPermission('paysystem.view');
        $create = $auth->getPermission('paysystem.create');
        $update = $auth->getPermission('paysystem.update');
        $delete = $auth->getPermission('paysystem.delete');
        $auth->removeChild($admin, $listview);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $viewDelivery);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->removeChild($customer, $listview);
        $auth->removeChild($customer, $view);
        $auth->removeChild($customer, $viewDelivery);
        $auth->removeChild($customer, $create);
        $auth->removeChild($customer, $update);
        $auth->removeChild($customer, $delete);
        $auth->remove($listview);
        $auth->remove($view);
        $auth->remove($viewDelivery);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);

        $listview = $auth->createPermission('api.listview');
        $listview->description = 'View api list';
        $auth->add($listview);
        $auth->addChild($admin, $listview);
        $view = $auth->createPermission('api.view');
        $view->description = 'View api data';
        $auth->add($view);
        $auth->addChild($admin, $view);
        $create = $auth->createPermission('api.create');
        $create->description = 'Create a new api';
        $auth->add($create);
        $auth->addChild($admin, $create);
        $update = $auth->createPermission('api.update');
        $update->description = 'Update api data';
        $auth->add($update);
        $auth->addChild($admin, $update);
        $delete = $auth->createPermission('api.delete');
        $delete->description = 'Delete api';
        $auth->add($delete);
        $auth->addChild($admin, $delete);

        $listview = $auth->createPermission('3rd_party_api.listview');
        $listview->description = 'View 3rd-api list';
        $auth->add($listview);
        $auth->addChild($admin, $listview);
        $auth->addChild($customer, $listview);
        $view = $auth->createPermission('3rd_party_api.view');
        $view->description = 'View 3rd-api data';
        $auth->add($view);
        $auth->addChild($admin, $view);
        $auth->addChild($customer, $view);
        $create = $auth->createPermission('3rd_party_api.create');
        $create->description = 'Create a new 3rd-api';
        $auth->add($create);
        $auth->addChild($admin, $create);
        $auth->addChild($customer, $create);
        $update = $auth->createPermission('3rd_party_api.update');
        $update->description = 'Update 3rd-api data';
        $auth->add($update);
        $auth->addChild($admin, $update);
        $auth->addChild($customer, $update);
        $delete = $auth->createPermission('3rd_party_api.delete');
        $delete->description = 'Delete 3rd-api';
        $auth->add($delete);
        $auth->addChild($admin, $delete);
        $auth->addChild($customer, $delete);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201127_142955_edit_permissions cannot be reverted.\n";

        return false;
    }
}
