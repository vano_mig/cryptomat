<?php

use yii\db\Migration;

/**
 * Class m200610_072027_company_reg_number
 */
class m200610_072027_company_reg_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        #$this->addColumn('company', 'reg_number', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200610_072027_company_reg_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_072027_company_reg_number cannot be reverted.\n";

        return false;
    }
    */
}
