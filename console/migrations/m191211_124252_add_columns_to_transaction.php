<?php

use yii\db\Migration;

/**
 * Class m191211_124252_add_columns_to_transaction
 */
class m191211_124252_add_columns_to_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%transaction}}', 'wallet');
        $this->dropColumn('{{%transaction}}', 'operation');
        $this->dropColumn('{{%transaction}}', 'status_transaction');
        $this->dropColumn('{{%transaction}}', 'status_system');
        $this->addColumn('{{%transaction}}', 'ref_card_no', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'amount', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'currency', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'res_file_ref', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'response_code', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'response_text', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'type', $this->string()->null()->defaultValue(null));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%transaction}}', 'ref_card_no');
        $this->dropColumn('{{%transaction}}', 'amount');
        $this->dropColumn('{{%transaction}}', 'currency');
        $this->dropColumn('{{%transaction}}', 'res_file_ref');
        $this->dropColumn('{{%transaction}}', 'response_code');
        $this->dropColumn('{{%transaction}}', 'response_text');
        $this->dropColumn('{{%transaction}}', 'type');
        $this->addColumn('{{%transaction}}', 'wallet', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'operation', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'status_transaction', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%transaction}}', 'status_system', $this->string()->null()->defaultValue(null));
    }
}
