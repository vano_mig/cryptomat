<?php

use yii\db\Migration;

/**
 * Class m191129_143032_create_user_profile_colum
 */
class m191129_143032_create_user_profile_colum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'reg_person', $this->string()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'reg_person');
    }
}
