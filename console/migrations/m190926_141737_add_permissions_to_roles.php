<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m190926_141737_add_permissions_to_roles
 */
class m190926_141737_add_permissions_to_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        //доступ к пользователям
        $userCreate = $auth->createPermission('permission.create');
        $userCreate->description = 'Создание роли';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('permission.update');
        $userUpdate->description = 'Редактирование роли';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('permission.delete');
        $userDelete->description = 'Удаление роли';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('permission.view');
        $userView->description = 'Просмотр роли';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('permission.listview');
        $userListview->description = 'Просмотр списка ролей';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('permission.listview');
        $view = $auth->getPermission('permission.view');
        $create = $auth->getPermission('permission.create');
        $update = $auth->getPermission('permission.update');
        $delete = $auth->getPermission('permission.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
