<?php

use yii\db\Migration;

/**
 * Class m210909_095517_kyc_users_table
 */
class m210909_095517_kyc_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{kyc_user}}', [
			'id' => $this->primaryKey(),
			'phone_number' => $this->string(),
			'verification' => $this->boolean(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{kyc_user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_095517_kyc_users_table cannot be reverted.\n";

        return false;
    }
    */
}
