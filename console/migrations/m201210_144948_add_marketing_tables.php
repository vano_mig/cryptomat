<?php

use yii\db\Migration;

/**
 * Class m201210_144948_add_marketing_tables
 */
class m201210_144948_add_marketing_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sms}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'send_to'=>$this->text()->null()->defaultValue(null),
            'adres_list'=> $this->string()->null()->defaultValue(null),
            'from'=>$this->string()->null()->defaultValue(null),
            'message'=>$this->text()->null()->defaultValue(null),
            'amount'=>$this->decimal(10,2)->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'error'=>$this->string()->null()->defaultValue(null),
            'schedule_date'=>$this->dateTime()->null()->defaultValue(null),
            'sms_id'=>$this->string()->null()->defaultValue(null),
            'created_at'=>$this->string()->null()->defaultValue(null),
        ]);

        $this->createTable('address_list', [
            'id' => $this->primaryKey(),
            'type'=>$this->string(255)->null()->defaultValue(null),
            'name'=>$this->string()->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'user_id'=> $this->string()->null()->defaultValue(null),
        ]);

        $this->createTable('advertisement', [
            'id' => $this->primaryKey(),
            'company_owner_id' => $this->integer()->null()->defaultValue(null),
            'company_id' => $this->integer()->null()->defaultValue(null),
            'currency_id' => $this->string()->null()->defaultValue(null),
            'price'=>$this->string(255)->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'start_date'=>$this->dateTime()->null()->defaultValue(null),
            'end_date'=>$this->dateTime()->null()->defaultValue(null),
            'queue'=> $this->integer()->null()->defaultValue(null),
            'name'=> $this->string()->null()->defaultValue(null),
            'length'=> $this->string()->null()->defaultValue(null),
            'repeat_per_hour'=> $this->integer()->null()->defaultValue(null),
            'video'=> $this->string()->null()->defaultValue(null),
            'hours'=> $this->text()->null()->defaultValue(null),
            'created_at'=> $this->dateTime()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%email}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'send_to'=>$this->string(40)->null()->defaultValue(null),
            'adres_list'=>$this->string()->null()->defaultValue(null),
            'from'=>$this->string()->null()->defaultValue(null),
            'subject'=>$this->string()->null()->defaultValue(null),
            'message'=>$this->text()->null()->defaultValue(null),
            'amount'=>$this->decimal(10,2)->null()->defaultValue(null),
            'status'=>$this->string()->null()->defaultValue(null),
            'error'=>$this->string()->null()->defaultValue(null),
            'file'=>$this->string()->null()->defaultValue(null),
            'created_at'=>$this->string()->null()->defaultValue(null),
        ]);

        $this->createTable('{{%email_template}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->null()->defaultValue(null),
            'description'=>$this->string()->null()->defaultValue(null),
            'template_id'=>$this->integer()->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sms}}');
        $this->dropTable('{{%address_list}}');
        $this->dropTable('{{%advertisement}}');
        $this->dropTable('{{%email}}');
        $this->dropTable('{{%email_template}}');
    }
}
