<?php

use yii\db\Migration;

/**
 * Class m191015_092058_add_new_table___client_product
 */
class m191015_092058_add_new_table___client_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client_products', [
            'id' => $this->primaryKey(),
            'client_id' => $this->string(32)->null()->defaultValue(null),
            'stores_id' => $this->string(32)->null()->defaultValue(null),
            'price' => $this->string(32)->null()->defaultValue(null),
            'price_vat' => $this->string(32)->null()->defaultValue(null),
            'quantity' => $this->string(32)->null()->defaultValue(null),
            'price_hundred' => $this->string(32)->null()->defaultValue(null),
            'caterer' => $this->string(200)->null()->defaultValue(null),
            'shelf_life' => $this->dateTime()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
        ]);

        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client_products');
    }
}
