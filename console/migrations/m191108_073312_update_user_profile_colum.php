<?php

use yii\db\Migration;

/**
 * Class m191108_073312_update_user_profile_colum
 */
class m191108_073312_update_user_profile_colum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user_profile}}', 'pkpass_card_url');
        $this->dropColumn('{{%user_profile}}', 'internal_marker');
        $this->addColumn('{{%user_profile}}', 'qr_key', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'pkpass_identifier', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'pkpass_qr_url', $this->string()->null()->defaultValue(null));
        $this->createIndex('qr_key','{{%user_profile}}', 'qr_key'); //  $unique = false так как есть проверка в модели frontend\models\RegistrationForm
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user_profile}}', 'pkpass_card_url', $this->string()->null()->defaultValue(null));
        $this->addColumn('{{%user_profile}}', 'internal_marker', $this->string()->null()->defaultValue(null));
        $this->dropColumn('{{%user_profile}}', 'pkpass_identifier');
        $this->dropColumn('{{%user_profile}}', 'pkpass_qr_url');
        $this->dropColumn('{{%user_profile}}', 'qr_key');
    }
}
