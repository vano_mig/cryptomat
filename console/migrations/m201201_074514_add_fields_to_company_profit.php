<?php

use yii\db\Migration;

/**
 * Class m201201_074514_add_fields_to_company_profit
 */
class m201201_074514_add_fields_to_company_profit extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company_profit}}', 'status', $this->integer()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%company_profit}}', 'status');
    }
}
