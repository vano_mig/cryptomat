<?php

use yii\db\Migration;
use common\models\User;

/**
 * Handles the creation of table `{{%pkpass_pass}}`.
 */
class m200319_114205_create_pkpass_pass_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pkpass_pass}}', [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer()->null()->defaultValue(null),
            'company_id' => $this->integer()->null()->defaultValue(null),
            'serial_number' => $this->string()->null()->defaultValue(null),
            'user_info' => $this->text()->null()->defaultValue(null),
            'relevant_date' => $this->dateTime()->null()->defaultValue(null),
            'expiration_date' => $this->dateTime()->null()->defaultValue(null),
            'voided' => $this->boolean()->null()->defaultValue(null),
            'beacons' => $this->text()->null()->defaultValue(null),
            'locations' => $this->text()->null()->defaultValue(null),
            'authenticationToken' => $this->text()->null()->defaultValue(null),
            'webServiceURL' => $this->text()->null()->defaultValue(null),
            'auxiliaryFields' => $this->text()->null()->defaultValue(null),
            'backFields' => $this->text()->null()->defaultValue(null),
            'headerFields' => $this->text()->null()->defaultValue(null),
            'primaryFields' => $this->text()->null()->defaultValue(null),
            'secondaryFields' => $this->text()->null()->defaultValue(null),
        ]);

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);

        $listview = $auth->createPermission('pkpass_pass.listview');
        $listview->description = 'PKPass passes list';
        $auth->add($listview);
        $auth->addChild($user, $listview);

        $view = $auth->createPermission('pkpass_pass.view');
        $view->description = 'View PKPass pass';
        $auth->add($view);
        $auth->addChild($user, $view);

        $create = $auth->createPermission('pkpass_pass.create');
        $create->description = 'Create PKPass pass';
        $auth->add($create);
        $auth->addChild($user, $create);

        $update = $auth->createPermission('pkpass_pass.update');
        $update->description = 'Update PKPass pass';
        $auth->add($update);
        $auth->addChild($user, $update);

        $delete = $auth->createPermission('pkpass_pass.delete');
        $delete->description = 'Delete PKPass pass';
        $auth->add($delete);
        $auth->addChild($user, $delete);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pkpass_pass}}');

        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $create = $auth->getPermission('pkpass_pass.create');
        $update = $auth->getPermission('pkpass_pass.update');
        $delete = $auth->getPermission('pkpass_pass.delete');
        $view = $auth->getPermission('pkpass_pass.view');
        $listview = $auth->getPermission('pkpass_pass.listview');
        $auth->removeChild($user, $create);
        $auth->removeChild($user, $update);
        $auth->removeChild($user, $delete);
        $auth->removeChild($user, $view);
        $auth->removeChild($user, $listview);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
        $auth->remove($view);
        $auth->remove($listview);
    }
}
