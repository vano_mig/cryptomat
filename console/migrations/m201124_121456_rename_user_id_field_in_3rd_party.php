<?php

use yii\db\Migration;

/**
 * Class m201124_121456_rename_user_id_field_in_3rd_party
 */
class m201124_121456_rename_user_id_field_in_3rd_party extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%3rd_party_api}}', 'user_id', 'company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%3rd_party_api}}', 'company_id', 'user_id');
    }
}
