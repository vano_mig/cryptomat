<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sms_info}}`.
 */
class m200702_123420_create_sms_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sms_info}}', [
            'id' => $this->primaryKey(),
            'from_number' => $this->string()->null()->defaultValue(null),
            'company_id' => $this->integer()->null()->defaultValue(null),
            'message' => $this->string()->null()->defaultValue(null),
            'user_number' => $this->string()->null()->defaultValue(null),
            'user_id' => $this->integer()->null()->defaultValue(null),
            'dataTime' => $this->dateTime()->null()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sms_info}}');
    }
}
