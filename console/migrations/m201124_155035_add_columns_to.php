<?php

use yii\db\Migration;

/**
 * Class m201124_155035_add_columns_to
 */
class m201124_155035_add_columns_to extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%crypto_currency}}', 'precision', $this->tinyInteger()->null()->defaultValue(null));
        $this->addColumn('{{%atm}}', 'currency_id', $this->tinyInteger()->null()->defaultValue(null));
        $this->addColumn('{{%atm}}', 'language', $this->string(10)->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%crypto_currency}}', 'precision');
        $this->dropColumn('{{%atm}}', 'currency_id');
        $this->dropColumn('{{%atm}}', 'language');
    }
}
