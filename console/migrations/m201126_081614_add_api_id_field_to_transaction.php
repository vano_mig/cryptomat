<?php

use yii\db\Migration;

/**
 * Class m201126_081614_add_api_id_field_to_transaction
 */
class m201126_081614_add_api_id_field_to_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction}}', 'api_id', $this->tinyInteger()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('{{%transaction}}', 'api_id');
    }
}