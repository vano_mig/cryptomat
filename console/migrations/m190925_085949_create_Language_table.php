<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Language}}`.
 */
class m190925_085949_create_Language_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%language}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(255)->null()->defaultValue(null),
            'iso_code'=>$this->string(255)->null()->defaultValue(null),
            'main'=>$this->string(255)->null()->defaultValue(null),
            'status'=>$this->string(255)->null()->defaultValue(null),
        ]);

        foreach($this->languages() as $lang) {
            $this->insert('{{%language}}', [
                'name' => $lang['name'],
                'iso_code' => $lang['iso_code'],
                'main'=>$lang['main'],
                'status' => $lang['status'],
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%language}}');
    }

    public function languages()
    {
        return [
            ['name'=>'Ukraine', 'iso_code'=>'ru', 'main'=>'inactive', 'status'=>'active'],
            ['name'=>'England', 'iso_code'=>'en', 'main'=>'active', 'status'=>'active'],
        ];
    }
}
