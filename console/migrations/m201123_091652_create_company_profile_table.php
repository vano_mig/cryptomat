<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_profile}}`.
 */
class m201123_091652_create_company_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_profile}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->tinyInteger()->null()->defaultValue(null),
            'platform_id'=>$this->tinyInteger()->null()->defaultValue(null),
            'public_key'=>$this->string()->null()->defaultValue(null),
            'private_key'=>$this->string()->null()->defaultValue(null),
            'token'=>$this->string()->null()->defaultValue(null),
            'twilio_sid'=>$this->string()->null()->defaultValue(null),
            'twilio_token'=>$this->string()->null()->defaultValue(null),
            'twilio_from_number'=>$this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_profile}}');
    }
}
