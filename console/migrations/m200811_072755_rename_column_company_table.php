<?php

use yii\db\Migration;

/**
 * Class m200811_072755_rename_column_company_table
 */
class m200811_072755_rename_column_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%company}}', 'sixt_number', 'contract_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%company}}', 'contract_number', 'sixt_number');
    }
}
