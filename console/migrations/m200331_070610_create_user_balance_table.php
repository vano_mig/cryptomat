<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_balance}}`.
 */
class m200331_070610_create_user_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%company_balance}}', 'type');
        $this->createTable('{{%user_balance}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'company_id'=>$this->integer()->null()->defaultValue(null),
            'cost'=>$this->decimal(10,2)->null()->defaultValue(null),
            'description'=>$this->text()->null()->defaultValue(null),
            'status'=>$this->string(50)->null()->defaultValue(null),
            'type'=>$this->string(50)->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null),
            'transaction_id'=>$this->string()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%company_balance}}', 'type', $this->string()->null()->defaultValue(null));
        $this->dropTable('{{%user_balance}}');
    }
}
