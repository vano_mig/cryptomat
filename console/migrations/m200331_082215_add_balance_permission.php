<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m200331_082215_add_balance_permission
 */
class m200331_082215_add_balance_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $operator = $auth->getRole(User::ROLE_CUSTOMER);

        $create = $auth->createPermission('company.balance');
        $create->description = 'Conpany balance';
        $auth->add($create);
        $auth->addChild($user, $create);
        $auth->addChild($operator, $create);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole(User::ROLE_ADMIN);
        $operator = $auth->getRole(User::ROLE_CUSTOMER);
        $create = $auth->getPermission('company.balance');
        $auth->removeChild($user, $create);
        $auth->removeChild($operator, $create);
        $auth->remove($create);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200331_082215_add_balance_permission cannot be reverted.\n";

        return false;
    }
    */
}
