<?php

namespace console\controllers;

use common\models\Support;
use yii\console\Controller;

/**
 * Class SupportController
 * @package console\controllers
 */
class SupportController extends Controller
{
    // close ticket after 2 days
    public function actionCloseTicket()
    {
        $date = date('Y-m-d H:i:s', strtotime('-2 days'));
        $models = Support::find()->where(['not in', 'status', [Support::STATUS_SOLVED, Support::STATUS_ACTIVE]])->andWhere(['<', 'updated_at', $date])->all();
        if(!empty($models)) {
            foreach ($models as $model) {
                $model->status = Support::STATUS_SOLVED;
                $model->save();
            }
        }
    }
}