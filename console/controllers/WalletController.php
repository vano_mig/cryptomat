<?php

namespace console\controllers;

use backend\modules\admin\formatter\WalletFormatter;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\admin\models\Transaction;
use backend\modules\api\models\Coinmarketcap;
use backend\modules\api\models\CoinPayments;
use backend\modules\user\models\CompanyProfit;
use backend\modules\virtual_terminal\models\Atm;
use common\models\Log;
use yii\console\Controller;
use Exception;

/**
 * Class WalletController
 * @package console\controllers
 */
class WalletController extends Controller
{
//Log::logAtm('error', $res['info']['error']);
	public function actionCheckReplenish(): bool
	{
		$data = ReplenishWallet::find()->where(['block' => ReplenishWallet::STATUS_ACTIVE])->all();

		if ($data) {
			foreach ($data as $dataKey)
			{
				$info = WalletFormatter::getBalance($dataKey);
				$dataKey->balance = $info;
				$dataKey->save();
			}
		}
		return true;
	}

	public function actionCheckExchange(): bool
	{
		$data = ExchangeWallet::find()->where(['block' => ExchangeWallet::STATUS_ACTIVE])->all();

		if ($data) {
			foreach ($data as $dataKey)
			{
				$info = WalletFormatter::getBalanceCash($dataKey);
				if ($info !== false)
				{
					$dataKey->balance = $info;
					$dataKey->save();
				}
			}
		}
		return true;
	}
}
