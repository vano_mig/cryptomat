<?php

namespace console\controllers;

use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\admin\models\Transaction;
use backend\modules\api\models\Coinmarketcap;
use backend\modules\api\models\CoinPayments;
use backend\modules\user\models\CompanyProfit;
use backend\modules\virtual_terminal\models\Atm;
use common\models\Log;
use yii\console\Controller;
use Exception;

/**
 * Class api test
 * @package console\controllers
 */
class AtmController extends Controller
{
//    private $api_key_coinmarketcap = '56152a21-7ac6-49d8-9176-e2738a632a8c';
    private $api_key_coinmarketcap = 'f4d8c43a-21a1-43f1-8e5b-863943548e05';
    // get exchange rate by coinmarketcap

    /**
     * Get exchange rate by coinmarketcap
     * @return mixed
     */
    public function actionGetExchangeRate()
    {
        $res['status'] = 'error';

        try {
            $currency = [];
            $crypto_currency = CryptoCurrency::find()->where(['active' => 1])->andWhere(['!=', 'code', 'JPT'])->asArray()->all();

            foreach ($crypto_currency as $key => $value) {
                $code = strtoupper($value['code']);
                $currency[$value['id']] = $code == 'USDT.ERC20' ? 'USDT' : $code;
            }

            $currencyImplode = implode(',', $currency);

            /*
             * for production in other package coinmarketcap
             *
            $currencyList = (new Currency())->getCurrencyList();
            $convertImplode = implode(',', $currencyList);

            $url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol={$currencyImplode}&convert={$convertImplode}";
             */
            $currencyList = (new Currency())->getCurrencyList();
            foreach ($currencyList as $idCurrency=>$item) {


                $url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol={$currencyImplode}&convert={$item}";
                $content_type = 'application/json';

                $headers = array(
                    'X-CMC_PRO_API_KEY: ' . $this->api_key_coinmarketcap,
                    'Content-Type: ' . $content_type
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $result = curl_exec($ch);

                curl_close($ch);

                $result = json_decode($result, true);

                foreach ($result['data'] as $key => $value) {
                    /**
                     *for production in other package coinmarketcap
                     *
                     * foreach ($value['quote'] as $data) {
                     * $cryptoCurrency_id = array_search($key, $currency);
                     * $result['price'] = round($data['price'], 8, 1);
                     * $res['info'] = $data;
                     * $id = null;
                     * foreach ($currencyList as $idCurrency=>$item) {
                     * if($item == $value['symbol']) {
                     * $id = $idCurrency;
                     * }
                     * }
                     * $model = new Coinmarketcap();
                     * $model->price = $data['price'];
                     * $model->currency_id = $cryptoCurrency_id;
                     * $model->percent_change_1h = $data['percent_change_1h'];
                     * $model->percent_change_24h = $data['percent_change_24h'];
                     * $model->percent_change_7d = $data['percent_change_7d'];
                     * $model->volume_24h = $data['volume_24h'];
                     * $model->last_updated = date('Y-m-d H:i:s', time());
                     * $model->currency_cash_id = $id;
                     * $model->save();
                     * }
                     */
                    $results = $value['quote'][$item];
                    $cryptoCurrency_id = array_search($key, $currency);
                    $result['price'] = round($results['price'], 8, 1);
                    $res['info'] = $results;
                    $model = new Coinmarketcap();
                    $model->price = $results['price'];
                    $model->currency_id = $cryptoCurrency_id;
                    $model->percent_change_1h = $results['percent_change_1h'];
                    $model->percent_change_24h = $results['percent_change_24h'];
                    $model->percent_change_7d = $results['percent_change_7d'];
                    $model->volume_24h = $results['volume_24h'];
                    $model->last_updated = date('Y-m-d H:i:s', time());
                    $model->currency_cash_id = $idCurrency;
                    $model->save();
                    $res['status'] = 'success';
                }
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            Log::logAtm('error', $res['info']['error']);
            //TODO: Нужно добавить запись лога в файл
        }

        return $res;
    }

    // get actual status transactions
    public function actionCheckTransactionsStatus()
    {
        $atm = Atm::findAll(['status' => Atm::UNBLOCKED]);
        if (empty($atm)) {
            return true;
        }

        foreach ($atm as $data) {

            $list = Transaction::find()->where(['terminal_id' => $data->id, 'status' => Transaction::STATUS_WAIT])->all();
            if (empty($list)) {
                continue;
            }
            foreach ($list as $transaction) {
                $wallet = ReplenishWallet::findOne(['id'=>$transaction->api_id, 'company_id'=>$data->company_id]);
                $provider = $wallet->provider;
                $name = $provider->apiName->api_name;
                $config = [
                    'public_key' => $wallet->api_key,
                    'secret_key' => $wallet->api_secret,
                    'client_id' => $wallet->client_id
                ];
                $path = "\\backend\modules\api\models\\" . $name;
                $model = new $path($config);
                if ($name == 'CoinPayments') {
                    if ($transaction->type == Transaction::TYPE_BUY_CRYPTO) {
                        $information = $model->checkWithdrawal(['txid' => $transaction->transaction_id]);
                        if ($information['status'] == 'success') {
                            if (array_key_exists('info', $information) && $information['info']['status'] == 2) {
                                $transaction->status = Transaction::STATUS_SEND_CRYPTO_SUCCESS;
                            } elseif (array_key_exists('info', $information) && $information['info']['status'] < 0) {
                                $transaction->status = Transaction::STATUS_ERROR;
                                Log::logAtm('txid->', ($transaction->transaction_id));
                                Log::logAtm('$withdrawal console->', $information);
                            } else {
                                if (strtotime($transaction->created_at . '+2 days') < strtotime(date('Y-m-d H:i:s'))) {
                                    $transaction->status = Transaction::STATUS_ERROR;
                                    Log::logAtm('txid->', ($transaction->transaction_id));
                                    Log::logAtm('$withdrawal console->', $information);
                                }
                            }
                            $transaction->save();
                        } else {
                            Log::logAtm('txid->', ($transaction->transaction_id));
                            Log::logAtm('$withdrawal console->', $information);
                        }
                    } elseif ($transaction->type == Transaction::TYPE_BUY_CASH) {
                        $information = $model->checkDeposit(['txid' => $transaction->transaction_id]);
                        if ($information['status'] == 'success') {
                            if ($information['info']['error'] == 'ok') {
                                if ($information['info']['result']['status'] == 100 || $information['info']['result']['status'] == 1) {
                                    $transaction->status = Transaction::STATUS_SUCCESS;
                                    $fee = CompanyProfit::findOne(['transaction_id'=>$transaction->id]);
                                    if($fee) {
                                        $fee->status = CompanyProfit::STATUS_SUCCESS;
                                        $fee->save();
                                    }
                                } elseif ($information['info']['result']['status'] == -1) {
                                    $transaction->status = Transaction::STATUS_CANCELED;
                                } else {
                                    if (strtotime($transaction->created_at . '+2 days') < strtotime(date('Y-m-d H:i:s'))) {
                                        $transaction->status = Transaction::STATUS_ERROR;
                                        Log::logAtm('txid->', ($transaction->transaction_id));
                                        Log::logAtm('$received console->', $information);
                                    }
                                }
                                $transaction->save();
                            } else {
                                Log::logAtm('txid->', ($transaction->transaction_id));
                                Log::logAtm('$received console->', $information);
                            }
                        } else {
                            Log::logAtm('txid->', ($transaction->transaction_id));
                            Log::logAtm('$received console->', $information);
                        }
                    }
                } elseif ($name == 'Bitstamp') {
                    if ($transaction->type == Transaction::TYPE_BUY_CRYPTO) {
                        $information = $model->checkWithdrawal(['txid' => $transaction->transaction_id]);
                        if ($information['status'] == 'error') {
                            Log::logAtm('txid->', ($transaction->transaction_id));
                            Log::logAtm('$withdrawal console->', $information);
                        } else {
                            Log::logAtm('txid->', ($transaction->transaction_id));
                            Log::logAtm('$withdrawal status console->', $information);
                        }
                        $transaction->save();
                    }
                }
            }

        }
    }
}