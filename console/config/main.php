<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/main-local.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['queue', 'log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'yii'=>[
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation'=>true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                '*'=>[
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation'=>true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=crypto',
            'username' => 'ivan',
            'password' => 'cnfhjcnf2007',
            'charset' => 'utf8mb4',

        ],
        'queue' => [
            'class' => yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => yii\mutex\MysqlMutex::class, // Mutex used to sync queries
            'ttr' => 5 * 60, // Max time for job execution
            'attempts' => 50, // Max number of attempts
        ],
    ],
    'params' => $params,
];
