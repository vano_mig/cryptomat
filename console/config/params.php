<?php
return [
    'supportEmail' => 'support@ms-store24.com',
    'senderEmail' => 'noreply@ms-store24.com',
    'registerEmail' => 'registrierung@ms-store24.com',
    'paymentEmail' => 'verwaltung@ms-store24.com',
    'patrickEmail' => 'p.doepfner@my-smart-store24.de',
    'ivanEmail' => 'vano.mig@gmail.com',
    'senderName' => 'CRYPTOMATIC',
];
