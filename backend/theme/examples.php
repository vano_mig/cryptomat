<!----------------------------------------------------------------------------------------------------------------------- Tabbed Block (middle tab is active) -->
<div class="card card-my-gray card-tabs max-wr-50 mx-auto elevation-1">
    <div class="card-header p-2 border-0 elevation-inset-2">
        <ul class="nav nav-tabs border-0" id="custom-tabs-one-tab" role="tablist">
            <li class="rounded nav-item elevation-2 mx-2 my-1">
                <a class="rounded nav-link"
                   id="custom-tabs-one-1-tab"
                   data-toggle="pill"
                   href="#custom-tabs-one-1"
                   role="tab"
                   aria-controls="custom-tabs-one-1"
                   aria-selected="false"
                >
                    Home 1
                </a>
            </li>
            <li class="rounded nav-item elevation-2 mx-2 my-1">
                <a class="rounded nav-link active"
                   id="custom-tabs-one-2-tab"
                   data-toggle="pill"
                   href="#custom-tabs-one-2"
                   role="tab"
                   aria-controls="custom-tabs-one-2"
                   aria-selected="true"
                >
                    Profile 2
                </a>
            </li>
            <li class="rounded nav-item elevation-2 mx-2 my-1">
                <a class="rounded nav-link"
                   id="custom-tabs-one-3-tab"
                   data-toggle="pill"
                   href="#custom-tabs-one-3"
                   role="tab"
                   aria-controls="custom-tabs-one-3"
                   aria-selected="false"
                >
                    Messages 3
                </a>
            </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
            <div class="tab-pane fade"
                 id="custom-tabs-one-1"
                 role="tabpanel"
                 aria-labelledby="custom-tabs-one-1-tab"
            >
                Lorem ipsum dolor sit amet. 1
            </div>
            <div class="tab-pane fade active show"
                 id="custom-tabs-one-2"
                 role="tabpanel"
                 aria-labelledby="custom-tabs-one-2-tab"
            >
                Mauris tincidunt mi. 2
            </div>
            <div class="tab-pane fade"
                 id="custom-tabs-one-3"
                 role="tabpanel"
                 aria-labelledby="custom-tabs-one-3-tab"
            >
                Morbi turpis dolor. 3
            </div>
        </div>
    </div>
</div>

<!---------------------------------------------------------------------------------------------------------------------->

<div class="form-group field-registrationform-email required">
    <label for="registrationform-email">Email</label>
    <input type="text" id="registrationform-email" class="form-control" name="RegistrationForm[email]"
           aria-required="true">

    <div class="invalid-feedback"></div>
</div>

<div class="form-group field-registrationform-email required validating">
    <label for="registrationform-email">Email</label>
    <input type="text" id="registrationform-email" class="form-control is-invalid" name="RegistrationForm[email]"
           aria-required="true" aria-invalid="true">

    <div class="invalid-feedback">Email darf nicht leer sein.</div>
</div>

<div class="form-group field-registrationform-email required validating">
    <label for="registrationform-email">Email</label>
    <input type="text" id="registrationform-email" class="form-control is-valid" name="RegistrationForm[email]"
           aria-required="true" aria-invalid="false">

    <div class="invalid-feedback"></div>
</div>


<div class="col">
    <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active"
               id="custom-content-above-home-tab"
               data-toggle="pill"
               href="#custom-content-above-home"
               role="tab"
               aria-controls="custom-content-above-home"
               aria-selected="true"
            >
                Home
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link"
               id="custom-content-above-profile-tab"
               data-toggle="pill"
               href="#custom-content-above-profile"
               role="tab"
               aria-controls="custom-content-above-profile"
               aria-selected="false"
            >
                Profile
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link"
               id="custom-content-above-messages-tab"
               data-toggle="pill"
               href="#custom-content-above-messages"
               role="tab"
               aria-controls="custom-content-above-messages"
               aria-selected="false"
            >
                Messages
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link"
               id="custom-content-above-settings-tab"
               data-toggle="pill"
               href="#custom-content-above-settings"
               role="tab"
               aria-controls="custom-content-above-settings"
               aria-selected="false"
            >
                Settings
            </a>
        </li>
    </ul>
    <div class="tab-content" id="custom-content-above-tabContent">
        <div class="tab-pane fade active show"
             id="custom-content-above-home"
             role="tabpanel"
             aria-labelledby="custom-content-above-home-tab"
        >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin malesuada lacus ullamcorper dui molestie, sit
            amet congue quam finibus. Etiam ultricies nunc non magna feugiat commodo. Etiam odio magna, mollis auctor
            felis vitae, ullamcorper ornare ligula. Proin pellentesque tincidunt nisi, vitae ullamcorper felis aliquam
            id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin id
            orci eu lectus blandit suscipit. Phasellus porta, ante et varius ornare, sem enim sollicitudin eros, at
            commodo leo est vitae lacus. Etiam ut porta sem. Proin porttitor porta nisl, id tempor risus rhoncus quis.
            In in quam a nibh cursus pulvinar non consequat neque. Mauris lacus elit, condimentum ac condimentum at,
            semper vitae lectus. Cras lacinia erat eget sapien porta consectetur.
        </div>
        <div class="tab-pane fade"
             id="custom-content-above-profile"
             role="tabpanel"
             aria-labelledby="custom-content-above-profile-tab"
        >
            Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor,
            et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum
            primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus
            interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit
            amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam.
        </div>
        <div class="tab-pane fade"
             id="custom-content-above-messages"
             role="tabpanel"
             aria-labelledby="custom-content-above-messages-tab"
        >
            Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi
            placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla.
            Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a,
            malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus
            efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae
            gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus
            tincidunt eleifend ac ornare magna.
        </div>
        <div class="tab-pane fade"
             id="custom-content-above-settings"
             role="tabpanel"
             aria-labelledby="custom-content-above-settings-tab"
        >
            Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare
            sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie
            tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra.
            Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim.
            In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis.
        </div>
    </div>
</div>


<div class="my-info-field">
    <div class="ml-2">
        <i class="fas fa-info-circle"></i>
        <small class="form-text text-muted">Barcode format.</small>
    </div>
</div>










