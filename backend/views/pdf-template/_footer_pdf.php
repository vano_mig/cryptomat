<?php

/* @var $company frontend\models\Company; */
/* @var $agent frontend\modules\user\models\User; */

?>
<?php if($company->id == 3): ?>
    <table class="footer-table border-collapse-collapse width-100" style="border:none !important;">
        <tr>
            <td colspan="3">
                <?php echo Yii::t('finance', 'Die Ware bleibt bis zur vollstandigen Bezahlung unser Eigentum. Wir liefern unter Zugrundelegung unserer AGB\'s.'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo Yii::t('finance','Company address'), ": ",
                    $company->street, ' ', $company->zip_code, ' ', $company->city; ?>
                <br>
                <?php echo Yii::t('finance', 'Geschaftsfuhrer'), ': ', $agent->username, ' ',
                    $agent->last_name; ?><br>
                <?php echo Yii::t('finance','Amtsgericht Ulm HRB'), " ",
                    $company->reg_number, ' * USt.-Id-Nr. ', $company->vat_number; ?>
            </td>
            <td>
                <?php echo Yii::t('finance', 'Phone number'), ": ", $agent->userProfile->phone_number; ?><br>
                <?php echo Yii::t('finance', 'E-Mail'), ": ", $agent->email; ?><br>
                <?php echo Yii::t('finance', 'Web-site: '.Yii::getAlias('@domain')); ?>
            </td>
            <td>
                <?php echo $company->bank_name; ?><br>
                <?php echo $company->iban; ?><br>
                <?php echo $company->bic; ?>
            </td>
        </tr>
    </table>
<?php else: ?>
    <table class="footer-table border-collapse-collapse width-100" style="border:none !important;">
        <tr>
            <td>
                <?php echo Yii::t('finance','Company address'), ": ",
                $company->street, ' ', $company->zip_code, ' ', $company->city; ?>
                <br>
                <?php echo Yii::t('finance', 'Order owner'), ': ', $agent->username, ' ',
                $agent->last_name; ?><br>
                <?php echo Yii::t('finance','Company reg number'), ": ",
                $company->reg_number, Yii::t('finance','Company VAT number'), ": ", $company->vat_number; ?>
            </td>
            <td>
                <?php echo Yii::t('finance', 'Phone number'), ": ", $agent->userProfile->phone_number; ?><br>
                <?php echo Yii::t('finance', 'Order owner e-mail'), ": ", $agent->email; ?><br>
<!--                --><?php //if($agent->site){
//                    echo Yii::t('finance', 'Site'), ": ", $agent->site;
//                } ?>
            </td>
            <td>
                <?php echo Yii::t('finance', 'Service bank'), ": ", $company->bank_name; ?><br>
                <?php echo Yii::t('finance', 'Company IBAN'), ": ", $company->iban; ?><br>
                <?php echo Yii::t('finance', 'Company BIC'), ": ", $company->bic; ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
{PAGENO} / {nb}