<?php

$this->title = Yii::t('site_title','CRYPTOMATIC API DOCUMENTATION');
?>

<div class="col-12">
    <h4><?php echo $this->title?></h4>
    <div class="row">
        <div class="col-12 font-weight-bold">INITIALIZATION</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/initialization</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "currency_cash": "USD",
                    "language": "en",
                    "email": "support@test.com",
                    "atm_number": 7,
                    "status": "success"
                    "fee": "3.5"
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">EXCHANGE RATE</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/get-exchange-rate</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'currency_code' => 'btc',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                    "info": {
                        "price": "64.57248421",
                        "volume_24h": 55047.86325632,
                        "percent_change_1h": 0.91600015,
                        "percent_change_24h": 1.80907822,
                        "percent_change_7d": 1.77611585,
                        "market_cap": 0,
                        "last_updated": "2020-10-09T13:19:43.000Z"
                    }
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">SELL CRYPTO</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/create-object-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: base64_encode(json_encode(params))</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'amount' => 10,
                    'phone_number' => '+380632826183',
                    'type' => 'CRYPTO',
                    'uid' => '5fc0bf4d4f463',
                    'currency_code' => 'ltc',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "amount_crypto": "10.52631579",
                    "currency_code": "LTC",
                    "timeout": 7200,
                    "wallet": "MCtbz6uiiCGRqavsFpHAue5f8KiLaiuGuv",
                    "qrcode_url": "https://office.cryptomatic-atm.org/scan-qr?id=CPEK5MZHGRIBBBOKINRQNBOBOZ",
                    "transaction_id": "CPEK5MZHGRIBBBOKINRQNBOBOZ",
                    "id_for_printer": 7,
                    "status": "success"
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CREATE BUY CRYPTO</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/create-crypto-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'amount' => 10,
                    'wallet' => '0xrguhdbn4w7ygwh84238522rhfwiuthw4',
                    'type' => 'CRYPTO',
                    'uid' => '5fc0bf4d4f463',
                    'currency_code' => 'ltc',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                    {
                        "status": "success",
                        "system_id": 48,
                        "amount": "100",
                        "amount_crypto": "1.54864725",
                        "wallet": "0x247af53b629cde4460e2703b56227a0c3d65d229",
                        "id_for_printer": 7,
                    }
<!--                {-->
<!--                    "amount_crypto": "10.52631579",-->
<!--                    "currency_code": "LTC",-->
<!--                    "timeout": 7200,-->
<!--                    "wallet": "MCtbz6uiiCGRqavsFpHAue5f8KiLaiuGuv",-->
<!--                    "qrcode_url": "https://office.crypto.my/scan-qr?id=CPEK5MZHGRIBBBOKINRQNBOBOZ",-->
<!--                    "transaction_id": "CPEK5MZHGRIBBBOKINRQNBOBOZ",-->
<!--                    "id_for_printer": 7,-->
<!--                    "status": "success"-->
<!--                }-->
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CONFIRM BUY CRYPTO</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/confirm-crypto-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'system_id' => 3,
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                    "amount": "100",
                    "amount_crypto": "1.54864725",
                    "transaction_id": "test",
                    "wallet": "0x247af53b629cde4460e2703b56227a0c3d65d229",
                    "id_for_printer": 7
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CANCEL BUY CRYPTO</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/cancel-crypto-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'system_id' => 3,
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">GET ERRORS</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/get-errors</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'error' => [
                        'error_description' => 'some description',
                        'code' => 2
                    ],
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CHECK PHONE NUMBER</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/check-phone-number</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: base64_encode(json_encode(params))</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'phone_number' => '+3806332862754',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">GET LIST CRYPTO CURRENCY</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/get-crypto-currency</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'type' => 'CASH',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                    "currencies": {
                        "btc": {
                            "name":"Bitcoin",
                            "code":"btc",
                            "rate":"21342.4452344",
                            "precision":"8",
                            "icon":"https://office.cryptomatic-atm.org/img/system/logo/btc.png",
                        },
                        "eth": {
                            "name":"Ethereum",
                            "code":"eth",
                            "rate":"21342.4452344",
                            "precision":"8",
                            "icon":"https://office.cryptomatic-atm.org/img/system/logo/eth.png",
                        },
                    }
                }</pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CHECK TRANSACTION STATUS</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/check-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'txid' => 'CIOHIAGEFIAEOIAHJORGAE',
                    'type' => 'CRYPTO',
                    'test' => 1,
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                    "info": {
                        "text": "Waiting for buyer funds...",
                        "amount_crypto": "1.59344725",
                        "received_crypto": "0.00000000",
                        "payment_wallet": "0x976ee6bbcf6fdd815b685c411d1fbd0465d79ea7",
                        "status": 0
                        "give_amount": 0.43632
                        "type": "CASH",
                        "id_for_printer": 7
                    }
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CHANGE STATUS TRANSACTION</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/change-status-transaction</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'txid' => 'CIOHIAGEFIAEOIAHJORGAE',
                    'status' => 1,
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
    <div class="row">
        <div class="col-12 font-weight-bold">CHECK KYC</div>
        <div class="col-12">URL: https://office.cryptomatic-atm.org/api/atm/check-kyc</div>
        <div class="col-4>">
            <div class="col-12">METHOD: POST</div>
            <div class="col-12">REQUEST: json_encode(params)</div>
            <div class="col-12">Params</div>
            <div class="col-12">
                <pre>
                request = [
                    'uid' => '5fc0bf4d4f463',
                    'qr_code' => 'eedxtheyd4edthf',
                ]
                </pre>
            </div>
            <div class="col-12">RESPONSE: </div>
            <div class="col-12">FORMAT : JSON</div>
            <div class="col-12">
                <pre>
                {
                    "status": "success",
                }
                </pre>
            </div>
        </div>
        <div class="col-8>"></div>
    </div>
</div>
