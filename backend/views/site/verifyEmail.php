<?php

/* @var $this yii\web\View
 * @var $model frontend\models\VerifyEmailForm
 */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('site', 'Verify Email');
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'token', ['options' => ['style' => 'padding: 0 0 1.5rem 0;']]) ?>

            <div class="row form-group">
                <?= Html::submitButton(Yii::t('email', 'Send'), ['class' => 'btn bg-gradient-primary mb-1 mx-auto']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>