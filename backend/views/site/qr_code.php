<?php

/**
 * @var $writer BaconQrCode\Writer;
 * @var $id integer
 */
?>
<style>
    .img-fluid svg {
        width: 100%;
    }
    .img-fluid {
        top: 50%;
        position: absolute;
        transform: translate(50%, -50%);
    }
</style>
<div class="img-fluid text-center col-11 col-sm-12 col-md-10 img-logo img-logo-cub">
    <?php echo $text; ?>
</div>
