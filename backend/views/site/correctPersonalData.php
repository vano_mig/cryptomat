<?php

/* @var $this yii\web\View */

/* @var $model frontend\models\CorrectPersonalData */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('site', 'Correct personal data');
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'last_name') ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'phone_number') ?>

            <div class="col-xs-12"
                 style="border-radius: 1rem; border: 1px solid lightgray; margin: 1rem 0; padding: 1rem 1rem;">

                <?= $form->field($model, 'country') ?>

                <?= $form->field($model, 'city') ?>

                <?= $form->field($model, 'street_house') ?>

                <?= $form->field($model, 'post_code') ?>

            </div>
            <div class="col-xs-12">
                <div style="display: inline-flex; align-items: center; width: 100%;">
                    <?= $form->field($model, 'mailing')->checkbox()->label(Yii::t('site', 'Receive promotions and news about by email')); ?>
                    <a target="_blank" style="margin-bottom: 15px; padding: 10px;"
                       class="fas fa-external-link-alt fa-lg"
                       href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/site/mailing/"></a>
                </div>

                <br>
                <div class="col-12"
                     style="border-top: 1px solid #bfbfbf; display: inline-flex; justify-content: center; padding: 0;">
                    <div class="row form-group" style="margin-top: 1rem">
                        <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn bg-gradient-primary mx-auto', 'name' => 'save']) ?>
                    </div>
                </div>
                <br>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

