<?php
/* @var $this yii\web\View */
$this->title = Yii::t('site', 'Add Card');
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body my-card">
            <div style="margin: 0 auto; width: 479px;" class="iframe">
                <iframe style="margin: auto" width="479px" height="570px" frameborder="0"
                        src="https://www.pmt-
token.eu/index.php?sid=&machine_type=BEWOTEC_V1&machine_id=103697768&responseUrl
=aHR0cHM6Ly9tcy1zdG9yZTI0LmNvbS9zaXRlL2NhcmQ=&sid=01&Identifier=001&language=<?php echo Yii::$app->language ?>"></iframe>
<!--                <iframe style="margin: auto" width="479px" height="570px" frameborder="0"-->
<!--                        src="https://www.test.pmt--->
<!--token.eu/index.php?sid=&machine_type=BEWOTEC_V1&machine_id=JPTECH_PMT_01&responseUrl-->
<!--=aHR0cHM6Ly9tcy1zdG9yZTI0LmNvbS9zaXRlL2NhcmQ=&sid=01&Identifier=001&language=--><?php //echo Yii::$app->language ?><!--"></iframe>-->
            </div>
        </div>
    </div>
</div>
