<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-backend',
    'name' => 'CRYPTOMATIC',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'aliases' => [
        '@admin_lte' => '@backend/theme/AdminLTE',
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => 'asdfgbkdjgbjgfdkbsdgbfvfzkbdvsdg',
            'class' => 'backend\components\LanguageRequest'
        ],
        'assetManager' => [
            'linkAssets' => true,
            'bundles' => [
                'yii\bootstrap4\BootstrapPluginAsset' => false,
                'yii\bootstrap4\BootstrapAsset' => false,
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'class' => backend\components\BackendFormatter::class,
            'defaultTimeZone' => 'Europe/Kiev',
            'timeZone' => 'Europe/Kiev',
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['backend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                '*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['backend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'backend\components\LanguageUrlManager',
            'rules' => [
                '<action>' => 'site/<action>',
                'GET,POST pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>/<pass:.+>' => '/pkpass/pkpass-web-service/registration',
                'GET,POST pkpass/pkpass-web-service/v1/devices/<device:[^\s]+>/registrations_attido/<type:[^\s]+>/<pass:[^\s]+>' => 'pkpass/pkpass-web-service/registration-attido',
                'GET pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>' => 'pkpass/pkpass-web-service/passes',
                'GET pkpass/pkpass-web-service/v1/passes/<type:.+>/<pass:.+>' => 'pkpass/pkpass-web-service/update',
                'DELETE pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>/<pass:.+>' => 'pkpass/pkpass-web-service/delete',
                'GET,POST pkpass/pkpass-web-service/v1/log' => 'pkpass/pkpass-web-service/log',
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => backend\modules\admin\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
//            'view' => [
//                'theme' => [
//                    'basePath' => '@backend/modules/admin/themes/lte',
//                    'baseUrl' => '@web/modules/admin/themes/lte',
////                    'pathMap' => [
////                        '@app/views' => '@backend/modules/admin/themes/lte/views',
////                        '@app/widgets' => '@backend/modules/admin/themes/lte/views/widgets',
////                    ],
//                ],
//            ],
        ],
        'user' => [
            'class' => backend\modules\user\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'pkpass' => [
            'class' => backend\modules\pkpass\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'terminal' => [
            'class' => backend\modules\virtual_terminal\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'virtual-terminal' => [
            'class' => backend\modules\virtual_terminal\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'api' => [
            'class' => backend\modules\api\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'marketing' => [
            'class' => backend\modules\marketing\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'advert' => [
            'class' => backend\modules\advert\Module::class,
            'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
    ],
    'container' => [
        'definitions' => [
            \yii\widgets\LinkPager::class => \backend\modules\admin\widgets\yii\LinkPager::class,
            \yii\bootstrap4\ActiveField::class => \backend\modules\admin\widgets\yii\ActiveField::class,
        ],
    ],
    'params' => $params,

];

// if (!YII_ENV_PROD) {
//     // configuration adjustments for 'dev' environment
//     $config['bootstrap'][] = 'debug';
//     $config['modules']['debug'] = [
//         'class' => 'yii\debug\Module',
//     ];

//     $config['bootstrap'][] = 'gii';
//     $config['modules']['gii'] = [
//         'class' => 'yii\gii\Module',
//     ];
// }

return $config;
