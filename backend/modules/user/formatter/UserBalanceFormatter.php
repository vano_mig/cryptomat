<?php
namespace backend\modules\user\formatter;

use backend\components\BackendFormatter;
use backend\modules\user\models\UserBalance;
use backend\modules\user\models\CompanyBalance;
use backend\modules\user\models\CompanyPackage;
use backend\modules\user\models\Package;
use backend\modules\user\models\User;
use Yii;
use yii\bootstrap4\Html;

class UserBalanceFormatter extends BackendFormatter
{
    /**
     * Return username by its id
     * @param $data integer
     * @return mixed
     */
    public function asUser($user_id)
    {
        $user = User::find()->where(['id' => $user_id])->one();
        $user_name = '#' . $user->id . ' - ' . $user->username;
        return $user_name;
    }




    /**
     * Get Package status
     * @param $value string
     * @return string
     */
    public function asStatusUserBalance($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new Package();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get CompanyPackage status
     * @param $value string
     * @return string
     */
    public function asCompanyPackageStatus($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new CompanyPackage();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } elseif ($value == $model::STATUS_INACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-primary']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }
    /**
     * Get CompanyBalance status
     * @param $value string
     * @return string
     */
    public function asStatusBalance($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new CompanyBalance();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-primary']);
            } elseif ($value == $model::STATUS_CLOSED) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } elseif($value == $model::STATUS_WAIT) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-secondary']);
            }elseif($value == $model::STATUS_PAID) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-warning']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get CompanyBalance service
     * @param $value string
     * @return string
     */
    public function asType($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new UserBalance();
        $types = $model->getTypes();
        if (array_key_exists($value, $types)) {
            switch ($value) {
                case $model::TYPE_RETURN_PACK:
                    $result = Html::tag('span', $types[$value], ['class' => 'badge badge-success']);
                    break;
                case $model::TYPE_PAYMENT_DEFERRED:
                    $result = Html::tag('span', $types[$value], ['class' => 'badge badge-danger']);

            };
        }
        return $result;
    }

    public function asMessage($data):string
    {
        if($data->type == $data::TYPE_RETURN_PACK) {
            $result = Yii::t('user_balance', 'Product:').' - '.$data->product->getProductName($data->product->productLanguage);
        } else {
            $result = $data->description;
        }
        return $result;
    }
}