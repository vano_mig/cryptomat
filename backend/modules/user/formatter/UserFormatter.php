<?php

namespace backend\modules\user\formatter;

use backend\components\BackendFormatter;
use yii\bootstrap4\Html;
use backend\modules\user\models\User;
use Yii;

class UserFormatter extends BackendFormatter
{
    /**
     * Get Role name
     * @param $value string
     * @return string
     */
    public function asRole($value): string
    {
        return Yii::t('users', $value);
    }

    /**
     * Get user status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
        $model = new User;
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_ACTIVE) {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-success']);
        } elseif ($value == $model::STATUS_INACTIVE) {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-secondary']);
        } elseif ($value == $model::STATUS_DELETED) {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-danger']);
        } else {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-warning']);
        }
    }

    /**
     * Get image to user
     * @param $data string
     * @return string
     */
    public function asImage($data, $size = false)
    {
        $image = Yii::t('admin', 'not set');
        $style = $size == true ? '' : 'style="width:100px;"';
        if ($data) {
            $image = '<img src="' . $data . '" ' . $style . '>';
        }
        return $image;
    }

    public function asWhiteList($value):string
    {
        $result = "<ul>";
        if ($value) {
            $array = json_decode($value);
            if(!empty($array)) {
                foreach($array as $item) {
                    $result .= "<li>$item</li>";
                }
            } else {
                $result .= Yii::t('admin', 'not set');
            }
        } else {
            $result .= Yii::t('admin', 'not set');
        }
        $result .= "</ul>";
        return $result;
    }
}
