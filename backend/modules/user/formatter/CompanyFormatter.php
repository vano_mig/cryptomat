<?php

namespace backend\modules\user\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Country;
use backend\modules\user\models\Company;
use backend\modules\user\models\CompanyBalance;
use backend\modules\user\models\Package;
use backend\modules\user\models\User;
use Yii;
use yii\bootstrap4\Html;
use yii\helpers\Url;

class CompanyFormatter extends BackendFormatter
{
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public function asLang($value): string
    {
        $country = Country::find()->where(['id' => $value])->one();
        if (!$country) {
            return Yii::t('admin', 'not set');
        }

        $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-' . $country->iso_code]) . ' ' . Html::tag('span',
                $country->iso_code);
        if ($country->iso_code == 'uk') {
            $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-ua']) . ' ' . Html::tag('span', $value);
        }
        if ($country->iso_code == 'en') {
            $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-gb']) . ' ' . Html::tag('span', $value);
        }
        return $res;
    }

    /**
     * Get Company status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new Company();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get User email
     * @param $value array
     * @return string
     */
    public function asUser($value): string
    {
        if (empty($value)) {
            $result = Yii::t('admin', 'not set');
        } else {
            $result = '';
            foreach ($value as $item) {
                $user = User::findOne($item);
                if ($user) {
                    $result .= Html::a('#' . $user->id . ' - ' . $user->email,
                            Yii::$app->urlManager->createUrl(['/user/user/view', 'id' => $user->id])) . '<br>';
                }
            }
        }
        return $result;
    }

    /**
     * Get image to company
     * @param $data string
     * @return string
     */
    public function asImage($data, $size = false)
    {
        $image = Yii::t('admin', 'not set');
        $style = $size == true ? '' : 'style="width:100px;"';
        if ($data) {
            $image = '<img class="rounded elevation-2" src="' . $data . '" ' . $style . '>';
        }
        return $image;
    }

    /**
     * Get Package status
     * @param $value string
     * @return string
     */
    public function asStatusPackage($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new Package();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get company name
     * @param $data object
     * @return string
     */
    public function asCompany($data): string
    {
        if ($data->company) {
            $result = Html::a($data->company->name, Url::toRoute(['/user/company/view', 'id' => $data->company_id]));
        } else {
            $result = Yii::t('admin', 'not set');
        }
        return $result;
    }

    /**
     * Get CompanyBalance status
     * @param $value string
     * @return string
     */
    public function asStatusBalance($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new CompanyBalance();
        $status = $model->getStatuses();
        if (array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-primary']);
            } elseif ($value == $model::STATUS_CLOSED) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-success']);
            } elseif($value == $model::STATUS_WAIT) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-secondary']);
            }elseif($value == $model::STATUS_PAID) {
                $result = Html::tag('span', $value, ['class' => 'badge badge-warning']);
            } else {
                $result = Html::tag('span', $value, ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get CompanyBalance service
     * @param $value string
     * @return string
     */
    public function asService($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new CompanyBalance();
        $services = $model->getServices();
        if (array_key_exists($value, $services)) {
            switch ($value) {
                case $model::SERVICE_RFID:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-success']);
                    break;
                case $model::SERVICE_ADVERT:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-danger']);
                    break;
                case $model::SERVICE_PRODUCT:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-primary']);
                    break;
                case $model::SERVICE_PACKAGE:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-secondary']);
                    break;
                case $model::SERVICE_SMS:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-info']);
                    break;
                case $model::SERVICE_EMAIL:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-info']);
                    break;
                case $model::SERVICE_WEEKLY_INVOICE:
                    $result = Html::tag('span', $services[$value], ['class' => 'badge badge-warning']);
                    break;
            };
        }
        return $result;
    }
}