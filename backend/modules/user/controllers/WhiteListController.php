<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\User;
use Yii;
use backend\modules\user\models\WhiteList;
use backend\modules\user\models\WhiteListSearch;
use yii\bootstrap4\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WhiteListController implements the CRUD actions for WhiteList model.
 */
class WhiteListController  extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WhiteList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('atm.update');
        $searchModel = new WhiteListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WhiteList model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('atm.update');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WhiteList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('atm.update');
        $model = new WhiteList();

        if (Yii::$app->request->post('WhiteList')) {
            $post = Yii::$app->request->post('WhiteList');
            $model->company_id = $post['company_id'];
//            $model->atm_id = $post['atm_id'];
            $model->ip_address = json_encode($post['ip_address']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            $model->ip_address = json_decode($model->ip_address, true);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WhiteList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('atm.update');
        $model = $this->findModel($id);

        if (Yii::$app->request->post('WhiteList')) {
            $post = Yii::$app->request->post('WhiteList');
            $model->company_id = $post['company_id'];
//            $model->atm_id = $post['atm_id'];
            $model->ip_address = json_encode($post['ip_address']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $model->ip_address = json_decode($model->ip_address, true);
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WhiteList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('atm.update');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WhiteList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WhiteList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = WhiteList::findOne($id);
        } else {
            $model = WhiteList::findOne(['id'=>$id, 'company_id'=>Yii::$app->user->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('white_list', 'The requested page does not exist.'));
    }

    public function actionAddAddress()
    {
        $result = [];
        $result['status'] = 'error';
        if($_POST && array_key_exists('id', $_POST)) {
            $model = new WhiteList();
            $field = new ActiveForm();
            $id = $_POST['id'] +1;
            $result['data'] = $this->renderPartial('_form_ip', ['model'=>$model, 'form'=>$field, 'id'=>$id]);
            $result['status'] = 'success';
        }
        return $this->asJson($result);
    }
}
