<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\CompanyProfile;
use backend\modules\user\models\CompanyReferal;
use backend\modules\user\models\CompanySearch;
use backend\modules\user\models\User;
use backend\modules\user\models\UserProfile;
use Yii;
use backend\modules\user\models\Company;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyController implements the CRUD actions for Currency model.
 */
class CompanyController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     */
    public function actionIndex()
    {
        $this->accessRules('company.listview');
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('company.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     */
    public function actionCreate()
    {
        $this->accessRules('company.create');
        $model = new Company();
        $profile = new CompanyProfile();
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['Company']['size']['image'] != false) {
                $model->saveImage('image');
            }
            if (Yii::$app->request->post('CompanyProfile')) {
                $profile->attributes = Yii::$app->request->post('CompanyProfile');

            }
            if ($model->save()) {
                $profile->company_id = $model->id;
                $profile->save();
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }
        $model->image = Company::PATH_DEFAULT_LOGO;
        return $this->render('create', [
            'model' => $model,
            'profile'=>$profile
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('company.update');
        $model = $this->findModel($id);
        $profile = $model->profile;
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['Company']['size']['image'] != false) {
                $model->saveImage('image');
            }
            if (!$model->image) {
                $model->image = $model->oldAttributes['image'];
            }

            if (Yii::$app->request->post('CompanyProfile')) {
                $profile->attributes = Yii::$app->request->post('CompanyProfile');

            }
            if ( $model->save() && $profile->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }

        return $this->render('update', [
            'model' => $model,
            'profile'=>$profile
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->accessRules('company.delete');
        $model = $this->findModel($id);
        $model->status = Company::STATUS_DELETED;
        $model->save();
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Currency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->identity->role === User::ROLE_ADMIN || Yii::$app->user->identity->company_id == $id) {
            if (($model = Company::findOne($id)) !== null) {
                return $model;
            }
        }
        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

    /**
     * Display company staff
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUsers($id)
    {
        $this->accessRules('company.user');
        $model = $this->findModel($id);
        $users = User::find()->where(['company_id' => $id])->all();
        return $this->render('staff', ['model' => $model, 'users' => $users]);

    }

    /**
     * Display User model
     * @param $id
     * @param $user_id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionViewUser($id, $user_id)
    {
        $this->accessRules('company.user');
        $model = $this->findModel($id);
        $user = User::find()->where(['id' => $user_id, 'company_id' => $id])->one();
        return $this->render('view_user', ['model' => $model, 'user' => $user]);

    }

    /**
     * Update company user data
     * @param $id
     * @param $user_id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdateUser($id, $user_id)
    {
        $this->accessRules('company.user');
        $model = $this->findModel($id);
        $user = User::find()->where(['id' => $user_id, 'company_id' => $id])->one();
        if (Yii::$app->request->post('User')) {
            $user->load(Yii::$app->request->post());
            if (($user->oldAttributes)['status'] != $user->status && ($user->oldAttributes)['status'] == User::STATUS_DELETED) {
                if (preg_match('/^' . User::STATUS_DELETED_PREFIX_EMAIL . '/', "$user->email")) {
                    $user->email = substr($user->email, iconv_strlen(User::STATUS_DELETED_PREFIX_EMAIL));
                }
            }
            if ($user->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['users', 'id' => $model->id]));
            }
        }
        return $this->render('update_user', ['model' => $model, 'user' => $user]);
    }

    /**
     * Create new company user
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionAddUser($id)
    {
        $this->accessRules('company.user');
        $model = $this->findModel($id);
        $user = new User();
        if (Yii::$app->request->post('User')) {
            $user->attributes = Yii::$app->request->post('User');
            if (Yii::$app->request->post('User')['password_hash']) {
                $user->setPassword($user->password_hash);
            }

            $user->company_id = $model->id;
            if ($user->save()) {
                $userProfile = new UserProfile();
                $userProfile->user_id = $user->id;
                $userProfile->save();
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['users', 'id' => $model->id]));
            }
        }
        return $this->render('create_user', ['model' => $model, 'user' => $user]);
    }

    public function actionDeleteUser($id, $user_id)
    {
        $this->accessRules('company.user');
        $model = $this->findModel($id);
        $user = User::find()->where(['id' => $user_id, 'company_id' => $id])->one();
        if (!empty($user)) {
            $user->email = User::STATUS_DELETED_PREFIX_EMAIL . $user->email;
            $user->status = User::STATUS_DELETED;
            if ($user->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'User deleted'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('admin', 'User not deleted'));
            }
        }
        return $this->redirect(Url::toRoute(['users', 'id' => $model->id]));
    }

    /**
     * Manage Company agents
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAgents($id)
    {
        if (Yii::$app->user->can(User::ROLE_CUSTOMER) && $id != Yii::$app->user->identity->company_id) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }
        $user = $this->findModel($id);
        $model = new Company();
        $agents = CompanyReferal::find()->where(['company_id' => $id])->all();
        $referalModel = CompanyReferal::find()->where(['company_referral_id' => $id])->all();
        $myStatus = true;
        if (Yii::$app->request->post()) {
            $delReferal = CompanyReferal::find()->where(['company_id' => $id])->all();
            if (!empty($delReferal)) {
                foreach ($delReferal as $item) {
                    $this->deleteReferal($id, $item->company_referral_id);
                    $item->delete();
                }
            }
            if (!empty(Yii::$app->request->post('CompanyAgent')['id'])) {
                foreach (Yii::$app->request->post('CompanyAgent')['id'] as $agent) {
                    $companyReferal = new CompanyReferal();
                    $companyReferal->company_id = $user->id;
                    $companyReferal->company_referral_id = $agent;
                    $companyReferal->save();
                }
            }
        }
        if (Yii::$app->request->post('Company')) {
            $companyReferal = new CompanyReferal();
            $companyReferal->company_id = $user->id;
            $companyReferal->company_referral_id = Yii::$app->request->post('Company')['id'];
            foreach ($referalModel as $item) {
                if (($item->company_id) == $companyReferal->company_referral_id) {
                    $myStatus = false;
                    break;
                }
            }
            if ($myStatus) {
                $companyReferal->save();
                $this->addReferal($user->id, $companyReferal->company_referral_id);

            }

            if ($myStatus) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
            return $this->redirect(Url::toRoute(['view', 'id' => $user->id]));
        }

        return $this->render('agents', ['model' => $model, 'agents' => $agents, 'id' => $id]);

    }

    /**
     * Save agent to owner user
     * @param $id integer, $referal_id integer
     */
    public function addReferal($id, $referal_id)
    {
        $model = CompanyReferal::find()->where(['company_referral_id' => $id])->all();
        if (!empty($model)) {
            foreach ($model as $item) {
                if (CompanyReferal::find()->where(['company_id' => $item->company_id, 'company_referral_id' => $referal_id])->one()) {
                    continue;
                } else {
                    $companyReferal = new CompanyReferal();
                    $companyReferal->company_id = $item->company_id;
                    $companyReferal->company_referral_id = $referal_id;
                    $companyReferal->save();
                }
                $this->addReferal($item->user_id, $referal_id);
            }
        }
    }

    public function deleteReferal($id, $company_referral_id)
    {
        $model = CompanyReferal::find()->where(['company_referral_id' => $id])->all();
        if (!empty($model)) {
            foreach ($model as $item) {
                $user = CompanyReferal::find()->where(['company_id' => $item->company_id, 'company_referral_id' => $company_referral_id])->one();
                if ($user) {
                    $user_id = $user->company_id;
                    $user->delete();
                    $this->deleteReferal($user_id, $company_referral_id);
                }
            }
        }
    }
}
