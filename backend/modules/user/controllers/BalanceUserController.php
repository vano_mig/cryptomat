<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\User;
use Yii;
use backend\modules\user\models\UserBalance;
use backend\modules\user\models\UserBalanceSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserBalanceController implements the CRUD actions for UserBalance model.
 */
class BalanceUserController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserBalance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('user_balance.listview');
        $searchModel = new UserBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserBalance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {     $this->accessRules('user_balance.view');
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new UserBalance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $this->accessRules('user_balance.create');
//        $model = new UserBalance();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Finds the UserBalance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserBalance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = UserBalance::findOne($id);
        } else {
            $model = UserBalance::find()->where(['id'=>$id, 'user_id'=>Yii::$app->user->id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
