<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\User;
use backend\modules\user\models\UserSearch;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class KycController
 * @package backend\modules\user\controllers
 */
Class KycController extends FrontendController
{
    public function actionIndex()
    {
        $this->accessRules('kyc.list');
        $model = new UserSearch();
        $searchModel = $model->searchKyc();
        return $this->render('kyc', [
            'searchModel' => $model,
            'dataProvider' => $searchModel,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->accessRules('kyc.update');
        if (Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = User::findOne($id);
        } else {
            $model = User::findOne(['company_id' => Yii::$app->user->identity->company_id]);
        }

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }

        if (Yii::$app->request->post('User')) {
            $model->kyc = Yii::$app->request->post('User')['kyc'];
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            }
        }
        return $this->render('update', ['model' => $model]);
    }
}
