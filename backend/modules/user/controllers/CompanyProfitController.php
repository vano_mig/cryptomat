<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\user\models\User;
use Yii;
use backend\modules\user\models\CompanyProfit;
use backend\modules\user\models\CompanyProfitSearch;
use yii\web\NotFoundHttpException;

/**
 * CompanyProfitController implements the CRUD actions for CompanyProfit model.
 */
class CompanyProfitController extends FrontendController
{

    /**
     * Lists all CompanyProfit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('transaction.listview');
        $searchModel = new CompanyProfitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listCurrency = (new CryptoCurrency())->getListCodeFull();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listCurrency'=>$listCurrency
        ]);
    }

    /**
     * Displays a single CompanyProfit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('transaction.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the CompanyProfit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyProfit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = CompanyProfit::findOne($id);
        } elseif(Yii::$app->user->can(User::ROLE_CUSTOMER)) {
            $ids = [];
            $ids[] = Yii::$app->user->identity->company_id;
            $referrals = Yii::$app->user->identity->company->refferal;
            if(!empty($referrals)) {
                foreach ($referrals as $item) {
                    $ids[] = $item->company_referral_id;
                }
            }
            $model = CompanyProfit::find()->where(['id'=>$id])->andWhere(['in', 'company_id', $ids])->one();
        } else {
            $model = CompanyProfit::findOne(['id'=>$id, 'company_id'=>Yii::$app->user->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('company', 'The requested page does not exist.'));
    }
}
