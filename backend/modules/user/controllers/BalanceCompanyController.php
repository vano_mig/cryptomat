<?php

namespace backend\modules\user\controllers;

use frontend\models\Company;
use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\CompanyBalance;
use backend\modules\user\models\CompanyBalanceSearch;
use backend\modules\user\models\User;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * CompanyBalanceController implements the CRUD actions for CompanyBalance model.
 */
class BalanceCompanyController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyBalance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('company.balance');
        $searchModel = new CompanyBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyBalance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('company.balance');
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Print order as .pdf-file
     * @param $id integer
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */

    public function actionViewPdf($id)
    {
        $this->accessRules('company.balance');
        $model = $this->findModel($id);

        $content = $this->renderPartial('_form_pdf', [
            'model' => $model
        ]);
        $header = $this->getPDFHeader();
        $footer = $this->getPDFFooter($model->company_id);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => Yii::t('finance', 'Invoice') . ' - ' . $id . '.pdf',
            'content' => $content,
            'marginBottom' => 28,
            'marginTop' => 30,
            'marginLeft' => 5,
            'marginRight' => 5,
            'cssFile' => Yii::getAlias('@webroot') . '/css/pdfStyle.css',
            'methods' => [
                'SetTitle' => Yii::t('finance', 'Invoice') . ' - ' . $id,
                'SetHeader' => $header,
                'SetFooter' => $footer,
            ]
        ]);
        return $pdf->render();
    }

    private function getPDFFooter($id)
    {
        $user = User::find()->where(['company_id' => $id, 'role' => User::ROLE_CUSTOMER])->one();
        if (empty($user)) {
            return Yii::t('error', 'User did not found, can not create footer');
        }

        $company = Company::findOne($id);
        $footer = $this->renderPartial('//pdf-template/_footer_pdf', [
            'company' => $company,
            'agent' => $user
        ]);

        return $footer;
    }

    /**
     * Send order to client email
     * @param $invoice_id
     * @return \yii\web\Response
     */
    public function actionSendPdf($invoice_id)
    {
        $this->accessRules('company.balance');
        $invoice_pdf = $this->generatePDFInvoice($invoice_id);
        $mail_subject = Yii::t('admin', 'Invoice') . ': ' . $invoice_id;
        $mail_body = Yii::t('admin', 'Invoice for your RF ID order') . ' #' . $invoice_id;
        $pdf_file_name = Yii::t('admin', 'Invoice for RF ID order') . ' #' . $invoice_id . '.pdf';

        Yii::$app->mailer->compose()
            ->attachContent($invoice_pdf, [
                'fileName' => $pdf_file_name,
                'contentType' => 'application/pdf'
            ])
            ->setFrom([Yii::$app->params['paymentEmail'] => Yii::$app->params['senderName']])
//            ->setTo(['dsci@i.ua'])
            ->setTo([(new User())->getCurrentUserEMail(), Yii::$app->params['ivanEmail']])
            ->setSubject($mail_subject)
            ->setTextBody($mail_body)
            ->send();
        Yii::$app->session->setFlash('success', Yii::t('admin', 'Invoice successfully sent'));
        return $this->redirect(Url::toRoute('index'));
    }

    private function generatePDFInvoice($id)
    {
        $model = $this->findModel($id);

        $header = $this->getPDFHeader();
        $content = $this->renderPartial('_form_pdf', [
            'model' => $model
        ]);
        $footer = $this->getPDFFooter($model->company_id);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => Yii::t('balance', 'Invoice') . ' - ' . $id . '.pdf',
            'content' => $content,
            'marginBottom' => 28,
            'marginTop' => 30,
            'marginLeft' => 5,
            'marginRight' => 5,
            'cssFile' => Yii::getAlias('@webroot') . '/css/pdfStyle.css',
            'methods' => [
                'SetTitle' => 'Invoice - ' . $id,
                'SetHeader' => $header,
                'SetFooter' => $footer
            ]
        ]);

        return $pdf->output($content, 'Invoice #' . $id . '.pdf');
    }

    private function getPDFHeader()
    {
        return $this->renderPartial('//pdf-template/_header_pdf');
    }

    /**
     * Creates a new CompanyBalance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('company.balance');
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }
        $model = new CompanyBalance();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->date == null) {
                $model->date = date('d-m-Y', strtotime('+3 days'));
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CompanyBalance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('company.balance');
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }
        $model = $this->findModel($id);
        $date = $model->date;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->date == null) {
                $model->date = $date;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CompanyBalance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('company.balance');
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }
//        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyBalance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyBalance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = CompanyBalance::findOne($id);
        } else {
            $model = CompanyBalance::findOne()->where(['id'=>$id, 'company_id'=>Yii::$app->user->identity->company_id])->one();
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
