<?php

namespace backend\modules\user\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\Permission;
use Yii;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class PermissionsController extends FrontendController
{
    /**
     * Display list roles
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     */
    public function actionIndex()
    {
        $this->accessRules('permission.listview');
        $model = new Permission();
        return $this->render('index', ['model'=>$model, 'roles'=>$model->getListRoles()]);
    }

    /**
     * Create new role
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $this->accessRules('permission.create');
        $model = new Permission();
        $permissions = $model->getPermissions();

        if(Yii::$app->request->post('Role')) {
            $role = Yii::$app->authManager->createRole(Yii::$app->request->post('Role')['name']);
            $role->description = Yii::$app->request->post('Role')['name'];
            Yii::$app->authManager->add($role);
            if(Yii::$app->request->post('Permission')) {
                foreach(Yii::$app->request->post('Permission') as $key => $item) {
                    $perm = Yii::$app->authManager->getPermission($key);
                    Yii::$app->authManager->addChild($role, $perm);
                }
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            }
        }
        return $this->render('create', ['permissions'=>$permissions]);
    }

    /**
     * Update role
     * @param $id string
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $this->accessRules('permission.update');
        $role = Yii::$app->authManager->getRole($id);
        if(!$role)
            throw new NotFoundHttpException('admin', 'not found');
        $model = new Permission();
        $permissions = $model->getPermissions();
        $userPermissions = $model->getPermissionsByRole($role->name);
        if(Yii::$app->request->post('role')) {
            Yii::$app->authManager->removeChildren($role);
            if(Yii::$app->request->post('Permission')) {
                foreach(Yii::$app->request->post('Permission') as $key => $item) {
                    $perm = Yii::$app->authManager->getPermission($key);
                    Yii::$app->authManager->addChild($role, $perm);
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
            return $this->redirect(Url::toRoute('index'));
        }
        return $this->render('update', ['role'=>$role, 'userPermissions'=>$userPermissions, 'permissions'=>$permissions]);
    }
}