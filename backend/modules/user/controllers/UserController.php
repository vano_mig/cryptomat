<?php

namespace backend\modules\user\controllers;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use backend\modules\admin\controllers\FrontendController;
use backend\modules\pkpass\models\PkpassPass;
use backend\modules\pkpass\models\PkpassTemplate;
use backend\modules\user\models\User;
use backend\modules\user\models\UserProfile;
use backend\modules\user\models\UserSearch;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

Class UserController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('user.listview');
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new User;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('user.view');
        $model = $this->findModel($id);

        if (empty(($modelPasss = PkpassPass::find()->where(['user_id' => $model->id])->one()))) {
            $modelPasss = new PkpassPass();
            $modelPasss->serial_number = uniqid('', true);
            $modelPasss->authentication_token = uniqid('token_', true);
            $modelPasss->user_id = $model->id;
            $modelPasss->template_id = (PkpassTemplate::find()->where(['status' => PkpassTemplate::STATUS_ACTIVE])->one())->id;
            $modelPasss->pass_type_identifier = PkpassTemplate::PASS_TYPE_IDENTIFIER;
            $modelPasss->save();
        }
        $modelPkpassTemplate = $modelPasss->pkpassTemplate;


        $modelPkpassTemplate->createPass($modelPasss,false);

        $renderer = new ImageRenderer(
            new RendererStyle(700),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);
        return $this->render('view', ['model' => $model,
            'writer'=>$writer,
            'modelPasss'=>$modelPasss]);
    }
    public function actionGetPassIos($serialNumber)
    {
        $passFile = fopen(Yii::getAlias('@webroot') . '/pkpass/temp/' . $serialNumber . '_pass.pkpass', 'rb');
        Yii::$app->response->setStatusCode(200);
        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
        Yii::$app->response->sendStreamAsFile($passFile, 'pass.pkpass', ['mimeType' => 'application/vnd.apple.pkpass', 'inline' => false])->send();
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('user.create');
        $model = new User(['scenario' => 'insert']);
        $model->image = $model::PATH_DEFAULT_LOGO;
        $modelProfileUpdate = new UserProfile();
        if (Yii::$app->request->post('User')) {
            $model->attributes = Yii::$app->request->post('User');
            $model->setPassword($model->password_hash);
            if ($_FILES['User']['size']['image'] != false) {
                $model->saveImage('image');
            }
            if ($model->validate() && $model->save()) {
                $modelProfileUpdate->load(Yii::$app->request->post());
                $modelProfileUpdate->user_id = $model->id;
                if ($modelProfileUpdate->validate() && $modelProfileUpdate->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                    return $this->redirect(Url::toRoute('index'));
                } else {
                    $model->delete();
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
        }
        $birthdaySelect = $model->birthdaySelect($model->birthdayY, $model->birthdayD, $model->birthdayY, $model->birthday);

        return $this->render('create', ['model' => $model, 'modelProfileUpdate' => $modelProfileUpdate, 'birthdaySelect' => $birthdaySelect]);
    }

    /**
     * Updates an existing Update model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('user.update');
        $model = $this->findModel($id);

        $model->scenario = 'pass';
        $modelProfileUpdate = UserProfile::find()->where(['user_id' => $id])->one();
        $role = $model->role;
        $oldHash = $model->password_hash;
        $model->password_hash = 'password';
        if (Yii::$app->request->post('User')) {
            $model->load(Yii::$app->request->post());
            if (($model->oldAttributes)['status'] != $model->status && ($model->oldAttributes)['status'] == $model::STATUS_DELETED) {
                if (preg_match('/^' . User::STATUS_DELETED_PREFIX_EMAIL . '/', "$model->email")) {
                    $model->email = substr($model->email, iconv_strlen(User::STATUS_DELETED_PREFIX_EMAIL));
                }
            }
            $oldPass = $model->password_hash;
            if ($model->birthdayY && $model->birthdayM && $model->birthdayD) {
                $model->birthday = $model->birthdayY . '-' . $model->birthdayM . '-' . $model->birthdayD;
            }

            $modelProfileUpdate->load(Yii::$app->request->post());

            if ($model->password_hash != 'password') {
                if ($model->validate()) {
                    $model->setPassword($model->password_hash);
                }
            } else {
                $model->password_hash = $oldHash;
                $model->password_check = $oldHash;
            }

            if ($_FILES['User']['size']['image'] != false) {
                $model->saveImage('image');
            }

            if (empty($model->image)) {
                $model->image = $model->oldAttributes['image'];
            }

            if ($model->validate() && $modelProfileUpdate->validate()) {
                if ($model->save() && $modelProfileUpdate->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                    $model->password_hash = $oldPass;
                }
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                $model->password_hash = $oldPass;
            }
        }
        $birthdaySelect = $model->birthdaySelect($model->birthdayY, $model->birthdayD, $model->birthdayY, $model->birthday);

        return $this->render('update', ['model' => $model,
            'modelProfileUpdate' => $modelProfileUpdate, 'birthdaySelect' => $birthdaySelect]);
    }

    /**
     * Deletes an existing User model.
     *
     * @param $id
     * @return yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->accessRules('user.delete');
        $model = User::findOne($id);
        if ($model->status != User::STATUS_DELETED) {
            $model->email = User::STATUS_DELETED_PREFIX_EMAIL . $model->email;
            $model->status = User::STATUS_DELETED;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'User deleted'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('admin', 'User not deleted'));
            }
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('admin', 'User already deleted'));
        }
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Manages user status$model->load(Yii::$app->request->post());
     * @param $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionActive($id)
    {
        $this->accessRules('user.update');
        $model = $this->findModel($id);

        if ($model->status == $model::STATUS_ACTIVE || $model->status == $model::STATUS_SEMI_ACTIVE) {
            $model->status = $model::STATUS_INACTIVE;
            Yii::$app->session->setFlash('success', Yii::t('user', 'User is blocked'));
        } elseif ($model->status == $model::STATUS_DELETED) {
            if (preg_match('/^' . User::STATUS_DELETED_PREFIX_EMAIL . '/', "$model->email")) {
                $model->email = substr($model->email, iconv_strlen(User::STATUS_DELETED_PREFIX_EMAIL));
            }
            $model->status = $model::STATUS_ACTIVE;
            Yii::$app->session->setFlash('success', Yii::t('user', 'User is restored'));
        } else {
            $model->status = $model::STATUS_ACTIVE;
            Yii::$app->session->setFlash('success', Yii::t('user', 'User is unblocked'));
        }

        if (!$model->save()) {
            Yii::$app->session->setFlash('success', null);
            Yii::$app->session->setFlash('danger', Yii::t('admin', 'Action failed'));
        }
        return $this->redirect(Url::toRoute('index'));
    }

    public function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = User::findOne($id);
        } else {
            $model = User::findOne(['id'=>Yii::$app->user->id]);
        }
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
        return $model;
    }
}
