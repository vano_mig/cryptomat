<?php

namespace backend\modules\user\controllers;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use frontend\models\UploadForm;
use backend\modules\admin\controllers\FrontendController;
use backend\modules\admin\models\RegistrationThirdParty;
use backend\modules\pkpass\models\PkpassPass;
use backend\modules\pkpass\models\PkpassTemplate;
use backend\modules\user\models\UserPassport;
use backend\modules\user\models\UserProfile;
use Yii;
use backend\modules\user\models\User;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use backend\modules\admin\models\IdentificationCard;
use frontend\models\PasscreatorService;
use backend\modules\admin\models\PasscreatorTemplate;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * View & Updates User personal info.
     * @return mixed
     */
    public function actionIndex($edit = false)
    {
        $model = User::findOne(Yii::$app->user->id);
        $files = new UploadForm();
        $model->scenario = 'pass';
        $modelProfileUpdate = $model->userProfile;
        $oldEmail = $model->email;
        $oldHash = $model->password_hash;
        $model->password_hash = '|-_^_^_-|';
        if ($model->load(Yii::$app->request->post()) && $modelProfileUpdate->load(Yii::$app->request->post())) {

            if ($_FILES['User']['size']['image'] != false) {
                $model->saveImage('image');
            }

            if (empty($model->image)) {
                $model->image = $model->oldAttributes['image'];
            }

            $oldPass = $model->password_hash;
            if ($model->password_hash != '|-_^_^_-|') {
                if ($model->validate()) {
                    $model->setPassword($model->password_hash);
                }
            } else {
                $model->password_hash = $oldHash;
                $model->password_check = $oldHash;
            }
            if ($model->birthdayY && $model->birthdayM && $model->birthdayD) {
                $model->birthday = $model->birthdayY . '-' . $model->birthdayM . '-' . $model->birthdayD;
            }
            if ($oldEmail != $model->email) {
                $model->status = User::STATUS_SEMI_ACTIVE;
            }

            if(Yii::$app->request->post('UploadForm')) {
                $files->imageFile = UploadedFile::getInstances($files, 'imageFile');
                if($files->validate()) {
                    $passport = new UserPassport();
                    if($passport->saveImages($files)) {
                        User::updateAll(['status' => User::STATUS_ACTIVE], ['id' => Yii::$app->user->id]);
                    }
                }
            }
            if ($model->save() && $modelProfileUpdate->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                if ($model->status == User::STATUS_SEMI_ACTIVE) {
                    Yii::$app->mailer->compose('emailVerify-html', ['user' => $model])->setFrom([Yii::$app->params['registerEmail'] => Yii::$app->params['senderName']])->setTo([$model->email])->setSubject('Verify Email')->send();
                }
                if ($edit) {
                    return $this->redirect(Url::toRoute('registration-third-party'));
                } else {
                    return $this->redirect(Url::toRoute('index'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                $model->password_hash = $oldPass;
            }
        }

        $birthdaySelect = $model->birthdaySelect($model->birthdayY, $model->birthdayD, $model->birthdayY, $model->birthday);

        return $this->render('index', [
            'model' => $model,
            'modelProfileUpdate' => $modelProfileUpdate,
            'birthdaySelect' => $birthdaySelect,
            'files'=>$files
        ]);
    }

    public function actionPayment()
    {
        $this->redirect('https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . Url::base(true) . '/pkpass/temp/pass.pkpass');
    }

    public function actionPaymentMethod()
    {

        if (!Yii::$app->user->can('card.create')) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }

        $userId = Yii::$app->user->id;
        $modelUser = User::find()->where(['id' => $userId])->one();
        $modelUserProfile = $modelUser->userProfile;
        $modelIdentificationCard = new IdentificationCard();
        $modelPass = new PasscreatorService();


        if (empty(($modelPasss = PkpassPass::find()->where(['user_id' => $userId])->one()))) {
            $modelPasss = new PkpassPass();
            $modelPasss->serial_number = uniqid('', true);
            $modelPasss->authentication_token = uniqid('token_', true);
            $modelPasss->user_id = $userId;
            $modelPasss->voided = false;
            $modelPasss->template_id = (PkpassTemplate::find()->where(['status' => PkpassTemplate::STATUS_ACTIVE])->one())->id;
            $modelPasss->save();
        }
        if (empty($modelUserProfile->qr_key) || $modelUserProfile->pkpass_status != $modelUserProfile::STATUS_PASS_ACTIVE) {
            if (empty($modelUserProfile->qr_key)) {
                $modelUserProfile->qr_key = $modelUserProfile->qr_key();
            }
            if ($modelUserProfile->pkpass_status != $modelUserProfile::STATUS_PASS_ACTIVE) {
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
            }
            $modelUserProfile->save();
        }
        if ($modelUser->status != User::STATUS_ACTIVE) {
            $modelUser->status = User::STATUS_ACTIVE;
            $modelUser->save();
        }

        $modelPkpassTemplate = $modelPasss->pkpassTemplate;
        $modelUser = $modelPasss->user;
        $modelUserProfile = $modelUser->userProfile;

        $modelPkpassTemplate->user = $modelUser;
        $modelPkpassTemplate->userProfile = $modelUserProfile;
        $modelPkpassTemplate->pass_serial_number = $modelPasss->serial_number;
        $modelPkpassTemplate->pass_authentication_token = $modelPasss->authentication_token;

        $pkpass = $modelPkpassTemplate->createPass();

        file_put_contents(Yii::getAlias('@webroot') . '/pkpass/temp/' . $modelPasss->serial_number . '_pass.pkpass', $pkpass);

        $renderer = new ImageRenderer(
            new RendererStyle(700),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);


        if (empty(PasscreatorTemplate::findOne(['template_status' => PasscreatorTemplate::STATUS_ACTIVE]))) {
            $passTemplate = false;
        } else {
            $passTemplate = true;
        }

        if ($modelUserProfile->pkpass_identifier != null || $modelUserProfile->pkpass_identifier != '') {
            $pkpass = true;
            $passId = $modelUserProfile->pkpass_identifier;
            $iosUrl = $modelUserProfile->ios_pass;
            $androidUrl = $modelUserProfile->android_pass;
        } else {
            $pkpass = false;
            $passId = null;
            $iosUrl = null;
            $androidUrl = null;
        }

        if (!empty($model = IdentificationCard::findOne(['user_id' => $userId]))) {
            $IdentificationCard = true;
            $modelIdentificationCard = $model;
            $identificationCardId = $model->card_id;
        } else {
            $IdentificationCard = false;
            $identificationCardId = null;
        }

        if (Yii::$app->request->post()) {

            if (Yii::$app->request->post('save') && $modelIdentificationCard->load(Yii::$app->request->post()) && $modelIdentificationCard->validate()) {
                if (($modelCard = IdentificationCard::find()->where(['card_id' => $modelIdentificationCard->card_id])->one()) !== null) {
                    $modelCard->user_id = $userId;
                    $modelCard->card_status = IdentificationCard::STATUS_ACTIVE;
                    if ($modelCard->save()) {
                        $modelUser->status = User::STATUS_ACTIVE;
                        if ($modelUser->save()) {
                            $IdentificationCard = true;
                            $modelIdentificationCard = $modelCard;
                            $identificationCardId = $modelIdentificationCard->card_id;
                            Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                        }
                        return $this->redirect(Url::toRoute('payment-method'));
                    }
                }
            }

            if (Yii::$app->request->post('pkpass') && $modelPass->load(Yii::$app->request->post()) && $modelPass->validate()) {
                $modelUserProfile->qr_key = $modelUserProfile->qr_key();
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
                if ($modelUserProfile->save()) {
                    $pkpass = $modelPass->passcreatorCreatePass($userId);
                    $passId = $pkpass->identifier;
                    $iosUrl = $pkpass->iPhoneUri;
                    $androidUrl = $pkpass->AndroidUri;
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
                return $this->redirect(Url::toRoute('payment-method'));
            }

            if (Yii::$app->request->post('card_status') == '0') {
                $modelIdentificationCard = IdentificationCard::find()->where(['card_id' => $modelIdentificationCard->card_id])->one();
                $modelIdentificationCard->card_status = $modelIdentificationCard::STATUS_INACTIVE;
                if ($modelIdentificationCard->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));

                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
                return $this->redirect(Url::toRoute('payment-method'));
            } elseif (Yii::$app->request->post('card_status') == '1') {
                $modelIdentificationCard = IdentificationCard::find()->where(['card_id' => $modelIdentificationCard->card_id])->one();
                $modelIdentificationCard->card_status = $modelIdentificationCard::STATUS_ACTIVE;
                if ($modelIdentificationCard->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
                return $this->redirect(Url::toRoute('payment-method'));
            }

            if (Yii::$app->request->post('pkpass_status') == '0') {
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_INACTIVE;
                if ($modelUserProfile->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
                return $this->redirect(Url::toRoute('payment-method'));
            } elseif (Yii::$app->request->post('pkpass_status') == '1') {
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
                if ($modelUserProfile->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
                return $this->redirect(Url::toRoute('payment-method'));
            }
        }

        return $this->render('paymentMethod', [
            'identificationCardId' => $identificationCardId,
            'modelUserProfile' => $modelUserProfile,
            'modelIdentificationCard' => $modelIdentificationCard,
            'modelPass' => $modelPass,
            'passId' => $passId,
            'iosUrl' => $iosUrl,
            'androidUrl' => $androidUrl,
            'passTemplate' => $passTemplate,
            'IdentificationCard' => $IdentificationCard,
            'modelPasss' => $modelPasss,
            'writer' => $writer
        ]);
    }

    /**
     * Change user credit card
     * @return string|yii\web\Response
     */
    public function actionAttachedCard()
    {
        $model = UserProfile::find()->where(['user_id' => Yii::$app->user->id])->one();

        if (Yii::$app->request->post('Delete')) {
            $model->deleteCard(Yii::$app->user->id);
            $model->card_status = $model::STATUS_CARD_INACTIVE;
            return $this->redirect(Url::toRoute('attached-card'));
        }

        if ($model->ref_card_no == '' && $model->ref_card_no == null && $model->reg_person != null) {
            return $this->redirect(Url::toRoute('add-card'));
        } elseif ($model->ref_card_no != '' && $model->ref_card_no != null && $model->reg_person == null) {
            return $this->redirect(Url::toRoute('registration-third-party'));
        } else {
            return $this->render('attachedCard', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Add card iframe.
     *
     * @return mixed
     */
    public function actionAddCard($edit = false)
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
        if (!Yii::$app->user->can('card.create')) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }

        $modelProfile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
        if ($edit == true) {
            return $this->render('addCard');
        } elseif ($modelProfile->ref_card_no == '' || $modelProfile->ref_card_no == null) {
            return $this->render('addCard');
        } else {
            return $this->redirect(Url::toRoute('registration-third-party'));
        }
    }

    /**
     * Add card.
     *
     * @param $xmlresponse
     * @return mixed
     */
    public function actionCard($xmlresponse = null)
    {
        $val = simplexml_load_string($xmlresponse);

        if (isset($val->RESPONSE->REFCARDNO)) {
            $vals = $val->RESPONSE;
            $userProfile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);

            $data = [];
            $data += ['ref_card_no' => (string)$vals->REFCARDNO];
            $data += ['ccno' => (string)$vals->CCNO];
            $data += ['card_type' => (string)$vals->CARDTYPE];
            $data += ['expiry_date' => (string)$vals->EXPIRYDATE];
            $data += ['first_name' => (string)$vals->FIRSTNAME];
            $data += ['last_name' => (string)$vals->LASTNAME];
            $data += ['errorCode' => (string)$vals->ERRORCODE];
            $data += ['errorText' => (string)$vals->ERRORTEXT];
            $data += ['action' => (string)$vals->ACTION];

            $userProfile->first_name_card = $data['first_name'];
            $userProfile->last_name_card = $data['last_name'];
            $userProfile->ccno = $data['ccno'];
            $userProfile->ref_card_no = $data['ref_card_no'];
            $userProfile->card_type = $data['card_type'];
            $userProfile->expiry_date = $data['expiry_date'];
            $userProfile->reg_person = null;
            $userProfile->card_status = $userProfile::STATUS_CARD_INACTIVE;

            if ($userProfile->save()) {
                return $this->renderPartial('card', [
                    'data' => $data,
                ]);
            } else {
                return $this->redirect(Url::toRoute('fail-card'));
            }
        } else {
            return $this->redirect(Url::toRoute('fail-card'));
        }
    }

    public function actionFailCard()
    {
        return $this->renderPartial('failCard');
    }

    public function actionRegistrationThirdParty()
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
        if (!Yii::$app->user->can('card.create')) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }

        $userId = Yii::$app->user->id;
        $model = new RegistrationThirdParty();
        $modelUser = User::findOne(['id' => $userId]);
        $modelUserProfile = $modelUser->userProfile;

        if ($modelUserProfile->reg_person != null) {
            return $this->redirect(Url::toRoute('attached-card'));
        } else {
            if (strtoupper($modelUser->username) == strtoupper($modelUserProfile->first_name_card) && strtoupper($modelUser->last_name) == strtoupper($modelUserProfile->last_name_card)) {
                $modelUserProfile->reg_person = 'person';
                $modelUserProfile->card_status = $modelUserProfile::STATUS_CARD_ACTIVE;
                $modelUserProfile->save();
                return $this->redirect(Url::toRoute('attached-card'));
            } else {
                if (Yii::$app->request->post('continue') && $model->load(Yii::$app->request->post()) && $model->validate()) {
                    $modelUserProfile->reg_person = 'third_party';
                    $modelUserProfile->card_status = $modelUserProfile::STATUS_CARD_ACTIVE;
                    $modelUserProfile->save();
                    return $this->redirect(Url::toRoute('attached-card'));
                }
                if (Yii::$app->request->post('change')) {
                    return $this->actionAddCard(true);
                }

                return $this->render('registrationThirdParty', [
                    'model' => $model,
                    'modelUser' => $modelUser,
                    'modelUserProfile' => $modelUserProfile,
                ]);
            }
        }
    }
}
