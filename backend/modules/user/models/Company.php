<?php

namespace backend\modules\user\models;

use backend\modules\admin\models\UploadForm;
use yii\imagine\Image;
use Imagine\Image\ImageInterface;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class Company
 * @package backend\modules\admin\models
 */
class Company extends \common\models\Company
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vat_number', 'zip_code', 'country_id', 'name', 'street', 'city', 'uid', 'reg_number', 'bank_name', 'bic', 'iban', 'contract_number'], 'required'],
            [['zip_code', 'country_id'], 'integer'],
            ['service_email', 'email'],
            [['vat_number', 'status'], 'string', 'min' => 4, 'max' => 50],
            [['name', 'image', 'street', 'city', 'reg_number', 'bank_name', 'uid', 'bic', 'iban', 'contract_number'], 'string', 'min' => 4, 'max' => 255],
            ['status', 'default', 'value'=>self::STATUS_ACTIVE],
            ['status', 'in', 'range'=>[self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['owners', 'safe'],
            ['image', 'default', 'value' => self::PATH_DEFAULT_LOGO],
        ];
    }

    /**
     * Upload file
     * @param $file string
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function saveImage($file): void
    {
        if ($this->isNewRecord) {
            $id = Company::find()->orderBy('id DESC')->one();
            if ($id === null) {
                $id = 1;
            } else {
                $id = $id->id + 1;
            }
        } else {
            $id = $this->id;
        }

        $path = Yii::getAlias('@backend') . '/web/img/company/' . $id . '/';
        $image = new UploadForm;
        $image->imageFile = UploadedFile::getInstance($this, $file);
        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                throw new NotFoundHttpException(sprintf('Directory "%s" was not created', $path));
            }
        }

        $image->imageFile->name = preg_replace('/[#$%^&*()+=\-\[\]\';,\/{}|":<>?~\\\\]/','_', $image->imageFile->name);

        if (!file_exists($path . $image->imageFile->name)) {
            if (!preg_match('/(png|jpg|jpeg|giff)$/ui', $image->imageFile->extension, $match)) {
                $this->addError($file, Yii::t('admin', 'Error format'));
            } else {
                $this->$file = $image->upload($path);
            }
            if (file_exists($path . $this->$file)) {
                if (preg_match('/\.(png|jpg|jpeg)$/ui', $this->$file, $match)) {
                    Image::thumbnail($path . $this->$file, 450, 450, ImageInterface::THUMBNAIL_FLAG_UPSCALE)
                        ->save($path . $this->$file, ['quality' => 50]);
                    $this->$file = '/img/company/' . $id . '/' . $this->$file;
                    if (isset($this->oldAttributes['image'])) {
                        $path = Yii::getAlias('@frontend') . '/web';
                        if ((file_exists($path . $this->oldAttributes['image']))) {
                            unlink(realpath($path . $this->oldAttributes['image']));
                        }
                    }
                }
            }
        } elseif (file_exists($path . $image->imageFile->name)) {
            $this->$file = '/img/company/' . $id . '/' . $image->imageFile->name;
        }
    }

    /**
     * Get company list
     * @return array
     */
    static public function getList():array
    {
        $list = [];
        $models = Company::find()->where(['status'=>Company::STATUS_ACTIVE])->all();
        if(!empty($models))
            foreach ($models as $model)
                $list[$model->id] = $model->name;

        return $list;
    }

    /**
     * Get company list agents
     * @param integer $id
     * @return array
     */
    public function getListAgents($id):array
    {
        $list = [];
        $models = Company::find()->where(['!=', 'id', $id])->all();
        if(!empty($models))
        {
            foreach ($models as $model) {
                $list[$model->id] = $model->name;
            }
        }
        return $list;
    }

    /**
     * Save company owners
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes):void
    {
        parent::afterSave($insert, $changedAttributes);
        if(empty($this->owners)) {
            $this->owners = [Yii::$app->user->id];
        }
        foreach ($this->owners as $user) {
            User::updateAll(['company_id'=>null], ['company_id'=>$this->id, 'company_role'=>User::COMPANY_ROLE_OWNER]);
            $model = User::findOne($user);
            if($model)
            {
                $model->company_id = $this->id;
                $model->company_role = User::COMPANY_ROLE_OWNER;
                $model->save();
            }
        }
    }

    /**
     * Get status list
     * @return array
     */
    public function getStatuses():array
    {
        return [
          self::STATUS_ACTIVE => Yii::t('company', 'Active'),
          self::STATUS_DELETED => Yii::t('company', 'Deleted'),
        ];
    }

    /**
     * Get Profile relation model
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(CompanyProfile::class, ['company_id'=>'id']);
    }

    public static function getName($id)
    {
        $res = Company::find()->where(['=', 'id', $id])->one();
        return $res->name;
    }


}
