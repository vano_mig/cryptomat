<?php

namespace backend\modules\user\models;

use Yii;

/**
 * Class CompanyReferal
 * @package backend\modules\user\models
 */
class CompanyReferal extends \common\models\CompanyReferal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'company_referral_id'], 'required'],
            [['company_id', 'company_referral_id', 'level'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('company', 'Company'),
            'company_referral_id' => Yii::t('company', 'Company Referal'),
            'level' => Yii::t('company', 'Level'),
        ];
    }

    /**
     * Get relative company model
     * @return yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(company::class, ['id'=>'company_referral_id']);
    }
}
