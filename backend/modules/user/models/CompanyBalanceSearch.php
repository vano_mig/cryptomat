<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompanyBalanceSearch represents the model behind the search form of `backend\modules\user\models\CompanyBalance`.
 */
class CompanyBalanceSearch extends CompanyBalance
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'quantity'], 'integer'],
            [['service', 'date', 'status', 'created_at'], 'safe'],
            [['cost', 'vat'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyBalance::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'service' => $this->service,
            'cost' => $this->cost,
            'quantity' => $this->quantity,
            'vat' => $this->vat,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andWhere(['company_id'=>$this->company_id]);
        }

        return $dataProvider;
    }
}
