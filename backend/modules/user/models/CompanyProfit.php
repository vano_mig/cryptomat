<?php

namespace backend\modules\user\models;

use backend\modules\api\models\Coinmarketcap;
use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_profit".
 *
 * @property int $id
 * @property int|null $terminal_id
 * @property int|null $widget_id
 * @property string|null $currency
 * @property float|null $amount
 * @property int|null $company_id
 * @property int|null $transaction_id
 * @property int|null $mode
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class CompanyProfit extends \yii\db\ActiveRecord
{
    public const STATUS_WAIT = 1;
    public const STATUS_SUCCESS = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_profit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['terminal_id', 'widget_id', 'company_id', 'transaction_id', 'mode', 'status'], 'integer'],
            [['amount'], 'number'],
            ['status', 'default', 'value'=>CompanyProfit::STATUS_SUCCESS],
            [['created_at', 'updated_at'], 'safe'],
            [['currency'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'terminal_id' => Yii::t('company', 'Terminal'),
            'widget_id' => Yii::t('api', 'Widget'),
            'currency' => Yii::t('admin', 'Currency'),
            'amount' => Yii::t('company', 'Amount'),
            'company_id' => Yii::t('company', 'Company'),
            'transaction_id' => Yii::t('api', 'Transaction ID'),
            'mode' => Yii::t('widget', 'Mode'),
            'status' => Yii::t('widget', 'Status'),
            'created_at' => Yii::t('admin', 'Created'),
            'updated_at' => Yii::t('admin', 'Updated'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
//                'value' => new Expression('NOW()'),
                'value' => (new Date())->now(),
            ]

        ];
    }

    public function getProfitCrypto($currency) {
        $model = CompanyProfit::find()->where(['currency'=>$currency, 'status'=>CompanyProfit::STATUS_SUCCESS]);
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $ids = [];
            $ids[] = Yii::$app->user->identity->company_id;
            $model->andWhere(['in', 'company_id', $ids]);
        }
        $list = $model->all();
        $amount = 0;
        $rate = (new Coinmarketcap())->getCurrencyRate($currency);
        if(!empty($list)) {
            foreach ($list as $item) {
                $amount += ($item->amount * $rate);
            }
        }
        return $amount;
    }
}
