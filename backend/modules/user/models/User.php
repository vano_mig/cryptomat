<?php

namespace backend\modules\user\models;

use backend\modules\admin\models\UploadForm;
use Yii;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class User
 * @package backend\modules\admin\models
 * @property UserProfile $userProfile
 *
 */
class User extends \common\models\User
{
    public function rules()
    {
        return [
            ['username', 'trim'],
            //      ['username', 'required'],
            //      ['username', 'string', 'min' => 1, 'max' => 255],

            ['last_name', 'trim'],

            ['birthdayD', 'trim'],
            //       ['birthdayD', 'required','message' => Yii::t('site', 'Day')],
            ['birthdayM', 'trim'],
            //       ['birthdayM', 'required','message' => Yii::t('site', 'Month')],
            ['birthdayY', 'trim'],
            ///        ['birthdayY', 'required','message' => Yii::t('site', 'Year')],

            ['birthday', 'trim'],
//            ['birthday', 'required'],
//            ['birthday', 'date', 'format'=>'yyy-mm-dd'],

            ['image', 'trim'],
            ['image', 'string'],
//            ['image', 'default', 'value' => self::PATH_DEFAULT_LOGO],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('users', 'This email address has already 
                been taken.'), 'on' => 'insert'],
            ['email', 'unique', 'filter' => ['!=', 'id', $this->id],
                'targetClass' => '\common\models\User',
                'message' => Yii::t('users', 'This email address has already 
                been taken.')],

            ['password_hash', 'trim'],
            ['password_hash', 'required',],
            ['password_hash', 'string', 'min' => 6],

            ['password_check', 'trim'],
            ['password_check', 'required', 'on' => ['pass', 'insert']],
            ['password_check', 'compare', 'compareAttribute' => 'password_hash', 'message' => Yii::t('users', 'Passwords do not match'), 'on' => ['pass', 'insert']],
            ['kyc', 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE,
                self::STATUS_INACTIVE, self::STATUS_SEMI_ACTIVE,  self::STATUS_DELETED]],

            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_CUSTOMER,
                self::ROLE_ACCOUNTANT, self::ROLE_MANAGER, self::ROLE_ADMIN, self::ROLE_FILLING, self::ROLE_SERVICE]],
            ['company_id', 'integer'],
            ['company_role', 'in', 'range' => [self::COMPANY_ROLE_FILLING, self::COMPANY_ROLE_SERVICE, self::COMPANY_ROLE_OWNER]]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'created_at' => Yii::t('admin', 'Created'),
            'updated_at' => Yii::t('admin', 'Updated'),
            'username' => Yii::t('users', 'First name'),
            'birthdayD' => Yii::t('site', 'Day'),
            'birthdayM' => Yii::t('site', 'Month'),
            'birthdayY' => Yii::t('site', 'Year'),
            'birthday' => Yii::t('site', 'Birthday'),
            'password_hash' => Yii::t('users', 'Password'),
            'email' => Yii::t('users', 'Email'),
//            'site' => Yii::t('users', 'Site'),
            'role' => Yii::t('users', 'Role'),
            'company_id' => Yii::t('users', 'Company'),
            'company_role' => Yii::t('users', 'Company role'),
            'status' => Yii::t('users', 'Status'),
            'image' => Yii::t('users', 'Photo'),
            'kyc' => Yii::t('users', 'KYC'),
        ];
    }

    /**
     * Get list all users
     * @param bool $username
     * if 'true' = returns username,
     * if 'false' = returns emails
     * @return array
     */
    public function getList($username = false): array
    {
        $result = [];
        $model = self::find()->all();
        if ($model && !$username) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['email'];
            }
        } elseif ($model && $username) {
            foreach ($model as $user) {
                $result[$user['id']] = $user['username'];
            }
        }
        return $result;
    }

    /**
     * Get list customers
     * @return array
     */
    public function getCustomersList(): array
    {
        $result = [];
        $model = self::find()->where(['role' => User::ROLE_CUSTOMER])->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['email'];
            }
        }
        return $result;
    }

    /**
     * Get list admin
     * @return array
     */
    public function getAdminList(): array
    {
        $result = [];
        $model = self::find()->where(['role' => User::ROLE_ADMIN])->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['email'];
            }
        }
        return $result;
    }

    /**
     * Get list customers
     * @return array
     */
    public function getLibraryUserList(): array
    {
        $result = [];
        $model = self::find()->where(['role' => [User::ROLE_CUSTOMER, User::ROLE_ADMIN]])->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['email'];
            }
        }
        return $result;
    }

    /**
     * Get list user roles
     * @return array
     */
    public function getRoles(): array
    {
        return [
            self::ROLE_USER => Yii::t('users', 'user'),
            self::ROLE_FILLING => Yii::t('users', 'filling'),
            self::ROLE_SERVICE => Yii::t('users', 'service'),
            self::ROLE_CUSTOMER => Yii::t('users', 'customer'),
            self::ROLE_ACCOUNTANT => Yii::t('users', 'accountant'),
            self::ROLE_MANAGER => Yii::t('users', 'manager'),
            self::ROLE_ADMIN => Yii::t('users', 'administrator'),
        ];
    }

    /**
     * Get list company roles
     * @return array
     */
    public function getCompanyRoles(): array
    {
        return [
            self::COMPANY_ROLE_FILLING => Yii::t('users', 'filling'),
            self::COMPANY_ROLE_SERVICE => Yii::t('users', 'service'),
            self::ROLE_CUSTOMER => Yii::t('users', 'owner'),
        ];
    }

    /**
     * Get list user statuses
     * @param null $id string
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('users', 'blocked'),
            self::STATUS_ACTIVE => Yii::t('users', 'active'),
            self::STATUS_SEMI_ACTIVE => Yii::t('users', 'semi active'),
            self::STATUS_DELETED => Yii::t('users', 'deleted')
        ];
        if ($id)
            return $array[$id];
        return $array;
    }

    /**
     * Get list user statuses
     * @param null $id string
     * @return array|mixed
     */
    public function getCompanyStatuses(): array
    {
        return [
            self::STATUS_INACTIVE => Yii::t('users', 'blocked'),
            self::STATUS_ACTIVE => Yii::t('users', 'active'),
        ];
    }

    /**
     * Upload file
     * @param $file string
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function saveImage($file): void
    {
        if ($this->isNewRecord) {
            $id = User::find()->orderBy('id DESC')->one();
            if ($id === null) {
                $id = 1;
            } else {
                $id = $id->id + 1;
            }
        } else {
            $id = $this->id;
        }

        $path = Yii::getAlias('@frontend') . '/web/img/logo/' . $id . '/';
        $image = new UploadForm;
        $image->imageFile = UploadedFile::getInstance($this, $file);
        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                throw new NotFoundHttpException(sprintf('Directory "%s" was not created', $path));
            }
        }
        if (!file_exists($path . $image->imageFile->name)) {
            if (!preg_match('/(png|jpg|jpeg|giff)$/ui', $image->imageFile->extension, $match)) {
                $this->addError($file, Yii::t('admin', 'Error format'));
            } else {
                $this->$file = $image->upload($path);
            }
            if (file_exists($path . $this->$file)) {
                if (preg_match('/\.(png|jpg|jpeg|giff)$/ui', $this->$file, $match)) {
                    Image::resize($path . $this->$file, 500, 500)->save($path . $this->$file, ['quality' => 50]);
                    $this->$file = '/img/logo/' . $id . '/' . $this->$file;
                    if (isset($this->oldAttributes['image'])) {
                        $path = Yii::getAlias('@frontend') . '/web';
                        if ((file_exists($path . $this->oldAttributes['image'])) & ($this->oldAttributes['image'] !== self::PATH_DEFAULT_LOGO)) {
                            unlink(realpath($path . $this->oldAttributes['image']));
                        }
                    }
                }
            }
        } elseif (file_exists($path . $image->imageFile->name)) {
            $this->$file = '/img/logo/' . $id . '/' . $image->imageFile->name;
        }
    }

    /**
     * Generate verification token
     * @return string
     * @throws \yii\base\Exception
     */
    public function verify_token()
    {
        $verification_token = Yii::$app->security->generateRandomString();
        if (User::find()->where(['verification_token' => $verification_token])->one()) {
            $this->verify_token();
        } else {
            return $verification_token;
        }
    }

    /**
     * After Save User
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes): bool
    {
        if ($insert) {
            $this->verification_token = $this->verify_token();
        }

        return parent::afterSave($insert, $changedAttributes);

    }

    /**
     * Get list clients
     * @return array
     */
    public function getClientList($id = null): array
    {
        $result = [];
        if($id) {
            $model = self::find()->where(['role' => User::ROLE_USER, 'agent_id'=>$id])->all();
        } else {
            $model = self::find()->where(['role' => User::ROLE_USER])->all();
        }
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['email'];
            }
        }
        return $result;
    }

    /**
     * Get current user email
     * @return string
     */
    public function getCurrentUserEMail()
    {
        $user = Yii::$app->user->id;

        $email = self::find()->where('id' == $user)->one();

        if ($email) {
            return $email->email;
        } else {
            return "E-mail not found";
        }
    }

    public function getCardList(): array
    {
        $result = [];
        if (Yii::$app->user->can(User::ROLE_ADMIN)) {
            $list = User::find()->where(['!=', 'role', User::ROLE_ADMIN])->andWhere(['status' => User::STATUS_ACTIVE])->all();
        } else {
            $list = User::find()->where(['!=', 'role', User::ROLE_ADMIN])->andWhere(['company_id' => Yii::$app->user->identity->company_id, 'status' => User::STATUS_ACTIVE])->all();
        }
        if (!empty($list)) {
            foreach ($list as $item) {
                $result[$item->id] = $item->email;
            }
        }
        return $result;
    }

    /**
     * Get user list to sms
     * @return array
     */
    public function getUserList(): array
    {
        $result = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = self::find()->where(['status'=>User::STATUS_ACTIVE])->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();
        } else {
            $list = Yii::$app->user->identity->company->getSubAgents();
            $model = self::find()->where(['role'=>User::ROLE_USER, 'agent_id'=>Yii::$app->user->id, 'status'=>User::STATUS_ACTIVE])->all();
            $model = array_merge($list, $model);
        }
        if (!empty($model)) {
            foreach ($model as $lang) {
                if($lang->role == User::ROLE_USER) {
                    $result[$lang->id] = $lang->userProfile->phone_number.' '. $lang->username.' '.$lang->last_name;
                } else {
                    $result[$lang->id] = $lang->userProfile->phone_number.' '. $lang->company->name.' '.$lang->username.' '.$lang->last_name;
                }
            }
        }
        return $result;
    }

    /**
     * Get user list to email
     * @return array
     */
    public function getUserListToEmail(): array
    {
        $result = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = self::find()->where(['status'=>User::STATUS_ACTIVE])->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();
        } else {
            $list = Yii::$app->user->identity->company->getSubAgents();
            $model = self::find()->where(['role'=>User::ROLE_USER, 'agent_id'=>Yii::$app->user->id, 'status'=>User::STATUS_ACTIVE])->all();
            $model = array_merge($list, $model);
        }
        if (!empty($model)) {
            foreach ($model as $lang) {
                if($lang->role == User::ROLE_USER) {
                    $result[$lang->id] = $lang->email.' '. $lang->username.' '.$lang->last_name;
                } else {
                    $result[$lang->id] = $lang->email.' '. $lang->company->name.' '.$lang->username.' '.$lang->last_name;
                }
            }
        }
        return $result;
    }

    public function getPassport()
    {
        return $this->hasMany(UserPassport::class, ['user_id'=>'id']);
    }

    /**
     * Get list user KYC statuses
     * @return array
     */
    public function getKycStatuses(): array
    {
        return [
            self::KYS_BLOCKED => Yii::t('users', 'blocked'),
            self::KYC_ACTIVE => Yii::t('users', 'active'),
        ];
    }
}