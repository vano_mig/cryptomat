<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompanySearch represents the model behind the search form of `backend\modules\admin\models\Country`.
 */
class CompanySearch extends Company
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['zip_code', 'country_id'], 'integer'],
            [['vat_number', 'status'], 'string', 'max' => 50],
            [['name', 'image', 'street', 'city', 'reg_number', 'bank_name', 'bic', 'iban', 'contract_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Company::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
//        echo "<pre>";
//        var_dump($dataProvider);
//        die;
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (Yii::$app->user->identity->role !== User::ROLE_ADMIN) {
            $query->andWhere(['id' => Yii::$app->user->identity->company_id]);
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'city', $this->city]);
        $query->andFilterWhere(['like', 'vat_number', $this->vat_number]);
        $query->andFilterWhere(['like', 'reg_number', $this->reg_number]);
        $query->andFilterWhere(['country_id'=> $this->country_id]);
        $query->andFilterWhere(['status'=> $this->status]);

        $query->orderBy(['id' => SORT_DESC]);
        return $dataProvider;
    }
}
