<?php

namespace backend\modules\user\models;

use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string $type
 * @property string|null $name
 * @property float|null $cost
 * @property float|null $cost_rf_id
 * @property float|null $cost_rf_id_metal
 * @property string|null $cost_advert
 * @property string|null $cost_email
 * @property string|null $status
 * @property string|null $created_at
 */
class Package extends \yii\db\ActiveRecord
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name', 'cost', 'cost_rf_id', 'cost_rf_id_metal', 'cost_advert', 'cost_email'], 'required'],
            [['type'], 'string',],
            [['type'], 'string', 'max'=>50],
            [['cost', 'cost_rf_id', 'cost_rf_id_metal', 'cost_advert', 'cost_email' ], 'formatNumber'],
            ['status', 'default', 'value'=>self::STATUS_ACTIVE],
            ['status', 'in', 'range'=>[self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * Validate $attribute to decimal
     * @param string $attribute
     */
    public function formatNumber($attribute)
    {
        $this->$attribute = (float)preg_replace('/,/', '.', $this->$attribute);
        $this->$attribute = number_format($this->$attribute, 2, '.', '');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'type' => Yii::t('admin', 'Type'),
            'name' => Yii::t('users', 'Name'),
            'cost' => Yii::t('users', 'Cost'),
            'cost_rf_id' => Yii::t('users', 'Cost RFID'),
            'cost_rf_id_metal' => Yii::t('users', 'Cost RFID metal'),
            'cost_advert' => Yii::t('admin', 'Cost advert'),
            'cost_email' => Yii::t('admin', 'Cost email'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * Get statuses list
     * @return array
     */
    public function getStatuses():array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('users', 'Active'),
            self::STATUS_DELETED => Yii::t('users', 'Deleted'),
        ];
    }

    /**
     * Get packages list
     * @return array
     */
    public function getList():array
    {
        $result = [];
        $list = Package::find()->where(['status'=>Package::STATUS_ACTIVE])->all();
        if(!empty($list)) {
            foreach ($list as $item) {
                $result[$item->id] = $item->name;
            }
        }
        return $result;
    }
}
