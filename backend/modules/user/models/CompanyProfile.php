<?php

namespace backend\modules\user\models;

use Yii;

/**
 * This is the model class for table "company_profile".
 *
 * @property int $id
 * @property int|null $company_id
 * @property int|null $platform_id
 * @property string|null $public_key
 * @property string|null $private_key
 * @property string|null $token
 * @property string|null $twilio_sid
 * @property string|null $coinmarketcap
 * @property string|null $twilio_token
 * @property string|null $twilio_from_number
 */
class CompanyProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['public_key', 'private_key', 'twilio_sid', 'platform_id', 'twilio_token', 'twilio_from_number'], 'required'],
            ['company_id', 'required', 'on'=>'profile'],
            [['company_id', 'platform_id'], 'integer'],
            [['public_key', 'private_key', 'token', 'twilio_sid', 'twilio_token', 'twilio_from_number', 'kys_url', 'kys_client_id', 'kys_secret_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('user', 'Company'),
            'platform_id'=>Yii::t('user', 'Platform'),
            'coin_public_key' => Yii::t('user', 'Public Key'),
            'coin_private_key' => Yii::t('user', 'Private Key'),
            'token' => Yii::t('user', 'Token'),
            'twilio_sid' => Yii::t('user', 'Twilio Sid'),
            'coinmarketcap' => Yii::t('user', 'Coinmarketcap'),
            'twilio_token' => Yii::t('user', 'Twilio Token'),
            'twilio_from_number' => Yii::t('user', 'Twilio From Number'),
            'kys_url' => Yii::t('user', 'KYC url'),
            'kys_client_id' => Yii::t('user', 'KYC client'),
            'kys_secret_key' => Yii::t('user', 'KYC secret key'),
        ];
    }
}
