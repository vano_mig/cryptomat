<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\CompanyProfit;

/**
 * CompanyProfitSearch represents the model behind the search form of `backend\modules\user\models\CompanyProfit`.
 */
class CompanyProfitSearch extends CompanyProfit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'terminal_id', 'widget_id', 'company_id', 'transaction_id', 'mode'], 'integer'],
            [['currency', 'created_at', 'updated_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyProfit::find()->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'terminal_id' => $this->terminal_id,
            'widget_id' => $this->widget_id,
            'amount' => $this->amount,
            'company_id' => $this->company_id,
            'transaction_id' => $this->transaction_id,
            'mode' => $this->mode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $ids = [];
            $ids[] = Yii::$app->user->identity->company_id;
            $query->andWhere(['in', 'company_id', $ids]);
            $query->andWhere([ 'status'=>CompanyProfit::STATUS_SUCCESS]);
        }

        $query->andFilterWhere(['like', 'currency', $this->currency]);

        return $dataProvider;
    }
}
