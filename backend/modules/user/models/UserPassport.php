<?php

namespace backend\modules\user\models;

use Yii;
use yii\web\NotFoundHttpException;

class UserPassport extends \common\models\UserPassport
{
    public function rules()
    {
        return [
            [['user_id', 'file'], 'required'],
            [['user_id'], 'integer'],
            [['file'], 'string'],
        ];
    }

    public function saveImages($object):bool
    {
        $path = Yii::getAlias('@frontend') . '/web/passport/'.Yii::$app->user->id.'/';
        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                throw new NotFoundHttpException(sprintf('Directory "%s" was not created', $path));
            }
        }
        if($object->upload($path)) {
            foreach ($object->imageFile as $image) {
                $model = new \frontend\models\UserPassport();
                $model->user_id = Yii::$app->user->id;
                $model->file = '/passport/'. $model->user_id . '/' .$image->name;

                if(!$model->validate()) {
                    echo "<pre>";print_r($model);die;
                }
                $model->save();
            }
            return true;
        } else {
            return false;
        }
    }
}