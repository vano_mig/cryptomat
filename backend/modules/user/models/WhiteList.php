<?php

namespace backend\modules\user\models;

use backend\modules\virtual_terminal\models\Atm;
use Yii;

/**
 * This is the model class for table "white_list".
 *
 * @property int $id
 * @property int|null $company_id
 * @property string|null $ip_address
 * @property int|null $atm_id
 */
class WhiteList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'white_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['company_id', 'required'],
            [['company_id', 'atm_id'], 'integer'],
            [['ip_address'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('фвьшт', 'ID'),
            'company_id' => Yii::t('white_list', 'Company'),
            'ip_address' => Yii::t('white_list', 'Ip Address'),
            'atm_id' => Yii::t('white_list', 'Atm'),
        ];
    }

    public function getAtm()
    {
        return $this->hasOne(Atm::class, ['id'=>'atm_id']);
    }
}
