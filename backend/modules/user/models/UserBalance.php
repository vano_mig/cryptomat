<?php

namespace backend\modules\user\models;

use common\components\Date;
use backend\modules\product\models\UserProduct;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_balance".
 *
 * @property int $id
 * @property int|null $company_id
 * @property int|null $user_id
 * @property float|null $cost
 * @property string|null $description
 * @property string|null $status
 * @property string|null $type
 * @property string|null $transaction_id
 * @property ActiveQuery $user
 * @property ActiveQuery $company
 * @property array $statuses
 * @property array $types
 * @property string|null $created_at
 */
class UserBalance extends \yii\db\ActiveRecord
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_PAID = 'paid';

    public const TYPE_RETURN_PACK = 'return_pack';
    public const TYPE_PAYMENT_DEFERRED = 'deferred_payment';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id', 'cost', 'description'], 'required'],
            [['company_id', 'user_id'], 'integer'],
            [['created_at', 'transaction_id'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_PAID, self::STATUS_ACTIVE]],
            ['type', 'default', 'value' => self::TYPE_RETURN_PACK],
            ['type', 'in', 'range' => [self::TYPE_RETURN_PACK, self::TYPE_PAYMENT_DEFERRED]],
            [['description'], 'string'],
            [['cost'], 'formatNumber'],
        ];
    }

    /**
     * Validate $attribute to decimal
     * @param string $attribute
     */
    public function formatNumber($attribute)
    {
        $this->$attribute = (float)preg_replace('/,/', '.', $this->$attribute);
        $this->$attribute = number_format($this->$attribute, 2, '.', '');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('company', 'Company'),
            'user_id' => Yii::t('company', 'User'),
            'cost' => Yii::t('company', 'Cost'),
            'description' => Yii::t('company', 'Message'),
            'status' => Yii::t('company', 'Status'),
            'transaction_id' => Yii::t('company', 'Invoice'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * Get status list
     * @return array
     */
    public function getStatuses():array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('company', 'Active'),
            self::STATUS_PAID => Yii::t('company', 'Paid'),
        ];
    }

    /**
     * Get Statuses list
     * @return array
     */
    public function getTypes(): array
    {
        return [
            self::TYPE_RETURN_PACK => Yii::t('company', 'Return pack'),
            self::TYPE_PAYMENT_DEFERRED => Yii::t('company', 'Deferred payment'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => (new Date())->now(),
            ]
        ];
    }

    /**
     * Get relative Company model
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Get relative User model
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(UserProduct::class, ['id'=>'description']);
    }
}
