<?php


namespace backend\modules\user\models;

/**
 * Class UserProfile
 * @package backend\modules\admin\models
 * @property User $user
 */
class UserProfile extends \common\models\UserProfile
{


    public function rules()
    {
        return [
            ['user_id', 'trim'],
            ['user_id', 'integer'],

            ['first_name_card', 'trim'],
            ['first_name_card', 'string', 'min' => 1, 'max' => 255],

            ['last_name_card', 'trim'],
            ['last_name_card', 'string', 'min' => 1, 'max' => 255],

            ['phone_number', 'trim'],
            ['phone_number', 'string', 'min' => 2, 'max' => 255],

            ['country', 'trim'],
            ['country', 'string', 'min' => 1, 'max' => 255],

            ['city', 'trim'],
            ['city', 'string', 'min' => 1, 'max' => 255],

            ['street_house', 'trim'],
            ['street_house', 'string', 'min' => 1, 'max' => 255],

            ['post_code', 'trim'],
            ['post_code', 'string', 'min' => 1, 'max' => 255],

            ['ref_card_no', 'trim'],
            ['ref_card_no', 'string', 'min' => 1, 'max' => 20],

            ['ccno', 'trim'],
            ['ccno', 'string', 'min' => 1, 'max' => 255],

            ['card_type', 'trim'],
            ['card_type', 'string', 'min' => 1, 'max' => 255],

            ['expiry_date', 'trim'],
            ['expiry_date', 'string', 'min' => 1, 'max' => 255],

            ['client_from', 'trim'],
            ['client_from', 'string', 'min' => 1, 'max' => 255],

            ['mailing', 'trim'],
            ['mailing', 'boolean'],

            ['reg_person', 'safe'],

            ['card_type', 'safe'],

        ];
    }


}