<?php

namespace backend\modules\user\models;

use common\components\Date;
use backend\modules\admin\models\Transaction;
use backend\modules\administration\controllers\SystemConfigController;
use backend\modules\administration\models\Configuration;
use kartik\mpdf\Pdf;
use Mpdf\Tag\U;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_balance".
 *
 * @property int $id
 * @property string $transaction
 * @property int|null $company_id
 * @property string|null $service
 * @property float|null $cost
 * @property int|null $quantity
 * @property float|null $vat
 * @property string|null $message
 * @property string|null $date
 * @property string|null $status
 * @property string|null $invoice
 * @property string|null $created_at
 */
class CompanyBalance extends \yii\db\ActiveRecord
{
    public const SERVICE_PACKAGE = 'package';
    public const SERVICE_RFID = 'rf_id';
    public const SERVICE_ADVERT = 'advert';
    public const SERVICE_PRODUCT = 'product';
    public const SERVICE_EMAIL = 'email';
    public const SERVICE_SMS = 'sms';
    public const SERVICE_WEEKLY_INVOICE = 'weekly';

    public const STATUS_ACTIVE = 'active';
    public const STATUS_PAID = 'paid';
    public const STATUS_CLOSED = 'closed';
    public const STATUS_WAIT = 'wait';


    public const STATUS_EXPIRED = 'expired paid';
    public const VAT = 19.00;



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'quantity', 'cost', 'service', 'message'], 'required'],
            [['company_id', 'quantity'], 'integer'],
            [['date', 'created_at', 'invoice', 'transaction'], 'safe'],
            ['vat', 'default', 'value' => self::VAT],
            ['service', 'in', 'range' => [self::SERVICE_SMS, self::SERVICE_PRODUCT, self::SERVICE_ADVERT, self::SERVICE_RFID, self::SERVICE_PACKAGE, self::SERVICE_EMAIL, self::SERVICE_WEEKLY_INVOICE]],
            ['status', 'default', 'value' => self::STATUS_WAIT],
            ['status', 'in', 'range' => [self::STATUS_PAID, self::STATUS_ACTIVE, self::STATUS_CLOSED, self::STATUS_WAIT, self::STATUS_EXPIRED]],
            [['message'], 'string'],
            [['cost'], 'formatNumber'],
        ];
    }

    /**
     * Validate $attribute to decimal
     * @param string $attribute
     */
    public function formatNumber($attribute)
    {
        $this->$attribute = (float)preg_replace('/,/', '.', $this->$attribute);
        $this->$attribute = number_format($this->$attribute, 2, '.', '');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('company', 'Company'),
            'service' => Yii::t('company', 'Type of service'),
            'cost' => Yii::t('company', 'Cost per one piece, €'),
            'quantity' => Yii::t('company', 'Quantity, pieces'),
            'vat' => Yii::t('company', 'VAT, %'),
            'transaction' => Yii::t('finance', 'Transaction'),
            'message' => Yii::t('company', 'Order description'),
            'date' => Yii::t('company', 'Order completion date'),
            'status' => Yii::t('company', 'Status'),
            'invoice' => Yii::t('company', 'Invoice'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * Get services list
     * @return array
     */
    public function getServices(): array
    {
        return [
            self::SERVICE_PACKAGE => Yii::t('company', 'Package'),
            self::SERVICE_RFID => Yii::t('company', 'RFID'),
            self::SERVICE_ADVERT => Yii::t('company', 'Advert'),
            self::SERVICE_PRODUCT => Yii::t('company', 'Product'),
            self::SERVICE_SMS => Yii::t('company', 'Sms'),
            self::SERVICE_EMAIL => Yii::t('company', 'Email'),
            self::SERVICE_WEEKLY_INVOICE => Yii::t('company', 'Weekly invoice')
        ];
    }

    /**
     * Get Statuses list
     * @return array
     */
    public function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('company', 'Active'),
            self::STATUS_PAID => Yii::t('company', 'Paid'),
            self::STATUS_CLOSED => Yii::t('company', 'Closed'),
            self::STATUS_WAIT => Yii::t('company', 'Waiting invoice'),
            self::STATUS_EXPIRED => Yii::t('company', 'Expired paid'),
            //  self::SERVICE_EMAIL => Yii::t('company', 'Email')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => (new Date())->now(),
            ]
        ];
    }

    public function beforeSave($insert)
    {
        $time = $this->date;
        if(!empty($time)){
            $time = date('Y-m-d');
            $this->date = $time;
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Get relative Company model
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Generate invoice to company
     * @param $array
     * @return bool
     * @throws \Mpdf\MpdfException
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \yii\base\InvalidConfigException
     */
    public function generateInvoice($array): bool
    {
        $result = false;
        foreach ($array as $key => $item) {
            $header = $this->getPDFHeader();
            $companyMs = Configuration::find()->where(['name' => 'company_ms'])->one();
            $company = Company::findOne($companyMs->value);
            $agent = User::find()->where(['role' => User::ROLE_CUSTOMER, 'company_id' => $company->id])->one();
            $user = User::find()->where(['role' => User::ROLE_CUSTOMER, 'company_id' => $key])->one();
            $footer = $this->getPDFFooter($company, $agent);
            $time = mktime();
            $content = Yii::$app->view->renderFile(Yii::getAlias('@frontend') . '/views/pdf-template/_weekly_invoice_pdf.php', [
                'user' => $user,
                'transaction' => $item,
                'company' => $company,
                'time' => $time
            ]);
            $this->invoice = '/company-invoice/Invoice_#' . $time . '.pdf';
            $this->service = self::SERVICE_WEEKLY_INVOICE;
            $this->status = self::STATUS_ACTIVE;
            $this->company_id = $key;
            $this->cost = 0;
            $this->quantity = 0;
            foreach ($item as $data) {
                foreach ($data as $value) {
                    $this->cost += $value->cost;
                    $this->quantity += $value->quantity;
                }
            }

            $path = Yii::getAlias('@frontend') . '/web/company-invoice/Invoice_#';
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'marginBottom' => 25,
                'marginTop' => 30,
                'marginLeft' => 4,
                'marginRight' => 4,
                'cssFile' => Yii::getAlias('@frontend') . '/web/css/pdfStyle.css',
                'methods' => ['SetHeader' => $header, 'SetFooter' => $footer],
                'filename' => $path . $time . '.pdf',
            ]);
            $pdf->render();

            Yii::$app->mailer->compose(['html' => 'sendBill-html', 'text' => 'sendBill-text'],
                ['user' => $user])
                ->attach($path . $time . '.pdf', ['contentType' => 'application/pdf'])
                ->setFrom([Yii::$app->params['paymentEmail'] => Yii::$app->params['senderName']])
                ->setTo([$user->email, Yii::$app->params['ivanEmail']])
                ->setSubject('Invoice #' . $time)
                ->send();
            $this->message = Yii::t('company', 'Weekly invoice');
            if ($this->save()) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Get Header
     * @return string
     */
    private function getPDFHeader(): string
    {
        return Yii::$app->view->renderFile(Yii::getAlias('@frontend') . '/views/pdf-template/_header_pdf.php');
    }

    /**
     * Get Footer
     * @param $company
     * @param $user
     * @return string
     */
    private function getPDFFooter($company, $user): string
    {
        $footer = Yii::$app->view->renderFile(Yii::getAlias('@frontend') . '/views/pdf-template/_footer_pdf.php', [
            'company' => $company,
            'agent' => $user
        ]);

        return $footer;
    }

    public function getNumber()
    {
        return $this->hasOne(Transaction::class, ['id'=>'transaction']);
    }

}
