<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;

/**
 * Permission class.
 */
class Permission extends Model
{
    /**
     * Get list roles in system
     * @return yii\rbac\Role[]
     */
    public function getListRoles():array
    {
        $roles = Yii::$app->authManager->getRoles();
        return $roles;
    }

    /**
     * Get count permissions by role
     * @param $id string
     * @return integer
     */
    public function getCountPermissionsByRole($id):int
    {
        $permissions = Yii::$app->authManager->getPermissionsByRole($id);
        $count = 0;
        if (!empty($permissions)) {
            $count = count($permissions);
        }
        return $count;
    }

    /**
     * Get list permissions
     * @return yii\rbac\Permission[]
     */
    public function getPermissions():array
    {
        $all_permissions = Yii::$app->authManager->getPermissions();
        $names = [];
        foreach ($all_permissions as $one_permission) {
            $name_array = explode('.', $one_permission->name);
            $names[$name_array[0]][] = $one_permission;
        }
        return $names;
    }

    /**
     * Get list permissions by role
     * @param $id string
     * @return array
     */
    public function getPermissionsByRole($id): array
    {
        $list = [];
        $permissions = Yii::$app->authManager->getPermissionsByRole($id);
        if (!empty ($permissions)) {
            foreach ($permissions as $permission) {
                $list[$permission->name] = $permission->description;
            }
        }
        return $list;
    }
}
