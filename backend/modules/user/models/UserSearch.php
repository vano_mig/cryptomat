<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `backend\modules\user\models\UserSearch`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id', 'status', 'kyc'], 'integer'],
            ['id', 'trim',],
            ['email', 'trim',],
            ['username', 'trim',],
            [['username', 'email', 'role', 'created_at', 'updated_at', 'title', 'lang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => 'id DESC'],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'username',
                'email',
                'status',
                'role',
                'created_at',
                'updated_at',

            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'role'=>$this->role,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andWhere(['id'=>Yii::$app->user->id]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);
        return $dataProvider;
    }

    public function searchKyc()
    {
        $model = User::find();
        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
        $model->andWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }
        $model->andWhere(['!=','role', User::ROLE_ADMIN]);
        $model->andWhere(['status'=>User::STATUS_ACTIVE]);
        $model->andWhere(['kyc'=>User::KYS_BLOCKED]);

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            //'sort'=> ['defaultOrder' => 'id DESC'],
        ]);
        return $dataProvider;
    }
}
