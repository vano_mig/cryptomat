<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\UserSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\formatter\UserFormatter;
use backend\modules\user\models\User;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'KYC list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'KYC')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'username',
                            'value' => function ($data) {
                                return $data->username;
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => ''],
                        ],
                        [
                            'attribute' => 'email',
                            'value' => function ($data) {
                                return $data->email;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'role',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(
                                    UserFormatter::class)->asRole($data->role);
                            },
                            'format' => 'raw',
                            'filter' => $searchModel->getRoles(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('kyc.update', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
