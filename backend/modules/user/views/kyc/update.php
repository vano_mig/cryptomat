<?php

/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\User|null */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('admin', 'Update user: ') . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center mb-3"><?php echo Yii::t('admin', 'User info') ?></h4>
                    <div class="row col mx-auto max-wr-70">
                        <div class="row col-12 p-0 d-inline-flex align-items-center mb-sm-5">
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model, 'username')->textInput(['readonly' => true]) ?>
                            </div>
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model, 'last_name')->textInput(['readonly' => true]) ?>
                            </div>
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model, 'birthday')->textInput(['readonly' => true]) ?>
                            </div>
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model->userProfile, 'country')->textInput(['readonly' => true]) ?>
                            </div>
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model->userProfile, 'city')->textInput(['readonly' => true]) ?>
                            </div>
                            <div class="col min-wr-16 max-wr-30 mx-auto">
                                <?php echo $form->field($model->userProfile, 'street_house')->textInput(['readonly' => true]) ?>
                            </div>
                        </div>
                        <?php if ($model->passport): ?>
                            <div>
                                <?php foreach ($model->passport as $item): ?>
                                    <img src="<?= Url::to($item->file); ?>" class="mb-2">
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-12">
                        <?php echo $form->field($model, 'kyc')->dropDownList($model->getKycStatuses(), ['prompt' => Yii::t('users', 'Select KYC status')]) ?>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                    </div>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>