<?php

use backend\modules\admin\models\Equipment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Package */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="package-form">
    <div class="col mx-auto max-wr-40">
        <?= $form->field($model, 'type')->dropDownList((new Equipment())->getTypes(),['placeholder' => Yii::t('users', 'Select type')])?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input name')]) ?>

        <?= $form->field($model, 'cost')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input cost')]) ?>

        <?= $form->field($model, 'cost_rf_id')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input cost RFID')]) ?>
        <?= $form->field($model, 'cost_rf_id_metal')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input cost RFID metal')]) ?>
        <?= $form->field($model, 'cost_advert')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input cost advert')]) ?>
        <?= $form->field($model, 'cost_email')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('users', 'Input cost email')]) ?>

        <?= $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt'=>Yii::t('users', 'Select status')]) ?>

    </div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'),
                ['class' => 'btn bg-gradient-success btn-block']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
