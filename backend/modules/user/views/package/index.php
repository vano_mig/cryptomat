<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\formatter\CompanyFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-money-check-alt"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= Html::a(Yii::t('users', 'New package'), ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'id',
                    'type',
                    'name',
                    'cost',
                    'cost_rf_id',
                    'cost_rf_id_metal',
                    'cost_advert',
                    'cost_email',
                    [
                        'attribute' => 'status',
                        'value' => function ($data) {
                            return Yii::$app->formatter->load(CompanyFormatter::class)->asStatusPackage($data->status);
                        },
                        'format' => 'html',
                        'headerOptions' => ['class' => 'text-center my-tw-8'],
                        'contentOptions' => ['class' => 'text-center align-middle'],
                    ],
                    'created_at',
                    [
                        'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                        'header' => Yii::t('admin', 'Actions'),
                        'template' => '{view} {update} {delete} ',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="far fa-eye"></i>', $url, [
                                    'class' => 'mx-2 text-info',
                                    'title' => Yii::t('admin', 'View'),
                                    'aria-label' => Yii::t('admin', 'View'),
                                    'data-pjax' => Yii::t('admin', 'View'),
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                    'class' => 'mx-2 text-warning',
                                    'title' => Yii::t('admin', 'Update'),
                                    'aria-label' => Yii::t('admin', 'Update'),
                                    'data-pjax' => Yii::t('admin', 'Update'),
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                    'class' => 'mx-2 text-dark',
                                    'title' => Yii::t('admin', 'Delete'),
                                    'aria-label' => Yii::t('admin', 'Delete'),
                                    'data-pjax' => Yii::t('admin', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                        'method' => 'POST'
                                    ],
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'view' => function ($model, $key, $index) {
                                return Yii::$app->user->can('package.view', ['user' => $model]);
                            },
                            'update' => function ($model, $key, $index) {
                                return Yii::$app->user->can('package.update', ['user' => $model]);
                            },
                            'delete' => function ($model, $key, $index) {
                                return Yii::$app->user->can('package.delete', ['user' => $model]);
                            },
                        ],
                    ]
                ],
            ]); ?>
        </div>
    </div>
</div>
</div>