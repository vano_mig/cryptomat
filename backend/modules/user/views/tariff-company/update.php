<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyPackage */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('company_package', 'Update Company Package');
$this->params['breadcrumbs'][] = ['label' => Yii::t('company', 'Company Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('company', 'Update');
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-briefcase"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
