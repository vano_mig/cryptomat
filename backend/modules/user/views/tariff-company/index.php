<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\CompanyPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\Package;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('company_package', 'Company Packages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-briefcase"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= Html::a(Yii::t('users', 'New company package'), ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company ? $data->company->name : Yii::t('admin', 'not set');
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Company())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'package_id',
                            'value' => function ($data) {
                                return $data->package ? $data->package->name : Yii::t('admin', 'not set');
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Package())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asCompanyPackageStatus($data->status);
                            },
                            'filter' => $searchModel->getStatuses(),
                            'format' => 'html',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle']
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {delete} ',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('package.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('package.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('package.delete', ['user' => $model]);
                                },
                            ],
                        ]
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>