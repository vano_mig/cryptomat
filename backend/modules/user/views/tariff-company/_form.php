<?php

use backend\modules\user\models\Company;
use backend\modules\user\models\Package;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyPackage */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="company-package-form">
        <div class="col mx-auto max-wr-40">
            <?= $form->field($model, 'company_id')->dropDownList((new Company())->getList(), ['prompt' => Yii::t('company_package', 'Select company')]) ?>

            <?= $form->field($model, 'package_id')->dropDownList((new Package())->getList(), ['prompt' => Yii::t('company_package', 'Select package')]) ?>

            <?= $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => Yii::t('company_package', 'Select status')]) ?>
        </div>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'),
                    ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                    ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>