<?php

/* @var $birthdaySelect array */
/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null */
/* @var $company array|backend\modules\user\models\Company|null|yii\db\ActiveRecord */
/* @var $modelProfileUpdate array|backend\modules\user\models\UserProfile|null|yii\db\ActiveRecord */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'Update user: ') .' '. $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
                'modelProfileUpdate' => $modelProfileUpdate,
                'birthdaySelect'=>$birthdaySelect
            ]) ?>
        </div>
    </div>
</div>