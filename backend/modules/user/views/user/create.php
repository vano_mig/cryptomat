<?php

/* @var $birthdaySelect array */
/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */
/* @var $modelProfileUpdate backend\modules\user\models\UserProfile */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('users', 'Create user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
                'birthdaySelect' => $birthdaySelect,
                'modelProfileUpdate' => $modelProfileUpdate,
            ]) ?>
        </div>
    </div>
</div>