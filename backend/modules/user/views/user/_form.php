<?php

/* @var $birthdaySelect array */
/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null|yii\db\ActiveRecord */
/* @var $modelProfileUpdate array|backend\modules\user\models\UserProfile|null|yii\db\ActiveRecord */

use backend\modules\user\models\Company;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

?>


<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php if (Yii::$app->user->identity->role == $model::ROLE_ADMIN) { ?>
            <div class="row col-12 border-bottom mb-3">
                <div class="col min-wr-16 max-wr-30 mx-auto">
                    <?php echo $form->field($model, 'role')->dropDownList($model->getRoles(), ['prompt' => Yii::t('users', 'Select role')]) ?>
                    <?php echo $form->field($model, 'status')->
                    dropDownList($model->getStatuses(),
                        ['prompt' => Yii::t('users', 'Select status')]); ?>
                    <?= $form->field($modelProfileUpdate, 'mailing', ['options' => ['class' => 'my-4']])->checkbox()->label(Yii::t('adminUserForm', 'Mailing')); ?>
                </div>
                <?php if ($model->role != $model::ROLE_USER): ?>
                    <div class="col min-wr-16 max-wr-30 mx-auto">
                        <?php echo $form->field($model, 'company_id')->dropDownList((new Company())->getList(), ['prompt' => Yii::t('users', 'Select company')]) ?>
                    </div>
                <?php endif ?>
            </div>
        <?php } ?>
        <div class="col-12">
            <h4 class="text-center mb-3"><?php echo Yii::t('admin', 'User info') ?></h4>
            <div class="row col mx-auto max-wr-70">
                <div class="row col-12">
                    <div class="col d-inline-flex mx-auto min-wr-16 max-wr-30">
                        <?php echo Html::img(Url::to($model->image), ['class' => 'mr-2 max-wr-8']) ?>
                        <?php echo $form->field($model, 'image', ['options' => ['class' => 'my-auto custom-file']])->fileInput(['class' => 'custom-file-input', 'id' => 'customFile'])->label(Yii::t('users', 'Photo'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                    </div>
                </div>
                <div class="row col-12 p-0 d-inline-flex align-items-center mb-sm-5">
                    <div class="col min-wr-16 max-wr-30 mx-auto">
                        <?php echo $form->field($model, 'email')->textInput()->label(Yii::t('users', 'Email (Login)')) ?>
                    </div>
                    <div class="col min-wr-16 max-wr-30 mx-auto">
                        <?php echo $form->field($model, 'password_hash', ['template' => "{label}\n{hint}\n{input}<div class='invalid-feedback'></div>", 'options' => ['class' => 'form-group mb-0']])->passwordInput() ?>
                        <?php echo $form->field($model, 'password_check')->passwordInput(['value' => $model->password_hash])->label(false) ?>
                    </div>
                </div>
                <div class="col min-wr-16 max-wr-30 mx-auto">
                    <?php echo $form->field($modelProfileUpdate, 'phone_number')->textInput() ?>
                    <?php echo $form->field($model, 'username')->textInput() ?>
                    <?php echo $form->field($model, 'last_name')->textInput() ?>

                    <label><?= Yii::t('site', 'Birthday') ?></label>
                    <div class="col d-inline-flex mb-3">
                        <?= $form->field($model, 'birthdayD', ['options' => ['style' => 'width:22%; margin-left:4%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionD'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionDCss']])->label(false); ?>
                        <?= $form->field($model, 'birthdayM', ['options' => ['style' => 'width:28%; margin-left:3%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionM'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionMCss']])->label(false); ?>
                        <?= $form->field($model, 'birthdayY', ['options' => ['style' => 'width:32%; margin-left:3%; position: relative;']])->dropDownList($birthdaySelect['optionY'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionYCss']])->label(false); ?>
                    </div>
                </div>
                <div class="col min-wr-16 max-wr-30 mx-auto my-auto">
                    <?php echo $form->field($modelProfileUpdate, 'country')->textInput(); ?>
                    <?php echo $form->field($modelProfileUpdate, 'city')->textInput(); ?>
                    <?php echo $form->field($modelProfileUpdate, 'street_house')->textInput(); ?>
                    <?php echo $form->field($modelProfileUpdate, 'post_code')->textInput(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php


$script = "
$( document ).ready(function() {
    $('.custom-file-input').on('change', function() {
        var fileName = $(this).val().split('\\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);   
    });
});
";
$this->registerJs($script, yii\web\View::POS_END);



