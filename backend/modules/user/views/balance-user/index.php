<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\formatter\UserBalanceFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\UserBalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user_balance', 'User Balances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-balance-scale nav-icon"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'cost',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(UserBalanceFormatter::class)->asUser($data->user_id);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'user_id',
                                (new User())->getLibraryUserList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible'=>Yii::$app->user->can(User::ROLE_ADMIN),
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company->name;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Company())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'type',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(UserBalanceFormatter::class)->asType($data->type);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'type', $searchModel->getTypes(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(UserBalanceFormatter::class)->asStatusBalance($data->status);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getStatuses(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'transaction_id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('user_balance.view', ['user' => $model]);
                                },


                            ],
                        ]
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>