<?php

use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\UserBalance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-balance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_id')->dropDownList((new Company())->getList(),
        ['prompt'=>Yii::t('user_balance', 'Select company')]) ?>

    <?= $form->field($model, 'user_id')->dropDownList((new User())->getList(),
        ['prompt'=>Yii::t('user_balance', 'Select user')]) ?>


    <?= $form->field($model, 'cost')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('user_balance','Input cost')]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder'=>Yii::t('user_balance','Input description')]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('user_balance','Input status')]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('user_balance','Input type')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user_balance', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
