<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\formatter\UserBalanceFormatter;
use backend\modules\user\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\UserBalance */

$this->title = Yii::t('user_balance', 'View user balance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_balance', 'User Balances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content-header">
    <?php echo Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i
                        class="fas fa-balance-scale nav-icon"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'cost',
                            [

                                'attribute' => 'description',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(UserBalanceFormatter::class)->asMessage($data);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'type',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(UserBalanceFormatter::class)->asType($data->type);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'transaction_id',
                                'value' => function ($data) {
                                    return $data->transaction_id ? $data->transaction_id : Yii::t('admin', 'not set');
                                },
                            ],
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return $data->company->name;
                                },
                            ],
                            [
                                'attribute' => 'user_id',
                                'value' => function ($data) {
                                    return $data->user ? $data->user->email : Yii::t('admin', 'not set');
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(UserBalanceFormatter::class)->asStatusBalance($data->status);
                                },
                                'format' => 'html',
                            ],
                            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('user_balance.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
