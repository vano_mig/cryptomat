<?php

use backend\models\User;
use backend\modules\admin\formatter\TransactionFormatter;
use backend\modules\user\models\Company;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyProfit */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('company', 'Company Profits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto col-8">
                <div class="table-responsive">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'terminal_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTerminal($data->terminal_id);
                                },
                                'format' => 'raw',
                            ],
                            'currency',
                            'amount',
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return Company::getName($data->company_id);                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'transaction_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTransaction($data->transaction_id);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'trnasaction_id',
                                'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Company::getName($data->company_id);
                                },
                                'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
                            ],
//                            [
//                                'attribute' => 'mode',
//                                'value' => function ($data) {
//                                    return Yii::$app->formatter->load(WidgetFormatter::class)->asMode($data->mode);
//                                },
//                                'format' => 'raw',
//                            ],
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('transaction.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('admin', 'List'), ['index', 'id' => $model->id], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>