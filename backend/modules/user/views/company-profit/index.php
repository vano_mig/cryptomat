<?php

use backend\models\User;
use backend\modules\admin\formatter\TransactionFormatter;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\user\models\Company;
use backend\modules\user\models\CompanyProfit;
use backend\modules\virtual_terminal\models\Atm;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\CompanyProfitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $listCurrency array */

$this->title = Yii::t('company', 'Company Profits');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="row bg-primary">
                <div class="col-12 text-center mt-4 mb-4"><?php echo strtoupper(Yii::t('company', 'Total profit'))?></div>
                <?php foreach ($listCurrency as $currency): ?>
                    <div class="col-2">
                        <div class="text-center font-bold"><?php echo $currency ?></div>
                        <div class="text-center mb-4"><?php echo (new CompanyProfit())->getProfitCrypto($currency) ?></div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'terminal_id',
                        'value' => function ($data) {
                            return Yii::$app->formatter->load(TransactionFormatter::class)->asTerminal($data->terminal_id);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'terminal_id',
                            (new Atm())->getList(),
                            ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        'format' => 'raw',
                    ],
                    'amount',
                    [
                        'attribute' => 'currency',
                        'filter' => Html::activeDropDownList($searchModel, 'currency',
                            (new CryptoCurrency())->getListCodeFull(),
                            ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'company_id',
                        'value' => function ($data) {
                            return Company::getName($data->company_id);
                        },
                        'filter' => Html::activeDropDownList($searchModel, 'company_id',
                            Company::getList(),
                            ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        'format' => 'raw',
                        'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'attribute' => 'transaction_id',
                        'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'attribute' => 'status',
//                        'value' => function ($data) {
//                            return Company::getName($data->company_id);
//                        },
//                        'filter' => Html::activeDropDownList($searchModel, 'company_id',
//                            Company::getList(),
//                            ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        'format' => 'raw',
                        'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
                    ],

//                    [
//                        'attribute' => 'mode',
//                        'value' => function ($data) {
//                            return Yii::$app->formatter->load(WidgetFormatter::class)->asMode($data->mode);
//                        },
//                        'filter' => Html::activeDropDownList($searchModel, 'mode',
//                            (new Widget())->getModes(),
//                            ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
//                        'format' => 'raw',
//                    ],
                    'created_at',
                    ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $models, $key) {
                                return Html::a('<i class="far fa-eye"></i>', $url, [
                                    'class' => 'mx-2 text-info',
                                    'title' => Yii::t('admin', 'View'),
                                    'aria-label' => Yii::t('admin', 'View'),
                                    'data-pjax' => Yii::t('admin', 'View'),
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'view' => function ($models, $key, $index) {
                                return Yii::$app->user->can('transaction.view', ['user' => $models]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>