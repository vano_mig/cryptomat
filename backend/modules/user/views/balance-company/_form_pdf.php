<?php

use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\CompanyBalance;

/**
 * @var $model CompanyBalance
 *
 */
/* @var $this \yii\web\View */
?>

<table>
    <tr>
        <td style="width: 80%; position: center;">
            <?php echo yii\widgets\DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'transaction',
                    [
                        'attribute' => 'service',
                        'value' => function ($data) {
                            return Yii::$app->formatter->load(CompanyFormatter::class)->asService($data->service);
                        },
                        'format' => 'html',
                    ],
                    'cost',
                    'quantity',
                    'vat',
                    'message',
                    [
                        'attribute' => 'status',
                        'value' => function ($data) {
                            return Yii::$app->formatter->load(CompanyFormatter::class)->asStatusBalance($data->status);
                        },
                        'format' => 'html',
                    ],
                    'created_at',
                    'date',
                ],
                'options' => [
                    'style' => 'width: 600px;'
                ],

            ]); ?>
        </td>
    </tr>
</table>
