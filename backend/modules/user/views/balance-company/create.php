<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyBalance */

$this->title = Yii::t('company', 'Create Company Invoice');
$this->params['breadcrumbs'][] = ['label' => Yii::t('company', 'Company Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> <?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>