<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyBalance */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('company', 'Company Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
            <div class="row  col-lg-4 mx-auto">
                <?php if (Yii::$app->user->can('company.update')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'View as .pdf-file'),
                            Url::toRoute(['view-pdf', 'id' => $model->id]),
                            ['class' => 'btn bg-gradient-info btn-sm']) ?>
                    </div>
                    <?php if (Yii::$app->user->can('company.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Send as .pdf-file'),
                                Url::toRoute(['send-pdf', 'invoice_id' => $model->id]),
                                ['class' => 'btn bg-gradient-success btn-sm']) ?>
                        </div>
                    <?php endif ?>
                <?php endif ?>

        </div><br>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'company',
                                'value' => function ($data) {
                                    return $data->company->name;
                                },
                            ],
                            [
                                'attribute' => 'service',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(CompanyFormatter::class)->asService($data->service);
                                },
                                'format' => 'html',
                            ],
                            'cost',
                            'vat',
                            'quantity',
                            [
                                'attribute' => 'transaction',
                                'value'=>function($data) {
                                    return $data->number ? $data->number->res_file_ref : Yii::t('admin', 'not set');
                                },
                            ],
                            [
                                'attribute' => 'message',
                                'format'=>'raw'
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(CompanyFormatter::class)->asStatusBalance($data->status);
                                },
                                'format'=>'html',
                            ],
//                            [
//                                'attribute' => 'created_at',
//                                'value' => function ($data) {
//                                    if(!empty($data->created_at)){
//                                        $date = strtotime($data->created_at);
//                                        return date('H:i:s _ d.m.yy', $date);
//                                    } else {
//                                        return Yii::t('error', 'Date was not set');
//                                    }
//                                },
//                            ],
//                            [
//                                'attribute' => 'date',
//                                'value' => function ($data) {
//                                    if(!empty($data->date)){
//                                        $date = strtotime($data->date);
//                                        return date('H:i:s d.m.y', $date);
//                                    } else {
//                                        return Yii::t('error', 'Date was not set');
//                                    }
//                                },
//                            ],
                            'date',
                            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'),
                                Url::toRoute(['update', 'id' => $model->id]),
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('company.balance')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>