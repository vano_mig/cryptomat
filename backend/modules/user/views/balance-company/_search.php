<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyBalanceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-balance-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>
    <?= $form->field($model, 'transaction') ?>

    <?= $form->field($model, 'company_id') ?>

    <?= $form->field($model, 'service') ?>

    <?= $form->field($model, 'cost') ?>

    <?= $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'vat') ?>

    <?php // echo $form->field($model, 'message') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('company', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('company', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
