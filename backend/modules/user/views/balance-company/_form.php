<?php

use backend\modules\user\models\Company;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\CompanyBalance */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="col mx-auto max-wr-40">


    <?= $form->field($model, 'company_id')->dropDownList(Company::getList(),
        ['prompt'=>Yii::t('company', 'Select company')]) ?>

    <?= $form->field($model, 'service')->dropDownList($model->getServices(),
        ['prompt'=>Yii::t('company', 'Select service type')]) ?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('category', 'Input cost')]) ?>

    <?= $form->field($model, 'quantity')->textInput(['placeholder'=>Yii::t('category', 'Input quantity')]) ?>

    <?= $form->field($model, 'message')->textInput(
        ['maxlength' => true, 'placeholder'=>Yii::t('category', 'Input order description')]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'options' => ['placeholder' => Yii::t('product', 'Choose date'), ": "],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
        ],
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatuses(),
        ['prompt'=>Yii::t('company', 'Select status'), ': ']) ?>

    </div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo \yii\bootstrap4\Html::submitButton(Yii::t('admin', 'Save'),
                ['class' => 'btn bg-gradient-success btn-block']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
