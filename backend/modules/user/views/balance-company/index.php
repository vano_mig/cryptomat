<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\CompanyBalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('company', 'Company Invoices');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i>
                <?php echo Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                <?php echo Html::a(Yii::t('admin', 'Add company invoice'),
                    Url::toRoute(['create']), ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => Yii::$app->user->can(User::ROLE_ADMIN) ? $searchModel : false,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                        ],
                        [
                            'attribute' => 'transaction',
                            'value'=>function($data) {
                                return $data->number ? $data->number->res_file_ref : Yii::t('admin', 'not set');
                            },
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company->name;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Company())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'service',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asService($data->service);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'service', $searchModel->getServices(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'cost',

                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'quantity',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'vat',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asStatusBalance($data->status);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getStatuses(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('company.balance');
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can(User::ROLE_ADMIN);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>