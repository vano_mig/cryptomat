<?php

/* @var $role null|yii\rbac\Role */
/* @var $permissions array|Yii\rbac\Permission[] */
/* @var $this yii\web\View */

/* @var $userPermissions array */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('admin', 'Update') . ': ' . Yii::t('users', $role->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-exclamation-triangle"></i> <?php echo Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'role' => $role,
                'permissions' => $permissions,
                'userPermissions' => $userPermissions
            ]) ?>
        </div>
    </div>
</div>