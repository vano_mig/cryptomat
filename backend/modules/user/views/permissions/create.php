<?php

/* @var $permissions array|Yii\rbac\Permission[] */

/* @var $this yii\web\View */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('app', 'New role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-exclamation-triangle"></i> <?php echo $this->title; ?></h3>
        </div>
        <div class="card-body">
            <div class="permission-form">
                <?php echo Html::beginForm('', 'post'); ?>
                <div class="form-group">
                    <label class="control-label" for="Role[name]"><?php echo Yii::t('permissions', 'Role') ?></label>
                    <?php echo Html::textInput('Role[name]', '', ['class' => 'form-control']); ?>
                </div>
                <?php if (!empty($permissions)): ?>
                    <?php foreach ($permissions as $key => $permission): ?>
                        <div class="form-group">
                            <?php echo Html::checkbox('Permission[' . $key . ']', 0); ?>
                            <label for="Permission[<?php echo $key ?>]"><?php echo Yii::t('permissions', $key) ?></label>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
                <?php echo Html:: hiddenInput(Yii:: $app->getRequest()->csrfParam, Yii:: $app->getRequest()->getCsrfToken(), []); ?>
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-primary']); ?>
                <?php echo Html::endForm() ?>
            </div>
        </div>
    </div>
</div>