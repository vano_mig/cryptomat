<?php

/* @var $roles array|Yii\rbac\Role[] */
/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\Permission */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\helpers\Url;
use common\models\User;

$this->title = Yii::t('admin', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header p-0">
            <h3 class="card-title p-2 my-1 ml-2"><i class="fas fa-exclamation-triangle"></i> <?php echo Html::encode($this->title) ?></h3>
            <div class="card-tools p-1 my-1 mr-2">
                <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
<!--                    --><?php //echo Html::a(Yii::t('admin', 'Delete Project'), Url::toRoute(['/default/delete-project', 'request' => 'drgbnes5uinwe92s']), [
//                        'class' => 'btn bg-gradient-danger btn-sm',
//                        'data' => [
//                            'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
//                            'method' => 'get',
//                        ],
//                    ]) ?>
                <?php endif ?>
            </div>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40 my-5">
                <div class="table-responsive border rounded elevation-2">
                    <table class="table table-condensed table-bordered m-0 table-border-0">
                        <thead>
                        <tr>
                            <th class="align-middle">
                                <?php echo Yii::t('admin', 'Role'); ?>
                            </th>
                            <th class="align-middle">
                                <?php echo Yii::t('admin', 'Count permissions'); ?>
                            </th>
                            <th class="align-middle text-center">
                                <?php echo Yii::t('admin', 'Actions'); ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($roles)): ?>
                            <?php foreach ($roles as $role): ?>
                                <tr>
                                    <td class="align-middle">
                                        <?php echo Yii::t('users', $role->name); ?>
                                    </td>
                                    <td class="align-middle">
                                        <?php echo $model->getCountPermissionsByRole($role->name); ?>
                                    </td>
                                    <td class="align-middle text-center p-1">
                                        <?php echo Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $role->name], ['class' => 'btn bg-gradient-warning btn-sm']); ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

