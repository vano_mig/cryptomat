<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null */
/* @var $birthdaySelect array */
/* @var $files UploadForm
/* @var $modelProfileUpdate bool|backend\modules\user\models\UserProfile */

use backend\modules\admin\models\UploadForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('admin', 'Profile');

?>

    <div class="content-header">
        <h1>
            <?php echo $this->title; ?>
        </h1>
    </div>
    <div class="content">
        <div class="card card-success card-outline">
            <div class="card-body">
                <div class="profile-user-update col-12">
                    <div class="profile-user-form">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="profile-user-update col-12 d-flex justify-content-center">
                            <div class="col-12"
                                 style="border: 1px solid #d4d7db; border-radius: 14px; max-width: 550px; margin:0 auto; ">
                                <div class="my-profile-name text-center pt-4">
                                    <p style="font-size: 1.5rem;"><?php echo ucfirst($model->username); ?><?php echo ucfirst($model->last_name); ?></p>
                                </div>
                                <div class="col-8 mx-auto my-userLogo ">
                                    <?php if ($model->image): ?>
                                        <img class="d-block col-8 mx-auto" src="<?php echo $model->image ?>">
                                    <?php endif ?>
                                    <div class="col-12">
                                        <?php echo $form->field($model, 'image', ['options' => ['class' => 'my-auto custom-file']])->fileInput(['class' => 'custom-file-input', 'id' => 'customFile'])->label(Yii::t('users', 'Photo'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                                    </div>
                                </div>
                                <div class="col-12 pt-5" style="padding: 0;">
                                    <?php if (Yii::$app->user->identity->role != $model::ROLE_USER) { ?>

                                        <?php echo $form->field($model, 'username')->textInput() ?>
                                        <?php echo $form->field($model, 'last_name')->textInput() ?>

                                        <div class="form-group" style="padding: 0 0 5px 0; ">
                                            <label><?= Yii::t('site', 'Birthday') ?></label>
                                            <div style="background-color: #fcfcfc; border-radius: 10px; width: 67%; display: inline-flex;">

                                                <?= $form->field($model, 'birthdayD', ['options' => ['style' => 'width:22%; margin-left:4%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionD'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionDCss']])->label(false); ?>
                                                <p style="margin:0; padding:5px 0 0 0; font-size: 18px;">/</p>
                                                <?= $form->field($model, 'birthdayM', ['options' => ['style' => 'width:28%; margin-left:3%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionM'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionMCss']])->label(false); ?>
                                                <p style="margin:0; padding:5px 0 0 0; font-size: 18px;">/</p>
                                                <?= $form->field($model, 'birthdayY', ['options' => ['style' => 'width:32%; margin-left:3%; position: relative;']])->dropDownList($birthdaySelect['optionY'], ['style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionYCss']])->label(false); ?>

                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php echo $form->field($model, 'email')->textInput()->label(Yii::t('users', 'Email (Login)')) ?>
                                    <?php echo $form->field($modelProfileUpdate, 'phone_number')->textInput(); ?>
                                    <?php echo $form->field($model, 'password_hash', ['options' => ['class' => 'mb-0']])->passwordInput(['style' => 'border-bottom: 1px solid #d4d7db; font-size: 2rem; padding:0 12px 2px 12px;']); ?>
                                    <?php echo $form->field($model, 'password_check')->passwordInput(['value' => $model->password_hash, 'style' => 'border-top: 1px solid #d4d7db; font-size: 2rem; padding:0 12px 2px 12px; '])->label(false) ?>
                                    <?php echo $form->field($modelProfileUpdate, 'country')->textInput(); ?>
                                    <?php echo $form->field($modelProfileUpdate, 'city')->textInput(); ?>
                                    <?php echo $form->field($modelProfileUpdate, 'street_house')->textInput(); ?>
                                    <?php echo $form->field($modelProfileUpdate, 'post_code')->textInput(); ?>

                                    <div style="display: inline-flex; justify-content: center; width: 100%;">
                                        <?= $form->field($modelProfileUpdate, 'mailing')->checkbox()->label(Yii::t('site', 'Receive promotions and news about by email')); ?>
                                        <a target="_blank" style="margin-bottom: 15px; padding: 10px;"
                                           class="fas fa-external-link-alt fa-lg"
                                           href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/site/mailing/"></a>
                                    </div>

                                    <?php if (Yii::$app->user->identity->role == $model::ROLE_ADMIN) { ?>
                                        <div class="col-12"
                                             style="border: 1px solid #d4d7db; padding-top: 1rem; border-radius: 10px; margin-bottom: 1rem;">

                                            <?php echo $form->field($model, 'role')->dropDownList($model->getRoles(), ['prompt' => Yii::t('users', 'Select role')]) ?>
                                            <?php if (!$model->isNewRecord) {
                                                echo $form->field($model, 'status')->
                                                dropDownList($model->getStatuses(),
                                                    ['prompt' => Yii::t('users', 'Select status')]);
                                            } ?>

                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($model->passport)): ?>
                            <?php foreach ($model->passport as $item): ?>
                                <div class="col-12" style="border: 1px solid #d4d7db; border-radius: 14px; max-width: 550px; margin:0 auto; ">
                                    <?php echo Html::img($item->file, ['class'=>'img-passprot']);?>
                                </div>
                            <?php endforeach ?>
                        <?php else:?>
                        <div class="col-12" style="border: 1px solid #d4d7db; border-radius: 14px; max-width: 550px; margin:0 auto; ">
                            <?php echo $form->field($files, 'imageFile[]', ['options' => ['class' => 'my-auto custom-file']])->fileInput(['class' => 'custom-file-input', 'multiple'=>true])->label(Yii::t('users', 'Passport Photo'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                        </div>
                        <?php endif ?>
                        <div class="col-12 mt-4 pt-4 border-top">
                            <div class="row col-12 col-lg-8 mx-auto">
                                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                                </div>

                                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                                    <?php echo Html::a(Yii::t('admin', 'Back'), Yii::$app->request->referrer, ['class' => 'btn bg-gradient-primary btn-block']); ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php

$script = "
$( document ).ready(function() {
    $('.custom-file-input').on('change', function() {
        var fileName = $(this).val().split('\\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);   
    });
});
";
$this->registerJs($script, yii\web\View::POS_END);