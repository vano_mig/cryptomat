<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\RegistrationThirdParty */
/* @var $modelUserProfile backend\modules\user\models\UserProfile */
/* @var $modelUser backend\modules\user\models\User|null */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = Yii::t('site', 'Registration Third Party');
?>

<div class="content-header">
    <h1>
        <?php echo $this->title; ?>
    </h1>
</div>
<div class="content">
    <div class="card card-success card-outline">

            <div class=" col mx-auto min-wr-16 max-wr-50 mx-auto pt-4">

                    <div class="row">
                        <p class="col-12 px-2"><?php echo Yii::t('site', "The cardholder's name is different from the name of a registered user:") ?></p>
                        <div class="row col d-flex justify-content-center w-100 mx-auto">
                            <div class="col mr-1"
                                 style="width: 50%; min-width: 260px; background-color: rgba(242, 242, 242, 0.9);  border-radius: 10px; padding: 1rem; margin-bottom: 0.5rem;">
                                <div><p class="m-0"
                                        style="padding-left: 1rem; font-weight: 600; text-align: center;"><?php echo Yii::t('site', 'Account:') ?></p>
                                </div>
                                <div style="border-bottom: 1px solid #e0e0e0; padding-bottom: 0.5rem; margin-bottom: 0.5rem;">
                                    <p class="m-0"><?php echo '<span style="font-weight: 500;" >' . Yii::t('site', 'First Name') . ': ' . '</span>' . $modelUser->username ?></p>
                                </div>
                                <div style="border-bottom: 1px solid #e0e0e0; padding-bottom: 0.5rem;"><p
                                            class="m-0"><?php echo '<span style="font-weight: 500;" >' . Yii::t('site', 'Last Name') . ': ' . '</span>' . $modelUser->last_name ?></p>
                                </div>
                            </div>
                            <div class="col"
                                 style="width: 50%; min-width: 260px; background-color: rgba(242, 242, 242, 0.9);  border-radius: 10px; padding: 1rem; margin-bottom: 0.5rem;">
                                <div><p class="m-0"
                                        style="padding-left: 1rem; font-weight: 600; text-align: center;"><?php echo Yii::t('site', 'Card:') ?></p>
                                </div>
                                <div style="border-bottom: 1px solid #e0e0e0; padding-bottom: 0.5rem; margin-bottom: 0.5rem;">
                                    <p class="m-0"><?php echo '<span style="font-weight: 500;" >' . Yii::t('site', 'First Name') . ': ' . '</span>' . $modelUserProfile->first_name_card ?></p>
                                </div>
                                <div style="border-bottom: 1px solid #e0e0e0; padding-bottom: 0.5rem;"><p
                                            class="m-0"><?php echo '<span style="font-weight: 500;" >' . Yii::t('site', 'Last Name') . ': ' . '</span>' . $modelUserProfile->last_name_card ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <?php $form = ActiveForm::begin(); ?>

                            <?= $form->field($model, 'agree', ['options' => ['class' => 'form-group required']])->checkbox()->label(Yii::t('site', 'I confirm the registration of a third party card')) ?>

                            <div class="row col-12">
                                <?= Html::submitButton(Yii::t('site', 'Continue'), ['class' => 'btn bg-gradient-primary mx-auto', 'name' => 'continue', 'value' => '1']) ?>
                            </div>
                            <br>
                            <?php ActiveForm::end(); ?>
                            <div class="col-12"
                                 style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 0; margin-bottom: 1rem;">
                                <p style="margin: 0.5rem 0; text-align: center;">- <?= Yii::t('site', 'or') ?> -</p>
                            </div>
                            <div class="col-12" style="margin: 1rem 0 2rem 0;">
                                <?php $form = ActiveForm::begin(); ?>
                                <div class="row col-12">
                                    <?= Html::submitButton(Yii::t('site', 'Change card'), ['class' => 'btn bg-gradient-primary mx-auto', 'name' => 'change', 'value' => '1']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>


        </div>
    </div>
</div>