<?php

/* @var $IdentificationCard bool */
/* @var $modelIdentificationCard array|backend\modules\admin\models\IdentificationCard|null|yii\db\ActiveRecord */
/* @var $passId null|string */
/* @var $androidUrl null|string */
/* @var $this yii\web\View */
/* @var $identificationCardId mixed|null|string */
/* @var $modelUserProfile backend\modules\user\models\UserProfile|mixed */
/* @var $passTemplate bool */
/* @var $modelPass frontend\models\PasscreatorService */

/* @var $iosUrl null|string */
/* @var $modelPasss array|\backend\modules\pkpass\models\PkpassPass|null|\yii\db\ActiveRecord */

/* @var $writer \BaconQrCode\Writer */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\models\User;
use yii\helpers\Url;

$url = urlencode(Url::base(true) . '/pkpass/temp/' . $modelPasss->serial_number . '_pass.pkpass');

$this->title = Yii::t('site', 'Payment Method');
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="content-header">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => ['label' => Yii::t('admin', 'Payment')],
        ]) ?>
    </div>
    <div class="content">
<?php if ((!empty($modelUserProfile->ref_card_no) && !empty($modelUserProfile->reg_person)) || Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
    <?php if ($passTemplate) { ?>
        <div class="card card-success card-outline">
            <div class="card-header">
                <h3 class="card-title"><i
                            class="fas fa-qrcode"></i> <?php if ($modelUserProfile->pkpass_status == $modelUserProfile::STATUS_PASS_ACTIVE) { ?>
                        <?= Yii::t('site', 'QR-code') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'Active'),
                                ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #00a65a; font-weight: 700;']); ?>
                    <?php } elseif ($passId == null) { ?>
                        <?= Yii::t('site', 'QR-code') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'No pass'),
                                ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #959595; font-weight: 700;']); ?>
                    <?php } else { ?>
                        <?= Yii::t('site', 'QR-code') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'Blocked'),
                                ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #d73925; font-weight: 700;']); ?>
                    <?php } ?></h3>
                <div class="card-tools">
                    <?php if ($passId != null) { ?>
                        <?php $form = ActiveForm::begin([
                            'id' => 'pkpass',
                            'enableClientValidation' => false,
                            'enableAjaxValidation' => false,
                        ]); ?>
                        <?php if ($modelUserProfile->pkpass_status == $modelUserProfile::STATUS_PASS_ACTIVE) { ?>
                            <div class="my-boxx">
                                <p style="float: left; margin: 4px 0 3px 0;"><?php echo Yii::t('site', 'Block pass:') ?></p>
                                <button type="submit" name="pkpass_status" value="0"
                                        class="btn bg-gradient-danger btn-xs"
                                        data-confirm="<?php echo Yii::t('admin', 'Are you sure you want to block the pass?'); ?>"
                                        style="float: left; margin-left: 1rem; font-size: 1rem; font-weight: 500;">
                                    <?php echo Yii::t('admin', 'Block') ?>
                                </button>
                            </div>
                        <?php } else { ?>
                            <div class="my-boxx">
                                <p style="float: left; margin: 4px 0 3px 0;"><?php echo Yii::t('site', 'Activate pass:') ?></p>
                                <button type="submit" name="pkpass_status" value="1"
                                        class="btn bg-gradient-success btn-xs"
                                        style="float: left; margin-left: 1rem; font-size: 1rem; font-weight: 500;"
                                        data-confirm="<?php echo Yii::t('admin', 'Are you sure you want to activate the pass?'); ?>">
                                    <?php echo Yii::t('admin', 'Activate') ?>
                                </button>
                            </div>
                        <?php } ?>
                        <?php ActiveForm::end(); ?>
                    <?php } ?>
                </div>
            </div>
            <div class="card-body">
                <div class="col-12 col-lg-10 mx-auto" style="margin-top: 2rem;">
                    <div class="col-12 p-0 m-0">
                        <ul class="nav nav-tabs d-flex justify-content-center" id="custom-content-above-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active"
                                   id="custom-content-above-home-tab"
                                   data-toggle="pill"
                                   href="#custom-content-above-home"
                                   role="tab"
                                   aria-controls="custom-content-above-home"
                                   aria-selected="true">
                                    <?= Yii::t('registration', 'Passcreator') ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"
                                   id="custom-content-above-profile-tab"
                                   data-toggle="pill"
                                   href="#custom-content-above-profile"
                                   role="tab"
                                   aria-controls="custom-content-above-profile"
                                   aria-selected="false">
                                    <?= Yii::t('registration', 'MS Pass') ?>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content pt-3" id="custom-content-above-tabContent">
                            <div class="tab-pane fade active show"
                                 id="custom-content-above-home"
                                 role="tabpanel"
                                 aria-labelledby="custom-content-above-home-tab">
                                <?php if ($passId == null) { ?>
                                    <?php $form = ActiveForm::begin([
                                        'id' => 'pass',
                                        'enableClientValidation' => false,
                                        'enableAjaxValidation' => false,
                                    ]); ?>
                                    <p style="text-align: center;"><?= Yii::t('site', 'Generate .pkpass electronic card for Android or iOS') ?></p>
                                    <div class="row">
                                        <?= Html::submitButton(Yii::t('site', 'Generate'), ['class' => 'btn bg-gradient-success mx-auto', 'name' => 'pkpass', 'value' => '1']) ?>
                                        <div style="display: inline-flex; align-items: center; width: 100%; justify-content: center; padding-top: 1rem;">
                                            <?= $form->field($modelPass, 'verifi', ['options' => ['class' => 'form-group required']])->checkbox() ?>
                                            <a target="_blank" style="margin-bottom: 15px; padding: 10px;"
                                               class="fas fa-external-link-alt fa-lg"
                                               href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/site/privacy-policy/"></a>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                <?php } else { ?>
                                    <div class="col-xs-12"
                                         style=" padding: 0; display: flex; flex-wrap: wrap; justify-content: space-around; align-items: center;">
                                        <div class=""
                                             style="min-width: 270px; width: 45%; max-width: 500px; margin-bottom: 3rem;">
                                            <div class="col-xs-10 col-xs-offset-1" style="float: left;">
                                                <h5 class="text-center"><?= Yii::t('site', 'Scan QR-code:') ?></h5>
                                                <?php if ($modelUserProfile->pkpass_status == $modelUserProfile::STATUS_PASS_ACTIVE) { ?>
                                                    <img style="padding: 1rem;" class="col-xs-12 img-thumbnail"
                                                         src="https://app.passcreator.com/passinstance/showpasslinkqrcode?passInstance%5B__identity%5D=<?php echo $passId ?>">
                                                <?php } else { ?>
                                                    <img style="background-color: #ec971f; padding: 1rem;"
                                                         class="col-xs-12 img-thumbnail"
                                                         src="https://app.passcreator.com/passinstance/showpasslinkqrcode?passInstance%5B__identity%5D=<?php echo $passId ?>">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="my-download-buttom "
                                             style="display: flex; flex-direction: column; min-width: 140px; width: 20%; position: relative; top: -1rem;">
                                            <div class="my-ios-buttom my-ios-buttom-m" style="">
                                                <p style="text-align: left;"><?= Yii::t('site', 'iOS') ?></p>
                                                <a href="<?php echo $iosUrl ?>" class="btn bg-gradient-secondary">
                                                    <i class="fab fa-apple"
                                                       style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Download') ?>
                                                </a>
                                            </div>
                                            <div class="my-android-buttom my-android-buttom-m"
                                                 style="margin-top: 3rem; margin-bottom: 2rem;">
                                                <p style="text-align: left;"><?= Yii::t('site', 'Android') ?></p>
                                                <a href="<?php echo $androidUrl ?>" class="btn bg-gradient-success">
                                                    <i class="fab fa-android"
                                                       style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Download') ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="" style="min-width: 270px;  width: 35%; margin-bottom: 2rem;">
                                            <?php $form = ActiveForm::begin([
                                                'id' => 'pass',
                                                'enableClientValidation' => false,
                                                'enableAjaxValidation' => false,
                                            ]); ?>
                                            <p style="text-align: center;"><?= Yii::t('site', 'Regenerate .pkpass electronic card for Android or iOS') ?></p>
                                            <div class="row">
                                                <?= Html::submitButton(Yii::t('site', 'Regenerate'), ['class' => 'btn bg-gradient-warning mx-auto', 'name' => 'pkpass', 'value' => '1', 'data-confirm' => Yii::t('admin', 'Regenerate QR-code?')]) ?>

                                                <input style="display: none;" type="checkbox" id="passcreatorservice-verifi"
                                                       name="PasscreatorService[verifi]" checked="checked" value="1">
                                            </div>
                                            <?php ActiveForm::end(); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane fade"
                                 id="custom-content-above-profile"
                                 role="tabpanel"
                                 aria-labelledby="custom-content-above-profile-tab">
                                <div class="col-xs-12"
                                     style=" padding: 0; display: flex; flex-wrap: wrap; justify-content: space-around; align-items: center;">
                                    <div class=""
                                         style="min-width: 270px; width: 45%; max-width: 350px; margin-bottom: 3rem;">
                                        <div class="col-xs-10 col-xs-offset-1" style="float: left;">
                                            <h5 class="text-center"><?= Yii::t('site', 'Scan QR-code:') ?></h5>
                                            <div class="col-12 mx-auto my-svg img-thumbnail">
                                                <?php echo $writer->writeString('https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="my-download-buttom "
                                         style="display: flex; flex-direction: column; min-width: 140px; width: 20%; position: relative; top: -1rem;">
                                        <div class="my-android-buttom my-android-buttom-m"
                                             style="margin-top: 3rem; margin-bottom: 2rem;">
                                            <p style="text-align: left;"><?= Yii::t('site', 'Android') ?></p>
                                            <a href="<?= 'https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url ?>"
                                               class="btn bg-gradient-success">
                                                <i class="fab fa-android"
                                                   style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Download') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i
                        class="far fa-id-card"></i> <?php if ($modelIdentificationCard->card_status == $modelIdentificationCard::STATUS_ACTIVE) { ?>
                    <?= Yii::t('site', 'Identification Card') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'Active'),
                            ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #00a65a; font-weight: 700;']); ?>
                <?php } elseif ($IdentificationCard) { ?>
                    <?= Yii::t('site', 'Identification Card') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'Blocked'),
                            ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #d73925; font-weight: 700;']); ?>
                <?php } else { ?>
                    <?= Yii::t('site', 'Identification Card') ?><?php echo ' ' . Html::tag('span', Yii::t('admin', 'No card'),
                            ['class' => '', 'style' => 'font-size: 1rem; vertical-align: 10%; color: #959595; font-weight: 700;']); ?>
                <?php } ?></h3>
            <div class="card-tools">
                <?php if ($modelIdentificationCard->user_id == Yii::$app->user->id) { ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'Card',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                    ]); ?>
                    <?php if ($modelIdentificationCard->card_status == $modelIdentificationCard::STATUS_ACTIVE) { ?>
                        <div class="my-boxx">
                            <p style="float: left; margin: 4px 0 3px 0;"><?php echo Yii::t('site', 'Block card:') ?></p>
                            <button type="submit" name="card_status" value="0"
                                    class="btn bg-gradient-danger btn-xs"
                                    data-confirm="<?php echo Yii::t('admin', 'Are you sure you want to block the card?'); ?>"
                                    style="float: left; margin-left: 1rem; font-size: 1rem; font-weight: 500;">
                                <?php echo Yii::t('admin', 'Block') ?>
                            </button>
                        </div>
                    <?php } else { ?>
                        <div class="my-boxx">
                            <p style="float: left; margin: 4px 0 3px 0;"><?php echo Yii::t('site', 'Activate card:') ?></p>
                            <button type="submit" name="card_status" value="1"
                                    class="btn bg-gradient-success btn-xs"
                                    style="float: left; margin-left: 1rem; font-size: 1rem; font-weight: 500;"
                                    data-confirm="<?php echo Yii::t('admin', 'Are you sure you want to activate the card?'); ?>">
                                <?php echo Yii::t('admin', 'Activate') ?>
                            </button>
                        </div>
                    <?php } ?>
                    <?php ActiveForm::end(); ?>
                <?php } ?>
            </div>
        </div>
        <div class="card-body">
            <div class="col-12 col-lg-10 mx-auto"
                 style="padding: 0; display: flex; flex-wrap: wrap; justify-content: center; margin-top: 2rem; margin-bottom: 3rem;">
                <div class="col-5 " style="min-width: 270px;">
                    <div class="">
                        <div class="">
                            <div class=""
                                 style=" padding-bottom: 10%; border-radius: 10px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
                                 <?php if ($modelIdentificationCard->user_id == Yii::$app->user->id && $modelIdentificationCard->card_status == $modelIdentificationCard::STATUS_INACTIVE) { ?>
                                         background: rgba(236,151,31,0.54);
                                         background: -moz-linear-gradient(top, rgba(236,151,31,0.54) 0%, rgba(236,151,31,0.9) 100%);
                                         background: -webkit-linear-gradient(top, rgba(236,151,31,0.54) 0%, rgba(236,151,31,0.9) 100%);
                                         background: -o-linear-gradient(top, rgba(236,151,31,0.54) 0%, rgba(236,151,31,0.9) 100%);
                                         background: -ms-linear-gradient(top, rgba(236,151,31,0.54) 0%, rgba(236,151,31,0.9) 100%);
                                         background: linear-gradient(to bottom, rgba(236,151,31,0.54) 0%, rgba(236,151,31,0.9) 100%);
                                         filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ec971f', endColorstr='#ec971f', GradientType=0 );
                                 <?php } elseif ($modelIdentificationCard->user_id != Yii::$app->user->id) { ?>
                                         background: rgba(163,163,163,0.54);
                                         background: -moz-linear-gradient(top, rgba(163,163,163,0.54) 0%, rgba(59,59,59,0.9) 100%);
                                         background: -webkit-linear-gradient(top, rgba(163,163,163,0.54) 0%, rgba(59,59,59,0.9) 100%);
                                         background: -o-linear-gradient(top, rgba(163,163,163,0.54) 0%, rgba(59,59,59,0.9) 100%);
                                         background: -ms-linear-gradient(top, rgba(163,163,163,0.54) 0%, rgba(59,59,59,0.9) 100%);
                                         background: linear-gradient(to bottom, rgba(163,163,163,0.54) 0%, rgba(59,59,59,0.9) 100%);
                                         filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a3a3a3', endColorstr='#3b3b3b', GradientType=0 );
                                 <?php } else { ?>
                                         background: rgba(0, 255, 34, 0.54);
                                         background: -moz-linear-gradient(top, rgba(0, 255, 34, 0.54) 0%, rgba(32, 173, 0, 0.9) 100%);
                                         background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0, 255, 34, 0.54)), color-stop(100%, rgba(32, 173, 0, 0.9)));
                                         background: -webkit-linear-gradient(top, rgba(0, 255, 34, 0.54) 0%, rgba(32, 173, 0, 0.9) 100%);
                                         background: -o-linear-gradient(top, rgba(0, 255, 34, 0.54) 0%, rgba(32, 173, 0, 0.9) 100%);
                                         background: -ms-linear-gradient(top, rgba(0, 255, 34, 0.54) 0%, rgba(32, 173, 0, 0.9) 100%);
                                         background: linear-gradient(to bottom, rgba(0, 255, 34, 0.54) 0%, rgba(32, 173, 0, 0.9) 100%);
                                         filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00ff22', endColorstr='#20ad00', GradientType=0);
                                 <?php } ?>">
                                <img class="img-fluid w-75 mx-auto d-block pt-5"
                                     src="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/img/system/logo/Cryptomatic_Logo.png">
                                <p class="pt-3"
                                   style="text-align: center; font-size: 3rem"><?php echo $identificationCardId ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7 " style="min-width: 270px; margin-top: 1rem;">
                    <div class="col-12 col-md-8 mx-auto" style="margin-top: 3rem;">
                        <?php $form = ActiveForm::begin([
                            'id' => 'identificationCard',
                            'enableClientValidation' => false,
                            'enableAjaxValidation' => false,
                        ]); ?>

                        <p style="float: left; margin-top: 1rem;"><?= Yii::t('site', 'Enter MS-Store24 Card Number:') ?></p>
                        <?= $form->field($modelIdentificationCard, 'card_id')->label(false)->textInput(['value' => '']) ?>
                        <div class="row">
                            <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn bg-gradient-success mx-auto', 'name' => 'save', 'value' => '1']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-qrcode"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col-12">
                <div class="row col-12 col-lg-8 mx-auto">
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <a class="btn bg-gradient-success btn-block"
                           href="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/user/profile/add-card">
                            <?php echo Yii::t('site', 'Add Card') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>