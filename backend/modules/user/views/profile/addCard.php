<?php
/* @var $this yii\web\View */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('site', 'Add Card');
$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Attached Card'), 'url' => ['profile/attached-card']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Payment')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-credit-card"></i><?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div style="padding-top: 1rem;">
                <div class="site-signup col my-cardd"
                     style="background-color: rgb(255, 255, 255); padding: 0;">
                    <div class="col-xs-12 my-cardd" style="padding: 0 5px;">
                        <div style="margin: 0 auto; width: 479px;" class="iframe">
                            <iframe style="margin: auto" width="479px" height="570px" frameborder="0"
                                    src="https://www.test.pmt-
token.eu/index.php?sid=&machine_type=BEWOTEC_V1&machine_id=JPTECH_PMT_01&responseUrl
=aHR0cHM6Ly9tcy1zdG9yZTI0LmNvbS91c2VyL3Byb2ZpbGUvY2FyZA==&sid=01&Identifier=001&language=<?php echo Yii::$app->language ?>"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
