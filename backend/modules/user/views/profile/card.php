<?php

/* @var $data array */
/* @var $this yii\web\View */

$this->title = Yii::t('site', 'Card');
?>
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,700,900&display=swap&subset=cyrillic,cyrillic-ext"
      rel="stylesheet">

<style>
    .block * {
        font-family: 'Fira Sans', sans-serif;
    }

    .block {
        font-family: 'Fira Sans', sans-serif;
        margin-left: 2px;
        margin-top: 27px;
        width: 420px;
        height: 418px;
        box-shadow: 0 0 15px #999;
        border-radius: 10px;
        padding-top: 10px;
        box-sizing: border-box;
    }

    .error * {
        text-align: center;

    }

    .text {
        text-align: center;
        font-size: 25px;
        color: rgb(99, 178, 7);
        margin: 15px 0;
        text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.4);
    }

    .card {
        width: 400px;
        height: 235px;
        border-radius: 10px;
        margin: 0 10px;
        padding-top: 65px;
        box-sizing: border-box;
        background: url(<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/img/system/card/<?php
     if ($data['card_type'] == 'VI') {
         echo 'VI.png) 97% 4% no-repeat, linear-gradient(20deg,#01DABF,#014FA0';
     } elseif ($data['card_type'] == 'MC') {
         echo 'MC.png) 97% 4% no-repeat, linear-gradient(20deg,#0097DB,#0600A0';
     } elseif ($data['card_type'] == 'AX') {
         echo 'AX.png) 97% 4% no-repeat, linear-gradient(20deg,#188955,#006493';
     }
     ?>);
        box-shadow: 1px -2px rgb(0, 0, 0) inset;

    }

    .cardNo {
        width: 400px;
        font-size: 30px;
        text-align: center;
        vertical-align: middle;
        margin: 7px 0;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.4);
    }

    .firstName {
        font-size: 23px;
        margin: 0;
        margin-left: 40px;
        margin-top: 30px;
        display: block;
        float: left;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

    .lastName {
        font-size: 23px;
        margin: 0;
        margin-left: 40px;
        margin-top: 10px;
        display: block;
        float: left;
        clear: left;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

    .exDate {
        font-size: 23px;
        margin: 0;
        margin-top: 10px;
        margin-right: 40px;
        display: block;
        float: right;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

    a {
        font-weight: 500;
        color: #333;
        text-decoration: none;
        padding: 15px;
        border-radius: 5px;
        border: 1px solid rgb(136, 152, 160);
        background: rgb(237, 237, 237);
        box-shadow: 1px -3px rgb(211, 211, 211) inset;
        transition: 0.2s;
        display: block;
        margin: 20px auto;
        margin-top: 30px;
        width: 120px;
        text-align: center;
    }

    a:hover {
        background: rgb(222, 223, 224);
    }

    a:active {

        box-shadow: 0 3px rgb(211, 211, 211) inset;
    }

    input {
        font-size: 15px;
        font-weight: 500;
        color: #333;
        text-decoration: none;
        padding: 15px;
        border-radius: 5px;
        border: 1px solid rgb(136, 152, 160);
        background: rgb(237, 237, 237);
        box-shadow: 1px -3px rgb(211, 211, 211) inset;
        transition: 0.2s;
        display: block;
        margin: 20px auto;
        margin-top: 30px;
        width: 140px;
        text-align: center;
    }


    input:hover {
        background: rgb(222, 223, 224);
    }

    input:active {

        box-shadow: 0 3px rgb(211, 211, 211) inset;
    }
</style>


<div class="block">

    <?php
    if ($data['action'] != 'OK') {
        echo '<div class="error"><p>'. Yii::t('add card'. 'Aborted transaction') .'</p></div>';
    }
    if ($data['errorCode'] != '0') {
        echo '<div class="error"><p>'. Yii::t('add card'. 'Error code') . ': ' . $data['errorCode'] . '</p>';
        echo '<p>'. Yii::t('add card'. 'Error text') .':<br>' . $data['errorText'] . '</p></div>';
    }
    if ($data['action'] == 'OK' || $data['errorCode'] == '0') {
        ?>
        <p class="text"><?= Yii::t('site', 'Your card has been added.'); ?></p>
        <div class="card">
            <p class="cardNo">
                <?php
                $i = 1;
                $no = '';
                foreach (str_split($data['ccno']) as $item) {
                    $no .= $item;
                    if ($i == 4) {
                        $i = 0;
                        $no .= '&nbsp;&nbsp;';
                    }
                    $i++;
                }
                echo $no;
                ?>
            </p>

            <p class="firstName">
                <?php
                echo $data['first_name'];
                ?>
            </p>

            <p class="lastName">
                <?php
                echo $data['last_name'];
                ?>
            </p>
            <p class="exDate">
                <?php

                $e = 1;
                $date = '';
                foreach (str_split($data['expiry_date']) as $item) {
                    $date .= $item;
                    if ($e == 2) {
                        $date .= ' / ';
                    }
                    $e++;
                }
                echo $date;
                ?>
            </p>
        </div>
        <a href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/user/profile/registration-third-party" target="_top">
            <?= Yii::t('site', 'Continue'); ?></a>
        <?php
    } else {
        ?>
        <button class="btn bg-gradient-primary center-block" type="button"
                onclick="history.back();"><?= Yii::t('site', 'Back'); ?></button>
        <?php
    }
    ?>
</div>



