<?php

/* @var $this yii\web\View */

/* @var $model array|backend\modules\user\models\UserProfile|null|yii\db\ActiveRecord */


use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('site', 'Attached Card');
$this->params['breadcrumbs'][] = $this->title;
?>


<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,700,900&display=swap&subset=cyrillic,cyrillic-ext"
      rel="stylesheet">
<style>
    .cardd * {
        font-family: 'Fira Sans', sans-serif;
    }

    .cardd {
        width: 400px;
        height: 235px;
        border-radius: 10px;
        margin: 0 auto;
        padding-top: 65px;
        box-sizing: border-box;
        background: url(<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/img/system/card/<?php
     if ($model->card_type == 'VI') {
         echo 'VI.png) 97% 4% no-repeat, linear-gradient(20deg,#01DABF,#014FA0';
     } elseif ($model->card_type == 'MC') {
         echo 'MC.png) 97% 4% no-repeat, linear-gradient(20deg,#0097DB,#0600A0';
     } elseif ($model->card_type == 'AX') {
         echo 'AX.png) 97% 4% no-repeat, linear-gradient(20deg,#188955,#006493';
     }
     ?>);
        box-shadow: 1px -2px rgb(0, 0, 0) inset;

    }

    .cardNo {
        width: 400px;
        font-size: 30px;
        text-align: center;
        vertical-align: middle;
        margin: 7px 0;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.4);
    }

    .firstName {
        font-size: 23px;
        margin: 0;
        margin-left: 40px;
        margin-top: 30px;
        display: block;
        float: left;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

    .lastName {
        font-size: 23px;
        margin: 0;
        margin-left: 40px;
        margin-top: 5px;
        display: block;
        float: left;
        clear: left;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

    .exDate {
        font-size: 23px;
        margin: 0;
        margin-top: 5px;
        margin-right: 40px;
        display: block;
        float: right;
        color: transparent;
        background: black;
        -webkit-background-clip: text;
        -moz-background-clip: text;
        background-clip: text;
        text-shadow: 0px 2px 2px rgba(255, 255, 255, 0.3);
    }

</style>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Payment')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-credit-card"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div style="margin-top: 2rem;">
                <?php if ($model->ccno != null) { ?>
                    <div class="carddiv px-2" style="position: relative;">
                        <div class="cardd" style="">
                            <p class="cardNo">
                                <?php
                                $i = 1;
                                $no = '';
                                foreach (str_split($model->ccno) as $item) {
                                    $no .= $item;
                                    if ($i == 4) {
                                        $i = 0;
                                        $no .= '&nbsp;&nbsp;';
                                    }
                                    $i++;
                                }
                                echo $no;
                                ?>
                            </p>

                            <p class="firstName">
                                <?php
                                echo $model->first_name_card
                                ?>
                            </p>

                            <p class="lastName">
                                <?php
                                echo $model->last_name_card;
                                ?>
                            </p>
                            <p class="exDate ">
                                <?php

                                $e = 1;
                                $date = '';
                                foreach (str_split($model->expiry_date) as $item) {
                                    $date .= $item;
                                    if ($e == 2) {
                                        $date .= ' / ';
                                    }
                                    $e++;
                                }
                                echo $date;

                                ?>
                            </p>
                        </div>
                    </div>

                <?php } ?>

                <?php $form = ActiveForm::begin(); ?>
                <div class="col-12 pt-5">
                    <div class="row col-12 col-lg-8 mx-auto">
                        <?php if ($model->ccno != null) { ?>
                            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                                <?php echo Html::submitButton(Yii::t('admin', 'Remove card'), ['name' => 'Delete', 'value' => 'del', 'class' => 'btn bg-gradient-danger btn-block']) ?>
                            </div>
                        <?php } else { ?>
                            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                                <a class="btn bg-gradient-success btn-block"
                                   href="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/user/profile/add-card">
                                    <?php echo Yii::t('site', 'Add Card') ?></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>




