<?php
/* @var $this yii\web\View */
/* @var $data array */

$this->title = Yii::t('site', 'Card');
?>
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,700,900&display=swap&subset=cyrillic,cyrillic-ext"
      rel="stylesheet">

<style>
    .block * {
        font-family: 'Fira Sans', sans-serif;
    }

    .block {
        font-family: 'Fira Sans', sans-serif;
        margin-left: 2px;
        margin-top: 27px;
        width: 420px;
        height: 418px;
        box-shadow: 0 0 15px #999;
        border-radius: 10px;
        padding-top: 10px;
        box-sizing: border-box;
    }

    .error * {
        text-align: center;

    }

    .text {
        text-align: center;
        font-size: 25px;
        color: rgb(178, 43, 23);
        margin: 15px 0;
        text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.4);
    }

    .card {
        width: 400px;
        height: 235px;
        border-radius: 10px;
        margin: 0 10px;
        padding-top: 20px;
        box-sizing: border-box;
        box-shadow: 1px -2px rgb(0, 0, 0) inset;
        background: linear-gradient(20deg, #01DABF, #014FA0);
    }

    .card p {
        font-size: 1.1rem;
    }

    a {
        font-weight: 500;
        color: #333;
        text-decoration: none;
        padding: 15px;
        border-radius: 5px;
        border: 1px solid rgb(136, 152, 160);
        background: rgb(237, 237, 237);
        box-shadow: 1px -3px rgb(211, 211, 211) inset;
        transition: 0.2s;
        display: block;
        margin: 20px auto;
        margin-top: 30px;
        width: 120px;
        text-align: center;
    }

    a:hover {
        background: rgb(222, 223, 224);
    }

    a:active {

        box-shadow: 0 3px rgb(211, 211, 211) inset;
    }

    input {
        font-size: 15px;
        font-weight: 500;
        color: #333;
        text-decoration: none;
        padding: 15px;
        border-radius: 5px;
        border: 1px solid rgb(136, 152, 160);
        background: rgb(237, 237, 237);
        box-shadow: 1px -3px rgb(211, 211, 211) inset;
        transition: 0.2s;
        display: block;
        margin: 20px auto;
        margin-top: 30px;
        width: 140px;
        text-align: center;
    }


    input:hover {
        background: rgb(222, 223, 224);
    }

    input:active {

        box-shadow: 0 3px rgb(211, 211, 211) inset;
    }
</style>


<div class="block">
    <p class="text"><?= Yii::t('site', 'Your card has not been added.'); ?></p>
    <div class="card">

    </div>
    <a href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] ?>/user/profile/add-card" target="_top">
        <?= Yii::t('site', 'Try again'); ?></a>
</div>



