<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\formatter\UserFormatter;
use backend\modules\user\models\Company;
use \yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\WhiteList */

$this->title = Yii::t('admin', 'View') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('white_list', 'White Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="white-list-view">
    <?= Breadcrumbs::widget([
        'links' => $this->params['breadcrumbs'] ?? [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-lock-open"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col mx-auto max-wr-40">
                    <div class="table-responsive">
                        <?php echo DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                [
                                    'attribute' => 'company_id',
                                    'value' => function ($data) {
                                        return Html::a( Company::getName($data->company_id), Url::to(['/user/company/view', 'id' =>$data->company_id]));
                                    },
                                    'format' => 'raw',
                                ],
//                                [
//                                    'attribute' => 'atm_id',
//                                    'value' => function ($data) {
//                                        return $data->atm_id ? $data->atm->name : Yii::t('admin', 'not set');
//                                    },
//                                    'format' => 'raw',
//                                ],

                                [
                                    'attribute' => 'ip_address',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(UserFormatter::class)->asWhiteList($data->ip_address);
                                    },
                                    'format' => 'html',
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('atm.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), Url::toRoute(['update', 'id' => $model->id]), ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('atm.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), Url::toRoute(['delete', 'id' => $model->id]), ['class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('atm.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

