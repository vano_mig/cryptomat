<?php

// $model WhiteLIst object
// $id integer

if (!empty($model->ip_address)) :?>
    <?php $count = count($model->ip_address); ?>
    <?php $i = 1; ?>
    <?php foreach ($model->ip_address as $key => $item) : ?>

        <div class="row ml-1">
            <?php echo $form->field($model, 'ip_address')->textInput(['name' => "WhiteList[ip_address][$i]", 'placeholder' => Yii::t('white_list', 'Input IP address'),
                'value' => "$item"]); ?>
            <button class="remove btn btn-danger" data-id="<?= $i ?>"><?php echo Yii::t('admin', 'remove') ?></button>
            <?php if ($count == $i): ?>
                <button class="add btn btn-success" data-id="<?= $i ?>"><?php echo Yii::t('admin', 'add') ?></button>
            <?php endif ?>
            <?php $i++; ?>
        </div>
    <?php endforeach ?>
<?php else : ?>
    <div class="row ml-1">
        <?php echo $form->field($model, 'ip_address')->textInput(['name' => "WhiteList[ip_address][$id]", 'placeholder' => Yii::t('white_list', 'Input IP address')]); ?>
        <?php if ($id > 0): ?>
            <button class="remove btn btn-danger" data-id="<?= $id ?>"><?php echo Yii::t('admin', 'remove') ?></button>
        <?php endif ?>
        <button class="add btn btn-success" data-id="<?= $id ?>"><?php echo Yii::t('admin', 'add') ?></button>
    </div>
<?php endif ?>