<?php

use backend\modules\user\models\Company;
use backend\modules\virtual_terminal\models\Atm;
use \yii\bootstrap4\Html;
use yii\helpers\Url;
use \yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\WhiteList */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false,
    'enableAjaxValidation' => false,]); ?>
<div class="col mx-auto max-wr-40">
    <?php echo $form->field($model, 'company_id')->dropDownList(Company::getList(),
        ['prompt' => Yii::t('company', 'Select company')]) ?>

<!--    --><?php //echo $form->field($model, 'atm_id')->dropDownList((new Atm())->getList(),
//        ['prompt' => Yii::t('company', 'Select Atm')]) ?>
    <div class="fields">
        <?php echo $this->render('_form_ip', ['model' => $model, 'form' => $form, 'id' => 0]); ?>
    </div>

</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'),
                ['class' => 'btn bg-gradient-success btn-block']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = "
    $(document).on('click', '.add', function(event) {
        let id = $(this).data('id');
        let button = $(this); 
        $.ajax({
            url: '" . Url::toRoute(['/user/white-list/add-address']) . "',
            type: 'POST',
            data: {id: id},
            dataType: 'json',
            success: function(res) {
                if (res.status == 'success') {
                     let count  = button.parent().siblings();
                     console.log(count);
                    if(count.length == 0 ) {
                        button.removeClass('add btn-success').addClass('remove btn-danger').text('" . Yii::t('admin', 'remove') . "');
                    } else {
                        button.remove();    
                    }
                    $('.fields').append(res.data);
                }
            },
            error: function(err) {
                alert('Connection error');
                console.log(err.responseText);
            }
        })
        return false;
    })
    
    $(document).on('click', '.remove', function(event) {
        let id = $(this).data('id');
        let button = $(this); 
        $('.add').remove();
        let buttonR = $('<button>').addClass('add btn btn-success').text('" . Yii::t('admin', 'add') . "').attr('data-id', id);
        let count  = button.parent().siblings();
        if(count.length == 1) {
             $(this).parent().remove();
            $('.remove').remove();
            count.append(buttonR);
        } 
        else {
            button.parent().siblings().last().append(buttonR);
              $(this).parent().remove();
        }
        return false;
    })
";

$this->registerJs($script, yii\web\View::POS_END);
?>

