<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Country */

$this->title = Yii::t('admin', 'Add company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Company list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Company')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-building"></i> <?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
                'profile'=>$profile
            ]) ?>
        </div>
    </div>
</div>