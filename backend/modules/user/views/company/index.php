<?php

use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\admin\models\Country;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Company list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-building"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('company.create')): ?>
                <?php echo Html::a(Yii::t('admin', 'Add company'), Url::toRoute(['create']), ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => Yii::$app->user->can(User::ROLE_ADMIN) ? $searchModel : false,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                        ],
                        [
                            'attribute' => 'image',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asImage($data->image);
                            },
                            'filter' => false,
                            'format' => 'html',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'name',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'city',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'country_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asLang($data->country_id);
                            },
                            'filter' => Yii::$app->user->identity->role == User::ROLE_ADMIN ? Html::activeDropDownList($searchModel, 'country_id', (new Country())->getListCountry(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']) : false,
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'vat_number',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'reg_number',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asStatus($data->status);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getStatuses(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {delete} ',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('company.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('company.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('company.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
