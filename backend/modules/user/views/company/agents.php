<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Company */
/* @var $agents array|backend\modules\user\models\CompanyReferal[]|yii\db\ActiveRecord[] */
/* @var integer $id */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('company', 'Add agent');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Administration')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-building"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>
            <?php echo $form->field($model, 'id')->dropDownList($model->getListAgents($id), ['prompt' => Yii::t('users', 'Select company')])->label(Yii::t('company', 'Company Name')) ?>
            <?php if (!empty($agents)): ?>
                <ol>
                    <?php foreach ($agents as $agent): ?>
                        <li>
                            <h5><?php echo $agent->company->name ?></h5>
                        </li>
                        <?php echo $form->field($agent, 'company_referral_id')->hiddenInput(['name' => 'CompanyAgent[id][]'])->label(false) ?>
                    <?php endforeach ?>
                </ol>
            <?php endif ?>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                    </div>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$script = "
    $(document).on('click', '.my-delete-agent', function (event) {
         $(this).closest('.my-agent').remove();
    })";
$this->registerJs($script, yii\web\View::POS_END);
?>
