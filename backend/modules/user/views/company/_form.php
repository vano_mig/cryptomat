<?php

use backend\modules\admin\models\Country;
use backend\modules\user\models\User;
use frontend\modules\admin\models\Currency;
use frontend\modules\admin\models\Language;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>
    <div class="card card-my-gray card-tabs max-wr-50 mx-auto elevation-1">
        <div class="card-header p-2 border-0 elevation-inset-2">
            <ul class="nav nav-tabs col-12">
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link active"
                       id="panel1-tab"
                       data-toggle="pill"
                       href="#panel1"
                       role="tab"
                       aria-controls="panel1"
                       aria-selected="true">
                        <?php echo Yii::t('admin', 'Company') ?>
                    </a>
                </li>
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link"
                       id="settings-tab"
                       data-toggle="pill"
                       href="#settings"
                       role="tab"
                       aria-controls="settings"
                       aria-selected="false">
                        <?php echo Yii::t('company', 'KYC Settings'); ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade active show"
                     id="panel1"
                     role="tabpanel"
                     aria-labelledby="panel1-tab">
                    <div class="col-12">
                        <div class="col mx-auto max-wr-30">
                            <?php echo $form->field($model, 'uid')->textInput(['placeholder' => Yii::t('company', 'Uniqid'), 'value' => ($model->isNewRecord || empty($model->uid) ? uniqid() : $model->uid), 'readonly' => true]); ?>
                            <?php echo $form->field($model, 'name')->textInput(['placeholder' => Yii::t('company', 'Input name')]); ?>
                            <?php echo $form->field($model, 'vat_number')->textInput(['placeholder' => Yii::t('company', 'Input Vat number')]); ?>
                            <?php echo $form->field($model, 'reg_number')->textInput(['placeholder' => Yii::t('company', 'Input registration number')]); ?>
                            <?php echo $form->field($model, 'bank_name')->textInput(['placeholder' => Yii::t('company', 'Input bank name')]); ?>
                            <?php echo $form->field($model, 'bic')->textInput(['placeholder' => Yii::t('company', 'Input BIC')]); ?>
                            <?php echo $form->field($model, 'iban')->textInput(['placeholder' => Yii::t('company', 'Input IBAN')]); ?>
                            <?php echo $form->field($model, 'contract_number')->textInput(['placeholder' => Yii::t('company', 'Input contract number')]); ?>
                            <?php echo $form->field($model, 'country_id')->dropDownList((new Country())->getListCountry(),
                                ['prompt' => Yii::t('company', 'Select country')]); ?>
                            <?php echo $form->field($model, 'city')->textInput(['placeholder' => Yii::t('company', 'Input city')]); ?>
                            <?php echo $form->field($model, 'zip_code')->textInput(['placeholder' => Yii::t('company', 'Input ZIP code')]); ?>
                            <?php echo $form->field($model, 'street')->textInput(['placeholder' => Yii::t('company', 'Input street')]); ?>
                            <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                                <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => Yii::t('company', 'Select status')]) ?>
                                <?php echo $form->field($model, 'service_email')->textInput(['placeholder' => Yii::t('company', 'Input Email')]); ?>
                                <?php echo $form->field($model, 'owners')->widget(Select2::class, [
                                    'data' => (new User())->getCustomersList(),
                                    'options' => ['multiple' => true, 'placeholder' => Yii::t('company', 'Select owners')],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            <?php endif ?>
                            <div class="col d-inline-flex">
                                <?php echo Html::img(Url::to($model->image), ['class' => 'mr-2 max-wr-8']) ?>
                                <?php echo $form->field($model, 'image', ['options' => ['class' => 'my-auto custom-file']])->fileInput(['class' => 'custom-file-input', 'id' => 'customFile'])->label(Yii::t('users', 'Logo'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="settings"
                     role="tabpanel"
                     aria-labelledby="settings-tab">
                    <div class="col mx-auto">
                        <div class="col mx-auto max-wr-30">
                            <?= $form->field($profile, 'kys_url')->textInput() ?>

                            <?= $form->field($profile, 'kys_client_id')->textInput() ?>

                            <?= $form->field($profile, 'kys_secret_key')->textInput() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$script = "
$( document ).ready(function() {
    $('.custom-file-input').on('change', function() {
        var fileName = $(this).val().split('\\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);   
    });
});
";
$this->registerJs($script, yii\web\View::POS_END);