<?php

use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\User;
use frontend\modules\admin\formatter\CurrencyFormatter;
use frontend\modules\admin\formatter\LanguageFormatter;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Company */

$this->title = Yii::t('admin', 'View') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Company list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'View');
?>


<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>


<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-building"></i> <?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <div class="card card-my-gray card-tabs max-wr-70 mx-auto elevation-1">
                <div class="card-header p-2 border-0 elevation-inset-2">
                    <ul class="nav nav-tabs border-0" id="custom-tabs-one-tab" role="tablist">
                        <li class="rounded nav-item elevation-2 mx-2 my-1">
                            <a class="rounded nav-link active"
                               id="panel1-tab"
                               data-toggle="pill"
                               href="#panel1"
                               role="tab"
                               aria-controls="panel1"
                               aria-selected="true">
                                <?php echo Yii::t('admin', 'Company') ?>
                            </a>
                        </li>

                        <li class="rounded nav-item elevation-2 mx-2 my-1">
                            <a class="rounded nav-link"
                               id="settings-tab"
                               data-toggle="pill"
                               href="#settings"
                               role="tab"
                               aria-controls="settings"
                               aria-selected="false">
                                <?php echo Yii::t('company', 'KYC Settings'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <div class="tab-pane fade active show"
                             id="panel1"
                             role="tabpanel"
                             aria-labelledby="panel1-tab">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'uid',
                                    'id',
                                    'name',
                                    'street',
                                    'city',
                                    [
                                        'attribute' => 'country_id',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(CompanyFormatter::class)->asLang($data->country_id);
                                        },
                                        'format' => 'raw',
                                    ],
                                    'zip_code',
                                    'vat_number',
                                    'reg_number',
                                    'bank_name',
                                    'iban',
                                    'bic',
                                    'contract_number',
                                    [
                                        'attribute' => 'status',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(CompanyFormatter::class)->asStatus($data->status);
                                        },
                                        'format' => 'raw',
                                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                                    ],
                                    'service_email',
                                    [
                                        'attribute' => 'image',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(CompanyFormatter::class)->asImage($data->image);
                                        },
                                        'format' => 'html',
                                    ],
                                ],
                            ]) ?>
                        </div>
                        <div class="tab-pane fade"
                             id="settings"
                             role="tabpanel"
                             aria-labelledby="settings-tab">
                            <?php echo DetailView::widget([
                                'model' => $model->profile,
                                'attributes' => [
                                    [
                                        'attribute' => 'kys_url',
                                        'format' => 'html',
                                    ],
                                    [
                                        'attribute' => 'kys_client_id',
                                        'format' => 'html',
                                    ],
                                    [
                                        'attribute' => 'kys_secret_key',
                                        'format' => 'html',
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mt-4 pt-4 border-top">
            <div class="row col-12 col-lg-8 mx-auto">
                <?php if (Yii::$app->user->can('company.user')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'Staff'),
                            Url::toRoute(['users', 'id' => $model->id]),
                            ['class' => 'btn bg-gradient-info btn-block']) ?>
                    </div>
                <?php endif ?>
                <?php if (Yii::$app->user->can('company.view')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'Agents'),
                            Url::toRoute(['agents', 'id' => $model->id]),
                            ['class' => 'btn bg-gradient-orange btn-block']) ?>
                    </div>
                <?php endif ?>
                <?php if (Yii::$app->user->can('company.delete')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'Delete'),
                            Url::toRoute(['delete', 'id' => $model->id]),
                            [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                    </div>
                <?php endif ?>
                <?php if (Yii::$app->user->can('company.update')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'Update'),
                            Url::toRoute(['update', 'id' => $model->id]),
                            ['class' => 'btn bg-gradient-warning btn-block']) ?>
                    </div>
                <?php endif ?>
                <?php if (Yii::$app->user->can('company.listview')): ?>
                    <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                        <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                            ['class' => 'btn bg-gradient-primary btn-block']) ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>