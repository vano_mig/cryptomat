<?php

namespace backend\modules\user;

use Yii;

/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\user\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['*'] = [
            'class' => 'yii\i18n\DbMessageSource',
            'forceTranslation' => true,
            'on missingTranslation' => [
                'backend\modules\user\components\TranslationEventHandler',
                'handleMissingTranslation'
            ],
        ];
    }
}
