<?php

namespace backend\modules\virtual_terminal\formatter;

use backend\components\BackendFormatter;
use backend\modules\virtual_terminal\models\Atm;
use yii\bootstrap4\Html;

class AtmFormatter extends BackendFormatter
{

    /**
     * Get Language status
     * @param $value string
     * @return string
     */
    public static function asStatus($value): string
    {
        $model = new Atm;
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }

    public static function asBlocked($value): string
    {
        $model = new Atm;
        $status = $model->getBlocked($value);
        if ($value == $model::BLOCKED) {
            return Html::tag('span', $status, ['class' => 'badge badge-danger']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }

}
