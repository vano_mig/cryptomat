<?php

namespace backend\modules\virtual_terminal\formatter;

use backend\components\BackendFormatter;
use backend\modules\virtual_terminal\models\Atm;
use backend\modules\virtual_terminal\models\VirtualTerminal;
use yii\bootstrap4\Html;

class VirtualTerminalFormatter extends BackendFormatter
{

    /**
     * Get Language status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
        $model = new VirtualTerminal;
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }

    /**
     * Get Mode
     * @param $value string
     * @return string
     */
    public static function asMode($value): string
    {
        $model = new VirtualTerminal();
        $status = $model->getMode();
        if ($value == $model::MODE_TEST) {
            return Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status[$value], ['class' => 'badge badge-danger']);
    }
}
