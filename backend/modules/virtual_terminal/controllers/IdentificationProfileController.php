<?php

namespace backend\modules\virtual_terminal\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\admin\formatter\CryptoCurrencyFormatterOld;
use backend\modules\admin\models\ApiName;
use backend\modules\admin\models\ApiNameCryptoCurrency;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\user\models\User;
use backend\modules\virtual_terminal\models\IdentificationConfigs;
use Yii;
use backend\modules\virtual_terminal\models\IdentificationProfile;
use backend\modules\virtual_terminal\models\IdentificationProfileSearch;
use yii\bootstrap4\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IdentificationProfileController implements the CRUD actions for IdentificationProfile model.
 */
class IdentificationProfileController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IdentificationProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('identification_profile.list');
        $searchModel = new IdentificationProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IdentificationProfile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('identification_profile.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Get ProductLanguage form to ajax
     * @return string
     */
    public function actionNewForm()
    {
        if ( Yii::$app->request->post('id') ) {
            $model = new IdentificationProfile();
            $model_config = new IdentificationConfigs();
            $form = new ActiveForm;
            $json['data'] = $this->renderAjax('_form_config', [
                'model' => $model,
                'form' => $form,
                'm_config' => $model_config,
                'id' => Yii::$app->request->post('id'),
                'name' => Yii::$app->request->post('name')
            ]);
            echo json_encode($json);
        }
    }

    public function convertWallets($list, $array, $type)
	{
		$replenishWalletsArray = [];
		foreach ($list as $key => $value) {
			foreach ($array as $k => $v)
			{
				if ($key === $v->provider->crypto_currency && $v->provider->info[$type] == ApiName::TYPE_ACTIVE)
					$replenishWalletsArray[$key][$v->id] = ApiFormatter::apiCryptoCurrencyGetPairById($v->provider_id) . ' (' . $v->provider_name . ')';
			}
			if (empty($replenishWalletsArray[$key]))
				$replenishWalletsArray[$key] = [];
		}
		return ($replenishWalletsArray);
	}

    /**
     * Get ProductLanguage form to ajax
     * @return string
     */
    public function actionFormWallets()
    {
		$json = [];
        if ( Yii::$app->request->post('id') ) {
			$modelWallets = IdentificationWallets::find()->where(['profile_id' => Yii::$app->request->post('modelId')])->all();
			if (empty($modelWallets))
            	$modelWallets = [new IdentificationWallets()];
//            $modelWallets = (new IdentificationWallets())::find()->where(['company_id' => Yii::$app->request->post('id')])->all();
			$replenishWallet = (new ReplenishWallet())::find()->where(['company_id' => Yii::$app->request->post('id')])->all();
			$list = CryptoCurrencyFormatterOld::getList();
            $form = new ActiveForm;
            $json['data'] = $this->renderAjax('_form_wallets', [
                'models' => $modelWallets,
                'form' => $form,
				'list' => $list,
				'buy' => $this->convertWallets($list, $replenishWallet, 'buy'),
                'sell' => $this->convertWallets($list, $replenishWallet, 'sell'),
            ]);
		}
		return json_encode($json);
	}

    /**
     * Creates a new IdentificationProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('identification_profile.create');
        $model = new IdentificationProfile();
        $modelConfig[] = new IdentificationConfigs();
        $modelWallets = [];
        if ($post = Yii::$app->request->post()) {
            $IProfile = $post['IdentificationProfile'];

            if ( $IProfile['info']['name'] && $IProfile['info']['currency_id']  )
            {
                $model->name = $IProfile['info']['name'];
                $model->currency_id = $IProfile['info']['currency_id'];
                $model->company_id = Yii::$app->user->can(User::ROLE_ADMIN) ? $IProfile['info']['company_id'] : Yii::$app->user->identity->company_id;
                $model->save();

				if ($IProfile['info']['wallets']) {

					if (isset($IProfile['info']['wallets']['buy']))
						foreach ($IProfile['info']['wallets']['buy'] as $ke => $va)
						{
							if (!empty($va)) {
								$modelWallet = new IdentificationWallets();
								$modelWallet->currency_id = $ke;
								$modelWallet->replenish_id = $va;
								$modelWallet->profile_id = $model->id;
								$modelWallet->type = IdentificationWallets::TYPE_BUY;
								$modelWallet->save();
							}
						}

					if (isset($IProfile['info']['wallets']['sell']))
						foreach ($IProfile['info']['wallets']['sell'] as $ke => $va)
						{
							if (!empty($va)) {
								$modelWallet = new IdentificationWallets();
								$modelWallet->currency_id = $ke;
								$modelWallet->replenish_id = $va;
								$modelWallet->profile_id = $model->id;
								$modelWallet->type = IdentificationWallets::TYPE_SELL;
								$modelWallet->save();
							}
						}

				}

                foreach ($IProfile as $key => $value)
                {
                    if ($key != "info")
                    {

                        $crypto_id = CryptoCurrencyFormatterOld::getElementByName($key);

                        foreach ($value as $res) {

                            if ($res['value'] > 0 && ($res['sell'] > 0 || $res['buy'] > 0) && ( $res['service_sum_scan'] > 0 ||
                                    $res['service_sum_photo_id'] > 0 || $res['service_sum_email'] > 0)) {

                                $modelProfile = new IdentificationConfigs();
                                $modelProfile->profile_id = $model->id;
                                $modelProfile->currency_id = $crypto_id['id'];
                                $modelProfile->common_sum_scan = $res['service_sum_scan'];
                                $modelProfile->common_sum_photo_id = $res['service_sum_photo_id'];
                                $modelProfile->common_sum_email = $res['service_sum_email'];
                                $modelProfile->value = $res['value'];
                                $modelProfile->buy = $res['buy'];
                                $modelProfile->sell = $res['sell'];
//                                $modelProfile->buy_sell = isset($res['type']) && $res['type'] == "sell" ? 0 : 1;
                                $modelProfile->type = $key == "common" ? 1 : 0;
                                if (!$modelProfile->save())
                                    var_dump($modelProfile);
                            }

                        }

                    }
                }

            }

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_config' => $modelConfig,
			'model_wallets' => $modelWallets
        ]);
    }

    /**
     * Updates an existing IdentificationProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('identification_profile.update');
        $model = $this->findModel($id);
        $modelConfig = $model->config;

        if ($post = Yii::$app->request->post()) {
            IdentificationConfigs::deleteAll(['profile_id' => $model->id]);
            IdentificationWallets::deleteAll(['profile_id' => $model->id]);
            $IProfile = $post['IdentificationProfile'];

            if ( $IProfile['info']['name'] && $IProfile['info']['currency_id']  )
            {
				$model->name = $IProfile['info']['name'];
				$model->currency_id = $IProfile['info']['currency_id'];
				$model->company_id = $IProfile['info']['company_id'];

				if ($IProfile['info']['wallets']) {

					if (isset($IProfile['info']['wallets']['buy']))
						foreach ($IProfile['info']['wallets']['buy'] as $ke => $va)
						{
							if (!empty($va)) {
								$modelWallet = new IdentificationWallets();
								$modelWallet->currency_id = $ke;
								$modelWallet->replenish_id = $va;
								$modelWallet->profile_id = $model->id;
								$modelWallet->type = IdentificationWallets::TYPE_BUY;
								$modelWallet->save();
							}
						}

					if (isset($IProfile['info']['wallets']['sell']))
						foreach ($IProfile['info']['wallets']['sell'] as $ke => $va)
						{
							if (!empty($va)) {
								$modelWallet = new IdentificationWallets();
								$modelWallet->currency_id = $ke;
								$modelWallet->replenish_id = $va;
								$modelWallet->profile_id = $model->id;
								$modelWallet->type = IdentificationWallets::TYPE_SELL;
								$modelWallet->save();
							}
						}

				}

                foreach ($IProfile as $key => $value)
                {
                    if ($key != "info")
                    {

                        $crypto_id = CryptoCurrencyFormatterOld::getElementByName($key);

                        foreach ($value as $res) {

                            if ($res['value'] > 0 && ($res['sell'] > 0 || $res['buy'] > 0) && ( $res['service_sum_scan'] > 0 ||
                                    $res['service_sum_photo_id'] > 0 || $res['service_sum_email'] > 0)) {

                                $modelProfile = new IdentificationConfigs();
                                $modelProfile->profile_id = $model->id;
                                $modelProfile->currency_id = $crypto_id['id'];
                                $modelProfile->common_sum_scan = $res['service_sum_scan'];
                                $modelProfile->common_sum_photo_id = $res['service_sum_photo_id'];
                                $modelProfile->common_sum_email = $res['service_sum_email'];
                                $modelProfile->value = $res['value'];
                                $modelProfile->buy = $res['buy'];
                                $modelProfile->sell = $res['sell'];
                                $modelProfile->type = $key == "common" ? 1 : 0;
//                                var_dump($modelProfile);
                                if (!$modelProfile->save())
                                    var_dump($modelProfile);
//                                die;
                            }

                        }

                    }
                }

            }
			if (!$model->save())
				var_dump($model);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_config' => $modelConfig
        ]);
    }

    /**
     * Deletes an existing IdentificationProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('identification_profile.delete');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IdentificationProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IdentificationProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = IdentificationProfile::findOne($id);
        } else {
            $model = IdentificationProfile::findOne(['id'=>$id, 'company_id'=>Yii::$app->iser->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('terminal', 'The requested page does not exist.'));
    }
}
