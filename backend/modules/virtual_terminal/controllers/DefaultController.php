<?php

namespace backend\modules\virtual_terminal\controllers;

use yii\web\Controller;

/**
 * Default controller for the `virtual_terminal` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
