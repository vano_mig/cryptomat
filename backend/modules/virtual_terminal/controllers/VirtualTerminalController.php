<?php

namespace backend\modules\virtual_terminal\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\User;
use Yii;
use backend\modules\virtual_terminal\models\VirtualTerminal;
use backend\modules\virtual_terminal\models\VirtualTerminalSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VirtualTerminalController implements the CRUD actions for VirtualTerminal model.
 */
class VirtualTerminalController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VirtualTerminal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('virtual_terminal.list');
        $searchModel = new VirtualTerminalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VirtualTerminal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('virtual_terminal.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VirtualTerminal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('virtual_terminal.create');
        $model = new VirtualTerminal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VirtualTerminal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('virtual_terminal.update');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VirtualTerminal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('virtual_terminal.delete');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VirtualTerminal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VirtualTerminal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $this->accessRules('virtual_terminal.view');
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = VirtualTerminal::findOne($id);
        } else {
            $model = VirtualTerminal::findOne(['id'=>$id, 'company_id'=>Yii::$app->user->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('virtual_terminal', 'The requested page does not exist.'));
    }
}
