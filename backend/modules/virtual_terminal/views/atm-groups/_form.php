<?php

use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\AtmGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="col mx-auto max-wr-40">

        <?php if(Yii::$app->user->can(User::ROLE_ADMIN)) : ?>
        <?= $form->field($model, 'company_id')->dropDownList((new Company())->getList(), ['prompt'=>Yii::t('admin', 'Select company')]) ?>
        <?php endif ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    </div>

    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">

            <?php if (Yii::$app->user->can('atm_groups.create') || Yii::$app->user->can('atm_groups.update')): ?>

                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                </div>

            <?php endif; ?>

            <?php if (Yii::$app->user->can('atm_groups.list')): ?>

                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
                </div>

            <?php endif; ?>

        </div>
    </div>

<?php ActiveForm::end(); ?>