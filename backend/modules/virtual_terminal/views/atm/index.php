<?php

use backend\modules\admin\formatter\CountryFormatter;
use backend\modules\virtual_terminal\formatter\AtmFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\models\Company;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\virtual_terminal\models\AtmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('terminal', 'ATMs');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-list"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('atm.create')): ?>
                <?php echo Html::a(Yii::t('terminal', 'Add ATM'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'uid',
                            'value' => function ($data) {
                                return $data->uid;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'name',
                            'value' => function ($data) {
                                return $data->name;
                            },
                            'format' => 'raw',
                        ],

                        [
                            'attribute' => 'ip',
                            'value' => function ($data) {
                                return $data->ip ? $data->ip : null;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
//                                return $data->status;
                                return Yii::$app->formatter->load( AtmFormatter::class )->asStatus($data->status);
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                            'filter' => $searchModel->getStatuses(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', 'All'), 'class' => 'form-control'],
                        ],
						[
							'attribute' => 'group_id',
							'value' => function ($data) {
								return $data->group_id;
							},
							'format' => 'raw',
						],
						[
							'attribute' => 'profile_id',
							'value' => function ($data) {
								return $data->profile_id;
							},
							'format' => 'raw',
						],
                        [
                            'attribute' => 'blocked',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load( AtmFormatter::class )->asBlocked($data->blocked);
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                            'filter' => $searchModel->getBlocked(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', 'All'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'company_name',
                            'value' => function ($data) {
                                return Html::a( (new Company)->getName($data->company_id), Url::to(['/user/company/view', 'id' =>$data->company_id]));
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'country_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load( CountryFormatter::class )->asCountryById($data->country_id);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
//                                'template' => '{answer} {view} {update} {delete}',
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($models, $key, $index) {
                                    return \Yii::$app->user->can('atm.view', ['user' => $models]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('atm.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('atm.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>

