<?php

use backend\modules\admin\models\Language;
use backend\modules\user\models\Company;
use backend\modules\virtual_terminal\models\AtmGroups;
use backend\modules\virtual_terminal\models\IdentificationProfile;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use backend\modules\admin\models\Country;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\Atm */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

    <div class="col mx-auto max-wr-40">

        <?= $form->field($model, 'uid')->textInput(['maxlength' => true, 'value' => $model->uid ? $model->uid : uniqid(), 'readonly' => true]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?php if (!$model->isNewRecord):?>

        <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>

        <?php endif; ?>

        <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => Yii::t('fridge', 'Select status')]); ?>

        <?= $form->field($model, 'blocked')->checkbox() ?>

        <?= $form->field($model, 'daily_limit')->textInput() ?>

        <?php echo $form->field($model, 'company_id')->dropDownList((new Company)->getList(), ['prompt' => Yii::t('terminal', 'Select company')]); ?>

        <?php echo $form->field($model, 'country_id')->dropDownList((new Country)->getList(), ['prompt' => Yii::t('terminal', 'Select country')]); ?>

        <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'group_id')->dropDownList((new AtmGroups())->getList(), ['prompt' => Yii::t('terminal', 'Select Group')]); ?>

        <?php echo $form->field($model, 'profile_id')->dropDownList((new IdentificationProfile())->getList(), ['prompt' => Yii::t('terminal', 'Select Profile')]); ?>

        <?= $form->field($model, 'fee')->textInput() ?>

		<?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
        <?php echo $form->field($model, 'language')->dropDownList((new Language())->getList(), ['prompt' => Yii::t('terminal', 'Select language')]); ?>


    </div>

    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">

            <?php if (Yii::$app->user->can('atm.create') || Yii::$app->user->can('atm.update')): ?>

                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                </div>

            <?php endif; ?>

            <?php if (Yii::$app->user->can('atm.list')): ?>

                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
                </div>

            <?php endif; ?>

        </div>
    </div>

<?php ActiveForm::end(); ?>
