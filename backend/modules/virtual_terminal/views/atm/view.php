<?php

use backend\modules\admin\formatter\CountryFormatter;
use backend\modules\virtual_terminal\formatter\AtmFormatter;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;
use backend\modules\user\models\Company;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\Atm */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('terminal', 'ATM'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-list"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'id',
                                'value' => function ($data) {
                                    return $data->id;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'uid',
                                'value' => function ($data) {
                                    return $data->uid;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'name',
                                'value' => function ($data) {
                                    return $data->name;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'ip',
                                'value' => function ($data) {
                                    return $data->ip ? $data->ip : null;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return AtmFormatter::asStatus($data->status);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'daily_limit',
                                'value' => function ($data) {
                                    return $data->daily_limit;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'city',
                                'value' => function ($data) {
                                    return $data->city;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'state',
                                'value' => function ($data) {
                                    return $data->state;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'address',
                                'value' => function ($data) {
                                    return $data->address;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'group_id',
                                'value' => function ($data) {
                                    return $data->group_id;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'profile_id',
                                'value' => function ($data) {
                                    return $data->profile_id;
                                },
                                'format' => 'raw',
                            ],
                            'language',
                            'fee',
                            [
                                'attribute' => 'blocked',
                                'value' => function ($data) {
                                    return AtmFormatter::asBlocked($data->blocked);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return Html::a( Company::getName($data->company_id), Url::to(['/user/company/view', 'id' =>$data->company_id]));
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'country_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load( CountryFormatter::class )->asCountryById($data->country_id);
                                },
                                'format' => 'raw',
                            ],
							[
								'attribute' => 'longitude',
								'value' => function ($data) {
									return $data->longitude;
								},
								'format' => 'raw',
							],
							[
								'attribute' => 'latitude',
								'value' => function ($data) {
									return $data->latitude;
								},
								'format' => 'raw',
							],
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>

                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">

                    <?php if (Yii::$app->user->can('atm.update')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('terminal', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('atm.delete')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('terminal', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block ',
                                'data' => [
                                    'confirm' => Yii::t('terminal', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('atm.list')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('terminal', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>

