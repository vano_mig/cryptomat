<?php

use backend\modules\virtual_terminal\formatter\VirtualTerminalFormatter;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use \yii\helpers\Url;
use backend\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\VirtualTerminal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('terminal', 'Virtual Terminal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-vuejs"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'id',
                                'value' => function ($data) {
                                    return $data->id;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'uid',
                                'value' => function ($data) {
                                    return $data->uid;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'name',
                                'value' => function ($data) {
                                    return $data->name;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'ip',
                                'value' => function ($data) {
                                    return $data->ip;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load( VirtualTerminalFormatter::class )->asStatus($data->status);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'mode',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load( VirtualTerminalFormatter::class )->asMode($data->mode);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return $data->company->name;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'country_id',
                                'value' => function ($data) {
                                    return $data->country ? $data->country->name : Yii::t('admin', 'not set');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'currency_id',
                                'value' => function ($data) {
                                    return $data->currency ? $data->currency->name : Yii::t('admin', 'not set');
                                },
                                'format' => 'raw',
                            ],
                            'currency_id',
                            'reload'
                        ],
                    ]) ?>

                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">

                    <?php if (Yii::$app->user->can('virtual_terminal.update')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('virtual_terminal.delete')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block ',
                                'data' => [
                                    'confirm' => Yii::t('virtual_terminal', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('virtual_terminal.list')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>
