<?php

use backend\modules\admin\models\Country;
use backend\modules\admin\models\Currency;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\VirtualTerminal */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="col mx-auto max-wr-40">

    <?= $form->field($model, 'uid')->textInput(['maxlength' => true, 'value' => $model->uid ? $model->uid : uniqid(), 'readonly' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord): ?>

        <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => Yii::t('fridge', 'Select status')]); ?>
    <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
        <?php echo $form->field($model, 'company_id')->dropDownList((new Company)->getList(), ['prompt' => Yii::t('fridge', 'Select company')]); ?>
    <?php endif ?>
    <?php echo $form->field($model, 'mode')->dropDownList($model->getMode(), ['prompt' => Yii::t('fridge', 'Select mode')]); ?>

    <?php echo $form->field($model, 'country_id')->widget(Select2::class, [
        'data' => (new Country())->getList(),
        'options' => ['placeholder' => Yii::t('fridge', 'Select country')],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'theme' => 'default',
    ]); ?>

    <?php echo $form->field($model, 'currency_id')->widget(Select2::class, [
        'data' => (new Currency())->getCurrencyList(),
        'options' => ['placeholder' => Yii::t('fridge', 'Select currency')],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'theme' => 'default',
    ]); ?>
</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

        <?php if (Yii::$app->user->can('virtual_terminal.create') || Yii::$app->user->can('virtual_terminal.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
            </div>

        <?php endif; ?>

        <?php if (Yii::$app->user->can('virtual_terminal.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>

        <?php endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
