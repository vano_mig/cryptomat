<?php

/* @var $this \yii\web\View */
///* @var $dataProvider \yii\data\ActiveDataProvider */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\formatter\SupportFormatter;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\models\Company;
use backend\modules\virtual_terminal\formatter\AtmFormatter;
use backend\modules\virtual_terminal\formatter\VirtualTerminalFormatter;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\virtual_terminal\models\VirtualTerminalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('terminal', 'Virtual Terminals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-vuejs"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('virtual_terminal.create')): ?>
                <?php echo Html::a(Yii::t('virtual_terminal', 'Add Virtual Terminal'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'uid',
                            'value' => function ($data) {
                                return $data->uid;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'name',
                            'value' => function ($data) {
                                return $data->name;
                            },
                            'format' => 'raw',
                        ],

                        [
                            'attribute' => 'ip',
                            'value' => function ($data) {
                                return $data->ip;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load( VirtualTerminalFormatter::class )->asStatus($data->status);
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                            'filter' => $searchModel->getStatuses(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', 'All'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'mode',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load( VirtualTerminalFormatter::class )->asMode($data->mode);
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                            'filter' => $searchModel->getMode(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', 'All'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'company_name',
                            'value' => function ($data) {
                                return Html::a(Company::getName($data->company_id), Url::to(['/user/company/view', 'id' =>$data->company_id]));
                            },
                            'format' => 'html',
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
//                                'template' => '{answer} {view} {update} {delete}',
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($models, $key, $index) {
                                    return \Yii::$app->user->can('virtual_terminal.view', ['user' => $models]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('virtual_terminal.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('virtual_terminal.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>

