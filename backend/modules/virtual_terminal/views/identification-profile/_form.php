<?php

use backend\modules\admin\formatter\CryptoCurrencyFormatterOld;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use backend\modules\virtual_terminal\models\IdentificationConfigs;
use backend\modules\virtual_terminal\models\IdentificationProfile;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use backend\modules\admin\models\Currency;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\IdentificationProfile */
/* @var $model_config backend\modules\virtual_terminal\models\IdentificationConfigs */
/* @var $model_wallets backend\modules\admin\models\IdentificationWallets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col mx-auto max-wr-40">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'name' => "IdentificationProfile[info][name]"]) ?>

    <?= $form->field($model, 'currency_id')->dropDownList((new Currency())->getCurrencyList(), ['prompt' => Yii::t('terminal', 'Select Currency'), 'name' => "IdentificationProfile[info][currency_id]"]) ?>

	<?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>

    <?= $form->field($model, 'company_id')->dropDownList((new Company())->getList(), [
            'prompt' => Yii::t('terminal','Select Company'),
            'value' => Yii::$app->user->can(User::ROLE_ADMIN) ? ($model->company_id) ?? $model->company_id : Yii::$app->user->identity->company_id,
            'name' => "IdentificationProfile[info][company_id]",
            'hidden' => !Yii::$app->user->can(User::ROLE_ADMIN)
        ])
        ?>

	<?php endif; ?>

    <hr>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Common limits</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Service limits</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

            <?php
            $bmodel = $model;
            if (empty($model_config))
                $model_config[] = new IdentificationConfigs();

            $test = false;
            $arrayWallet = -1;
            ?>

            <div class="card p-3">

                <div class="common">

                    <?php foreach ($model_config as $key => $m_config):

                        if ($m_config['type'] == 1 || count($model_config) - 1 == $key && $test == false):

                            if ( count($model_config) - 1 == $key && $test == false && $key != 0 )
                                $m_config = new IdentificationConfigs;

                            $arrayWallet += 1;
                            $test = true;

                            echo $this->render('_form_config', [
                                'model' => $model,
                                'form' => $form,
                                'm_config' => $m_config,
                                'id' => $arrayWallet,
                                'name' => 'common'
                            ]) ?>

                   <?php endif; endforeach; ?>
                </div>
                <?php echo Html::button(Yii::t('terminal', 'Add more'), ['class' => 'add-more btn bg-gradient-primary btn-block', 'data-id' => $arrayWallet + 1, 'data-name' => 'common']); ?>
            </div>
        </div>

        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

            <div class="accordion" id="accordionExample">
                <div class="card">

                    <?php
                    $list = CryptoCurrencyFormatterOld::getList();
                    foreach ($list as $key => $value): ?>

                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne_<?php echo $key?>" aria-expanded="true" aria-controls="collapseOne">
                                    <?php
                                    echo $value;
                                    ?>
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne_<?php echo $key?>" class="collapse <?php echo (( $key == 1 ) ? "show" : null) ?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card p-3">

                                <div class="<?php echo preg_replace('/[^a-zA-Z0-9\']/', '_', $value); ?>">
                                <?php

                                $test = false;
                                $arrayWallet = -1;

                                foreach ($model_config as $key2 => $m_config):

                                    if ($m_config['type'] == 0 && $m_config['currency_id'] == $key || $key2 == count($model_config) - 1 && $test == false):

                                        if ($m_config['currency_id'] != $key && $key2 == count($model_config) - 1 && $test == false) {
                                            $m_config = new IdentificationConfigs;
                                            $model = new IdentificationProfile;
                                        }

                                        $arrayWallet += 1;
                                        $test = true;

                                        echo $this->render('_form_config', [
                                            'model' => $model,
                                            'form' => $form,
                                            'm_config' => $m_config,
                                            'id' => $arrayWallet,
                                            'name' => $value
                                        ]);

                                    endif;

                                endforeach;

                                ?>
                                </div>

                                <?php echo Html::button(Yii::t('terminal', 'Add more'), ['class' => 'add-more btn bg-gradient-primary btn-block', 'data-id' => $arrayWallet + 1, 'data-name' => preg_replace('/[^a-zA-Z0-9\']/', '_', $value)]); ?>
                            </div>
                        </div>

                    <?php endforeach;?>
                </div>
            </div>

        </div>

    </div>

    <hr>

    <h5>Wallets</h5>
	<div class="wallets">

    </div>


</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

        <?php if (Yii::$app->user->can('identification_profile.create') || Yii::$app->user->can('identification_profile.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
            </div>

        <?php endif; ?>

        <?php if (Yii::$app->user->can('identification_profile.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>

        <?php endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$company = $bmodel->id;
$js = <<<JS

$(document).ready(() => {
    $('[name="IdentificationProfile[info][company_id]"]').trigger("change");
})

$('[name="IdentificationProfile[info][company_id]"]').on('change', function (elem) {
    let value = $(this).val();
    if (value) {
        $.ajax({
            url: "/terminal/identification-profile/form-wallets",
            type: 'POST',
            data: {id: value, modelId: "$company"},
            dataType: 'json',
            success: function(res) {
                $('.wallets').html(res.data)
                // console.log(res.data)
            },
            error: function(err) {
                alert('Connection error');
                console.log(err.responseText);
            }
        })
    } else {
        $('.wallets').html('')
    }
})

$('.delete').off();
$('.delete').on('click', (block) => {
    var id = $(block.target).data('id');
    var name = $(block.target).data('name');
    $(`#\${name}_\${id}`).remove();
})
    $('.add-more').on('click', (block) => {
    var id = $(block.target).data('id');
    var name = $(block.target).data('name');
    $.ajax({
        url: "/terminal/identification-profile/new-form",
        type: 'POST',
        data: {id: id, name: name},
        dataType: 'json',
        success: function(res) {
            if (res) {
                $(`.\${name}`).append(res.data);
                $('.delete').off();
                $('.delete').on('click', (block) => {
                    var id = $(block.target).data('id');
                    var name = $(block.target).data('name');
                    $(`#\${name}_\${id}`).remove();
                })
                $(block.target).data('id', id + 1);
            }
        },
        error: function(err) {
            alert('Connection error');
            console.log(err.responseText);
        }
    })
})
JS;

$this->registerJs($js, yii\web\View::POS_END);
?>
