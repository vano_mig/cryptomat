<?php

use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\admin\models\Currency;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\virtual_terminal\models\IdentificationProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('terminal', 'Identification Profiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-passport"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('identification_profile.create')): ?>
                <?php echo Html::a(Yii::t('terminal', 'Add Profile'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
			[
				'attribute' => 'company_id',
				'value' => function ($data) {
					return Company::getName($data->company_id);
				},
				'format' => 'html',
                'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
			],
			[
				'attribute' => 'currency_id',
				'value' => function ($data) {
					return CurrencyFormatter::currencyGetNameById($data->currency_id);
				},
				'format' => 'html',
                'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
			],
            //'common_sum_scan',
            //'common_sum_photo_id',
            //'common_sum_email:email',

			[
				'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
				'header' => Yii::t('admin', 'Actions'),
				'template' => '{view} {update} {delete}',
				'buttons' => [
					'view' => function ($url, $models, $key) {
						return Html::a('<i class="far fa-eye"></i>', $url, [
							'class' => 'mx-2 text-info',
							'title' => Yii::t('admin', 'View'),
							'aria-label' => Yii::t('admin', 'View'),
							'data-pjax' => Yii::t('admin', 'View'),
						]);
					},
					'update' => function ($url, $model, $key) {
						return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
							'class' => 'mx-2 text-warning',
							'title' => Yii::t('admin', 'Update'),
							'aria-label' => Yii::t('admin', 'Update'),
							'data-pjax' => Yii::t('admin', 'Update'),
						]);
					},
					'delete' => function ($url, $model, $key) {
						return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
							'class' => 'mx-2 text-dark',
							'title' => Yii::t('admin', 'Delete'),
							'aria-label' => Yii::t('admin', 'Delete'),
							'data-pjax' => Yii::t('admin', 'Delete'),
							'data' => [
								'confirm' => Yii::t('admin',
									'Are you sure you want to delete this item?'),
								'method' => 'POST'
							],
						]);
					},
				],
				'visibleButtons' => [
					'view' => function ($models, $key, $index) {
						return Yii::$app->user->can('identification_profile.view', ['user' => $models]);
					},
					'update' => function ($model, $key, $index) {
						return Yii::$app->user->can('identification_profile.update', ['user' => $model]);
					},
					'delete' => function ($model, $key, $index) {
						return Yii::$app->user->can('identification_profile.delete', ['user' => $model]);
					},
				],
			],
        ],
    ]); ?>

            </div>
        </div>
    </div>
</div>