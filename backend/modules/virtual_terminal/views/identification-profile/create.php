<?php

use yii\helpers\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\IdentificationProfile */
/* @var $model_config backend\modules\virtual_terminal\models\IdentificationConfigs */
/* @var $model_wallets backend\modules\admin\models\IdentificationWallets */

$this->title = Yii::t('terminal', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('terminal', 'Identification Profile'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-passport"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
                'model_config' => $model_config,
                'model_wallets' => $model_wallets,
            ]) ?>
        </div>
    </div>
</div>