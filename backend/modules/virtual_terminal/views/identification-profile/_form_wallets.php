<?php

use backend\modules\admin\formatter\CryptoCurrencyFormatterOld;

use backend\modules\admin\models\ReplenishWallet;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\IdentificationWallets */
/* @var $form yii\widgets\ActiveForm */
/* @var $buy */
/* @var $sell */
/* @var $list */
/* @var $models */
/* @var $id */
?>


<?php

//    echo $form->field($model, 'profile_id')->textInput();
//var_dump($list);
//var_dump($replenishWallet);


//var_dump($replenishWalletsArray);

foreach ($list as $key => $value) {
	$modelTest = $models[0];
	foreach ($models as $model)
		if ($model->currency_id === $key && $model->type == IdentificationWallets::TYPE_BUY)
			$modelTest = $model;

	echo $form->field($modelTest, 'replenish_id')->dropDownList($buy[$key], [
		'prompt' => Yii::t('admin', 'Not given'),
		'disabled' => $buy[$key] ? false : true,
		'name'=>"IdentificationProfile[info][wallets][buy][{$key}]",
		"id" => "buy_{$value}_{$key}"
	])->label('Buy ' . $value);
}

foreach ($list as $key => $value) {
	$modelTest = $models[0];
	foreach ($models as $model)
		if ($model->currency_id === $key && $model->type == IdentificationWallets::TYPE_SELL)
			$modelTest = $model;

	echo $form->field($modelTest, 'replenish_id')->dropDownList($sell[$key], [
		'prompt' => Yii::t('admin', 'Not given'),
		'disabled' => $sell[$key] ? false : true,
		'name' => "IdentificationProfile[info][wallets][sell][{$key}]",
		"id" => "sell_{$value}_{$key}"
	])->label('Sell ' . $value);
}
//var_dump($array);

?>