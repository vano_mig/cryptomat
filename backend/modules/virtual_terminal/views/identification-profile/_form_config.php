<?php

use backend\modules\admin\formatter\CryptoCurrencyFormatterOld;
use backend\modules\virtual_terminal\models\IdentificationConfigs;
use backend\modules\virtual_terminal\models\IdentificationProfile;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\virtual_terminal\models\IdentificationProfile */
/* @var $m_config backend\modules\virtual_terminal\models\IdentificationConfigs */
/* @var $id backend\modules\virtual_terminal\models\IdentificationConfigs */
/* @var $form yii\widgets\ActiveForm */
?>


    <div class="card card-body" id="<?php echo (preg_replace('/[^a-zA-Z0-9\']/', '_', $name) . "_{$id}") ?>">

        <?php

                echo $form->field($m_config, 'common_sum_scan')->checkbox(['name'=>"IdentificationProfile[{$name}][{$id}][service_sum_scan]", "id" => "scan_{$name}_{$id}"]);

                echo $form->field($m_config, 'common_sum_photo_id')->checkbox(['name'=>"IdentificationProfile[{$name}][{$id}][service_sum_photo_id]", "id" => "photo_{$name}_{$id}"]);

                echo $form->field($m_config, 'common_sum_email')->checkbox(['name'=>"IdentificationProfile[{$name}][{$id}][service_sum_email]", "id" => "email_{$name}_{$id}"]);

                echo $form->field($m_config, 'value')->input('number', ['name'=>"IdentificationProfile[{$name}][{$id}][value]", 'value' => $m_config->value, "id" => "value_{$name}_{$id}"]);

                echo $form->field($m_config, 'buy')->checkbox(['name'=>"IdentificationProfile[{$name}][{$id}][buy]", "id" => "{$name}_{$id}_buy"]);

                echo $form->field($m_config, 'sell')->checkbox(['name'=>"IdentificationProfile[{$name}][{$id}][sell]", "id" => "{$name}_{$id}_sell"]);

            ?>

        <?php
        if ($id > 0 || !$m_config->isNewRecord)
            echo Html::button(Yii::t('terminal', 'Remove'), ['class' => 'delete btn bg-gradient-danger btn-block', 'data-id' => $id, 'data-name' => preg_replace('/[^a-zA-Z0-9\']/', '_', $name)]);
        ?>
    </div>
