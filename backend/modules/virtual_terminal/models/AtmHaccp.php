<?php

namespace backend\modules\virtual_terminal\models;

use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "atm_haccp".
 *
 * @property int $id
 * @property int|null $terminal_id
 * @property string|null $content
 * @property int|null $code
 * @property int|null $status
 * @property string|null $created_at
 */
class AtmHaccp extends \yii\db\ActiveRecord
{
    public const STATUS_NEW = 1;
    public const STATUS_OLD = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm_haccp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['terminal_id', 'content'], 'required'],
            [['terminal_id', 'status', 'code'], 'integer'],
            [['content'], 'string'],
            ['status', 'default', 'value'=>self::STATUS_NEW],
            ['status', 'in', 'range'=>[self::STATUS_NEW, self::STATUS_OLD]],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'terminal_id' => Yii::t('atm', 'Terminal'),
            'content' => Yii::t('atm', 'Content'),
            'code' => Yii::t('atm', 'Code'),
            'status' => Yii::t('atm', 'Status'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }
}
