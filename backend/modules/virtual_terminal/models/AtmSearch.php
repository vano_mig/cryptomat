<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\virtual_terminal\models\Atm;

/**
 * AtmSearch represents the model behind the search form of `backend\modules\virtual_terminal\models\Atm`.
 */
class AtmSearch extends Atm
{

    public $company_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'blocked', 'daily_limit', 'company_id', 'fee'], 'integer'],
            [['uid', 'name', 'ip', 'status', 'state', 'city', 'address', 'created_at', 'updated_at', 'company_name'], 'safe'],
            [['longitude', 'latitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Atm::find()->joinWith(['company'])->distinct();

        // add conditions that should always apply here

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'atm.status' => SORT_ASC,
                    'id' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'uid',
                    'atm.status',
                    'status',
                    'name',
                    'blocked',
                    'ip',
                    'company_name' => [
                        'asc' => ['company.name' => SORT_ASC],
                        'desc' => ['company.name' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'atm.id' => $this->id,
            'blocked' => $this->blocked,
            'daily_limit' => $this->daily_limit,
            'atm.status' => $this->status,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'fee' => $this->fee,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andFilterWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'atm.name', $this->name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'company.name', $this->company_name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
