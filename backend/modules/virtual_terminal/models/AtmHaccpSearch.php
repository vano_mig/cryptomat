<?php

namespace backend\modules\virtual_terminal\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\virtual_terminal\models\AtmHaccp;

/**
 * AtmHaccpSearch represents the model behind the search form of `backend\modules\virtual_terminal\models\AtmHaccp`.
 */
class AtmHaccpSearch extends AtmHaccp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'terminal_id', 'status', 'code'], 'integer'],
            [['content', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AtmHaccp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'terminal_id' => $this->terminal_id,
            'status' => $this->status,
            'code' => $this->code,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
