<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "identification_profile".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $company_id
 * @property int|null $currency_id
 * @property int|null $service_limit_value
 */
class IdentificationProfile extends \yii\db\ActiveRecord
{
    public $service_limit_value;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'identification_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'currency_id'], 'integer'],
            [['service_limit_value'], 'safe'],
            [['name', 'company_id', 'currency_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('terminal', 'ID'),
            'name' => Yii::t('terminal', 'Name'),
            'company_id' => Yii::t('terminal', 'Company ID'),
            'currency_id' => Yii::t('terminal', 'Currency ID'),
            'service_limit_value' => Yii::t('terminal', 'service_limit_value'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabelName($name)
    {
        return $this->attributeLabels()[$name];
    }

    public function getConfig()
    {
        return $this->hasMany(IdentificationConfigs::class, ['profile_id' => 'id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

	public function getIdentificationWallets()
	{
		return $this->hasMany(IdentificationWallets::class, ['profile_id' => 'id']);
	}

    public function getList():array
    {
        $result = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $list = IdentificationProfile::find()->asArray()->all();
        } else {
            $list = IdentificationProfile::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->asArray()->all();
        }
        if(!empty($list)) {
            foreach($list as $item) {
                $result[$item['id']] = $item['name'];
            }
        }
        return $result;
    }
}
