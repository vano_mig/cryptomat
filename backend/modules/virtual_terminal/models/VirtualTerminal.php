<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\admin\models\Country;
use backend\modules\admin\models\Currency;
use backend\modules\user\models\Company;
use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "virtual_terminal".
 *
 * @property int $id
 * @property string|null $uid
 * @property string|null $name
 * @property string|null $ip
 * @property string|null $status
 * @property int|null $company_id
 * @property string|null $mode
 * @property string|null $country
 * @property string|null $currency
 * @property string|null $company
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class VirtualTerminal extends \yii\db\ActiveRecord
{

    public const STATUS_ACTIVE = 'active';
    public const STATUS_INACTIVE = 'inactive';
    public const MODE_TEST = 'test';
    public const MODE_LIFE = 'life';

    public const KEY_TEST = 'pk_test_bPm3K5bkhSEjklXb6Anzs5enfhYez7pS';
    public const KEY_LIFE = 'pk_live_r2XnjpubeZDj57h11lgTap6yBUIyWP4';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'virtual_terminal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            ['country_id', 'required'],
            [['company_id', 'status', 'name', 'currency_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['ip'], 'match', 'pattern' => '/^(?:\d{1,3}\.){3}\d{1,3}$/u'],
            ['status', 'in', 'range' => [$this::STATUS_ACTIVE,
                $this::STATUS_INACTIVE]],
            ['mode', 'default', 'value'=>self::MODE_TEST],
            ['mode', 'in', 'range' => [self::MODE_TEST,
                self::MODE_LIFE]],
            ['currency_id', 'integer'],
            [['uid', 'name', 'ip'], 'string', 'max' => 255],
            ['reload', 'safe']
        ];
    }

    /**
     * Get Identification Card
     *
     * @return yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * @param null $id
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $list = [
            self::STATUS_ACTIVE => Yii::t('fridge', 'active'),
            self::STATUS_INACTIVE => Yii::t('fridge', 'inactive'),
        ];
        if (array_key_exists($id, $list))
            return $list[$id];
        return $list;
    }

    /**
     * @return array
     */
    public function getMode():array
    {
        $list = [
            self::MODE_TEST => Yii::t('fridge', 'Test'),
            self::MODE_LIFE => Yii::t('fridge', 'Life'),
        ];
        return $list;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'uid' => Yii::t('virtual_terminal', 'Uid'),
            'name' => Yii::t('virtual_terminal', 'Name'),
            'ip' => Yii::t('virtual_terminal', 'Ip'),
            'status' => Yii::t('virtual_terminal', 'Status'),
            'company_name' => Yii::t('virtual_terminal', 'Company'),
            'mode' => Yii::t('virtual_terminal', 'Mode'),
            'currency_id' => Yii::t('virtual_terminal', 'Currency'),
            'created_at' => Yii::t('admin', 'Created'),
            'updated_at' => Yii::t('admin', 'Updated'),
            'reload' => Yii::t('admin', 'Reload time'),
        ];
    }

    /**
     * Get relation Currency model
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id'=>'currency_id']);
    }

    /**
     * Get Identification Card
     *
     * @return yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}
