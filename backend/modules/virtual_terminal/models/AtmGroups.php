<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "atm_groups".
 *
 * @property int $id
 * @property string|null $name
 */
class AtmGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'company_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('terminal', 'ID'),
            'name' => Yii::t('terminal', 'Name'),
            'company_id' => Yii::t('terminal', 'Company'),
        ];
    }

    public function getList():array
    {
        $result = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $list = AtmGroups::find()->asArray()->all();
        } else {
            $list = AtmGroups::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->asArray()->all();
        }
        if(!empty($list)) {
            foreach($list as $item) {
                $result[$item['id']] = $item['name'];
            }
        }
        return $result;
    }
}
