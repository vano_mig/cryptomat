<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\virtual_terminal\models\VirtualTerminal;

/**
 * VirtualTerminalSearch represents the model behind the search form of `backend\modules\virtual_terminal\models\VirtualTerminal`.
 */
class VirtualTerminalSearch extends VirtualTerminal
{
    public $company_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'safe'],
            [['uid', 'name', 'ip', 'status', 'created_at', 'updated_at', 'company_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VirtualTerminal::find()->joinWith(['company'])->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'virtual_terminal.status' => SORT_ASC,
                    'id' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'virtual_terminal.status',
                    'status',
                    'uid',
                    'name',
                    'ip',
                    'company_name' => [
                        'asc' => ['company.name' => SORT_ASC],
                        'desc' => ['company.name' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'virtual_terminal.id' => $this->id,
            'virtual_terminal.status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andFilterWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }

//        $query = SourceMessageSearch::find()->joinWith(['messages'])->distinct();
//        $query->andFilterWhere(['like', 'message.translation', $this->translation]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'virtual_terminal.name', $this->name])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'company.name', $this->company_name]);

        return $dataProvider;
    }
}
