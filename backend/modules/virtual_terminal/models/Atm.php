<?php

namespace backend\modules\virtual_terminal\models;

use backend\modules\admin\models\Country;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "atm".
 *
 * @property int $id
 * @property string|null $uid
 * @property string|null $name
 * @property string|null $ip
 * @property string|null $status
 * @property int|null $blocked
 * @property int|null $daily_limit
 * @property int|null $company_id
 * @property int|null $country_id
 * @property string|null $state
 * @property string|null $city
 * @property string|null $address
 * @property float|null $longitude
 * @property float|null $latitude
 * @property float|null $fee
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $language
 * @property int|null $profile_id
 * @property int|null $group_id
 */
class Atm extends \yii\db\ActiveRecord
{

    public const STATUS_ACTIVE = 'active';
    public const STATUS_INACTIVE = 'inactive';
    public const SERIES_CASH = 1;
    public const SERIES_CRYPTO = 2;
    public const BLOCKED = 1;
    public const UNBLOCKED = 0;
    public const DAILY_LIMIT = 100000;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blocked', 'daily_limit', 'company_id', 'country_id', 'group_id'], 'integer'],
            [['longitude', 'latitude', 'fee'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['blocked', 'daily_limit', 'name', 'status', 'state', 'city', 'address', 'profile_id', 'country_id', 'company_id', 'fee', 'language'], 'required'],
            [['uid', 'name', 'ip', 'status', 'state', 'city', 'address', 'language'], 'string', 'max' => 255],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * @param null $id
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $list = [
            self::STATUS_ACTIVE => Yii::t('fridge', 'active'),
            self::STATUS_INACTIVE => Yii::t('fridge', 'inactive'),
        ];
        if (array_key_exists($id, $list))
            return $list[$id];
        return $list;
    }

    public function getBlocked($id = null)
    {
        $list = [
            self::UNBLOCKED => Yii::t('terminal', 'Unlocked'),
            self::BLOCKED => Yii::t('terminal', 'Locked'),
        ];
        if (array_key_exists($id, $list))
            return $list[$id];
        return $list;
    }


    /**
     * Get Company model
     *
     * @return yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Get relation Country model
     *
     * @return yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('terminal', 'ID'),
            'uid' => Yii::t('terminal', 'Uid'),
            'name' => Yii::t('terminal', 'Name'),
            'ip' => Yii::t('terminal', 'Ip'),
            'status' => Yii::t('terminal', 'Status'),
            'blocked' => Yii::t('terminal', 'Blocked'),
            'daily_limit' => Yii::t('terminal', 'Daily Limit'),
            'company_name' => Yii::t('terminal', 'Company'),
            'country_id' => Yii::t('terminal', 'Country'),
            'state' => Yii::t('terminal', 'State'),
            'city' => Yii::t('terminal', 'City'),
            'address' => Yii::t('terminal', 'Address'),
            'longitude' => Yii::t('terminal', 'Longitude'),
            'latitude' => Yii::t('terminal', 'Latitude'),
            'fee' => Yii::t('terminal', 'Fee, %'),
            'created_at' => Yii::t('terminal', 'Created At'),
            'updated_at' => Yii::t('terminal', 'Updated At'),
            'group_id' => Yii::t('terminal', 'Group'),
            'profile_id' => Yii::t('terminal', 'Profile'),
        ];
    }

	public function getProfile()
	{
		return $this->hasOne(IdentificationProfile::class, ['id'=>'profile_id']);
	}

    public function getReplenishWallet($currency)
    {
    	$result = $this->getIdentificationWalletsReplenish()->andWhere(['currency_id' => $currency])->one();
    	if (isset($result->replenish_id))
    		$result = $result->hasOne(ReplenishWallet::class, ['id' => 'replenish_id'])->one();
        return $result;
    }

    public function getExchangeWallet($currency)
    {
		$result = $this->getIdentificationWalletsExchange()->andWhere(['currency_id' => $currency])->one();
		if (isset($result->exchange_id))
			$result = $result->hasOne(ExchangeWallet::class, ['id' => 'exchange_id'])->one();
		return $result;
    }

    public function getIdentificationWalletsReplenish()
    {
        return $this->hasMany(IdentificationWallets::class, ['profile_id'=>'profile_id'])->where(['type' => IdentificationWallets::TYPE_BUY]);
    }

    public function getIdentificationWalletsExchange()
    {
        return $this->hasMany(IdentificationWallets::class, ['profile_id'=>'profile_id'])->where(['type' => IdentificationWallets::TYPE_SELL]);
    }

    public function getList():array
    {
        $data = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $list = Atm::find()->asArray()->all();
        } else {
            $list = Atm::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->asArray()->all();
        }
        if(!empty($list)) {
            foreach ($list as $item) {
                $data[$item['id']] = $item['name'];
            }
        }
        return $data;
    }
}
