<?php

namespace backend\modules\virtual_terminal\models;

use Yii;

/**
 * This is the model class for table "identification_configs".
 *
 * @property int $id
 * @property int|null $profile_id
 * @property int|null $currency_id
 * @property int|null $common_sum_scan
 * @property int|null $common_sum_photo_id
 * @property int|null $common_sum_email
 * @property int|null $type
 * @property int|null $buy
 * @property int|null $sell
 * @property int|null $value
 */
class IdentificationConfigs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'identification_configs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'currency_id', 'buy', 'value', 'sell', 'common_sum_scan', 'common_sum_photo_id', 'common_sum_email', 'type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('terminal', 'ID'),
            'profile_id' => Yii::t('terminal', 'Profile ID'),
            'currency_id' => Yii::t('terminal', 'Currency ID'),
            'common_sum_scan' => Yii::t('terminal', 'Common Sum Scan'),
            'common_sum_photo_id' => Yii::t('terminal', 'Common Sum Photo ID'),
            'common_sum_email' => Yii::t('terminal', 'Common Sum Email'),
            'buy' => Yii::t('terminal', 'Buy'),
            'sell' => Yii::t('terminal', 'Sell'),
            'type' => Yii::t('terminal', 'Type'),
        ];
    }
}
