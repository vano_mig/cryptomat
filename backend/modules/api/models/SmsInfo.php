<?php

namespace backend\modules\api\models;

use common\components\Date;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sms_info".
 *
 * @property int $id
 * @property string|null $from_number
 * @property int|null $company_id
 * @property string|null $message
 * @property string|null $user_number
 * @property int|null $user_id
 * @property string|null $dataTime
 */
class SmsInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id'], 'integer'],
            [['dataTime'], 'safe'],
            [['from_number', 'message', 'user_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @param $info
     * @return bool
     */

    public static function sendSms($info)
    {
        try {
            $client = new Client($info['sid'], $info['token']);
        } catch (ConfigurationException $e) {
            return false;
        }

        if (!empty($info['users'])) {

            foreach ($info['users'] as $key => $value) {
                try {
                    $client->messages->create(
                        $key,
                        [
                            'from' => $info['from_number'],
                            'body' => $info['message']
                        ]
                    );

                    $model = new SmsInfo();
                    $model->user_id = 0;
                    $model->message = $info['message'];
                    $model->company_id = $info['company_id'];
                    $model->from_number = $info['from_number'];
                    $model->user_number = $key;
                    $model->save();

                } catch (TwilioException $t) {
                    return $t->getMessage();
                }
            }

        }

        return true;
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dataTime'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('API', 'ID'),
            'from_number' => Yii::t('API', 'From Number'),
            'company_id' => Yii::t('API', 'Company'),
            'message' => Yii::t('API', 'Message'),
            'user_number' => Yii::t('API', 'User Number'),
            'user_id' => Yii::t('API', 'User'),
            'dataTime' => Yii::t('API', 'Data Time'),
        ];
    }
}
