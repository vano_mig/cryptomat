<?php

namespace backend\modules\api\models;

use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\Transaction;
use backend\modules\api\interfaces\ApiInterface;
use backend\modules\user\models\CompanyProfit;
use backend\modules\virtual_terminal\models\Atm;
use common\models\Log;
use Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use CoinpaymentsAPI;


class CoinPayments extends Model implements ApiInterface
{
    public $public_key;
    private $secret_key;
    public $config;
    private $coin;

    public function __construct($config = [])
    {
        parent::__construct($this->config);
        $this->public_key = $config['public_key'];
        $this->secret_key = $config['secret_key'];
        $this->coin = new CoinpaymentsAPI($this->secret_key, $this->public_key, 'json');
    }

    public function getBalance($array): array
    {
        try {
            $information = $this->coin->GetAllCoinBalances();
            $res['info'] = $information['result'][strtoupper($array['currency'])]['balancef'];
            $res['status'] = 'success';
        } catch (\Exception $e) {
            $res['status'] = 'error';
            $res['message'] = $e->getMessage();
        }
        return $res;
    }

    public function getFee($array):array
	{
		try {
			$info = $this->actionBalance();
			$info = json_decode($info, true);
//            $info = get_object_vars($info);
			$currencyOne = strtolower($array['currencyCrypto']);
			$currencyPair = isset($array['currencyCash']) ? strtolower($array['currencyCrypto'] . $array['currencyCash']) : null;
//            $res['info'] = $info["${$array['currency']}_available"];
			$res['info'] = ['buyFee' => !empty($currencyPair) ? $info[$currencyPair.'_fee'] : null, 'withdrawalFee' => $info[$currencyOne.'_withdrawal_fee']];
			$res['status'] = 'success';
		} catch (\Exception $e) {
			$res['status'] = 'error';
		}
		return $res;
	}

    public function checkDeposit($array):array
    {
        $res['status'] = 'error';
        $messages['request'] = [];
        try {
            $info = $this->coin->GetTxInfoSingle($array['txid']);
            $res['info'] = $info;
            $res['status'] = 'success';
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('checkDeposit', $messages);
        }
        return $res;
    }

    public function checkWithdrawal($array): array
    {
        $res['status'] = 'error';
        $messages['request'] = [];
        try {
            $info = $this->coin->GetWithdrawalInformation($array['txid']);
            $res['info'] = $info['result'];
            $res['status'] = 'success';
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckWithdrawal', $messages);
        }
        return $res;
    }

    public function checkBalance($currency): array
    {
        try {
            $information = $this->coin->getBalance(['currency'=>strtoupper($currency)]);
            $res['info'] = $information;
            $res['status'] = 'success';

        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckBalance', $messages);
        }

        return $res;
    }

    /**
     * Create transaction to receive golden or money
     * @return \yii\web\Response
     */
    public function createObjectTransaction($array): array
    {
        try {
            $company = $array['company'];
            $currency = $array['currency'];
            $data = $this->coin->CreateSimpleTransaction($array['price'], strtoupper($currency->code), Yii::$app->params['ivanEmail']);
            $currencyCash = $array['currency_cash'];
            // TODO: DELETE THIS
            if ($data['error'] == 'ok') {
                $address = preg_replace('/^.*:/', '', $data['result']['address']);
                Log::logAtm('$address', $address);
                $transaction = new Transaction();
                $transaction->terminal_id = $array['atm_id'];
                $transaction->type = $array['type'];
                $transaction->amount_sent = $array['amount'];
                $transaction->currency_sent = $array['type'] == Transaction::TYPE_BUY_CASH ? $currencyCash : strtoupper($currency->code);
                $transaction->amount_receive = $data['result']['amount'];
                $transaction->currency_receive = strtoupper($currency->code);
                $transaction->status = Transaction::STATUS_WAIT;
                $transaction->transaction_id = $data['result']['txn_id'];
                $transaction->phone_number = $array['phone_number'];
                $transaction->address = $address;
                $transaction->address_coin = $address;
                $transaction->confirm_needed = $data['result']['confirms_needed'];
                $transaction->endtime = time() + $data['result']['timeout'];
                $transaction->api_id = $array['api_id'];
                if (!$transaction->save()) {
                    Log::logAtm('error', $transaction);
                }
                if($array['price_to_pay'] !== null) {
                    $fee = new CompanyProfit();
                    $fee->terminal_id = $array['atm_id'];
                    $fee->currency = $transaction->currency_receive;
                    $fee->amount = $array['price'] - $array['price_to_pay'];
                    $fee->company_id = $company->id;
                    $fee->transaction_id = $transaction->id;
                    $fee->status = CompanyProfit::STATUS_WAIT;
                    $fee->mode = null;
                    $fee->save();
                }
                $sid = $company->thirdParty->account_sid;
                $token = $company->thirdParty->auth_token;

                $arrayUsers = array(
                    $array['phone_number'] => $address,
                );

                $message = $array['type'] == Transaction::TYPE_BUY_CASH ? Yii::t("api", "address, after payment use this QR Code to receive money") : Yii::t("api", "address, after payment use this QR Code to receive gold");
                $InfoSms = [
                    'sid' => $sid,
                    'token' => $token,
                    'users' => $arrayUsers,
                    'message' => Yii::t("api", "Pay") . " " . $transaction->amount_receive . " " . Yii::t("api", "to") . " " . $address . " " . $message . ', ' . Yii::getAlias('@domain') . Url::to(['/admin/qr-code/get-qr-code', 'id' => $data['result']['txn_id']]),
                    Yii::getAlias('@domain') . Url::to(['/admin/qr-code/get-qr-code', 'id' => $data['result']['txn_id']]),
                    'from_number' => $company->thirdParty->from_number,
                    'company_id' => $company->id
                ];
                SmsInfo::sendSms($InfoSms);

                $result['amount_crypto'] = $transaction->amount_receive;
                $result['currency_code'] = strtoupper($currency->code);
                $result['timeout'] = $data['result']['timeout'];
                $result['wallet'] = $address;
                $result['qrcode_url'] = Yii::getAlias('@domain') . Url::to(['/site/scan-qr', 'id' => $data['result']['txn_id']]);
                $result['transaction_id'] = $data['result']['txn_id'];
                $result['id_for_printer'] = $transaction->id;
                $result['status'] = 'success';
                Log::logAtm('CreateObjectTr', $data);
                Log::logAtm('$result', $result);
            } else {
                $result['info']['error'] = Yii::t('api', 'Fail to create transaction!', '', $array['language']);
                $messages['response'] = $result['info']['error'];
                $messages['error'] = $data;
                Log::logAtm('CreateObjectTransaction', $messages);
            }
        } catch (Exception $e) {
            $result['info']['error'] = $e->getMessage();
            $messages['response'] = $result['info']['error'];
            Log::logAtm('CreateObjectTransaction', $messages);
        }
        return $result;
    }

    /**
     * Check transaction status
     * @return \yii\web\Response
     */
    public function checkTransaction($array): array
    {
        $result['status'] = 'error';
        $messages['request'] = $array;

        try {
            $model = Transaction::findOne(['transaction_id' => $array['txid'], 'status' => [Transaction::STATUS_WAIT, Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO]]);
            $type = Transaction::getTypeByName($array['type']);
            if ($model !== null && $type == $model->type) {
                if ($model->status == Transaction::STATUS_SUCCESS) {
                    $result['info']['amount_crypto'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
                    $result['info']['received_crypto'] = $model->amount_receive;
                    $result['info']['payment_wallet'] = $model->address;
                    $result['info']['status'] = 1;
                    $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
                    $result['info']['type'] = Transaction::getTypeByid($model->type);
                    $result['info']['id_for_printer'] = $model->id;
                    $result['status'] = 'success';
                    $model->status = Transaction::AWAITING_CONFIRM;
                    $model->save();
                } else {
                    $info = $this->coin->GetTxInfoSingle($array['txid']);
                    Log::logAtm('Check', $info);
                    $info = $info['result'];
                    if ((isset($array['test']) && $array['test'] == 1)) {
                        $result['info']['text'] = 'success';
                        $result['info']['amount_crypto'] = (string)$model->amount_receive;
                        $result['info']['received_crypto'] = (string)$model->amount_receive;
                        $result['info']['payment_wallet'] = $model->address;
                        $result['info']['status'] = 'success';
                        $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
                        $result['info']['type'] = Transaction::getTypeByid($model->type);
                        $result['info']['id_for_printer'] = $model->id;
                        $result['status'] = 'success';
                        $model->status = Transaction::AWAITING_CONFIRM;
                        $model->save();
                    } else if (!empty($info) && $info['amount'] <= $info['received'] && $info['type'] == 'coins'
                        && ($info['status'] == 100 || $info['status'] == 1)) {
                        $result['info']['text'] = $info['status_text'];
                        $result['info']['amount_crypto'] = (string)$info['amountf'];
                        $result['info']['received_crypto'] = (string)$info['receivedf'];
                        $result['info']['payment_wallet'] = $info['payment_address'];
                        $result['info']['status'] = $info['status'] == 0 ? 1 : $info['status'];
                        $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
                        $result['info']['type'] = Transaction::getTypeByid($model->type);
                        $result['info']['id_for_printer'] = $model->id;
                        $result['status'] = 'success';
                        $model->status = Transaction::AWAITING_CONFIRM;
                        $fee = CompanyProfit::findOne(['transaction_id'=>$model->id]);
                        if($fee) {
                            $fee->status = CompanyProfit::STATUS_SUCCESS;
                            $fee->save();
                        }
                        $model->save();
                    } else {
                        Log::logAtm('Info', $info);
                        $result['info']['error'] = Yii::t('api', 'Transaction not found!', '', $array['language']);
                        $messages['response'] = $result['info']['error'];
                        Log::logAtm('CheckTransaction', $messages);
                    }
                }
            } else {
                $result['info']['error'] = 'Fail to check transaction!';
                $messages['response'] = $result['info']['error'];
                Log::logAtm('CheckTransaction', $messages);
            }
        } catch (Exception $e) {
            $result['info']['error'] = $e->getMessage();
            $messages['response'] = $result['info']['error'];
            Log::logAtm('CheckTransaction', $messages);
        }
        return $result;
    }

    /**
     * Confirm transaction to buy crypto
     * @return \yii\web\Response
     */
    public function confirmCryptoTransaction($array): array
    {
        $result['status'] = 'error';
        $messages['request'] = [];
        try {
            if(array_key_exists('wallet', $array))
                unset($array['wallet']);
            if(array_key_exists('crypto_currency', $array))
                unset($array['crypto_currency']);
            $information = $this->coin->createWithdrawal($array);
            if ($information['error'] == 'ok') {
                $result['transaction_id'] = $information['result']['id'];
                $result['status'] = 'success';
            } else {
                $messages['response'] = $information;
                Log::logAtm('ConfirmCryptoTransaction', $messages);
            }
        } catch (Exception $e) {
            $result['info']['error'] = $e->getMessage();
            $messages['response'] = $result['info']['error'];
            Log::logAtm('ConfirmCryptoTransaction', $messages);
        }
        return $result;
    }
}