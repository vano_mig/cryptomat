<?php

namespace backend\modules\api\models;

use backend\modules\api\interfaces\ApiInterface;
use yii\base\Model;
use yii\rest\Controller;
use CoinpaymentsAPI;


class Kraken extends Model implements ApiInterface
{
	public $public_key;
	private $secret_key;
	public $config;
	public $kraken;

	public function __construct($config = [])
	{
		parent::__construct($this->config);
		$this->public_key = $config['public_key'];
		$this->secret_key = $config['secret_key'];
		$this->kraken = new KrakenAPIClient($this->public_key, $this->secret_key);
	}

	public function getBalance($name)
	{
		try {
			$res = $this->kraken->QueryPrivate('Balance');
			$res = (empty($res['result'])) ? 0 : $res['result'][strtoupper($name)];
		} catch (\Exception $e) {
			$res = null;
		}
		return $res;
	}

	public function actionOpenOrders($state)
	{
		try {
			$res = $this->kraken->QueryPrivate('OpenOrders', array('trades' => $state));
		} catch (KrakenAPIException $e) {
			$res = $e;
		}

		return $res;
	}

	public function actionAddOrder($state)
	{
		try {
			$res = $this->kraken->QueryPrivate('AddOrder', array(
				'pair' => 'XXBTZUSD',
				'type' => 'sell',
				'ordertype' => 'limit',
				'price' => '120',
				'volume' => '1.123'
			));
		} catch (KrakenAPIException $e) {
			$res = $e;
		}

		return $res;
	}

	public function actionTrades($state)
	{
		try {
			$res = $this->kraken->QueryPublic('Trades', array('pair' => 'XXBTZEUR', 'since' => '137589964200000000'));
		} catch (KrakenAPIException $e) {
			$res = $e;
		}

		return $res;
	}

	public function actionTicker($state)
	{
		try {
			$res = $this->kraken->QueryPublic('Ticker', array('pair' => 'XXBTZUSD'));
		} catch (KrakenAPIException $e) {
			$res = $e;
		}

		return $res;
	}

	public function actionAssets($state)
	{
		try {
			$res = $this->kraken->QueryPublic('Assets');
		} catch (KrakenAPIException $e) {
			$res = $e;
		}

		return $res;
	}

}