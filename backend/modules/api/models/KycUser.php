<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $iso_code
 * @property string $phone_code
 * @property string $code_3
 */
class KycUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kyc_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['phone_number', 'required'],
            ['phone_number', 'string'],
            ['verification','required'],
            ['verification','boolean'],
        ];
    }

}
