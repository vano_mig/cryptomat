<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "coinbase_transaction".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $buy_id
 * @property string|null $status
 * @property string|null $payment_method_id
 * @property string|null $transaction_id
 * @property float|null $amount
 * @property string|null $currency
 * @property float|null $total_amount
 * @property string|null $total_currency
 * @property float|null $subtotal_amount
 * @property string|null $subtotal_currency
 * @property float|null $fee_amount
 * @property string|null $fee_currency
 * @property string|null $resource
 * @property int|null $committed
 * @property int|null $instant
 * @property string|null $payout_at
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class CoinbaseTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coinbase_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'committed', 'instant'], 'integer'],
            [['amount', 'total_amount', 'subtotal_amount', 'fee_amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['buy_id', 'status', 'payment_method_id', 'transaction_id', 'currency', 'total_currency', 'subtotal_currency', 'fee_currency', 'resource', 'payout_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('API', 'ID'),
            'user_id' => Yii::t('API', 'User'),
            'buy_id' => Yii::t('API', 'Buy ID'),
            'status' => Yii::t('API', 'Status'),
            'payment_method_id' => Yii::t('API', 'Payment Method'),
            'transaction_id' => Yii::t('API', 'Transaction'),
            'amount' => Yii::t('API', 'Amount'),
            'currency' => Yii::t('API', 'Currency'),
            'total_amount' => Yii::t('API', 'Total Amount'),
            'total_currency' => Yii::t('API', 'Total Currency'),
            'subtotal_amount' => Yii::t('API', 'Subtotal Amount'),
            'subtotal_currency' => Yii::t('API', 'Subtotal Currency'),
            'fee_amount' => Yii::t('API', 'Fee Amount'),
            'fee_currency' => Yii::t('API', 'Fee Currency'),
            'resource' => Yii::t('API', 'Resource'),
            'committed' => Yii::t('API', 'Committed'),
            'instant' => Yii::t('API', 'Instant'),
            'payout_at' => Yii::t('API', 'Payout At'),
            'created_at' => Yii::t('API', 'Created'),
            'updated_at' => Yii::t('API', 'Updated'),
        ];
    }
}
