<?php

namespace backend\modules\api\models;

use yii\base\Model;
use Exception;

class Check extends Model
{
    public static $pattern = '/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)(\d{4,14})$/';

    /**
     * Validate phone number
     * @param string $phone
     * @return bool
     */
    public static function validatePhone($phone):bool
    {
        $res = preg_match(self::$pattern, $phone);
        return $res == 0 ? false : true;
    }

    /**
     * Validate wallet
     * @param string $wallet
     * @return bool
     */
    public static function validateWallet($wallet, $regex):bool
    {
//        $res = preg_match("/^0x[a-fA-F0-9]{40}$/", $wallet);
        $res = preg_match("/$regex/", $wallet);
        return $res == 0 ? false : true;
    }

    /**
     * Parse json
     * @param string $json
     * @return array
     */
    public static function parseJson($json):array
    {
        try {
            $res = json_decode($json, true);
        } catch (Exception $exception) {
            $res = [];
        }
        return $res;
    }

    /**
     * @param null|mixed $value
     * @param $currency
     * @return float|int|mixed
     */
    public static function calcRate($value = null, $crypto, $currency ):array
    {
        $last = Coinmarketcap::find()->where(['currency_id' => $crypto, 'currency_cash_id'=>$currency])->orderBy(['id' => SORT_DESC])->asArray()->one();
        if ($value !== null)
            $rate =  $value / $last['price'];
        $rate = $last['price'];
        $id = $last['id'];
        return ['id'=>$id, 'rate'=>$rate];
    }
}