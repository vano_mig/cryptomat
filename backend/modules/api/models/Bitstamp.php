<?php

namespace backend\modules\api\models;

use backend\modules\admin\formatter\WalletFormatter;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\MarketTransaction;
use common\models\Log;
use Exception;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use backend\modules\api\interfaces\ApiInterface;

/**
 * This is the model class for table "bitstamp_api".
 *
 * @property int $id
 * @property string|null $client_id
 * @property string|null $api_key
 * @property string|null $api_secret
 * @property int|null $user_id
 */
//class Bitstamp extends \yii\db\ActiveRecord implements ApiInterface
class Bitstamp extends Model implements ApiInterface
{
	public $public_key;
	private $secret_key;
	private $client_id;
	public $config;

	public function __construct($config = [])
	{
		parent::__construct($this->config);
		$this->public_key = $config['public_key'];
		$this->secret_key = $config['secret_key'];
		$this->client_id = $config['client_id'];

//		$this->public_key = 'ZR8TffVput3h15SdzUTJOLSkPNN59zPr';
//		$this->secret_key = '63QTh3jVLC4jhLwoTMNFfvgMTOkK1sZw';
//		$this->client_id = 'nvzo4933';
	}

	public function getBalance($array):array
	{
		try {
			$info = $this->actionBalance();
			$info = json_decode($info, true);
//            $info = get_object_vars($info);
			$currency = strtolower($array['currency']);
//            $res['info'] = $info["${$array['currency']}_available"];
			$res['info'] = $info[$currency.'_available'];
			$res['status'] = 'success';
		} catch (\Exception $e) {
			$res['status'] = 'error';
		}
		return $res;
	}

	public function getFee($array):array
	{
		try {
			$info = $this->actionBalance();
			$info = json_decode($info, true);
//            $info = get_object_vars($info);
			$currencyOne = strtolower($array['currencyCrypto']);
			$currencyPair = isset($array['currencyCash']) ? strtolower($array['currencyCrypto'] . $array['currencyCash']) : null;
//            $res['info'] = $info["${$array['currency']}_available"];
			$res['info'] = ['buyFee' => !empty($currencyPair) ? $info[$currencyPair.'_fee'] : null, 'withdrawalFee' => $info[$currencyOne.'_withdrawal_fee']];
			$res['status'] = 'success';
		} catch (\Exception $e) {
			$res['status'] = 'error';
		}
		return $res;
	}

//    public function getBalance($array, $wallet = []):array
//    {
//        $info = $this->actionBalance();
//        $info = json_decode($info, true);
//        try {
//            if(array_key_exists('amount', $array) && $wallet['wallet']->exchange_wallet) {
//                $currency = strtolower($array['currency']);
//                $res['info'] = $info[$currency.'_available'];
//
////                $exchange = strtolower($array['currency']);
//                if($array['amount'] > floatval($res['info'])) {
//                    $currency = Currency::find()->where(['id' => $wallet['profile']->currency_id])->one();
//                    Log::logAtm('Bitstamp balance $currency', $currency);
//                    $currencyNew = strtolower($currency->code);
//                    $crypto = CryptoCurrency::find()->where(['code' => $array['currency']])->one();
//                    Log::logAtm('Bitstamp balance $crypto', $crypto);
//                    $rate = Check::calcRate(null, $crypto->id, $currency->id);
//                    Log::logAtm('Bitstamp balance $rate', $rate);
//                    $price = round($array['amount']/$rate['rate'] , $crypto->precision);
//                    Log::logAtm('Bitstamp balance $price', $price);
//                    $precisions = 1;
//                    $count = $crypto->precision;
//                    for ($i = 0; $i < $count; $i++) {
//                        $precisions = $precisions * 10;
//                    }
//                    Log::logAtm('Bitstamp balance $precisions', $precisions);
//                    $exchange = ceil($price * $precisions) / $precisions;
//                    Log::logAtm('Bitstamp balance $exchange', $exchange);
//                    $res['info'] = $exchange;
//                }
//            } else if (array_key_exists('currency', $array)) {
//				$res['info'] = $info['usd_available'];
//			}
////            } else {
////                $currency = strtolower($array['currency']);
////                $res['info'] = $info[$currency.'_available'];
////            }
////            $info = json_decode($info, true);
////            $info = get_object_vars($info);
////            $currency = strtolower($array['currency']);
////            $res['info'] = $info["${$array['currency']}_available"];
////            $res['info'] = $info[$currency.'_available'];
//            $res['status'] = 'success';
//        } catch (\Exception $e) {
//			Log::logAtm('Bitstamp balance', $e->getMessage());
//            $res['status'] = 'error';
//        }
//        return $res;
//    }

    public function checkDeposit($array):array
    {
        $res['status'] = 'error';
        $res['info']['error'] = Yii::t('api', 'Method is not allowed', '', $array['language']);
        return $res;
    }

    public function checkWithdrawal($array):array
    {
//        $result = $this->curlV2('withdrawal/status/', ['id'=>$array['txid']], 'POST');
        $result = $this->curlV2('crypto_transactions/', [], 'POST');
//        $result = $this->curlV2('crypto-transactions/', ['id'=>$array['txid']], 'POST');
        $result = json_decode($result, true);
        Log::logAtm('withdrawal check', $result);
        if(array_key_exists('status', $result)) {
            $res['status'] = 'error';
            $res['info']['error'] = $result['reason'];
        } else {
            $res['status'] = 'success';
        }
        return $res;
    }

    public function createObjectTransaction($array):array
    {
        $res['status'] = 'error';
        $res['info']['error'] = Yii::t('api', 'Method is not allowed', '', $array['language']);
        return $res;
    }

    public function checkTransaction($array):array
    {
        $res['status'] = 'error';
        $res['info']['error'] = Yii::t('api', 'Method is not allowed', '', $array['language']);
        return $res;
    }

    public function confirmCryptoTransaction($array):array
    {
        $result['status'] = 'error';
        $messages['request'] = [];
		$error = false;
        try {
//            $data =[
//                'amount'=>$array['amount'],
//                'address'=>$array['address']
//            ];
			$currency = Currency::findOne(['id'=> $array['currency_receive'], 'status' => Currency::STATUS_ACTIVE]);
			$cryptoCurrency = CryptoCurrency::findOne(['id'=> $array['currency_sent'], 'active' => CryptoCurrency::STATUS_ACTIVE]);
            $function = $cryptoCurrency->code.'Withdrawal';

			$cryptoBalance = WalletFormatter::getBalance($array['wallet']);
			$apiFee = WalletFormatter::getFeeBuy($array['wallet']);
			Log::logAtm('confirmCryptoTransaction $cryptoBalance', $cryptoBalance);

			if (isset($array['wallet']->exchange_wallet))
				$exchangeWallets = ExchangeWallet::findOne(['id' => $array['wallet']->exchange_wallet]);

			if (isset($exchangeWallets)) {
				$moneyBalance = WalletFormatter::getBalanceCash($exchangeWallets);
				Log::logAtm('confirmCryptoTransaction $moneyBalance', $moneyBalance);
			}

			$precisions = 1;
			$count = $cryptoCurrency->precision;
			for ($i = 0; $i < $count; $i++) {
				$precisions = $precisions * 10;
			}

			Log::logAtm('confirmCryptoTransaction $amount', $array['amount_sent']);

			$MarketPrice = Coinmarketcap::findOne(['id' => $array['market_id']]);
//			$amountToTransfer = $array['amount_sent'] * (1 + (0.55 * 0.01));

			$sendCrypto = ceil(($array['amount_sent'] - $apiFee['withdrawalFee']) * $precisions) / $precisions;

			$amountReceive = round(($array['amount_received']) * 1.10, 5);
			Log::logAtm('confirmCryptoTransaction $MarketPrice', $MarketPrice->price);
			Log::logAtm('confirmCryptoTransaction $amountCrypto', $sendCrypto);
//			Log::logAtm('confirmCryptoTransaction $amount_sent', $amount_sent);

			$orderPair = strtolower($cryptoCurrency->code.$currency->code);

			Log::logAtm('confirmCryptoTransaction $orderPair', $orderPair);
			Log::logAtm('confirmCryptoTransaction $amountReceive', $amountReceive);

			if ($array['amount_sent'] > $cryptoBalance && isset($moneyBalance) && $moneyBalance > $amountReceive)
			{
				Log::logAtm('buyMarket');
//				$order = json_decode('{"price": "215.67", "amount": "0.14690302", "type": "0", "id": "1329708457881600", "datetime": "2021-02-16 10:20:28.647687"}');
				$order = json_decode($this->buyMarket($orderPair, $array['amount_sent']));
				$error = isset($order['status']) && $order['status'] == 'error';
				Log::logAtm('confirmCryptoTransaction $order', $order);
				$cryptoBalance = WalletFormatter::getBalance($array['wallet']);
			}

			if ($error == false && $array['amount_sent'] <= $cryptoBalance)
			{
				Log::logAtm('sendCrypto');
				$result = $this->withdraw($function, ['amount' => $sendCrypto, 'address' => $array['address']]);
//				$result = ['status' => 'success', 'transaction_id' => 'test_'.uniqid()];
				$result['order'] = $order ?? null;
			} else {
				Log::logAtm('error', [
					'$cryptoBalance' => $cryptoBalance,
					'$moneyBalance' => $moneyBalance ?? null,
					'$amountReceive' => $amountReceive,
					'$sendCrypto' => $sendCrypto,
					'$error' => $error,
					'$array[\'amount_sent\']' => $array['amount_sent'],
					]);
			}

        } catch (Exception $e) {
            $result['info']['error'] = $e->getMessage();
            $messages['response'] = $result['info']['error'];
            Log::logAtm('ConfirmCryptoTransaction bitstamp', $messages);
        }
        return $result;
    }

    public function withdraw($function, $data)
    {
        $result['status'] = 'error';
        Log::logAtm('Bitstamp withdraw date', $data);
        $res = $this->$function($data);
        $res = json_decode($res, true);
        Log::logAtm('Bitstamp withdraw $res' , $res);
        if(!empty($res) && !array_key_exists('status', $res)) {
            $result['transaction_id'] = $res['id'];
            $result['status'] = 'success';
        } else {
            $messages['response'] = 'Error withdrawal';
            $result['info']['error'] = $messages['response'];
            Log::logAtm('Bitstamp withdraw', $messages);
        }
        return $result;
    }

	/**
	 * @param $pair
	 * @param $amount
	 * @return array|string
	 * @throws NotFoundHttpException
	 */
    public function buyMarket($pair, $amount)
    {
        $data = $this->curlV2("buy/market/{$pair}/", [
            'amount' => $amount,
        ], 'POST');

		if ($data) {
			return $data;
		} else {
			return [];
		}
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function btcWithdrawal($array)
    {
        $data = $this->curlV1('bitcoin_withdrawal/', $array, 'POST');

        if ($data)
        {
            return $data;
        } else {
            return [];
        }
    }

    /**
     * @return array|bool|string
     * @throws NotFoundHttpException
     */
    public function ltcWithdrawal($array)
    {
//    	Log::logAtm('Test', $array);
        $data = $this->curlV2('ltc_withdrawal/', $array, 'POST');
        if ($data)
        {
            return $data;
        } else {
            return [];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function ethWithdrawal($array)
    {
        /**
         * #destination_tag (Optional) => Address destination tag.
         */
        $data = $this->curlV2('eth_withdrawal/', $array, 'POST');

        if ($data)
        {
           return $data;
        } else {
            return [];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function bchWithdrawal($array)
    {
        $data = $this->curlV2('bch_withdrawal/', $array, 'POST');
        if ($data)
           return $data;
        else
            return [];
    }


	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionCancelAllOrders()
	{
		return $this->curlV1('cancel_all_orders/', [], 'POST');
	}


	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function tradingPairsInfo()
	{
		return $this->curlV2('ohlc/ltcusd', []);
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionRippleWithdrawal()
	{
		$data = $this->curlV1('ripple_withdrawal/', [
			'amount' => 0,
			'address' => 0
		], 'POST');

		if ($data)
		{
			$model = new BitstampCryptoWithdrawal();
			$model->amount = $data['amount'];
			$model->currency = 'ripple';
			$model->transaction_id = $data['id'];
			$model->datetime = $data['datetime'];
			$model->address = $data['address'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionBitcoinDepositAddress()
	{
		return $this->curlV1('bitcoin_deposit_address/', [], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionRippleDepositAddress()
	{
		return  $this->curlV1('ripple_address/', [], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionUnconfirmedBtc()
	{
		return $this->curlV1('unconfirmed_btc/', [], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionBalance()
	{
//		return $this->curlV1('balance/', [], 'POST');
		return $this->curlV2('balance/', ['offset' => '1'], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionUserTransactions()
	{
		/**
		 * #offset => Skip that many transactions before returning results (default: 0, maximum: 200000).
		If you need to export older history contact support OR use combination of limit and since_id parameters
		 * #limit => Limit result to that many transactions (default: 100; maximum: 1000).
		 * #since_timestamp (Optional) => Show only transactions from unix timestamp (for max 30 days old).
		 * #since_id (Optional) => Show only transactions from specified transaction id.
		 */
		return $this->curlV2('user_transactions/', ['offset' => '0'], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionCryptoTransactions()
	{
		/**
		 * #limit => Limit result to that many transactions (default: 100; minimum: 1; maximum: 1000).
		 * #offset => Skip that many transactions before returning results (default: 0, maximum: 200000).
		 */
		return $this->curlV2('crypto-transactions/', ['offset' => '0'], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionOpenOrders()
	{
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
		return $this->curlV2("open_orders/{$pair}/", ['offset' => '0'], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionCancelOrder()
	{
		return $this->curlV2("cancel_order/", ['offset' => '0', 'id' => 0], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionBuy()
	{
		/**
		 * #limit_price (Optional) => If the order gets executed, a new sell order will be placed, with "limit_price" as its price.
		 * #daily_order (Optional) => Opens buy limit order which will be canceled at 0:00 UTC unless it already has been executed. Possible value: True
		 * #ioc_order (Optional) => An Immediate-Or-Cancel (IOC) order is an order that must be executed immediately. Any portion of an IOC order that cannot be filled immediately will be cancelled. Possible value: True
		 * #fok_order (Optional) => A Fill-Or-Kill (FOK) order is an order that must be executed immediately in its entirety. If the order cannot be immediately executed in its entirety, it will be cancelled. Possible value: True
		 */
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
		$data = $this->curlV2("buy/{$pair}/", [
			'offset' => '0',
			'amount' => '0',
			'price' => '0'
		], 'POST');


		if (!empty($data))
		{
			$model = new BitstampTransactions();
			$model->type = 1;
			$model->limit_order = 1;
			$model->order_id = $data['id'];
			$model->price = $data['price'];
			$model->amount = $data['amount'];
			$model->datetime = $data['datetime'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

//	/**
//	 * @return bool|string
//	 * @throws NotFoundHttpException
//	 */
//	public function actionBuyMarket()
//	{
//		$post = Yii::$app->request->post();
//		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//		$data = $this->curlV2("buy/market/{$pair}/", [
//			'offset' => '0',
//			'amount' => '0',
//		], 'POST');
//
//		if (!empty($data))
//		{
//			$model = new BitstampTransactions();
//			$model->type = 1;
//			$model->market_order = 1;
//			$model->order_id = $data['id'];
//			$model->price = $data['price'];
//			$model->amount = $data['amount'];
//			$model->datetime = $data['datetime'];
//			$model->user_id = Yii::$app->user->id;
//			$model->save();
//		}
//
//		return (!empty($model));
//	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionBuyInstant()
	{
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';

		$data = $this->curlV2("buy/instant/{$pair}/", [
			'offset' => '0',
			'amount' => '0',
		], 'POST');

		if (!empty($data))
		{
			$model = new BitstampTransactions();
			$model->type = 1;
			$model->instant_order = 1;
			$model->order_id = $data['id'];
			$model->price = $data['price'];
			$model->amount = $data['amount'];
			$model->datetime = $data['datetime'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionSell()
	{
		/**
		 * #limit_price (Optional) => If the order gets executed, a new sell order will be placed, with "limit_price" as its price.
		 * #daily_order (Optional) => Opens buy limit order which will be canceled at 0:00 UTC unless it already has been executed. Possible value: True
		 * #ioc_order (Optional) => An Immediate-Or-Cancel (IOC) order is an order that must be executed immediately. Any portion of an IOC order that cannot be filled immediately will be cancelled. Possible value: True
		 * #fok_order (Optional) => A Fill-Or-Kill (FOK) order is an order that must be executed immediately in its entirety. If the order cannot be immediately executed in its entirety, it will be cancelled. Possible value: True
		 */
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';

		$data = $this->curlV2("sell/{$pair}/", [
			'offset' => '0',
			'amount' => '0',
			'price' => '0'
		], 'POST');


		if (!empty($data))
		{
			$model = new BitstampTransactions();
			$model->type = 0;
			$model->limit_order = 1;
			$model->order_id = $data['id'];
			$model->price = $data['price'];
			$model->amount = $data['amount'];
			$model->datetime = $data['datetime'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionSellMarket()
	{
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';

		$data = $this->curlV2("sell/market/{$pair}/", [
			'offset' => '0',
			'amount' => '0',
		], 'POST');

		if (!empty($data))
		{
			$model = new BitstampTransactions();
			$model->type = 0;
			$model->market_order = 1;
			$model->order_id = $data['id'];
			$model->price = $data['price'];
			$model->amount = $data['amount'];
			$model->datetime = $data['datetime'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionSellInstant()
	{
		$post = Yii::$app->request->post();
		$pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';

		$data = $this->curlV2("sell/instant/{$pair}/", [
			'offset' => '0',
			'amount' => '0',
		], 'POST');

		if (!empty($data))
		{
			$model = new BitstampTransactions();
			$model->type = 0;
			$model->instant_order = 1;
			$model->order_id = $data['id'];
			$model->price = $data['price'];
			$model->amount = $data['amount'];
			$model->datetime = $data['datetime'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionWithdrawalRequests()
	{
		/**
		 * #timedelta (Optional, default 86400) => Withdrawal requests from number of seconds ago to now (max. 50000000).
		 */
		return $this->curlV2('withdrawal-requests/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionXrpWithdrawal()
	{
		/**
		 * #destination_tag (Optional) => Address destination tag.
		 */
		$data = $this->curlV2('xrp_withdrawal/', [
			'offset' => '0',
			'amount' => '0',
			'address' => '0'
		], 'POST');

		if ($data)
		{
			$model = new BitstampCryptoWithdrawal();
			$model->amount = $data['amount'];
			$model->currency = 'xrp';
			$model->transaction_id = $data['id'];
			$model->datetime = $data['datetime'];
			$model->address = $data['address'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionXlmWithdrawal()
	{
		/**
		 * #destination_tag (Optional) => Address destination tag.
		 */
		$data = $this->curlV2('xlm_withdrawal/', [
			'offset' => '0',
			'amount' => '0',
			'address' => '0'
		], 'POST');

		if ($data)
		{
			$model = new BitstampCryptoWithdrawal();
			$model->amount = $data['amount'];
			$model->currency = 'xlm';
			$model->transaction_id = $data['id'];
			$model->datetime = $data['datetime'];
			$model->address = $data['address'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionPaxWithdrawal()
	{
		/**
		 * #destination_tag (Optional) => Address destination tag.
		 */
		$data = $this->curlV2('pax_withdrawal/', [
			'offset' => '0',
			'amount' => '0',
			'address' => '0'
		], 'POST');

		if ($data)
		{
			$model = new BitstampCryptoWithdrawal();
			$model->amount = $data['amount'];
			$model->currency = 'pax';
			$model->transaction_id = $data['id'];
			$model->datetime = $data['datetime'];
			$model->address = $data['address'];
			$model->user_id = Yii::$app->user->id;
			$model->save();
		}

		return (!empty($model));
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionXrpDepositAddress()
	{
		return $this->curlV2('xrp_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionLtcDepositAddress()
	{
		return $this->curlV2('ltc_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionEthDepositAddress()
	{
		return $this->curlV2('eth_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionBchDepositAddress()
	{
		return $this->curlV2('bch_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionXlmDepositAddress()
	{
		return $this->curlV2('xlm_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	public function actionPaxDepositAddress()
	{
		return $this->curlV2('pax_address/', [
			'offset' => '0',
		], 'POST');
	}

	/**
	 * @param $id
	 * @param $url
	 * @param $data
	 * @param string $type
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	private function curlV1($url, $data, $type = 'GET')
	{
		$nonce = explode(' ', microtime());
		$nonce = $nonce[1].substr($nonce[0], 2, 3);

		$message = $nonce . $this->client_id . $this->public_key;

		$SIGNATURE = strtoupper(hash_hmac('sha256', $message, $this->secret_key));

		$ch = curl_init();

		$payload = [
			'key' => $this->public_key,
			'signature' => $SIGNATURE,
			'nonce' => $nonce,
		];

		$comb = array_merge($payload, $data);

		curl_setopt($ch, CURLOPT_URL,            "https://www.bitstamp.net/api/{$url}" );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );

		if ($type == 'POST')
			curl_setopt($ch, CURLOPT_POST,       true );
		curl_setopt($ch, CURLOPT_POSTFIELDS,  $comb);

		$result = curl_exec($ch);

		curl_close($ch);

		return $result;
	}

	/**
	 * @param $id
	 * @param $url
	 * @param $data
	 * @param string $type
	 * @return bool|string
	 * @throws NotFoundHttpException
	 */
	private function curlV2($url, $data, $type = 'GET')
	{

		$nonce = Uuid::uuid4()->toString();
		$timestamp = explode(' ', microtime());
		$timestamp = $timestamp[1].substr($timestamp[0], 2, 3);
		$content_type = 'application/x-www-form-urlencoded';

		$payload = [

		];

		$comb = array_merge($payload, $data);

		$payload_string = http_build_query($comb);

		$message = 'BITSTAMP ' . $this->public_key .
			$type .
			'www.bitstamp.net' .
			"/api/v2/{$url}" .
			'' .
			$content_type .
			$nonce .
			$timestamp .
			'v2' .
			$payload_string;

		$SIGNATURE = hash_hmac('sha256', $message, $this->secret_key);

		$headers = array(
			'X-Auth: BITSTAMP ' . $this->public_key,
			'X-Auth-Signature: '. $SIGNATURE,
			'X-Auth-Nonce:'. $nonce,
			'X-Auth-Timestamp:'. $timestamp,
			'X-Auth-Version:'. 'v2',
			'Content-Type:'. $content_type
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,            "https://www.bitstamp.net/api/v2/{$url}" );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_HEADER, 1);

		if ($type == 'POST')
			curl_setopt($ch, CURLOPT_POST,       true );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		if (!empty($payload_string))
			curl_setopt($ch, CURLOPT_POSTFIELDS,  $payload_string);

		$result = curl_exec($ch);

		$headers = $this->get_headers_from_curl_response($result);

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result = substr($result, $header_size);

		Log::logCurl("resultCurlV2 {$url}", $result);
		Log::logCurl("resultCurlV2 payload_string", $payload_string);

		curl_close($ch);

		$string_to_sign = ( $nonce . $timestamp . $headers['Content-Type']) . $result;
		$signature_check = hash_hmac('sha256', $string_to_sign, $this->secret_key);

		return ($type == 'GET' || ($type == 'POST' && !empty($headers['X-Server-Auth-Signature']) && $signature_check == $headers['X-Server-Auth-Signature']) ? $result : null);
	}

	/**
	 * @param $response
	 * @return array
	 */
	public function get_headers_from_curl_response($response)
	{
		$headers = array();

		$header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

		foreach (explode("\r\n", $header_text) as $i => $line)
			if ($i === 0)
				$headers['http_code'] = $line;
			else
			{
				list ($key, $value) = explode(': ', $line);

				$headers[$key] = $value;
			}

		return $headers;
	}
}
