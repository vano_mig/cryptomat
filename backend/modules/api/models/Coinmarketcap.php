<?php

namespace backend\modules\api\models;

use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use Yii;

/**
 * This is the model class for table "coinmarketcap".
 *
 * @property int $id
 * @property float|null $price
 * @property float|null $volume_24h
 * @property float|null $percent_change_1h
 * @property float|null $percent_change_24h
 * @property int|null $currency_id
 * @property float|null $percent_change_7d
 * @property string|null $last_updated
 * @property int|null $currency_cash_id
 */
class Coinmarketcap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coinmarketcap';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'volume_24h', 'currency_id', 'currency_cash_id', 'percent_change_1h', 'percent_change_24h', 'percent_change_7d'], 'number'],
            [['last_updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'price' => Yii::t('admin', 'Price'),
            'volume_24h' => Yii::t('admin', 'Volume 24h'),
            'currency_id' => Yii::t('admin', 'Crypto Id'),
            'currency_cash_id' => Yii::t('admin', 'Currency'),
            'percent_change_1h' => Yii::t('admin', 'Percent Change 1h'),
            'percent_change_24h' => Yii::t('admin', 'Percent Change 24h'),
            'percent_change_7d' => Yii::t('admin', 'Percent Change 7d'),
            'last_updated' => Yii::t('admin', 'Last Updated'),
        ];
    }

    public function getCurrencyRate($currency)
    {
        $model = CryptoCurrency::findOne(['code'=>$currency, 'active'=>CryptoCurrency::STATUS_ACTIVE]);
        if($model === null) {
            $model = Currency::findOne(['code'=>$currency, 'status'=>Currency::STATUS_ACTIVE]);
        }

        $rate = Coinmarketcap::find()->where(['currency_id'=>$model->id])->asArray()->orderBy(['id'=>SORT_DESC])->one();
        return $rate ? $rate['price'] : 1;
    }
}
