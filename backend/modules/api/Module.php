<?php

namespace backend\modules\api;

use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['*'] = [
            'class' => 'yii\i18n\DbMessageSource',
            'forceTranslation' => true,
            'on missingTranslation' => [
                'backend\modules\api\components\TranslationEventHandler',
                'handleMissingTranslation'
            ],
//            'sourceLanguage' => 'fr',
//            'basePath'       => '@frontend/modules/admin/messages',
//            'fileMap'        => [
//            ],
        ];
    }
}
