<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\SmsInfo;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use yii\rest\Controller;

class SmsInfoController extends Controller
{

    public function actionIndex()
    {
        $sid = 'ACaaf77437371aa27828947bdf60cef3c9';
        $token = '711dfddc51bcc069b0df809f2b9c9951';

        $arrayUsers = array(
            '+380954174915' => \Yii::$app->user->id,
            );

        $InfoSms = [
            'sid' => $sid,
            'token' => $token,
            'users' => $arrayUsers,
            'message' => 'Test: '.microtime(),
            'from_number' => '+12029913248',
            'company_id' => rand()
        ];

        return SmsInfo::sendSms($InfoSms);

//        return $this->render('index');
    }

}
