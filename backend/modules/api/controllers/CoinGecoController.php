<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\CoinGeco;
use yii\rest\Controller;

class CoinGecoController extends Controller
{

    /**
     * $data = $client->simple()->getPrice('0x,bitcoin', 'usd,rub');
     * @param $id string
     * @param $currency string
     * @return false|string
     */
    public function actionGetPrice($id = 'bitcoin', $currency = 'usd') {

        $client = new CoinGeco();
        $data = $client->simple()->getPrice($id, $currency);

        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetCurrencies() {

        $client = new CoinGeco();
        $data = $client->simple()->getSupportedVsCurrencies();

        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetCoin() {

        $client = new CoinGeco();
        $data = $client->coins()->getList();

        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetMarket($id = 'usd') {

        $client = new CoinGeco();
        $data = $result = $client->coins()->getMarkets($id);

        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetTickers() {

        $client = new CoinGeco();
        $data = $client->coins()->getTickers('bitcoin');

        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetExchanges() {

        $client = new CoinGeco();
        $data = $client->exchanges()->getExchanges();
        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetExchangeList() {

        $client = new CoinGeco();
        $data = $client->exchanges()->getList();
        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetExchange($id = 'binance') {

        $client = new CoinGeco();
        $data = $client->exchanges()->getExchange($id);
        return $this->asJson($data);
    }
    /**
     * @return false|string
     */
    public function actionGetExchangeTickers($id = 'binance', $params = ['coin_ids' => '0x,bitcoin']) {

        $client = new CoinGeco();
        $data = $client->exchanges()->getTickers($id, $params);
        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetExchangeRate() {

        $client = new CoinGeco();
        $data = $client->exchangeRates()->getExchangeRates();
        return $this->asJson($data);
    }

    /**
     * @return false|string
     */
    public function actionGetGlobal() {

        $client = new CoinGeco();
        $data = $client->globals()->getGlobal();
        return $this->asJson($data);
    }
}
