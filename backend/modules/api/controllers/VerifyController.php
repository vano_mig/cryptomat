<?php

namespace backend\modules\api\controllers;

use backend\modules\virtual_terminal\models\Atm;
use common\models\Log;
use Exception;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Request;
use Yoti\DocScan\DocScanClient;
use Yoti\DocScan\Session\Create\Check\RequestedDocumentAuthenticityCheckBuilder;
use Yoti\DocScan\Session\Create\Check\RequestedFaceMatchCheckBuilder;
use Yoti\DocScan\Session\Create\Check\RequestedLivenessCheckBuilder;
use Yoti\DocScan\Session\Create\SdkConfigBuilder;
use Yoti\DocScan\Session\Create\SessionSpecificationBuilder;
use Yoti\DocScan\Session\Create\Task\RequestedTextExtractionTaskBuilder;
use Yoti\DocScan\Session\Create\NotificationConfigBuilder;
use yii\helpers\Url;
use backend\modules\api\models\KycUser;

class VerifyController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Headers' => ['Content-type'],
            ],
        ];
        return $behaviors;
    }

    public function actionClient()
    {
        $result = [];
        $result['status'] = 'error';
        if($_POST) {
            if(array_key_exists('request', $_POST)) {
                try{
//                    $request = base64_decode($_POST['request']);
                    $request = json_decode($_POST['request'], true);
                    $atm = Atm::find()->where(['uid'=>$request['uid']])->limit(1)->one();
                    $verification_request = [
                        'reference' => 'ref-' . rand(4, 444) . rand(4, 444),
                        'country' => strtoupper($atm->country->iso_code),
                        'language' => strtoupper($atm->language),
                        'email' => Yii::$app->params['verifyEmail'],
                        'callback_url' => '',
                        'verification_mode' => 'any',
                    ];
                    $verification_request['face'] = [
                        'proof' => $request['face']['proof']
                    ];
                    $verification_request['document'] = [
                        'proof' => $request['document']['proof'],
                        'additional_proof' => $request['document']['additional_proof'],
                        'name' => [
                            'first_name' => $request['document']['name']['first_name'],
                            'last_name' => $request['document']['name']['last_name'],
                            'fuzzy_match' => '1',
                        ],
                        'dob' => $request['document']['dob'],
                        'document_number' => $request['document']['document_number'],
                        'expiry_date' => $request['document']['expiry_date'],
                        'issue_date' => $request['document']['issue_date'],
                        'supported_types' => ['id_card', 'passport', 'driver_licence'],
                        'gender' => $request['document']['gender'],
                    ];
                    $verification_request['address'] = [
                        'proof' =>$request['address']['proof'],
                        'name' => [
                            'first_name' => $request['document']['name']['first_name'],
                            'last_name' => $request['document']['name']['last_name'],
                            'fuzzy_match' => '1'
                        ],
                        'full_address' => $request['address']['full_address'],
                        'address_fuzzy_match' => '1',
                        'issue_date' => $request['address']['issue_date'],
                        'supported_types' => ['utility_bill', 'passport', 'bank_statement']
                    ];

                    $auth = $atm->company->profile->kys_client_id . ":" . $atm->company->profile->kys_secret_key;
                    $headers = ['Content-Type: application/json'];
                    $post_data = json_encode($verification_request);
                    $response = $this->send_curl($atm->company->profile->kys_url, $post_data, $headers, $auth);
                    $response_data = $response['body'];
                    $exploded = explode("\n", $response['headers']);
                    $sp_signature = trim(explode(':', $exploded[6])[1]);
                    $calculate_signature = hash('sha256', $response_data . $atm->company->profile->kys_secret_key);
                    $decoded_response = json_decode($response_data, true);
                    $event_name = $decoded_response['event'];

                    if ($event_name == 'verification.accepted') {
                        if ($sp_signature == $calculate_signature) {
                            $result['status'] = 'success';
                            $result['message'] = "Verification accepted : $response_data";
                        } else {
                            $result['status'] = 'error';
                            $result['error'] = "Invalid signature :  $response_data";
                        }
                    } else {
                        $result['status'] = 'error';
                        $result['error'] = $response_data;
                    }
                }catch(Exception $e) {
                    $result['status'] = 'error';
                    $result['error'] = $e->getMessage();
                }
            }
        } else {
            $result['error'] = Yii::t('api', 'Method is not allowed');
        }
        return $this->asJson($result);
    }

    function send_curl($url, $post_data, $headers, $auth)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $html_response = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($html_response, 0, $header_size);
        $body = substr($html_response, $header_size);
        curl_close($ch);
        return ['headers' => $headers, 'body' => $body];
    }





    public function actionCreateYotiSession(){
        $response = [];
        $response['status'] = 'success';

        if($_POST){
            $request = $_POST['request'];
            $request = base64_decode($request);
            $request = json_decode($request, true);
            $phone_number = $request['phone_number'];

            try{
                $user = KycUser::find()->where(['phone_number'=>$phone_number])->one();
                if(!$user ){
                    $YOTI_CLIENT_SDK_ID = '3d67a848-cc65-4148-97d2-1a4af2718039';
                    $YOTI_PEM = __DIR__.'/../../../assets/key/cryptomatic-verify-access-security.pem';

                    $client = new DocScanClient($YOTI_CLIENT_SDK_ID, $YOTI_PEM);

                    $sessionSpec = (new SessionSpecificationBuilder())
                        ->withClientSessionTokenTtl(600)
                        ->withResourcesTtl(90000)
                        ->withRequestedCheck(
                            (new RequestedDocumentAuthenticityCheckBuilder())
                                ->build()
                        )
                        ->withRequestedCheck(
                            (new RequestedLivenessCheckBuilder())
                                ->forZoomLiveness()
                                ->withMaxRetries(3)
                                ->build()
                        )
                        ->withRequestedCheck(
                            (new RequestedFaceMatchCheckBuilder())
                                ->withManualCheckFallback()
                                ->build()
                        )
                        ->withRequestedTask(
                            (new RequestedTextExtractionTaskBuilder())
                                ->withManualCheckAlways()
                                ->build()
                        )
                        ->withSdkConfig(
                            (new SdkConfigBuilder())
                                ->withAllowsCamera()
                                ->withPrimaryColour('#2d9fff')
                                ->withSecondaryColour('#FFFFFF')
                                ->withFontColour('#FFFFFF')
                                ->withLocale('en')
                                ->withPresetIssuingCountry('GBR')
                                ->withSuccessUrl('https://office.atmcryptomatic.com/api/default/success?phone='.$phone_number)
                                ->withErrorUrl('https://office.atmcryptomatic.com/api/default/error')
                                ->build()
                        )
                        ->withNotifications(
                            (new NotificationConfigBuilder())
                                ->withEndpoint('https://yourdomain.example/idverify/updates')
                                ->forResourceUpdate()
                                ->forTaskCompletion()
                                ->forCheckCompletion()
                                ->forSessionCompletion()
                                ->build()
                        )
                        ->build();
        
                    $session = $client->createSession($sessionSpec);
        
                    $sessionId = $session->getSessionId();
                    $sessionToken = $session->getClientSessionToken();
                    $response['verification'] = false;
                    $response['session_id'] = $sessionId;
                    $response['token'] = $sessionToken;
                    $response['link'] = 'https://api.yoti.com/idverify/v1/web/index.html?sessionID='.$sessionId.'&sessionToken='.$sessionToken;
                }else{
                    $response['verification'] = true;
                }
            }catch(Exception $e){
                $response['status'] = 'error';
                $response['error'] = $e->getMessage();
            }
        }

        
        return $this->asJson($response);
    }


    

}

