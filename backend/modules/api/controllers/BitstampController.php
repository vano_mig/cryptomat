<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\BitstampCryptoWithdrawal;
use backend\modules\api\models\BitstampTransactions;
use Yii;
use backend\modules\api\models\Bitstamp;
use backend\modules\api\models\BitstampSearch;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Ramsey\Uuid\Uuid;

/**
 * BitstampController implements the CRUD actions for bitstamp model.
 */
class BitstampController extends Controller
{

//	/**
//     * {@inheritdoc}
//     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }
//
//    /**
//     * Lists all bitstamp models.
//     * @return mixed
//     */
//    public function actionIndex()
//    {
//        $searchModel = new BitstampSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
//
//    /**
//     * Displays a single bitstamp model.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionCancelAllOrders()
//    {
//        return $this->curlV1('2', 'cancel_all_orders/', [], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBitcoinWithdrawal()
//    {
//        $data = $this->curlV1('2', 'bitcoin_withdrawal/', [
//            'amount' => 0,
//            'address' => 0
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'btc';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionRippleWithdrawal()
//    {
//        $data = $this->curlV1('2', 'ripple_withdrawal/', [
//            'amount' => 0,
//            'address' => 0
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'ripple';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBitcoinDepositAddress()
//    {
//        return $this->curlV1('2', 'bitcoin_deposit_address/', [], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionTradingPairsInfo()
//    {
//        return $this->curlV2('2', 'trading-pairs-info/', [], 'GET');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionRippleDepositAddress()
//    {
//        return  $this->curlV1('2', 'ripple_address/', [], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionUnconfirmedBtc()
//    {
//        return $this->curlV1('2', 'unconfirmed_btc/', [], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBalance()
//    {
//        return $this->curlV2('2', 'balance/', [], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionUserTransactions()
//    {
//        /**
//         * #offset => Skip that many transactions before returning results (default: 0, maximum: 200000).
//        If you need to export older history contact support OR use combination of limit and since_id parameters
//         * #limit => Limit result to that many transactions (default: 100; maximum: 1000).
//         * #since_timestamp (Optional) => Show only transactions from unix timestamp (for max 30 days old).
//         * #since_id (Optional) => Show only transactions from specified transaction id.
//         */
//        return $this->curlV2('2', 'user_transactions/', ['offset' => '0'], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionCryptoTransactions()
//    {
//        /**
//         * #limit => Limit result to that many transactions (default: 100; minimum: 1; maximum: 1000).
//         * #offset => Skip that many transactions before returning results (default: 0, maximum: 200000).
//         */
//        return $this->curlV2('2', 'crypto-transactions/', ['offset' => '0'], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionOpenOrders()
//    {
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//        return $this->curlV2('2', "open_orders/{$pair}/", ['offset' => '0'], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionCancelOrder()
//    {
//        return $this->curlV2('2', "cancel_order/", ['offset' => '0', 'id' => 0], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBuy()
//    {
//        /**
//         * #limit_price (Optional) => If the order gets executed, a new sell order will be placed, with "limit_price" as its price.
//         * #daily_order (Optional) => Opens buy limit order which will be canceled at 0:00 UTC unless it already has been executed. Possible value: True
//         * #ioc_order (Optional) => An Immediate-Or-Cancel (IOC) order is an order that must be executed immediately. Any portion of an IOC order that cannot be filled immediately will be cancelled. Possible value: True
//         * #fok_order (Optional) => A Fill-Or-Kill (FOK) order is an order that must be executed immediately in its entirety. If the order cannot be immediately executed in its entirety, it will be cancelled. Possible value: True
//         */
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//        $data = $this->curlV2('2', "buy/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//            'price' => '0'
//        ], 'POST');
//
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 1;
//            $model->limit_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBuyMarket()
//    {
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//        $data = $this->curlV2('2', "buy/market/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//        ], 'POST');
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 1;
//            $model->market_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBuyInstant()
//    {
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//        $data = $this->curlV2('2', "buy/instant/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//        ], 'POST');
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 1;
//            $model->instant_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionSell()
//    {
//        /**
//         * #limit_price (Optional) => If the order gets executed, a new sell order will be placed, with "limit_price" as its price.
//         * #daily_order (Optional) => Opens buy limit order which will be canceled at 0:00 UTC unless it already has been executed. Possible value: True
//         * #ioc_order (Optional) => An Immediate-Or-Cancel (IOC) order is an order that must be executed immediately. Any portion of an IOC order that cannot be filled immediately will be cancelled. Possible value: True
//         * #fok_order (Optional) => A Fill-Or-Kill (FOK) order is an order that must be executed immediately in its entirety. If the order cannot be immediately executed in its entirety, it will be cancelled. Possible value: True
//         */
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//        $data = $this->curlV2('2', "sell/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//            'price' => '0'
//        ], 'POST');
//
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 0;
//            $model->limit_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionSellMarket()
//    {
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//        $data = $this->curlV2('2', "sell/market/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//        ], 'POST');
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 0;
//            $model->market_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionSellInstant()
//    {
//        $post = Yii::$app->request->post();
//        $pair = !empty($post['currency_pair']) ? $post['currency_pair'] : 'all';
//
//        $data = $this->curlV2('2', "sell/instant/{$pair}/", [
//            'offset' => '0',
//            'amount' => '0',
//        ], 'POST');
//
//        if (!empty($data))
//        {
//            $model = new BitstampTransactions();
//            $model->type = 0;
//            $model->instant_order = 1;
//            $model->order_id = $data['id'];
//            $model->price = $data['price'];
//            $model->amount = $data['amount'];
//            $model->datetime = $data['datetime'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionWithdrawalRequests()
//    {
//        /**
//         * #timedelta (Optional, default 86400) => Withdrawal requests from number of seconds ago to now (max. 50000000).
//         */
//        return $this->curlV2('2', 'withdrawal-requests/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionXrpWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'xrp_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'xrp';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionLtcWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'ltc_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'ltc';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionEthWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'eth_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'eth';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBchWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'bch_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'bch';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionXlmWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'xlm_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'xlm';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionPaxWithdrawal()
//    {
//        /**
//         * #destination_tag (Optional) => Address destination tag.
//         */
//        $data = $this->curlV2('2', 'pax_withdrawal/', [
//            'offset' => '0',
//            'amount' => '0',
//            'address' => '0'
//        ], 'POST');
//
//        if ($data)
//        {
//            $model = new BitstampCryptoWithdrawal();
//            $model->amount = $data['amount'];
//            $model->currency = 'pax';
//            $model->transaction_id = $data['id'];
//            $model->datetime = $data['datetime'];
//            $model->address = $data['address'];
//            $model->user_id = Yii::$app->user->id;
//            $model->save();
//        }
//
//        return (!empty($model));
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionXrpDepositAddress()
//    {
//        return $this->curlV2('2', 'xrp_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionLtcDepositAddress()
//    {
//        return $this->curlV2('2', 'ltc_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionEthDepositAddress()
//    {
//        return $this->curlV2('2', 'eth_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionBchDepositAddress()
//    {
//        return $this->curlV2('2', 'bch_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionXlmDepositAddress()
//    {
//        return $this->curlV2('2', 'xlm_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    public function actionPaxDepositAddress()
//    {
//        return $this->curlV2('2', 'pax_address/', [
//            'offset' => '0',
//        ], 'POST');
//    }
//
//    /**
//     * @param $id
//     * @param $url
//     * @param $data
//     * @param string $type
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    private function curlV1($id, $url, $data, $type = 'GET')
//    {
//        $model = $this->findModel($id);
//        $nonce = explode(' ', microtime());
//        $nonce = $nonce[1].substr($nonce[0], 2, 3);
//
//        $message = $nonce . $model->client_id . $model->api_key;
//
//        $SIGNATURE = strtoupper(hash_hmac('sha256', $message, $model->api_secret));
//
//        $ch = curl_init();
//
//        $payload = [
//            'key' => $model->api_key,
//            'signature' => $SIGNATURE,
//            'nonce' => $nonce,
//        ];
//
//        $comb = array_merge($payload, $data);
//
//        curl_setopt($ch, CURLOPT_URL,            "https://www.bitstamp.net/api/{$url}" );
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
//
//        if ($type == 'POST')
//            curl_setopt($ch, CURLOPT_POST,       true );
//        curl_setopt($ch, CURLOPT_POSTFIELDS,  $comb);
//
//        $result = curl_exec($ch);
//
//        curl_close($ch);
//
//        return $result;
//    }
//
//    /**
//     * @param $id
//     * @param $url
//     * @param $data
//     * @param string $type
//     * @return bool|string
//     * @throws NotFoundHttpException
//     */
//    private function curlV2($id, $url, $data, $type = 'GET')
//    {
//        $model = $this->findModel($id);
//
//        $nonce = Uuid::uuid4()->toString();
//        $timestamp = explode(' ', microtime());
//        $timestamp = $timestamp[1].substr($timestamp[0], 2, 3);
//        $content_type = 'application/x-www-form-urlencoded';
//
//        $payload = [
//
//        ];
//
//        $comb = array_merge($payload, $data);
//
//        $payload_string = http_build_query($comb);
//
//        $message = 'BITSTAMP ' . $model->api_key .
//            $type .
//            'www.bitstamp.net' .
//            "/api/v2/{$url}" .
//            '' .
//            $content_type .
//            $nonce .
//            $timestamp .
//            'v2' .
//            $payload_string;
//
//        $SIGNATURE = hash_hmac('sha256', $message, $model->api_secret);
//
//        $headers = array(
//            'X-Auth: BITSTAMP ' . $model->api_key,
//            'X-Auth-Signature: '. $SIGNATURE,
//            'X-Auth-Nonce:'. $nonce,
//            'X-Auth-Timestamp:'. $timestamp,
//            'X-Auth-Version:'. 'v2',
//            'Content-Type:'. $content_type
//        );
//
//        $ch = curl_init();
//
//        curl_setopt($ch, CURLOPT_URL,            "https://www.bitstamp.net/api/v2/{$url}" );
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
//        curl_setopt($ch, CURLOPT_HEADER, 1);
//
//        if ($type == 'POST')
//            curl_setopt($ch, CURLOPT_POST,       true );
//        if (!empty($payload_string))
//        	curl_setopt($ch, CURLOPT_POSTFIELDS,  $payload_string);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//
//        $result = curl_exec($ch);
//
//        $headers = $this->get_headers_from_curl_response($result);
//
//        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
//        $result = substr($result, $header_size);
//
//        curl_close($ch);
//
//        $string_to_sign = ( $nonce . $timestamp . $headers['Content-Type']) . $result;
//        $signature_check = hash_hmac('sha256', $string_to_sign, $model->api_secret);
//
//        return ($type == 'GET' || $type == 'POST' && !empty($headers['X-Server-Auth-Signature']) && $signature_check == $headers['X-Server-Auth-Signature'] ? $result : null);
//    }
//
//    /**
//     * @param $response
//     * @return array
//     */
//    public function get_headers_from_curl_response($response)
//    {
//        $headers = array();
//
//        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
//
//        foreach (explode("\r\n", $header_text) as $i => $line)
//            if ($i === 0)
//                $headers['http_code'] = $line;
//            else
//            {
//                list ($key, $value) = explode(': ', $line);
//
//                $headers[$key] = $value;
//            }
//
//        return $headers;
//    }
//
//    /**
//     * Creates a new bitstamp model.
//     * If creation is successful, the browser will be redirected to the 'view' page.
//     * @return mixed
//     */
//    public function actionCreate()
//    {
//        $model = new Bitstamp();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }
//
//    /**
//     * Updates an existing bitstamp model.
//     * If update is successful, the browser will be redirected to the 'view' page.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
//
//    /**
//     * Deletes an existing bitstamp model.
//     * If deletion is successful, the browser will be redirected to the 'index' page.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }
//
//    /**
//     * Finds the bitstamp model based on its primary key value.
//     * If the model is not found, a 404 HTTP exception will be thrown.
//     * @param integer $id
//     * @return Bitstamp the loaded model
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    protected function findModel($id)
//    {
//        if (($model = Bitstamp::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException(Yii::t('API', 'The requested page does not exist.'));
//    }
}
