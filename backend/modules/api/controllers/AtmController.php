<?php

namespace backend\modules\api\controllers;

use backend\modules\admin\formatter\WalletFormatter;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\IdentificationWallets;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\admin\models\Transaction;
use backend\modules\api\models\Check;
use backend\modules\api\models\Coinmarketcap;
use backend\modules\user\models\CompanyProfit;
use backend\modules\user\models\User;
use backend\modules\user\models\UserProfile;
use backend\modules\user\models\WhiteList;
use backend\modules\virtual_terminal\models\Atm;
use backend\modules\virtual_terminal\models\AtmHaccp;
use backend\modules\virtual_terminal\models\VirtualTerminal;
use common\models\Log;
use Exception;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Request;

class AtmController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Headers' => ['Content-type'],
            ],

        ];

        return $behaviors;
    }

    /**
     * Initialization terminal
     * @return \yii\web\Response
     */
    public function actionInitializationVirtual()
    {
        $result = [];
        if ($_POST) {
            $request = $_POST['request'];
            $uid = $request['uid'];
            $vAtm = VirtualTerminal::findOne(['uid' => $uid]);
            if (!$vAtm) {
                $result['status'] = 500;
                $result['error'] = 'ATM was not found';
            } else {
                $vAtm->ip = isset($request['ip']) ? $request['ip'] : $_SERVER['REMOTE_ADDR'];
                $vAtm->save();
                $result['mode'] = $vAtm->mode;
                $result['country'] = $vAtm->country->code_3;
                $result['currency'] = $vAtm->currency->code;
                $result['key'] = $vAtm->mode == VirtualTerminal::MODE_TEST ? VirtualTerminal::KEY_TEST : VirtualTerminal::KEY_LIFE;
                $result['external_id'] = $vAtm->company->uid;
                $result['status'] = 200;
            }
        }
        return $this->asJson($result);
    }

    /**
     * Initialization ATM
     * @return yii\web\Response
     */
    public function actionInitialization()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $res['status'] = 'error';
        $messages['request'] = [];
        try {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            $company = $atm->company;
            $ipList = WhiteList::find()->where(['company_id'=>$company->id])->limit(1)->one();
            if($ipList) {
                $list = json_decode($ipList->ip_address, true);
                if(!in_array($ip, $list)) {
                    throw new Exception(Yii::t('api', 'wrong ip, access deny'));
                }
            }
            if ($atm !== null) {
                $currency = Currency::findOne($atm->profile->currency_id);
                $res['status'] = 'success';
                $res['atm_number'] = $atm->id;
                $res['email'] = $company->service_email;
                $res['language'] = $atm->language;
                $res['currency_cash'] = $currency->code;
                $res['fee'] = $atm->fee;
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('Initialization', $messages);
            Log::logAtm('Initialization-LINE', $e->getLine());
        }
        return $this->asJson($res);
    }

    /**
     * Set date of reload terminal
     * @return \yii\web\Response
     */
    public function actionReload()
    {
        $result = [];
        if ($_POST) {
            $request = $_POST['request'];
            $uid = $request['uid'];
            $date = $request['date'];
            $vAtm = VirtualTerminal::find()->where(['uid' => $uid])->one();
            if (!$vAtm) {
                $result['status'] = 500;
                $result['error'] = 'ATM was not found';
            } else {
                $vAtm->reload = $date;
                $vAtm->save();
                $result['status'] = 200;
                $result['date'] = $vAtm->reload;
            }
        }
        return $this->asJson($result);
    }

    /**
     * Get date of reload terminal
     * @return \yii\web\Response
     */
    public function actionCheckDate()
    {
        $result = [];
        $uid = $_GET['uid'];
        $vAtm = VirtualTerminal::find()->where(['uid' => $uid])->one();
        if (!$vAtm) {
            $result['status'] = 500;
            $result['error'] = 'ATM was not found';
        } else {
            $result['status'] = 200;
            $result['date'] = $vAtm->reload;
        }
        return $this->asJson($result);
    }

    public function actionGetExchangeRate()
    {
        $res['status'] = 'error';

        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;
                $model = Atm::findOne(['uid' => $post['uid']]);
                if ($model !== null) {
                    $last = Coinmarketcap::find()->where(['currency_id'=>$post['currency_code'],'currency_cash_id' => $model->profile->currency_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
                    unset($last['id']);
                    $res['info'] = $last;
                    $res['status'] = 'success';
                } else {
                    $res['info']['error'] = 'Error arguments!#1';
                    $messages['response'] = $res['info']['error'];
                    Log::logAtm('GetExchangeRate', $messages);
                }
            } catch (Exception $e) {
                $res['info']['error'] = $e->getMessage();
                $messages['response'] = $res['info']['error'];
                Log::logAtm('GetExchangeRate', $messages);
            }
        } else {
            $res['info']['error'] = 'Error arguments!#0';
            $messages['request'] = [];
            $messages['response'] = $res['info']['error'];
            Log::logAtm('GetExchangeRate', $messages);
        }
        return $this->asJson($res);
    }

    public function actionGetBalance()
    {
        $result['status'] = 'error';
        if (!empty($_POST) && isset($_POST['request'])) {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if (!$atm) {
                $result['info']['error'] = 'atm was not found';
            } else {
                $currency = CryptoCurrency::find()->where(['code'=>$post['currency']])->asArray()->one();
                $profile = $atm->profile;
                $profileWallet = IdentificationWallets::findOne(['profile_id'=>$profile->id, 'type'=>IdentificationWallets::TYPE_BUY, 'currency_id'=>$currency->id]);
                $wallet = ReplenishWallet::findOne(['id'=>$profileWallet->replenish_id_id, 'company_id'=>$atm->company_id]);
                $provider = $wallet->provider;
                $name = $provider->apiName->api_name;
                $path = "\\backend\modules\api\models\\" . $name;
                $config = [
                    'public_key' => $wallet->api_key,
                    'secret_key' => $wallet->api_secret,
                    'client_id' => $wallet->client_id
                ];
                $model = new $path($config);
                $result = $model->getBalance(['currency' => $post['currency']]);
            }
        }
        return $this->asJson($result);
    }

    public function actionCheckDeposit()
    {
        $res['status'] = 'error';
        $messages['request'] = [];
        try {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if ($atm !== null) {
                $transaction = Transaction::find()->where(['transaction_id'=>$post['txid']])->asArray()->one();
                $wallet = ReplenishWallet::findOne(['id'=>$transaction['api_id'], 'company_id'=>$atm->company_id]);
                $provider = $wallet->provider;
                $name = $provider->apiName->api_name;
                $path = "\\backend\modules\api\models\\" . $name;
                $config = [
                    'public_key' => $wallet->api_key,
                    'secret_key' => $wallet->api_secret,
                    'client_id' => $wallet->client_id
                ];
                $model = new $path($config);
                $res = $model->checkDeposit(['txid' => $post['txid'], 'language'=>$atm->language, ]);
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('actionCheckDeposit', $messages);
        }
        return $this->asJson($res);
    }

    public function actionCheckWithdrawal()
    {
        $res['status'] = 'error';
        $messages['request'] = [];
        try {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if ($atm !== null) {
                $transaction = Transaction::find()->where(['transaction_id'=>$post['txid']])->asArray()->one();
                $wallet = ReplenishWallet::findOne(['id'=>$transaction['api_id'], 'company_id'=>$atm->company_id]);
                $provider = $wallet->provider;
                $name = $provider->apiName->api_name;
                $path = "\\backend\modules\api\models\\" . $name;
                $config = [
                    'public_key' => $wallet->api_key,
                    'secret_key' => $wallet->api_secret,
                    'client_id' => $wallet->client_id
                ];
                $model = new $path($config);
                $res = $model->checkWithdrawal(['txid' => $post['txid']]);
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckWithdrawal', $messages);
        }
        return $this->asJson($res);
    }

    public function actionCreateObjectTransaction()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $result['status'] = 'error';
        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson(base64_decode($_POST['request']));
                $messages['request'] = $post;
                if (Check::validatePhone($post['phone_number'])) {
                    $atm = Atm::find()->where(['uid' => $post['uid']])->one();
                    $currency = CryptoCurrency::findOne(['code'=>$post['currency_code'], 'active' => CryptoCurrency::STATUS_ACTIVE]);
                    $type = Transaction::getTypeByName($post['type']);
                    $rate = Check::calcRate(null, $currency->id, $atm->profile->currency_id);
                    $money = $rate['rate'];
                    $price = $post['amount'] / $money;
                    $fee = $atm->fee != null ? $atm->fee : 3;
                    $priceToPay = round($price, $currency->precision);
//                    $price = round($price / (1 - ($fee * 0.01)) / $rate['rate'], $currency->precision);
                    $price = round($price / (1 - ($fee * 0.01)), $currency->precision);
                    $precisions = 1;
                    $count = $currency->precision;
                    for ($i = 0; $i < $count; $i++) {
                        $precisions = $precisions * 10;
                    }
                    $firstGold = ceil($price * $precisions) / $precisions;

                    $ipList = WhiteList::find()->where(['company_id'=>$atm->company_id])->limit(1)->one();
                    if($ipList) {
                        $list = json_decode($ipList->ip_address, true);
                        if(!in_array($ip, $list)) {
                            throw new Exception(Yii::t('api', 'wrong ip, access deny'));
                        }
                    }

                    $idWallet = IdentificationWallets::findOne(['profile_id'=>$atm->profile_id, 'type'=>IdentificationWallets::TYPE_SELL, 'currency_id'=>$currency->id]);
                    $wallet = ReplenishWallet::findOne(['id'=>$idWallet->replenish_id, 'company_id'=>$atm->company_id]);
                    $provider = $wallet->provider;
                    $name = $provider->apiName->api_name;
                    $path = "\\backend\modules\api\models\\" . $name;
                    $config = [
                        'public_key' => $wallet->api_key,
                        'secret_key' => $wallet->api_secret,
                        'client_id' => $wallet->client_id
                    ];
                    $array = [
                        'amount' => $post['amount'],
                        'type' => $type,
                        'currency' => $currency,
                        'currency_cash' => $atm->profile->currency->code,
                        'atm_id' => $atm->id,
                        'phone_number' => $post['phone_number'],
                        'api_id' => $wallet->id,
                        'price'=>$firstGold,
                        'company'=>$atm->company,
                        'price_to_pay'=>$priceToPay,
                        'language'=>$atm->language,
                    ];
                    $model = new $path($config);
                    $result = $model->createObjectTransaction($array);
                } else {
                    $result['info']['error'] = 'Phone number is invalid!';
                    $messages['response'] = $result['info']['error'];
                    Log::logAtm('CreateObjectTransaction', $messages);
                }
            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('CreateObjectTransaction', $messages);
            }
        }
        return $this->asJson($result);
    }

    public function actionCheckTransaction()
    {
        $result['status'] = 'error';
        $messages['request'] = [];
        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;
                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
                if ($atm !== null) {
                    $transaction = Transaction::find()->where(['transaction_id'=>$post['txid']])->asArray()->one();
                    if($transaction === null)
                        throw new Exception(Yii::t('api', 'Transaction not found', '', $atm->language));
                    $wallet = ReplenishWallet::findOne(['id'=>$transaction['api_id'], 'company_id'=>$atm->company_id]);
                    $provider = $wallet->provider;
                    $name = $provider->apiName->api_name;
                    $path = "\\backend\modules\api\models\\" . $name;
                    $config = [
                        'public_key' => $wallet->api_key,
                        'secret_key' => $wallet->api_secret,
                        'client_id' => $wallet->client_id
                    ];
                    $array = [
                        'txid' => $post['txid'],
                        'type' => $post['type'],
                        'test' => $post['test'] ?? 0,
                        'language' => $atm->language,
                    ];
                    $model = new $path($config);
                    $result = $model->checkTransaction($array);
                } else {
                    $result['info']['error'] = 'Fail to check transaction!';
                    $messages['response'] = $result['info']['error'];
                    Log::logAtm('CheckTransaction', $messages);
                }
            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('CheckTransaction', $messages);
            }
        }
        return $this->asJson($result);
    }

	public function actionTestBitstamp()
	{
		$atm = Atm::findOne(['uid' => 'vlad_atm_1']);
		$replenishWallets = $atm->getReplenishWallet($_GET['id']);
//		$exchangeWallets = $atm->exchangeWallets;
		echo "<pre>";
		var_dump($replenishWallets);
		echo "</pre>";
    }

    /**
     * Create transaction to buy crypto
     * @return \yii\web\Response
     */
    public function actionCreateCryptoTransaction()
    {
        $result['status'] = 'error';
        $messages['request'] = [];

        $ip = $_SERVER['REMOTE_ADDR'];

        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;

                $atm = Atm::findOne(['uid' => $post['uid']]);

                $ipList = WhiteList::find()->where(['company_id'=>$atm->company_id])->limit(1)->one();
                if($ipList) {
                    $list = json_decode($ipList->ip_address, true);
                    if(!in_array($ip, $list)) {
                        throw new Exception(Yii::t('api', 'wrong ip, access deny'));
                    }
                }
                $currency = CryptoCurrency::findOne(['code'=>$post['currency_code'], 'active' => CryptoCurrency::STATUS_ACTIVE]);
                $Rate = Check::calcRate(null, $currency->id, $atm->profile->currency_id);
                $money = $Rate['rate'];

				$fee = $atm->fee != null ? $atm->fee : 0.5;

				$wallets = $atm->getReplenishWallet($currency->id);
				if (isset($wallets->exchange_wallet))
					$exchangeWallets = ExchangeWallet::findOne(['id' => $wallets->exchange_wallet]);

                $precisions = 1;
                $count = $currency->precision;
                for ($i = 0; $i < $count; $i++) {
                    $precisions = $precisions * 10;
                }

                $apiFee = WalletFormatter::getFeeBuy($wallets);

				$amountToPrice = $post['amount'] / $money;

				$amountWithFee = $amountToPrice * (1 - ($fee * 0.01));
				$amountToPay = $amountWithFee + $apiFee['withdrawalFee'];

				$amountCrypto = ceil($amountToPay * $precisions) / $precisions;

				$amountMoney = round(($amountCrypto * $money) * (1 + ($apiFee['buyFee'] * 0.01)), 5);
				$amountMoneyToBuy = $amountMoney * (1 + (10 * 0.01));

				Log::logAtm('$amountCrypto', $amountCrypto);
				Log::logAtm('$amountMoneyToBuy', $amountMoneyToBuy);

                $walletAddress = preg_replace('/^.*:/', '', $post['wallet']);

				$cryptoBalance = WalletFormatter::getBalance($wallets);
                // if(!Check::validateWallet($walletAddress, $currency->address_regex)){
                //     throw new Exception('Invalid wallet');
                // }

                //Todo finish me please

				if ($post['amount'] != null && $cryptoBalance != false /*&& Check::validateWallet($walletAddress, $currency->address_regex)*/) {

					Log::logAtm('CreateCryptoTransaction $information - balance crypto', $cryptoBalance);

					if (isset($exchangeWallets)) {
						$moneyBalance = WalletFormatter::getBalanceCash($exchangeWallets);
						Log::logAtm('CreateCryptoTransaction $information - balance cash', $moneyBalance);
					}

					if (floatval($cryptoBalance) > floatval($amountCrypto) ||
						isset($moneyBalance) && floatval($moneyBalance) > floatval($amountMoneyToBuy)) {
						$transaction = new Transaction();
						$transaction->terminal_id = $atm->id;
						$transaction->type = Transaction::TYPE_BUY_CRYPTO;
						$transaction->amount_receive = $amountMoney;
						$transaction->currency_receive = $atm->profile->currency->id;
						$transaction->phone_number = null;
						$transaction->amount_sent = $amountCrypto;
						$transaction->currency_sent = $currency->id;
						$transaction->status = Transaction::AWAITING_CONFIRM;
						$transaction->transaction_id = null;
						$transaction->address = $walletAddress;
						$transaction->address_coin = null;
						$transaction->endtime = null;
						$transaction->api_id = $wallets->id;
						$transaction->coinmarketcap_id_2 = $Rate['id'];
						if ($transaction->validate()) {
							$transaction->save();
							$result['status'] = 'success';
							$result['system_id'] = $transaction->id;
							$result['amount'] = (string)$post['amount'];
							$result['amount_crypto'] = $amountCrypto;
							$result['wallet'] = $walletAddress;
							$result['id_for_printer'] = $transaction->id;
						} else {
							$result['info']['error'] = Yii::t('api', 'Internal server error!', '', $atm->language);
							$messages['response'] = [$result['info']['error'], 'amount' => $post['amount'], '$amountCrypto' => $amountCrypto, '$moneyBalance' => $moneyBalance ?? null, '$cryptoBalance' => $cryptoBalance];
							$messages['message'] = $transaction;
							Log::logAtm('CreateCryptoTransaction not validate', $messages);
						}
					} else {
						$result['info']['error'] = Yii::t('api', 'Error! There are no', '', $atm->language) . ' ' . strtoupper($post['currency_code']);
						$messages['response'] = [$result['info']['error'], 'amount' => $post['amount'], '$amountCrypto' => $amountCrypto, '$moneyBalance' => $moneyBalance ?? null, '$cryptoBalance' => $cryptoBalance];
						Log::logAtm('CreateCryptoTransaction are balance and wallet', $messages);
					}
				}

            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage() . ', file: ' . $e->getFile() . ", line: " . $e->getLine();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('CreateCryptoTransaction exception', $messages);
            }
        }
        return $this->asJson($result);
    }

    public function actionConfirmCryptoTransaction()
    {
        $result['status'] = 'error';
        $messages['request'] = [];
        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;

                $atm = Atm::findOne(['uid' => $post['uid']]);
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
                $model = Transaction::findOne(['id' => $post['system_id'], 'status' => [Transaction::AWAITING_CONFIRM]]);
                if ($model !== null) {
                    $wallet = ReplenishWallet::findOne(['id'=>$model->api_id, 'company_id'=>$atm->company_id]);
                    $provider = $wallet->provider;
                    $name = $provider->apiName->api_name;
                    $path = "\\backend\modules\api\models\\" . $name;
                    $config = [
                        'public_key' => $wallet->api_key,
                        'secret_key' => $wallet->api_secret,
                        'client_id' => $wallet->client_id
                    ];
                    $array = [
                        'amount_sent' => $model->amount_sent,
                        'amount_received' => $model->amount_receive,
                        'currency_sent' => $model->currency_sent,
                        'currency_receive' => $model->currency_receive,
                        'address' => $model->address,
                        'auto_confirm' => 1,
                        'wallet' => $wallet,
						'fee' => $atm->fee,
						'market_id' => $model->coinmarketcap_id_2,
                    ];
                    $models = new $path($config);
                    $result = $models->confirmCryptoTransaction($array);

                    if ($result['status'] == 'success') {
//                        $price = $model->address_coin;
                        Log::logAtm('CreateCrypto', $result);
                        $model->status = Transaction::STATUS_SEND_CRYPTO_SUCCESS;
//                        $model->address_coin = null;
                        $model->transaction_id = (string)$result['transaction_id'];
                        $model->save();

//                        $fee = new CompanyProfit();
//                        $fee->terminal_id = $atm->id;
//                        $fee->currency = $model->currency_receive;
//                        $fee->amount = (float)$price - $model->amount_sent;
//                        $fee->company_id = $atm->company_id;
//                        $fee->transaction_id = $model->id;
//                        $fee->mode = null;
//                        $fee->save();

                        $result['amount'] = (string)$model->amount_receive;
                        $result['amount_crypto'] = (string)$model->amount_sent;
//                        $result['transaction_id'] = $result['transaction_id'];
                        $result['wallet'] = $model->address;
                        $result['id_for_printer'] = $model->id;
                        $result['status'] = 'success';
                    } else {
                        $result['info']['error'] = Yii::t('api', 'Fail to create transaction!', '', $atm->language);
                        $messages['response'] = $result;
                        Log::logAtm('ConfirmCryptoTransaction buy', $messages);
                    }
                } else {
                    $result['info']['error'] = Yii::t('api', 'Fail to create transaction!', '', $atm->language);
                    $messages['response'] = $result['info']['error'];
                    Log::logAtm('ConfirmCryptoTransaction transaction not found', $messages);
                }
            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('ConfirmCryptoTransaction', $messages);
            }
        }
        return $this->asJson($result);
    }

    /**
     * Cancel transaction to buy crypto
     * @return \yii\web\Response
     */
    public function actionCancelCryptoTransaction()
    {
        $result['status'] = 'error';
        $messages['request'] = [];
        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;
                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
                $model = Transaction::findOne(['id' => $post['system_id'], 'status' => [Transaction::AWAITING_CONFIRM]]);
                if (!empty($atm) && !empty($model)) {
                    $result['status'] = 'success';
                    $model->status = Transaction::STATUS_CANCELED;
                    $model->save();
                } else {
                    $result['info']['error'] = 'Fail to cancel transaction!';
                    $messages['response'] = $result['info']['error'];
                    Log::logAtm('CancelCryptoTransaction', $messages);
                }
            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('CancelCryptoTransaction', $messages);
            }
        }
        return $this->asJson($result);
    }

    public function actionGetErrors()
    {
        $res['status'] = 'success';
        $messages['request'] = [];
        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;
                $model = Atm::findOne(['uid' => $post['uid']]);
                AtmHaccp::updateAll(['status' => AtmHaccp::STATUS_OLD], ['terminal_id' => $model->id]);
                $haccp = new AtmHaccp();
                $haccp->terminal_id = $model->id;
                $haccp->content = $post['error']['description'];
                $haccp->code = $post['error']['code'];
                $haccp->status = AtmHaccp::STATUS_NEW;
                $haccp->save();
            } catch (Exception $e) {
                $res['info']['error'] = $e->getMessage();
                $messages['response'] = $res['info']['error'];
                Log::logAtm('GetError', $messages);
            }
        }
        return $this->asJson($res);
    }

    /**
     * Change transaction status
     * @return \yii\web\Response
     */
    public function actionChangeStatusTransaction()
    {
        $result['status'] = 'error';

        if (!empty($_POST) && isset($_POST['request'])) {
            try {
                $post = Check::parseJson($_POST['request']);
                $messages['request'] = $post;
                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
                $model = Transaction::findOne(['transaction_id' => $post['txid'], 'status' => [Transaction::AWAITING_CONFIRM, Transaction::STATUS_SUCCESS]]);
                if ($atm !== null && !empty($model)) {
                    $status = Transaction::STATUS_SUCCESS;
                    if ($post['status'] == 1) {
                        if ($model->type == Transaction::TYPE_BUY_CASH) {
                            $status = Transaction::STATUS_SEND_CASH_SUCCESS;
                        } else {
                            $status = Transaction::STATUS_SEND_CRYPTO_SUCCESS;
                        }
                    }
                    $model->status = $status;
                    $model->save();
                    $result['status'] = 'success';
                } else {
                    $result['info']['error'] = 'Fail to change transaction!';
                }
            } catch (Exception $e) {
                $result['info']['error'] = $e->getMessage();
                $messages['response'] = $result['info']['error'];
                Log::logAtm('ChangeStatusTransaction', $messages);
            }
        }
        return $this->asJson($result);
    }

    /**
     * Check phone number
     * @return yii\web\Response
     */
    public function actionCheckPhoneNumber()
    {
        $res['status'] = 'error';
        try {
            $post = Check::parseJson(base64_decode($_POST['request']));
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if ($atm !== null && Check::validatePhone($post['phone_number'])) {
                $model = Transaction::findOne(['phone_number' => $post['phone_number'], 'status' => Transaction::STATUS_WAIT, Transaction::STATUS_SUCCESS, Transaction::STATUS_RECEIVE_CRYPTO]);
                if ($model !== null)
                    $res['status'] = 'success';
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckPhoneNumber', $messages);
        }

        return $this->asJson($res);
    }

    public function actionGetCryptoCurrency()
    {
        $res['status'] = 'error';
        $messages['request'] = [];
        $res['currencies'] = [];
        try {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if ($atm !== null) {
                $type = Transaction::getTypeByName($post['type']);
                if($type == Transaction::TYPE_BUY_CASH) {
                    $idWallets = IdentificationWallets::find()->where(['profile_id'=>$atm->profile_id, 'type'=>IdentificationWallets::TYPE_SELL])->all();
                } elseif ($type == Transaction::TYPE_BUY_CRYPTO) {
                    $idWallets = IdentificationWallets::find()->where(['profile_id'=>$atm->profile_id, 'type'=>IdentificationWallets::TYPE_BUY])->all();
                }
                if(!empty($idWallets)) {
                    foreach ($idWallets as $item) {
                        $rate = Check::calcRate(null, $item->cryptoCurrency->code, $atm->profile->currency_id);
                        $currency = $item->cryptoCurrency;
                        $res['status'] = 'success';
                        $res['currencies'][$currency->code] = [
                            'name'=>$currency->name,
                            'code'=>$currency->code,
                            'rate'=>$rate['rate'],
                            'precision'=>$currency->precision,
                            'icon'=>Yii::getAlias('@domain').$currency->icon,
                        ];
                    }
                }
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('Initialization', $messages);
        }
        return $this->asJson($res);
    }

    /**
     * Check phone number
     * @return yii\web\Response
     */
    public function actionCheckyc()
    {
        $res['status'] = 'error';
        try {
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            $profile = UserProfile::findOne(['qr_key'=>$post['qr_code']]);
            if ($atm !== null && $profile !== null) {
                $user = User::findOne($profile->user->id);
                if($user && $user->kyc == User::KYC_ACTIVE) {
                    $res['status'] = 'success';
                }
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckPhoneNumber', $messages);
        }

        return $this->asJson($res);
    }
}