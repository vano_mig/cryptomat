<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\KrakenAPIClient;
use backend\modules\api\models\KrakenAPIException;

class KrakenController extends \yii\web\Controller
{
	public $kraken;
	public $publicKey = 'wBRYTReH9Q1LESRng6u7dt1uXNHKDx60GwGVDeWCD9TQX66DrUcwpGpJ';
	private $privateKey = 'JyH6QNP8bK1UgnXFqJzcnDQYMr0pdtaLHOBXtGkhWsxMW5fMhzufMLaFiQZvfxz0XA+yLQytC+NShlu9qq2kUQ==';

	public function __construct($id, $module, $config = [])
	{
		$this->kraken = new KrakenAPIClient($this->publicKey, $this->privateKey);
		parent::__construct($id, $module, $config);
	}

	public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionBalance()
    {
		try {
			$res = $this->kraken->QueryPrivate('Balance');
		} catch (KrakenAPIException $e) {
			$res = $e;
		}
        return $this->asJson($res);
    }

}
