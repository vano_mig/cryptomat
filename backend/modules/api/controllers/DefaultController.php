<?php

namespace backend\modules\api\controllers;

use yii\web\Controller;
use backend\modules\api\models\KycUser;
use backend\modules\admin\models\CryptoCurrency;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSuccess($phone = null)
    {
        // $users = KycUser::find()->all();
        // echo '<pre>';print_r($users);echo '</pre>';die;
        // echo '<pre>';print_r($phone);echo '</pre>';die;
        // $currency = CryptoCurrency::find()->where(['code'=>'btc', 'active' => CryptoCurrency::STATUS_ACTIVE])->all();
        // echo '<pre>';print_r($currency);echo '</pre>';die;
        $user = KycUser::find()->where(['phone_number'=>$phone])->one();
        if($user && !$user->verification){
            $user->verification = true;
            $user->save();
        }elseif(!$user){
            $user = new KycUser;
            $user->phone_number = strval($phone);
            $user->verification = true;
            $user->save();
        }
        

        $this->layout = false;
        return $this->render('successView');
    }

    public function actionTestView(){
        $users = KycUser::find()->all();
        echo '<pre>';print_r($phone);echo '</pre>';die;
        $currency = CryptoCurrency::find()->where(['code'=>'btc', 'active' => CryptoCurrency::STATUS_ACTIVE])->all();
        echo '<pre>';print_r($currency);echo '</pre>';die;
    }

    public function actionTest(){
        $user = KycUser::find()->where(['phone_number' => '+380994322272'])->one();
        $user->phone_number = '+380994322272';
        $user->save();
        echo '<pre>';print_r($user);echo '</pre>';die;
        $user->phone_number = '55555555555';
        $user->verification = true;
        if($user->save()){
            return 'ddcsdf';
        }
    }

    public function actionError()
    {
        $this->layout = false;
        return $this->render('errorView');
    }

    public function actionV(){
        $users = KycUser::find()->all();
        echo '<pre>';print_r($users);echo '</pre>';die;
    }
}
