<?php

namespace backend\modules\api\controllers;

use yii\rest\Controller;

class BitfinexController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    private function curl($url, $data, $type = 'POST')
    {

        $api_key = '5BdxvuumlS9sJ2VLVZudMgHE73gBXWyqsAqLXd8SrfU';
        $API_SECRET = b'eDEIhuhkAi6bcPfzbNbkSJVhRjJjwxasy0XMfHSQ6K2';
        $content_type = 'application/json';

        $nonce = time() * 1000;

        $body = json_encode($data);

        $body = $body == "[]" ? "{}" : $body;

        $signature = '/api/' . $url . $nonce . $body;
        $sig = hash_hmac('sha384', $signature, $API_SECRET);


        $headers = array(
            'bfx-signature: ' . $sig,
            'bfx-apikey: '. $api_key,
            'bfx-nonce: '. $nonce,
            'Content-Type: '. $content_type
        );

        $ch = curl_init();

//        curl_setopt($ch, CURLOPT_VERBOSE, true);
//        curl_setopt($ch, CURLOPT_STDERR, $f);

        curl_setopt($ch, CURLOPT_URL,            "https://api.bitfinex.com/{$url}" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );

//        if ($type == 'POST')
            curl_setopt($ch, CURLOPT_POST,       true );
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function actionWallets()
    {
        return $this->curl('v2/auth/w/deposit/address', array('wallet'=> 'trading',
            'method'=> 'litecoin',
            'op_renew' => 0));
//        return $this->render('index');
    }

    public function actionRetrieveOrders()
    {

        return $this->curl('v2/auth/r/orders', array());
    }

    public function actionSubmitOrder()
    {
        $data = $this->curl('v2/auth/r/orders', array('type' => 'LIMIT',
            'symbol' => 'tBTCUSD',
            'price' => '15',
            'amount' => '0.001',
            'flags' => 0,
            'meta' => ['aff_code' => "AFF_CODE_HERE"]
        ));

        $info = $data[4][0];

        $result = array(
            'MTS' => $data[0],
            'TYPE' => $data[1],
            'MESSAGE_ID' => $data[2],
            'info' => array(
                'ID' => $info[0],
                'GID' => $info[1],
                'CID' => $info[2],
                'SYMBOL' => $info[3],
                'MTS_CREATE' => $info[4],
                'MTS_UPDATE' => $info[5],
                'AMOUNT' => $info[6],
                'AMOUNT_ORIG' => $info[7],
                'TYPE' => $info[8],
                'TYPE_PREV' => $info[9],
                'MTS_TIF' => $info[10],
                '_PLACEHOLDER' => $info[11],
                'FLAGS' => $info[12],
                'ORDER_STATUS' => $info[13],
                '_PLACEHOLDER2' => $info[14],
                '_PLACEHOLDER3' => $info[15],
                'PRICE' => $info[16],
                'PRICE_AVG' => $info[17],
                'PRICE_TRAILING' => $info[18],
                '_PLACEHOLDER7' => $info[19],
                '_PLACEHOLDER8' => $info[20],
                '_PLACEHOLDER9' => $info[21],
                'HIDDEN' => $info[22],
                'PLACED_ID' => $info[23],
                '_PLACEHOLDER10' => $info[24],
                '_PLACEHOLDER11' => $info[25],
                '_PLACEHOLDER12' => $info[26],
                'ROUTING' => $info[27],
                '_PLACEHOLDER13' => $info[28],
                '_PLACEHOLDER14' => $info[29],
                'META' => $info[30],
            ),
            'CODE' => $data[5],
            'STATUS' => $data[6],
            'TEXT' => $data[7],
        );

        return $result;
    }

    public function actionMarketAveragePrice()
    {
        $data = $this->curl('v2/auth/w/order/update', array(
            'id' => 12345,
            'price' => 15,
            'amount' => 0.001
        ));

        $info = $data[4][0];

        $result = array(
            'MTS' => $data[0],
            'TYPE' => $data[1],
            'MESSAGE_ID' => $data[2],
            'info' => array(
                'ID' => $info[0],
                'GID' => $info[1],
                'CID' => $info[2],
                'SYMBOL' => $info[3],
                'MTS_CREATE' => $info[4],
                'MTS_UPDATE' => $info[5],
                'AMOUNT' => $info[6],
                'AMOUNT_ORIG' => $info[7],
                'TYPE' => $info[8],
                'TYPE_PREV' => $info[9],
                'MTS_TIF' => $info[10],
                '_PLACEHOLDER' => $info[11],
                'FLAGS' => $info[12],
                'ORDER_STATUS' => $info[13],
                '_PLACEHOLDER2' => $info[14],
                '_PLACEHOLDER3' => $info[15],
                'PRICE' => $info[16],
                'PRICE_AVG' => $info[17],
                'PRICE_TRAILING' => $info[18],
                '_PLACEHOLDER7' => $info[19],
                '_PLACEHOLDER8' => $info[20],
                '_PLACEHOLDER9' => $info[21],
                'HIDDEN' => $info[22],
                'PLACED_ID' => $info[23],
                '_PLACEHOLDER10' => $info[24],
                '_PLACEHOLDER11' => $info[25],
                '_PLACEHOLDER12' => $info[26],
                'ROUTING' => $info[27],
                '_PLACEHOLDER13' => $info[28],
                '_PLACEHOLDER14' => $info[29],
                'META' => $info[30],
            ),
            'CODE' => $data[5],
            'STATUS' => $data[6],
            'TEXT' => $data[7],
        );

        return $result;

//        //Increase remaining amount by 0.1
//        {
//            id: 1149698545,
//            delta: '0.1',
//            price: '6500'
//        }
//
////Change both price parameters of a stop limit
//        {
//            id: 1149698545,
//            delta: '0.1',
//            price: '2500',
//            price_aux_limit: '2600',
//            flags: 64
//        }
//
////Set order expiration date
//        {
//            id: 1149698545,
//            tif: '2020-06-30'
//        }

    }

    public function actionOrderHistory()
    {
        $data = $this->curl('v2/auth/r/orders/tBTCUSD/hist', array());

        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'ID' => $value[0],
                'GID' => $value[1],
                'CID' => $value[2],
                'SYMBOL' => $value[3],
                'MTS_CREATE' => $value[4],
                'MTS_UPDATE' => $value[5],
                'AMOUNT' => $value[6],
                'AMOUNT_ORIG' => $value[7],
                'TYPE' => $value[8],
                'TYPE_PREV' => $value[9],
                'MTS_TIF' => $value[10],
                '_PLACEHOLDER' => $value[11],
                'FLAGS' => $value[12],
                'ORDER_STATUS' => $value[13],
                '_PLACEHOLDER2' => $value[14],
                '_PLACEHOLDER3' => $value[15],
                'PRICE' => $value[16],
                'PRICE_AVG' => $value[17],
                'PRICE_TRAILING' => $value[18],
                '_PLACEHOLDER7' => $value[19],
                '_PLACEHOLDER8' => $value[20],
                '_PLACEHOLDER9' => $value[21],
                'HIDDEN' => $value[22],
                'PLACED_ID' => $value[23],
                '_PLACEHOLDER10' => $value[24],
                '_PLACEHOLDER11' => $value[25],
                '_PLACEHOLDER12' => $value[26],
                'ROUTING' => $value[27],
                '_PLACEHOLDER13' => $value[28],
                '_PLACEHOLDER14' => $value[29],
                'META' => $value[30],
            );
        }

        return $result;
    }

    public function actionOrderTrades()
    {
        $data = $this->curl('v2/auth/r/order/tBTCUSD:12345/trades', array());

        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'ID' => $value[0],
                'PAIR' => $value[1],
                'MTS_CREATE' => $value[1],
                'ORDER_ID' => $value[2],
                'EXEC_AMOUNT' => $value[3],
                'EXEC_PRICE' => $value[4],
                '_PLACEHOLDER' => $value[5],
                '_PLACEHOLDER2' => $value[6],
                'MAKER' => $value[7],
                'FEE' => $value[8],
                'FEE_CURRENCY' => $value[9],
            );
        }

        return $result;
    }

    public function actionTrades()
    {

        $data = $this->curl('v2/auth/r/trades/tBTCUSD/hist', array());

        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'ID' => $value[0],
                'PAIR' => $value[1],
                'MTS_CREATE' => $value[1],
                'ORDER_ID' => $value[2],
                'EXEC_AMOUNT' => $value[3],
                'EXEC_PRICE' => $value[4],
                '_PLACEHOLDER' => $value[5],
                '_PLACEHOLDER2' => $value[6],
                'MAKER' => $value[7],
                'FEE' => $value[8],
                'FEE_CURRENCY' => $value[9],
            );
        }

        return $result;
    }

    public function actionLedgers()
    {

        $data = $this->curl('v2/auth/r/ledgers/BTC/hist', array());

        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'ID' => $value[0],
                'CURRENCY' => $value[1],
                'PLACEHOLDER' => $value[1],
                'MTS' => $value[2],
                'PLACEHOLDER2' => $value[3],
                'AMOUNT' => $value[4],
                'BALANCE' => $value[5],
                '_PLACEHOLDER3' => $value[6],
                'DESCRIPTION' => $value[7],
            );
        }

        return $result;
    }

    public function actionMarginInfoBase()
    {

        $data = $this->curl('v2/auth/r/info/margin/base', array());

        $value = $data[0][1];
        $result = array(
            'USER_PL' => $value[0],
            'USER_SWAPS' => $value[1],
            'MARGIN_BALANCE' => $value[2],
            'MARGIN_NET' => $value[3],
            'MARGIN_MIN' => $value[4],
        );

        return $result;
    }

    public function actionMarginInfoSymbol()
    {

        $data = $this->curl('v2/auth/r/info/margin/tBTCUSD', array());

        $value = $data[0][2];
        $result = array(
            'USER_PL' => $value[0],
            'USER_SWAPS' => $value[1],
            'MARGIN_BALANCE' => $value[2],
            'MARGIN_NET' => $value[3],
            'MARGIN_MIN' => $value[4],
            'TRADABLE_BALANCE' => $value[5],
            'GROSS_BALANCE' => $value[6],
            'BUY' => $value[7],
            'SELL' => $value[8],
        );

        return $result;
    }

    public function actionMarginInfoSymbolAll()
    {

        $data = $this->curl('v2/auth/r/info/margin/sym_all', array());

        $result = array();

        foreach ($data as $key => $value)
        {
            $info = $data[0][2];
            $result[$key] = array(
                'USER_PL' => $info[0],
                'USER_SWAPS' => $info[1],
                'MARGIN_BALANCE' => $info[2],
                'MARGIN_NET' => $info[3],
                'MARGIN_MIN' => $info[4],
                'TRADABLE_BALANCE' => $info[5],
                'GROSS_BALANCE' => $info[6],
                'BUY' => $info[7],
                'SELL' => $info[8],
            );
        }

        return $result;
    }

    public function actionRetrievePosition()
    {

        $data = $this->curl('v2/auth/r/positions', array());

        $result = array();

        foreach ($data as $key => $value)
        {
            $info = $data[0][4];
            $result[$key] = array(
                'MTS' => $data[0],
                'TYPE' => $data[1],
                'MESSAGE_ID' => $data[2],
                'info' => array(
                    'SYMBOL' => $info[0],
                    'POSITION_STATUS' => $info[1],
                    'AMOUNT' => $info[2],
                    'BASE_PRICE' => $info[3],
                    'MARGIN_FUNDING' => $info[4],
                    'MARGIN_FUNDING_TYPE' => $info[5],
                    'PLACEHOLDER' => $info[6],
                    'PLACEHOLDER2' => $info[7],
                    'PLACEHOLDER3' => $info[8],
                    'PLACEHOLDER4' => $info[9],
                    'PLACEHOLDER5' => $info[10],
                    'POSITION_ID' => $info[11],
                    'MTS_CREATE' => $info[12],
                    'MTS_UPDATE' => $info[13],
                    'PLACEHOLDER6' => $info[14],
                    'POS_TYPE' => $info[15],
                    'PLACEHOLDER7' => $info[16],
                    'COLLATERAL' => $info[17],
                    'MIN_COLLATERAL' => $info[18],
                    'META' => $info[19],
                ),
                'CODE' => $data[5],
                'STATUS' => $data[6],
                'TEXT' => $data[7],
            );
        }

        return $result;
    }

    public function actionClaimPosition()
    {

        $data = $this->curl('v2/auth/w/position/claim', array(
            'id' => 12345
        ));

        $result = array();

        foreach ($data as $key => $value)
        {
            $info = $data[0][2];
            $result[$key] = array(
                'SYMBOL' => $info[0],
                'STATUS' => $info[1],
                'AMOUNT' => $info[2],
                'BASE_PRICE' => $info[3],
                'MARGIN_FUNDING' => $info[4],
                'MARGIN_FUNDING_TYPE' => $info[5],
                'PL' => $info[6],
                'PL_PERC' => $info[7],
                'PRICE_LIQ' => $info[8],
                'LEVERAGE' => $info[9],
                '_PLACEHOLDER' => $info[10],
                'POSITION_ID' => $info[11],
                'MTS_CREATE' => $info[12],
                'MTS_UPDATE' => $info[13],
                '_PLACEHOLDER2' => $info[14],
                'TYPE' => $info[15],
                '_PLACEHOLDER3' => $info[16],
                'COLLATERAL' => $info[17],
                'COLLATERAL_MIN' => $info[18],
                'META' => $info[19],
            );
        }

        return $result;
    }

    public function actionPositionsHistory()
    {

        $data = $this->curl('v2/auth/r/positions/hist', array(
            'start' => 1544555024000,
            'end' => 1544555054000,
            'limit' => 20
        ));

        $result = array();

        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'SYMBOL' => $value[0],
                'STATUS' => $value[1],
                'BASE_PRICE' => $value[2],
                'MARGIN_FUNDING' => $value[3],
                'MARGIN_FUNDING_TYPE' => $value[4],
                '_PLACEHOLDER' => $value[5],
                '_PLACEHOLDER2' => $value[6],
                '_PLACEHOLDER3' => $value[7],
                '_PLACEHOLDER4' => $value[8],
                '_PLACEHOLDER5' => $value[9],
                'POSITION_ID' => $value[10],
                'MTSCREATE' => $value[11],
                'MTSUPDATE' => $value[11],
            );
        }

        return $result;
    }

    public function actionPositionsSnapshot()
    {

        $data = $this->curl('v2/auth/r/positions/snap', array(
            'start' => 1544555024000,
            'end' => 1544555054000,
            'limit' => 20
        ));

        $result = array();

        foreach ($data as $key => $value) {
            $result[$key] = array(
                'SYMBOL' => $value[0],
                'STATUS' => $value[1],
                'BASE_PRICE' => $value[2],
                'MARGIN_FUNDING' => $value[3],
                'MARGIN_FUNDING_TYPE' => $value[4],
                '_PLACEHOLDER' => $value[5],
                '_PLACEHOLDER2' => $value[6],
                '_PLACEHOLDER3' => $value[7],
                '_PLACEHOLDER4' => $value[8],
                '_PLACEHOLDER5' => $value[9],
                'POSITION_ID' => $value[10],
                'MTSCREATE' => $value[11],
                'MTSUPDATE' => $value[11],
            );
        }
    }

    public function actionPositionsAudit()
    {

        $data = $this->curl('v2/auth/r/positions/audit', array(
            'id' => [1, 2, 3], // id
        ));

        $result = array();

        foreach ($data as $key => $value)
        {
            $result[$key] = array(
                'SYMBOL' => $value[0],
                'STATUS' => $value[1],
                'BASE_PRICE' => $value[2],
                'MARGIN_FUNDING' => $value[3],
                'MARGIN_FUNDING_TYPE' => $value[4],
                '_PLACEHOLDER' => $value[5],
                '_PLACEHOLDER2' => $value[6],
                '_PLACEHOLDER3' => $value[7],
                '_PLACEHOLDER4' => $value[8],
                '_PLACEHOLDER5' => $value[9],
                'POSITION_ID' => $value[10],
                'MTSCREATE' => $value[11],
                'MTSUPDATE' => $value[12],
                '_PLACEHOLDER6' => $value[13],
                'TYPE' => $value[14],
                '_PLACEHOLDER7' => $value[15],
                'COLLATERAL' => $value[16],
                'COLLATERAL_MIN' => $value[17],
                'META' => $value[18],
            );
        }

        return $result;
    }

}
