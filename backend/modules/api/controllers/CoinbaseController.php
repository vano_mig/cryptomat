<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\CoinbaseTransaction;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Account;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Resource\Withdrawal;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Resource\Deposit;
use Coinbase\Wallet\Resource\Buy;
use Coinbase\Wallet\Resource\Sell;
use Yii;
use yii\rest\Controller;

/**
 * Class CoinbaseController
 * @package backend\modules\api\controllers
 */
class CoinbaseController extends Controller
{
//ACZ8D5X0IkRh7rge
//1BaPUyIarTPmVHRFWhl3COlpNdUqArM1
    private $apiKey = 'd8SpdLZtzXxSGROy';
    private $apiSecret = 'LZUAhJi0TubYSvny8PGqi6vZztKWPkJ7';

    private $configuration;
    private $client;

    public function __construct($id, $module, $config = [])
    {
        $this->configuration = Configuration::apiKey($this->apiKey, $this->apiSecret);
        $this->client = Client::create($this->configuration);
        parent::__construct($id, $module, $config);
    }

    /**
     * @return false|string
     */
    public function actionGetCurrencies() {

        $data = $this->client->getCurrencies();
        return $this->asJson($data);
//        return json_encode($data);
    }

    /**
     * @return false|string
     */
    public function actionGetAccountsDetails() {

        $this->client->getAccounts();
        $data = $this->client->decodeLastResponse();
        return $this->asJson($data);
//        return json_encode($data);
    }

    /**
     * @return false|string
     */
    public function actionGetAccountDetails() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {
            $data = $this->client->getAccount($post['currency']);
            $data = $data->getRawData();
            return $this->asJson($data);
//            return json_encode($data);
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionGetBuyPrice() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {
            $data = $this->client->getBuyPrice($post['currency']);
            return $this->asJson($data);
//            return json_encode($data);
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionGetSellPrice() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {
            $data = $this->client->getSellPrice($post['currency']);
            return $this->asJson($data);
//            return json_encode($data);
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionGetSpotPrice() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {
            $data = $this->client->getSpotPrice($post['currency']);
            return $this->asJson($data);
//            return json_encode($data);
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionBuy() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {

            $buy = new Buy([
                `${$post['currency']}Amount` => $post['amount']
            ]);

            $account = $this->client->getAccount('usd');

            $this->client->createAccountBuy($account, $buy);


            $data = $this->client->decodeLastResponse();

            $model = new CoinbaseTransaction();
            $model->user_id = Yii::$app->user->id;
            $model->transaction_id = $data['id'];
            $model->status = $data['status'];
            $model->payment_method_id = $data['payment_method']['id'];
            $model->transaction_id = $data['transaction']['id'];
            $model->amount = $data['amount']['amount'];
            $model->currency = $data['amount']['currency'];
            $model->total_amount = $data['total']['amount'];
            $model->total_currency = $data['total']['currency'];
            $model->fee_amount = $data['fee']['amount'];
            $model->fee_currency = $data['fee']['currency'];
            $model->subtotal_amount = $data['subtotal']['amount'];
            $model->subtotal_currency = $data['subtotal']['currency'];
            $model->created_at = $data['created_at'];
            $model->updated_at = $data['updated_at'];
            $model->resource = $data['resource'];
            $model->committed = $data['committed'];
            $model->instant = $data['instant'];
            $model->payout_at = $data['payout_at'];

            $model->save();
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionSell() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {

            $sell = new Sell([
                `${$post['currency']}Amount` => $post['aount']
            ]);

            $account = $this->client->getAccount('usd');

            $this->client->createAccountSell($account, $sell);

            $data = $this->client->decodeLastResponse();

            $model = new CoinbaseTransaction();
            $model->user_id = Yii::$app->user->id;
            $model->transaction_id = $data['id'];
            $model->status = $data['status'];
            $model->payment_method_id = $data['payment_method']['id'];
            $model->transaction_id = $data['transaction']['id'];
            $model->amount = $data['amount']['amount'];
            $model->currency = $data['amount']['currency'];
            $model->total_amount = $data['total']['amount'];
            $model->total_currency = $data['total']['currency'];
            $model->fee_amount = $data['fee']['amount'];
            $model->fee_currency = $data['fee']['currency'];
            $model->subtotal_amount = $data['subtotal']['amount'];
            $model->subtotal_currency = $data['subtotal']['currency'];
            $model->created_at = $data['created_at'];
            $model->updated_at = $data['updated_at'];
            $model->resource = $data['resource'];
            $model->committed = $data['committed'];
            $model->instant = $data['instant'];
            $model->payout_at = $data['payout_at'];

            $model->save();
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionDepositUSD() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {

            $deposit = new Deposit([
                'amount' => new Money($post['amount'], CurrencyCode::USD)
            ]);

            $account = $this->client->getAccount('usd');

            $this->client->createAccountDeposit($account, $deposit);
        }
        return false;
    }

    /**
     * @return false|string
     */
    public function actionWithdrawalUSD() {

        $post = Yii::$app->request->post();
        if ($post && $post['currency']) {

            $withdrawal = new Withdrawal([
                'amount' => new Money($post['amount'], CurrencyCode::USD)
            ]);

            $account = $this->client->getAccount('usd');

            $this->client->createAccountWithdrawal($account, $withdrawal);
        }
        return false;
    }

    public function actionIndex()
    {


        $accounts = $this->client->getAccounts();

        echo "<pre>";
        var_dump($accounts);
        echo "</pre>";
        foreach ($accounts as $key => $value) {
            $data = $value->getRawData();
//            print_r($data['id']);
            print_r($data['id']);
            print_r($data['name']);
            print_r($data['currency']);
            print_r($data['balance']['amount']);
            print_r($data['balance']['currency']);
            print_r($data['native_balance']['amount']);
            print_r($data['native_balance']['currency']);
            print_r("\n");
        }

		$account = $this->client->getAccount("BTC");
//		echo "<pre>";
//		var_dump($account);
//		echo "</pre>";


        return $this->asJson($account->getRawData());
//        return $this->render('index');
    }

}
