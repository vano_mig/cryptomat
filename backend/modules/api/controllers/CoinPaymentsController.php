<?php

namespace backend\modules\api\controllers;

use backend\modules\admin\models\Currency;
use backend\modules\admin\models\Transaction;
use backend\modules\api\models\Check;
use backend\modules\api\models\Coinmarketcap;
use backend\modules\api\models\CoinPayments;
use backend\modules\api\models\SmsInfo;
use backend\modules\virtual_terminal\models\Atm;
use backend\modules\virtual_terminal\models\AtmHaccp;
use common\models\Log;
use Exception;
use Yii;
use yii\filters\Cors;
use yii\helpers\Url;
use yii\rest\Controller;

class CoinPaymentsController extends Controller
{
    public $enableCsrfValidation = false;
    const BUY_GOLD = 'GOLD';
    const BUY_CASH = 'CASH';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Headers' => ['Content-type'],
            ],
        ];

        return $behaviors;
    }

//    /**
//     * Initialization ATM
//     * @return yii\web\Response
//     */
//    public function actionInitialization()
//    {
//        $res['status'] = 'error';
//        $messages['request'] = [];
//        try {
//            $post = Check::parseJson($_POST['request']);
//            $messages['request'] = $post;
//            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//            if ($atm !== null)
//                $res['status'] = 'success';
//            $res['atm_number'] = $atm->id;
//            $res['email'] = $atm->company->service_email;
//        } catch (Exception $e) {
//            $res['info']['error'] = $e->getMessage();
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('Initialization', $messages);
//        }
//        return $this->asJson($res);
//    }

//    public function checkDeposit()
//    {
//        $res['status'] = 'error';
//        $messages['request'] = [];
//        try {
//            $post = Check::parseJson($_POST['request']);
//            $messages['request'] = $post;
//            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//            if ($atm !== null) {
//                $companyProfile = $atm->companyProfile;
//                $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//                $info = $coin->getTxInfo($post['txid']);
//                $res['info'] = $info['result'];
//                $res['status'] = 'success';
//            }
//        } catch (Exception $e) {
//            $res['info']['error'] = $e->getMessage();
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('Initialization', $messages);
//        }
//        return $this->asJson($res);
//    }
//
//    public function actionCheckWithdrawal()
//    {
//        $res['status'] = 'error';
//        $messages['request'] = [];
//        try {
//            $post = Check::parseJson($_POST['request']);
//            $messages['request'] = $post;
//            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//            if ($atm !== null) {
//                $companyProfile = $atm->companyProfile;
//                $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//                $info = $coin->getWithdrawInfo($post['txid']);
//                $res['info'] = $info['result'];
//                $res['status'] = 'success';
//                Log::logAtm('CheckWithdrawal', ['result'=>$info, 'txid'=>$post['txid']] );
//            }
//        } catch (Exception $e) {
//            $res['info']['error'] = $e->getMessage();
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('CheckWithdrawal', $messages);
//        }
//        return $this->asJson($res);
//    }

//    /**
//     * Check phone number
//     * @return yii\web\Response
//     */
//    public function actionCheckPhoneNumber()
//    {
//        $res['status'] = 'error';
//        try {
//            $post = Check::parseJson(base64_decode($_POST['request']));
//            $messages['request'] = $post;
//            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//            if ($atm !== null && Check::validatePhone($post['phone_number'])) {
//                $model = Transaction::findOne(['phone_number' => $post['phone_number'], 'status' => Transaction::STATUS_WAIT, Transaction::STATUS_SUCCESS, Transaction::STATUS_RECEIVE_CRYPTO]);
//                if ($model !== null)
//                    $res['status'] = 'success';
//            }
//        } catch (Exception $e) {
//            $res['info']['error'] = $e->getMessage();
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('CheckPhoneNumber', $messages);
//        }
//
//        return $this->asJson($res);
//    }


    /**
     * Check phone number
     * @return yii\web\Response
     */
//    public function actionCheckBalance()
//    {
//        $res['status'] = 'error';
//        try {
//            $res['request'] = $_POST;
//            $post = Check::parseJson(base64_decode($_POST['request']));
//            $messages['request'] = $post;
//            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//            if ($atm !== null) {
//                $companyProfile = $atm->companyProfile;
//                $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//
//                $information = $coin->getBalance(strtoupper($atm->currency->code));
//                $res['info'] = $information;
//
//                $res['status'] = 'success';
//            }
//        } catch (Exception $e) {
//            $res['info']['error'] = $e->getMessage();
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('CheckPhoneNumber', $messages);
//        }
//
//        return $this->asJson($res);
//    }


    /**
     * Check phone number
     * @return yii\web\Response
     */
    public function actionCheckBalances()
    {
        $res['status'] = 'error';
        try {
            $res['request'] = $_POST;
            $post = Check::parseJson($_POST['request']);
            $messages['request'] = $post;
            $atm = Atm::find()->where(['uid' => $post['uid']])->one();
            if ($atm !== null) {
                $companyProfile = $atm->companyProfile;
                $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);

                $information = $coin->getBalances();
                $res['info'] = $information;

                $res['status'] = 'success';
            }
        } catch (Exception $e) {
            $res['info']['error'] = $e->getMessage();
            $messages['response'] = $res['info']['error'];
            Log::logAtm('CheckPhoneNumber', $messages);
        }

        return $this->asJson($res);
    }

//    /**
//     * Create transaction to receive golden or money
//     * @return \yii\web\Response
//     */
//    public function actionCreateObjectTransaction()
//    {
//        $result['status'] = 'error';
//
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson(base64_decode($_POST['request']));
//                $messages['request'] = $post;
//                if (Check::validatePhone($post['phone_number'])) {
//                    $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//                    $amount = Atm::asAmount($post['amount']);
//
//                    $company = $atm->company;
//                    $companyProfile = $atm->companyProfile;
//                    $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//
//                    $type = Transaction::getTypeByName($post['type']);
//                    $rate = Check::calcRate(null, $atm->currency_id)['rate'];
////                    $price = $type == Transaction::TYPE_BUY_CASH ? $amount / $rate : ( ($company->price * $rate) - 3 ) / $rate;
//                    $price = $type == Transaction::TYPE_BUY_CASH ? ($amount + 1) / $rate : ( ($company->price * $rate) * (1 - (0.6 * 0.01) ) ) / $rate;
////                    $price = $type == Transaction::TYPE_BUY_CASH ? $amount / $rate : ( ($company->price * $rate) * (1 - (0.6 * 0.01) ) ) / $rate;
//                    Log::logAtm('$price ->', $price);
////                    Log::logAtm('$rate ->', $rate);
////                    Log::logAtm('$price with 1% ->', ($amount + $rate * 1/100)/ $rate);
//                    $precision = 1;
//                    $count = $atm->currency->precision;
//                    for ($i=0; $i < $count; $i++) {
//                        $precision = $precision * 10;
//                    }
//                    $Check = ceil($price * $precision)/$precision;
//                    $CheckWITH = ceil(($amount + $rate * 1/100)/ $rate * $precision)/$precision;
////                    $Check = round($price, 4);
//                    $data = $coin->createTransaction($Check, strtoupper($atm->currency->code), Yii::$app->params['ivanEmail']);
//                    Log::logAtm('$Check ->', $Check);
//                    Log::logAtm('$data ->', $data);
//                    $currency = Currency::find()->where(['code'=>'USD'])->one();
//                    // TODO: DELETE THIS
//                    if ($data['error'] == 'ok' /*&& random_int(0, 1) == 1*/) {
//                        $transaction = new Transaction();
//                        $transaction->terminal_id = $atm->id;
//                        $transaction->type = $type;
//                        $transaction->amount_sent = $amount;
////                        $transaction->currency_sent = strtoupper($atm->currency->code);
//                        $transaction->currency_sent = $type == Transaction::TYPE_BUY_CASH ? $currency->code : strtoupper($atm->currency->code);
//                        $transaction->amount_receive = $type == Transaction::TYPE_BUY_CASH ? $data['result']['amount'] : $company->price;
////                        $transaction->currency_receive = $type == Transaction::TYPE_BUY_CASH ? $currency->code : strtoupper($atm->currency->code);
//                        $transaction->currency_receive = strtoupper($atm->currency->code);
//                        $transaction->status = Transaction::STATUS_WAIT;
//                        $transaction->transaction_id = $data['result']['txn_id'];
//                        $transaction->phone_number = $post['phone_number'];
//                        $transaction->address = $data['result']['address'];
//                        $transaction->address_coin = $data['result']['address'];
//                        $transaction->confirm_needed = $data['result']['confirms_needed'];
//                        $transaction->endtime = time() + $data['result']['timeout'];
//                        if(!$transaction->save()) {
//                            Log::logAtm('error', $transaction);
//                        }
//
//                        $sid = $companyProfile->twilio_sid;
//                        $token = $companyProfile->twilio_token;
//
//                        $arrayUsers = array(
//                            $post['phone_number'] => $data['result']['address'],
//                        );
//
//                        $message = $type == Transaction::TYPE_BUY_CASH ? Yii::t("api", "address, after payment use this QR Code to receive money")  : Yii::t("api", "address, after payment use this QR Code to receive gold") ;
//                        $InfoSms = [
//                            'sid' => $sid,
//                            'token' => $token,
//                            'users' => $arrayUsers,
//                            'message' => Yii::t("api", "Pay") . " " . $transaction->amount_receive . " " . Yii::t("api", "to") . " " . $data['result']['address'] . " " . $message.', '.Yii::getAlias('@domain') . Url::to(['/admin/qr-code/get-qr-code', 'id' => $data['result']['txn_id']]),
//                            Yii::getAlias('@domain') . Url::to(['/admin/qr-code/get-qr-code', 'id' => $data['result']['txn_id']]),
//                            'from_number' => $companyProfile->twilio_from_number,
//                            'company_id' => $company->id
//                        ];
//                        SmsInfo::sendSms($InfoSms);
//
//                        $result['amount_crypto'] = $transaction->amount_receive;
//                        $result['currency'] = strtoupper($atm->currency->code);
//                        $result['timeout'] = $data['result']['timeout'];
//                        $result['wallet'] = $data['result']['address'];
//                        $result['qrcode_url'] = Yii::getAlias('@domain') . Url::to(['/site/scan-qr', 'id' => $data['result']['txn_id']]);
//                        $result['transaction_id'] = $data['result']['txn_id'];
//                        $result['id_for_printer'] = $transaction->id;
//                        $result['status'] = 'success';
//                        Log::logAtm('CreateObjectTr', $data);
//                        Log::logAtm('$result', $result);
//                    } else {
//                        $result['info']['error'] = Yii::t('api','Fail to create transaction!', '', $atm->language_id);
//                        $messages['response'] = $result['info']['error'];
//                        Log::logAtm('CreateObjectTransaction', $messages);
//                    }
//
//                } else {
//                    $result['info']['error'] = 'Phone number is invalid!';
//                    $messages['response'] = $result['info']['error'];
//                    Log::logAtm('CreateObjectTransaction', $messages);
//                }
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('CreateObjectTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }

    /**
     * Change transaction status
     * @return \yii\web\Response
     */
//    public function actionChangeStatusTransaction()
//    {
//        $result['status'] = 'error';
//
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//                // TODO: Delete `Transaction::STATUS_USED` on production!
////                $model = Transaction::findOne(['transaction_id' => $post['txid'], 'status' => [Transaction::STATUS_SUCCESS, Transaction::STATUS_USED]]);
//                $model = Transaction::findOne(['transaction_id' => $post['txid'], 'status' => [Transaction::AWAITING_CONFIRM, Transaction::STATUS_SUCCESS]]);
//                if ($atm !== null && !empty($model)) {
//                    $status = Transaction::STATUS_SUCCESS;
//                    if($post['status'] == 1) {
//                        if($model->type ==Transaction::TYPE_BUY_CASH) {
//                            $status = Transaction::STATUS_SEND_CASH_SUCCESS;
//                        } else {
//                            $status = Transaction::STATUS_USED;
//                        }
//                    }
////                    $model->status = $post['status'] == 1 ? Transaction::STATUS_USED : Transaction::STATUS_SUCCESS;
//                    $model->status = $status;
//                    $model->save();
//                    $result['status'] = 'success';
//                } else {
//                    $result['info']['error'] = 'Fail to change transaction!';
//                }
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('ChangeStatusTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }

    /**
     * Check transaction status
     * @return \yii\web\Response
     */
//    public function actionCheckTransaction()
//    {
//        $result['status'] = 'error';
//        $messages['request'] = [];
//
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//                $model = Transaction::findOne(['transaction_id' => $post['txid'], 'status' => [Transaction::STATUS_WAIT, Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO]]);
//
//                $type = Transaction::getTypeByName($post['type']);
////                Log::logAtm('Check', $type. ' '.$model->type);
//                // TODO: DELETE THIS
//                if ($atm !== null && $model !== null && $type == $model->type /*&& random_int(0, 1) == 1*/) {
//                    $companyProfile = $atm->companyProfile;
//                    $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//                    if($model->status == Transaction::STATUS_SUCCESS) {
//                        $result['info']['amount_crypto'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
//                        $result['info']['received_crypto'] = $model->amount_receive;
//                        $result['info']['payment_wallet'] = $model->address;
//                        $result['info']['status'] =  1 ;
//                        $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
//                        $result['info']['type'] = Transaction::getTypeByid($model->type);
//                        $result['info']['id_for_printer'] = $model->id;
//                        $result['status'] = 'success';
//                        $model->status = Transaction::AWAITING_CONFIRM;
//                        $model->save();
//                    } else {
//                        $info = $coin->getTxInfo($post['txid']);
//                        Log::logAtm('Check', $info);
//                        $info = $info['result'];
//                        if ((isset($post['test']) && $post['test'] == 1)) {
//                            $result['info']['text'] = 'success';
//                            $result['info']['amount_crypto'] = (string)$model->amount_receive;
//                            $result['info']['received_crypto'] = (string)$model->amount_receive;
//                            $result['info']['payment_wallet'] = $model->address;
//                            $result['info']['status'] = 'success';
//                            $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
//                            $result['info']['type'] = Transaction::getTypeByid($model->type);
//                            $result['info']['id_for_printer'] = $model->id;
//                            $result['status'] = 'success';
//                            $model->status = Transaction::AWAITING_CONFIRM;
//                            $model->save();
//                        } else if (!empty($info) && $info['amount'] <= $info['received'] && $info['type'] == 'coins'
//                            && ($info['status'] == 100 || $info['status'] == 1)) {
//                            $result['info']['text'] = $info['status_text'];
//                            $result['info']['amount_crypto'] = (string)$info['amountf'];
//                            $result['info']['received_crypto'] = (string)$info['receivedf'];
//                            $result['info']['payment_wallet'] = $info['payment_address'];
//                            $result['info']['status'] = $info['status'] == 0 ? 1 : $info['status'];
//                            $result['info']['give_amount'] = $type == Transaction::TYPE_BUY_CASH ? (int)$model->amount_sent : 1;
//                            $result['info']['type'] = Transaction::getTypeByid($model->type);
//                            $result['info']['id_for_printer'] = $model->id;
//                            $result['status'] = 'success';
//                            $model->status = Transaction::AWAITING_CONFIRM;
//                            $model->save();
//                        } else {
//                            Log::logAtm('Info', $info);
//                            $result['info']['error'] = Yii::t('api','Transaction not found!', '', $atm->language_id);
//                            $messages['response'] = $result['info']['error'];
//                            Log::logAtm('CheckTransaction', $messages);
//                        }
//                    }
//                } else {
//                    $result['info']['error'] = 'Fail to check transaction!';
//                    $messages['response'] = $result['info']['error'];
//                    Log::logAtm('CheckTransaction', $messages);
//                }
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('CheckTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }
//
//    // TODO: Добавить историю баланса
//
    /**
     * Create transaction to buy crypto
     * @return \yii\web\Response
     */
//    public function actionCreateCryptoTransaction()
//    {
//        $result['status'] = 'error';
//        $messages['request'] = [];
//
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
////                Log::logAtm('atm', $atm);
//                $amount = Atm::asAmount($post['amount']);
//                $wallet = $post['wallet'];
//                $Check = round($amount / Check::calcRate(null, $atm->currency_id)['rate'], $atm->currency->precision, 1);
//                $companyProfile = $atm->companyProfile;
//
//                $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key, 'secret_key' => $companyProfile->coin_private_key]);
//
//                $information = $coin->getBalance(strtoupper($atm->currency->code));
//                // TODO: DELETE THIS
//                if (Check::validateWallet($wallet, $atm->currency->address_regex) && $amount != null &&
//                    floatval($information) > floatval($Check) /*&& random_int(0, 1) == 1*/) {
//                    $currency = Currency::find()->where(['code'=>'USD'])->one();
//                    $transaction = new Transaction();
//                    $transaction->terminal_id = $atm->id;
//                    $transaction->type = Transaction::TYPE_BUY_CRYPTO;
//                    $transaction->amount_receive = $amount;
//                    $transaction->currency_receive = $currency->code;
//                    $transaction->phone_number = null;
//                    $transaction->amount_sent = $Check;
//                    $transaction->currency_sent = strtoupper($atm->currency->code);
//                    $transaction->status = Transaction::AWAITING_CONFIRM;
//                    $transaction->transaction_id = null;
//                    $transaction->address = $wallet;
//                    $transaction->endtime = time() + 7200;
//                    if ($transaction->validate()) {
//                        $transaction->save();
//                        $result['status'] = 'success';
//                        $result['system_id'] = $transaction->id;
//                        $result['amount'] = (string)$amount;
//                        $result['amount_crypto'] = (string)$Check;
//                        $result['wallet'] = $wallet;
//                        $result['id_for_printer'] = $transaction->id;
//                    } else {
//                        $result['info']['error'] = Yii::t('api', 'Internal server error!', '', $atm->language_id);
//                        $messages['response'] = [$result['info']['error'], 'amount'=>$amount, 'Check'=>$Check, 'getBalance'=>$information];
//                        Log::logAtm('CreateCryptoTransaction', $messages);
//                    }
//                } else {
//                    $result['info']['error'] = Yii::t('api','Error! There are no', '', $atm->language_id).' '.strtoupper($atm->currency->code);
//                    $messages['response'] = [$result['info']['error'], 'amount'=>$amount, 'Check'=>$Check, 'getBalance'=>$information];
//                    Log::logAtm('CreateCryptoTransaction', $messages);
//                }
//
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('CreateCryptoTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }

//    /**
//     * Confirm transaction to buy crypto
//     * @return \yii\web\Response
//     */
//    public function actionConfirmCryptoTransaction()
//    {
//        $result['status'] = 'error';
//        $messages['request'] = [];
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//                $companyProfile = $atm->companyProfile;
//                $model = Transaction::findOne(['id' => $post['id'], 'status' => [Transaction::AWAITING_CONFIRM]]);
//                if ($model !== null) {
//                    $coin = new CoinPayments(['public_key' => $companyProfile->coin_public_key,
//                        'secret_key' => $companyProfile->coin_private_key]);
//                    $fields = [
//                        'amount' => $model->amount_sent,
//                        'currency' => strtoupper($atm->currency->code),
//                        'address' => $model->address,
//                        'auto_confirm' => 1
//                    ];
//                    // TODO: update information amount
//                    $information = $coin->createWithdrawal($fields);
////					$information = ['error' => 'ok', 'result' => ['id' => 'test'.$model->id]];
//                    // TODO: DELETE THIS
//                    if ($information['error'] == 'ok' /*&& random_int(0, 1) == 1*/) {
//                        Log::logAtm('CreateCrypto', $information);
//                        $model->status = Transaction::STATUS_WAIT;
//                        $model->transaction_id = $information['result']['id'];
//                        $model->save();
//
//                        $result['amount'] = (string)$model->amount_receive;
//                        $result['amount_crypto'] = (string)$model->amount_sent;
//                        $result['transaction_id'] = $information['result']['id'];
//                        $result['wallet'] = $model->address;
//                        $result['id_for_printer'] = $model->id;
//                        $result['status'] = 'success';
//                    } else {
//                        $result['info']['error'] = Yii::t('api','Fail to create transaction!', '', $atm->language_id);
//                        $messages['response'] = $result['info']['error'];
//                        Log::logAtm('ConfirmCryptoTransaction', $messages);
//                        //						throw new Exception($information['error']);
//                    }
//                } else {
//                    $result['info']['error'] = Yii::t('api','Fail to create transaction!', '', $atm->language_id);
//                    $messages['response'] = $result['info']['error'];
//                    Log::logAtm('ConfirmCryptoTransaction', $messages);
//                }
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('ConfirmCryptoTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }

    /**
     * Cancel transaction to buy crypto
     * @return \yii\web\Response
     */
//    public function actionCancelCryptoTransaction()
//    {
//        $result['status'] = 'error';
//        $messages['request'] = [];
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $atm = Atm::find()->where(['uid' => $post['uid']])->one();
//                $model = Transaction::findOne(['id' => $post['id'], 'status' => [Transaction::AWAITING_CONFIRM]]);
//                if (!empty($atm) && !empty($model)) {
//                    $result['status'] = 'success';
//                    $model->status = Transaction::STATUS_CANCELED;
//                    $model->save();
//                } else {
//                    $result['info']['error'] = 'Fail to cancel transaction!';
//                    $messages['response'] = $result['info']['error'];
//                    Log::logAtm('CancelCryptoTransaction', $messages);
//                }
//            } catch (Exception $e) {
//                $result['info']['error'] = $e->getMessage();
//                $messages['response'] = $result['info']['error'];
//                Log::logAtm('CancelCryptoTransaction', $messages);
//            }
//        }
//        return $this->asJson($result);
//    }

//    /**
//     * Get exchange rate information
//     * @return \yii\web\Response
//     */
//    public function actionGetExchangeRate()
//    {
//        $res['status'] = 'error';
//
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $model = Atm::findOne(['uid' => $post['uid']]);
////                Log::logAtm('exchangeRate ATM------>', $model);
//                if (!empty($model)) {
//                    $last = Coinmarketcap::find()->where(['currency_id' => $model->currency_id])->orderBy(['id' => SORT_DESC])->asArray()->one();
////                    Log::logAtm('exchangeRate------>', $last);
//                    unset($last['id']);
//                    $res['info'] = $last;
//                    $res['status'] = 'success';
//
//                } else {
//                    $res['info']['error'] = 'Error arguments!#1';
//                    $messages['response'] = $res['info']['error'];
//                    Log::logAtm('GetExchangeRate', $messages);
//                }
//            } catch (Exception $e) {
//                $res['info']['error'] = $e->getMessage();
//                $messages['response'] = $res['info']['error'];
//                Log::logAtm('GetExchangeRate', $messages);
//            }
//        } else {
//            $res['info']['error'] = 'Error arguments!#0';
//            $messages['request'] = [];
//            $messages['response'] = $res['info']['error'];
//            Log::logAtm('GetExchangeRate', $messages);
//        }
//        return $this->asJson($res);
//    }

//    public function actionGetErrors()
//    {
//        $res['status'] = 'success';
//        $messages['request'] = [];
//        if (!empty($_POST) && isset($_POST['request'])) {
//            try {
//                $post = Check::parseJson($_POST['request']);
//                $messages['request'] = $post;
//                $model = Atm::findOne(['uid' => $post['uid']]);
//                AtmHaccp::updateAll(['status'=>AtmHaccp::STATUS_OLD], ['terminal_id'=>$model->id]);
//                $haccp = new AtmHaccp();
//                $haccp->terminal_id = $model->id;
//                $haccp->content = $post['error']['error_description'];
//                $haccp->code = $post['error']['code'];
//                $haccp->status = AtmHaccp::STATUS_NEW;
//                $haccp->save();
//            } catch (Exception $e) {
//                $res['info']['error'] = $e->getMessage();
//                $messages['response'] = $res['info']['error'];
//                Log::logAtm('GetError', $messages);
//            }
//        }
//        return $this->asJson($res);
//    }
}