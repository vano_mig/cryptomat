<?php

namespace backend\modules\api\controllers;

class BittrexController extends \yii\web\Controller
{
    private $api_key = '3b8f2ad0d2b24910987fe7d2061129dc';
    private $API_SECRET = b'ab0efef530584561acc14453d6f11ae6';

    /**
     * @param $url
     * @param array $data
     * @param string $type
     * @return bool|string
     */
    private function curl($url, $data = [], $type = 'GET')
    {
        $nonce = time() * 1000;
        $content_type = 'application/json';

        $data = !empty($data) ? json_encode($data) : "";
        $body2 = hash('sha512', $data);
        $signature = $nonce . 'https://api.bittrex.com/v3/' . $url . $type . $body2;
        $SIGNATURE = hash_hmac('sha512', $signature, $this->API_SECRET);
        $headers = array(
            'Api-Signature: ' . $SIGNATURE,
            'Api-Key: '. $this->api_key,
            'Api-Timestamp: '. $nonce,
            'Api-Content-Hash: '. $body2,
            'Content-Type: '. $content_type
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            "https://api.bittrex.com/v3/{$url}" );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        if ($type == 'POST')
            curl_setopt($ch, CURLOPT_POST,           true );
        if (!empty($data))
            curl_setopt($ch, CURLOPT_POSTFIELDS,     $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return curl_exec($ch);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return bool|string
     */
    public function actionBalanceCurrency()
    {

        return $this->curl('balances/btc');
    }

    /**
     * @return bool|string
     */
    public function actionBalance()
    {

        return $this->curl('balances');
    }

    /**
     * @return bool|string
     */
    public function actionAddresses()
    {

        return $this->curl('addresses');
    }

    /**
     * @return bool|string
     */
    public function actionAccount()
    {

        return $this->curl('account');
    }

    /**
     * @return bool|string
     */
    public function actionAccountVolume()
    {

        return $this->curl('account/volume');
    }

    /**
     * @return bool|string
     */
    public function actionAddressesPost()
    {

        return $this->curl('addresses', array('currencySymbol' => "btc"), 'POST');
    }

    /**
     * @return bool|string
     */
    public function actionAddressesCurrency()
    {

        return $this->curl('addresses/btc');
    }

    /**
     * @return bool|string
     */
    public function actionConditionalOrdersCreate()
    {

        return $this->curl('conditional-orders/', array(
            'marketSymbol' => 'string',
            'operand' => 'string',
            'triggerPrice' => 'number (double)',
            'trailingStopPercent' => 'number (double)',
            'orderToCreate' => array(
                    'marketSymbol' => 'string',
                    'direction' => 'string',
                    'type' => 'string',
                    'quantity' => 'number (double)',
                    'ceiling' => 'number (double)',
                    'limit' => 'number (double)',
                    'timeInForce' => 'string',
                    'clientOrderId' => 'string (uuid)',
                    'useAwards' => 'boolean',
                ),
            'orderToCancel' => array(
                    'type' => 'string',
                    'id' => 'string (uuid)',
                ),
            'clientConditionalOrderId' => 'string (uuid)',
        ), 'POST');
    }

    /**
     * @return bool|string
     */
    public function actionConditionalOrdersClosed()
    {

        return $this->curl('conditional-orders/closed');
    }

    /**
     * @return bool|string
     */
    public function actionConditionalOrdersOpen()
    {

        return $this->curl('conditional-orders/open');
    }

    /**
     * @return bool|string
     */
    public function actionCurrencies()
    {

        return $this->curl('currencies');
    }

    /**
     * @return bool|string
     */
    public function actionCurrenciesSymbol()
    {

        return $this->curl('currencies/btc');
    }

    /**
     * @return bool|string
     */
    public function actionDepositsList()
    {

        return $this->curl('deposits/open');
    }

    /**
     * @return bool|string
     */
    public function actionDepositsClosedList()
    {

        return $this->curl('deposits/closed');
    }

    /**
     * @return bool|string
     */
    public function actionDepositsCurrenciesList()
    {

        /**
         * txId: string
         * in path REQUIRED
         * the transaction id to lookup
         */
        return $this->curl('deposits/btc/txId');
    }

    /**
     * @return bool|string
     */
    public function actionDepositsId()
    {

        /**
         * depositId: string
         * in path REQUIRED
         * (uuid-formatted string) - ID of the deposit to retrieve
         */
        return $this->curl('deposits/depositId');
    }

    /**
     * @return bool|string
     */
    public function actionMarkets()
    {

        return $this->curl('markets');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSummaries()
    {

        return $this->curl('markets/summaries');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsTickers()
    {

        return $this->curl('markets/tickers');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbolTickers()
    {

        return $this->curl('markets/btc/ticker');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbol()
    {

        return $this->curl('markets/btc');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbolSummary()
    {

        return $this->curl('markets/btc/summary');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbolOrderbook()
    {

        return $this->curl('markets/btc/orderbook');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbolTrades()
    {

        return $this->curl('markets/btc/trades');
    }

    /**
     * @return bool|string
     */
    public function actionMarketsSymbolCandles()
    {

        return $this->curl('markets/btc/candles/100/recent');
    }

    /**
     * @return bool|string
     */
    public function actionOrdersClosed()
    {

        return $this->curl('orders/closed');
    }

    /**
     * @return bool|string
     */
    public function actionOrdersOpen()
    {

        return $this->curl('orders/open');
    }

    /**
     * @return bool|string
     */
    public function actionOrdersId()
    {

        return $this->curl('orders/id');
    }

    /**
     * @return bool|string
     */
    public function actionOrdersDeleteId()
    {

        return $this->curl('orders/id', array('orderId' => ''), 'DELETE');
    }

    /**
     * @return bool|string
     */
    public function actionOrdersExecutions()
    {

        return $this->curl('orders/id/executions', array('orderId' => ''));
    }

    /**
     * @return bool|string
     */
    public function actionOrdersPost()
    {

        return $this->curl('orders/', array(
            'marketSymbol' => 'string',
            'direction' => 'string',
            'type' => 'string',
            'quantity' => 'number (double)',
            'ceiling' => 'number (double)',
            'limit' => 'number (double)',
            'timeInForce' => 'string',
            'clientOrderId' => 'string (uuid)',
            'useAwards' => 'boolean',
        ), 'POST');
    }

    /**
     * @return bool|string
     */
    public function actionTransfersSentList()
    {

        return $this->curl('transfers/sent');
    }

    /**
     * @return bool|string
     */
    public function actionTransfersSentReceived()
    {

        return $this->curl('transfers/received');
    }

    /**
     * @return bool|string
     */
    public function actionTransfersSentId()
    {
        /**
         * transferId => (uuid-formatted string) - ID of the transfer to retrieve
         */
        return $this->curl('transfers/transferId');
    }

    /**
     * @return bool|string
     */
    public function actionTransfersNew()
    {
        /**
         * transferId => (uuid-formatted string) - ID of the transfer to retrieve
         */
        return $this->curl('transfers', array(
            'toSubaccountId' => 'string (uuid)',
            'requestId' => 'string (uuid)',
            'currencySymbol' => 'string',
            'amount' => 'number (double)',
            'toMasterAccount' => 'boolean',
        ), 'POST');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsOpen()
    {

        return $this->curl('withdrawals/open');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsClosed()
    {

        return $this->curl('withdrawals/closed');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsByTxId()
    {
        /**
         * txId => the transaction id to lookup
         */
        return $this->curl('withdrawals/ByTxId/txId');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsId()
    {
        /**
         * withdrawalId => (uuid-formatted string) - ID of withdrawal to retrieve
         */
        return $this->curl('withdrawals/withdrawalId');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsIdCancel()
    {
        /**
         * withdrawalId => (uuid-formatted string) - ID of withdrawal to cancel
         */
        return $this->curl('withdrawals/withdrawalId', array(), 'DELETE');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalsIdPost()
    {

        return $this->curl('withdrawals/withdrawalId', array(
            'currencySymbol' => 'string',
            'quantity' => 'number (double)',
            'cryptoAddress' => 'string',
            'cryptoAddressTag' => 'string',
        ), 'POST');
    }

    /**
     * @return bool|string
     */
    public function actionWithdrawalswWitelistAddresses()
    {

        return $this->curl('withdrawals/whitelistAddresses');
    }

}
