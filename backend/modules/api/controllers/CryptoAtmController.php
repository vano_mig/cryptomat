<?php
namespace backend\modules\api\controllers;

use backend\modules\api\models\Bitstamp;
use backend\modules\virtual_terminal\models\Atm;
use Yii;
use yii\filters\Cors;
use yii\rest\Controller;

class CryptoAtmController extends Controller
{
	public $enableCsrfValidation = false;

	public function actionTest()
	{
		$post = Yii::$app->request->get();
		$atm = Atm::findOne(['uid' => $post['uid']]);
//		var_dump($atm);
		$model_path = "backend\modules\api\models\\{$post["name"]}";
		$model = new $model_path();
		$information = $model->getBalance('btc');
		return $this->asJson($information);
	}
}
?>