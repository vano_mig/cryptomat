<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Success</title>
</head>
<body style="width:100%;height:100%;background-color:transparent;">
    <div style='position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);padding:10px 20px;background-color:darkmagenta;text-align:center;border-radius:20px;'>   
        <h1 style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;font-size:50px;color:white;">Success</h1>
    </div>
    <script>
        console.log('kyc_session_completed_successfully');
    </script>
</body>
</html>