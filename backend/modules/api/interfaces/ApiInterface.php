<?php

namespace backend\modules\api\interfaces;

interface ApiInterface
{
	public function getBalance($array):array;

	public function checkDeposit($array):array;

    public function checkWithdrawal($array):array;

    public function createObjectTransaction($array):array;

    public function checkTransaction($array):array;

    public function confirmCryptoTransaction($array):array;
}