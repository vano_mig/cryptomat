<?php

namespace backend\modules\admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadEmail
 * @package frontend\models\form
 */
class UploadEmail extends Model
{
    /**
     * @var UploadedFile
     */
    public $videoFile;

    public function rules()
    {
        return [
            [['videoFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * Save Email
     * @param $path
     * @return bool|string
     */
    public function upload($path)
    {
        if ($this->validate()) {
            $this->videoFile->saveAs($path . $this->videoFile->baseName . '.' . $this->videoFile->extension);
            chmod($path . $this->videoFile->baseName . '.' . $this->videoFile->extension, 0755);
            return $this->videoFile->name;
        } else {
            return false;
        }
    }
}