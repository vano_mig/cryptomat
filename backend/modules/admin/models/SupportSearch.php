<?php

namespace backend\modules\admin\models;

use yii\data\ActiveDataProvider;
use backend\modules\user\models\User;
use Yii;

/**
 * Class SupportSearch
 * @package backend\modules\admin\models
 */
class SupportSearch extends Support
{
    public $email;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            [['ticket_id', 'client_id', 'answerer_id'], 'integer'],
            [['text', 'title'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['category', 'status'], 'string', 'max' => 255],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = Support::find();
        $query->joinWith('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'email',
                'category',
                'title',
                'status',
                'answerer_id',
                'created_at',
                'updated_at',
            ]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (Yii::$app->user->identity->role != User::ROLE_ADMIN) {
            $query->andWhere(['client_id' => Yii::$app->user->id]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'category' => $this->category,
            'answerer_id' => $this->answerer_id,
            'support.created_at' => $this->created_at,
            'support.updated_at' => $this->updated_at,
        ])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'title', $this->title]);


        $query;

        $query->andWhere(['ticket_id' => null]);

        return $dataProvider;
    }
}
