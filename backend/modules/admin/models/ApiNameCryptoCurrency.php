<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "api_name_crypto_currency".
 *
 * @property int $id
 * @property string|null $api_id
 * @property int|null $crypto_currency
 * @property int|null $active
 */
class ApiNameCryptoCurrency extends \yii\db\ActiveRecord
{
	public const STATUS_ACTIVE = 1;
	public const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_name_crypto_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['crypto_currency', 'active'], 'integer'],
            [['api_id'], 'string', 'max' => 255],
			[['api_id', 'crypto_currency', 'active'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'api_id' => Yii::t('api', 'Api Name'),
            'crypto_currency' => Yii::t('api', 'Crypto Currency'),
            'active' => Yii::t('api', 'Active'),
        ];
    }

    public function getApiName()
    {
        return $this->hasOne(ApiName::class, ['id'=>'api_id'])->andWhere(['active'=>ApiName::STATUS_ACTIVE]);
    }

	static public function listStatuses()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('admin', 'active'),
			self::STATUS_INACTIVE => Yii::t('admin', 'inactive'),
		];
	}

	static public function getStatuses($id)
	{
		$array = self::listStatuses();

		return (array_key_exists($id, $array)) ? $array[$id] : $array[self::STATUS_INACTIVE];
	}

	static public function getAll()
	{
		$res = [];

		$model = ApiNameCryptoCurrency::find()->where(['active' => ApiNameCryptoCurrency::STATUS_ACTIVE])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = ['api_id' => $value['api_id'], 'crypto_currency' => $value['crypto_currency']];

		return $res;
	}

	static public function getByApiId($id)
	{
		$res = [];

		$model = ApiNameCryptoCurrency::find()->where(['api_id' => $id])->one();

		if ($model)
			$res = $model;

		return $res;
	}

	static public function getById($id)
	{
		$res = [];

		$model = ApiNameCryptoCurrency::find()->where(['id' => $id])->one();

		if ($model)
			$res = $model;

		return $res;
	}

	public function getInfo()
	{
		return $this->hasOne(ApiName::class, ['id'=>'api_id'])->andWhere(['active'=>ApiName::STATUS_ACTIVE]);
	}

	public function getCryptoCurrency()
    {
        return $this->hasOne(CryptoCurrency::class, ['id'=>'crypto_currency']);
    }

}
