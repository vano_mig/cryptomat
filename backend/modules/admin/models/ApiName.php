<?php

namespace backend\modules\admin\models;

use Twilio\Rest\Api;
use Yii;

/**
 * This is the model class for table "api_name".
 *
 * @property int $id
 * @property string|null $api_name
 * @property string|null $api_url
 * @property int|null $active
 * @property int|null $type
 * @property string|null $form
 * @property int|null $buy
 * @property int|null $sell
 * @property int|null $exchange
 */
class ApiName extends \yii\db\ActiveRecord
{
	public const STATUS_ACTIVE = 0;
	public const STATUS_INACTIVE = 1;

	public const TYPE_ACTIVE = 1;
	public const TYPE_EXCHANGE = 1;
	public const TYPE_3D_PARTY = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active', 'type', 'buy', 'sell', 'exchange'], 'integer'],
            [['api_name', 'api_url', 'form'], 'string', 'max' => 255],
            ['type', 'default', 'value'=>self::TYPE_EXCHANGE],
			[['api_name', 'api_url', 'active'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'api_name' => 'Api Name',
            'api_url' => 'Api Url',
            'active' => 'Active',
            'type' => 'Type',
            'form' => 'Form',
        ];
    }

	static public function getListAll()
	{
		$res = [];

		$model = ApiName::find()->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['api_name'];

		return $res;
	}

	static public function getList()
	{
		$res = [];

		$model = ApiName::find()->where(['active'=>ApiName::STATUS_ACTIVE, 'type'=>ApiName::TYPE_EXCHANGE])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['api_name'];

		return $res;
	}

	static public function getListByType($type)
	{
		$res = [];

		$model = ApiName::find()->where(['active'=>ApiName::STATUS_ACTIVE, 'type'=>ApiName::TYPE_EXCHANGE])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				if ($value[$type])
					$res[$value['id']] = $value['api_name'];

		return $res;
	}

	static public function getListParty()
	{
		$res = [];

		$model = ApiName::find()->where(['active'=>ApiName::STATUS_ACTIVE, 'type'=>ApiName::TYPE_3D_PARTY])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['api_name'];

		return $res;
	}

	static public function listStatuses()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('admin', 'active'),
			self::STATUS_INACTIVE => Yii::t('admin', 'inactive'),
		];
	}


	static public function getStatuses($id = null)
	{
		$array = self::listStatuses();

		return (array_key_exists($id, $array)) ? $array[$id] : $array[self::STATUS_INACTIVE];
	}

	static public function listTypes()
	{
		return [
			self::TYPE_EXCHANGE => Yii::t('admin', 'Exchange'),
			self::TYPE_3D_PARTY => Yii::t('admin', '3RD PARTY'),
		];
	}

	static public function getTypes($type)
	{
		$array = self::listTypes();

		return (array_key_exists($type, $array)) ? $array[$type] : Yii::t('admin', 'not set');
	}

	static public function getByName($name)
	{
		$res = [];

		$model = ApiName::findOne(['name' => $name]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['api_name'];
			$res['buy'] = $model['buy'];
			$res['sell'] = $model['sell'];
			$res['exchange'] = $model['exchange'];
			$res['active'] = $model['active'];
		}

		return $res;
	}

	static public function getById($id)
	{
		$res = [];

		$model = ApiName::findOne(['id' => $id]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['api_name'];
			$res['buy'] = $model['buy'];
			$res['sell'] = $model['sell'];
			$res['exchange'] = $model['exchange'];
			$res['active'] = $model['active'];
		}

		return $res;
	}
}
