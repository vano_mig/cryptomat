<?php

namespace backend\modules\admin\models;

use backend\modules\api\models\Coinmarketcap;
use backend\modules\user\models\CompanyProfit;
use backend\modules\user\models\User;
use backend\modules\virtual_terminal\models\Atm;
use Yii;
use yii\base\Model;

/**
 * Class Dashboard
 * @package backend\modules\admin\models
 */
class Dashboard extends Model
{

    const TEST_TRANSACTIONS = false;

    /**
     * get Сompany terminals and widgets ID
     * @return array
     */
    public function getSourcesTransaction()
    {
        $source['terminal'] = [];
        $source['widget'] = [];

        $queryAtm = Atm::find();
//        $queryWidget = Widget::find();
        $queryAtm->select(['id', 'uid']);
//        $queryWidget->select(['id', 'uid']);

        if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $queryAtm->where(['company_id' => Yii::$app->user->identity->company_id]);
//            $queryWidget->where(['company_id' => Yii::$app->user->identity->company_id]);
        }

        $modelAtm = $queryAtm->all();
//        $modelWidget = $queryWidget->all();

        if ($modelAtm !== null) {
            $source['terminal'] = $modelAtm;

        }
//        if ($modelWidget !== null) {
//            $source['widget'] = $modelWidget;
//        }
        return $source;
    }

    public static function timeStep($i)
    {
        if ($i == 0) {
            $dateBeginWeek = date('Y-m-d 00:00:00', strtotime('this day'));
            $dateEndWeek = date('Y-m-d H:i:s');
            $now = true;
        } else {
            $dateBeginWeek = date('Y-m-d 00:00:00', strtotime("this day -$i day"));
            $p = $i - 1;
            $dateEndWeek = date('Y-m-d 00:00:00', strtotime("this day -$p day"));
            $now = false;
        }

        return ['dateBeginWeek' => $dateBeginWeek, 'dateEndWeek' => $dateEndWeek, 'now' => $now];
    }

    /**
     * Get transactions by Currency
     * @param $array
     * @return array
     */
    public function getTransactionsCurrencyIn($sourcesTransaction, $currency, $currencys): array
    {
        $result['sales'] = [];
        $result['lastSevenWeekDate'] = '';
        unset($currencys[$currency]);
        $terminalId = [];
        foreach ($sourcesTransaction['terminal'] as $itemTerminal) {
            $terminalId[] = $itemTerminal->id;
        }
//        $widgetId = [];
//        foreach ($sourcesTransaction['widget'] as $itemWidget) {
//            $widgetId[] = $itemWidget->id;
//        }
        $rate = (new Coinmarketcap())->getCurrencyRate($currency);
        for ($i = 7; $i >= 0; $i--) {
            $step = self::timeStep($i);
            foreach ($currencys as $item) {
                $countIn1 = Transaction::find();
                $countIn1->where(['in', 'terminal_id', $terminalId])
                    ->andWhere(['in', 'currency_receive', strtoupper($currency)])
                    ->andWhere(['in', 'currency_sent', strtoupper($item)])
                    ->andWhere(['in', 'status', [Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO_SUCCESS, Transaction::STATUS_SEND_CASH_SUCCESS]])
                    ->andWhere(['between', 'created_at', $step['dateBeginWeek'], $step['dateEndWeek']]);
//
//                if (!self::TEST_TRANSACTIONS) {
//                    $countIn1->andWhere(['not', ['transaction_id' => null]])
//                        ->andWhere(['not like', 'transaction_id', 'test']);
//                }

                $countInn = $countIn1->all();
//                if($currency == 'USD' && $item == 'btc') {
//                    echo "<pre>";print_r($countIn1);
//                }
//                echo "<pre>";print_r($countInn);
                $amou = 0;
                if ($countInn) {
                    foreach ($countInn as $itemd) {
                        $amou += ($itemd->amount_receive * $rate);
                    }
                }
                if ($item === 'USDT.ERC20') {
                    $item = 'USDT';
                }
                if ($currency === 'USDT.ERC20') {
                    $currency = 'USDT';
                }
                $result['sales'][$currency . '/' . $item] = array_key_exists($currency . '/' . $item, $result['sales']) ? $result['sales'][$currency . '/' . $item] . $amou . ', ' : $amou . ', ';
                if ($currency === 'USDT') {
                    $currency = 'USDT.ERC20';
                }
            }

            if ($step['now']) {
                $date = Yii::t('dashboard', 'Now');
            } else {
                $date = date('Y.m.d', strtotime($step['dateEndWeek']));
            }
            $result['lastSevenWeekDate'] .= '\'' . $date . '\', ';
        }
        return $this->getChartColor($result);
    }

    /**
     * Get transactions by Currency
     * @param $array
     * @return array
     */
    public function getTransactionsCurrencyOut($sourcesTransaction, $currency, $currencys): array
    {
        $result['sales'] = [];
        $result['lastSevenWeekDate'] = '';
        unset($currencys[$currency]);
        $terminalId = [];
        foreach ($sourcesTransaction['terminal'] as $itemTerminal) {
            $terminalId[] = $itemTerminal->id;
        }
        $widgetId = [];
        foreach ($sourcesTransaction['widget'] as $itemWidget) {
            $widgetId[] = $itemWidget->id;
        }
        $rate = (new Coinmarketcap())->getCurrencyRate($currency);
        for ($i = 7; $i >= 0; $i--) {
            $step = self::timeStep($i);
            foreach ($currencys as $item) {

                $countIn1 = Transaction::find();
                $countIn1->where(['in', 'terminal_id', $terminalId]);
                $countIn1->andWhere(['in', 'currency_sent', strtoupper($currency)])
                    ->andWhere(['in', 'currency_receive', strtoupper($item)])
                    ->andWhere(['in', 'status', [Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO_SUCCESS, Transaction::STATUS_SEND_CASH_SUCCESS]])
                    ->andWhere(['between', 'created_at', $step['dateBeginWeek'], $step['dateEndWeek']]);
                if (!self::TEST_TRANSACTIONS) {
                    $countIn1->andWhere(['not', ['transaction_id' => null]])
                        ->andWhere(['not like', 'transaction_id', 'test']);
                }
                $countInn = $countIn1->all();
                $amou = 0;
                if ($countInn) {
                    foreach ($countInn as $itemd) {
                        $amou += ($itemd->amount_sent * $rate);
                    }
                }

                $countIn2 = Transaction::find();
                $countIn2->where(['in', 'terminal_id', $terminalId])
                    ->andWhere(['in', 'currency_sent', $currency])
                    ->andWhere(['in', 'status', [Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO_SUCCESS, Transaction::STATUS_SEND_CASH_SUCCESS]])
                    ->andWhere(['between', 'created_at', $step['dateBeginWeek'], $step['dateEndWeek']]);

                if (!self::TEST_TRANSACTIONS) {
                    $countIn2->andWhere(['not', ['transaction_id' => null]])
                        ->andWhere(['not like', 'transaction_id', 'test']);
                }

                $countInn2 = $countIn2->all();

                if ($countInn2) {
                    foreach ($countInn2 as $itemd) {
                        $tr = Transaction::findOne($itemd->transaction_begin);
                        if ($tr) {
                            $amou += ($itemd->amount_sent * $rate);
                        }
                    }
                }

                if ($item === 'USDT.ERC20') {
                    $item = 'USDT';
                }
                if ($currency === 'USDT.ERC20') {
                    $currency = 'USDT';
                }
                $result['sales'][$currency . '/' . $item] = array_key_exists($currency . '/' . $item, $result['sales']) ? $result['sales'][$currency . '/' . $item] . $amou . ', ' : $amou . ', ';
                if ($currency === 'USDT') {
                    $currency = 'USDT.ERC20';
                }
            }
            if ($step['now']) {
                $date = Yii::t('dashboard', 'Now');
            } else {
                $date = date('Y.m.d', strtotime($step['dateEndWeek']));
            }
            $result['lastSevenWeekDate'] .= '\'' . $date . '\', ';
        }
        return $this->getChartColor($result);
    }

    /**
     * Get color to chart in dashboard
     * @param $array
     * @return array
     * @throws \Exception
     */
    public function getChartColor($array): array
    {
        $result['lastSevenWeekDate'] = $array['lastSevenWeekDate'];
        $result['chartData'] = '';
        $colors = [
            "56, 103, 214",
            "136, 84, 208",
            "250, 130, 49",
            "247, 183, 49",
            "32, 191, 107",
            "75, 101, 132",
            "15, 185, 177"
        ];

        if (!empty($array['sales'])) {
            $j = 0;
            foreach ($array['sales'] as $key => $value) {
                if ($j <= 6) {
                    $color = $colors[$j];
                } else {
                    $color = random_int(0, 255) . ', ' . random_int(0, 255) . ', ' . random_int(0, 255);
                }
                $result['chartData'] .= "{
                    label: '$key',
                    backgroundColor: 'rgba($color, 0.7)',
                    borderColor: 'rgb($color)',
                    data: [$value],
                },";
                $j++;
            }
        }
        return $result;
    }

    public function getDashboard()
    {
        $list = (new CryptoCurrency())->getListCodeFull();
        $result = [];
        if (!empty($list)) {
            $model = new CompanyProfit();
            foreach ($list as $data) {
                $result[$data] = ['currency' => $data,
                    'data' => $model->getProfitCrypto($data)];
            }
        }
        return $this->getProfitColor($result);
    }


    public function getReceiveCurrency($sourcesTransaction, $currency): array
    {
        $result[$currency] = [];
        $result[$currency] = [
            'currency'=>$currency,
            'data' => 0];
        $terminalId = [];
        foreach ($sourcesTransaction['terminal'] as $itemTerminal) {
            $terminalId[] = $itemTerminal->id;
        }

        $rate = (new Coinmarketcap())->getCurrencyRate($currency);
        $dataEnd = date('Y-m-d H:i:s');
        $dateBegin = date('Y-m-d H:i:s', strtotime('-7 day'));
        $countIn1 = Transaction::find();
        $countIn1->where(['in', 'terminal_id', $terminalId])
            ->andWhere(['in', 'currency_receive', strtoupper($currency)])
            ->andWhere(['in', 'status', [Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO_SUCCESS, Transaction::STATUS_SEND_CASH_SUCCESS]])
            ->andWhere(['between', 'created_at', $dateBegin, $dataEnd]);

        if (!self::TEST_TRANSACTIONS) {
            $countIn1->andWhere(['not', ['transaction_id' => null]])
                ->andWhere(['not like', 'transaction_id', 'test']);
        }
//echo "<pre>";print_r($countIn1);die;
        $countInn = $countIn1->all();
        if ($countInn) {
            foreach ($countInn as $itemd) {

                $result[$currency]['data'] += ($itemd->amount_receive * $rate);
            }
        }
        return $result;
    }

    public function getSentCurrency($sourcesTransaction, $currency): array
    {
        $result[$currency] = [];
        $result[$currency] = [
            'currency'=>$currency,
            'data' => 0];

        $terminalId = [];
        foreach ($sourcesTransaction['terminal'] as $itemTerminal) {
            $terminalId[] = $itemTerminal->id;
        }

        $rate = (new Coinmarketcap())->getCurrencyRate($currency);
        $dataEnd = date('Y-m-d H:i:s');
        $dateBegin = date('Y-m-d H:i:s', strtotime('-7 day'));
        $countIn1 = Transaction::find();
        $countIn1->where(['in', 'terminal_id', $terminalId])
            ->andWhere(['in', 'currency_sent', strtoupper($currency)])
            ->andWhere(['in', 'status', [Transaction::STATUS_SUCCESS, Transaction::STATUS_SEND_CRYPTO_SUCCESS, Transaction::STATUS_SEND_CASH_SUCCESS]])
            ->andWhere(['between', 'created_at', $dateBegin, $dataEnd]);

        if (!self::TEST_TRANSACTIONS) {
            $countIn1->andWhere(['not', ['transaction_id' => null]])
                ->andWhere(['not like', 'transaction_id', 'test']);
        }

        $countInn = $countIn1->all();
        if ($countInn) {
            foreach ($countInn as $itemd) {
                $result[$currency]['data'] += ($itemd->amount_receive * $rate);
            }
        }
        return $result;
    }

    public function getProfitColor($array)
    {
        $result['label'] = [];
        $result['data'] = [];
        $result['backgroundColor'] = [];
        $colors = [
            "56, 103, 214",
            "136, 84, 208",
            "250, 130, 49",
            "247, 183, 49",
            "32, 191, 107",
            "75, 101, 132",
            "15, 185, 177"
        ];

        if (!empty($array)) {
            $j = 0;
            foreach ($array as $value) {
                if ($j <= 6) {
                    $color = $colors[$j];
                } else {
                    $color = random_int(0, 255) . ', ' . random_int(0, 255) . ', ' . random_int(0, 255);
                }
                $result['label'][] = $value['currency'];
                $result['data'][] = $value['data'];
                $result['backgroundColor'][] = "rgba($color, 0.7)";
                $j++;
            }
        }
        return $result;
    }
}
