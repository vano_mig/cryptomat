<?php

namespace backend\modules\admin\models;

use backend\modules\user\models\User;
use backend\modules\virtual_terminal\models\Atm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * Class Transaction
 * @package frontend\models
 */
class Transaction extends \common\models\Transaction
{
    public $dateFrom = '';
    public $dateTo = '';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->dateTo = date('Y-m-d');
        $this->dateFrom = date('Y-m-d', strtotime('- 7 days'));
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['terminal_id', 'type', 'status', 'confirm_needed', 'coinmarketcap_id', 'coinmarketcap_id_2', 'widget_id', 'transaction_begin', 'api_id','currency_receive', 'currency_sent'], 'integer'],
            [['endtime'], 'safe'],
            [['amount_receive', 'amount_sent'], 'number'],
            [['address', 'address_coin', 'phone_number'], 'string'],
            ['return_payment', 'default', 'value'=>self::RETURN_NO],
            ['return_payment', 'in', 'range'=>[self::RETURN_NO, self::RETURN_YES, self::RETURN_SUCCESS]],
            [['created_at', 'updated_at'], 'safe'],
            [['transaction_id'], 'string', 'max' => 255],
        ];
    }


    public function search($params = [])
    {
        $query = Transaction::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $ids = [];
            $ids[] = Yii::$app->user->identity->company_id;

            $atm = [];
            $list = Atm::find()->where(['in', 'company_id', $ids])->all();
            if (!empty($list)) {
                foreach ($list as $item) {
                    $atm[] = $item->id;
                }
            }

//            $widget = [];
//            $list = Widget::find()->where(['in', 'company_id', $ids])->all();
//            if (!empty($list)) {
//                foreach ($list as $item) {
//                    $widget[] = $item->id;
//                }
//            }
            $list = Atm::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
            if (!empty($list)) {
                foreach ($list as $item) {
                    $atm[] = $item->id;
                }
            }
            $query->andWhere(['terminal_id' => $atm]);
//            $query->orWhere(['widget_id' => $widget]);
        }

//        if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
//            $widget = [];
//            $list = Widget::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
//            if (!empty($list)) {
//                foreach ($list as $item) {
//                    $widget[] = $item->id;
//                }
//            }
//            $query->orWhere(['widget_id' => $widget]);
//        }
//        $query->andWhere(['not', ['transaction_id' => null]]);
//        $query->andWhere(['not like', 'transaction_id', 'test']);
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'terminal_id' => $this->terminal_id,
            'widget_id' => $this->widget_id,
            'type' => $this->type,
            'amount_receive' => $this->amount_receive,
            'amount_sent' => $this->amount_sent,
            'currency_receive' => $this->currency_receive,
            'currency_sent' => $this->currency_sent,
            'phone_number' => $this->phone_number,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'transaction_id', $this->transaction_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function dashboardSearch($params)
    {
        $query = Transaction::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $ids = [];
            $ids[] = Yii::$app->user->identity->company_id;


            $atm = [];
            $list = Atm::find()->where(['in', 'company_id', $ids])->all();
            if (!empty($list)) {
                foreach ($list as $item) {
                    $atm[] = $item->id;
                }
            }


            $list = Atm::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
            if (!empty($list)) {
                foreach ($list as $item) {
                    $atm[] = $item->id;
                }
            }
            $query->andWhere(['terminal_id' => $atm]);
        }
        $query->andWhere(['not', ['transaction_id' => null]]);
        $query->andWhere(['not like', 'transaction_id', 'test']);
        $query->andWhere('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)');


        $query->andFilterWhere(['like', 'transaction_id', $this->transaction_id]);

        return $dataProvider;
    }
}