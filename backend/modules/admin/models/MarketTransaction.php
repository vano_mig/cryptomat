<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "market_transaction".
 *
 * @property int $id
 * @property float|null $price
 * @property float|null $amount
 * @property int|null $transaction_id
 * @property int|null $type
 * @property string|null $market_id
 * @property string|null $datetime
 */
class MarketTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'market_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'amount'], 'number'],
            [['type'], 'integer'],
            [['datetime'], 'safe'],
            [['transaction_id'], 'string'],
            [['market_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'price' => Yii::t('admin', 'Price'),
            'amount' => Yii::t('admin', 'Amount'),
            'transaction_id' => Yii::t('admin', 'Transaction ID'),
            'type' => Yii::t('admin', 'Type'),
            'market_id' => Yii::t('admin', 'Market ID'),
            'datetime' => Yii::t('admin', 'Datetime'),
        ];
    }
}
