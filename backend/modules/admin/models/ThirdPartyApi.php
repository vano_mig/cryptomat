<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "3rd_party_api".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $user_id
 * @property int|null $provider_id
 * @property string|null $account_sid
 * @property string|null $auth_token
 * @property string|null $messaging_service_sid
 * @property string|null $from_number
 * @property float|null $cost
 * @property int|null $currency_id
 */
class ThirdPartyApi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '3rd_party_api';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'provider_id', 'name'], 'required'],
            [['company_id', 'provider_id', 'currency_id'], 'integer'],
            [['cost'], 'number'],
            [['name', 'account_sid', 'auth_token', 'messaging_service_sid', 'from_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('admin', 'Name'),
            'company_id' => Yii::t('admin', 'Company'),
            'provider_id' => Yii::t('admin', 'Provider ID'),
            'account_sid' => Yii::t('admin', 'Account Sid'),
            'auth_token' => Yii::t('admin', 'Auth Token'),
            'messaging_service_sid' => Yii::t('admin', 'Messaging Service Sid'),
            'from_number' => Yii::t('admin', 'From Number'),
            'cost' => Yii::t('admin', 'Cost'),
            'currency_id' => Yii::t('admin', 'Currency ID'),
        ];
    }

    public function getForm()
    {
        return $this->hasOne(ApiName::class, ['id'=>'provider_id']);
    }
}
