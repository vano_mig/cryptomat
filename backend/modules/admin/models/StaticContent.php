<?php

namespace backend\modules\admin\models;

/**
 * Class StaticContent
 * @package backend\modules\admin\models
 */
class StaticContent extends \common\models\StaticContent
{
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 5],
            ['name', 'string', 'max' => 255],
        ];
    }
}
