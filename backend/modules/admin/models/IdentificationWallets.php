<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "identification_wallets".
 *
 * @property int $id
 * @property int|null $profile_id
 * @property int|null $replenish_id
 * @property int|null $exchange_id
 * @property int|null $type
 * @property int|null $currency_id
 */
class IdentificationWallets extends \yii\db\ActiveRecord
{
    const TYPE_BUY = 1;
    const TYPE_SELL = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'identification_wallets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'replenish_id', 'exchange_id', 'type', 'currency_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'profile_id' => Yii::t('admin', 'Profile ID'),
            'replenish_id' => Yii::t('admin', 'Replenish ID'),
            'exchange_id' => Yii::t('admin', 'Exchange ID'),
            'type' => Yii::t('admin', 'Type'),
            'currency_id' => Yii::t('admin', 'Currency ID'),
        ];
    }

    public function getCryptoCurrency()
    {
        return $this->hasOne(CryptoCurrency::class, ['id'=>'currency_id'])->andWhere(['active'=>CryptoCurrency::STATUS_ACTIVE]);
    }

}
