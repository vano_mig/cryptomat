<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "api_name_currency".
 *
 * @property int $id
 * @property int|null $currency
 * @property int|null $api_id
 * @property int|null $active
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ApiNameCurrency extends \yii\db\ActiveRecord
{
	public const STATUS_ACTIVE = 1;
	public const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_name_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['api_id', 'active'], 'integer'],
            [['currency', 'active', 'api_id'], 'required'],
            [['api_id', 'currency'], 'unique', 'targetAttribute' => ['api_id', 'currency']],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'currency' => Yii::t('admin', 'Currency'),
            'api_id' => Yii::t('admin', 'Api ID'),
            'active' => Yii::t('admin', 'Active'),
            'created_at' => Yii::t('admin', 'Created At'),
            'updated_at' => Yii::t('admin', 'Updated At'),
        ];
    }

	static public function listStatuses()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('admin', 'active'),
			self::STATUS_INACTIVE => Yii::t('admin', 'inactive'),
		];
	}

	static public function getStatuses($id)
	{
		$array = self::listStatuses();

		return (array_key_exists($id, $array)) ? $array[$id] : $array[self::STATUS_INACTIVE];
	}

	static public function getAll()
	{
		$res = [];

		$model = ApiNameCurrency::find()->where(['active' => ApiNameCurrency::STATUS_ACTIVE])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = ['api_id' => $value['api_id'], 'currency' => $value['currency']];;

		return $res;
	}


	static public function getById($id)
	{
		$res = [];

		$model = ApiNameCurrency::find()->where(['id' => $id])->one();

		if ($model)
			$res = $model;

		return $res;
	}

	public function getCurrencyName()
    {
        return $this->hasOne(Currency::class, ['id'=>'currency']);
    }

    public function getApiName()
    {
        return $this->hasOne(ApiName::class, ['id'=>'api_id'])->andWhere(['active'=>ApiName::STATUS_ACTIVE]);
    }

}
