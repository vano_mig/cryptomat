<?php

namespace backend\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SourceMessageSearch represents the model behind the search form of `backend\modules\admin\models\SourceMessage`.
 */
class SourceMessageSearch extends SourceMessage
{
    public $islanguage;
    public $nolanguage;
    public $translation;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'id', 'translation','islanguage','nolanguage','category'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = SourceMessageSearch::find()->joinWith(['messages'])->distinct();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort' => [
                'defaultOrder' => [
                    'source_message.id' => SORT_DESC,
                ],
                'attributes' => [
                    'source_message.id',
                    'category',
                    'message' => [
                        'asc' => ['source_message.message' => SORT_ASC],
                        'desc' => ['source_message.message' => SORT_DESC],
                    ],
                    'translation' => [
                        'asc' => ['message.translation' => SORT_ASC],
                        'desc' => ['message.translation' => SORT_DESC],
                    ],
                ],
            ],

            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if ($this->islanguage) {
            if ($this->islanguage != 'message') {
                if ($this->nolanguage == '1') {
                    $query->where(['message.language' => $this->islanguage]);
                    $query->andWhere(['not in', 'message.translation', [null, '']]);
                    $query->andWhere(['REGEXP', 'message.translation', '[^\s*$]']);
                } else {
                    $query->where(['message.language' => $this->islanguage]);
                    $query->andWhere(['in', 'message.translation', [null, '']]);
                    $query->orWhere(['REGEXP', 'message.translation', '/^\s*$/']);
                }
            }
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'source_message.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);
        $query->andFilterWhere(['like', 'message.translation', $this->translation]);
//        $query->andFilterWhere(['like', 'source_message.message', $this->message]);


        return $dataProvider;
    }

}
