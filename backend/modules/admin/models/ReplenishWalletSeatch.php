<?php

namespace backend\modules\admin\models;

use backend\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\admin\models\ReplenishWallet;

/**
 * ReplenishWalletSeatch represents the model behind the search form of `backend\modules\admin\models\ReplenishWallet`.
 */
class ReplenishWalletSeatch extends ReplenishWallet
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'block', 'company_id'], 'integer'],
            [['provider_name', 'api_key', 'api_secret'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReplenishWallet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'block' => $this->block,
            'company_id' => $this->company_id,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }

        $query->andFilterWhere(['like', 'provider_name', $this->provider_name])
            ->andFilterWhere(['like', 'api_key', $this->api_key])
            ->andFilterWhere(['like', 'api_secret', $this->api_secret]);

        return $dataProvider;
    }
}
