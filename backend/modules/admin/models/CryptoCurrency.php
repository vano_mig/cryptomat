<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * This is the model class for table "crypto_currency".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $code
 * @property integer|null $precision
 * @property string|null $icon
 * @property string|null $address_regex
 * @property integer|null $min_price
 * @property string|null $full_name
 */
class CryptoCurrency extends \yii\db\ActiveRecord
{
	public const STATUS_ACTIVE = 1;
	public const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crypto_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'active', 'icon', 'precision', 'address_regex', 'full_name'], 'required'],
            [['name', 'code', 'active', 'icon', 'address_regex', 'full_name'], 'string', 'max' => 255],
            [['precision', 'min_price'], 'integer'],
        ];
    }

	public function getListCodeFull()
    {
        $result = [];
        $model = self::find()->where(['active'=>CryptoCurrency::STATUS_ACTIVE])->asArray()->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['code']] = $lang['code'];
            }
        }
        return $result;
    }

	/**
	 * @return array
	 */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('admin', 'Name'),
            'code' => Yii::t('admin', 'Code'),
            'active' => Yii::t('admin', 'Active'),
            'precision' => Yii::t('admin', 'Precision'),
            'icon' => Yii::t('admin', 'Icon'),
            'address_regex' => Yii::t('admin', 'Address Regex'),
            'min_price' => Yii::t('admin', 'Min Price'),
            'full_name' => Yii::t('admin', 'Full name'),
        ];
    }

	static public function getCurrencyList()
	{
		$result = [];
		$model = self::find()->asArray()->all();

		if ($model)
			foreach ($model as $lang)
				$result[$lang['id']] = $lang['name'];

		return $result;
	}

	static public function listStatuses()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('admin', 'active'),
			self::STATUS_INACTIVE => Yii::t('admin', 'inactive'),
		];
	}

	static public function getStatuses($id = null)
	{
		$array =  self::listStatuses();

		return (array_key_exists($id, $array)) ? $array[$id] : $array[self::STATUS_INACTIVE];
	}

	static public function getAllName()
	{
		$res = [];

		$model = CryptoCurrency::find()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['name'];

		return $res;
	}

	static public function getAllCode()
	{
		$res = [];

		$model = CryptoCurrency::find()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['code'];

		return $res;
	}

	static public function getByName($name)
	{
		$res = [];

		$model = CryptoCurrency::findOne(['name' => $name]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['name'];
			$res['code'] = $model['code'];
			$res['active'] = $model['active'];
		}

		return $res;
	}

	static public function getById($id)
	{
		$res = [];

		$model = CryptoCurrency::findOne(['id' => $id]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['name'];
			$res['code'] = $model['code'];
			$res['active'] = $model['active'];
		}

		return $res;
	}
}
