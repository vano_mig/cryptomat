<?php

namespace backend\modules\admin\models;

class Image extends \yii\imagine\Image
{
    public static $thumbnailBackgroundAlpha = 0;
}