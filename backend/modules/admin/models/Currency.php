<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * Class Currency
 * @package backend\modules\admin\models
 */
class Currency extends \common\models\Currency
{
    public $rate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'system_currency', 'status'], 'string', 'max' => 255],
            ['rate', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('currency', 'Name'),
            'code' => Yii::t('currency', 'Code'),
            'system_currency' => Yii::t('currency', 'System Currency'),
            'status' => Yii::t('currency', 'Status'),
            'rate' => Yii::t('currency', 'Rate'),
        ];
    }

    /**
     * Get syatem currency
     * @return array|null
     */
    public function getSystemCurrency()
    {
        $system_currency = self::find()->where([
            'status' => self::STATUS_ACTIVE,
            'system_currency' => self::STATUS_ACTIVE
        ])->asArray()->one();

        return $system_currency;
    }

	public function getCurrencyList()
	{
		$result = [];
		$model = self::find()->where(['status' => self::STATUS_ACTIVE])->asArray()->all();

		if ($model)
			foreach ($model as $lang)
				$result[$lang['id']] = $lang['code'];

		return $result;
	}

	static public function listStatuses()
	{
		return [
			self::STATUS_ACTIVE => Yii::t('currency', 'active'),
			self::STATUS_INACTIVE => Yii::t('currency', 'inactive'),
		];
	}

	static public function getStatuses($id = null)
	{
		$array = self::listStatuses();

		return (array_key_exists($id, $array)) ? $array[$id] : $array[self::STATUS_INACTIVE];
	}

	static public function getAllName()
	{
		$res = [];

		$model = Currency::find()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['name'];

		return $res;
	}

	static public function getByName($name)
	{
		$res = [];

		$model = Currency::findOne(['name' => $name]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['name'];
			$res['code'] = $model['code'];
			$res['status'] = $model['status'];
		}

		return $res;
	}

	static public function getById($id)
	{
		$res = [];

		$model = Currency::findOne(['id' => $id]);
		if ($model) {
			$res['id'] = $model['id'];
			$res['name'] = $model['name'];
			$res['code'] = $model['code'];
			$res['status'] = $model['status'];
		}

		return $res;
	}

    public function getListCode()
    {
        $result = [];
        $model = self::find()->where(['status' => self::STATUS_ACTIVE])->asArray()->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['id']] = $lang['code'];
            }
        }
        return $result;
    }

    public function getCodeName()
    {
        $result = [];
        $model = self::find()->where(['status' => self::STATUS_ACTIVE])->asArray()->all();
        if ($model) {
            foreach ($model as $lang) {
                $result[$lang['code']] = $lang['code'];
            }
        }
        return $result;
    }
}