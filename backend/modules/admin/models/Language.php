<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * Class Language
 * @package backend\modules\admin\models
 */
class Language extends \common\models\Language
{

    /**
     * Get list or language statuses
     * @return array
     */
    public function getStatuses($id = null)
    {
        $array = [
            self::ACTIVE => Yii::t('language', 'active'),
            self::INACTIVE => Yii::t('language', 'inactive')
        ];
        if ($id) {
            return $array[$id];
        }
        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [
                'name',
                'required',
                'message' => Yii::t('language',
                    'You should choose a language.')
            ],

            ['main', 'default', 'value' => self::INACTIVE],
            ['main', 'in', 'range' => [self::ACTIVE, self::INACTIVE]],

            ['status', 'default', 'value' => self::ACTIVE],
            ['status', 'in', 'range' => [self::ACTIVE, self::INACTIVE]]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('language', 'Name'),
            'iso_code' => Yii::t('language', 'Iso Code'),
            'main' => Yii::t('language', 'Main'),
            'status' => Yii::t('language', 'Status'),
        ];
    }

    /**
     * check language activeness - one language should always be active
     * return void
     */
    public function checkMainLang($status, $id): void
    {

        if ($status == Language::ACTIVE) {
            Language::updateAll(['main' => Language::INACTIVE]);
        } else {
            $active = Language::find()->where(['main' => Language::ACTIVE])->one();
            if (!$active) {
                $default = Language::find()->where(['iso_code' => Yii::$app->language])->one();
                if (!$default) {
                    $default = Language::find()->orderBy(['id' => SORT_DESC])->one();
                }
                $default->main = Language::ACTIVE;
                $default->status = Language::ACTIVE;
                $default->save();
            } elseif ($id == $active->id) {
                $default = Language::find()->where(['!=', 'id', $id])->orderBy(['id' => SORT_DESC])->one();
                $default->main = Language::ACTIVE;
                $default->status = Language::ACTIVE;
                $default->save();
            }
        }
    }

    /**
     * Get list of all active Languages
     * @param array $langs array
     * @return array
     */
    public function getList($langs = [])
    {
        $list = Language::find()->where(['status' => self::ACTIVE])->all();
        $array = [];
        if ($list) {
            foreach ($list as $item) {
                if (!empty($langs)) {
                    if (!in_array($item->iso_code, $langs)) {
                        $array[$item->iso_code] = $item->name;
                    }
                } else {
                    $array[$item->iso_code] = $item->name;
                }
            }
        }
        return $array;
    }

    /**
     * Get name of language with its iso code
     * @param string $iso_code
     * @return string
     */
    public function getLanguageName(string $iso_code): string
    {
        $language_name = Language::find()->where(['iso_code' => $iso_code])->one();
        return $language_name->name;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if ($insert) {
            $model = Message::find()->where(['language' => $this->iso_code])->one();
            if (!$model) {
                $list = SourceMessage::find()->select('id')->asArray()->all();
                if (!empty($list)) {
                    foreach ($list as $item) {
                        $message = new Message();
                        $message->id = $item['id'];
                        $message->language = $this->iso_code;
                        $message->save();
                    }
                }
            }
        }
    }
}
