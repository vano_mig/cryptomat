<?php

namespace backend\modules\admin\models;

use backend\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\admin\models\ThirdPartyApi;

/**
 * ThirdPartyApiSearch represents the model behind the search form of `backend\modules\admin\models\ThirdPartyApi`.
 */
class ThirdPartyApiSearch extends ThirdPartyApi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'provider_id', 'currency_id'], 'integer'],
            [['name', 'account_sid', 'auth_token', 'messaging_service_sid', 'from_number'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ThirdPartyApi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'provider_id' => $this->provider_id,
            'cost' => $this->cost,
            'currency_id' => $this->currency_id,
        ]);

        if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
            $query->andWhere(['company_id'=>Yii::$app->user->identity->company_id]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'account_sid', $this->account_sid])
            ->andFilterWhere(['like', 'auth_token', $this->auth_token])
            ->andFilterWhere(['like', 'messaging_service_sid', $this->messaging_service_sid])
            ->andFilterWhere(['like', 'from_number', $this->from_number]);

        return $dataProvider;
    }
}
