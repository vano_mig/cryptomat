<?php

namespace backend\modules\admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package frontend\models\form
 * @property mixed|null $imageFile
 */
class UploadForm extends \backend\models\UploadForm
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, giff'],
        ];
    }

    public function upload($path)
    {
        if ($this->validate()) {
            $this->imageFile->saveAs($path . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            chmod($path . $this->imageFile->baseName . '.' . $this->imageFile->extension, 0755);
            return $this->imageFile->name;
        } else {
            return false;
        }
    }
}