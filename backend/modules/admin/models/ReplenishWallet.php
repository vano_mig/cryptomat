<?php

namespace backend\modules\admin\models;

//use backend\modules\admin\formatter\ApiCryptoCurrencyFormatter;
use backend\modules\admin\formatter\WalletFormatter;
use common\models\Log;
use Yii;

/**
 * This is the model class for table "replenish_wallet".
 *
 * @property int $id
 * @property string|null $provider_name
 * @property int|null $provider_id
 * @property string|null $api_key
 * @property string|null $api_secret
 * @property int|null $block
 * @property int|null $company_id
 * @property string|null $client_id
 * @property string|null $exchange_wallet
 */
class ReplenishWallet extends \yii\db\ActiveRecord
{
	public const STATUS_ACTIVE = 1;
	public const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'replenish_wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provider_id', 'block', 'company_id', 'exchange_wallet'], 'integer'],
            [['provider_name', 'api_key', 'api_secret', 'client_id'], 'string', 'max' => 255],
            [['provider_id', 'provider_name', 'api_key', 'api_secret', 'block', 'company_id'], 'required'],
        ];
    }
	/**
	 * @param null $id
	 * @return array|mixed
	 */
	static public function listStatuses($id = null)
	{
		$array = [
			self::STATUS_INACTIVE => Yii::t('admin', 'inactive'),
			self::STATUS_ACTIVE => Yii::t('admin', 'active'),
		];
		if (array_key_exists($id, $array)) {
			return $array[$id];
		}
		return $array;
	}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'provider_name' => Yii::t('admin', 'Provider Name'),
            'provider_id' => Yii::t('admin', 'Provider ID'),
            'api_key' => Yii::t('admin', 'Api Key'),
            'api_secret' => Yii::t('admin', 'Api Secret'),
            'block' => Yii::t('admin', 'Status'),
            'company_id' => Yii::t('admin', 'Company ID'),
            'client_id' => Yii::t('admin', 'Client ID'),
            'exchange_wallet' => Yii::t('admin', 'Exchange Wallet'),
        ];
    }

    public function getProvider()
	{
		return $this->hasOne(ApiNameCryptoCurrency::class, ['id'=>'provider_id'])->andWhere(['active'=>ApiNameCryptoCurrency::STATUS_ACTIVE]);
	}

    public function getExchange()
    {
        return $this->hasOne(ExchangeWallet::class, ['id'=>'exchange_wallet']);
    }

	static public function getAll()
	{
		$res = [];

		$model = ReplenishWallet::find()->where(['block' => ReplenishWallet::STATUS_ACTIVE])->asArray()->all();
		if ($model)
			foreach ($model as $value)
				$res[$value['id']] = $value['provider_id'];

		return $res;
	}
}
