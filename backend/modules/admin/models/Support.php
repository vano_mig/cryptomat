<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * Class Support
 * @package backend\modules\admin\models
 */
class Support extends \common\models\Support
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text', 'category'], 'required'],
            [[ 'text', 'title'], 'trim'],
            [['title'], 'string', 'length' => [1, 255]],
            ['status', 'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_SOLVED, self::STATUS_STOPPED, self::STATUS_ANSWERED]],
            [['client_id', 'answerer_id'], 'integer'],
            [['text'], 'string', 'min'=>1, 'on'=>'answer'],
            [['text'], 'string', 'min'=>1, 'except'=>'answer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }


    /**
     * Get list of issues
     * @return array
     */
    public function getList():array
    {
        $result = [];
        $model = self::find()->all();
        if ($model) {
            foreach ($model as $issue) {
                $result[$issue['name']] = $issue['name'];
            }
        }
        return $result;
    }

    /**
     * Get list of states
     * @return mixed
     */
    public function listStatuses($id = null)
    {
        $array = [
            self::STATUS_ACTIVE => Yii::t('support', 'active'),
            self::STATUS_SOLVED => Yii::t('support', 'solved'),
            self::STATUS_STOPPED => Yii::t('support', 'stopped'),
            self::STATUS_ANSWERED => Yii::t('support', 'answered'),
        ];
        if (array_key_exists($id, $array)) {
            return $array[$id];
        }
        return $array;
    }

    /**
     * Get list of categories
     * @return mixed
     */
    public function listCategories($id = null)
    {
        $array = [
            self::CATEGORY_ORGANIZATION => Yii::t('support', 'organization'),
            self::CATEGORY_TECHNICAL => Yii::t('support', 'technical'),
        ];
        if (array_key_exists($id, $array)) {
            return $array[$id];
        }
        return $array;
    }
}
