<?php

namespace backend\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\admin\models\MarketTransaction;

/**
 * MarketTransactionSearch represents the model behind the search form of `app\modules\admin\models\MarketTransaction`.
 */
class MarketTransactionSearch extends MarketTransaction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'type'], 'integer'],
            [['price', 'amount'], 'number'],
            [['market_id', 'datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MarketTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'amount' => $this->amount,
            'transaction_id' => $this->transaction_id,
            'type' => $this->type,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'market_id', $this->market_id]);

        return $dataProvider;
    }
}
