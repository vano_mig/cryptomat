<?php

namespace backend\modules\admin\models;

use Yii;

/**
 * Class Country
 * @package backend\modules\admin\models
 */
class Country extends \common\models\Country
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'iso_code', 'phone_code', 'code_3'], 'required'],
            [['name', 'iso_code', 'phone_code'], 'trim'],
            [['name'], 'string', 'length' => [4, 100]],
            [['iso_code', 'code_3'], 'string', 'length' => [2, 3]],
            ['phone_code', 'match', 'pattern'=>'/^[0-9()+-]+$/',
            'message' => Yii::t('country','It only can contain numeric characters and ()+- symbols')],
            ['phone_code', 'string', 'max' => 24],
            ['phone_code', 'number']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('country', 'Name'),
            'iso_code' => Yii::t('country', 'Iso Code'),
            'code_3' => Yii::t('country', 'Iso Code 3'),
            'phone_code' => Yii::t('country', 'Phone Code'),
        ];
    }

    /**
     * Get list countries
     * @return array
     */
    public function getList()
    {
        $result = [];
        $model = self::find()->asArray()->all();
        if($model)
        {
            foreach($model as $lang)
            {
                $result[$lang['id']] = $lang['name'];
            }
        }
        return $result;
    }

    /**
     * Get list countries
     * @param $id
     * @return array
     */
    public function getCountryById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }


    /**
     * Get list countries with id
     * @return array
     */
    public function getListCountry()
    {
        $result = [];
        $model = self::find()->all();
        if($model)
        {
            foreach($model as $lang)
            {
                $result[$lang['id']] = $lang['name'];
            }
        }
        return $result;
    }
}
