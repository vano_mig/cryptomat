<?php

namespace backend\modules\admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadVeUploadVideorsion
 * @package frontend\models\form
 */
class UploadVideo extends Model
{
    /**
     * @var UploadedFile
     */
    public $videoFile;

    public function rules()
    {
        return [
            [['videoFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'mp4, 3gp'],
        ];
    }

    public function upload($path)
    {
        if ($this->validate()) {
            $this->videoFile->saveAs($path . $this->videoFile->baseName . '.' . $this->videoFile->extension);
            chmod($path . $this->videoFile->baseName . '.' . $this->videoFile->extension, 0755);
            return $this->videoFile->name;
        } else {
            return false;
        }
    }
}
