<?php

namespace backend\modules\admin;

use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['*'] = [
            'class' => 'yii\i18n\DbMessageSource',
            'forceTranslation' => true,
            'on missingTranslation' => [
                'backend\modules\admin\components\TranslationEventHandler',
                'handleMissingTranslation'
            ],
//            'sourceLanguage' => 'fr',
//            'basePath'       => '@frontend/modules/admin/messages',
//            'fileMap'        => [
//            ],
        ];
    }
}
