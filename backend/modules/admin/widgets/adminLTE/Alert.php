<?php

namespace backend\modules\admin\widgets\adminLTE;

class Alert extends \common\widgets\Alert
{
    public $options = ['class' => 'elevation-4 my-alert m-0 border border-secondary fade show'];
}
