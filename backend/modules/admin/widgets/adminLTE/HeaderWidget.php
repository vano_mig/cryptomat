<?php
namespace backend\modules\admin\widgets\adminLTE;

use yii\base\Widget;

class HeaderWidget extends Widget
{
    public $user;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('@backend/modules/admin/views/widgets/views/header/view', [
            'user' => $this->user
        ]);
    }
}