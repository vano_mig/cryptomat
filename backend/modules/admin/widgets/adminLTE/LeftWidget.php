<?php

namespace backend\modules\admin\widgets\adminLTE;

use backend\modules\user\models\User;
use yii\base\Widget;
use Yii;

class LeftWidget extends Widget
{
    public $user;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if (Yii::$app->user->identity->role == User::ROLE_USER) {
            return $this->render('@backend/modules/admin/views/widgets/views/left/viewUser', [
                'user' => $this->user
            ]);
        } else {
            return $this->render('@backend/modules/admin/views/widgets/views/left/view', [
                'user' => $this->user
            ]);
        }
    }
}