<?php
namespace backend\modules\admin\widgets\adminLTE;

use yii\base\Widget;

class FooterWidget extends Widget
{
    public $user;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('@backend/modules/admin/views/widgets/views/footer/view', [
            'user' => $this->user
        ]);
    }
}