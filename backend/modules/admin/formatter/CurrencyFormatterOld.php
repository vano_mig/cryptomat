<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Currency;

use Yii;
use yii\bootstrap4\Html;

class CurrencyFormatterOld extends BackendFormatter
{
	/**
	 * @param $value
	 * @return string
	 */
    public function asStatus($value)
    {
        $model = new Currency;
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }


    public function asCurrencyName($id): string
    {
        $model = Currency::find()->where(['id' => $id])->one();
        if (!$model) {
            return Yii::t('admin', 'Name did not set for this currency');
        }
        return $model->name;
    }

//    /**
//     * Get currency status
//     * @param $data string
//     * @return string
//     */
//    public function asCode($id, $html = false): string
//    {
//        $model = Currency::find()->where(['id' => $id, 'status' => Currency::STATUS_ACTIVE])->one();
//        if (!$model) {
//            $model = Currency::find()->where(['system_currency' => Currency::STATUS_ACTIVE])->one();
//        }
//        if (empty($html)){
//            return Html::tag('span', $model->code, ['class' => 'badge badge-primary']);
//        } else {
//            return $model->code;
//        }
//    }
//
//    /**
//     * Get currency symbol
//     * @param $currency_id integer
//     * @return string
//     */
//    public static function asCurrencySign($currency_id): string
//    {
//        switch ($currency_id) {
//            case 48: return '€'; break;
//            case 118: return '₽'; break;
//            case 59: return '₴'; break;
//            case 148: return '$'; break;
//            case null: return Yii::t('error', "Currency did not found"); break;
//            default: return '€';
//        }
//    }
}