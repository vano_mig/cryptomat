<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Support;
use Yii;
use yii\bootstrap4\Html;

class SupportFormatter extends BackendFormatter
{
    /**
     * Return category
     * @param string $value
     * @return string
     */
    public function asCategory($value): string
    {
        $model = new Support();
        $result = Yii::t('admin', 'not set');
        if ($value) {
            if ($value == $model::CATEGORY_TECHNICAL) {
                $result = Html::tag('span', Yii::t('support', $value),
                    ['class' => 'badge badge-primary']);
            } else {
                $result = Html::tag('span', Yii::t('support', $value),
                    ['class' => 'badge badge-warning']);
            }
        }
        return $result;
    }

    /**
     * Return status
     * @param string $value
     * @return string
     */
    public function asStatus($value): string
    {
        $model = new Support;
        $state = $model->listStatuses($value);
        if ($value == $model::STATUS_ACTIVE) {
            return Html::tag('span', $state, ['class' => 'badge badge-primary']);
        } elseif ($value == $model::STATUS_SOLVED) {
            return Html::tag('span', $state, ['class' => 'badge badge-success']);
        } else {
            return Html::tag('span', $state, ['class' => 'badge badge-secondary']);
        }
    }
}