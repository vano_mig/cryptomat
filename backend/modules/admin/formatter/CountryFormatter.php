<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Country;
use Yii;
use yii\bootstrap4\Html;

class CountryFormatter extends BackendFormatter
{
    /**
     * Get country name
     * @param $data object
     * @return string
     */
    public function asLang($data): string
    {
        if ($data->iso_code == 'uk') {
            $res = Html::tag('span','', ['class' => 'mr-2 flag-icon flag-icon-ua']).Html::tag('span', $data->iso_code);
        }
        elseif ($data->iso_code == 'en') {
            $res = Html::tag('span','', ['class' => 'mr-2 flag-icon flag-icon-gb']).Html::tag('span', $data->iso_code);
        }
        else
        {
            $res = Html::tag('span','', ['class' => 'mr-2 flag-icon flag-icon-'.$data->iso_code]).Html::tag('span', $data->iso_code);
        }
        return $res;
    }

    /**
     * @param $id
     * @return string
     */
    public function asCountryById($id): string
    {
        $info = Country::find()->where(['id' => $id])->one();
        return $this->asLang($info);
    }
}