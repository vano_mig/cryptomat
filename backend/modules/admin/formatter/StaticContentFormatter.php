<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Language;

/**
 * Class StaticContentFormatter
 * @package backend\modules\admin\formatter
 */
class StaticContentFormatter extends BackendFormatter
{
    /**
     * @param $model
     * @return string
     */
    public static function getLanguages($model)
    {

        $language = [];
        foreach ($model as $item) {
            if (!empty($languageModel = Language::findOne(['iso_code' => $item->language]))) {
                $language[] = "$languageModel->name" . ', ';
            }
        }
        return substr(implode($language),0,-2);
    }
}