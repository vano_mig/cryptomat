<?php
//
//namespace backend\modules\admin\formatter;
//
//use backend\components\BackendFormatter;
//use backend\modules\admin\models\ApiName;
//use Yii;
//use yii\bootstrap4\Html;
//
//class ApiNameFormatter extends BackendFormatter
//{
//
//	/**
//	 * @return array|\yii\db\ActiveRecord[]
//	 */
//	public static function getElementById($id)
//	{
//		$mod = ApiName::find()->where(['id' => $id])->one();
//		$res['id'] = $mod ? $mod->id : 0;
//		$res['name'] = $mod ? $mod->api_name : 0;
//		$res['replenish'] = $mod ? $mod->replenish : 0;
//		$res['exchange'] = $mod ? $mod->exchange : 0;
//		return $res;
//	}
//
//	/**
//	 * Get currency status
//	 * @param $value string
//	 * @return string
//	 */
//	public function asStatus($value): string
//	{
//		$status = ApiName::getStatuses($value);
//		if ($value == ApiName::STATUS_ACTIVE) {
//			return Html::tag('span', $status, ['class' => 'badge badge-success']);
//		}
//		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
//	}
//
//	public function getName($value):string
//    {
//        $result = \Yii::t('admin', 'not set');
//        $model = ApiName::findOne(['id'=>$value, 'active'=>ApiName::STATUS_ACTIVE]);
//        if($model)
//            $result = $model->api_name;
//        return $result;
//    }
//
//    /**
//     * Get type
//     * @param $value string
//     * @return string
//     */
//    public function asType($value): string
//    {
//        $status = ApiName::getTypes();
//        if(array_key_exists($value, $status)) {
//            if ($value == ApiName::TYPE_EXCHANGE) {
//                return Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
//            }
//            return Html::tag('span', $status[$value], ['class' => 'badge badge-secondary']);
//        }
//        return Yii::t('admin', 'not set');
//    }
//}