<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Language;
use backend\modules\admin\models\SourceMessage;
use yii\bootstrap4\Html;
use Yii;

class TranslationFormatter extends BackendFormatter
{
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public function asLang($data): string
    {
        $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-' . $data->language]) . ' ' . Html::tag('span', $data->language);
        if ($data->language == 'uk') {
            $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-ua']) . ' ' . Html::tag('span', $data->language);
        }
        if ($data->language == 'en') {
            $res = Html::tag('span', '', ['class' => 'flag-icon flag-icon-gb']) . ' ' . Html::tag('span', $data->language);
        }
        return $res;
    }

    /**
     * Get Translation status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
        $model = new SourceMessage();
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_OK) {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-success']);
        } elseif ($value == $model::STATUS_EDIT) {
            return Html::tag('span', Yii::t('currency', $status),
                ['class' => 'badge badge-warning']);
        }
    }

    /**
     * @param $value
     * @return array
     */
    public function getTranslationLanguage()
    {
        $array = [];
        $list = ['message' => Yii::t('admin', '--All--')];
        $list += (new  Language())->activeLanguages;
        foreach ($list as $key => $item) {
            $array[$key] = Yii::t('language', $item);
        }
        return $array;
    }

}