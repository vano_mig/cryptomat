<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Transaction;
use backend\modules\virtual_terminal\models\Atm;
use yii\helpers\Url;
use Yii;
use yii\bootstrap4\Html;

class TransactionFormatter extends BackendFormatter
{
    /**
     * Get terminal name
     * @param integer $id
     * @return string
     */
    public function asTerminal($id)
    {
        $result = Yii::t('admin', 'not set');
        $model = Atm::findOne($id);
        if($model)
            $result = $model->name;
        return $result;
    }

    /**
     * Get widget unique
     * @param integer $id
     * @return string
     */
    public function asWidget($id)
    {
        $result = Yii::t('admin', 'not set');
        $model = Widget::findOne($id);
        if($model)
            $result = $model->uid;
        return $result;
    }

    /**
     * Get transaction type
     * @param object $data
     * @return string
     */
    public function asType($data)
    {
        $result = $data->type;
        $list = $data->getType();
        if(array_key_exists($data->type, $list))
            if ($data->type == Transaction::TYPE_BUY_CASH) {
                $result = Html::tag('span', $list[$data->type], ['class' => 'badge badge-success']);
            } else {
                $result = Html::tag('span', $list[$data->type], ['class' => 'badge badge-primary']);
            }
        return $result;
    }

    /**
     * Get transaction status
     * @param object $data
     * @return string
     */
    public function asStatus($data)
    {
        $result = $data->status;
        $list = $data->getStatus();
        if(array_key_exists($data->status, $list))
        	switch ($data->status) {
				case Transaction::AWAITING_CONFIRM:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-secondary']);
					break;
				case Transaction::STATUS_WAIT:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-warning']);
					break;
				case Transaction::STATUS_SUCCESS:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-primary']);
					break;
				case Transaction::STATUS_USED:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-success']);
					break;
				case Transaction::STATUS_CANCELED:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-danger']);
					break;
                case Transaction::STATUS_SEND_CRYPTO_SUCCESS:
                    $result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-success']);
                    break;
                case Transaction::STATUS_RECEIVE_CRYPTO:
                    $result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-success']);
                    break;
                case Transaction::STATUS_SEND_CASH_SUCCESS:
                    $result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-success']);
                    break;
				default:
					$result = Html::tag('span', $list[$data->status], ['class' => 'badge badge-info']);
					break;
			}
        return $result;
    }

    public function asTransaction($value):string
    {
        $result = Yii::t('admin', 'not set');
        if($value !== null) {
            $result = Html::a($value, Url::toRoute(['/admin/transaction/view?id='.$value]), ['target'=>'_blank']);
        }
        return $result;
    }

    public function asTransactionUid($value):string
    {
        $result = '---';
        if($value !== null) {
            $transaction=Transaction::find()->where(['id'=>$value])->one();
            if ($transaction !== null && $transaction->transaction_id) {
                $result = Yii::t('api', 'ID').' '. $transaction->id. ' '.Html::a($transaction->transaction_id, Url::toRoute(['/admin/transaction/view?id='.$value]), ['target'=>'_blank']);
            }
        }
        return $result;
    }
}