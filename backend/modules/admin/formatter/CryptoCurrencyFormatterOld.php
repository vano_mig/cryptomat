<?php

namespace backend\modules\admin\formatter;


use backend\components\BackendFormatter;
use backend\modules\admin\models\CryptoCurrency;
use yii\bootstrap4\Html;

class CryptoCurrencyFormatterOld extends BackendFormatter
{

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        $mod = CryptoCurrency::find()->all();
        $res = [];
        if($mod)
        {
            foreach($mod as $val)
            {
                $res[$val['id']] = $val['name'];
            }
        }
        return $res;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElementByName($name)
    {
        $mod = CryptoCurrency::find()->where(['name' => $name])->one();
		$res['id'] = $mod ? $mod->id : 0;
		$res['name'] = $mod ? $mod->name : 0;
		$res['code'] = $mod ? $mod->code : 0;
		$res['active'] = $mod ? $mod->active : 0;
        return $res;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElementById($id)
    {
        $mod = CryptoCurrency::find()->where(['id' => $id])->one();
        $res['id'] = $mod ? $mod->id : 0;
        $res['name'] = $mod ? $mod->name : 0;
        $res['code'] = $mod ? $mod->code : 0;
        $res['active'] = $mod ? $mod->active : 0;
        return $res;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getType()
    {
        return [0 => 'Buy', 1 => 'Sell'];
    }

	/**
	 * @param $value
	 * @return mixed
	 */
    static public function asStatus($value): string
    {
        $status = CryptoCurrency::getStatuses($value);
        if ($value == CryptoCurrency::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }

}