<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\ApiName;
use backend\modules\admin\models\ApiNameCryptoCurrency;
use backend\modules\admin\models\ApiNameCurrency;
use backend\modules\admin\models\CryptoCurrency;
use Yii;
use yii\bootstrap4\Html;

class ApiFormatter extends BackendFormatter
{

	public static function apiGetType($value)
	{
		$status = ApiName::getTypes($value);

		if ($value == ApiName::TYPE_EXCHANGE) {
			return Html::tag('span', $status, ['class' => 'badge badge-success']);
		}
		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	public static function apiGetStatus($value)
	{
		$status = ApiName::getStatuses($value);
		if ($value == ApiName::STATUS_ACTIVE) {
			return Html::tag('span', $status, ['class' => 'badge badge-success']);
		}
		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	public static function apiGetNameById($id)
	{
		$model = ApiName::getById($id);

		if (!$model)
			return Yii::t('admin', 'Name did not set for this api');

		return $model['name'];
	}

	public static function apiGetIdByName($name)
	{
		$model = ApiName::getByName($name);

		if (!$model)
			return Yii::t('admin', 'Id did not found for this api');

		return $model['name'];
	}

	public static function apiGetAllByType($type)
	{
		$model = ApiName::getListByType($type);

		if (!$model)
			return [];

		return $model;
	}

	//

	public static function apiCurrencyGetStatus($value)
	{
		$status = ApiNameCurrency::getStatuses($value);
		if ($value == ApiNameCurrency::STATUS_ACTIVE) {
			return Html::tag('span', $status, ['class' => 'badge badge-success']);
		}
		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	public static function apiCurrencyGetPairById($id)
	{
		$model = ApiNameCurrency::getById($id);
		$api = ApiFormatter::apiGetNameById($model['api_id']);
		$currency = CurrencyFormatter::currencyGetNameById($model['currency']);

		return $api . ' ' . $currency;
	}

	public static function apiCurrencyGetAllPair()
	{
		$models = ApiNameCurrency::getAll();
		$res = [];

		foreach ($models as $key => $value)
		{
			$api = ApiFormatter::apiGetNameById($value['api_id']);
			$currency = CurrencyFormatter::currencyGetNameById($value['currency']);
			$res[$key] = $api . ' ' . $currency;
		}

		return $res;
	}

	//

	public static function apiCryptoCurrencyGetStatus($value)
	{
		$status = ApiNameCryptoCurrency::getStatuses($value);
		if ($value == ApiNameCryptoCurrency::STATUS_ACTIVE) {
			return Html::tag('span', $status, ['class' => 'badge badge-success']);
		}
		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	public static function apiCryptoCurrencyGetPairById($id)
	{
		$model = ApiNameCryptoCurrency::getById($id);
		$api = ApiFormatter::apiGetNameById($model['api_id']);
		$currency = CurrencyFormatter::cryptoCurrencyGetNameById($model['crypto_currency']);

		return $api . ' ' . $currency;
	}

	public static function apiCryptoCurrencyGetAllPair()
	{
		$models = ApiNameCryptoCurrency::getAll();
		$res = [];

		foreach ($models as $key => $value)
		{
			$api = ApiFormatter::apiGetNameById($value['api_id']);
			$currency = CurrencyFormatter::cryptoCurrencyGetNameById($value['crypto_currency']);
			$res[$key] = $api . ' ' . $currency;
		}

		return $res;
	}

	public static function apiCryptoCurrencyGetTypePair($type)
	{
		$models = ApiNameCryptoCurrency::getAll();
		$res = [];
		foreach ($models as $key => $value)
		{
			$api = ApiName::getById($value['api_id']);
			if ($api[$type] == ApiName::TYPE_EXCHANGE) {
				$currency = CurrencyFormatter::cryptoCurrencyGetNameById($value['crypto_currency']);
				$res[$key] = $api['name'] . ' ' . $currency;
			}
		}
		return $res;
//		return $model['name'];
	}
}