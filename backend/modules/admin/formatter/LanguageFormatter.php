<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Language;
use yii\bootstrap4\Html;

class LanguageFormatter extends BackendFormatter
{
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public function asLangFlag($data): string
    {

        $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $data->iso_code]);
        if ($data->iso_code == 'uk') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']);
        }
        if ($data->iso_code == 'en') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']);
        }
        return $res;
    }
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public function asLang($data): string
    {

        $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $data->iso_code]) . Html::tag('span',
                $data->iso_code);
        if ($data->iso_code == 'uk') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']) . Html::tag('span', $data->iso_code);
        }
        if ($data->iso_code == 'en') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']) . Html::tag('span', $data->iso_code);
        }
        return $res;
    }

    /**
     * Get Language status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
        $model = new Language;
        $status = $model->getStatuses($value);
        if ($value == $model::ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }
}
