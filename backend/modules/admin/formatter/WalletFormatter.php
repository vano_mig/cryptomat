<?php

namespace backend\modules\admin\formatter;


use backend\components\BackendFormatter;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\admin\models\ReplenishWallet;
use common\models\Log;
use Yii;
use yii\bootstrap4\Html;

class WalletFormatter extends BackendFormatter
{

    /**
     * @param $value
     * @return string
     */
    public function replenishGetStatus($value): string
    {
        $status = ReplenishWallet::listStatuses($value);
        if ($value == ReplenishWallet::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }


    /**
     * @param $value
     * @return string
     */
    public function exchangeGetStatus($value): string
    {
        $status = ExchangeWallet::listStatuses($value);
        if ($value == ExchangeWallet::STATUS_ACTIVE) {
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        }
        return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
    }

    static public function exchangeGetAll(): array
    {
        $models = ExchangeWallet::getAll();

        foreach ($models as $key => $value) {
            $models[$key] = ApiFormatter::apiCurrencyGetPairById($value);
        }

        return $models;
    }

    static public function replenishGetAll(): array
    {
        $models = ReplenishWallet::getAll();

        foreach ($models as $key => $value) {
            $models[$key] = ApiFormatter::apiCryptoCurrencyGetPairById($value);
        }

        return $models;
    }

    static public function getBalance($data)
    {
        $name = $data->provider->apiName->api_name;
        $currency = $data->provider->cryptoCurrency->code;
        $path = "\\backend\modules\api\models\\" . $name;
        $config = [
            'public_key' => $data->api_key,
            'secret_key' => $data->api_secret,
            'client_id' => $data->client_id
        ];
        $model = new $path($config);
        $result = $model->getBalance(['currency' => $currency]);
        $response = $result['status'] == 'success' ? $result['info'] : false;
        return $response;
    }

    static public function getFeeBuy($data)
    {
		$currencyTwo = null;

        $name = $data->provider->apiName->api_name;
        $currency = $data->provider->cryptoCurrency->code;
        if ($data->exchange_wallet)
        	$currencyTwo = $data->exchange->provider->getCurrencyName()->one()->code;
        $path = "\\backend\modules\api\models\\" . $name;
        $config = [
            'public_key' => $data->api_key,
            'secret_key' => $data->api_secret,
            'client_id' => $data->client_id
        ];
        $model = new $path($config);
        Log::logAtm('$currencyTwo', $currencyTwo);
		$result = $model->getFee(['currencyCrypto' => $currency, 'currencyCash' => $currencyTwo]);
        $response = $result['status'] == 'success' ? $result['info'] : false;
        return $response;
    }

	static public function getBalanceCash($data)
    {
        $name = $data->provider->apiName->api_name;
        $currency = strtolower($data->provider->currencyName->code);
        $path = "\\backend\modules\api\models\\" . $name;
        $config = [
            'public_key' => $data->api_key,
            'secret_key' => $data->api_secret,
            'client_id' => $data->client_id
        ];
        $model = new $path($config);
        $result = $model->getBalance(['currency' => $currency]);
        $response = $result['status'] == 'success' ? $result['info'] : false;
        return $response;
    }

}