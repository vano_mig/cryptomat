<?php

namespace backend\modules\admin\formatter;

use backend\components\BackendFormatter;

use backend\modules\admin\models\Currency;
use backend\modules\admin\models\ApiNameCryptoCurrency;
use backend\modules\admin\models\CryptoCurrency;

use Yii;
use yii\bootstrap4\Html;

class CurrencyFormatter extends BackendFormatter
{
	//

	static public function currencyGetStatus($value)
	{
		$status = Currency::getStatuses($value);

		if ($status == Currency::STATUS_ACTIVE)
			return Html::tag('span', $status, ['class' => 'badge badge-success']);

		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	static public function currencyGetIdByName($name)
	{
		$model = Currency::getByName($name);

		if (!$model)
			return Yii::t('admin', 'Id did not found for this currency');

		return $model['id'];
	}

	static public function currencyGetNameById($id)
	{
		$model = Currency::getById($id);

		if (!$model)
			return Yii::t('admin', 'Name did not set for this currency');

		return $model['name'];
	}

	//

	static public function cryptoCurrencyGetStatus($value)
	{
		$status = CryptoCurrency::getStatuses($value);

		if ($value == CryptoCurrency::STATUS_ACTIVE)
			return Html::tag('span', $status, ['class' => 'badge badge-success']);

		return Html::tag('span', $status, ['class' => 'badge badge-secondary']);
	}

	static public function cryptoCurrencyGetIdByName($name)
	{
		$model = CryptoCurrency::getByName($name);

		if (!$model)
			return Yii::t('admin', 'Id did not found for this currency');

		return $model['id'];
	}

	static public function cryptoCurrencyGetNameById($id)
	{
		$model = CryptoCurrency::getById($id);

		if (!$model)
			return Yii::t('admin', 'Name did not found for this currency');

		return $model['name'];
	}

}