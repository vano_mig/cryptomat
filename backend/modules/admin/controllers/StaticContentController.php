<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Language;
use backend\modules\admin\models\StaticContentLanguage;
use Yii;
use backend\modules\admin\models\StaticContent;
use backend\modules\admin\models\StaticContentSearch;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StaticContentController implements the CRUD actions for StaticContent model.
 */
class StaticContentController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StaticContent models.
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     */
    public function actionIndex()
    {
        $this->accessRules('static_content.listview');
        $searchModel = new StaticContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StaticContent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     */
    public function actionView($id)
    {
        $this->accessRules('static_content.view');
        $languages = Language::find()->all();
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'languages' => $languages,
        ]);
    }

    /**
     * Creates a new StaticContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     */
    public function actionCreate()
    {
        $this->accessRules('static_content.create');
        $languages = Language::find()->all();
        $model = new StaticContent();

        if (Yii::$app->request->post('Save')) {
            $name = Yii::$app->request->post('StaticContent');
            if ($name) {
                $model->name = $name['name'];
                $validation = false;
                $arrayLanguage = [];
                $newContent = Yii::$app->request->post('StaticContentLanguage');
                if ($newContent) {
                    foreach ($newContent as $item) {
                        $modelContent = new StaticContentLanguage();
                        $modelContent->language = array_key_exists('lang', $item) ? $item['lang'] : '';
                        $modelContent->content = $item['content'];
                        $modelContent->content_id = $model->id;
                        if(!$modelContent->validate()) {
                            $validation = true;
                        }
                        $arrayLanguage[] = $modelContent;
                    }
                }
                $validation = $validation == true ? false : true;
                if($validation && $model->validate()) {
                    $model->save();
                    foreach ($arrayLanguage as $item) {
                        $item->content_id = $model->id;
                        $item->save();
                    }
                }
            }
            return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
        }

        return $this->render('create', [
            'model' => $model,
            'languages' => $languages,
        ]);
    }

    /**
     * Updates an existing StaticContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('static_content.update');
        $model = $this->findModel($id);
        $languages = Language::find()->all();
        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post('Save')) {
                $name = Yii::$app->request->post('StaticContent');
                if ($name) {
                    $model->name = $name['name'];
                    $validation = false;
                    $newContent = Yii::$app->request->post('StaticContentLanguage');
                    $arrayLanguage = [];
                    if ($newContent) {
                        foreach ($newContent as $item) {
                            $modelContent = new StaticContentLanguage();
                            $modelContent->language = array_key_exists('lang', $item) ? $item['lang'] : '';
                            $modelContent->content = $item['content'];
                            $modelContent->content_id = $model->id;
                            if(!$modelContent->validate()) {
                                $validation = true;
                            }
                            $arrayLanguage[] = $modelContent;
                        }
                    }
                    $validation = $validation == true ? false : true;
                    if($validation && $model->validate()) {
                        StaticContentLanguage::deleteAll(['content_id'=>$model->id]);
                        $model->save();
                        foreach ($arrayLanguage as $item) {
                            $item->save();
                        }
                    }
                    return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
                }
            }
            if (Yii::$app->request->post('deleteTranslate')) {
                $contentDel = Yii::$app->request->post('deleteTranslate');
                if ($mod = StaticContentLanguage::findOne(['id' => $contentDel])) {
                    $mod->delete();
                    return $this->render('update', [
                        'model' => $model,
                        'languages' => $languages,
                    ]);
                }
            }
        }
        return $this->render('update', [
            'model' => $model,
            'languages' => $languages,
        ]);
    }

    /**
     * Deletes an existing StaticContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->accessRules('static_content.delete');
        $this->findModel($id)->delete();
        $modelAllContent = $this->findModelContent($id);
        foreach ($modelAllContent as $modelContent) {
            $modelContent->delete();
        }
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the StaticContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StaticContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StaticContent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

    /**
     * Finds the StaticContentLanguage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelContent($id)
    {
        if (($model = StaticContentLanguage::findAll(['content_id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

}
