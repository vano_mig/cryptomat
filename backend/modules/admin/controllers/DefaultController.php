<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\CryptoCurrency;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\Dashboard;
use backend\modules\admin\models\Transaction;
use common\models\tool\DTO;
use Yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends FrontendController
{

    public function actionIndex()
    {

        $DTO = new DTO();
        try {
            $receiveCurrency = [];
            $sentCurrency = [];
            $transactionHistorySearch = new Transaction();
            $transactionHistoryDataProvider = $transactionHistorySearch->dashboardSearch(Yii::$app->request->queryParams);
            $cryptoCurrency = new CryptoCurrency();
            $cryptoCurrencyList = $cryptoCurrency->getListCodeFull();
            $currency = new Currency();
            $CurrencyList = $currency->getCodeName();
            $modelDashboard = new Dashboard();
            $sourcesTransaction = $modelDashboard->getSourcesTransaction();

            $currencyIn = array_merge($cryptoCurrencyList, $CurrencyList);
            $currencyOut = $currencyIn;
            $arrayIn = [];
            $arrayOut = [];
            foreach ($currencyIn as $cuIn) {
                $arrayIn[$cuIn] = $modelDashboard->getTransactionsCurrencyIn($sourcesTransaction, $cuIn, $currencyIn);
                $receiveCurrency = array_merge($modelDashboard->getReceiveCurrency($sourcesTransaction, $cuIn), $receiveCurrency);
            }
            $receiveCurrency = $modelDashboard->getProfitColor($receiveCurrency);
            foreach ($currencyOut as $cuOut) {
                    $arrayOut[$cuOut] = $modelDashboard->getTransactionsCurrencyOut($sourcesTransaction, $cuOut, $currencyIn);
                $sentCurrency = array_merge($modelDashboard->getSentCurrency($sourcesTransaction, $cuOut), $sentCurrency);
            }
            $sentCurrency = $modelDashboard->getProfitColor($sentCurrency);
            $DTO->transactionHistoryDataProvider = $transactionHistoryDataProvider;

            $DTO->currencysIn = $currencyIn;
            $DTO->currencysOut = $currencyOut;
            $DTO->arrayIn = $arrayIn;
            $DTO->arrayOutOut = $arrayOut;
        } catch (\Exception $e) {
            echo "<pre>";print_r($e->getMessage());
            echo "<pre>";print_r($e->getLine());die;
            $DTO->error = true;
        }
//echo "<pre>";print_r($DTO);die;
        $profit = (new Dashboard())->getDashboard();
        return $this->render('index', [
            'DTO' => $DTO,
            'profit' =>$profit,
            'receiveCurrency' =>$receiveCurrency,
            'sentCurrency' =>$sentCurrency
        ]);
    }

}