<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\ApiName;
use backend\modules\user\models\User;
use Yii;
use backend\modules\admin\models\ThirdPartyApi;
use backend\modules\admin\models\ThirdPartyApiSearch;
use yii\bootstrap4\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ThirdPartyApiController implements the CRUD actions for ThirdPartyApi model.
 */
class ThirdPartyApiController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThirdPartyApi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('3rd_party_api.listview');
        $searchModel = new ThirdPartyApiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ThirdPartyApi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('3rd_party_api.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionNewForm()
	{
		$post = Yii::$app->request->post();
		$json['data'] = null;
		if ( isset($post['id']) ) {
			try {
				$model = $this->findModel($post['model_id']);
			} catch (NotFoundHttpException $e) {
				$model = new ThirdPartyApi();
			}
			$form = new ActiveForm();
			$apiModel = ApiName::findOne($post['id']);
			if ($apiModel ) {
                try
                {
                    $json['data'] = $this->renderAjax($apiModel->form, [
                        'model' => $model,
                        'form' => $form,
                    ]);
                    $json['status'] = 'success';
                }
                catch (NotFoundHttpException $e)
                {
                    $json['status'] = 'error';
                }
			}
		}
		return json_encode($json);
	}

    /**
     * Creates a new ThirdPartyApi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('3rd_party_api.create');
        $model = new ThirdPartyApi();

        if ($model->load(Yii::$app->request->post())) {
            if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
                $model->company_id = Yii::$app->user->identity->company_id;
            }
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ThirdPartyApi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('3rd_party_api.update');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
                $model->company_id = Yii::$app->user->identity->company_id;
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ThirdPartyApi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('3rd_party_api.delete');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ThirdPartyApi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThirdPartyApi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = ThirdPartyApi::findOne($id);
        } else {
            $model = ThirdPartyApi::findOne(['id'=>$id, 'company_id'=>Yii::$app->iser->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }


        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
