<?php

namespace backend\modules\admin\controllers;

use Yii;
use backend\modules\admin\models\Currency;
use backend\modules\admin\models\CurrencySearch;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CurrencyController implements the CRUD actions for Currency model.
 */
class CurrencyController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Currency models.
     * @throws ForbiddenHttpException if access deny
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('currency.listview');
        $searchModel = new CurrencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Currency model.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('currency.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Currency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws ForbiddenHttpException if access deny
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('currency.create');
        $model = new Currency();

        if ($model->load(Yii::$app->request->post())) {
            if($model->system_currency == Currency::STATUS_ACTIVE) {
                Currency::updateAll(['system_currency'=>Currency::STATUS_INACTIVE]);
            }
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Currency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @throws ForbiddenHttpException if access deny
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('currency.update');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->system_currency == Currency::STATUS_ACTIVE) {
                Currency::updateAll(['system_currency'=>Currency::STATUS_INACTIVE]);
            }
            if($model->system_currency == Currency::STATUS_INACTIVE) {
                $active = Currency::find()->where(['system_currency'=>Currency::STATUS_ACTIVE])->one();
                if(!$active) {
                    $active = Currency::find()->where(['status'=>Currency::STATUS_ACTIVE])->one();
                    if(!$active) {
                        $model->system_currency = Currency::STATUS_ACTIVE;
                    }
                    else {
                        $active->status = Currency::STATUS_ACTIVE;
                        $active->system_currency = Currency::STATUS_ACTIVE;
                        $active->save();
                    }
                }
            }
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Currency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->accessRules('currency.delete');
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Currency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Currency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Currency::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

    /**
     * Makes language active
     * @param $id integer
     * @throws ForbiddenHttpException if access deny
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionActive($id)
    {
        $this->accessRules('currency.update');
        $model = $this->findModel($id);
        if($model->status == $model::STATUS_ACTIVE)
        {
            $model->status = $model::STATUS_INACTIVE;
        }
        else {
            $model->status = $model::STATUS_ACTIVE;
        }
        if($model->save()) {
            return $this->redirect(Url::toRoute('index'));
        }
    }
}
