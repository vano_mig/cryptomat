<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Language;
use backend\modules\admin\models\Message;
use backend\modules\admin\models\SourceMessage;
use backend\modules\admin\models\SourceMessageSearch;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class TranslationController
 * @package backend\modules\admin\controllers
 */
class TranslationController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SourceMessage models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('translation.listview');
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     *
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('translation.view');
        $modelLanguage = new Language();
        $langs = $modelLanguage->activeLanguages;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'langs' => $langs
        ]);
    }

    /**
     * Create view.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->accessRules('translation.create');
        $model = new SourceMessage();
        $modelMessage = new Message();
        $modelLanguage = new Language();
        $lang = $modelLanguage->activeLanguages;

        return $this->render('create', [
            'model' => $model,
            'modelMessage' => $modelMessage,
            'lang' => $lang,
        ]);
    }

    /**
     * Update view.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('translation.update');
        $model = $this->findModel($id);
        $modelLanguage = new Language();
        $lang = $modelLanguage->activeLanguages;
        return $this->render('update', [
            'model' => $model,
            'lang' => $lang,
        ]);
    }

    /**
     * Updates logic.
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateTranslation()
    {
        if (Yii::$app->request->post('save')) {
            $post = Yii::$app->request->post();
            $model = $this->findModel($post['SourceMessage']['id']);
            $modelMessage = $model->messages;
            if (Model::loadMultiple($modelMessage, Yii::$app->request->post()) && Model::validateMultiple($modelMessage)) {
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $model->save();
                    foreach ($modelMessage as $setting) {
                        $setting->save(false);
                    }
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                    return $this->redirect(Url::toRoute(['view', 'id' => $post['SourceMessage']['id']]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
            return $this->redirect(Url::toRoute(['update', 'id' => $post['SourceMessage']['id']]));
        }
    }

    /**
     * Create logic.
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateTranslation()
    {
        if (Yii::$app->request->post('save')) {
            $post = Yii::$app->request->post();
            $model = new SourceMessage();
            if ($model->load($post) && $model->validate()) {
                $model->save();
                foreach ($post['Message'] as $item) {
                    $modelMessage = new Message();
                    $modelMessage->id = $model->id;
                    $modelMessage->language = $item['language'];
                    $modelMessage->translation = $item['translation'];
                    if ($modelMessage->validate()) {
                        $modelMessage->save();
                    } else {
                        $modelMessage::deleteAll(['id'=> $model->id]);
                        $model->delete();
                        Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                        return $this->redirect(Url::toRoute(['create']));
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
        }
        return $this->redirect(Url::toRoute(['create']));
    }

    /**
     * Deletes translation.
     *
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->accessRules('translation.delete');
        $this->findModel($id)->delete();
        Message::deleteAll(['id' => $id]);
        Yii::$app->session->setFlash('success', Yii::t('admin', 'record deleted'));
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}