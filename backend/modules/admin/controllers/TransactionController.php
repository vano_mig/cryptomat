<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Transaction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends FrontendController
{
	/**
	 * @return array|array[]
	 */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('transaction.listview');
        $searchModel = new Transaction();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->request->get('Transaction')) {
            $searchModel->attributes = Yii::$app->request->get('Transaction');
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Transaction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('transaction.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionChangeStatus()
    {
        $result['info'] = null;
        $result['error'] = null;

        if (!empty($_POST) && !empty($_POST['txid']) && isset($_POST['status'])) {

            $model = Transaction::findOne(['transaction_id' => $_POST['txid']]);
            if ($model !== null && ($model->status == 1 || $model->status == 2)) {
                $model->status = $_POST['status'] == 1 ? 2 : 0;
                $model->save();
                $result['info']['success'] = true;
            } else {
                $result['error'] = 'Transaction not found!';
            }

        }

        return $this->asJson($result);
    }

    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('api', 'The requested page does not exist.'));
    }
}