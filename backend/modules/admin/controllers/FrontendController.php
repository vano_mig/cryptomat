<?php

namespace backend\modules\admin\controllers;

use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use Yii;

class FrontendController extends Controller
{
    /**
     * @param $action
     * @return bool|void
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect((Url::toRoute('/login')))->send();
        }
        return parent::beforeAction($action);
    }

    /**
     * Check access rule
     * @param $rule
     * @return mixed
     * @throws ForbiddenHttpException if user access deny
     */
    public function accessRules($rule)
    {
        if (!Yii::$app->user->can($rule)) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }
    }
}