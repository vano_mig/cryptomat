<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Support;
use backend\modules\admin\models\SupportSearch;
use backend\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * SupportController implements the CRUD actions for Support model.
 */
class SupportController extends FrontendController
{
	/**
	 * @return array|array[]
	 */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Support models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('support.listview');
        $modelSearch = new SupportSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    /**
     * Displays a single Support model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('support.view');
        $model = $this->findModel($id);
        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new Support model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->accessRules('support.create');
        $model = new Support();

        if ($model->load(Yii::$app->request->post())) {
            $model->client_id = Yii::$app->user->id;
            $model->answerer_id = Yii::$app->user->id;
            $model->ticket_id = null;
            $model->status = $model::STATUS_ACTIVE;
            if ($model->save()) {
                Yii::$app->mailer->compose(['html' => 'sendTicket-html', 'text' => 'sendTicket-text'], ['model' => $model, 'newTicket' => true])
                    ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                    ->setTo([Yii::$app->user->identity->email, Yii::$app->params['ivanEmail'], Yii::$app->params['patrickEmail'], 'grid111@i.ua'])
                    ->setSubject('New ticket #' . $model->id)
                    ->send();
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Answer save.
     *
     * @return yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionAnswer()
    {
        $this->accessRules('support.view');
        if (Yii::$app->request->post('Support')) {
            $model = $this->findModel(Yii::$app->request->post('Support')['id']);
            $modelAnswer = new Support();
            $modelAnswer->attributes = $model->attributes;
            if ($modelAnswer->load(Yii::$app->request->post())) {
                $modelAnswer->ticket_id = $model->id;
                $modelAnswer->answerer_id = Yii::$app->user->id;
                if ($modelAnswer->save()) {
                    $user = User::findOne($model->client_id);
                    Yii::$app->mailer->compose(['html' => 'sendTicket-html', 'text' => 'sendTicket-text'], ['model' => $model, 'newTicket' => false])
                        ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                        ->setTo([$user->email, Yii::$app->params['ivanEmail'], Yii::$app->params['patrickEmail'], 'grid111@i.ua'])
                        ->setSubject('Answer to ticket #' . $model->id)
                        ->send();
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                }
            }
        }
        return $this->redirect(Url::toRoute(['view', 'id' => Yii::$app->request->post('Support')['id']]));
    }

    /**
     * Updates an existing Support model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('support.update');
        $model = Support::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Support model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->accessRules('support.delete');
        Support::deleteAll(['ticket_id' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Support model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Support the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Support::find()->where(['id' => $id, 'ticket_id' => null])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
