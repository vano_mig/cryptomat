<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\formatter\WalletFormatter;
use backend\modules\admin\models\ExchangeWallet;
use backend\modules\user\models\User;
use common\models\Log;
use Yii;
use backend\modules\admin\models\ReplenishWallet;
use backend\modules\admin\models\ReplenishWalletSeatch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReplenishWalletController implements the CRUD actions for ReplenishWallet model.
 */
class ReplenishWalletController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * Lists all ReplenishWallet models.
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
    public function actionIndex()
    {
        $this->accessRules('identification_profile.list');
        $searchModel = new ReplenishWalletSeatch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReplenishWallet model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('identification_profile.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ReplenishWallet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('identification_profile.create');
        $model = new ReplenishWallet();

        if ($model->load(Yii::$app->request->post())) {
			if (!Yii::$app->user->can(User::ROLE_ADMIN))
				$model->company_id = Yii::$app->user->identity->company_id;
			$this->checkBalance($model);
        	$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function checkBalance($model) {
		if (Yii::$app->request->post()['submitForm'] == 'test') {
			$status = 'dark';

//			$apiInfo = ApiCryptoCurrencyFormatter::getElementById($model->provider_id, true);
//
//			$apiNameInfo = ApiNameFormatter::getElementById($apiInfo['api_id']);
//
//			$connt_path = "backend\modules\api\models\\{$apiNameInfo["name"]}";
//
//			$cryptoInfo = CryptoCurrencyFormatterOld::getElementById($apiInfo['crypto_currency']);
//
//			$test = new $connt_path(['public_key' => $model->api_key, 'secret_key' => $model->api_secret, 'client_id' => $model->client_id]);
//
//			$balance = $test->getBalance($cryptoInfo['code']);
//			if ($balance === null)
//				$res = Yii::t('admin', 'Incorrect data!');
//			else
//				$res = $cryptoInfo['name'] . ': ' . $balance;
//
//			Yii::$app->session->setFlash($status, $res);
		}
	}

    /**
     * Updates an existing ReplenishWallet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('identification_profile.update');
        $model = $this->findModel($id);
        $secret = $model->api_secret;
        if ($model->load(Yii::$app->request->post())) {
			$this->checkBalance($model);
			if(!$model->api_secret)
			    $model->api_secret = $secret;
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ReplenishWallet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('identification_profile.delete');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReplenishWallet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReplenishWallet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = ReplenishWallet::findOne($id);
        } else {
            $model = ReplenishWallet::findOne(['id'=>$id, 'company_id'=>Yii::$app->user->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

	public function actionCheckBalance(): string
	{
		$post = Yii::$app->request->post();
		$res['status'] = 'error';
		$res['info']['i'] = $post['i'];

		if ($post && !empty($post['id'])) {
			try {
				$data = ReplenishWallet::find()->where(['block' => ReplenishWallet::STATUS_ACTIVE, 'id' => $post['id']])->one();

				if (!empty($data)) {
					$info = WalletFormatter::getBalance($data);
					if ($info !== false)
					{
						$data->balance = (int)$info;
						$data->save();
						$res['info']['balance'] = $info;
						$res['status'] = 'success';
						return json_encode($res);
					}
				}
			} catch (\Exception $e) {
				$res['info']['error'] = $e->getMessage();
				Log::logAtm('error: Check balance Replenish', $res['info']['error']);
			}

		}
		return json_encode($res);
	}

}
