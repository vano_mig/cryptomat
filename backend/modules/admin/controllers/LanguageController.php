<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Country;
use backend\modules\admin\models\Language;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * LanguageController implements the CRUD actions for Language model.
 */
class LanguageController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Language models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('language.listview');
        $dataProvider = new ActiveDataProvider([
            'query' => Language::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Language model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('language.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Language the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Language::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

    /**
     * Creates a new Language model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->accessRules('language.create');
        $model = new Language();
        if ($model->load(Yii::$app->request->post())) {
            $country = Country::find()->where(['id' => $model->name])->one();
            $model->name = $country->name;
            $model->iso_code = $country->iso_code;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('language.update');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $country = Country::find()->where(['id' => $model->name])->one();
            $model->iso_code = $country->iso_code;
            $model->name = $country->name;
            if ($model->main != ($model->getOldAttribute('main'))) {
                $model->checkMainLang($model->main, $model->id);
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Makes language active
     * @param $id integer
     * @return mixed
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionMain($id)
    {
        $this->accessRules('language.update');
        $model = $this->findModel($id);
        if ($model->main == $model::ACTIVE) {
            $model->main = $model::INACTIVE;
        } else {
            $model->main = $model::ACTIVE;
            $model->status = $model::ACTIVE;
        }
        $model->checkMainLang($model->main, $model->id);
        if ($model->save()) {
            return $this->redirect(Url::toRoute('index'));
        }
    }

    /**
     * Deletes an existing Language model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->accessRules('language.delete');
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));

        return $this->redirect(Url::toRoute('index'));
    }
}
