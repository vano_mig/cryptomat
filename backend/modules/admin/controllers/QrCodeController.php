<?php

namespace backend\modules\admin\controllers;

use backend\modules\admin\models\Transaction;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

/**
 * QrCodeController implements the CRUD actions for QrCode model.
 */
class QrCodeController extends Controller
{
    /**
     * Generate Qr Code
     * @param string $id
     * @throws NotFoundHttpException
     */
    public function actionGetQrCode($id)
    {
        $this->layout = '@backend/views/layouts/qr_code.php';
        $model = Transaction::findOne(['transaction_id'=>$id, 'status'=>[Transaction::STATUS_WAIT, Transaction::STATUS_SUCCESS, Transaction::STATUS_RECEIVE_CRYPTO]]);
        if($model === null) {
            throw new NotFoundHttpException(Yii::t('admin', 'page not found'));
        }
        $renderer = new ImageRenderer(
            new RendererStyle(600),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);
        return $this->render('qr_code', ['writer'=>$writer, 'id'=>$id]);
    }
}
