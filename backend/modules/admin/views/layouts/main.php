<?php

/* @var $this yii\web\View */

/* @var $content string */

use yii\bootstrap4\Html;
use backend\assets\AppAsset;
use backend\modules\admin\widgets\adminLTE\LeftWidget;
use backend\modules\admin\widgets\adminLTE\HeaderWidget;
use backend\modules\admin\widgets\adminLTE\FooterWidget;
use backend\modules\admin\widgets\adminLTE\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= Url::to('/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Url::to('/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Url::to('/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Url::to('/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= Url::to('/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Url::to('/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Url::to('/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= Url::to('/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= Url::to('/favicon/apple-icon-180x180.png') ?>">
    <link rel="apple-touch-icon" sizes="192x192" href="<?= Url::to('/favicon/apple-icon.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Url::to('/favicon/favicon-16x16.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Url::to('/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="36x36" href="<?= Url::to('/favicon/android-icon-36x36.png') ?>">
    <link rel="icon" type="image/png" sizes="48x48" href="<?= Url::to('/favicon/android-icon-48x48.png') ?>">
    <link rel="icon" type="image/png" sizes="70x70" href="<?= Url::to('/favicon/ms-icon-70x70.png') ?>">
    <link rel="icon" type="image/png" sizes="72x72" href="<?= Url::to('/favicon/android-icon-72x72.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= Url::to('/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="144x144" href="<?= Url::to('/favicon/android-icon-144x144.png') ?>">
    <link rel="icon" type="image/png" sizes="150x150" href="<?= Url::to('/favicon/ms-icon-150x150.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= Url::to('/favicon/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="310x310" href="<?= Url::to('/favicon/ms-icon-310x310.png') ?>">
    <link rel="manifest" href="<?= Url::to('/favicon/manifest.json') ?>">
    <meta name="msapplication-TileColor" content="#199019">
    <meta name="msapplication-TileImage" content="<?= Url::to('/favicon/ms-icon-144x144.png') ?>">
    <meta name="theme-color" content="#199019">

</head>
<body class="sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <?php echo LeftWidget::widget(['user' => Yii::$app->user]) ?>
    <?php echo HeaderWidget::widget(['user' => Yii::$app->user]) ?>
    <main class="content-wrapper pl-2">
        <?= Alert::widget() ?>
        <?php echo $content ?>
    </main>
    <?php echo FooterWidget::widget(['user' => Yii::$app->user]) ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
