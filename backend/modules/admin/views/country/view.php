<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Country */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\modules\admin\formatter\CountryFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'View') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'View');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-globe-europe"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?php echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'iso_code',
                                'value' => function ($data) {
                                    return \Yii::$app->formatter->load(CountryFormatter::class)->asLang($data);
                                },
                                'format' => 'raw',
                            ],
                            'code_3',
                            'phone_code',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('country.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'),
                                Url::toRoute(['update', 'id' => $model->id]),
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('country.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                [
                                    'class' => 'btn bg-gradient-danger btn-block',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want
                                to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('country.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>