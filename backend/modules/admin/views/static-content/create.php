<?php

/* @var $languages array|frontend\models\Language[]|backend\modules\admin\models\Language[]|yii\db\ActiveRecord[] */
/* @var $this yii\web\View */

/* @var $model backend\modules\admin\models\StaticContent */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'Create Static Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Static Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-edit"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
                'languages' => $languages,
            ]) ?>
        </div>
    </div>
</div>
