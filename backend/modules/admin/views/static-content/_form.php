<?php

/* @var $languages array|\frontend\models\Language[]|backend\modules\admin\models\Language[]|yii\db\ActiveRecord[] */
/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\StaticContent|yii\db\ActiveRecord */

/*
 * TODO bootstrap
 */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;


$modelContents = $model->staticContentLanguage;
$lang = [];
$i = 1;
$lengAll = [];
foreach ($languages as $language) {
    $lengAll += [$language->iso_code => $language->name];
}
?>

    <div class="static-content-form">
        <?php $form = ActiveForm::begin(); ?>
        <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
        <div class="row nav-tabs-custom ">
            <div class="ml-auto pb-2 pr-2">
                <?= Html::Button(Yii::t('admin', 'Add language'), ['class' => 'btn bg-gradient-success text-nowrap my-create', 'name' => 'Create', 'value' => '1']) ?>
            </div>
            <div class="col-12 card card-my-gray card-tabs elevation-1">
                <div class="card-header p-2 border-0 elevation-inset-2">
                    <ul class="nav nav-tabs border-0" id="custom-tabs-one-tab">
                        <?php if (!$modelContents || (empty($modelContents[1]) && !array_key_exists($modelContents[0]->language, $lengAll))) { ?>
                            <li class="rounded nav-item elevation-2 mx-2 my-1">
                                <a class="rounded nav-link p-1 active"
                                   id="ntab-tab"
                                   data-toggle="pill"
                                   href="#ntab"
                                   role="tab"
                                   aria-controls="ntab"
                                   aria-expanded="true">
                                    <div class="none" style="height: 40px; position: fixed; width: 172px;"></div>
                                    <select class="border form-control"
                                            name="StaticContentLanguage[<?php echo $i ?>][lang]">
                                        <option selected
                                                disabled><?php echo Yii::t('product', 'Select language') ?></option>
                                        <?php foreach ($languages as $language) { ?>
                                            <option value="<?php echo $language->iso_code ?>"><?php echo $language->name ?></option>
                                        <?php } ?>
                                    </select>
                                </a>
                            </li>
                        <?php } ?>
                        <?php $r = 1; ?>
                        <?php foreach ($modelContents as $item) { ?>
                            <?php if (array_key_exists($item->language, $lengAll)) { ?>
                                <li class="rounded nav-item elevation-2 mx-2 my-1">
                                    <a class="h-100 d-inline-flex align-items-center justify-content-center rounded nav-link py-1 px-2<?= $r == 1 ? 'active' : '' ?>"
                                       id="<?php echo '#tab_' . $r ?>-tab"
                                       data-toggle="pill"
                                       href='<?php echo '#tab_' . $r ?>'
                                       role="tab"
                                       aria-controls="<?php echo '#tab_' . $r ?>"
                                       aria-expanded="<?= $r == 1 ? 'true' : 'false' ?>">
                                        <samp class="d-block mt-1 mb-1 pt-1"><?php echo Yii::t('language', $lengAll[$item->language]) ?></samp>
                                        <button type="submit" name="deleteTranslate" value="<?php echo $item->id ?>"
                                                class="btn bg-gradient-danger btn-sm" style="margin-left: 1rem;"
                                                data-pjax="<?php echo Yii::t('yii', 'Delete'); ?>"
                                                data-confirm="<?php echo Yii::t('admin', 'Are you sure you want to delete ' . $lengAll[$item->language] . ' translation?'); ?>">
                                            <i class="fas fa-times"></i></button>
                                    </a>
                                </li>
                                <input type="hidden" name="StaticContentLanguage[<?php echo $r; ?>][lang]"
                                       value="<?php echo $item->language ?>">
                                <?php $lang[] = "$item->language"; ?>
                            <?php } ?>
                            <?php $r++; ?>
                        <?php } ?>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <?php if (!$modelContents || (empty($modelContents[1]) && !array_key_exists($modelContents[0]->language, $lengAll))) { ?>
                            <div class="tab-pane fade active show" id="ntab" role="tabpanel" aria-labelledby="ntab-tab">
                            <textarea class="mceEdit"
                                      name="StaticContentLanguage[<?php echo $i ?>][content]"></textarea>
                            </div>
                        <?php } ?>
                        <?php $r = 1; ?>
                        <?php foreach ($modelContents as $item) { ?>
                            <?php if (array_key_exists($item->language, $lengAll)) { ?>
                        <div class="tab-pane fade <?= $r == 1 ? 'active show' : ''; ?>"
                             id="<?php echo 'tab_' . $r ?>"
                             role="tabpanel"
                             aria-labelledby="<?php echo 'tab_' . $r ?>-tab">
                            <textarea class="mceEdit"
                                      name="<?php echo 'StaticContentLanguage[' . $r . '][content]' ?>"><?php echo $item->content ?></textarea>
                                </div>
                            <?php } ?>
                            <?php $r++ ?>
                            <?php $i = $r; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success', 'name' => 'Save', 'value' => '1']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="d-none">
        <li class="my-li rounded nav-item elevation-2 mx-2 my-1">
            <a class="d-inline-flex rounded nav-link p-1 active"
               data-toggle="pill"
               role="tab"
               aria-expanded="true">
                <div class="none" style="height: 40px; position: fixed; width: 172px;"></div>
                <select class="border form-control" style="cursor: pointer;">
                    <option selected disabled><?php echo Yii::t('product', 'Select language') ?></option>
                    <?php foreach ($languages as $language) {
                        $j = 0;
                        foreach ($lang as $item) {
                            if ($language->iso_code == $item) {
                                $j++;
                                break;
                            }
                        }
                        if ($j == 0) { ?>
                            <option value="<?php echo $language->iso_code ?>"><?php echo $language->name ?></option>
                        <?php }
                    } ?>
                </select>
                <button type="button" class="btn bg-gradient-warning btn-sm my-delete ml-3"><i
                            class="fa fa-times"></i></button>
            </a>
        </li>
        <div class="my-div tab-pane active">
            <textarea class="my-textarea"></textarea>
        </div>
    </div>

<?php
$alert = Yii::t('admin', 'Are you sure you want to delete this?');
$js = "
$(window).on('load', function () {
    tinymce.init({
        mode: 'specific_textareas',
        editor_selector: 'mceEdit',
        height: 500,
        contextmenu: false,
        menubar: false,
        plugins: ['link preview code'],
        toolbar: 'undo redo | removeformat | bold underline italic strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview code',
        branding: false,
    });
    
    var i = $i;
    $(document).on('touchstart click', '.my-create', function(event){
        if (i <= 1) {
            i +=1;
        }
        $('.nav-tabs-custom *').removeClass('active');
        $('.my-div').clone(true).removeClass('my-div').appendTo('.tab-content').attr({'id': 'tab_'+i});
        $('#tab_'+i+' '+'textarea').addClass('newElement'+i).attr({'name':'StaticContentLanguage['+i+'][content]', 'id': 'leng_'+i});
        $('.my-li').clone().removeClass('my-li').appendTo('.nav-tabs').attr({'id': 'li_'+i});
        $('#li_'+i+' '+'a').attr({'href':'#tab_'+i, 'id': 'li_'+i+'-tab', 'aria-controls': 'li_'+i, });
        $('#li_'+i+' '+'a'+' '+'select').attr({'name':'StaticContentLanguage['+i+'][lang]'});
        $('#li_'+i+' '+'a'+' '+'button').attr({'id': i});

        tinymce.init({
            selector: '#leng_'+i,
            height: 500,
            contextmenu: false,
            menubar: false,
            plugins: ['link preview code'],
            toolbar: 'undo redo | removeformat | bold underline italic strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview code',
            branding: false,
        });
        i+=1;
    });
    
    $(document).on('touchstart click', '.my-delete', function(event){
    var confirm1 = confirm('" . $alert . "');
        if (confirm1) {
            var id_r = $(this).attr('id');
            if  ($('#li_'+id_r).attr('class') == 'active') {
                $('.nav-tabs li:first').addClass('active');
                $('.tab-content div:first').addClass('active');
            }
            $('#li_'+id_r).remove();
            $('#tab_'+id_r).remove();
//            re_id +=1;
        }
    });
});    
";

$this->registerJs($js, yii\web\View::POS_END);

$css = '
ul  a.active .none {
display: none;
}
';

$this->registerCss($css);

?>