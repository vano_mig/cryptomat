<?php

/* @var $languages array|frontend\models\Language[]|backend\modules\admin\models\Language[]|yii\db\ActiveRecord[] */
/* @var $this yii\web\View */

/* @var $model backend\modules\admin\models\StaticContent */

/*
 * TODO bootstrap
 */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('admin', 'Static Contents') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Static Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'View');

$lengAll = [];
foreach ($languages as $language) {
    $lengAll += [$language->iso_code => $language->name];
}
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-edit"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="col-12 d-inline-flex align-items-center rounded border text-lg">
                    <p class="m-0 p-0"> <?= Yii::t('Static Content', 'Embed code:') ?></p>
                    <code class="ml-3">StaticContent::Content('<?php echo $model->id ?>'); </code>
                </div>
                <div class="col-12 d-inline-flex mt-1 text-md">
                <p><?= Yii::t('Static Content', 'Translations:') ?></p>
                <?php $i = 0; ?>
                    <div class="ml-3">
                <?php foreach ($model->staticContentLanguage as $item) { ?>
                    <?php if (array_key_exists($item->language, $lengAll)) { ?>
                        <?php if ($i == 0) { ?>
                            <p><?php echo Yii::t('language', $lengAll[$item->language]) ?></p>
                        <?php } else { ?>
                            <p><?php echo Yii::t('language', $lengAll[$item->language]) ?></p>
                        <?php } ?>
                        <?php $i = 1; ?>
                    <?php } ?>
                <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (\Yii::$app->user->can('static_content.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (\Yii::$app->user->can('static_content.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (\Yii::$app->user->can('static_content.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>