<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Language|yii\db\ActiveRecord */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use backend\modules\admin\models\Country;

?>


<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col min-wr-16 max-wr-30 mx-auto">
        <?php echo $form->field($model, 'name')->dropDownList((new Country())->getList(),
            ['prompt' => Yii::t('language', 'Select name')]) ?>

        <?php echo $form->field($model, 'main')->dropDownList($model->getStatuses(),
            ['prompt' => Yii::t('language', 'Select main')]) ?>

        <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(),
            ['prompt' => Yii::t('language', 'Select status')]) ?>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']); ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

