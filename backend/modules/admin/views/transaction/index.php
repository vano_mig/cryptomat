<?php

/* @var $search backend\modules\admin\models\Transaction */
/* @var $pages yii\data\Pagination */
/* @var $this yii\web\View */
/* @var $model array|backend\modules\admin\models\Transaction[] */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\modules\admin\formatter\TransactionFormatter;
use backend\modules\admin\models\CryptoCurrency;
use backend\modules\virtual_terminal\models\Atm;
use yii\bootstrap4\Html;
use backend\modules\user\models\User;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-align-left"></i> <?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php if (!empty($dataProvider->getModels())) : ?>
<!--                <div class="row col-12 mt-2">-->
<!--                    <div class="row ml-auto d-flex justify-content-end">-->
<!--                    --><?php //echo Html::a(Yii::t('admin', 'Export XLS'), Url::toRoute(['/admin/transaction/export', 'Transaction' => $searchModel->attributes, 'Transaction[dateFrom]' => $searchModel->dateFrom, 'Transaction[dateTo]' => $searchModel->dateTo, 'Transaction[format]' => 'xls']), ['class' => 'btn bg-gradient-info text-nowrap mx-1 mb-2', 'target'=>'_blank']) ?>
<!--                    --><?php //echo Html::a(Yii::t('admin', 'Export CSV'), Url::toRoute(['/admin/transaction/export', 'Transaction' => $searchModel->attributes, 'Transaction[dateFrom]' => $searchModel->dateFrom, 'Transaction[dateTo]' => $searchModel->dateTo, 'Transaction[format]' => 'csv']), ['class' => 'btn bg-gradient-info text-nowrap mx-1 mb-2', 'target'=>'_blank']) ?>
<!--                </div>-->
                </div>
                <div class="table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'terminal_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTerminal($data->terminal_id);
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'terminal_id',
                                    (new Atm())->getList(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                            ],
//                            [
//                                'attribute' => 'widget_id',
//                                'value' => function ($data) {
//                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asWidget($data->widget_id);
//                                },
//                                'filter' => Html::activeDropDownList($searchModel, 'widget_id',
//                                    (new Widget())->getList(),
//                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
//                                'format' => 'raw',
//                                'visible' => Yii::$app->user->can(\backend\models\User::ROLE_ADMIN) || Widget::findOne(['company_id' => Yii::$app->user->identity->company_id]) != null,
//                            ],
                            [
                                'attribute' => 'type',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asType($data);
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'type',
                                    $searchModel->getType(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                            ],
                            'amount_receive',
                            [
                                'attribute' => 'currency_receive',
                                'filter' => Html::activeDropDownList($searchModel, 'currency_receive',
                                    (new CryptoCurrency())->getListCodeFull(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                            ],
                            'amount_sent',
                            [
                                'attribute' => 'currency_sent',
                                'filter' => Html::activeDropDownList($searchModel, 'currency_sent',
                                    (new CryptoCurrency())->getListCodeFull(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                                'visible' => Yii::$app->user->can(User::ROLE_ADMIN) || Widget::findOne(['company_id' => Yii::$app->user->identity->company_id]) != null,
                            ],
                            'transaction_id',
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asStatus($data);
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'status',
                                    $searchModel->getStatus(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'transaction_begin',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTransaction($data->transaction_begin);
                                },
                                'format' => 'raw',
                            ],
                            'updated_at',
                            ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                                'buttons' => [
                                    'view' => function ($url, $models, $key) {
                                        return Html::a('<i class="far fa-eye"></i>', $url, [
                                            'class' => 'mx-2 text-info',
                                            'title' => Yii::t('admin', 'View'),
                                            'aria-label' => Yii::t('admin', 'View'),
                                            'data-pjax' => Yii::t('admin', 'View'),
                                        ]);
                                    },
                                ],
                                'visibleButtons' => [
                                    'view' => function ($models, $key, $index) {
                                        return Yii::$app->user->can('transaction.view', ['user' => $models]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>

            <?php else : ?>

                <h3 class="mt-2" style="font-size: 1.8rem; text-align: center;"><?php echo '«' . Yii::t('transactions', 'No transactions') . '»'; ?></h3>

            <?php endif ?>
        </div>
    </div>
</div>
