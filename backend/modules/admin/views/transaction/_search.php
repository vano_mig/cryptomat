<?php

use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'action' => ['index'],
    'method' => 'get',
]); ?>
<div class="row col-12">
    <div class="col min-wr-16 max-wr-30 mx-auto">

        <?php echo $form->field($model, 'dateFrom')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'options' => ['placeholder' => Yii::t('order', 'choose start time of execution term') . ":"]
        ])->label(Yii::t('admin', 'Date from') . ":"); ?>
        <?php echo $form->field($model, 'dateTo')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ],
            'options' => ['placeholder' => Yii::t('order', 'choose end time of execution term') . ":"]
        ])->label(Yii::t('admin', 'Date to') . ":"); ?>

    </div>
    <div class="col min-wr-16 max-wr-30 mx-auto">
        <?php //echo $form->field($model, 'amount')->textInput(['placeholder' => Yii::t('transaction', 'Input amount')]) ?>

        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?php echo $form->field($model, 'type')->dropDownList($model::getType(), ['prompt' => Yii::t('transaction', 'Select type')]); ?>
        <?php endif ?>
        <?php if (Yii::$app->user->identity->role != User::ROLE_USER): ?>
            <?php //echo $form->field($model, 'ref_card_no')->textInput(['placeholder' => Yii::t('transaction', 'Input reference card number')]) ?>
        <?php endif ?>
    </div>
    <div class="col-12 mt-2 pb-3 mb-3 pt-4 border-bottom">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Accept'), ['class' => 'btn bg-gradient-info btn-block']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
