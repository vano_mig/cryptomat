<?php

use yii\bootstrap4\Html;
use yii\db\Query;

/**
 * @var $model Query
 *
 */
/* @var $this \yii\web\View */
?>

<div style="width: 170px;">
    <?php echo Html::img(Yii::$app->basePath.'/web/img/invoice/ms_logo.png', ['style' => 'width:150px; align:left;']); ?>
</div>
<br><br>

<div>Transaction history</div>
<table class="grey-border-table whole-size" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
    <thead>
    <tr >
        <th class="text-align-center">
            <?php echo Yii::t('admin', 'ID'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Transaction'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Ref Card No'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Amount'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Currency'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Response Code'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('transaction', 'Receipt'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('admin', 'Company'); ?>
        </th>
        <th class="text-align-center">
            <?php echo Yii::t('admin', 'Created'); ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->each() as $item): ?>
        <tr>
            <td>
                <?php echo $item['id'];?>
            </td>
            <td>
                <?php echo $item['res_file_ref'];?>
            </td>
            <td>
                <?php echo $item['ref_card_no'];?>
            </td>
            <td>
                <?php echo $item['amount'];?>
            </td>
            <td>
                <?php echo $item['currency'];?>
            </td>
            <td>
                <?php echo $item['response_code'];?>
            </td>
            <td>
                <?php echo $item['receipt'];?>
            </td>
            <td>
                <?php echo $item['name'];?>
            </td>
            <td>
                <?php echo $item['created_at'];?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>