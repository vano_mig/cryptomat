<?php

use backend\models\User;
use backend\modules\admin\formatter\TransactionFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Transaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => $this->params['breadcrumbs'] ?? [],
        'homeLink' => ['label' => Yii::t('admin', 'Statistics')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-inbox"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto col-8">
                <div class="table-responsive">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'terminal_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTerminal($data->terminal_id);
                                },
                                'format' => 'raw',
                            ],
//                            [
//                                'attribute' => 'widget_id',
//                                'value' => function ($data) {
//                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asWidget($data->widget_id);
//                                },
//                                'format' => 'raw',
//                            ],
                            [
                                'attribute' => 'type',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asType($data);
                                },
                                'format' => 'raw',
                            ],
                            'amount_receive',
                            'currency_receive',
                            'amount_sent',
                            'currency_sent',
                            'phone_number',
                            'transaction_id',
                            'address',
                            'address_coin',
                            [
                                'attribute' => 'transaction_begin',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asTransaction($data->transaction_begin);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(TransactionFormatter::class)->asStatus($data);
                                },
                                'format' => 'raw',
                            ],
                            'api_id',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>

                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">

                    <?php if (Yii::$app->user->can('transaction.listview')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('api', 'List'), ['index', 'id' => $model->id], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>

