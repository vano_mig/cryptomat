<?php

use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\CryptoCurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Crypto Currencies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-coins"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('crypto_currency.create')): ?>
                <?php echo Html::a(Yii::t('virtual_terminal', 'Add currencies'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'code',
			[
				'attribute' => 'active',
				'value' => function ($data) {
					return CurrencyFormatter::cryptoCurrencyGetStatus($data->active);
				},
				'format' => 'html',
			],

            [
                'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                'header' => Yii::t('admin', 'Actions'),
//                                'template' => '{answer} {view} {update} {delete}',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $models, $key) {
                        return Html::a('<i class="far fa-eye"></i>', $url, [
                            'class' => 'mx-2 text-info',
                            'title' => Yii::t('admin', 'View'),
                            'aria-label' => Yii::t('admin', 'View'),
                            'data-pjax' => Yii::t('admin', 'View'),
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                            'class' => 'mx-2 text-warning',
                            'title' => Yii::t('admin', 'Update'),
                            'aria-label' => Yii::t('admin', 'Update'),
                            'data-pjax' => Yii::t('admin', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                            'class' => 'mx-2 text-dark',
                            'title' => Yii::t('admin', 'Delete'),
                            'aria-label' => Yii::t('admin', 'Delete'),
                            'data-pjax' => Yii::t('admin', 'Delete'),
                            'data' => [
                                'confirm' => Yii::t('admin',
                                    'Are you sure you want to delete this item?'),
                                'method' => 'POST'
                            ],
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'view' => function ($models, $key, $index) {
                        return \Yii::$app->user->can('crypto_currency.view', ['user' => $models]);
                    },
                    'update' => function ($model, $key, $index) {
                        return \Yii::$app->user->can('crypto_currency.update', ['user' => $model]);
                    },
                    'delete' => function ($model, $key, $index) {
                        return \Yii::$app->user->can('crypto_currency.delete', ['user' => $model]);
                    },
                ],
            ],
        ],
    ]); ?>


            </div>
        </div>
    </div>
</div>
