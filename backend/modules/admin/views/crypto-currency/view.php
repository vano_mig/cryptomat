<?php

use backend\modules\admin\formatter\CryptoCurrencyFormatterOld;
use backend\modules\admin\formatter\CurrencyFormatter;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\CryptoCurrency */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Crypto Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('terminal', 'Terminal')],
    ]) ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-coins"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'full_name',
                        'code',
                        'precision',
                        'icon',
                        'min_price',
                        'address_regex',
						[
							'attribute' => 'active',
							'value' => function ($data) {
								return CurrencyFormatter::cryptoCurrencyGetStatus($data->active);
							},
							'format' => 'html',
						],
                    ],
                ]) ?>

                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">

                    <?php if (Yii::$app->user->can('crypto_currency.update')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('crypto_currency.delete')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block ',
                                'data' => [
                                    'confirm' => Yii::t('virtual_terminal', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>

                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('crypto_currency.list')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('virtual_terminal', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>
