<?php

?>

<footer class="main-footer">
    <div class="container">
        <p class="m-0 footer-center">
            &copy;<?= ' ' . '2019' . ' - ' . date('Y') . ', «' . Yii::t('site', 'CRYPTOMATICATM') . '»' ?>
        </p>
    </div>
</footer>
