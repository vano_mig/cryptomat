<?php

use yii\bootstrap4\Html;
use backend\modules\admin\models\Language;
use backend\modules\admin\formatter\LanguageFormatter;

$languages = Language::findAll(['status' => Language::ACTIVE]);
$liLanguages = [];
if ($languages) {
    foreach ($languages as $language) {
        if ($language->iso_code == Yii::$app->language) {
            $lanName = $language;
        }
    }
}
?>

<nav class="main-header navbar navbar-expand navbar-white navbar-dark my-navbar-purple border-0">
    <ul class="navbar-nav mr-auto my-hamburger-nav">
        <li class="nav-item">
            <a class="nav-link my-hamburger" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="mr-0 mr-md-2  mb-md-0 my-lang-li-nav dropdown nav-item">
            <a class="btn my-btn-outline-light my-btn-outline-light-admin dropdown-toggle my-leng-nav nav-link " href="#" data-toggle="dropdown"
               aria-expanded="false"><?php echo Yii::$app->formatter->load(LanguageFormatter::class)->asLangFlag($lanName).  Yii::t('language', Language::find()->where(['iso_code' => Yii::$app->language])->one()->name) ?></a>
            <?php if (count($languages) > 1): ?>
                <div class="dropdown-menu">
                    <?php foreach ($languages as $language): ?>
                        <?php if ($language->iso_code != Yii::$app->language): ?>
                            <a class="dropdown-item"
                               href="/<?php echo $language->iso_code . Yii::$app->getRequest()->getLangUrl() ?>"><?php echo Yii::$app->formatter->load(LanguageFormatter::class)->asLangFlag($language).  Yii::t('language', $language->name) ?></a>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
        </li>
        <li class="nav-item ml-1">
            <?php echo Html::a(Html::encode(Yii::t('admin', 'Profile')), ['/user/profile'], ['class' => 'btn btn-outline-light']) ?>
        </li>
        <li class="nav-item ml-1">
            <?php
            echo Html::beginForm(['/site/logout'], 'post');
            echo Html::submitButton(
                ' <i class="fas fa-sign-out-alt"></i>',
                ['class' => 'btn btn-outline-light', 'title' => Yii::t('admin', 'Logout')]
            );
            echo Html::endForm();
            ?>
        </li>
    </ul>
</nav>
