<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use backend\modules\user\models\User;

$leftSide = [
    'System' => [
        'name' => Yii::t('admin', 'System'),
        'icon' => 'fas fa-cog nav-icon',
        'active' => true,
        'accordion' => [
            'Users' => [
                'permission' => Yii::$app->user->can('user.listview'),
                'name' => Yii::t('admin', 'Users'),
                'icon' => 'fas fa-user nav-icon',
                'url' => '/user/user',
                'active' => true,
            ],
            'Language' => [
                'permission' => Yii::$app->user->can('language.listview'),
                'name' => Yii::t('admin', 'Language'),
                'icon' => 'fas fa-map-marker-alt nav-icon',
                'url' => '/admin/language',
                'active' => true,
            ],
            'Country' => [
                'permission' => Yii::$app->user->can('country.listview'),
                'name' => Yii::t('admin', 'Country'),
                'icon' => 'fas fa-globe-europe nav-icon',
                'url' => '/admin/country',
                'active' => true,
            ],
            'Currency' => [
                'permission' => Yii::$app->user->can('currency.listview'),
                'name' => Yii::t('admin', 'Currency'),
                'icon' => 'fas fa-euro-sign nav-icon',
                'url' => '/admin/currency',
                'active' => true,
            ],
            'Crypto Currency' => [
                'permission' => Yii::$app->user->can('crypto_currency.list'),
                'name' => Yii::t('admin', 'Crypto Currency'),
                'icon' => 'fas fa-coins nav-icon',
                'url' => '/admin/crypto-currency',
                'active' => true,
            ],
            'Translation' => [
                'permission' => Yii::$app->user->can('translation.listview'),
                'name' => Yii::t('admin', 'Translation'),
                'icon' => 'fas fa-language nav-icon',
                'url' => '/admin/translation',
                'active' => true,
            ],
            'Apis' => [
                'permission' => Yii::$app->user->can('api.listview'),
                'name' => Yii::t('admin', 'Apis'),
                'icon' => 'fas fa-network-wired nav-icon',
                'url' => '/admin/api-name',
                'active' => true,
            ],
            'Api Currency' => [
                'permission' => Yii::$app->user->can('api.listview'),
                'name' => Yii::t('admin', 'Api Currency'),
                'icon' => 'fab fa-gg nav-icon',
                'url' => '/admin/api-name-currency',
                'active' => true,
            ],
            'Api Crypto Currency' => [
                'permission' => Yii::$app->user->can('api.listview'),
                'name' => Yii::t('admin', 'Api Crypto Currency'),
                'icon' => 'fab fa-btc nav-icon',
                'url' => '/admin/api-name-crypto-currency',
                'active' => true,
            ],
            'Rules' => [
                'permission' => Yii::$app->user->can(User::ROLE_ADMIN),
                'name' => Yii::t('admin', 'Rules'),
                'icon' => 'fas fa-clipboard-check nav-icon',
                'url' => '/user/permissions',
                'active' => true,
            ]
        ]
    ],
    'KYC' => [
        'permission' => Yii::$app->user->can('kyc.list'),
        'name' => Yii::t('admin', 'KYC'),
        'icon' => 'far fa-list-alt nav-icon',
        'url' => '/user/kyc',
        'active' => true,
        'accordion' => []
    ],
    'WHITE_LIST' => [
        'permission' => Yii::$app->user->can('atm.update'),
        'name' => Yii::t('admin', 'White list'),
        'icon' => 'fas fa-lock-open nav-icon',
        'url' => '/user/white-list',
        'active' => true,
        'accordion' => []
    ],
    'Administration' => [
        'name' => Yii::t('admin', 'Administration'),
        'icon' => 'fas fa-clipboard-check nav-icon',
        'active' => true,
        'accordion' => [
            'Company list' => [
                'permission' => Yii::$app->user->can('company.listview'),
                'name' => Yii::t('admin', 'Company list'),
                'icon' => 'fas fa-building nav-icon',
                'url' => '/user/company',
                'active' => true,
            ],
            'Static Content' => [
                'permission' => Yii::$app->user->can('static_content.listview'),
                'name' => Yii::t('admin', 'Static Content'),
                'icon' => 'fas fa-edit nav-icon',
                'url' => '/admin/static-content',
                'active' => true,
            ],
        ]
    ],
    'Terminals' => [
        'name' => Yii::t('admin', 'Terminals'),
        'icon' => 'fas fa-laptop nav-icon',
        'active' => true,
        'accordion' => [
            'VTM' => [
                'permission' => Yii::$app->user->can('virtual_terminal.list'),
                'name' => Yii::t('admin', 'VTM'),
                'icon' => 'fab fa-vuejs nav-icon',
                'url' => '/terminal/virtual-terminal',
                'active' => true,
            ],
            'ATM' => [
                'permission' => Yii::$app->user->can('atm.list'),
                'name' => Yii::t('admin', 'ATM'),
                'icon' => 'fas fa-list nav-icon',
                'url' => '/terminal/atm',
                'active' => true,
            ],
            'ATM Groups' => [
                'permission' => Yii::$app->user->can('atm_groups.list'),
                'name' => Yii::t('admin', 'ATM Groups'),
                'icon' => 'fas fa-layer-group nav-icon',
                'url' => '/terminal/atm-groups',
                'active' => true,
            ],
            'ATM Profiles' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'ATM Profiles'),
                'icon' => 'fas fa-passport nav-icon',
                'url' => '/terminal/identification-profile',
                'active' => true,
            ],
        ]
    ],
    'Exchange/Wallets' => [
        'name' => Yii::t('admin', 'Exchange/Wallets'),
        'icon' => 'fas fa-chart-bar nav-icon',
        'active' => true,
        'accordion' => [
            'Replenish' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Replenish wallets'),
                'icon' => 'fas fa-wallet nav-icon',
                'url' => '/admin/replenish-wallet',
                'active' => true,
            ],
            'Exchange' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Exchange wallets'),
                'icon' => 'fas fa-exchange-alt nav-icon',
                'url' => '/admin/exchange-wallet',
                'active' => true,
            ],
        ]
    ],
    'ThirdPartyApi' => [
        'permission' => Yii::$app->user->can('3rd_party_api.listview'),
        'name' => Yii::t('admin', '3rd party APIs'),
        'icon' => 'fab fa-servicestack nav-icon',
        'url' => '/admin/third-party-api',
        'active' => true,
        'accordion' => []
    ],

    'Wallet' => [
        'name' => Yii::t('admin', 'Wallet'),
        'icon' => 'fas fa-chart-bar nav-icon',
        'active' => true,
        'accordion' => [
            'Template' => [
                'permission' => Yii::$app->user->can('pkpass_template.listview'),
                'name' => Yii::t('admin', 'PASS TEMPLATE'),
                'icon' => 'fas fa-star-half-alt nav-icon',
                'url' => '/pkpass/pkpass-template',
                'active' => true,
            ],
            'Pass' => [
                'permission' => Yii::$app->user->can('pkpass_push.listview'),
                'name' => Yii::t('admin', 'PASS'),
                'icon' => 'fas fa-qrcode nav-icon',
                'url' => '/pkpass/pkpass-pass',
                'active' => true,
            ],
            'Active Pass' => [
                'permission' => Yii::$app->user->can('pkpass_push.listview'),
                'name' => Yii::t('admin', 'ACTIVE PASS'),
                'icon' => 'fas fa-mobile-alt nav-icon',
                'url' => '/pkpass/pkpass-device',
                'active' => true,
            ],
        ]
    ],
    'Digital' => [
        'name' => Yii::t('admin', 'Digital signale'),
        'icon' => 'fas fa-photo-video nav-icon nav-icon',
        'active' => true,
        'accordion' => [
            'Digital signale' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Digital signale kiosks'),
                'icon' => 'fab fa-hubspot nav-icon',
                'url' => '/advert/advert',
                'active' => true,
            ],
        ]
    ],
    'Marketing' => [
        'name' => Yii::t('admin', 'Marketing'),
        'icon' => 'fas fa-lightbulb nav-icon',
        'active' => true,
        'accordion' => [
            'Video' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Video tracks'),
                'icon' => 'fas fa-ad nav-icon',
                'url' => '/marketing/advertisement',
                'active' => true,
            ],
            'SMS' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'SMS Service'),
                'icon' => 'fas fa-sms nav-icon',
                'url' => '/marketing/sms',
                'active' => true,
            ],
            'Address' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Address list'),
                'icon' => 'fas fa-address-book nav-icon',
                'url' => '/marketing/address-list',
                'active' => true,
            ],
            'Email' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Email Service'),
                'icon' => 'fas fa-envelope-open-text nav-icon',
                'url' => '/marketing/email',
                'active' => true,
            ],
            'Template' => [
                'permission' => Yii::$app->user->can('identification_profile.list'),
                'name' => Yii::t('admin', 'Template list'),
                'icon' => 'fas fa-envelope-square nav-icon',
                'url' => '/marketing/template-email',
                'active' => true,
            ],
        ]
    ],
    'Statistics' => [
        'name' => Yii::t('admin', 'Statistics'),
        'icon' => 'fas fa-chart-bar nav-icon',
        'active' => true,
        'accordion' => [
            'Transactions' => [
                'permission' => Yii::$app->user->can('transaction.listview'),
                'name' => Yii::t('admin', 'Transactions'),
                'icon' => 'fas fa-align-left nav-icon',
                'url' => '/admin/transaction',
                'active' => true,
            ],
            'Profit' => [
                'permission' => Yii::$app->user->can('transaction.listview'),
                'name' => Yii::t('admin', 'Profit'),
                'icon' => 'fas fa-file-invoice-dollar nav-icon',
                'url' => '/user/company-profit',
                'active' => true,
            ],
        ]
    ],
    'Support' => [
        'permission' => Yii::$app->user->can('support.listview'),
        'name' => Yii::t('admin', 'Support'),
        'icon' => 'fas fa-file-import nav-icon',
        'url' => '/admin/support',
        'active' => true,
        'accordion' => []
    ],
]

?>

<aside class="main-sidebar sidebar-dark-primary">

    <a href="<?php echo Url::toRoute('/admin/default'); ?>" class="brand-link d-flex justify-content-center border-bottom-0">

        <span class="logo-mini logo-xs"><b><?php echo Yii::t('sidebarLogo', 'ATM') ?></b></span>

        <span class="logo-lg brand-text"><?php echo Yii::t('sidebarLogo', 'CRYPTOMATIC') ?></span>

    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-1">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat text-sm"
                data-widget="treeview"
                role="menu" data-accordion="false">

                <?php

                foreach ($leftSide as $key => $value)
                {
                    try {
                        if ($value['active'] && !empty($value['accordion']))
                        {
                            $isHidden = true;
                            $preg = "#(";
                            foreach ($value['accordion'] as $aKey => $aValue)
                            {
                                if ($aValue['permission'])
                                {
                                    if ($isHidden === false)
                                        $preg .= '|';
                                    $preg .= "{$aValue['url']}(?:(?:\?|\/)|\b$)";
                                    $isHidden = false;
                                }
                            }
//
                            $preg .= ")#";
                            if ($isHidden === false) {
                                echo '<li class="nav-item has-treeview ' . (preg_match($preg, $_SERVER['REQUEST_URI']) ? "menu-open" : "") .'">';
                                echo '<a href="#" class="'. (preg_match($preg, $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link') .'">
                                        <i class="' . $value['icon'] .'"></i>
                                        <p style="  display: inline-flex;
                                                    align-items: center;
                                                    justify-content: space-between;
                                                    width: 80%;">
                                            <span>' . $value['name'] . '</span>
                                            <i class="right fas fa-angle-left" style="top: inherit; right: 1rem"></i>
                                        </p>
                                    </a>';
                                echo '<ul class="nav nav-treeview">';
                                foreach ($value['accordion'] as $aKey => $aValue)
                                {
                                    if ($aValue['active'] && $aValue['permission']) {
                                        echo '<li class="nav-item">
                                        ' . Html::a(
                                                '<i class="'. $aValue['icon'] .'"></i><p>' . $aValue['name'] . '</p>',
                                                Url::toRoute([$aValue['url']]), ['class' => preg_match("#{$aValue['url']}(?:(?:\?|\/)|\b$)#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                                            ) .'
                                    </li>';
                                    }
                                }
                                echo '</ul>';
                                echo '</li>';
                            }
                        } else {
                            if ($value['active'] && $value['permission'])
                            {
                                echo '<li class="nav-item">';
                                echo Html::a(
                                    '<i class="'. $value['icon'] .'"></i><p>' . $value['name'] . '</p>',
                                    Url::toRoute([$value['url']]), ['class' => preg_match("#{$value['url']}(?:(?:!\?|/)$|\b)#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                                );
                                echo '</li>';
                            }
                        }
                    } catch (Exception $e)
                    {
                        throw new yii\web\HttpException('500', $e->getMessage() . '; Line: "' . $e->getLine() . '"; File: "' . $e->getFile(). '"');
                    }
                }
                ?>
            </ul>
            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>