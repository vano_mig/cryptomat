<?php

use yii\helpers\Url;
use yii\bootstrap4\Html;
use backend\modules\user\models\User;

?>
<aside class="main-sidebar sidebar-dark-primary sidebar-dark-success elevation-2">
    <a href="<?php echo Url::toRoute('/admin/default'); ?>" class="brand-link logo-switch my-brand-link">
        <span class="brand-text logo-xs"><?php echo Yii::t('sidebarLogo', 'ATM') ?></span>
        <span class="brand-text logo-xl"><?php echo Yii::t('sidebarLogo', 'CRYPTOMATIC') ?></span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat text-sm"
                data-widget="treeview"
                role="menu" data-accordion="false">
                <li class="nav-item">
                    <?php echo Html::a(
                        '<i class="far fa-user-circle nav-icon"></i><p>' . ucfirst(Yii::$app->user->identity->username) . ' ' . ucfirst(Yii::$app->user->identity->last_name) . '</p>',
                        Url::toRoute(['/user/profile']), ['class' => preg_match("#/user/profile\b$#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                    ) ?>
                </li>
                <?php if (Yii::$app->user->can('statistic.listview') || Yii::$app->user->can('transaction.listview')) : ?>
                    <li class="nav-item has-treeview <?= preg_match("#/admin/transaction\b#", $_SERVER['REQUEST_URI']) ? 'menu-open' : '' ?>">
                        <a class="<?= preg_match("#/admin/transaction\b#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link' ?>" href="#">
                            <i class="fas fa-chart-bar nav-icon"></i>
                            <p>
                                <?php echo Yii::t('admin', 'Statistics'); ?>
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <?php if (Yii::$app->user->can('transaction.listview')): ?>
                                <li class="nav-item">
                                    <?php echo Html::a(
                                        '<i class="fas fa-align-left nav-icon"></i><p>' . Yii::t('admin', 'Transactions') . '</p>',
                                        Url::toRoute(['/admin/transaction']), ['class' => preg_match("#/admin/transaction\b#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                                    ) ?>
                                </li>
                            <?php endif ?>
                        </ul>
                    </li>
                <?php endif ?>
                <?php if (Yii::$app->user->can('support.listview')): ?>
                    <li class="nav-item">
                        <?php echo Html::a(
                            '<i class="fas fa-file-import nav-icon"></i><p>' . Yii::t('admin', 'Support') . '</p>',
                            Url::toRoute(['/admin/support']), ['class' => preg_match("#/admin/support\b#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                        ) ?>
                    </li>
                <?php endif ?>
            </ul>
        </nav>
    </div>
</aside>