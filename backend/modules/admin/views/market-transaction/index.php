<?php

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\MarketTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Market Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
	<?=     Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    'homeLink' => ['label' => Yii::t('gii', 'NAME')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-import"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
							<?= Html::a(Yii::t('admin', 'Create Market Transaction'), ['create'], ['class' => 'btn btn-success']) ?>
			            <div class="table-responsive">

													    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
				
									<?= GridView::widget([
                    'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
        'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

					            'id',
            'price',
            'amount',
            'transaction_id',
            'type',
            //'market_id',
            //'datetime',
                    ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $models, $key) {
                                return Html::a('<i class="far fa-eye"></i>', $url, [
                                    'class' => 'mx-2 text-info',
                                    'title' => Yii::t('admin', 'View'),
                                    'aria-label' => Yii::t('admin', 'View'),
                                    'data-pjax' => Yii::t('admin', 'View'),
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                    'class' => 'mx-2 text-warning',
                                    'title' => Yii::t('admin', 'Update'),
                                    'aria-label' => Yii::t('admin', 'Update'),
                                    'data-pjax' => Yii::t('admin', 'Update'),
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                    'class' => 'mx-2 text-dark',
                                    'title' => Yii::t('admin', 'Delete'),
                                    'aria-label' => Yii::t('admin', 'Delete'),
                                    'data-pjax' => Yii::t('admin', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('admin',
                                        'Are you sure you want to delete this item?'),
                                        'method' => 'POST'
                                    ],
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'view' => function ($models, $key, $index) {
                                return \Yii::$app->user->can('gii.view', ['user' => $models]);
                            },
                            'update' => function ($model, $key, $index) {
                                return \Yii::$app->user->can('gii.update', ['user' => $model]);
                            },
                            'delete' => function ($model, $key, $index) {
                                return \Yii::$app->user->can('gii.delete', ['user' => $model]);
                            },
                        ],
                    ],
                ],
                ]); ?>
				
				
            </div>
        </div>
    </div>
</div>