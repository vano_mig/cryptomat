<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\MarketTransaction */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<div class="col mx-auto max-wr-40">

        <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_id')->textInput() ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'market_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'datetime')->textInput() ?>


</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

		<?php  if (Yii::$app->user->can('gii.create') || Yii::$app->user->can('gii.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>

		<?php  endif; ?>

		<?php  if (Yii::$app->user->can('gii.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>

		<?php  endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
