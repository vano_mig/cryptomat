<?php

//use backend\modules\admin\formatter\ApiCryptoCurrencyFormatter;
use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ReplenishWallet */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'enableClientValidation' => false]); ?>

<div class="col mx-auto max-wr-40">

    <?= $form->field($model, 'provider_name')->textInput(['maxlength' => true]) ?>

	<?php echo $form->field($model, 'provider_id')->dropDownList(ApiFormatter::apiCryptoCurrencyGetAllPair(), ['prompt' => Yii::t('admin', 'Select provider')]); ?>
	<?php echo $form->field($model, 'exchange_wallet')->dropDownList(\backend\modules\admin\formatter\WalletFormatter::exchangeGetAll(), ['prompt' => Yii::t('admin', 'Select provider')]); ?>

	<?php
	echo $form->field($model, 'company_id')->dropDownList(Company::getList(),
		[
			'hidden' => !Yii::$app->user->can(User::ROLE_ADMIN),
			'value' => Yii::$app->user->can(User::ROLE_ADMIN) ? (!$model->isNewRecord) ? $model->company_id : null : Yii::$app->user->identity->company_id,
			'prompt' => Yii::t('admin', 'Select company')
		])->label(Yii::$app->user->can(User::ROLE_ADMIN))
	?>

	<?= $form->field($model, 'api_key')->textInput(['maxlength' => true]) ?>

	<?php
    if (!$model->isNewRecord) {
        $len = strlen($model->api_secret);
        $val = preg_replace('/./i', '*', $model->api_secret, $len - 4 > 0 ? $len - 4 : $len);
	}
	?>
	<?= $form->field($model, 'api_secret')->textInput(['maxlength' => true, 'value' => "", 'placeholder' => (!$model->isNewRecord) ? $val : ""]) ?>

	<?= $form->field($model, 'client_id')->textInput(['maxlength' => true]) ?>

	<?php echo $form->field($model, 'block')->dropDownList($model::listStatuses(), ['prompt' => Yii::t('admin', 'Select status')]); ?>


</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

		<?php  if (Yii::$app->user->can('identification_profile.create')): ?>

<!--            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">-->
<!---->
<!--				--><?php //echo Html::submitButton(Yii::t('admin', 'Save & Test'), ['class' => 'btn bg-gradient-dark btn-block', 'value' => 'test', 'name' => 'submitForm']) ?>
<!--            </div>-->

		<?php  endif; ?>

		<?php  if (Yii::$app->user->can('identification_profile.create') || Yii::$app->user->can('identification_profile.update')): ?>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block', 'name' => 'submitForm', 'value' => 'save']) ?>
            </div>

		<?php  endif; ?>

		<?php  if (Yii::$app->user->can('identification_profile.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>

		<?php  endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
