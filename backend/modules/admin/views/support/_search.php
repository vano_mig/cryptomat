<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\SupportSearch */
/* @var $form ActiveForm */
?>
<div class="search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'ticket_id') ?>
    <?php echo $form->field($model, 'client_id') ?>
    <?php echo $form->field($model, 'support_id') ?>
    <?php echo $form->field($model, 'text') ?>
    <?php echo $form->field($model, 'created_at') ?>
    <?php echo $form->field($model, 'updated_at') ?>
    <?php echo $form->field($model, 'client_name') ?>
    <?php echo $form->field($model, 'title') ?>
    <?php echo $form->field($model, 'email') ?>
    <?php echo $form->field($model, 'category') ?>
    <?php echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Submit', ['class' => 'btn bg-gradient-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
