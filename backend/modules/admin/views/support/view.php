<?php

/* @var $this yii\web\View */

/* @var $model backend\modules\admin\models\Support */


use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use \yii\helpers\Url;

$this->title = Yii::t('support', 'Ticket') . ' №' . $model->id . ' «' . $model->title . '»';
$this->params['breadcrumbs'][] = ['label' => Yii::t('support', 'Support tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'View');

?>
    <div class="content-header">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => ['label' => Yii::t('admin', 'Support')],
        ]) ?>
    </div>
    <div class="content">
        <div class="card card-success card-outline">
            <div class="card-header p-0">
                <h3 class="card-title" style="padding: 12px 0 12px 20px;"><i
                            class="fas fa-file-import"></i> <?php echo Html::encode($this->title) ?>
                </h3>
                <?php if (Yii::$app->user->can('support.update')): ?>
                    <div class="card-tools" style=" padding: 7px 20px 0 0;">
                        <?php echo Html::a(Yii::t('admin', 'Update Ticket'), ['update', 'id' => $model->ticket_id ? $model->ticket_id : $model->id],
                            ['class' => 'btn bg-gradient-warning btn-sm']) ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::toRoute('/admin/support/answer')]); ?>
                <div class="support-view">
                    <div class="col-12 col-lg-10 mx-auto mt-3" style="padding: 0;">
                        <div class="direct-chat-messages"
                             style="height: auto; max-height: 500px; min-height: 300px; border-left: 1px solid #d4d4d4; border-right: 1px solid #d4d4d4; border-radius: 10px; background-color: #fbfbfb;">


                            <div class="my-support-m"
                                 style="width:100%; display: inline-flex; border-bottom: 1px solid #d4d4d4; border-top: 1px solid #d4d4d4; margin: 0.5rem 0; border-radius: 1px; <?php if ($model->answerer_id != $model->client_id) { ?> background-color:  #dbe8d5; <?php } else { ?>background-color: #ececec; <?php } ?>">
                                <div style="vertical-align: top; min-width: 248px; width: 248px; flex-grow: 1; display: flex; align-content: flex-start; flex-wrap: wrap; border-right: 1px solid #d4d4d4; border-radius: 1px; padding: 1rem;">
                                <span style="width: 100%; text-align: left;"> <?php if ($model->answerer_id != $model->client_id) {
                                        echo Yii::t('support', 'Tech Support');
                                    } ?></span>
                                    <span style="width: 100%; text-align: left;"> <?php echo ucwords($model->user->username) . ' ' . ucwords($model->user->last_name) ?></span>
                                    <span style="width: 100%; text-align: left; color: #777777;"><?php echo date('j F, H:m', strtotime($model->created_at)) ?></span>
                                </div>

                                <p style="min-width:248px; border-radius: 1px; padding: 1rem; margin:0; width: 100%;">
                                    <?php echo $model->text ?>
                                </p>
                            </div>


                            <?php if (!empty($modelAnswers = $model->supportAnswers)) { ?>
                                <?php foreach ($modelAnswers as $item) { ?>


                                    <div class="my-support-m"
                                         style="width:100%; display: inline-flex; border-bottom: 1px solid #d4d4d4; border-top: 1px solid #d4d4d4; margin: 0.5rem 0; border-radius: 1px; <?php if ($item->answerer_id != $model->client_id) { ?> background-color:  #dbe8d5; <?php } else { ?>background-color: #ececec; <?php } ?>">
                                        <div style=" vertical-align: top; min-width: 248px; width: 248px; display: flex; align-content: flex-start; flex-wrap: wrap; border-right: 1px solid #d4d4d4; border-radius: 1px; padding: 1rem;">
                                        <span style="width: 100%; text-align: left; color: #087d00; font-weight: 500;"> <?php if ($item->answerer_id != $model->client_id) { ?>
                                                <i class="fas fa-headset" style="color: #333333;"></i>
                                                <?php echo Yii::t('support', 'Tech Support');
                                            } ?></span>
                                            <span style="width: 100%; text-align: left;"><?php echo ucwords($item->user->username) . ' ' . ucwords($item->user->last_name) ?></span>
                                            <span style="width: 100%; text-align: left; color: #777777;"><?php echo date('j F, H:m', strtotime($item->created_at)) ?></span>
                                        </div>

                                        <p style="min-width:248px; border-radius: 1px; padding: 1rem; margin:0; width: 100%;">
                                            <?php echo $item->text ?>
                                        </p>
                                    </div>


                                <?php } ?>
                            <?php } ?>


                        </div>
                        <div class="box-footer" style="padding-top: 0rem; margin-top: 1rem;">
                            <?php echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
                            <?php echo $form->field($model, 'text', ['options' => ['class' => ''], 'template' => "<div class='col-xs-12 input-group' style='padding:0;'>{input}<span class='input-group-btn'>
                            <button type='submit' class='btn bg-gradient-success btn-flat' name='answer' value ='answer'>" . Yii::t('admin', 'Write answer') . "</button>
                          </span></div><div class='col-xs-12' style='padding:0;'>\n{error}</div>"])->textarea(['style' => 'max-width: inherit', 'value' => '']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-4 pt-4 border-top">
                    <div class="row col-12 col-lg-8 mx-auto">
                        <?php if (Yii::$app->user->can('support.listview')): ?>
                            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                                <?php echo Html::a(Yii::t('admin', 'List of all tickets'), ['index'],
                                    ['class' => 'btn bg-gradient-primary btn-block']) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<?php


$script = "
$( document ).ready(function() {
  document.querySelector('.direct-chat-messages').scrollTop = document.querySelector('.direct-chat-messages').scrollHeight;
});

    ";
$this->registerJs($script, yii\web\View::POS_END);