<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Support */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin(); ?>
<div class="col mx-auto max-wr-40">


    <?php echo $form->field($model, 'title') ?>
    <?php echo $form->field($model, 'text')->textArea() ?>

    <?php echo $form->field($model, 'category')->dropDownList($model->listCategories(),
        ['prompt' => Yii::t('support', 'Select category:')]) ?>
    <?php if (!$model->isNewRecord): ?>
        <?php echo $form->field($model, 'status')->dropDownList($model->listStatuses(),
            ['prompt' => Yii::t('support', 'Select status:')]) ?>
    <?php endif ?>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


