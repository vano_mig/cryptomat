<?php

/* @var $modelSearch \backend\modules\admin\models\SupportSearch */
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */


use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\formatter\SupportFormatter;
use backend\modules\admin\widgets\yii\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;


$this->title = Yii::t('support', 'Support tickets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Support')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-import"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('support.create')): ?>
                <?php echo Html::a(Yii::t('support', 'Add ticket'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $modelSearch,
                    'columns' => [
              [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'title',
                            'value' => function ($data) {
                                return Html::a($data->title, Url::toRoute(['/admin/support/view?id=' . $data->id]),
                                    ['style' => 'font-size: 16px; font-weight: 500;']);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'email',
                            'value' => function ($data) {
                                if (!empty($data->user)) {
                                    return Html::a($data->user->email,
                                        Url::toRoute(['/user/user/update?id=' . $data->user->id]));
                                } else {
                                    return '-';
                                }
                            },
                            'format' => 'raw',
                        ],

                        [
                            'attribute' => 'category',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(SupportFormatter::class)->asCategory($data->category);
                            },
                            'filter' => Html::activeDropDownList($modelSearch, 'category',
                                $modelSearch->listCategories(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(SupportFormatter::class)->asStatus($data->status);
                            },
                            'filter' => Html::activeDropDownList($modelSearch, 'status',
                                $modelSearch->listStatuses(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        'created_at',
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
//                                'template' => '{answer} {view} {update} {delete}',
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'visibleButtons' => [
                                    'view' => function ($models, $key, $index) {
                                        return \Yii::$app->user->can('support.view', ['user' => $models]);
                                    },
                                ],
                                'update' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('support.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('support.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

