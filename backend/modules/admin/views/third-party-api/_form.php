<?php

use backend\modules\admin\models\ApiName;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ThirdPartyApi */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
	'enableClientValidation' => true,
	'id' => 'twilio_form'
]); ?>

<div class="col mx-auto max-wr-40">
    <?php if(Yii::$app->user->can(User::ROLE_ADMIN)):?>
        <?= $form->field($model, 'company_id')->dropDownList(Company::getList(), ['prompt' => Yii::t('admin', 'Select Company')]) ?>
    <?php endif ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'provider_id')->dropDownList(ApiName::getListParty(), ['prompt' => Yii::t('admin', 'Select Api'), 'id' => 'select_api', 'disabled'=>$model->isNewRecord ? false : true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?php echo Yii::$app->view->render($model->form->form, ['model'=>$model, 'form'=>$form]) ?>
    <?php endif ?>
    <div id="ajaxForm"></div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
		<?php  if (Yii::$app->user->can('3rd_party_api.create') || Yii::$app->user->can('3rd_party_api.update')): ?>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
		<?php  endif; ?>
		<?php  if (Yii::$app->user->can('3rd_party_api.listview')): ?>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>
		<?php  endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
$('#select_api').on('change', (block) => {
    var val = $(block.target).val();
   $.ajax({
        url: "/admin/third-party-api/new-form",
        type: 'POST',
        dataType: 'json',
        data: {id: val, model_id: "$model->id"},
        success: function(res) {
            if (res) {
                $('#ajaxForm').html(res.data);
                    console.log(res.data)
            }
        },
        error: function(err) {
            alert('Connection error');
            console.log(err.responseText);
        }
    }) 
})
JS;

$this->registerJs($js, yii\web\View::POS_END);
?>