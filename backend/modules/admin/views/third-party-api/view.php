<?php

use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\user\models\Company;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ThirdPartyApi */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Third Party Apis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('gii', 'System')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-servicestack"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
							[
								'attribute' => 'company_id',
								'value' => function ($data) {
									return Company::getName($data->company_id);
								},
								'format' => 'html',
							],
							[
								'attribute' => 'provider_id',
								'value' => function ($data) {
									return ApiFormatter::apiGetNameById($data->provider_id);
								},
								'format' => 'html',
							],
                            'account_sid',
                            'auth_token',
                            'messaging_service_sid',
                            'from_number',
//            'cost',
//            'currency_id',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('3rd_party_api.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('3rd_party_api.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('3rd_party_api.list')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('admin', 'List'), ['index', 'id' => $model->id], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

