<?php

use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\ThirdPartyApiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Third Party Apis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('gii', 'System')],
    ])
    ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-servicestack"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if(Yii::$app->user->can('3rd_party_api.create')):?>
            <?= Html::a(Yii::t('admin', 'Create Third Party Api'), ['create'], ['class' => 'btn btn-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'name',
						[
							'attribute' => 'company_id',
							'value' => function ($data) {
								return Company::getName($data->company_id);
							},
							'format' => 'html',
                            'filter' => Html::activeDropDownList($searchModel, 'company_id',
                                Company::getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
						],
						[
							'attribute' => 'provider_id',
							'value' => function ($data) {
								return ApiFormatter::apiGetNameById($data->provider_id);
							},
							'format' => 'html',
						],
                        'account_sid',
                        //'auth_token',
                        //'messaging_service_sid',
                        //'from_number',
                        //'cost',
                        //'currency_id',
                        ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($models, $key, $index) {
                                    return Yii::$app->user->can('3rd_party_api.view', ['user' => $models]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('3rd_party_api.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('3rd_party_api.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>