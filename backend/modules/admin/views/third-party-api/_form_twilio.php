<?php

use backend\modules\admin\models\Currency;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ThirdPartyApi */
/* @var $form yii\widgets\ActiveForm */
//\backend\assets\AppAsset::register($this);
//\frontend\assets\SiteAsset::register($this);
?>

<?= $form->field($model, 'account_sid')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'auth_token')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'messaging_service_sid')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'from_number')->textInput(['maxlength' => true]) ?>




