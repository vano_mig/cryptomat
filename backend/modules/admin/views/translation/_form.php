<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\SourceMessage|yii\db\ActiveRecord */

/* @var $lang array */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\widgets\DetailView;

$i = 0;
$j = 0;
$itemOrder = 1;
$checkOrder = 1;
?>

<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::toRoute('/admin/translation/update-translation')]); ?>
<div class="row">
    <div class="row col-12 elevation-1 m-0">
        <div class="col min-wr-16 max-wr-30 mx-auto ">
            <label><?= Yii::t('translation', 'Info'); ?></label>
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'category',
                    'message'
                ],
            ]) ?>
        </div>
        <div class="col min-wr-16 max-wr-40 mx-auto ">
            <?php echo $form->field($model, 'id')->hiddenInput()->label(false) ?>
            <?php echo $form->field($model, 'comment')->textarea() ?>
        </div>
    </div>
    <div class="col row d-flex">
        <div class="row col-12 mt-2">
            <div class="row mx-auto d-flex">
                <?php foreach ($model->messages as $item) { ?>
                    <div class=" custom-control custom-checkbox border border-r-10 p-1 my-pl-2r m-2 w-auto elevation-1 <?= $item->language == 'de' ? 'order-0' : 'order-' . $checkOrder ?>">
                        <input type="checkbox" class="custom-control-input" id="customCheck<?php echo $j ?>"
                               data-toggle="collapse"
                               data-target="#collapse<?php echo $j ?>" aria-expanded="false"
                               aria-controls="collapse<?php echo $j ?>"
                            <?php
                            if ($item->translation != null && $item->translation != '' && preg_match("#[^\s*$]#", $item->translation) || $item->language == 'de') {
                                echo 'checked';
                            }
                            ?>
                        >
                        <label class="custom-control-label"
                               for="customCheck<?php echo $j ?>"><?php echo Yii::t('language', $lang[$item->language]) ?></label>
                    </div>
                    <?php $j++ ?>
                    <?php $checkOrder++ ?>
                <?php } ?>
            </div>
        </div>
        <?php foreach ($model->messages as $item) { ?>
            <div class="col min-wr-27 max-wr-40  my-2 mx-auto <?= $item->language == 'de' ? 'order-0' : 'order-' . $itemOrder ?> collapse multi-collapse
            <?php
            if ($item->translation != null && $item->translation != '' && preg_match("#[^\s*$]#", $item->translation) || $item->language == 'de') {
                echo 'show';
            }
            ?>"
                 id="collapse<?php echo $i ?>">
                <div class="col-12 border-r-5 border elevation-2 mx-2">
                    <?php echo $form->field($item, '[' . $i . ']language')->textInput(['disabled' => 'disabled', 'value' => Yii::t('language', $lang[$item->language])]) ?>
                    <?php echo $form->field($item, '[' . $i . ']id')->hiddenInput()->label(false) ?>
                    <?php echo $form->field($item, '[' . $i . ']language')->hiddenInput()->label(false) ?>
                    <?php echo $form->field($item, '[' . $i . ']translation')->textarea() ?>
                </div>
            </div>
            <?php $itemOrder++ ?>
            <?php $i++ ?>
        <?php } ?>
    </div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block', 'name' => 'save', 'value' => '1']) ?>
        </div>
        <?php if (Yii::$app->user->can('translation.delete')): ?>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn bg-gradient-danger btn-block',
                    'data' => [
                        'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        <?php endif; ?>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


