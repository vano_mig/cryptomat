<?php

/* @var $this yii\web\View */

/* @var $model backend\modules\admin\models\SourceMessageSearch */


use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use \backend\modules\admin\formatter\TranslationFormatter;


?>

<div class="col-12 mt-3 border-top">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row col pt-3 max-wr-50 mx-auto mt-2">


        <!--        --><?php //echo $form->field($model, 'nolanguage', ['options' => ['class' => 'col wr-17 d-flex align-items-center']])
        //            ->radioList([Yii::t('translation', 'With translation'), Yii::t('translation', 'Without translation')])
        //            ->label(false) ?>
        <div class="col wr-17 d-flex align-items-center field-sourcemessagesearch-nolanguage">
            <div class="col">
                <input type="hidden" name="SourceMessageSearch[nolanguage]" value="">
                <div id="sourcemessagesearch-nolanguage" role="radiogroup">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="i0" class="custom-control-input" name="SourceMessageSearch[nolanguage]"
                               value="1"
                            <?php
                            if (isset(Yii::$app->request->queryParams['SourceMessageSearch']['nolanguage'])) {
                                if (Yii::$app->request->queryParams['SourceMessageSearch']['nolanguage'] == '1') {
                                    echo 'checked';
                                }
                            }
                            ?>
                        >
                        <label class="custom-control-label"
                               for="i0"><?php echo Yii::t('translation', 'With translation') ?></label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="i1" class="custom-control-input" name="SourceMessageSearch[nolanguage]"
                               value="2"
                            <?php
                            if (isset(Yii::$app->request->queryParams['SourceMessageSearch']['nolanguage'])) {
                                if (Yii::$app->request->queryParams['SourceMessageSearch']['nolanguage'] != '1') {
                                    echo 'checked';
                                }
                            } else {
                                echo 'checked';
                            }
                            ?>
                        >
                        <label class="custom-control-label"
                               for="i1"><?php echo Yii::t('translation', 'Without translation') ?></label>
                        <div class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $form->field($model, 'islanguage', ['options' => ['class' => 'col'], 'labelOptions' => ['class' => 'col']])->dropDownList((new TranslationFormatter())->translationLanguage) ?>
    </div>
    <div class="col-12 mt-2 pb-3 mb-3 pt-4 border-bottom">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Accept'), ['class' => 'btn bg-gradient-info btn-block']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
