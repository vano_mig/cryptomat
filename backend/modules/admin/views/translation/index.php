<?php

/* @var $searchModel \yii\db\ActiveQuery */
/* @var $this \yii\web\View */
/* @var $searchModel \backend\modules\admin\models\SourceMessageSearch */

/* @var $dataProvider \yii\data\ActiveDataProvider */


use yii\bootstrap4\Html;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('admin', 'Translations');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-language"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('translation.create')): ?>
                <?php echo Html::a(Yii::t('admin', 'Add translation'), Url::toRoute(['create']), ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="table-responsive">

                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                        ],
                        'category',
                        [
                            'attribute' => 'message',
                            'contentOptions' => ['class' => 'text-wrap'],

                        ],
                        [
                            'attribute' => 'translation',
                            'value' => function ($data, $id, $nol, $sdgf) {
                                if (isset(Yii::$app->request->queryParams['SourceMessageSearch']['islanguage'])) {
                                    if (Yii::$app->request->queryParams['SourceMessageSearch']['islanguage'] != 'message') {
                                        foreach ($data->messages as $item) {
                                            if ($item->language == Yii::$app->request->queryParams['SourceMessageSearch']['islanguage']) {
                                                if (Yii::$app->request->queryParams['SourceMessageSearch']['nolanguage'] == 2) {
                                                    return '---';
                                                } else {
                                                    return $item->translation;
                                                }
                                            }
                                        }
                                    }
                                }
                                $tr = '';
                                foreach ($data->messages as $key => $item) {
                                    if (!empty($item->translation)) {
                                        if ($item->translation != null && $item->translation != '' && preg_match("#[^\s*$]#", $item->translation)) {
                                            if ($key != 0) {
                                                $tr .= ' ' . ',' . ' ';
                                            }
                                            if ($item->language == 'uk') {
                                                $tr .= Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']) . Html::tag('span', $item->language);
                                            } elseif ($item->language == 'en') {
                                                $tr .= Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']) . Html::tag('span', $item->language);
                                            } else {
                                                $tr .= Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $item->language]) . Html::tag('span', $item->language);
                                            }
                                        }
                                    }
                                }
                                return $tr;

                            },
                            'contentOptions' => ['class' => 'text-wrap'],
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('translation.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('translation.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('translation.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
