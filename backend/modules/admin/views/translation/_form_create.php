<?php

/* @var $modelMessage \backend\modules\admin\models\Message */
/* @var $this \yii\web\View */
/* @var $model \backend\modules\admin\models\SourceMessage|\yii\db\ActiveRecord */

/* @var $lang array */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

$j = 1;
$itemOrder = 1;
?>

<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::toRoute('/admin/translation/create-translation')]); ?>
<div class="row">
    <div class="row col-12 elevation-1 m-0">
        <div class="col min-wr-16 max-wr-40 mx-auto ">
            <?php echo $form->field($model, 'category')->textInput() ?>
            <?php echo $form->field($model, 'message')->textarea() ?>
            <?php echo $form->field($model, 'comment')->textarea() ?>
        </div>
    </div>
    <div class="col row d-flex">
        <div class="row col-12 mt-2">
            <div class="row mx-auto d-flex">
                <?php foreach ($lang as $key => $item) { ?>
                    <div class=" custom-control custom-checkbox border border-r-10 p-1 my-pl-2r m-2 w-auto elevation-1 <?= $key == 'de' ? 'order-0' : 'order-' . $j ?>">
                        <input type="checkbox" class="custom-control-input" id="customCheck<?php echo $j ?>"
                               data-toggle="collapse"
                               data-target="#collapse<?php echo $j ?>" aria-expanded="false"
                               aria-controls="collapse<?php echo $j ?>" <?= $key == 'de' ? 'checked' : '' ?>>
                        <label class="custom-control-label"
                               for="customCheck<?php echo $j ?>"><?php echo Yii::t('language', $item) ?></label>
                    </div>
                    <?php $j++ ?>
                <?php } ?>
            </div>
        </div>
        <?php foreach ($lang as $key => $item) { ?>
            <div class="col min-wr-27 max-wr-40  my-2 mx-auto <?= $key == 'de' ? 'order-0  show' : 'order-' . $itemOrder ?> collapse multi-collapse"
                 id="collapse<?php echo $itemOrder ?>">
                <div class="col-12 border-r-5 border elevation-2 mx-2">
                    <?php echo $form->field($modelMessage, '[' . $item . ']language')->textInput(['disabled' => 'disabled', 'value' => Yii::t('language', $item)]) ?>
                    <?php echo $form->field($modelMessage, '[' . $item . ']language')->hiddenInput(['value' => $key])->label(false) ?>
                    <?php echo $form->field($modelMessage, '[' . $item . ']translation')->textarea() ?>
                </div>
            </div>
            <?php $itemOrder++ ?>
        <?php } ?>
    </div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block', 'name' => 'save', 'value' => '1']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
