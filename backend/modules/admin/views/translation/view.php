<?php

/* @var $this yii\web\View */

/* @var $model backend\modules\admin\models\SourceMessage */

/* @var $langs array */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('admin', 'View') . ': ' . $model->message;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'View');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-language"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?php echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'category',
                            'message',
                        ],
                    ]) ?>
                </div>
                <?php if (!empty($model->messages)) : ?>
                    <?php foreach ($model->messages as $lang): ?>
                        <div class="table-responsive">
                            <?php echo DetailView::widget([
                                'model' => $lang,
                                'attributes' => [
                                    [
                                        'label' => Yii::t('translation', 'Language'),
                                        'value' => Yii::t('language', $langs[$lang->language]),
                                        'contentOptions' => ['class' => ''],
                                        'captionOptions' => ['class' => 'my-tw-9'],
                                    ],
                                    [
                                        'label' => Yii::t('translation', 'Translation'),
                                        'value' => $lang->translation,
                                        'contentOptions' => ['class' => ''],
                                        'captionOptions' => ['class' => 'my-tw-9'],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('translation.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id],
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('translation.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('translation.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), ['index'],
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>