<?php

/* @var $modelMessage backend\modules\admin\models\Message */
/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\SourceMessage */
/* @var $lang array */
/* @var $count int */



use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('translation', 'Update message') . ': ' . $model->message;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-language"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
                'lang' => $lang,
            ]) ?>
        </div>
    </div>
</div>
