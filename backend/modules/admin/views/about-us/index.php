<?php
/* @var $this yii\web\View */

use backend\modules\admin\models\StaticContent;
use yii\bootstrap4\Html;

$this->title = Yii::t('user', 'About Us');
$this->params['breadcrumbs'][] = '';

?>

<div class="content-header">
    <?php echo '<h9><span style="color: #6a6a6a;">' . $this->title . '</span></h9>' ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-info-circle"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col-10 col-sm-8 mx-auto">
                <img class="img-fluid d-block col-12 col-sm-12 col-md-10 col-lg-6 mx-auto"
                     src="/img/system/logo/Cryptomatic_Logo.png" alt="">
            </div>
            <div class="box-body col-12 col-sm-12 col-md-10 col-lg-8 mx-auto pt-4">
                <?php echo StaticContent::Content('6'); ?>
                <div class="col-12">
                    <p style="text-align: center;">
                        <a style="font-size: 2rem; padding-top: 5px;"
                           href="https://my-smart-store24.de/">my-smart-store24.de</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
