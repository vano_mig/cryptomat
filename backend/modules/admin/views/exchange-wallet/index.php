<?php

//use backend\modules\admin\formatter\ApiCryptoCurrencyFormatter;
use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\admin\formatter\WalletFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\ExchangeWalletSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Exchange Wallets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Exchange/Wallets')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-exchange-alt"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if(Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?= Html::a(Yii::t('admin', 'Create Exchange Wallet'), ['create'], ['class' => 'btn btn-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'provider_name',
                        [
                            'attribute' => 'provider_id',
                            'value' => function ($data) {
								return ApiFormatter::apiCurrencyGetPairById($data->provider_id);
                            },
                            'format' => 'html',
                        ],
						[
							'attribute' => 'company_id',
							'value' => function ($data) {
								return Company::getName($data->company_id);
							},
                            'filter' => Html::activeDropDownList($searchModel, 'company_id',
                                Company::getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
							'format' => 'html',
                            'visible'=>Yii::$app->user->can(User::ROLE_ADMIN)
						],
                        [
                            'attribute' => 'block',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(WalletFormatter::class)->exchangeGetStatus($data->block);
                            },
                            'format' => 'html',
                        ],
                        [
                            'header' => Yii::t('wallet', 'Balance'),
                            'value' => function ($data) {
								return 'Loading...';
//								return $data->balance;
//                                return Html::tag('div', $data->balance, ['params' => ['id' => 'id2']]);
//                                return Html::tag('span', $data->balance, ['class' => 'balance', 'attributes' => ['data-id' => $data->id]]);
//                                return Yii::$app->formatter->load(WalletFormatter::class)->getBalanceCash($data);
                            },
							'contentOptions' => function ($data) {
								return ['data-id' => $data->id, 'class' => 'balance'];
							},
                            'format' => 'html',
                        ],
                        ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($models, $key, $index) {
                                    return \Yii::$app->user->can('identification_profile.view', ['user' => $models]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('identification_profile.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('identification_profile.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>


<?php
$error = Yii::t('admin', 'Error');
$url = Yii::getAlias('@domain') . Url::to(['/admin/exchange-wallet/check-balance']);
$js = <<<JS
    $(document).ready(async () => {
        console.log('data');
        let datas = $('.balance');
        for(var i = 0; i < datas.length; i++) {
            await $.ajax({
                'url': "$url",
                'method': 'POST',
                'data': {'id': $(datas[i]).data('id'), 'i': i}
            }).done((data) => {
                try {
                  let json = JSON.parse(data);
                  console.log(json)
                   if (json['status'] === 'success')
                        $(datas[json['info']['i']]).html(json['info']['balance'])
                   else {
                       $(datas[json['info']['i']]).html("$error")
                   }
                } catch (data) {
                    console.log(data);
                } 
            }).fail((data) => {
                console.log(data);
            })
        }
    })
JS;
$this->registerJS($js);

?>