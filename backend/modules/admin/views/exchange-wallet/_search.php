<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ReplenishWalletSeatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="replenish-wallet-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'provider_name') ?>

    <?= $form->field($model, 'provider_id') ?>

    <?= $form->field($model, 'api_key') ?>

    <?= $form->field($model, 'api_secret') ?>

    <?= $form->field($model, 'client_id') ?>

    <?php // echo $form->field($model, 'block') ?>

    <?php // echo $form->field($model, 'company_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('virtual_terminal', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('virtual_terminal', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
