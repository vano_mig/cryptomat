<?php

/* @var $this yii\web\View */
?>

<div class="box box-primary">
    <div class="box-body">
        <div class="api-index">

            <h3>
                <?php echo Yii::t('api', 'API Documentation'); ?>
            </h3>
            <div class="col-xs-12">
                <h4>
                    <?php echo Yii::t('api', 'Fridge api'); ?>
                </h4>
                <div class="col-xs-12">
                    <h5>
                        <?php echo Yii::t('api', 'Activate fridge'); ?>
                    </h5>
                    <div class="col-xs-12">
                        <?php $param = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/connection/api/fridge?unique=$param'; ?>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Method'); ?></div>
                        <div class="col-xs-9">GET</div>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Url {url}', ['url' => $param]); ?></div>
                        <div class="col-xs-9">$param
                            = <?php echo Yii::t('api', 'Fridge unique identification in base64 format'); ?></div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h5>
                        <?php echo Yii::t('api', 'Get current products data'); ?>
                    </h5>
                    <div class="col-xs-12">
                        <?php $param = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/connection/api/get-products?request=$param'; ?>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Method'); ?></div>
                        <div class="col-xs-9">GET</div>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Url {url}', ['url' => $param]); ?></div>
                        <div class="col-xs-9">
                            <div class="col-xs-12">
                                <div class="col-xs-2">unique</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Fridge unique identification in base64 format'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">name</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Product name'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">price</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Product price'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">quantity</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Product quantity'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">products</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'List of products array in json format'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">param</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Array with keys: unique and products in base64 format'); ?></div>
                            </div>
                        </div>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Example'); ?></div>
                        <div class="col-xs-9" style="white-space: pre-wrap">

                            $listProducts = [
                                [
                                    'name'=>'Milk',
                                    'price'=>12.5,
                                    'quantity'=>2
                                ],
                                [
                                    'name'=>'Ice cream',
                                    'price'=>10.83,
                                    'quantity'=>1
                                ],
                            ];

                            $products = [
                                {
                                    "name":"Milk",
                                    "price":12.5,
                                    "quantity":2
                                },
                                {
                                    "name":"Ice cream",
                                    "price":10.83,
                                    "quantity":1
                                }
                            ]

                            <?php $listProducts = [
                                [
                                    'name' => 'Milk',
                                    'price' => 12.5,
                                    'quantity' => 2
                                ],
                                [
                                    'name' => 'Ice cream',
                                    'price' => 10.83,
                                    'quantity' => 1
                                ],
                            ];
                            $products = json_encode($listProducts);

                            $param = base64_encode($products); ?>

                            $param = W3sibmFtZSI6Ik1pbGsiLCJwcmljZSI6MTIuNSwicXVhbnRpdHkiOjJ9LHsibmFtZSI6IkljZSBjcmVhbSIsInByaWNlIjoxMC44MywicXVhbnRpdHkiOjF9XQ==

                            URL = <?php echo $param = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/api/get-products?request=' . $param; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h5>
                        <?php echo Yii::t('api', 'Monitoring fridge system'); ?>
                    </h5>
                    <div class="col-xs-12">
                        <?php $param = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/api/monitoring?params=$param'; ?>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Method'); ?></div>
                        <div class="col-xs-9">GET</div>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Url {url}', ['url' => $param]); ?></div>
                        <div class="col-xs-9">
                            <div class="col-xs-12">
                                <div class="col-xs-2">unique</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Fridge unique identification in base64 format'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">status</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'System status'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">error</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Errors'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">device</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Devixe of fridge with error'); ?></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-2">message</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Error description'); ?></div>
                            </div>

                            <div class="col-xs-12">
                                <div class="col-xs-2">param</div>
                                <div class="col-xs-10"><?php echo Yii::t('api', 'Array with keys: unique and fridge status in base64 format'); ?></div>
                            </div>
                        </div>
                        <div class="col-xs-3"><?php echo Yii::t('api', 'Example'); ?></div>
                        <div class="col-xs-9" style="white-space: pre-wrap">

                            $unique = 'unuqie',
                            $error = [
                                [
                                    'device'=>'door',
                                    'message'=>'open door',
                                ],
                                [
                                    'device'=>'nfc',
                                    'message'=>'not answered',
                                ],
                            ];
                            $resultOk = [
                                'unique'=>$unique,
                                'status'=>'success',
                            ];
                            $resultError = [
                                'unique'=>$unique,
                                'status'=>'error',
                                'error'=>$error
                            ];

                            $answerSuccess = {
                                "unique":"unuqie",
                                "status":"success"
                            }

                            $answerError =  {
                                "unique":"unuqie",
                                "status":"error",
                                "error":[
                                    {
                                        "device":"door",
                                        "message":"open door"
                                    },
                                    {
                                        "device":"nfc",
                                        "message":"not answered"
                                    }
                                ]
                            }

                            $param = W3sibmFtZSI6Ik1pbGsiLCJwcmljZSI6MTIuNSwicXVhbnRpdHkiOjJ9LHsibmFtZSI6IkljZSBjcmVhbSIsInByaWNlIjoxMC44MywicXVhbnRpdHkiOjF9XQ==
                            <?php
                            $unique = 'unuqie';
                            $error = [
                                [
                                    'device'=>'door',
                                    'message'=>'open door',
                                ],
                                [
                                    'device'=>'nfc',
                                    'message'=>'not answered',
                                ],
                            ];
                            $resultOk = [
                                'unique'=>$unique,
                                'status'=>'success',
                            ];
                            $resultError = [
                                'unique'=>$unique,
                                'status'=>'error',
                                'error'=>$error
                            ];
                            $products = json_encode($resultError);

                            $param = base64_encode($products); ?>

                            URL = <?php echo $param = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/api/monitoring?request=' . $param; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>