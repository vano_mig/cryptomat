<?php

/* @var $DTO \common\models\tool\DTO */

/* @var $this yii\web\View */

use backend\modules\admin\formatter\TransactionFormatter;
use backend\modules\admin\widgets\yii\GridView;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

$this->title = Yii::t('admin', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (isset($DTO->error)) : ?>
    <img class="img-fluid col-11 col-sm-12 col-md-10 img-logo img-logo-cub" src="/img/system/logo/Cryptomatic_Logo.svg" alt="">
<?php else: ?>
    <div class="content-header">
    </div>
    <div class="a-block col-12">
    </div>
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="d-inline-flex align-items-center py-0 pl-0">
                        <div class="card card-primary w-100 mb-0">
                            <div class="card-header">
                                <h3 class="card-title"><?php echo Yii::t('admin', 'Profit') ?></h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <canvas id="profitChart" class="w-100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="d-inline-flex align-items-center py-0 pl-0">
                        <div class="card card-primary w-100 mb-0">
                            <div class="card-header">
                                <h3 class="card-title"><?php echo Yii::t('dashboard', 'Currency received') ?></h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <canvas id="receiveChart" class="w-100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="d-inline-flex align-items-center py-0 pl-0">
                        <div class="card card-primary w-100 mb-0">
                            <div class="card-header">
                                <h3 class="card-title"><?php echo Yii::t('dashboard', 'Transferred currency') ?></h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <canvas id="sentChart" class="w-100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="card">-->
<!--                    <div class="card-header d-inline-flex align-items-center py-0 pl-0">-->
<!--                        <div class="row card-title col justify-content-center justify-content-md-start">-->
<!--                            <div class="col d-inline-flex align-items-center wr-16 min-hr-3">-->
<!--                                <i class="fas fa-angle-double-down text-24 mr-1"></i>-->
<!--                                --><?php //echo Yii::t('dashboard', 'Currency received') ?>
<!--                                <i class="fas fa-long-arrow-alt-right ml-3 fa-2x"></i>-->
<!--                            </div>-->
<!--                            <div class="col wr-17 select-equipment-sales">-->
<!--                                --><?php //echo \kartik\select2\Select2::widget([
//                                    'name' => 'equipment_sales',
//                                    'hideSearch' => true,
//                                    'size' => \kartik\select2\Select2::LARGE,
//                                    'data' => $DTO->currencysIn,
//                                    'value' => 'USD',
//                                ]); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="card-tools">-->
<!--                            <button type="button" class="btn btn-tool"-->
<!--                                    data-card-widget="collapse">-->
<!--                                <i class="fas fa-minus"></i>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="card-body my-h-block">-->
<!--                        <div class="my-card-chart d-flex align-items-center">-->
<!--                            <div class="w-100 my-chart-div">-->
<!--                                <canvas class="my-chart-canvas w-100" id="myChart"></canvas>-->
<!--                            </div>-->
<!--                            <div class="my-legend col p-0" id="legend"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="card">-->
<!--                    <div class="card-header d-inline-flex align-items-center py-0 pl-0">-->
<!--                        <div class="row card-title col justify-content-center justify-content-md-start">-->
<!--                            <div class="col d-inline-flex align-items-center wr-16 min-hr-3">-->
<!--                                <i class="fas fa-angle-double-up text-24 mr-1"></i>-->
<!--                                --><?php //echo Yii::t('dashboard', 'Transferred currency') ?>
<!--                                <i class="fas fa-long-arrow-alt-right ml-3 fa-2x"></i>-->
<!--                            </div>-->
<!--                            <div class="col wr-17 select-equipment-sales_2">-->
<!--                                --><?php //echo \kartik\select2\Select2::widget([
//                                    'id' => 'select2_2',
//                                    'name' => 'equipment_sales_2',
//                                    'hideSearch' => true,
//                                    'size' => \kartik\select2\Select2::LARGE,
//                                    'data' => $DTO->currencysOut,
//                                    'value' => 'USD',
//                                ]); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="card-tools">-->
<!--                            <button type="button" class="btn btn-tool"-->
<!--                                    data-card-widget="collapse">-->
<!--                                <i class="fas fa-minus"></i>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="card-body my-h-block_2">-->
<!--                        <div class="my-card-chart d-flex align-items-center">-->
<!--                            <div class="w-100 my-chart-div">-->
<!--                                <canvas class="my-chart-canvas w-100" id="myChart_2"></canvas>-->
<!--                            </div>-->
<!--                            <div class="my-legend_2 col p-0" id="legend_2"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i
                                    class="fas fa-align-left"></i> <?php echo Yii::t('admin', 'Transactions at last 7 days:') ?>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool"
                                    data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php Pjax::begin();
                            echo GridView::widget([
                                'dataProvider' => $DTO->transactionHistoryDataProvider,
                                'columns' => [
                                    [
                                        'attribute' => 'id',
                                        'filter' => false,
                                    ],
//                                [
//                                    'label' => Yii::t('dashboard', 'Source'),
//                                    'value' => function ($data) {
//                                        if ($data->widget_id) {
//                                            return Yii::t('api', 'Widget');
//                                        } elseif ($data->terminal_id) {
//                                            return Yii::t('terminal', 'ATM');
//                                        } else {
//                                            return '---';
//                                        }
//                                    },
//                                    'filter' => false,
//                                ],
                                    [
                                        'attribute' => 'transaction_id',
                                        'value' => function ($data) {
                                            return Html::a($data->transaction_id, ['transaction/view', 'id' => $data->id], ['class' => 'text-decoration-none']);
                                        },
                                        'format' => 'raw',
                                        'filter' => false,
                                    ],
                                    [
                                        'attribute' => 'transaction_begin',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(TransactionFormatter::class)->asTransactionUid($data->transaction_begin);
                                        },
                                        'format' => 'raw',
                                        'filter' => false,
                                    ],
//                                    [
//                                        'attribute' => 'type',
//                                        'value' => function ($data) {
//                                            return Yii::$app->formatter->load(TransactionFormatter::class)->asType($data);
//                                        },
//                                        'filter' => false,
//                                        'format' => 'raw',
//                                    ],
                                    [
                                        'attribute' => 'status',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(TransactionFormatter::class)->asStatus($data);
                                        },
                                        'filter' => false,
                                        'format' => 'raw',
                                    ],
                                    'created_at',
                                ],
                            ]);
                            Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php
//$labels = $profit['label'];
//$data = $profit['data'];
//$color = $profit['backgroundColor'];

$labels = '';
$data = '';
$background = '';

$labelsReceive = '';
$dataReceive = '';
$backgroundReceive = '';

$labelsSent = '';
$dataSent = '';
$backgroundSent = '';

if(!empty($profit['label'])) {
    $labels  = '[';
    foreach ($profit['label'] as $item) {
        $labels .= "'".$item."' ,";
    }
    $labels .= ']';

    $data  = '[';
    foreach ($profit['data'] as $item) {
        $data .= $item.' ,';
    }
    $data .= ']';

    $background  = '[';
    foreach ($profit['backgroundColor'] as $item) {
        $background .= "'".$item."' ,";
    }
    $background .= ']';
} else {
    $labels = '[]';
    $data = '[]';
    $background = '[]';
}

if(!empty($receiveCurrency['label'])) {
    $labelsReceive  = '[';
    foreach ($receiveCurrency['label'] as $item) {
        $labelsReceive .= "'".$item."' ,";
    }
    $labelsReceive .= ']';

    $dataReceive  = '[';
    foreach ($receiveCurrency['data'] as $item) {
        $dataReceive .= $item.' ,';
    }
    $dataReceive .= ']';

    $backgroundReceive  = '[';
    foreach ($receiveCurrency['backgroundColor'] as $item) {
        $backgroundReceive .= "'".$item."' ,";
    }
    $backgroundReceive .= ']';
} else {
    $labelsReceive = '[]';
    $dataReceive = '[]';
    $backgroundReceive = '[]';
}

if(!empty($sentCurrency['label'])) {
    $labelsSent  = '[';
    foreach ($sentCurrency['label'] as $item) {
        $labelsSent .= "'".$item."' ,";
    }
    $labelsSent .= ']';

    $dataSent  = '[';
    foreach ($sentCurrency['data'] as $item) {
        $dataSent .= $item.' ,';
    }
    $dataSent .= ']';

    $backgroundSent  = '[';
    foreach ($sentCurrency['backgroundColor'] as $item) {
        $backgroundSent .= "'".$item."' ,";
    }
    $backgroundSent .= ']';
} else {
    $labelsSent = '[]';
    $dataSent = '[]';
    $backgroundSent = '[]';
}

$chart = "
    $(window).on('load', function() {
        var donutChartCanvas = $('#profitChart').get(0).getContext('2d')
    
        var donutData = {
          labels: $labels,
          datasets: [
            {
              data: $data,
              backgroundColor : $background,
            }
          ]
        }
        var donutOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        var chartDognut = new Chart( donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        });
        chartDognut.canvas.parentNode.style.height = '430px';
        chartDognut.update();
    });
";

$chart2 = "
    $(window).on('load', function() {
        var donutChartCanvas = $('#receiveChart').get(0).getContext('2d')
    
        var donutData = {
          labels: $labelsReceive,
          datasets: [
            {
              data: $dataReceive,
              backgroundColor : $backgroundReceive,
            }
          ]
        }
        var donutOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        var chartDognut = new Chart( donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        });
        chartDognut.canvas.parentNode.style.height = '430px';
        chartDognut.update();
    });
";

$chart3 = "
    $(window).on('load', function() {
        var donutChartCanvas = $('#sentChart').get(0).getContext('2d')
    
        var donutData = {
          labels: $labelsSent,
          datasets: [
            {
              data: $dataSent,
              backgroundColor : $backgroundSent,
            }
          ]
        }
        var donutOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        var chartDognut = new Chart( donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        });
        chartDognut.canvas.parentNode.style.height = '430px';
        chartDognut.update();
    });
";

//$lwftl = '[]';
//$lwftl2 = '[]';
//$lwftd = "[";
//$lwftd2 = "[";
//if (!isset($DTO->error)) {
//    foreach ($DTO->currencysIn as $cu) {
//        $lwftd .= "['" . $cu . "', [" . $DTO->arrayIn[$cu]["chartData"] . "]],";
//        $lwftl = "[" . $DTO->arrayIn[$cu]["lastSevenWeekDate"] . "]";
//    }
//    foreach ($DTO->currencysOut as $cus) {
//        $lwftd2 .= "['" . $cus . "', [" . $DTO->arrayOutOut[$cus]["chartData"] . "]],";
//        $lwftl2 = "[" . $DTO->arrayOutOut[$cus]["lastSevenWeekDate"] . "]";
//    }
//}
//
//
//$lwftd .= "]";
//$lwftd2 .= "]";
//$js2 = "
//    $(window).on('load', function () {
//
//        var lwftl_2 = $lwftl2;
//        var lwftd_2 = new Map($lwftd2);
//
//        var chartLabels_2 = lwftl_2;
//        var chartDatasets_2 = lwftd_2.get('USD');
//
//        var ctx_2 = document.getElementById('myChart_2').getContext('2d');
//        var chart_2 = new Chart(ctx_2, {
//            type: 'line',
//            data: {
//                labels: chartLabels_2,
//                datasets: chartDatasets_2
//            },
//            options: {
//                responsive: true,
//                maintainAspectRatio: false,
//                legend: {
//                display: false,
//                    position: 'bottom',
//                    labels: {
//                        fontSize: 14,
//                        padding: 10
//                    }
//                },
//                scales: {
//                    xAxes: [{
//                        display: true,
//                        scaleLabel: {
//                            display: true
//                        }
//                    }],
//                    yAxes: [{
//                        display: true,
//                        scaleLabel: {
//                            display: true,
//                            labelString: ''
//                        },
//                        ticks: {
//                            suggestedMin: 0
//                        }
//                    }]
//                }
//            }
//        });
//
//        chart_2.canvas.parentNode.style.height = '430px';
//
//        function legendClickCallback_2(event) {
//            event = event || window.event;
//
//            var target = event.target || event.srcElement;
//            while (target.nodeName !== 'LI') {
//                target = target.parentElement;
//            }
//            var parent = target.parentElement;
//            var index = Array.prototype.slice.call(parent.children).indexOf(target);
//
//            if (chart_2.getDatasetMeta(index).hidden === null || chart_2.getDatasetMeta(index).hidden === false) {
//                target.classList.add('hidden');
//                chart_2.getDatasetMeta(index).hidden=true;
//            } else {
//                target.classList.remove('hidden');
//                chart_2.getDatasetMeta(index).hidden=false;
//            }
//            chart_2.update();
//        }
//
//        var myLegendContainer_2 = document.getElementById('legend_2');
//        myLegendContainer_2.innerHTML = chart_2.generateLegend();
//        var legendItems_2 = myLegendContainer_2.getElementsByTagName('li');
//        for (var i = 0; i < legendItems_2.length; i += 1) {
//            legendItems_2[i].addEventListener('click', legendClickCallback_2, false);
//        }
//
//        var valess_2;
//
//        $('.select-equipment-sales_2 select').change(function() {
//            valess_2 = $('.select-equipment-sales_2 select').val();
//            chart_2.data.datasets = lwftd_2.get(valess_2);
//            chart_2.update();
//            var myLegendContainer_2 = document.getElementById('legend_2');
//            myLegendContainer_2.innerHTML = chart_2.generateLegend();
//            var legendItems_2 = myLegendContainer_2.getElementsByTagName('li');
//            for (var i = 0; i < legendItems_2.length; i += 1) {
//                legendItems_2[i].addEventListener('click', legendClickCallback_2, false);
//            }
//        });
//    });
//    ";
//
//$js1 = "
//    $(window).on('load', function () {
//
//        var lwftl = $lwftl;
//        var lwftd = new Map($lwftd);
//
//        var chartLabels = lwftl;
//        var chartDatasets = lwftd.get('USD');
//
//        var ctx = document.getElementById('myChart').getContext('2d');
//        var chart = new Chart(ctx, {
//            type: 'line',
//            data: {
//                labels: chartLabels,
//                datasets: chartDatasets
//            },
//            options: {
//                responsive: true,
//                maintainAspectRatio: false,
//                legend: {
//                display: false,
//                    position: 'bottom',
//                    labels: {
//                        fontSize: 14,
//                        padding: 10
//                    }
//                },
//                scales: {
//                    xAxes: [{
//                        display: true,
//                        scaleLabel: {
//                            display: true
//                        }
//                    }],
//                    yAxes: [{
//                        display: true,
//                        scaleLabel: {
//                            display: true,
//                            labelString: ''
//                        },
//                        ticks: {
//                            suggestedMin: 0
//                        }
//                    }]
//                }
//            }
//        });
//
//        chart.canvas.parentNode.style.height = '430px';
//
//        function legendClickCallback(event) {
//            event = event || window.event;
//
//            var target = event.target || event.srcElement;
//            while (target.nodeName !== 'LI') {
//                target = target.parentElement;
//            }
//            var parent = target.parentElement;
//            var index = Array.prototype.slice.call(parent.children).indexOf(target);
//
//            if (chart.getDatasetMeta(index).hidden === null || chart.getDatasetMeta(index).hidden === false) {
//                target.classList.add('hidden');
//                chart.getDatasetMeta(index).hidden=true;
//            } else {
//                target.classList.remove('hidden');
//                chart.getDatasetMeta(index).hidden=false;
//            }
//            chart.update();
//        }
//
//        var myLegendContainer = document.getElementById('legend');
//        myLegendContainer.innerHTML = chart.generateLegend();
//        var legendItems = myLegendContainer.getElementsByTagName('li');
//        for (var i = 0; i < legendItems.length; i += 1) {
//            legendItems[i].addEventListener('click', legendClickCallback, false);
//        }
//
//        var valess;
//
//        $('.select-equipment-sales select').change(function() {
//            valess = $('.select-equipment-sales select').val();
//            chart.data.datasets = lwftd.get(valess);
//            chart.update();
//            var myLegendContainer = document.getElementById('legend');
//            myLegendContainer.innerHTML = chart.generateLegend();
//            var legendItems = myLegendContainer.getElementsByTagName('li');
//            for (var i = 0; i < legendItems.length; i += 1) {
//                legendItems[i].addEventListener('click', legendClickCallback, false);
//            }
//        });
//    });
//    ";

$this->registerJs($chart, yii\web\View::POS_END);
$this->registerJs($chart2, yii\web\View::POS_END);
$this->registerJs($chart3, yii\web\View::POS_END);
//$this->registerJs($js1, yii\web\View::POS_END);
//$this->registerJs($js2, yii\web\View::POS_END);
