<?php

/* @var $countProductError array */
/* @var $information array */
/* @var $date array */
/* @var $pendingOrdersStorage int|string */
/* @var $pendingOrdersFridge int|string */
/* @var $users int|string */
/* @var $lastWeekTransactions array */
/* @var $agents int|string */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $this yii\web\View */
/* @var $modelPasss array|\backend\modules\pkpass\models\PkpassPass|null|\yii\db\ActiveRecord */
/* @var $writer \BaconQrCode\Writer */

use backend\modules\fridge\formatter\FridgeMonitoringFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = Yii::t('admin', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
$url = urlencode(Url::base(true) . '/pkpass/temp/' . $modelPasss->serial_number . '_pass.pkpass');
?>

<img class="img-fluid col-11 col-sm-12 col-md-10 img-logo img-logo-cub" style="top: 38vh" src="/img/system/logo/Cryptomatic_Logo.svg" alt="">
<!--<main class="mt-2 container m-auto p-0 main-index" style="min-height: 100% !important;">-->
<!---->
<!--</main>-->
<!--<div class="col-12">-->
<!--    <img class="img-fluid col-11 col-sm-12 col-md-10 img-logo img-logo-cub" src="/img/system/logo/Cryptomatic_Logo.svg" alt="">-->
<!--</div>-->

