<?php

use backend\modules\admin\widgets\yii\GridView;
use backend\modules\fridge\formatter\FridgeMonitoringFormatter;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $fridge array */
/* @var $subAgents integer */
/* @var $qr_code */
/* @var $card array|backend\modules\admin\models\IdentificationCard|null|yii\db\ActiveRecord */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cryptomatic';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
</div>
<div class="content">
    <div class="row">
        <div class="col max-wr-20">
            <div class="info-box">
                <?php if ($card): ?>
                    <span class="info-box-icon elevation-1 bg-green"><i class="far fa-id-card"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?php echo Yii::t('site', 'Identification card') ?></span>
                        <span class="info-box-text"><?php echo $card->card_id; ?></span>
                    </div>
                <?php else: ?>
                    <span class="info-box-icon elevation-1 bg-yellow"><i class="far fa-id-card"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?php echo Yii::t('site', 'Identification card') ?></span>
                        <span class="info-box-text"><?php echo Yii::t('admin', 'No card') ?></span>
                    </div>
                <?php endif ?>
            </div>
        </div>
        <div class="col max-wr-20">
            <div class="info-box">
                <?php if ($qr_code->pkpass_identifier): ?>
                    <span class="info-box-icon elevation-1 bg-green"><i class="fas fa-qrcode"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?php echo Yii::t('admin', 'QR-code') ?></span>
                        <span class="info-box-text"><?php echo $qr_code->qr_key; ?></span>
                    </div>
                <?php else: ?>
                    <span class="info-box-icon elevation-1 bg-yellow"><i class="fas fa-qrcode"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number"><?php echo Yii::t('admin', 'QR-code') ?></span>
                        <span class="info-box-text"><?php echo Yii::t('admin', 'No QR-code') ?></span>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i
                            class="fas fa-align-left"></i> <?php echo Yii::t('admin', 'Transactions at last 7 days:') ?>
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool"
                            data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <?php Pjax::begin();
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'id',
                            'created_at',
                            [
                                'attribute'=>'fridge_unique_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(FridgeMonitoringFormatter::class)->asName($data['fridge_unique_id']);
                                },
                            ],
                            'amount',
                            'currency',
                            'response_text',
                            'receipt',
                            'ref_card_no',
                        ],
                    ]);
                    Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
