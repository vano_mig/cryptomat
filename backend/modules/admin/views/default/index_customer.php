<?php

/* @var $information array */
/* @var $this \yii\web\View */
/* @var $date array */
/* @var $pendingOrdersFridge int|string */
/* @var $lastWeekTransactions array */
/* @var $countProductError array */
/* @var $pendingOrdersStorage int|string */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/* @var $subAgents int|string */

use backend\modules\admin\widgets\yii\GridView;
use backend\modules\fridge\formatter\FridgeMonitoringFormatter;
use yii\widgets\Pjax;
use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = Yii::t('admin', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;

?>

    <div class="content-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col">
                <div class="info-box">
                    <span class="info-box-icon elevation-1 bg-green"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number text-nowrap"><?php echo Yii::t('admin', 'Sub agents') ?></span>
                        <span class="info-box-text"><?php echo $subAgents; ?></span>
                        <span class="info-box-text text-white">.</span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="info-box">
                    <span class="info-box-icon elevation-1 bg-green"><i class="fas fa-list"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number text-nowrap"><?php echo Yii::t('admin', 'Fridges') ?></span>
                        <div class="row">
                            <div class="col-6">
                                <span class="info-box-text"><?php echo Yii::t('admin', 'Count') . ': ' . $information['fridgeStatus']['count']; ?></span>
                                <span class="info-box-text"><?php echo Yii::t('admin', 'Inactive') . ': ' . $information['fridgeStatus']['inactive']; ?></span>
                            </div>
                            <div class="col-6">
                                <span class="info-box-text"><?php echo Yii::t('admin', 'Online') . ': ' . $information['fridgeStatus']['online']; ?></span>
                                <span class="info-box-text"><?php echo Yii::t('admin', 'Offline') . ': ' . $information['fridgeStatus']['offline']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="info-box">
                    <span class="info-box-icon elevation-1 bg-green"><i class="fas fa-shopping-cart"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number text-nowrap"><?php echo Yii::t('admin', 'Sales ') ?></span>
                        <div class="d-flex flex-wrap flex-sm-nowrap">
                            <div class="pr-1 mr-auto">
                                <span class="text-nowrap"><?php echo Yii::t('admin', 'This week') . ': ' . $date['thisWeek']; ?></span>
                                <div class="col-12"></div>
                                <span class="text-nowrap"><?php echo Yii::t('admin', 'This month') . ': ' . $date['thisMonth']; ?></span>
                            </div>
                            <div class="pl-1 mx-auto">
                                <span class="text-nowrap"><?php echo Yii::t('admin', 'Last week') . ': ' . $date['lastWeek']; ?></span>
                                <div class="col-12"></div>
                                <span class="text-nowrap"><?php echo Yii::t('admin', 'Last month') . ': ' . $date['lastMonth']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="info-box">
                    <span class="info-box-icon elevation-1 bg-green"><i class="far fa-list-alt"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number text-nowrap"><?php echo Yii::t('admin', 'Pending orders ') ?></span>
                        <span class="info-box-text"><?php echo Yii::t('admin', 'In fridge') . ': ' . $pendingOrdersFridge; ?></span>
                        <span class="info-box-text"><?php echo Yii::t('admin', 'In storage') . ': ' . $pendingOrdersStorage; ?></span>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($information['fridgeError'] || $countProductError['productError']) { ?>
            <div class="row">
                <div class="col">
                    <div class="card card-danger card-outline">
                        <div class="card-header">
                            <h3 class="card-title"><i
                                        class="fas fa-exclamation-triangle text-danger"></i> <?php echo Yii::t('admin', 'Alert') ?>
                            </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php if ($information['fridgeError']) { ?>
                                    <div class="col min-wr-19 mx-auto">
                                        <h3 class=""><?php echo Yii::t('site', 'MS STORE') . ' ' . Yii::t('admin', 'Alert(s)') . ' ' . count($information['fridgeError']) ?></h3>
                                        <div class="card collapsed-card elevation-1">
                                            <div class="card-header">
                                                <h3 class="card-title"><i
                                                            class="fas fa-inbox"></i> <?php echo Yii::t('site', 'MS STORE') ?>
                                                </h3>
                                                <div class="card-tools">
                                                    <i class="fas fa-exclamation-triangle text-danger"></i>
                                                    <button type="button" class="btn btn-tool"
                                                            data-card-widget="collapse">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="timeline">
                                                    <?php foreach ($information['fridgeError'] as $item) { ?>
                                                        <?php if ($item['error']['critical'] || $item['error']['uncritical']) : ?>
                                                        <div>
                                                            <i class="fas fa-inbox bg-yellow"></i>
                                                            <div class="timeline-item">
                                                                <span class="time pt-2"><a style="font-size: 1rem;"
                                                                                           href="<?= Url::toRoute(['/fridge/fridge']) ?>"><?php echo Yii::t('site', 'Link') . ' ' ?>
                                                                    <i class="fas fa-external-link-alt"></i></a>
                                                                </span>
                                                                <h3 class="timeline-header"><?php echo Yii::t('site', 'Fridge name') . ': ' ?><?php echo ucfirst($item['fridge_name']) ?></h3>
                                                                <div class="timeline-body">
                                                                    <?php if ($item['error']) { ?>
                                                                        <div class="row">
                                                                            <?php if (!empty($item['error']['critical'])) { ?>
                                                                                <div class="col">
                                                                                    <?php $i = 1; ?>
                                                                                    <?php foreach ($item['error']['critical'] as $items) { ?>
                                                                                        <p class="m-0"><i
                                                                                                    class="fas fa-exclamation-triangle text-danger"></i> <?php echo $i++ . '.' . ' ' . $items ?>
                                                                                        </p>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            <?php } ?>
                                                                            <?php if (!empty($item['error']['uncritical'])) { ?>
                                                                                <div class="col">
                                                                                    <?php $i = 1; ?>
                                                                                    <?php foreach ($item['error']['uncritical'] as $items) { ?>
                                                                                        <p class="m-0"><i
                                                                                                    class="fas fa-exclamation-circle text-warning"></i> <?php echo $i++ . '.' . ' ' . $items ?>
                                                                                        </p>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php endif; ?>
                                                    <?php } ?>
                                                    <div>
                                                        <i class="fas"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($countProductError['productError']) { ?>
                                    <div class="col min-wr-19 mx-auto" <?php if ($information['fridgeError']) { ?> style="border-left: 1px solid #e8e8e8;" <?php } ?>>
                                        <h3><?php echo Yii::t('site', 'Product') . ' ' . Yii::t('admin', 'Alert(s)') . ' ' . $countProductError['countProductError'] ?></h3>
                                        <?php foreach ($countProductError['productError'] as $itemss) { ?>
                                            <?php foreach ($itemss as $data) { ?>
                                                <?php if ($data == 'fridge' || $data == 'store') { ?>
                                                    <?php continue; ?>
                                                <?php } ?>
                                                <?php
                                                $noP = 1;
                                                $fridgeId = '';
                                                $storeId = '';
                                                ?>
                                                <div class="card collapsed-card elevation-1">
                                                    <div class="card-header">
                                                        <?php if ($itemss['location'] == 'fridge') { ?>
                                                            <h3 class="card-title"><i
                                                                        class="fas fa-inbox"></i> <?php echo Yii::t('site', 'Fridge name') . ': ' ?><?php echo ucfirst($data['locName']) ?>
                                                            </h3>
                                                            <?php $fridgeId = $data['locId'] ?>
                                                        <?php } else { ?>
                                                            <h3 class="card-title"><i
                                                                        class="fas fa-warehouse"></i> <?php echo Yii::t('site', 'Storage name') . ': ' ?><?php echo ucfirst($data['locName']) ?>
                                                            </h3>
                                                            <?php $storeId = $data['locId'] ?>
                                                        <?php } ?>
                                                        <div class="card-tools">
                                                            <span class=""><?php echo ucfirst(Yii::t('admin', 'Alert(s)')) . ' ' . count($data['error']) ?></span>
                                                            <button type="button" class="btn btn-tool"
                                                                    data-card-widget="collapse">
                                                                <i class="fas fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="timeline">
                                                            <?php foreach ($data['error'] as $item) { ?>
                                                                <div>
                                                                    <i class="fas fa-apple-alt bg-yellow"></i>
                                                                    <div class="timeline-item">
                                                                <span class="time pt-2"><a style="font-size: 1rem;"
                                                                                           href="<?= Url::toRoute(['/product/product/view-list', 'RfIdsSearch[store_id]' => $storeId, 'RfIdsSearch[fridge_id]' => $fridgeId, 'RfIdsSearch[status]' => $item['status'], 'id' => $item['product_id']]) ?>"><?php echo Yii::t('site', 'Link') . ' ' ?>
                                                                    <i class="fas fa-external-link-alt"></i></a>
                                                                </span>
                                                                        <h3 class="timeline-header"><?php echo ucfirst($item['product_name']) ?></h3>
                                                                        <div class="timeline-body d-flex justify-content-between">
                                                                            <p class="m-0"><i
                                                                                        class="fas fa-angle-double-right"></i><?php echo ' ' . $item['type'] ?>
                                                                            </p>
                                                                            <p class="m-0"><i
                                                                                        class="fas fa-exclamation-triangle text-danger"></i> <?php echo count($item['rfId']) ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <div>
                                                                <i class="fas"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col min-wr-19">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i
                                    class="fas fa-cart-arrow-down"></i> <?php echo Yii::t('admin', 'Sales') ?></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool"
                                    data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body my-h-block">
                        <div class="my-card-chart d-flex align-items-center">
                            <div class="w-100 my-chart-div">
                                <canvas class="my-chart-canvas w-100" id="myChart"></canvas>
                            </div>
                            <div class="my-legend col p-0" id="legend"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col min-wr-19 max-wr-30 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-inbox"></i> <?php echo Yii::t('admin', 'Fridges') ?>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool"
                                    data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body direct-chat-messages my-h-block min-hr-23">
                        <?php if (!empty($information['fridges'])) : ?>
                            <?php foreach ($information['fridges'] as $item) { ?>
                                <div class="info-box">
                                <span class="info-box-icon elevation-1 bg-blue"><img
                                            style="display: block; padding: 10px;"
                                            src="<?php echo Url::to('/img/system/icon/fridge.svg') ?>"></span>
                                    <div class="info-box-content">
                                        <span class="info-box-number"><?php echo $item->fridge_name ?></span>
                                        <?php if ($item->status == $item::STATUS_ACTIVE) { ?>
                                            <span class="badge badge-success"><?php echo Yii::t('admin', 'Online') ?></span>
                                        <?php } elseif ($item->status == $item::STATUS_INACTIVE) { ?>
                                            <span class="badge badge-danger"><?php echo Yii::t('admin', 'Offline') ?></span>
                                        <?php } else { ?>
                                            <span class="badge badge-warning"><?php echo Yii::t('admin', 'Inactive') ?>  </span>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endif ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i
                                    class="fas fa-align-left"></i> <?php echo Yii::t('admin', 'Transactions at last 7 days:') ?>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool"
                                    data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php Pjax::begin();
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    'id',
                                    'created_at',
                                    [
                                        'attribute' => 'fridge_unique_id',
                                        'value' => function ($data) {
                                            return Yii::$app->formatter->load(FridgeMonitoringFormatter::class)->asName($data['fridge_unique_id']);
                                        },
                                    ],
                                    'amount',
                                    'currency',
                                    'response_text',
                                    'receipt',
                                    'ref_card_no',
                                ],
                            ]);
                            Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

$js = "
    $(window).on('load', function () {
    
        $(function() {
	        $('.my-h-block').matchHeight();
        });
        
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [" . $lastWeekTransactions['lastSevenWeekDate'] . "],
                datasets: [" . $lastWeekTransactions['chartData'] . "]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,   
                legend: {
                display: false,
                    position: 'bottom',
                    labels: {
                        fontSize: 14,
                        padding: 10,
                        
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '',
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 10,
                        }
                    }]
                }
            }
        });
    
        var myLegendContainer = document.getElementById('legend');
        myLegendContainer.innerHTML = chart.generateLegend();
        var legendItems = myLegendContainer.getElementsByTagName('li');
        for (var i = 0; i < legendItems.length; i += 1) {
            legendItems[i].addEventListener('click', legendClickCallback, false);
        }
    
        function legendClickCallback(event) {
            event = event || window.event;
    
            var target = event.target || event.srcElement;
            while (target.nodeName !== 'LI') {
                target = target.parentElement;
            }
            var parent = target.parentElement;
            var index = Array.prototype.slice.call(parent.children).indexOf(target);

            if (chart.getDatasetMeta(index).hidden === null || chart.getDatasetMeta(index).hidden === false) {
                target.classList.add('hidden');
                chart.getDatasetMeta(index).hidden=true;
            } else {
                target.classList.remove('hidden');
                chart.getDatasetMeta(index).hidden=false;
            }
            chart.update();
        }
       
        chart.canvas.parentNode.style.height = '430px';
        
        function beforePrintHandler () {
            for (var id in Chart.instances) {
                Chart.instances[id].resize();
            }
        }
   
    });
    ";

$this->registerJs($js, yii\web\View::POS_END);
