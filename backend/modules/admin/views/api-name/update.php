<?php

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ApiName */

$this->title = Yii::t('app', 'Update Api Name: {name}', [
    'name' => $model->api_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Api'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->api_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="content-header">
	<?=     Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		'homeLink' => ['label' => Yii::t('admin', 'System')],
	])
	?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-network-wired"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>
