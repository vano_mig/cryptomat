<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Currency */

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('currency', 'Create Currency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-euro-sign"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
                        <?php echo $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
