<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\admin\models\Currency;
use backend\modules\admin\widgets\yii\Breadcrumbs;

$this->title = Yii::t('currency', 'Currencies');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-euro-sign"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
                <?php if (Yii::$app->user->can('currency.create')): ?>
                        <?php echo Html::a(Yii::t('admin', 'Add currency'),
                            Url::toRoute(['create']), ['class' => 'btn bg-gradient-success']) ?>
                <?php endif ?>
            <div class="table-responsive">
                    <?php echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                  [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                            'name',
                            'code',
                            [
                                'attribute' => 'system_currency',
                                'value' => function ($data) {
                                    return CurrencyFormatter::currencyGetNameById($data->system_currency);
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'system_currency', $searchModel->listStatuses(),
                                    ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                                'headerOptions' => ['class' => 'text-center my-tw-9'],
                                'contentOptions' => ['class' => 'text-center align-middle'],
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
									return CurrencyFormatter::currencyGetStatus($data->status);
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->listStatuses(), ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                                'format' => 'raw',
                                'headerOptions' => ['class' => 'text-center my-tw-9'],
                                'contentOptions' => ['class' => 'text-center align-middle'],
                            ],
                            [
                                'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                                'header' => Yii::t('admin', 'Actions'),
                                'template' => '{view} {update} {delete} {active}',
                                'buttons' => [
                                    'active' => function ($url, $model, $key) {
                                        if ($model->status == (new Currency())::STATUS_ACTIVE) {
                                            return Html::a('<i class="fas fa-star"></i>', $url, [
                                                'class' => 'mx-2 text-success',
                                                'title' => Yii::t('admin', 'Deactivate'),
                                                'aria-label' => Yii::t('admin', 'Lock'),
                                                'data-pjax' => Yii::t('admin', 'Lock'),
                                            ]);
                                        } else {
                                            return Html::a('<i class="far fa-star"></i>', $url, [
                                                'class' => 'mx-2 text-secondary',
                                                'title' => Yii::t('admin', 'Activate'),
                                                'aria-label' => Yii::t('admin', 'Unlock'),
                                                'data-pjax' => Yii::t('admin', 'Unlock'),
                                            ]);
                                        }
                                    },
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('<i class="far fa-eye"></i>', $url, [
                                            'class' => 'mx-2 text-info',
                                            'title' => Yii::t('admin', 'View'),
                                            'aria-label' => Yii::t('admin', 'View'),
                                            'data-pjax' => Yii::t('admin', 'View'),
                                        ]);
                                    },
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                            'class' => 'mx-2 text-warning',
                                            'title' => Yii::t('admin', 'Update'),
                                            'aria-label' => Yii::t('admin', 'Update'),
                                            'data-pjax' => Yii::t('admin', 'Update'),
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                            'class' => 'mx-2 text-dark',
                                            'title' => Yii::t('admin', 'Delete'),
                                            'aria-label' => Yii::t('admin', 'Delete'),
                                            'data-pjax' => Yii::t('admin', 'Delete'),
                                            'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                        ]);
                                    },
                                ],
                                'visibleButtons' => [
                                    'active' => function ($model, $key, $index) {
                                        return Yii::$app->user->can('currency.update', ['user' => $model]);
                                    },
                                    'view' => function ($model, $key, $index) {
                                        return Yii::$app->user->can('currency.view', ['user' => $model]);
                                    },
                                    'update' => function ($model, $key, $index) {
                                        return Yii::$app->user->can('currency.update', ['user' => $model]);
                                    },
                                    'delete' => function ($model, $key, $index) {
                                        return Yii::$app->user->can('currency.delete', ['user' => $model]);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
            </div>
        </div>
    </div>
</div>
