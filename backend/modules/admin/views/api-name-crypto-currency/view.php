<?php

use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\admin\formatter\CurrencyFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ApiNameCryptoCurrency */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Api crypto currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-btc"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'crypto_currency',
                                'value' => function ($data) {
                                    return CurrencyFormatter::cryptoCurrencyGetNameById($data->active);
                                },
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'api_id',
                                'value' => function ($data) {
                                    return ApiFormatter::apiGetNameById($data->api_id);
                                },
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'active',
                                'value' => function ($data) {
                                    return ApiFormatter::apiCryptoCurrencyGetStatus($data->active);
                                },
                                'format' => 'html',
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('api.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('api.delete')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('api.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?= Html::a(Yii::t('app', 'List'), ['index', 'id' => $model->id], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

