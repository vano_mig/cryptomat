<?php

use backend\modules\admin\models\ApiName;
use backend\modules\admin\models\CryptoCurrency;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ApiNameCryptoCurrency */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<div class="col mx-auto max-wr-40">

	<?php echo $form->field($model, 'api_id')->dropDownList((new ApiName())->getList(), ['prompt' => Yii::t('admin', 'Select Api')]) ?>

	<?php echo $form->field($model, 'crypto_currency')->dropDownList(CryptoCurrency::getCurrencyList(), ['prompt' => Yii::t('admin', 'Select Crypto')]) ?>

	<?php echo $form->field($model, 'active')->dropDownList($model->listStatuses(), ['prompt' => Yii::t('admin', 'Select Status')]) ?>

</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

		<?php  if (Yii::$app->user->can('api.create') || Yii::$app->user->can('api.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>

		<?php  endif; ?>

		<?php  if (Yii::$app->user->can('api.listview')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>

		<?php  endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
