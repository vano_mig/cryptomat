<?php

use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\admin\formatter\CurrencyFormatter;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\ApiNameCryptoCurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Apis crypto currencies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-btc"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('api.create')): ?>
                <?php echo Html::a(Yii::t('app', 'Create api crypto currency'),
                    ['create'], ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'crypto_currency',
                            'value' => function ($data) {
                                return CurrencyFormatter::cryptoCurrencyGetNameById($data->crypto_currency);
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'api_id',
                            'value' => function ($data) {
                                return ApiFormatter::apiGetNameById($data->api_id);
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'active',
                            'value' => function ($data) {
                                return ApiFormatter::apiCryptoCurrencyGetStatus($data->active);
                            },
                            'format' => 'html',
                        ],
                        ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'buttons' => [
                                'view' => function ($url, $models, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('admin',
                                                'Are you sure you want to delete this item?'),
                                            'method' => 'POST'
                                        ],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($models, $key, $index) {
                                    return Yii::$app->user->can('api.view', ['user' => $models]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('api.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('api.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>