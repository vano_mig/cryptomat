<?php

use backend\modules\admin\formatter\ApiFormatter;
use backend\modules\admin\models\ApiName;
use backend\modules\admin\models\ApiNameCurrency;
use backend\modules\admin\models\Currency;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\ApiNameCurrency */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<div class="col mx-auto max-wr-40">

		<?php echo $form->field($model, 'api_id')->dropDownList(ApiFormatter::apiGetAllByType('exchange'), ['prompt' => Yii::t('admin', 'Select Api')]) ?>

		<?php

        if ($model->isNewRecord) {
        echo $form->field($model, 'currency')->widget(Select2::class, [
			'data' => (new Currency)->getListCode(),
			'options' => ['placeholder' => Yii::t('admin', 'Select currency')],
			'pluginOptions' => [
				'allowClear' => true,
                'multiple' => true
			],
			'theme' => 'default',
		]);
		} else {
			echo $form->field($model, 'currency')->dropDownList((new Currency())->getListCode(), ['prompt' => Yii::t('admin', 'Select currency')]);
        }
        ?>


		<?php echo $form->field($model, 'active')->dropDownList(ApiNameCurrency::listStatuses(), ['prompt' => Yii::t('admin', 'Select status')]) ?>


</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

		<?php  if (Yii::$app->user->can('gii.create') || Yii::$app->user->can('gii.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>

		<?php  endif; ?>

		<?php  if (Yii::$app->user->can('gii.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>

		<?php  endif; ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
