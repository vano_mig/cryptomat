<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator backend\modules\gii\src\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>


<?= "<?php " ?>$form = ActiveForm::begin(); ?>

<div class="col mx-auto max-wr-40">

    <?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
    } ?>

</div>

<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">

		<?= "<?php " ?> if (Yii::$app->user->can('gii.create') || Yii::$app->user->can('gii.update')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= "<?= " ?>Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>

		<?= "<?php " ?> endif; ?>

		<?= "<?php " ?> if (Yii::$app->user->can('gii.list')): ?>

            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
				<?= "<?= " ?>Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
            </div>

		<?= "<?php " ?> endif; ?>

    </div>
</div>

<?= "<?php " ?>ActiveForm::end(); ?>
