<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator backend\modules\gii\src\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\bootstrap4\Html;
use <?= $generator->indexWidgetType === 'grid' ? "backend\\modules\\admin\\widgets\\yii\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
	<?= "<?= " ?>
    Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    'homeLink' => ['label' => Yii::t('gii', 'NAME')],
    ])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-import"></i> <?= "<?= " ?>Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
			<?php if (Yii::$app->user->can('gii.create')): ?>
				<?= "<?= " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
			<?php endif ?>
            <div class="table-responsive">

				<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
				<?php if(!empty($generator->searchModelClass)): ?>
					<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
				<?php endif; ?>

				<?php if ($generator->indexWidgetType === 'grid'): ?>
					<?= "<?= " ?>GridView::widget([
                    'dataProvider' => $dataProvider,
					<?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
                    ['class' => 'yii\grid\SerialColumn'],

					<?php
					$count = 0;
					if (($tableSchema = $generator->getTableSchema()) === false) {
						foreach ($generator->getColumnNames() as $name) {
							if (++$count < 6) {
								echo "            '" . $name . "',\n";
							} else {
								echo "            //'" . $name . "',\n";
							}
						}
					} else {
						foreach ($tableSchema->columns as $column) {
							$format = $generator->generateColumnFormat($column);
							if (++$count < 6) {
								echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
							} else {
								echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
							}
						}
					}
					?>
                    ['class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                        'buttons' => [
                            'view' => function ($url, $models, $key) {
                                return Html::a('<i class="far fa-eye"></i>', $url, [
                                    'class' => 'mx-2 text-info',
                                    'title' => Yii::t('admin', 'View'),
                                    'aria-label' => Yii::t('admin', 'View'),
                                    'data-pjax' => Yii::t('admin', 'View'),
                                ]);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                    'class' => 'mx-2 text-warning',
                                    'title' => Yii::t('admin', 'Update'),
                                    'aria-label' => Yii::t('admin', 'Update'),
                                    'data-pjax' => Yii::t('admin', 'Update'),
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                    'class' => 'mx-2 text-dark',
                                    'title' => Yii::t('admin', 'Delete'),
                                    'aria-label' => Yii::t('admin', 'Delete'),
                                    'data-pjax' => Yii::t('admin', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('admin',
                                        'Are you sure you want to delete this item?'),
                                        'method' => 'POST'
                                    ],
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'view' => function ($models, $key, $index) {
                                return \Yii::$app->user->can('gii.view', ['user' => $models]);
                            },
                            'update' => function ($model, $key, $index) {
                                return \Yii::$app->user->can('gii.update', ['user' => $model]);
                            },
                            'delete' => function ($model, $key, $index) {
                                return \Yii::$app->user->can('gii.delete', ['user' => $model]);
                            },
                        ],
                    ],
                ],
                ]); ?>
				<?php else: ?>
					<?= "<?= " ?>ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                    return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
                    ]) ?>
				<?php endif; ?>

				<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>

            </div>
        </div>
    </div>
</div>