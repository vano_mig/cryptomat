<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $generator backend\modules\gii\src\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\bootstrap4\Html;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content-header">
	<?= "<?= " ?>
	Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		'homeLink' => ['label' => Yii::t('gii', 'NAME')],
	])
    ?>
</div>

<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-inbox"></i> <?= "<?= " ?>Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">

                    <?= "<?= " ?>DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            <?php
                            if (($tableSchema = $generator->getTableSchema()) === false) {
                                foreach ($generator->getColumnNames() as $name) {
                                    echo "            '" . $name . "',\n";
                                }
                            } else {
                                foreach ($generator->getTableSchema()->columns as $column) {
                                    $format = $generator->generateColumnFormat($column);
                                    echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                                }
                            }
                            ?>
                        ],
                    ]) ?>

                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">

					<?= "<?php " ?> if (Yii::$app->user->can('gii.update')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
							<?= "<?= " ?>Html::a(<?= $generator->generateString('Update') ?>, ['update', <?= $urlParams ?>], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>

					<?= "<?php " ?> endif; ?>

					<?= "<?php " ?> if (Yii::$app->user->can('gii.delete')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
							<?= "<?= " ?>Html::a(<?= $generator->generateString('Delete') ?>, ['delete', <?= $urlParams ?>], [
                            'class' => 'btn bg-gradient-danger btn-block',
                            'data' => [
                            'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
                            'method' => 'post',
                            ],
                            ]) ?>
                        </div>

					<?= "<?php " ?> endif; ?>

					<?= "<?php " ?> if (Yii::$app->user->can('gii.list')): ?>

                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
							<?= "<?= " ?>Html::a(<?= $generator->generateString('List') ?>, ['index', <?= $urlParams ?>], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>

					<?= "<?php " ?> endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>

