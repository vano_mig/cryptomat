<?php

namespace backend\modules\pkpass\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\pkpass\models\PkpassTemplate;

/**
 * PkpassTemplateSearch represents the model behind the search form of `backend\modules\pkpass\models\PkpassTemplate`.
 */
class PkpassTemplateSearch extends PkpassTemplate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['status', 'template_name', 'description', 'organization_name', 'pass_type_identifier', 'team_identifier', 'relevant_date', 'expiration_date', 'style_key', 'beacons', 'locations', 'barcode', 'background_color', 'foreground_color', 'label_color', 'logo_text', 'nfc', 'auxiliary_fields', 'back_fields', 'header_fields', 'primary_fields', 'secondary_fields'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PkpassTemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'relevant_date' => $this->relevant_date,
            'expiration_date' => $this->expiration_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'template_name', $this->template_name])
            ->andFilterWhere(['like', 'organization_name', $this->organization_name])
            ->andFilterWhere(['like', 'pass_type_identifier', $this->pass_type_identifier])
            ->andFilterWhere(['like', 'team_identifier', $this->team_identifier])
            ->andFilterWhere(['like', 'style_key', $this->style_key])
            ->andFilterWhere(['like', 'beacons', $this->beacons])
            ->andFilterWhere(['like', 'locations', $this->locations])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'background_color', $this->background_color])
            ->andFilterWhere(['like', 'foreground_color', $this->foreground_color])
            ->andFilterWhere(['like', 'label_color', $this->label_color])
            ->andFilterWhere(['like', 'logo_text', $this->logo_text])
            ->andFilterWhere(['like', 'nfc', $this->nfc])
            ->andFilterWhere(['like', 'auxiliary_fields', $this->auxiliary_fields])
            ->andFilterWhere(['like', 'back_fields', $this->back_fields])
            ->andFilterWhere(['like', 'header_fields', $this->header_fields])
            ->andFilterWhere(['like', 'primary_fields', $this->primary_fields])
            ->andFilterWhere(['like', 'secondary_fields', $this->secondary_fields]);

        return $dataProvider;
    }
}
