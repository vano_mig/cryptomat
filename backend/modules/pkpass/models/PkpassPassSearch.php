<?php

namespace backend\modules\pkpass\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\pkpass\models\PkpassPass;

/**
 * PkpassPassSearch represents the model behind the search form of `backend\modules\pkpass\models\PkpassPass`.
 */
class PkpassPassSearch extends PkpassPass
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id','template_id', 'status'], 'integer'],
            [['serial_number', 'user_info', 'relevant_date', 'expiration_date', 'authentication_token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PkpassPass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'template_id' => $this->template_id,
            'relevant_date' => $this->relevant_date,
            'expiration_date' => $this->expiration_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere(['like', 'user_info', $this->user_info])
            ->andFilterWhere(['like', 'authentication_token', $this->authentication_token]);

        return $dataProvider;
    }
}
