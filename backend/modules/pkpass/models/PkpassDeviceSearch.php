<?php

namespace backend\modules\pkpass\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\pkpass\models\PkpassDevice;

/**
 * PkpassDeviceSearch represents the model behind the search form of `backend\modules\pkpass\models\PkpassDevice`.
 */
class PkpassDeviceSearch extends PkpassDevice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pass_id'], 'integer'],
            [['application', 'device_token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PkpassDevice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pass_id' => $this->pass_id,
        ]);

        $query->andFilterWhere(['like', 'application', $this->application])
            ->andFilterWhere(['like', 'device_token', $this->device_token]);

        return $dataProvider;
    }
}
