<?php

namespace backend\modules\pkpass\models;

use Yii;

/**
 * This is the model class for table "pkpass_device".
 *
 * @property int $id
 * @property string|null $pass_type_identifier
 * @property int|null $pass_id
 * @property string|null $application
 * @property string|null $device_token
 * @property boolean|null $update
 * @property PkpassPass $pkpassPass
 */

class PkpassDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pkpass_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pass_id'], 'integer'],
            [['update'], 'boolean'],
            [['application', 'device_token','pass_type_identifier'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pkpassDevice', 'ID'),
            'pass_id' => Yii::t('pkpassDevice', 'Pass ID'),
            'application' => Yii::t('pkpassDevice', 'Application'),
            'device_token' => Yii::t('pkpassDevice', 'Device Token'),
        ];
    }

    public function getPkpassPass()
    {
        return $this->hasOne(PkpassPass::class, ['id' => 'pass_id']);
    }

}
