<?php

/**
 * Copyright (c) 2017, Thomas Schoffelen BV.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

namespace backend\modules\pkpass\models;

use yii\base\Model;
use ZipArchive;
use Yii;

/**
 * Class PKPass.
 */
class PkpassService extends Model
{
    /**
     * Holds the path to the certificate
     * Variable: string.
     */
    protected $certPath;

    /**
     * Name of the downloaded file.
     */
    protected $name;

    /**
     * Holds the files to include in the .pkpass
     * Variable: array.
     */
    protected $files = [];

    /**
     * Holds the remote file urls to include in the .pkpass
     * Variable: array.
     */
    protected $remote_file_urls = [];

    /**
     * Holds the json
     * Variable: class.
     */
    protected $json;

    /**
     * Holds the SHAs of the $files array
     * Variable: array.
     */
    protected $shas;

    /**
     * Holds the password to the certificate
     * Variable: string.
     */
    protected $certPass = '';

    /**
     * Holds the path to the WWDR Intermediate certificate
     * Variable: string.
     */

    protected $wwdrCertPath = '';

    /**
     * Holds the path to a temporary folder with trailing slash.
     */
    protected $tempPath;

    /**
     * Holds error info if an error occurred.
     */
    private $sError = '';

    /**
     * Holds a auto-generated uniqid to prevent overwriting other processes pass
     * files.
     */
    private $uniqid = null;

    /**
     * Holds array of localization details
     * Variable: array.
     */
    protected $locales = [];

    /**
     * PKPass constructor.
     */
    public function __construct()
    {
        $this->tempPath = Yii::getAlias('@backend') . '/web/pkpass/temp/';  // Must end with slash!
        $this->wwdrCertPath = Yii::getAlias('@backend') . '/web/pkpass/certificate/AWDRCA.pem';
        $this->certPath = Yii::getAlias('@backend') . '/web/pkpass/certificate/pass-cryptomatic-key.p12';
        $this->certPass = 'Pass.cryptomatic.Pass';
    }

    /**
     * Set pass data.
     *
     * @param string|array $data
     * @return bool
     */
    public function setData($data)
    {
        if (is_array($data)) {
            $this->json = json_encode($data);

            return true;
        }

        if (json_decode($data) !== false) {
            $this->json = $data;

            return true;
        }

        $this->sError = 'This is not a JSON string.';

        return false;
    }

    /**
     * Add dictionary of strings for transilation.
     *
     * @param string $language language project need to be added
     * @param array $strings key value pair of transilation strings
     *     (default is equal to [])
     * @return bool
     */
    public function addLocaleStrings($language, $strings = [])
    {
        if (!is_array($strings) || empty($strings)) {
            $this->sError = "Translation strings empty or not an array";

            return false;
        }
        $dictionary = "";
        foreach ($strings as $key => $value) {
            $dictionary .= '"' . $key . '" = "' . $value . '";' . PHP_EOL;
        }
        $this->locales[$language] = $dictionary;

        return true;
    }

    /**
     * Add a file to the file array.
     *
     * @param string $language language for which file to be added
     * @param string $path Path to file
     * @param string $name Filename to use in pass archive
     *     (default is equal to $path)
     * @return bool
     */
    public function addLocaleFile($language, $path, $name = null)
    {
        if (file_exists($path)) {
            $name = ($name === null) ? basename($path) : $name;
            $this->files[$language . '.lproj/' . $name] = $path;

            return true;
        }

        $this->sError = sprintf('File %s does not exist.', $path);

        return false;
    }

    /**
     * Add a file to the file array.
     *
     * @param string $path Path to file
     * @param string $name Filename to use in pass archive
     *     (default is equal to $path)
     * @return bool
     */
    public function addFile($path, $name = null)
    {
        if (file_exists($path)) {
            $name = ($name === null) ? basename($path) : $name;
            $this->files[$name] = $path;

            return true;
        }

        $this->sError = sprintf('File %s does not exist.', $path);

        return false;
    }

    /**
     * Add a file from a url to the remote file urls array.
     *
     * @param string $url URL to file
     * @param string $name Filename to use in pass archive
     *     (default is equal to $url)
     * @return bool
     */
    public function addRemoteFile($url, $name = null)
    {
        $name = ($name === null) ? basename($url) : $name;
        $this->remote_file_urls[$name] = $url;

        return true;
    }

    /**
     * Add a locale file from a url to the remote file urls array.
     *
     * @param string $language language for which file to be added
     * @param string $url URL to file
     * @param string $name Filename to use in pass archive
     *     (default is equal to $url)
     * @return bool
     */
    public function addLocaleRemoteFile($language, $url, $name = null)
    {
        $name = ($name === null) ? basename($url) : $name;
        $this->remote_file_urls[$language . '.lproj/' . $name] = $url;

        return true;
    }

    /**
     * Create the actual .pkpass file.
     *
     * @param bool $output Whether to output it directly or return the pass
     *     contents as a string.
     *
     * @return bool|string
     */
    public function create()
    {
        $paths = $this->getTempPaths();

        if (!($manifest = $this->createManifest())) {
            $this->clean();

            return false;
        }

        if ($this->createSignature($manifest) == false) {
            $this->clean();

            return false;
        }

        if ($this->createZip($manifest) == false) {
            $this->clean();

            return false;
        }

        if (!file_exists($paths['pkpass']) || filesize($paths['pkpass']) < 1) {
            $this->sError = 'Error while creating pass.pkpass. Check your ZIP extension.';
            $this->clean();

            return false;
        }

        $file = file_get_contents($paths['pkpass']);
        $this->clean();

        return $file;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $error
     *
     * @return bool
     */
    public function checkError(&$error)
    {
        if (trim($this->sError) == '') {
            return false;
        }

        $error = $this->sError;

        return true;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->sError;
    }

    /**
     * Sub-function of create()
     * This function creates the hashes for the files and adds them into a json
     * string.
     */
    protected function createManifest()
    {
        $manifest = null;
        $this->shas['pass.json'] = sha1($this->json);
        foreach ($this->locales as $language => $strings) {
            $this->shas[$language . '.lproj/pass.strings'] = sha1($strings);
        }
        $has_icon = false;
        foreach ($this->files as $name => $path) {
            if (strtolower($name) == 'icon.png') {
                $has_icon = true;
            }
            $this->shas[$name] = sha1(file_get_contents($path));
        }
        foreach ($this->remote_file_urls as $name => $url) {
            if (strtolower($name) == 'icon.png') {
                $has_icon = true;
            }
            $this->shas[$name] = sha1(file_get_contents($url));
        }
        if (!$has_icon) {
            $this->sError = 'Missing required icon.png file.';
            $this->clean();

            return false;
        }

        $manifest = json_encode((object)$this->shas);

        return $manifest;
    }

    /**
     * Converts PKCS7 PEM to PKCS7 DER
     * Parameter: string, holding PKCS7 PEM, binary, detached
     * Return: string, PKCS7 DER.
     *
     * @param $signature
     *
     * @return string
     */
    protected function convertPEMtoDER($signature)
    {
        $begin = 'filename="smime.p7s"';
        $end = '------';
        $signature = substr($signature, strpos($signature, $begin) + strlen($begin));

        $signature = substr($signature, 0, strpos($signature, $end));
        $signature = trim($signature);
        $signature = base64_decode($signature);

        return $signature;
    }

    /**
     * Creates a signature and saves it
     * Parameter: json-string, manifest file
     * Return: boolean, true on success, false on failure.
     *
     * @param $manifest
     *
     * @return bool
     */
    protected function createSignature($manifest)
    {
        $paths = $this->getTempPaths();

        file_put_contents($paths['manifest'], $manifest);

        if (!$pkcs12 = file_get_contents($this->certPath)) {
            $this->sError = 'Could not read the certificate';

            return false;
        }

        $certs = [];
        if (!openssl_pkcs12_read($pkcs12, $certs, $this->certPass)) {
            $this->sError = 'Invalid certificate file. Make sure you have a ' .
                'P12 certificate that also contains a private key, and you ' .
                'have specified the correct password!';

            return false;
        }
        $certdata = openssl_x509_read($certs['cert']);
        $privkey = openssl_pkey_get_private($certs['pkey'], $this->certPass);

        $openssl_args = [
            $paths['manifest'],
            $paths['signature'],
            $certdata,
            $privkey,
            [],
            PKCS7_BINARY | PKCS7_DETACHED
        ];

        if (!empty($this->wwdrCertPath)) {
            if (!file_exists($this->wwdrCertPath)) {
                $this->sError = 'WWDR Intermediate Certificate does not exist';

                return false;
            }

            $openssl_args[] = $this->wwdrCertPath;
        }

        call_user_func_array('openssl_pkcs7_sign', $openssl_args);

        $signature = file_get_contents($paths['signature']);
        $signature = $this->convertPEMtoDER($signature);
        file_put_contents($paths['signature'], $signature);

        return true;
    }

    /**
     * Creates .pkpass (zip archive)
     * Parameter: json-string, manifest file
     * Return: boolean, true on succes, false on failure.
     *
     * @param $manifest
     *
     * @return bool
     */
    protected function createZip($manifest)
    {
        $paths = $this->getTempPaths();

        $zip = new ZipArchive();
        if (!$zip->open($paths['pkpass'], ZipArchive::CREATE)) {
            $this->sError = 'Could not open ' . basename($paths['pkpass']) . ' with ZipArchive extension.';

            return false;
        }
        $zip->addFile($paths['signature'], 'signature');
        $zip->addFromString('manifest.json', $manifest);
        $zip->addFromString('pass.json', $this->json);
        foreach ($this->locales as $language => $strings) {
            if (!$zip->addEmptyDir($language . '.lproj')) {
                $this->sError = 'Could not create ' . $language . '.lproj folder in zip archive.';

                return false;
            }
            $zip->addFromString($language . '.lproj/pass.strings', $strings);
        }
        foreach ($this->files as $name => $path) {
            $zip->addFile($path, $name);
        }
        foreach ($this->remote_file_urls as $name => $url) {
            $download_file = file_get_contents($url);
            $zip->addFromString($name, $download_file);
        }

        $zip->close();

        return true;
    }

    /**
     * Declares all paths used for temporary files.
     */
    protected function getTempPaths()
    {
        $paths = [
            'pkpass' => 'pass.pkpass',
            'signature' => 'signature',
            'manifest' => 'manifest.json',
        ];
        if (substr($this->tempPath, -1) != '/') {
            $this->tempPath = $this->tempPath . '/';
        }
        if (empty($this->uniqid)) {
            $this->uniqid = uniqid('PKPass', true);
        }
        if (!is_dir($this->tempPath . $this->uniqid)) {
            mkdir($this->tempPath . $this->uniqid, 755, true);
        }
        foreach ($paths as $pathName => $path) {
            $paths[$pathName] = $this->tempPath . $this->uniqid . '/' . $path;
        }

        return $paths;
    }

    /**
     * Removes all temporary files.
     */
    protected function clean()
    {
        $paths = $this->getTempPaths();

        foreach ($paths as $path) {
            if (file_exists($path)) {
                unlink($path);
            }
        }

        if (is_dir($this->tempPath . $this->uniqid)) {
            rmdir($this->tempPath . $this->uniqid);
        }

        return true;
    }
}
