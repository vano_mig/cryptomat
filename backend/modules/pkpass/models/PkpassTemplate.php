<?php

namespace backend\modules\pkpass\models;

use backend\modules\user\models\User;
use backend\modules\user\models\UserProfile;
use Yii;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "pkpass_template".
 *
 * @property int $id
 * @property int $status
 * @property string|null $company_id
 * @property string|null $description
 * @property string|null $template_name
 * @property string|null $organization_name
 * @property string|null $pass_type_identifier
 * @property string|null $team_identifier
 * @property string|null $relevant_date
 * @property string|null $expiration_date
 * @property string|null $style_key
 * @property string|null $beacons
 * @property string|null $locations
 * @property string|null $barcode
 * @property string|null $background_color
 * @property string|null $foreground_color
 * @property string|null $label_color
 * @property string|null $logo_text
 * @property string|null $nfc
 * @property string|null $auxiliary_fields
 * @property string|null $back_fields
 * @property string|null $header_fields
 * @property string|null $primary_fields
 * @property string|null $secondary_fields
 * @property string|null $pass_serial_number
 * @property string|null $pass_user_info
 * @property string|null $pass_relevant_date
 * @property string|null $pass_expiration_date
 * @property string|null $pass_status
 * @property string|null $pass_authentication_token
 * @property string|null $logo
 * @property string|null $icon
 * @property string|null $strip
 * @property UserProfile $userProfile
 * @property PkpassPass $pkpassPasses
 * @property User $user
 *
 */
class PkpassTemplate extends \yii\db\ActiveRecord
{
//PKBarcodeFormatCode128
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '5';
    const STATUS_BLOCKED = '10';

    const FORMAT_VERSION = 1;
    const STYLE_KEY = ['boardingPass', 'coupon', 'eventTicket', 'generic', 'storeCard'];
    const TEMP_STYLE_KEY = 'storeCard';
    const BARCODE = ['PKBarcodeFormatQR' => 'QR', 'PKBarcodeFormatPDF417' => 'PDF 417', 'PKBarcodeFormatAztec' => 'Aztec'];
    const PASS_TYPE_IDENTIFIER = 'pass.cryptomatic';
    const TEAM_INDENTIFIER = 'Z8X6XB5Y6A';
    const MESSAGE_ENCODING = 'iso-8859-1';
    const WEB_SERVISE_URL = '/pkpass/pkpass-web-service';
    const DEFAULT_GRAFPHIC = '/pkpass/default/img/';

    public $pass_serial_number;
    public $pass_user_info;
    public $pass_relevant_date;
    public $pass_expiration_date;
    public $pass_status;
    public $pass_authentication_token;
    public $user;
    public $userProfile;
    public $fields_id;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pkpass_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'organization_name', 'team_identifier', 'pass_type_identifier', 'barcode', 'template_name'], 'required'],
            [['company_id'], 'integer'],
            [['logo', 'icon', 'strip'], 'file', 'extensions' => 'png'],
            [['relevant_date', 'expiration_date', 'status', 'auxiliary_fields', 'back_fields', 'header_fields', 'primary_fields', 'secondary_fields'], 'safe'],
            [['beacons', 'locations', 'barcode', 'nfc', 'template_name'], 'string'],
            [['description', 'organization_name', 'pass_type_identifier', 'team_identifier', 'style_key', 'background_color', 'foreground_color', 'label_color', 'logo_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pkpassTemplate', 'ID'),
            'status' => Yii::t('pkpassTemplate', 'Status'),
            'template_name' => Yii::t('pkpassTemplate', 'Template Name'),
            'company_id' => Yii::t('pkpassTemplate', 'Company'),
            'description' => Yii::t('pkpassTemplate', 'Description'),
            'organization_name' => Yii::t('pkpassTemplate', 'Organization Name'),
            'pass_type_identifier' => Yii::t('pkpassTemplate', 'Pass Type Identifier'),
            'team_identifier' => Yii::t('pkpassTemplate', 'Team Identifier'),
            'relevant_date' => Yii::t('pkpassTemplate', 'Relevant Date'),
            'expiration_date' => Yii::t('pkpassTemplate', 'Expiration Date'),
            'style_key' => Yii::t('pkpassTemplate', 'Style Key'),
            'beacons' => Yii::t('pkpassTemplate', 'Beacons'),
            'locations' => Yii::t('pkpassTemplate', 'Locations'),
            'barcode' => Yii::t('pkpassTemplate', 'Barcode'),
            'background_color' => Yii::t('pkpassTemplate', 'Background Color'),
            'foreground_color' => Yii::t('pkpassTemplate', 'Foreground Color'),
            'label_color' => Yii::t('pkpassTemplate', 'Label Color'),
            'logo_text' => Yii::t('pkpassTemplate', 'Logo Text'),
            'nfc' => Yii::t('pkpassTemplate', 'NFC'),
            'auxiliary_fields' => Yii::t('pkpassTemplate', 'Auxiliary Fields'),
            'back_fields' => Yii::t('pkpassTemplate', 'Back Fields'),
            'header_fields' => Yii::t('pkpassTemplate', 'Header Fields'),
            'primary_fields' => Yii::t('pkpassTemplate', 'Primary Fields'),
            'secondary_fields' => Yii::t('pkpassTemplate', 'Secondary Fields'),
        ];
    }

    /**
     * Upload file
     * @param $file string
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function saveImage($file, $template_id)
    {

        $path = Yii::getAlias('@backend') . '/web/pkpass/template/' . $template_id . '/';
        $imageSize = getimagesize($_FILES['PkpassTemplate']['tmp_name'][$file]);

        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                return false;
            }
        }
//        Image::getImagine()->open($_FILES['PkpassTemplate']['tmp_name'][$file])->save($path . "1122.png", ['quality' => 50]);
        if ($file == 'icon') {
            if ($imageSize[0] < 58 || $imageSize[0] != $imageSize[1]) {
                $this->addError($file, Yii::t('pkpassTemplate', 'Minimum image size') . ' 58x58 px');
            } else {
                if ($imageSize[0] >= 87) {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 87, 87)->save($path . $file . '@3x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 58, 58)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 29, 29)->save($path . $file . '.png', ['quality' => 50]);
                } else {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 58, 58)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 29, 29)->save($path . $file . '.png', ['quality' => 50]);
                }
            }
        } elseif ($file == 'logo') {
            if ($imageSize[0] < 100 || $imageSize[0] != $imageSize[1]) {
                $this->addError($file, Yii::t('pkpassTemplate', 'Minimum image size') . ' 100x100 px');
            } else {
                if ($imageSize[0] >= 150) {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 150, 150)->save($path . $file . '@3x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 100, 100)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 50, 50)->save($path . $file . '.png', ['quality' => 50]);
                } else {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 100, 100)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 50, 50)->save($path . $file . '.png', ['quality' => 50]);
                }
            }
        } elseif ($file == 'strip') {
            if ($imageSize[0] < 500 || $imageSize[1] < 200) {
                $this->addError($file, Yii::t('pkpassTemplate', 'Minimum image size') . ' 500x200 px');
            } else {
                if ($imageSize[0] >= 750 || $imageSize[1] >= 300) {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 750, 300)->save($path . $file . '@3x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 500, 200)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 250, 100)->save($path . $file . '.png', ['quality' => 50]);
                } else {
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 500, 200)->save($path . $file . '@2x.png', ['quality' => 50]);
                    Image::resize($_FILES['PkpassTemplate']['tmp_name'][$file], 250, 100)->save($path . $file . '.png', ['quality' => 50]);
                }
            }
        }


        if (file_exists($path . $file . '.png')) {
            $this->$file = '/pkpass/template/' . $template_id . '/' . $file;
        } else {
            return false;
        }


    }

    public function sendField($id)
    {
        $array = [
            'us_username' => Yii::t('site', 'User First Name'),
            'us_last_name' => Yii::t('site', 'User Last Name'),
            'pr_first_name_card' => Yii::t('site', 'Credit Card First Name'),
            'pr_last_name_card' => Yii::t('site', 'Credit Card Last Name'),
        ];
        if ($id)
            return $array[$id];
        return $array;
    }

    public function checkValueInverted($value)
    {
        if (empty($value) || $value == "") {
            return true;
        } else {
            return false;
        }

    }

    public function checkFieldJson($value)
    {
        if ((empty($value) || $value == "") && !is_string($value)) {
            return true;
        } else {
            return false;
        }

    }

    public function checkField($value)
    {
        $return = [];
        if ($value) {
            foreach ($value as $item) {
                if (!empty($item['value']) || !empty($item['label'])) {
                    if (empty($item['key'])) {
                        $item['key'] = uniqid();
                    }
                    $return[] = $item;
                }
            }
        }
        if ($return) {
            return $return;
        } else {
            return null;
        }


    }

    public function createField($model_field, $field_name, $fields_id)
    {
        $fields_id = json_decode($fields_id, true);
        if (!is_array($fields_id)) {
            $fields_id = [];
        }
        if (!isset($fields_id[$field_name . '_id'])) {
            $fields_id += [$field_name . '_id' => []];
        }
        if (!$this->checkValueInverted($model_field) && !$fields_id[$field_name . '_id']) {
            $fieldsCount = count($model_field, true);
        } else {
            $fieldsCount = 0;
        }
        if ($fields_id[$field_name . '_id']) {
            $item = array_key_last($fields_id[$field_name . '_id']) + 1;
            $fields_id[$field_name . '_id'] += [$item => $item];
        } else {
            $fields_id[$field_name . '_id'] += [$fieldsCount => $fieldsCount];
        }

        return $fields_id;
    }

    public function getKey($data)
    {
        foreach ($data as $item) {
            $item['key'] = uniqid();
            usleep(10);
        }
        return $data;
    }

    private function requiredValue($data)
    {
        if ($this->checkValueInverted($this->organization_name) ||
            $this->checkValueInverted($this->pass_serial_number) ||
            $this->checkValueInverted($this->description) ||
            $this->checkValueInverted($this->pass_authentication_token)) {
            return false;
        }

        $data += [
            "passTypeIdentifier" => self::PASS_TYPE_IDENTIFIER,
            "formatVersion" => self::FORMAT_VERSION,
            "organizationName" => $this->organization_name,
            "teamIdentifier" => self::TEAM_INDENTIFIER,
            "serialNumber" => $this->pass_serial_number,
            "description" => $this->description,
            "authenticationToken" => $this->pass_authentication_token,
            "webServiceURL" => Url::base(true) . self::WEB_SERVISE_URL
        ];
        return $data;
    }

    private function logoText($data)
    {
        if ($this->checkValueInverted($this->logo_text)) {
            return $data;
        }
        $data += ["logoText" => $this->logo_text];
        return $data;
    }

    private function foregroundColor($data)
    {
        if ($this->checkValueInverted($this->foreground_color)) {
            return $data;
        }
        $data += ["foreground_color" => $this->foreground_color];
        return $data;
    }

    private function backgroundColor($data)
    {
        if ($this->checkValueInverted($this->background_color)) {
            return $data;
        }
        $data += ["backgroundColor" => $this->background_color];
        return $data;
    }

    private function labelColor($data)
    {
        if ($this->checkValueInverted($this->label_color)) {
            return $data;
        }
        $data += ["labelColor" => $this->label_color];
        return $data;
    }

    private function expirationDate($data)
    {
        if ($this->checkValueInverted($this->expiration_date)) {
            return $data;
        }
        $data += ["expirationDate" => date(DATE_ISO8601, strtotime($this->expiration_date))];
        return $data;
    }

    private function relevantDate($data)
    {
        if ($this->checkValueInverted($this->relevant_date)) {
            return $data;
        }
        $data += ["relevantDate" => date(DATE_ISO8601, strtotime($this->relevant_date))];
        return $data;
    }

    private function voided($data)
    {

        if ($this->status == $this::STATUS_BLOCKED || $this->pass_status == PkpassPass::STATUS_BLOCKED) {
            $data += ["voided" => 'true'];
        } else {
            $data += ["voided" => 'false'];
        }

        return $data;
    }

    private function setField($data)
    {
        foreach ($data as $item) {
            $fields[] = [
                "key" => $item["key"],
                "label" => $item["label"],
                "value" => $item["value"]
            ];
        }

        return $fields;
    }

    private function headerFields($data)
    {
        if ($this->checkValueInverted($this->header_fields)) {
            return $data;
        }
        $value = json_decode($this->header_fields, true);
        $data[self::TEMP_STYLE_KEY] += ["headerFields" => $this->setField($value)];
        return $data;
    }

    private function primaryFields($data)
    {
        if ($this->checkValueInverted($this->primary_fields)) {
            return $data;
        }
        $value = json_decode($this->primary_fields, true);
        $data[self::TEMP_STYLE_KEY] += ["primaryFields" => $value];
        return $data;
    }

    private function secondaryFields($data)
    {
//        if ($this->checkValueInverted($this->secondary_fields)) {
//            return $data;
//        }
//        $value = json_decode($this->secondary_fields, true);
        $value = [];
        if (!empty($this->user->username)) {
            $value[] =
                [
                    "key" => "1",
                    "label" => Yii::t('site', 'First Name'),
                    "value" => $this->user->username
                ];
        } else {
            $value[] = [
                "key" => "1",
                "label" => Yii::t('site', 'First Name'),
                "value" => Yii::t('site', 'User')
            ];
        }
        if (!empty($this->user->last_name)) {
            $value[] =
                [
                    "key" => "2",
                    "label" => Yii::t('site', 'Last Name'),
                    "value" => $this->user->last_name
                ];
        }

        $data[self::TEMP_STYLE_KEY] += ["secondaryFields" => $value];
        return $data;
    }

    private function backFields($data)
    {
        if ($this->checkValueInverted($this->back_fields)) {
            return $data;
        }
        $value = json_decode($this->back_fields, true);
        $data[self::TEMP_STYLE_KEY] += ["backFields" => $this->setField($value)];
        return $data;
    }

    private function auxiliaryFields($data)
    {
        if ($this->checkValueInverted($this->auxiliary_fields)) {
            return $data;
        }
        $value = json_decode($this->auxiliary_fields, true);
        $data[self::TEMP_STYLE_KEY] += ["auxiliaryFields" => $value];
        return $data;
    }

    private function barcode($data)
    {
        $data += [
            "barcode" => [
                "format" => $this->barcode,
                "message" => base64_encode($this->userProfile->qr_key),
                "messageEncoding" => self::MESSAGE_ENCODING,
                "altText" => $this->userProfile->qr_key
            ]
        ];
        return $data;
    }

    /**
     * @param PkpassPass $modelPass
     * @return false|resource
     */
    public function createPass($modelPass, $temp = true)
    {
        $modelUser = $modelPass->user;
        $modelUserProfile = $modelUser->userProfile;

        $this->user = $modelUser;
        $this->userProfile = $modelUserProfile;
        if (empty($modelUserProfile->qr_key)) {
            $modelUserProfile->qr_key = $modelUserProfile->qr_key();
            $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
            $modelUserProfile->save();
        }
        $this->pass_serial_number = $modelPass->serial_number;
        $this->pass_authentication_token = $modelPass->authentication_token;

        if ($modelPass->user_info) {
            $this->pass_user_info = $modelPass->user_info;
        }
        if ($modelPass->relevant_date) {
            $this->pass_relevant_date = $modelPass->relevant_date;
        }
        if ($modelPass->expiration_date) {
            $this->pass_expiration_date = $modelPass->expiration_date;
        }
        if ($modelPass->status == $modelPass::STATUS_BLOCKED) {
            $this->pass_status = $modelPass->status;
        }

        setlocale(LC_MONETARY, 'en_US');

        $pass = new PkpassService();
        $data = [];
        $data = $this->requiredValue($data);
        $data = $this->logoText($data);
        $data = $this->foregroundColor($data);
        $data = $this->backgroundColor($data);
        $data = $this->labelColor($data);
//        $data = $this->expirationDate($data);
//        $data = $this->relevantDate($data);
//        $data = $this->voided($data);
        $data = $this->barcode($data);
        $data[self::TEMP_STYLE_KEY] = [];
        $data = $this->headerFields($data);
        $data = $this->primaryFields($data);
        $data = $this->secondaryFields($data);
        $data = $this->backFields($data);
        $data = $this->auxiliaryFields($data);

        $pass->setData($data);

        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->icon . '.png');
        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->icon . '@2x.png');
//        $pass->addFile(Yii::getAlias('@webroot') . $this->icon . '@3x.png');
        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->logo . '.png');
        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->logo . '@2x.png');
//        $pass->addFile(Yii::getAlias('@webroot') . $this->logo . '@3x.png');
        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->strip . '.png');
        $pass->addFile(Yii::getAlias('@backend') . '/web/'.$this->strip . '@2x.png');
//        $pass->addFile(Yii::getAlias('@webroot') . $this->strip . '@3x.png');

        $file = $pass->create();
        $passBookFullPath = Yii::getAlias('@backend') . '/web/pkpass/temp/' . $modelPass->serial_number . '_pass.pkpass';
        file_put_contents($passBookFullPath, $file);
        $passFile = fopen($passBookFullPath, 'rb');
        if ($temp) {
            PkpassTemplate::deletePass($modelPass->serial_number);
        }

        return $passFile;
    }

    static function deletePass($serial_number)
    {
        $path = Yii::getAlias('@backend') . '/web/pkpass/temp/' . $serial_number . '_pass.pkpass';
        if (file_exists($path)) {
            unlink($path);
        }
    }


    public function getPkpassPasses()
    {
        return $this->hasMany(PkpassPass::class, ['template_id' => 'id']);
    }
}
