<?php

namespace backend\modules\pkpass\models;

use backend\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "pkpass_pass".
 *
 * @property int $id
 * @property int|null $template_id
 * @property string|null $pass_type_identifier
 * @property string|null $serial_number
 * @property string|null $user_info
 * @property string|null $relevant_date
 * @property string|null $expiration_date
 * @property int|null $status
 * @property string|null $authentication_token
 * @property string|null $user_id
 * @property string|null $created_at
 * @property PkpassTemplate $pkpassTemplate
 * @property PkpassDevice $pkpassDevices
 * @property User $user
 */
class PkpassPass extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '5';
    const STATUS_BLOCKED = '10';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pkpass_pass';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'datetime'],
            [['template_id','user_id'], 'integer'],
            [['user_info', 'authentication_token','pass_type_identifier'], 'string'],
            [['relevant_date', 'expiration_date', 'status'], 'safe'],
            [['serial_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pkpassPass', 'ID'),
            'template_id' => Yii::t('pkpassPass', 'Template ID'),
            'serial_number' => Yii::t('pkpassPass', 'Serial Number'),
            'user_info' => Yii::t('pkpassPass', 'User Info'),
            'relevant_date' => Yii::t('pkpassPass', 'Relevant Date'),
            'expiration_date' => Yii::t('pkpassPass', 'Expiration Date'),
            'status' => Yii::t('pkpassTemplate', 'Status'),
            'authentication_token' => Yii::t('pkpassPass', 'Authentication Token'),
        ];
    }

    /**
     * Get Pkpass Template
     *
     * @return yii\db\ActiveQuery
     */
    public function getPkpassTemplate()
    {
        return $this->hasOne(PkpassTemplate::class, ['id' => 'template_id']);
    }

    /**
     * Get User
     *
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getPkpassDevices()
    {
        return $this->hasMany(PkpassDevice::class, ['pass_id' => 'id']);
    }
}
