<?php

namespace backend\modules\pkpass\formatter;

use backend\components\BackendFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use backend\modules\pkpass\models\PkpassTemplate;
use Yii;
use yii\bootstrap4\Html;

class PkpassFormatter extends BackendFormatter
{
    static function getUserList(): array
    {
        $result = [];
//        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
//            $model = User::find()->where(['status'=>User::STATUS_ACTIVE])->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();
//        } else {
//            $list = Yii::$app->user->identity->company->getSubAgents();
//            $model = User::find()->where(['role'=>User::ROLE_USER, 'agent_id'=>Yii::$app->user->id, 'status'=>User::STATUS_ACTIVE])->all();
//            $model = array_merge($list, $model);
//        }
        $model = User::find()->where(['status' => User::STATUS_ACTIVE])->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();
        if (!empty($model)) {
            foreach ($model as $item) {
                $result[$item->id] = $item->email . ' ' . $item->username . ' ' . $item->last_name;
            }
        }
        return $result;
    }

    static function getCompanyList(): array
    {
        $result = [];
//        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
//            $model = User::find()->where(['status'=>User::STATUS_ACTIVE])->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();
//        } else {
//            $list = Yii::$app->user->identity->company->getSubAgents();
//            $model = User::find()->where(['role'=>User::ROLE_USER, 'agent_id'=>Yii::$app->user->id, 'status'=>User::STATUS_ACTIVE])->all();
//            $model = array_merge($list, $model);
//        }
        $model = Company::find()->all();

        if (!empty($model)) {
            foreach ($model as $item) {
                $result[$item->id] = $item->name;
            }
        }
        return $result;
    }

    static function getTemplateList(): array
    {
        $result = [];

        $model = PkpassTemplate::find()->all();
        if (!empty($model)) {
            foreach ($model as $item) {
                $result[$item->id] = $item->template_name;
            }
        }
        return $result;
    }

    /**
     * @param $data
     * @return User
     */
    static function getUser($data): User
    {

        $user = User::find()->where(['id' => $data->user_id])->one();

        return $user;
    }

    /**
     * @param $data
     * @return PkpassTemplate
     */
    static function getPkpassTemplate($data): PkpassTemplate
    {

        $user = PkpassTemplate::find()->where(['id' => $data->template_id])->one();

        return $user;
    }

    /**
     * @param $data
     * @return string
     */
    static function getVoided($data)
    {

        if ($data->status) {
            return Html::tag('span', Yii::t('currency', Yii::t('users', 'blocked')),
                ['class' => 'badge badge-danger']);
        } else {
            return Html::tag('span', Yii::t('currency', Yii::t('users', 'active')),
                ['class' => 'badge badge-success']);
        }
    }

    /**
     * @param $data
     * @return array|Company|string|\yii\db\ActiveRecord
     */
    static function getCompany($data)
    {
        $company = Company::find()->where(['id' => $data->company_id])->one();
        if (empty($company)) {
            return '-';
        } else {
            return $company->name;
        }

    }

    static function getBarcodeList(): array
    {
        $result = [];

        foreach (PkpassTemplate::BARCODE as $key => $item) {

            $result[$key] = $item;
        }

        return $result;
    }

    static function getStatusList(): array
    {
        $result = [];
        $result[PkpassTemplate::STATUS_INACTIVE] = Yii::t('pkpassTemplate', 'inactive');
        $result[PkpassTemplate::STATUS_ACTIVE] = Yii::t('pkpassTemplate', 'active');

        return $result;
    }

    static function asStatus($value): string
    {

        if ($value == PkpassTemplate::STATUS_ACTIVE) {
            return Html::tag('span', Yii::t('pkpassTemplate', 'active'),
                ['class' => 'badge badge-success']);
        } elseif ($value == PkpassTemplate::STATUS_INACTIVE) {
            return Html::tag('span', Yii::t('pkpassTemplate', 'inactive'),
                ['class' => 'badge badge-secondary']);
        } else {
            return '-';
        }
    }
}