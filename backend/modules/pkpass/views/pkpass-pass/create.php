<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassPass */

$this->title = Yii::t('pkpassPass', 'Create Pkpass Pass');
$this->params['breadcrumbs'][] = ['label' => Yii::t('pkpassPass', 'Pkpass Passes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Passcreator')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-qrcode"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>