<?php
/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassPass */

/* @var $form yii\widgets\ActiveForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use backend\modules\pkpass\formatter\PkpassFormatter;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;

?>


<?php $form = ActiveForm::begin(); ?>
    <div class="col mx-auto max-wr-30">

        <?= $form->field($model, 'template_id')->widget(Select2::class, [
            'data' => PkpassFormatter::getTemplateList(),
            'options' => ['placeholder' => Yii::t('pkpassTemplate', 'Select User')],
            'pluginOptions' => [
                'allowClear' => true

            ],
            'theme' => 'default',
        ]); ?>

        <?= $form->field($model, 'user_id')->widget(Select2::class, [
            'data' => PkpassFormatter::getUserList(),
            'options' => ['placeholder' => Yii::t('pkpassTemplate', 'Select User')],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme' => 'default',
        ]); ?>
        <?= $form->field($model, 'pass_type_identifier', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_pass_type_identifier', 'Pass type identifier, as issued by Apple. The value must correspond with your signing certificate.')) ?>

    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                    ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$t = '
<?= $form->field($model, \'user_info\')->textarea([\'rows\' => 6]); ?>


 <?= $form->field($model, \'relevant_date\', [\'template\' => \'<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}\'])->widget(DateTimePicker::classname(), [
            \'name\' => \'datetime_1\',
            \'pluginOptions\' => [
                \'autoclose\' => true,
                \'format\' => \'dd-mm-yyyy hh:ii\'
            ]
        ])->hint(Yii::t(\'passT_relevant_date\', \'Recommended for event tickets and boarding passes; otherwise optional. Date and time when the pass becomes relevant. For example, the start time of a movie.\')); ?>
        <?= $form->field($model, \'expiration_date\', [\'template\' => \'<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}\'])->widget(DateTimePicker::classname(), [
            \'name\' => \'datetime_2\',
            \'pluginOptions\' => [
                \'autoclose\' => true,
                \'format\' => \'dd-mm-yyyy hh:ii\'
            ]
        ])->hint(Yii::t(\'passT_expiration_date\', \'Date and time when the pass expires.\')); ?>

        <?= $form->field($model, \'voided\', [\'template\' => \'<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}\'],[\'options\'=>[\'class\'=>\'custom-checkbox\']])->checkbox()->label(Yii::t(\'pkpassPass\', \'Void\'))->hint(Yii::t(\'passT_voided\', \'Indicates that the pass is void—for example, a one time use coupon that has been redeemed.\')) ?>







';
$script = '
$( document ).ready(function() {

    $( ".color-picker" ).each(function() {
        $(this).spectrum({
            preferredFormat: "rgb",
            type: "component",
            showAlpha: "false",
            showButtons: "false"
        });
    });
    
});
';
$this->registerJs($script, yii\web\View::POS_END);