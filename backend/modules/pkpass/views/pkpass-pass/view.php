<?php
/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassPass */
/* @var $writer BaconQrCode\Writer */

/* @var $id int */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pkpassPass', 'Pkpass Passes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$url = urlencode(Url::base(true) . '/pkpass/temp/' . $model->serial_number . '_pass.pkpass');

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Passcreator')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-qrcode"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">

            <h4 class="col-12 mt-4" style="text-align: center; font-size: 2.5rem;"><?= Yii::t('site', 'QR-code') ?></h4>
            <div class="col-12 p-0 m-0">
                <ul class="nav nav-tabs d-flex justify-content-center" id="custom-content-above-tab"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"
                           id="custom-content-above-home-tab"
                           data-toggle="pill"
                           href="#custom-content-above-home"
                           role="tab"
                           aria-controls="custom-content-above-home"
                           aria-selected="true">

                            <i class="fab fa-apple"></i> <span> <?= Yii::t('registration', 'Iphone') ?></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           id="custom-content-above-profile-tab"
                           data-toggle="pill"
                           href="#custom-content-above-profile"
                           role="tab"
                           aria-controls="custom-content-above-profile"
                           aria-selected="false">
                            <i class="fab fa-android"></i> <?= Yii::t('registration', 'Android') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="custom-content-above-tabContent">
                    <div class="tab-pane fade active show"
                         id="custom-content-above-home"
                         role="tabpanel"
                         aria-labelledby="custom-content-above-home-tab">
                        <div style="padding-top: 1rem; margin-bottom: 1rem;">

                            <div class="col-xs-12" style="margin-bottom: 1rem;">
                                <div>
                                    <p style="text-align: center;"> <?= Yii::t('site', 'Scan QR-code:') ?></p>
                                    <div class="col max-wr-25 mx-auto my-svg">
                                        <?php echo $writer->writeString(Url::toRoute(['get-pass-ios', 'serialNumber' => $model->serial_number], 'https')); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12"
                                     style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 1rem;">
                                    <p style="margin: 0.5rem 0; text-align: center;">
                                        - <?= Yii::t('Wallet passes', 'or Download') . ':' ?> -</p>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-2">
                                        <?php echo Html::a('<i class="fab fa-apple"></i> ' . Yii::t('site', 'Download'),
                                            Url::toRoute(['get-pass-ios', 'serialNumber' => $model->serial_number], 'https'),
                                            ['class' => 'btn bg-gradient-secondary mx-auto']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade"
                         id="custom-content-above-profile"
                         role="tabpanel"
                         aria-labelledby="custom-content-above-profile-tab">
                        <div style="padding-top: 1rem; margin-bottom: 1rem;">

                            <div class="col-xs-12" style="margin-bottom: 1rem;">
                                <div>
                                    <p style="text-align: center;"> <?= Yii::t('site', 'Scan QR-code:') ?></p>
                                    <div class="col max-wr-25 mx-auto my-svg">
                                        <?php echo $writer->writeString('https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12"
                                     style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 1rem;">
                                    <p style="margin: 0.5rem 0; text-align: center;">
                                        - <?= Yii::t('Wallet passes', 'or Download') . ':' ?> -</p>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-2">
                                        <a href="<?= 'https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url ?>"
                                           class="btn bg-gradient-success mx-auto">
                                            <i class="fab fa-android"
                                               style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Download') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12"
                 style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 1rem;"></div>
            <div class="col mx-auto max-wr-30 pt-4">
                <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'user_id',
                            'template_id',
                            'serial_number',
                            'user_info:ntext',
                            'relevant_date',
                            'expiration_date',
                            'status',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('pkpass_pass.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'),
                                Url::toRoute(['update', 'id' => $model->id]),
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('pkpass_pass.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                [
                                    'class' => 'btn bg-gradient-danger btn-block',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('pkpass_pass.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
