<?php

use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassPassSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pkpass-pass-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'template_id') ?>

    <?= $form->field($model, 'serial_number') ?>

    <?= $form->field($model, 'user_info') ?>

    <?= $form->field($model, 'relevant_date') ?>

    <?php // echo $form->field($model, 'expiration_date') ?>

    <?php // echo $form->field($model, 'voided') ?>

    <?php // echo $form->field($model, 'beacons') ?>

    <?php // echo $form->field($model, 'locations') ?>

    <?php // echo $form->field($model, 'authentication_token') ?>

    <?php // echo $form->field($model, 'webServiceURL') ?>

    <?php // echo $form->field($model, 'auxiliary_fields') ?>

    <?php // echo $form->field($model, 'back_fields') ?>

    <?php // echo $form->field($model, 'header_fields') ?>

    <?php // echo $form->field($model, 'primary_fields') ?>

    <?php // echo $form->field($model, 'secondary_fields') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('pkpassPass', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('pkpassPass', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
