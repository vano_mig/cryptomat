<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassDevice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pkpass-device-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col mx-auto max-wr-30">
        <?= $form->field($model, 'pass_id')->textInput() ?>

        <?= $form->field($model, 'application')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'device_token')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'pass_type_identifier', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_pass_type_identifier', 'Pass type identifier, as issued by Apple. The value must correspond with your signing certificate.')) ?>

    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                    ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
