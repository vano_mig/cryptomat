<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassDeviceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pkpass-device-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'pass_id') ?>

    <?= $form->field($model, 'application') ?>

    <?= $form->field($model, 'device_token') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('pkpassDevice', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('pkpassDevice', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
