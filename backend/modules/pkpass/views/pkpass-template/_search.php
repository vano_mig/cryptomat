<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassTemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pkpass-template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'organization_name') ?>

    <?= $form->field($model, 'pass_type_identifier') ?>

    <?= $form->field($model, 'team_identifier') ?>

    <?php // echo $form->field($model, 'relevant_date') ?>

    <?php // echo $form->field($model, 'expiration_date') ?>

    <?php // echo $form->field($model, 'style_key') ?>

    <?php // echo $form->field($model, 'voided') ?>

    <?php // echo $form->field($model, 'beacons') ?>

    <?php // echo $form->field($model, 'locations') ?>

    <?php // echo $form->field($model, 'barcode') ?>

    <?php // echo $form->field($model, 'background_color') ?>

    <?php // echo $form->field($model, 'foreground_color') ?>

    <?php // echo $form->field($model, 'label_color') ?>

    <?php // echo $form->field($model, 'logo_text') ?>

    <?php // echo $form->field($model, 'nfc') ?>

    <?php // echo $form->field($model, 'auxiliary_fields') ?>

    <?php // echo $form->field($model, 'back_fields') ?>

    <?php // echo $form->field($model, 'header_fields') ?>

    <?php // echo $form->field($model, 'primary_fields') ?>

    <?php // echo $form->field($model, 'secondary_fields') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('pkpassTemplate', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('pkpassTemplate', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
