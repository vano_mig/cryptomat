<?php

/* @var $this \yii\web\View */

/* @var $model \backend\modules\pkpass\models\PkpassTemplate|\yii\db\ActiveRecord */

use yii\helpers\Url;


?>
<div class="embed-responsive embed-responsive-9by16 border-r-5 elevation-3">
    <div class="embed-responsive-item bg-info">
        <div class="col-12 h-100 p-0 m-0">
            <div class="row col-12 p-0 m-0" style="height: 12%">
                <div class="p-0 m-0 bg-green h-100" style="width: 20%;">
                    <img class="pass-logo" src="<?= Url::to($model->logo . '@2x.png') ?>">
                </div>

                <div class="p-0 m-0 bg-lime h-100 d-inline-flex align-items-center my-text-scale h-auto"
                     style="width: 40%;">
                    <span style="font-weight: 500; white-space: nowrap;"><?= $model->logo_text ?></span>
                </div>
                <div class="row p-0 m-0 bg-indigo h-100 my-text-scale align-items-center h-auto"
                     style="width: 40%;">
                    <div class="p-0 m-0 d-inline-flex">
                        <?php if ($model->header_fields) { ?>
                            <?php if (is_string($model->header_fields)) { ?>
                                <?php $model->header_fields = json_decode($model->header_fields, true) ?>
                            <?php } ?>
                            <?php foreach ($model->header_fields as $item) { ?>
                                <div class="p-0 m-0">
                                <span class="col-12 p-0 m-0 text-right"
                                      style="font-weight: 500; white-space: nowrap;"><?= $item['label'] ?> <i
                                            style="width: 5px; display: inline-block"></i></span>
                                    <span class="col-12 p-0 m-0 text-right"
                                          style="font-weight: 500; white-space: nowrap;"><?= $item['value'] ?> <i
                                                style="width: 5px; display: inline-block"></i></span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row col-12 p-0 m-0" style="height: 20%">
                <img class="img-fluid" src="<?= Url::to($model->strip . '@2x.png') ?>">
            </div>
        </div>
    </div>
</div>



