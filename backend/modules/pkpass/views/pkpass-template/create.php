<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassTemplate */
/* @var $fields_id array[] */
/* @var $id array|mixed|null */

$this->title = Yii::t('pkpassTemplate', 'Create Pkpass Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('pkpassTemplate', 'Pkpass Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Passcreator')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-star-half-alt"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
                'fields_id'=>$fields_id,
                'id' => $id
            ]) ?>
        </div>
    </div>
</div>