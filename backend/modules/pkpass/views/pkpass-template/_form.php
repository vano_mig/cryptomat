<?php
/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassTemplate */
/* @var $form yii\widgets\ActiveForm */
/* @var $fields_id array[] */
/* @var $id array|mixed|null */
?>

<?php

use backend\modules\pkpass\formatter\PkpassFormatter;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


if ($model->id) {
    $rout = '/pkpass/pkpass-template/update?id=' . $model->id;
} else {
    $rout = '/pkpass/pkpass-template/create';
}

?>
<?php Pjax::begin(['enablePushState' => false, 'options' => ['class' => 'col-12'], 'clientOptions' => ['method' => 'POST']]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'ss',
    'method' => 'post',
    'validateOnSubmit' => false,
]); ?>

    <div class="card card-my-gray card-tabs max-wr-70 mx-auto elevation-1">
        <div class="card-header p-2 border-0 elevation-inset-2">
            <ul class="nav nav-tabs border-0 d-flex justify-content-center" id="custom-tabs-one-tab" role="tablist">
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link <?= ($id == '1-1' || $id == '1-2' || $id == '1-3' || $id == '1-4') ? 'active' : '' ?>"
                       id="custom-tabs-one-2-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-2"
                       role="tab"
                       aria-controls="custom-tabs-one-2"
                       aria-selected="<?= ($id == '1-1' || $id == '1-2' || $id == '1-3' || $id == '1-4') ? 'true' : 'false' ?>">
                        <?= Yii::t('pkpassTemplate', 'Front Fields') ?>
                    </a>
                </li>
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link <?= ($id == '3') ? 'active' : '' ?>"
                       id="custom-tabs-one-3-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-3"
                       role="tab"
                       aria-controls="custom-tabs-one-3"
                       aria-selected="<?= ($id == '3') ? 'true' : 'false' ?>">
                        <?= Yii::t('pkpassTemplate', 'Back Fields') ?>
                    </a>
                </li>
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link"
                       id="custom-tabs-one-4-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-4"
                       role="tab"
                       aria-controls="custom-tabs-one-1"
                       aria-selected="false">
                        <?= Yii::t('pkpassTemplate', 'Color') ?>
                    </a>
                </li>
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link <?= ($id == '5') ? 'active' : '' ?>"
                       id="custom-tabs-one-5-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-5"
                       role="tab"
                       aria-controls="custom-tabs-one-5"
                       aria-selected="<?= ($id == '5') ? 'true' : 'false' ?>">
                        <?= Yii::t('pkpassTemplate', 'Images') ?>
                    </a>
                </li>
                <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link"
                       id="custom-tabs-one-1-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-1"
                       role="tab"
                       aria-controls="custom-tabs-one-1"
                       aria-selected="false">
                        <?= Yii::t('pkpassTemplate', 'System') ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="row card-body">
            <div class="col wr-17 my-3 mx-auto">
                <?= $this->render('_pass', [
                    'model' => $model,
                ]); ?>
            </div>
            <div class="col tab-content mx-auto max-wr-30" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade <?= ($id == '1-1' || $id == '1-2' || $id == '1-3' || $id == '1-4') ? 'active show' : '' ?>"
                     id="custom-tabs-one-2"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-2-tab">
                    <div class="col mx-auto min-wr-17 max-wr-30">
                        <?= $form->field($model, 'logo_text')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'barcode', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->dropDownList(PkpassFormatter::getBarcodeList())->hint(Yii::t('passT_barcode', 'Barcode format.')); ?>
                    </div>
                    <div class="col-12 p-0 m-0 card card-my-gray card-tabs elevation-1">
                        <div class="card-header p-2 border-0 elevation-inset-2">
                            <ul class="nav nav-tabs border-0 d-flex justify-content-center" id="custom-tabs-tab"
                                role="tablist">
                                <li class="rounded nav-item elevation-2 mx-2 my-1">
                                    <a class="rounded nav-link <?= ($id == '1-1' || $id == '3' || $id == '5') ? 'active' : '' ?>"
                                       id="custom-tabs-one-1-1-tab"
                                       data-toggle="pill"
                                       href="#custom-tabs-one-1-1"
                                       role="tab"
                                       aria-controls="custom-tabs-one-1-1"
                                       aria-selected="<?= ($id == '1-1' || $id == '3' || $id == '5') ? 'true' : 'false' ?>">
                                        <?= Yii::t('pkpassTemplate', 'Header Fields') ?>
                                    </a>
                                </li>
                                <li class="rounded nav-item elevation-2 mx-2 my-1">
                                    <a class="rounded nav-link <?= ($id == '1-2') ? 'active' : '' ?>"
                                       id="custom-tabs-one-1-2-tab"
                                       data-toggle="pill"
                                       href="#custom-tabs-one-1-2"
                                       role="tab"
                                       aria-controls="custom-tabs-one-1-2"
                                       aria-selected="<?= ($id == '1-2') ? 'true' : 'false' ?>">
                                        <?= Yii::t('pkpassTemplate', 'Primary Fields') ?>
                                    </a>
                                </li>
                                <li class="rounded nav-item elevation-2 mx-2 my-1">
                                    <a class="rounded nav-link <?= ($id == '1-3') ? 'active' : '' ?>"
                                       id="custom-tabs-one-1-3-tab"
                                       data-toggle="pill"
                                       href="#custom-tabs-one-1-3"
                                       role="tab"
                                       aria-controls="custom-tabs-one-1-3"
                                       aria-selected="<?= ($id == '1-3') ? 'true' : 'false' ?>">
                                        <?= Yii::t('pkpassTemplate', 'Secondary Fields') ?>
                                    </a>
                                </li>
                                <li class="rounded nav-item elevation-2 mx-2 my-1">
                                    <a class="rounded nav-link <?= ($id == '1-4') ? 'active' : '' ?>"
                                       id="custom-tabs-one-1-4-tab"
                                       data-toggle="pill"
                                       href="#custom-tabs-one-1-4"
                                       role="tab"
                                       aria-controls="custom-tabs-one-1-4"
                                       aria-selected="<?= ($id == '1-4') ? 'true' : 'false' ?>">
                                        <?= Yii::t('pkpassTemplate', 'Auxiliary Fields') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="row card-body">
                            <div class="col tab-content mx-auto max-wr-30" id="custom-tabs-tabContent">
                                <div class="tab-pane fade <?= ($id == '1-1' || $id == '3' || $id == '5') ? 'active show' : '' ?>"
                                     id="custom-tabs-one-1-1"
                                     role="tabpanel"
                                     aria-labelledby="custom-tabs-one-1-1-tab">
                                    <?php if ($model->header_fields) { ?>
                                        <?php foreach ($model->header_fields as $keyH => $itemH) { ?>
                                            <?php if ($itemH['key']) { ?>
                                                <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                    <div class="row col-12 p-0 m-0">
                                                        <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                            [$rout],
                                                            [
                                                                'id' => '1-1-' . $keyH,
                                                                'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                                'data-pjax' => '1',
                                                                'data-method' => 'post',
                                                                'data' => [
                                                                    'method' => 'post',
                                                                    'params' => [
                                                                        'id_d' => $keyH,
                                                                        'name_d' => 'header_fields',
                                                                        'fields_id_d' => json_encode($fields_id),
                                                                        'id' => '1-1'
                                                                    ],
                                                                ]]
                                                        ) ?>
                                                    </div>
                                                    <?= $form->field($model, "header_fields[$keyH][key]")->hiddenInput()->label(false) ?>
                                                    <?= $form->field($model, "header_fields[$keyH][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                    <?= $form->field($model, "header_fields[$keyH][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (isset($fields_id['header_id'])) { ?>
                                        <?php foreach ($fields_id['header_id'] as $itemNewH) { ?>
                                            <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                <div class="row col-12 p-0 m-0">
                                                    <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                        [$rout],
                                                        [
                                                            'id' => '1-1-' . $itemNewH,
                                                            'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                            'data-pjax' => '1',
                                                            'data-method' => 'post',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'params' => [
                                                                    'id_d' => $itemNewH,
                                                                    'name_d' => 'header_id',
                                                                    'fields_id_d' => json_encode($fields_id),
                                                                    'id' => '1-1'
                                                                ],
                                                            ]]
                                                    ) ?>
                                                </div>
                                                <?= $form->field($model, "header_fields[$itemNewH][key]")->hiddenInput()->label(false) ?>
                                                <?= $form->field($model, "header_fields[$itemNewH][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                <?= $form->field($model, "header_fields[$itemNewH][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="row col-12">
                                        <?= Html::a(Yii::t('pkpassTemplate', 'Add field'),
                                            [$rout],
                                            [
                                                'id' => '1-1',
                                                'class' => 'btn btn-success mx-auto add-field elevation-3 m-3',
                                                'data-pjax' => '1',
                                                'data-method' => 'post',
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'add_header' => json_encode($fields_id),
                                                        'id' => '1-1'
                                                    ],
                                                ]]
                                        ) ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?= ($id == '1-2') ? 'active show' : '' ?>"
                                     id="custom-tabs-one-1-2"
                                     role="tabpanel"
                                     aria-labelledby="custom-tabs-one-1-2-tab"
                                >
                                    <?php if ($model->primary_fields) { ?>
                                        <?php foreach ($model->primary_fields as $keyP => $itemP) { ?>
                                            <?php if ($itemP['key']) { ?>
                                                <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                    <div class="row col-12 p-0 m-0">
                                                        <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                            [$rout],
                                                            [
                                                                'id' => '1-2-' . $keyP,
                                                                'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                                'data-pjax' => '1',
                                                                'data-method' => 'post',
                                                                'data' => [
                                                                    'method' => 'post',
                                                                    'params' => [
                                                                        'id_d' => $keyP,
                                                                        'name_d' => 'primary_fields',
                                                                        'fields_id_d' => json_encode($fields_id),
                                                                        'id' => '1-2'
                                                                    ],
                                                                ]]
                                                        ) ?>
                                                    </div>
                                                    <?= $form->field($model, "primary_fields[$keyP][key]")->hiddenInput()->label(false) ?>
                                                    <?= $form->field($model, "primary_fields[$keyP][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                    <?= $form->field($model, "primary_fields[$keyP][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (isset($fields_id['primary_id'])) { ?>
                                        <?php foreach ($fields_id['primary_id'] as $itemNewP) { ?>
                                            <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                <div class="row col-12 p-0 m-0">
                                                    <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                        [$rout],
                                                        [
                                                            'id' => '1-2-' . $itemNewP,
                                                            'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                            'data-pjax' => '1',
                                                            'data-method' => 'post',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'params' => [
                                                                    'id_d' => $itemNewP,
                                                                    'name_d' => 'primary_id',
                                                                    'fields_id_d' => json_encode($fields_id),
                                                                    'id' => '1-2',
                                                                ],
                                                            ]]
                                                    ) ?>
                                                </div>
                                                <?= $form->field($model, "primary_fields[$itemNewP][key]")->hiddenInput()->label(false) ?>
                                                <?= $form->field($model, "primary_fields[$itemNewP][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                <?= $form->field($model, "primary_fields[$itemNewP][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="row col-12">
                                        <?= Html::a(Yii::t('pkpassTemplate', 'Add field'),
                                            [$rout],
                                            [
                                                'id' => '1-2',
                                                'class' => 'btn btn-success mx-auto add-field elevation-3 m-3',
                                                'data-pjax' => '1',
                                                'data-method' => 'post',
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'add_primary' => json_encode($fields_id),
                                                        'id' => '1-2',
                                                    ],
                                                ]]
                                        ) ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?= ($id == '1-3') ? 'active show' : '' ?>"
                                     id="custom-tabs-one-1-3"
                                     role="tabpanel"
                                     aria-labelledby="custom-tabs-one-1-3-tab"
                                >
                                    <?php if ($model->secondary_fields) { ?>
                                        <?php foreach ($model->secondary_fields as $keyS => $itemS) { ?>
                                            <?php if ($itemS['key']) { ?>
                                                <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                    <div class="row col-12 p-0 m-0">
                                                        <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                            [$rout],
                                                            [
                                                                'id' => '1-3-' . $keyS,
                                                                'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                                'data-pjax' => '1',
                                                                'data-method' => 'post',
                                                                'data' => [
                                                                    'method' => 'post',
                                                                    'params' => [
                                                                        'id_d' => $keyS,
                                                                        'name_d' => 'secondary_fields',
                                                                        'fields_id_d' => json_encode($fields_id),
                                                                        'id' => '1-3'
                                                                    ],
                                                                ]]
                                                        ) ?>
                                                    </div>
                                                    <?= $form->field($model, "secondary_fields[$keyS][key]")->hiddenInput()->label(false) ?>
                                                    <?= $form->field($model, "secondary_fields[$keyS][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                    <?= $form->field($model, "secondary_fields[$keyS][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (isset($fields_id['secondary_id'])) { ?>
                                        <?php foreach ($fields_id['secondary_id'] as $itemNewS) { ?>
                                            <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                <div class="row col-12 p-0 m-0">
                                                    <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                        [$rout],
                                                        [
                                                            'id' => '1-3-' . $itemNewS,
                                                            'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                            'data-pjax' => '1',
                                                            'data-method' => 'post',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'params' => [
                                                                    'id_d' => $itemNewS,
                                                                    'name_d' => 'secondary_id',
                                                                    'fields_id_d' => json_encode($fields_id),
                                                                    'id' => '1-3',
                                                                ],
                                                            ]]
                                                    ) ?>
                                                </div>
                                                <?= $form->field($model, "secondary_fields[$itemNewS][key]")->hiddenInput()->label(false) ?>
                                                <?= $form->field($model, "secondary_fields[$itemNewS][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                <?= $form->field($model, "secondary_fields[$itemNewS][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="row col-12">
                                        <?= Html::a(Yii::t('pkpassTemplate', 'Add field'),
                                            [$rout],
                                            [
                                                'id' => '1-3',
                                                'class' => 'btn btn-success mx-auto add-field elevation-3 m-3',
                                                'data-pjax' => '1',
                                                'data-method' => 'post',
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'add_secondary' => json_encode($fields_id),
                                                        'id' => '1-3',
                                                    ],
                                                ]]
                                        ) ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade <?= ($id == '1-4') ? 'active show' : '' ?>"
                                     id="custom-tabs-one-1-4"
                                     role="tabpanel"
                                     aria-labelledby="custom-tabs-one-1-4-tab"
                                >
                                    <?php if ($model->auxiliary_fields) { ?>
                                        <?php foreach ($model->auxiliary_fields as $keyA => $itemA) { ?>
                                            <?php if ($itemA['key']) { ?>
                                                <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                    <div class="row col-12 p-0 m-0">
                                                        <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                            [$rout],
                                                            [
                                                                'id' => '1-4-' . $keyA,
                                                                'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                                'data-pjax' => '1',
                                                                'data-method' => 'post',
                                                                'data' => [
                                                                    'method' => 'post',
                                                                    'params' => [
                                                                        'id_d' => $keyA,
                                                                        'name_d' => 'auxiliary_fields',
                                                                        'fields_id_d' => json_encode($fields_id),
                                                                        'id' => '1-4'
                                                                    ],
                                                                ]]
                                                        ) ?>
                                                    </div>
                                                    <?= $form->field($model, "auxiliary_fields[$keyA][key]")->hiddenInput()->label(false) ?>
                                                    <?= $form->field($model, "auxiliary_fields[$keyA][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                    <?= $form->field($model, "auxiliary_fields[$keyA][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (isset($fields_id['auxiliary_id'])) { ?>
                                        <?php foreach ($fields_id['auxiliary_id'] as $itemNewA) { ?>
                                            <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                                <div class="row col-12 p-0 m-0">
                                                    <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                        [$rout],
                                                        [
                                                            'id' => '1-4-' . $itemNewA,
                                                            'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                            'data-pjax' => '1',
                                                            'data-method' => 'post',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'params' => [
                                                                    'id_d' => $itemNewA,
                                                                    'name_d' => 'auxiliary_id',
                                                                    'fields_id_d' => json_encode($fields_id),
                                                                    'id' => '1-4',
                                                                ],
                                                            ]]
                                                    ) ?>
                                                </div>
                                                <?= $form->field($model, "auxiliary_fields[$itemNewA][key]")->hiddenInput()->label(false) ?>
                                                <?= $form->field($model, "auxiliary_fields[$itemNewA][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                                <?= $form->field($model, "auxiliary_fields[$itemNewA][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="row col-12">
                                        <?= Html::a(Yii::t('pkpassTemplate', 'Add field'),
                                            [$rout],
                                            [
                                                'id' => '1-4',
                                                'class' => 'btn btn-success mx-auto add-field elevation-3 m-3',
                                                'data-pjax' => '1',
                                                'data-method' => 'post',
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'add_auxiliary' => json_encode($fields_id),
                                                        'id' => '1-4',
                                                    ],
                                                ]]
                                        ) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?= ($id == '3') ? 'active show' : '' ?>"
                     id="custom-tabs-one-3"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-3-tab"
                >
                    <div class="col mx-auto min-wr-17 max-wr-30">

                        <?php if ($model->back_fields) { ?>
                            <?php foreach ($model->back_fields as $keyB => $itemB) { ?>
                                <?php if ($itemB['key']) { ?>
                                    <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                        <div class="row col-12 p-0 m-0">
                                            <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                                [$rout],
                                                [
                                                    'id' => '3-' . $keyB,
                                                    'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                    'data-pjax' => '1',
                                                    'data-method' => 'post',
                                                    'data' => [
                                                        'method' => 'post',
                                                        'params' => [
                                                            'id_d' => $keyB,
                                                            'name_d' => 'back_fields',
                                                            'fields_id_d' => json_encode($fields_id),
                                                            'id' => '3'
                                                        ],
                                                    ]]
                                            ) ?>
                                        </div>
                                        <?= $form->field($model, "back_fields[$keyB][key]")->hiddenInput()->label(false) ?>
                                        <?= $form->field($model, "back_fields[$keyB][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                        <?= $form->field($model, "back_fields[$keyB][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($fields_id['back_id'])) { ?>
                            <?php foreach ($fields_id['back_id'] as $itemNewB) { ?>
                                <div class="col-12 elevation-2 border-r-5 p-3 m-2 border">
                                    <div class="row col-12 p-0 m-0">
                                        <?= Html::a(Yii::t('pkpassTemplate', 'Delete field'),
                                            [$rout],
                                            [
                                                'id' => '3-' . $itemNewB,
                                                'class' => 'btn btn-warning ml-auto add-field elevation-3',
                                                'data-pjax' => '1',
                                                'data-method' => 'post',
                                                'data' => [
                                                    'method' => 'post',
                                                    'params' => [
                                                        'id_d' => $itemNewB,
                                                        'name_d' => 'back_id',
                                                        'fields_id_d' => json_encode($fields_id),
                                                        'id' => '3',
                                                    ],
                                                ]]
                                        ) ?>
                                    </div>
                                    <?= $form->field($model, "back_fields[$itemNewB][key]")->hiddenInput()->label(false) ?>
                                    <?= $form->field($model, "back_fields[$itemNewB][label]")->textInput()->label(Yii::t('pkpassTemplate', 'Label')) ?>
                                    <?= $form->field($model, "back_fields[$itemNewB][value]")->textarea(['rows' => 6])->label(Yii::t('pkpassTemplate', 'Value')) ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="row col-12">
                            <?= Html::a(Yii::t('pkpassTemplate', 'Add field'),
                                [$rout],
                                [
                                    'id' => '3',
                                    'class' => 'btn btn-success mx-auto add-field elevation-3 m-3',
                                    'data-pjax' => '1',
                                    'data-method' => 'post',
                                    'data' => [
                                        'method' => 'post',
                                        'params' => [
                                            'add_back' => json_encode($fields_id),
                                            'id' => '3',
                                        ],
                                    ]]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="custom-tabs-one-4"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-4-tab"
                >
                    <div class="col mx-auto min-wr-17 max-wr-30">
                        <?= $form->field($model, 'background_color')->textInput(['maxlength' => true, 'class' => 'color-picker form-control']) ?>

                        <?= $form->field($model, 'foreground_color')->textInput(['maxlength' => true, 'class' => 'color-picker form-control']) ?>

                        <?= $form->field($model, 'label_color')->textInput(['maxlength' => true, 'class' => 'color-picker form-control']) ?>
                    </div>
                </div>
                <div class="tab-pane fade <?= ($id == '5') ? 'active show' : '' ?>"
                     id="custom-tabs-one-5"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-5-tab"
                >
                    <div class="col mx-auto min-wr-17 max-wr-30">
                        <div class="col px-1 pt-2 pb-3 m-1 mb-3 d-inline-flex elevation-1 border">
                            <?php echo Html::img(Url::to($model->icon . '@2x.png'), ['class' => 'mr-2 wr-4  elevation-1 border-r-5']) ?>
                            <?php echo $form->field($model, 'icon', ['options' => ['class' => 'my-auto custom-file']])->fileInput(['class' => 'custom-file-input'])->label(Yii::t('pkpassTemplate', 'Icon'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                            <div class="my-info-field">
                                <div class="ml-2">
                                    <i class="fas fa-info-circle"></i>
                                    <small class="form-text text-muted"><?= Yii::t('pkpassTemplate_icon', 'The pass’s icon. This is displayed in notifications and in emails that have a pass attached, and on the lock screen.') ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="col px-1 pt-2 pb-3 m-1 mb-3 d-inline-flex elevation-1 border">
                            <?php echo Html::img(Url::to($model->logo . '@2x.png'), ['class' => 'mr-2 wr-4  elevation-1 border-r-5']) ?>
                            <?php echo $form->field($model, 'logo', ['options' => ['class' => 'form-group my-auto custom-file']])->fileInput(['class' => 'custom-file-input'])->label(Yii::t('pkpassTemplate', 'Logo'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                            <div class="my-info-field">
                                <div class="ml-2">
                                    <i class="fas fa-info-circle"></i>
                                    <small class="form-text text-muted"><?= Yii::t('pkpassTemplate_logo', 'The image displayed on the front of the pass in the top left.') ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row col px-1 pt-2 pb-4 m-1 mb-3 elevation-1 border">
                            <div class="col-12 p-0 m-0 my-info-field">
                                <div class="float-right">
                                    <i class="fas fa-info-circle"></i>
                                    <small class="form-text text-muted"><?= Yii::t('pkpassTemplate_strip', 'The image displayed behind the primary fields on the front of the pass.') ?></small>
                                </div>
                            </div>
                            <?php echo Html::img(Url::to($model->strip . '@2x.png'), ['class' => 'col mx-auto max-wr-20 p-1 mb-3 m-1 elevation-1 border-r-5']) ?>
                            <?php echo $form->field($model, 'strip', ['options' => ['class' => 'form-group my-auto custom-file']])->fileInput(['class' => 'custom-file-input'])->label(Yii::t('pkpassTemplate', 'Strip'), ['class' => 'custom-file-label', 'data-browse' => Yii::t('fileBrowse', 'Browse'), 'for' => 'customFile']) ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="custom-tabs-one-1"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-1-tab"
                >
                    <div class="col mx-auto min-wr-17 max-wr-30">
                        <?= $form->field($model, 'template_name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'status')->dropDownList(PkpassFormatter::getStatusList()); ?>
                        <?= $form->field($model, 'organization_name', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_organization_name', 'Display name of the organization that originated and signed the pass.')) ?>

                        <?= $form->field($model, 'company_id')->widget(Select2::class, [
                            'data' => PkpassFormatter::getCompanyList(),
                            'options' => ['placeholder' => Yii::t('pkpassTemplate', 'Select Company')],
                            'pluginOptions' => [
                                'allowClear' => true

                            ],
                            'theme' => 'default',
                        ]); ?>

                        <?= $form->field($model, 'description', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_description', 'Brief description of the pass. Don’t try to include all of the data on the pass in its description, just include enough detail to distinguish passes of the same type.')) ?>

                        <?= $form->field($model, 'pass_type_identifier', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_pass_type_identifier', 'Pass type identifier, as issued by Apple. The value must correspond with your signing certificate.')) ?>

                        <?= $form->field($model, 'team_identifier', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->textInput(['maxlength' => true])->hint(Yii::t('passT_team_identifier', 'Team identifier of the organization that originated and signed the pass, as issued by Apple.')) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block', 'name' => 'save', 'value' => json_encode($fields_id), 'data-pjax' => '0',
                ]) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                    ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php Pjax::end() ?>

<?php


/*
  <li class="rounded nav-item elevation-2 mx-2 my-1">
                    <a class="rounded nav-link"
                       id="custom-tabs-one-4-tab"
                       data-toggle="pill"
                       href="#custom-tabs-one-4"
                       role="tab"
                       aria-controls="custom-tabs-one-4"
                       aria-selected="false"
                    >
                        <?= Yii::t('pkpassTemplate', 'Locations') ?>
                    </a>
                </li>
 */
/*
  <div class="tab-pane fade"
                     id="custom-tabs-one-4"
                     role="tabpanel"
                     aria-labelledby="custom-tabs-one-4-tab"
                >
                    <div class="row col-12 p-0 m-0">
                        <div class="col wr-17 mx-auto my-3">
                            <?= $this->render('_pass', ['model' => $model]); ?>
                        </div>
                        <div class="col mx-auto min-wr-17 max-wr-30">

                            <?= $form->field($model, 'beacons')->textarea(['rows' => 6]) ?>

                            <?= $form->field($model, 'locations')->textarea(['rows' => 6]) ?>

                        </div>
                    </div>
                </div>



<?= $form->field($model, 'relevant_date', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->widget(DateTimePicker::classname(), [
                            'name' => 'datetime_1',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-mm-yyyy hh:ii'
                            ]
                        ])->hint(Yii::t('passT_relevant_date', 'Recommended for event tickets and boarding passes; otherwise optional. Date and time when the pass becomes relevant. For example, the start time of a movie.')); ?>
                        <?= $form->field($model, 'expiration_date', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->widget(DateTimePicker::classname(), [
                            'name' => 'datetime_2',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-mm-yyyy hh:ii'
                            ]
                        ])->hint(Yii::t('passT_expiration_date', 'Date and time when the pass expires.')); ?>

                        <?= $form->field($model, 'voided', ['template' => '<div class="my-info-field d-inline-flex">{label}<div class="ml-2"><i class="fas fa-info-circle"></i>{hint}</div></div>{input}{error}'])->checkbox()->hint(Yii::t('passT_voided', 'Indicates that the pass is void—for example, a one time use coupon that has been redeemed.')) ?>



 */

$script = '
$.pjax.defaults.scrollTo = false;
$( document ).ready(function() {
    function scale(block) {
        var step = 1,
            minfs = 1,
            sch = block.scrollHeight,
            h = block.offsetHeight,
            scw = block.scrollWidth,
            w = block.offsetWidth;
        if (scw < w || sch < h ) {
            var fontsize = parseInt($(block).css("font-size"), 10) - step;
            if (fontsize >= minfs){
                $(block).css("font-size",fontsize)
                scale(block);
            }
        }
    }

    $( ".my-text-scale" ).each(function() {
        scale(this); 
    });
    
    $( ".color-picker" ).each(function() {
        $(this).spectrum({
            type: "component",
            showPalette: false,
            showAlpha: false,
            showButtons: false,
            allowEmpty: false,
            preferredFormat: "rgb"
        });
    });
    
    $(document).on("pjax:end", function() {
        $( ".color-picker" ).each(function() {
            $(this).spectrum({
                type: "component",
                showPalette: false,
                showAlpha: false,
                showButtons: false,
                allowEmpty: false,
                preferredFormat: "rgb"
            });
        });
        
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\\").pop();
            $(this) . siblings(".custom-file-label").addClass("selected").html(fileName);   
        });
        
        $( ".my-text-scale" ).each(function() {
            scale(this); 
        });
    })
    
     $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\\").pop();
        $(this) . siblings(".custom-file-label").addClass("selected").html(fileName);   
     });

    $(document).on("touchstart click", ".yhujik", function(event){
        
      
    });
});
';
$this->registerJs($script, yii\web\View::POS_END);
