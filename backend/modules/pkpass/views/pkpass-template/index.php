<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use yii\bootstrap4\Html;
use backend\modules\pkpass\formatter\PkpassFormatter;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\pkpass\models\PkpassTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('pkpassTemplate', 'Pkpass Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Passcreator')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-star-half-alt"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('pkpass_template.create')): ?>
                <?= Html::a(Yii::t('pkpassTemplate', 'Create Pkpass Template'), Url::toRoute(['create']), ['class' => 'btn btn-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                        ],
                        'template_name',
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return PkpassFormatter::getCompany($data);
                            },
                        ],
                        'description',
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return PkpassFormatter::asStatus($data->status);
                            },
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                            'format' => 'raw',
                        ],
                        //'style_key',
                        [
                            'class' => 'backend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {delete} ',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('pkpass_template.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('pkpass_template.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('pkpass_template.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
