<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\pkpass\models\PkpassTemplate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pkpassTemplate', 'Pkpass Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Passcreator')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-star-half-alt"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'description',
                            'organization_name',
                            'pass_type_identifier',
                            'team_identifier',
                            'relevant_date',
                            'expiration_date',
                            'style_key',
                            'status',
                            'beacons:ntext',
                            'locations:ntext',
                            'barcode:ntext',
                            'background_color',
                            'foreground_color',
                            'label_color',
                            'logo_text',
                            'nfc:ntext'
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('pkpass_template.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'),
                                Url::toRoute(['update', 'id' => $model->id]),
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('pkpass_template.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                [
                                    'class' => 'btn bg-gradient-danger btn-block',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('pkpass_template.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
