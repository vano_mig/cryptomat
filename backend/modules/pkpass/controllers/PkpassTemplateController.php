<?php

namespace backend\modules\pkpass\controllers;

use backend\modules\pkpass\models\PkpassDevice;
use backend\modules\pkpass\models\PkpassPass;
use backend\modules\pkpass\models\PkpassService;
use Yii;
use backend\modules\pkpass\models\PkpassTemplate;
use backend\modules\pkpass\models\PkpassTemplateSearch;
use backend\modules\admin\controllers\FrontendController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PkpassTemplateController implements the CRUD actions for PkpassTemplate model.
 */
class PkpassTemplateController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PkpassTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('pkpass_template.listview');
        $searchModel = new PkpassTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PkpassTemplate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('pkpass_template.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PkpassTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($fields_id = [])
    {
        $this->accessRules('pkpass_template.create');
        $model = new PkpassTemplate();
        $model->background_color = 'rgb(235, 235, 235)';
        $model->foreground_color = 'rgb(12, 12, 12)';
        $model->label_color = 'rgb(12, 12, 12)';
        $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
        $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
        $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
        $model->status = $model::STATUS_INACTIVE;

        if (Yii::$app->request->post('save')) {
            if ($model->load(Yii::$app->request->post())) {

                if (!$model->icon) {
                    $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                }
                if (!$model->logo) {
                    $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                }
                if (!$model->strip) {
                    $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                }

                if (!$model->checkValueInverted($model->relevant_date)) {
                    $model->relevant_date = date_format(date_create_from_format('d-m-Y H:i', $model->relevant_date), 'Y-m-d H:i:s');
                }
                if (!$model->checkValueInverted($model->expiration_date)) {
                    $model->expiration_date = date_format(date_create_from_format('d-m-Y H:i', $model->expiration_date), 'Y-m-d H:i:s');
                }

                if (($model->header_fields = $model->checkField($model->header_fields))) {
                    $model->header_fields = $model->getKey($model->header_fields);
                    $model->header_fields = json_encode($model->header_fields);
                }
                if (($model->primary_fields = $model->checkField($model->primary_fields))) {
                    $model->primary_fields = $model->getKey($model->primary_fields);
                    $model->primary_fields = json_encode($model->primary_fields);
                }
                if (($model->secondary_fields = $model->checkField($model->secondary_fields))) {
                    $model->secondary_fields = $model->getKey($model->secondary_fields);
                    $model->secondary_fields = json_encode($model->secondary_fields);
                }
                if (($model->auxiliary_fields = $model->checkField($model->auxiliary_fields))) {
                    $model->auxiliary_fields = $model->getKey($model->auxiliary_fields);
                    $model->auxiliary_fields = json_encode($model->auxiliary_fields);
                }
                if (($model->back_fields = $model->checkField($model->back_fields))) {
                    $model->back_fields = $model->getKey($model->back_fields);
                    $model->back_fields = json_encode($model->back_fields);
                }

                if ($model->save()) {

                    if ($_FILES['PkpassTemplate']['size']['logo'] != false) {
                        $model->saveImage('logo', $model->id);
                    }
                    if ($_FILES['PkpassTemplate']['size']['icon'] != false) {
                        $model->saveImage('icon', $model->id);
                    }
                    if ($_FILES['PkpassTemplate']['size']['strip'] != false) {
                        $model->saveImage('strip', $model->id);
                    }

                    if ($model->hasErrors()) {
                        Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                        $model->load(Yii::$app->request->post());
                        if (!$model->icon) {
                            $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                        }
                        if (!$model->logo) {
                            $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                        }
                        if (!$model->strip) {
                            $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                        }
                        $newModel = clone $model;
                        $model->delete();
                        return $this->render('create', [
                            'model' => $newModel,
                            'fields_id' => $fields_id,
                            'id' => '5'
                        ]);
                    }

                    if ($model->status == $model::STATUS_ACTIVE) {
                        if (!empty(($temp = PkpassTemplate::find()->where(['status' => $model::STATUS_ACTIVE])->andWhere(['!=', 'id', $model->id])->one()))) {
                            $temp->status = $model::STATUS_INACTIVE;
                            $temp->save();
                        }
                    }
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                    if (!$model->checkValueInverted($model->relevant_date)) {
                        $model->relevant_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->relevant_date), 'd-m-Y H:i');
                    }
                    if (!$model->checkValueInverted($model->expiration_date)) {
                        $model->expiration_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->expiration_date), 'd-m-Y H:i');
                    }
                    if (!$model->checkFieldJson($model->back_fields)) {
                        $model->back_fields = json_decode($model->back_fields, true);
                    }
                    if (!$model->checkFieldJson($model->header_fields)) {
                        $model->header_fields = json_decode($model->header_fields, true);
                    }
                    if (!$model->checkFieldJson($model->primary_fields)) {
                        $model->primary_fields = json_decode($model->primary_fields, true);
                    }
                    if (!$model->checkFieldJson($model->secondary_fields)) {
                        $model->secondary_fields = json_decode($model->secondary_fields, true);
                    }
                    if (!$model->checkFieldJson($model->auxiliary_fields)) {
                        $model->auxiliary_fields = json_decode($model->auxiliary_fields, true);
                    }
                }
            }
        }

        if (Yii::$app->request->post('id_d') || Yii::$app->request->post('id_d') === '0') {
            $fields_id = json_decode(Yii::$app->request->post('fields_id_d'), true);
            $name_d = Yii::$app->request->post('name_d');
            $key = explode('_', $name_d);
            $id_d = Yii::$app->request->post('id_d');
            $id = Yii::$app->request->post('id');
            $arr = Yii::$app->request->post();
            unset($arr['PkpassTemplate'][$key[0] . '_fields'][$id_d]);
            unset($fields_id[$name_d][$id_d]);
            $model->load($arr);
            if (!$model->icon) {
                $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
            }
            if (!$model->logo) {
                $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
            }
            if (!$model->strip) {
                $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
            }
            return $this->renderAjax('create', [
                'model' => $model,
                'fields_id' => $fields_id,
                'id' => $id
            ]);
        }

        if (Yii::$app->request->post('add_header') ||
            Yii::$app->request->post('add_primary') ||
            Yii::$app->request->post('add_secondary') ||
            Yii::$app->request->post('add_auxiliary') ||
            Yii::$app->request->post('add_back')) {
            $id = Yii::$app->request->post('id');

            if (Yii::$app->request->post('add_header')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->header_fields, 'header', Yii::$app->request->post('add_header'));
            }
            if (Yii::$app->request->post('add_primary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->primary_fields, 'primary', Yii::$app->request->post('add_primary'));
            }
            if (Yii::$app->request->post('add_secondary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->secondary_fields, 'secondary', Yii::$app->request->post('add_secondary'));
            }
            if (Yii::$app->request->post('add_auxiliary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->auxiliary_fields, 'auxiliary', Yii::$app->request->post('add_auxiliary'));
            }
            if (Yii::$app->request->post('add_back')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->back_fields, 'back', Yii::$app->request->post('add_back'));
            }
            if (!$model->icon) {
                $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
            }
            if (!$model->logo) {
                $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
            }
            if (!$model->strip) {
                $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
            }
            return $this->renderAjax('create', [
                'model' => $model,
                'fields_id' => $fields_id,
                'id' => $id
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'fields_id' => $fields_id,
            'id' => '1-1'
        ]);
    }

    /**
     * Updates an existing PkpassTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $fields_id = [])
    {
        $this->accessRules('pkpass_template.update');
        $model = $this->findModel($id);
        $modelOldAttributes = $model->attributes;
        unset($modelOldAttributes['status']);
        unset($modelOldAttributes['template_name']);
        unset($modelOldAttributes['company_id']);

        if (!$model->icon) {
            $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
        }
        if (!$model->logo) {
            $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
        }
        if (!$model->strip) {
            $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
        }
        if (!$model->checkValueInverted($model->relevant_date)) {
            $model->relevant_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->relevant_date), 'd-m-Y H:i');
        }
        if (!$model->checkValueInverted($model->expiration_date)) {
            $model->expiration_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->expiration_date), 'd-m-Y H:i');
        }
        if (!$model->checkFieldJson($model->back_fields)) {
            $model->back_fields = json_decode($model->back_fields, true);
        }
        if (!$model->checkFieldJson($model->header_fields)) {
            $model->header_fields = json_decode($model->header_fields, true);
        }
        if (!$model->checkFieldJson($model->primary_fields)) {
            $model->primary_fields = json_decode($model->primary_fields, true);
        }
        if (!$model->checkFieldJson($model->secondary_fields)) {
            $model->secondary_fields = json_decode($model->secondary_fields, true);
        }
        if (!$model->checkFieldJson($model->auxiliary_fields)) {
            $model->auxiliary_fields = json_decode($model->auxiliary_fields, true);
        }

        if (Yii::$app->request->post('save')) {
            if ($model->load(Yii::$app->request->post())) {

                if (!$model->icon) {
                    $model->icon = $modelOldAttributes['icon'];
                    if (!$model->icon) {
                        $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                    }
                }
                if (!$model->logo) {
                    $model->logo = $modelOldAttributes['logo'];
                    if (!$model->icon) {
                        $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                    }
                }
                if (!$model->strip) {
                    $model->strip = $modelOldAttributes['strip'];
                    if (!$model->strip) {
                        $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                    }
                }
                if ($_FILES['PkpassTemplate']['size']['logo'] != false) {
                    $model->saveImage('logo', $model->id);
                }
                if ($_FILES['PkpassTemplate']['size']['icon'] != false) {
                    $model->saveImage('icon', $model->id);
                }
                if ($_FILES['PkpassTemplate']['size']['strip'] != false) {
                    $model->saveImage('strip', $model->id);
                }

                if ($model->hasErrors()) {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                    return $this->render('update', [
                        'model' => $model,
                        'fields_id' => $fields_id,
                        'id' => '5'
                    ]);
                }

                if (!$model->checkValueInverted($model->relevant_date)) {
                    $model->relevant_date = date_format(date_create_from_format('d-m-Y H:i', $model->relevant_date), 'Y-m-d H:i:s');
                }
                if (!$model->checkValueInverted($model->expiration_date)) {
                    $model->expiration_date = date_format(date_create_from_format('d-m-Y H:i', $model->expiration_date), 'Y-m-d H:i:s');
                }
                if (($model->back_fields = $model->checkField($model->back_fields))) {
                    if ((isset(Yii::$app->request->post()['PkpassTemplate']['back_fields']))) {
                        $model->back_fields = json_encode($model->back_fields);
                    } else {
                        $model->back_fields = null;
                    }
                }
                if (($model->header_fields = $model->checkField($model->header_fields))) {
                    if ((isset(Yii::$app->request->post()['PkpassTemplate']['header_fields']))) {
                        $model->header_fields = json_encode($model->header_fields);
                    } else {
                        $model->header_fields = null;
                    }
                }
                if (($model->primary_fields = $model->checkField($model->primary_fields))) {
                    if ((isset(Yii::$app->request->post()['PkpassTemplate']['primary_fields']))) {
                        $model->primary_fields = json_encode($model->primary_fields);
                    } else {
                        $model->primary_fields = null;
                    }
                }
                if (($model->secondary_fields = $model->checkField($model->secondary_fields))) {
                    if ((isset(Yii::$app->request->post()['PkpassTemplate']['secondary_fields']))) {
                        $model->secondary_fields = json_encode($model->secondary_fields);
                    } else {
                        $model->secondary_fields = null;
                    }
                }
                if (($model->auxiliary_fields = $model->checkField($model->auxiliary_fields))) {
                    if ((isset(Yii::$app->request->post()['PkpassTemplate']['auxiliary_fields']))) {
                        $model->auxiliary_fields = json_encode($model->auxiliary_fields);
                    } else {
                        $model->auxiliary_fields = null;
                    }

                }

                if ($model->save()) {
                    if ($model->status == $model::STATUS_ACTIVE) {
                        if (!empty(($temp = PkpassTemplate::find()->where(['status' => $model::STATUS_ACTIVE])->andWhere(['!=', 'id', $model->id])->one()))) {
                            $temp->status = $model::STATUS_INACTIVE;
                            $temp->save();
                        }
                    }
                    $modelAttributes = $model->attributes;
                    unset($modelAttributes['status']);
                    unset($modelAttributes['template_name']);
                    unset($modelAttributes['company_id']);
                    $model->status = ($model->oldAttributes)['status'];
                    $model->template_name = ($model->oldAttributes)['template_name'];
                    $model->company_id = ($model->oldAttributes)['company_id'];
                    $changedModel = array_diff_assoc($modelOldAttributes, $modelAttributes);
                    if ($changedModel) {
                        $modelPasses = $model->pkpassPasses;
                        foreach ($modelPasses as $modelPass) {
                            PkpassDevice::updateAll(['update' => true], ['pass_id' => $modelPass->id]);
                        }
                    }
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                    return $this->redirect(Url::toRoute(['update', 'id' => $model->id]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                    return $this->redirect(Url::toRoute(['update', 'id' => $model->id]));
                }
//            return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if (Yii::$app->request->post('id_d') || Yii::$app->request->post('id_d') == '0') {
            $model->load(Yii::$app->request->post());
            if (!$model->icon) {
                $model->icon = $modelOldAttributes['icon'];
                if (!$model->icon) {
                    $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                }
            }
            if (!$model->logo) {
                $model->logo = $modelOldAttributes['logo'];
                if (!$model->icon) {
                    $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                }
            }
            if (!$model->strip) {
                $model->strip = $modelOldAttributes['strip'];
                if (!$model->strip) {
                    $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                }
            }
            $fields_id = json_decode(Yii::$app->request->post('fields_id_d'), true);
            $name_d = Yii::$app->request->post('name_d');
            $key = explode('_', $name_d);
            $id_d = Yii::$app->request->post('id_d');
            $id = Yii::$app->request->post('id');
            if ($key['1'] == 'fields') {
                $arrM = $model->$name_d;
                unset($arrM[$id_d]);
                if ($arrM) {
                    $model->$name_d = $arrM;
                } else {
                    $model->$name_d = null;
                }
            } else {
                $arr = Yii::$app->request->post();
                unset($arr['PkpassTemplate'][$key[0] . '_fields'][$id_d]);
                unset($fields_id[$name_d][$id_d]);
                $model->load($arr);
                if (!$model->icon) {
                    $model->icon = $modelOldAttributes['icon'];
                    if (!$model->icon) {
                        $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                    }
                }
                if (!$model->logo) {
                    $model->logo = $modelOldAttributes['logo'];
                    if (!$model->icon) {
                        $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                    }
                }
                if (!$model->strip) {
                    $model->strip = $modelOldAttributes['strip'];
                    if (!$model->strip) {
                        $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                    }
                }
            }

            return $this->renderAjax('update', [
                'model' => $model,
                'fields_id' => $fields_id,
                'id' => $id
            ]);
        }

        if (Yii::$app->request->post('add_header') ||
            Yii::$app->request->post('add_primary') ||
            Yii::$app->request->post('add_secondary') ||
            Yii::$app->request->post('add_auxiliary') ||
            Yii::$app->request->post('add_back')) {

            $id = Yii::$app->request->post('id');

            if (Yii::$app->request->post('add_header')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->header_fields, 'header', Yii::$app->request->post('add_header'));
            }
            if (Yii::$app->request->post('add_primary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->primary_fields, 'primary', Yii::$app->request->post('add_primary'));
            }
            if (Yii::$app->request->post('add_secondary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->secondary_fields, 'secondary', Yii::$app->request->post('add_secondary'));
            }
            if (Yii::$app->request->post('add_auxiliary')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->auxiliary_fields, 'auxiliary', Yii::$app->request->post('add_auxiliary'));
            }
            if (Yii::$app->request->post('add_back')) {
                $model->load(Yii::$app->request->post());
                $fields_id = $model->createField($model->back_fields, 'back', Yii::$app->request->post('add_back'));
            }
            if (!$model->icon) {
                $model->icon = $modelOldAttributes['icon'];
                if (!$model->icon) {
                    $model->icon = $model::DEFAULT_GRAFPHIC . 'icon';
                }
            }
            if (!$model->logo) {
                $model->logo = $modelOldAttributes['logo'];
                if (!$model->icon) {
                    $model->logo = $model::DEFAULT_GRAFPHIC . 'logo';
                }
            }
            if (!$model->strip) {
                $model->strip = $modelOldAttributes['strip'];
                if (!$model->strip) {
                    $model->strip = $model::DEFAULT_GRAFPHIC . 'strip';
                }
            }
            return $this->renderAjax('update', [
                'model' => $model,
                'fields_id' => $fields_id,
                'id' => $id
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'fields_id' => $fields_id,
            'id' => '1-1'
        ]);
    }

    /**
     * Deletes an existing PkpassTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('pkpass_template.delete');
        $this->findModel($id)->delete();
        PkpassPass::deleteAll(['template_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the PkpassTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return PkpassTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PkpassTemplate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
