<?php

namespace backend\modules\pkpass\controllers;

use backend\modules\pkpass\models\PkpassDevice;
use backend\modules\pkpass\models\PkpassPass;
use Yii;
use yii\rest\Controller;


/**
 * PkpassTemplateController implements the CRUD actions for PkpassTemplate model.
 * @property string $date
 * @property string $get
 * @property string $post
 * @property string $log
 * @property string $headers
 *
 */
class PkpassWebServiceController extends Controller
{

    private $date;

    public function __construct($id, $module, $config = [])
    {
        $this->date = date("Y-m-d H:i:s");

        parent::__construct($id, $module, $config = []);
    }

    /**
     * Registering Device, when Pass is added to the IOS Wallet app
     *
     * GET    -> array [type => (string) , pass => (string) , device => (string)]
     * HEADER -> authorization => (string) = $application . ' ' . $pass_token
     */
    public function actionRegistration()
    {
        if (Yii::$app->request->get('device') && Yii::$app->request->get('pass') && Yii::$app->request->get('type')) {
            $headers = Yii::$app->request->headers;
            $authorization = $headers->get('authorization');
            $userAgent = $headers->get('user-agent');
            $userAgent = explode("/", $userAgent);
            $authorization = explode(" ", $authorization);
            $application = $userAgent[0];
            $passToken = $authorization[1];
            $device = Yii::$app->request->get('device');
            $passSerialNumber = Yii::$app->request->get('pass');
            $type = Yii::$app->request->get('type');
            if (($modelPass = $this->findModelPass($passToken, $passSerialNumber, $type))) {
                if ($this->findModelDevice($application, $type, $modelPass->id)) {
                    $this->log('OK - actionRegistration()', '[HTTP 200] OK');
                    Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                    Yii::$app->response->setStatusCode(200);
                    Yii::$app->response->send();
                } else {
                    $modelDevice = new PkpassDevice();
                    $modelDevice->pass_id = $modelPass->id;
                    $modelDevice->application = $application;
                    $modelDevice->device_token = $device;
                    $modelDevice->pass_type_identifier = $type;
                    if ($modelDevice->save()) {
                        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                        Yii::$app->response->setStatusCode(201);
                        Yii::$app->response->send();

                        $this->log('OK - actionRegistration()', '[HTTP 201] OK');
                    } else {
                        $this->log('ERROR - actionRegistration() - PkpassDevice->save', $modelDevice);
                    }
                }
            } else {
                $this->log('ERROR - actionRegistration() - findModelPass()', '[HTTP 401] passToken => ' . $passToken . ' | passSerialNumber => ' . $passSerialNumber . ' | type => ' . $type);
                Yii::$app->response->setStatusCode(401);
                Yii::$app->response->send();
            }
        }
    }

    /**
     * Registering Device, when Pass is added to the Android Wallet app
     *
     * GET    -> array [type => (string) , pass => (string) , device => (string)]
     * HEADER -> authorization => (string) = $application . ' ' . $pass_token
     */
    public function actionRegistrationAttido()
    {
        if (Yii::$app->request->get('device') && Yii::$app->request->get('pass') && Yii::$app->request->get('type')) {
            $headers = Yii::$app->request->headers;
            $authorization = $headers->get('authorization');
            $userAgent = $headers->get('user-agent');
            $userAgent = explode("/", $userAgent);
            $authorization = explode(" ", $authorization);
            $application = $userAgent[0];
            $passToken = $authorization[1];
            $device = Yii::$app->request->get('device');
            $passSerialNumber = Yii::$app->request->get('pass');
            $type = Yii::$app->request->get('type');
            if (($modelPass = $this->findModelPass($passToken, $passSerialNumber, $type))) {
                if ($this->findModelDevice($application, $type, $modelPass->id)) {
                    $this->log('OK - actionRegistrationAttido()', '[HTTP 200] OK');
                    Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                    Yii::$app->response->setStatusCode(200);
                    Yii::$app->response->send();
                } else {
                    $modelDevice = new PkpassDevice();
                    $modelDevice->pass_id = $modelPass->id;
                    $modelDevice->application = $application;
                    $modelDevice->device_token = $device;
                    $modelDevice->pass_type_identifier = $type;
                    if ($modelDevice->save()) {
                        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                        Yii::$app->response->setStatusCode(201);
                        Yii::$app->response->send();

                        $this->log('OK - actionRegistrationAttido()', '[HTTP 201] OK');
                    } else {
                        $this->log('ERROR - actionRegistrationAttido() - PkpassDevice->save', $modelDevice);
                    }
                }
            } else {
                $this->log('ERROR - actionRegistrationAttido() - findModelPass()', '[HTTP 401] passToken => ' . $passToken . ' | passSerialNumber => ' . $passSerialNumber . ' | type => ' . $type);
                Yii::$app->response->setStatusCode(401);
                Yii::$app->response->send();
            }
        }
    }

    /**
     * Getting the Serial Numbers for Passes Associated with a Device
     *  device , type , tag
     */
    public function actionPasses()
    {
        $this->log('OK - actionPasses()', '[HTTP 204] OK');
        Yii::$app->response->setStatusCode(204);
        Yii::$app->response->send();
    }


    /**
     * Getting the Latest Version of a Pass
     * type , pass
     *
     * @throws yii\web\RangeNotSatisfiableHttpException
     */
    public function actionUpdate()
    {
        if (Yii::$app->request->get('pass') && Yii::$app->request->get('type')) {
            $passSerialNumber = Yii::$app->request->get('pass');
            $type = Yii::$app->request->get('type');
            $headers = Yii::$app->request->headers;
            $authorization = $headers->get('authorization');
            $userAgent = $headers->get('user-agent');
            $userAgent = explode("/", $userAgent);
            $authorization = explode(" ", $authorization);
            $application = $userAgent[0];
            $passToken = $authorization[1];
            if (($modelPass = $this->findModelPass($passToken, $passSerialNumber, $type))) {
                if (($modelDevice = $this->findModelDevice($application, $type, $modelPass->id))) {
                    if ($modelDevice->update) {

                        $modelPkpassTemplate = $modelPass->pkpassTemplate;

                        $passFile=$modelPkpassTemplate->createPass($modelPass);

                        Yii::$app->response->setStatusCode(200);
                        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                        Yii::$app->response->sendStreamAsFile($passFile, $passSerialNumber . '_pass.pkpass', ['mimeType' => 'application/vnd.apple.pkpass', 'inline' => false])->send();

                        $modelDevice->update = false;
                        $modelDevice->save();

                        $this->log('OK - actionUpdate()', '[HTTP 200] OK');
                    } else {
                        $this->log('OK - actionUpdate()', '[HTTP 304] OK');
                        Yii::$app->response->setStatusCode(304);
                        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
                        Yii::$app->response->send();
                    }
                } else {
                    $this->log('ERROR - actionUpdate() - findModelDevice()', '[HTTP 404] passToken => ' . $passToken . ' | passSerialNumber => ' . $passSerialNumber . ' | type => ' . $type);
                    Yii::$app->response->setStatusCode(404);
                    Yii::$app->response->send();
                }
            } else {
                $this->log('ERROR - actionUpdate() - findModelPass()', '[HTTP 401] passToken => ' . $passToken . ' | passSerialNumber => ' . $passSerialNumber . ' | type => ' . $type);
                Yii::$app->response->setStatusCode(401);
                Yii::$app->response->send();
            }
        }
    }


    /**
     * Unregistering Device, when Pass removed from the Wallet app
     *
     * GET    -> array [type => (string) , pass => (string) , device => (string)]
     * HEADER -> authorization => (string) = $application . ' ' . $pass_token
     *
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        if (Yii::$app->request->get('device') && Yii::$app->request->get('pass') && Yii::$app->request->get('type')) {
            $headers = Yii::$app->request->headers;
            $authorization = $headers->get('authorization');
            $userAgent = $headers->get('user-agent');
            $userAgent = explode("/", $userAgent);
            $authorization = explode(" ", $authorization);
            $application = $userAgent[0];
            $passToken = $authorization[1];
            $device = Yii::$app->request->get('device');
            $type = Yii::$app->request->get('type');
            $passSerialNumber = Yii::$app->request->get('pass');
            if (($modelPass = $this->findModelPass($passToken, $passSerialNumber, $type))) {
                if (($modelDevice = $this->findModelDevice($application, $type, $modelPass->id))) {
                    $this->log('OK - actionDelete()', 'OK');
                    $modelDevice->delete();
                    Yii::$app->response->setStatusCode(200);
                    Yii::$app->response->send();
                } else {
                    $this->log('ERROR - actionDelete() - findModelDevice()', '[HTTP 404] passToken => ' . $device);
                    Yii::$app->response->setStatusCode(404);
                    Yii::$app->response->send();
                }
            } else {
                $this->log('ERROR - actionDelete() - findModelPass()', '[HTTP 401] passToken => ' . $passToken . ' | passSerialNumber => ' . $passSerialNumber . ' | type => ' . $type);
                Yii::$app->response->setStatusCode(401);
                Yii::$app->response->send();
            }
        }
    }

    /**
     * Logging Errors
     *
     * POST -> json
     */
    public function actionLog()
    {

        Yii::$app->response->setStatusCode(200);
        Yii::$app->response->send();

        $this->log('APP LOG head', headers_list());
        $this->log('APP LOG post', Yii::$app->request->post());

    }

    /**
     * Pkpass Device model
     *
     * @param $application
     * @param $device_token
     * @param $pass_type_identifier
     * @return bool|PkpassDevice
     */
    private function findModelDevice($application, $pass_type_identifier, $pass_id)
    {
        if (!empty($model = PkpassDevice::find()->where(['application' => $application, 'pass_type_identifier' => $pass_type_identifier, 'pass_id' => $pass_id])->one())) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * Pkpass Pass model
     *
     * @param $authentication_token
     * @param $serial_number
     * @param $pass_type_identifier
     * @return bool|PkpassPass
     */
    private function findModelPass($authentication_token, $serial_number, $pass_type_identifier)
    {
        if (($model = PkpassPass::find()->where(['authentication_token' => $authentication_token, 'serial_number' => $serial_number, 'pass_type_identifier' => $pass_type_identifier])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * Main log
     *
     * @param mixed $log
     * @param string $note
     */
    private function log($note = '', $log = '')
    {
        error_log(
            "\n\n" . " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " . $note . "\n" .
            $this->date . "\n"
            . print_r($log, true) .
            "\n" . " ---------------------------------------------------------------------------------------------------------------------\n",
            3, "/var/www/cryptomat/frontend/runtime/logs/pkpass/pkpass.log");
    }
}

