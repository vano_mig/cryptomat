<?php

namespace backend\modules\pkpass\controllers;

use backend\modules\pkpass\models\PkpassTemplate;
use Yii;
use backend\modules\pkpass\models\PkpassPass;
use backend\modules\pkpass\models\PkpassPassSearch;
use backend\modules\admin\controllers\FrontendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

/**
 * PkpassPassController implements the CRUD actions for PkpassPass model.
 */
class PkpassPassController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PkpassPass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('pkpass_pass.listview');
        $searchModel = new PkpassPassSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PkpassPass model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('pkpass_pass.view');
        $model = $this->findModel($id);
        $modelPkpassTemplate = $model->pkpassTemplate;

        $modelPkpassTemplate->createPass($model, false);

        $renderer = new ImageRenderer(
            new RendererStyle(700),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);

        return $this->render('view', [
            'model' => $model,
            'writer' => $writer
        ]);
    }

    /**
     * @param $serialNumber
     * @throws yii\web\RangeNotSatisfiableHttpException
     */
    public function actionGetPassIos($serialNumber)
    {
        $passFile = fopen(Yii::getAlias('@webroot') . '/pkpass/temp/' . $serialNumber . '_pass.pkpass', 'rb');
        Yii::$app->response->setStatusCode(200);
        Yii::$app->response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s T'));
        Yii::$app->response->sendStreamAsFile($passFile, 'pass.pkpass', ['mimeType' => 'application/vnd.apple.pkpass', 'inline' => false])->send();
    }

    /**
     * Creates a new PkpassPass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('pkpass_pass.create');
        $model = new PkpassPass();
        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->relevant_date) && $model->relevant_date != "") {
                $model->relevant_date = date_format(date_create_from_format('d-m-Y H:i', $model->relevant_date), 'Y-m-d H:i:s');
            }

            if (isset($model->expiration_date) && $model->expiration_date != "") {
                $model->expiration_date = date_format(date_create_from_format('d-m-Y H:i', $model->expiration_date), 'Y-m-d H:i:s');
            }

            $model->serial_number = uniqid('', true);
            $model->authentication_token = uniqid('token_', true);
            $model->pass_type_identifier = PkpassTemplate::PASS_TYPE_IDENTIFIER;
            $modelUser = $model->user;
            $modelUserProfile = $modelUser->userProfile;

            if (empty($modelUserProfile->qr_key)) {
                $modelUserProfile->qr_key = $modelUserProfile->qr_key();
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
                if (!$modelUserProfile->save()) {
                    Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                    return $this->refresh();
                }
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PkpassPass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('pkpass_pass.update');
        $model = $this->findModel($id);
        if (isset($model->relevant_date) && $model->relevant_date != "") {
            $model->relevant_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->relevant_date), 'd-m-Y H:i');
        }
        if (isset($model->expiration_date) && $model->expiration_date != "") {
            $model->expiration_date = date_format(date_create_from_format('Y-m-d H:i:s', $model->expiration_date), 'd-m-Y H:i');
        }
        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->relevant_date) && $model->relevant_date != "") {
                $model->relevant_date = date_format(date_create_from_format('d-m-Y H:i', $model->relevant_date), 'Y-m-d H:i:s');
            }

            if (isset($model->expiration_date) && $model->expiration_date != "") {
                $model->expiration_date = date_format(date_create_from_format('d-m-Y H:i', $model->expiration_date), 'Y-m-d H:i:s');
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PkpassPass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->accessRules('pkpass_pass.delete');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PkpassPass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PkpassPass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PkpassPass::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

}
