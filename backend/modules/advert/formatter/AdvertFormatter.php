<?php

namespace backend\modules\advert\formatter;

use backend\components\BackendFormatter;
use backend\modules\admin\models\Country;
use backend\modules\admin\models\Language;
use backend\modules\advert\models\Advert;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use http\Url;
use Yii;
use yii\bootstrap4\Html;

class AdvertFormatter extends BackendFormatter
{

    /**
     * Get Fridge status
     * @param $value string
     * @return string
     */
    public function asStatus($value): string
    {
       $model = new Advert();
        $status = $model->getStatuses($value);
        if ($value == $model::STATUS_ACTIVE)
            return Html::tag('span', $status, ['class' => 'badge badge-success']);
        elseif ($value == $model::STATUS_INACTIVE)
            return Html::tag('span', $status, ['class' => 'badge badge-danger']);
        elseif ($value == $model::STATUS_RESERVE)
            return Html::tag('span', $status, ['class' => 'badge badge-warning']);
        return Html::tag('span', $status, ['class' => 'badge badge-primary']);
    }

    /**
     * Get Fridge status error
     * @param $value string
     * @return string
     */
    public function asStatusError($value)
    {
        $model = new Advert();
        $status = $model->getProblemStatus();
       if(array_key_exists($value, $status)) {
            if ($value == $model::STATUS_NOT_ERROR)
                return Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
           elseif ($value == $model::STATUS_CRITICAL_PROBLEM || $value == $model::STATUS_CONNECTION_FAILED || $value == Advert::STATUS_BLOCKED)
                return Html::tag('span', $status[$value], ['class' => 'badge badge-danger']);
            elseif ($value == $model::STATUS_NON_CRITICAL_PROBLEM)
               return Html::tag('span', $status[$value], ['class' => 'badge badge-warning']);
          return Html::tag('span', $status[$value], ['class' => 'badge badge-primary']);
        }
    }

    /**
     * Get link to load video
     * @param $id
     * @return string
     */
    public function asVideo($id, $camera, $video)
    {
        if ($video) {
            $result = Html::a($video . ' <i class="fas fa-file-download"></i>', Url::toRoute(['fridge/download', 'id' => $id, 'camera' => $camera, 'video' => $video]));
        }
        return $result ?? Yii::t('admin', 'no set');
    }

    /**
     * Get fridge name
     * @param $id
     * @return string
     */
    public function asFridgeName($id): string
    {
        $result = Yii::t('admin', 'mot set');
        $fridge = Fridge::findOne($id);
        if ($fridge) {
            $result = $fridge->fridge_name;
        }
        return $result;
    }

    /**
     * Gete filling vode username
     * @param $id
     * @return string
     */
    public function asUser($id): string
    {
        $result = Yii::t('admin', 'mot set');
        $user = User::findOne($id);
        if ($user) {
            $result = '#' . $user->id . '- ' . $user->email;
        }
        return $result;
    }

    /**
     * Get count of products at filling
     * @param $value
     * @return int
     */
    public function asFillingProduct($value): int
    {
        $result = 0;
        if ($value) {
            $data = json_decode($value);
            $data = array($data);
            foreach ($data[0] as $count) {
                $result += $count;
            }
        }
        return $result;
    }

    /**
     * Get product name filling list
     * @param $value
     * @return string
     */
    public function asFillingProductName($value): string
    {
        $result = '';
        if ($value) {
            $data = json_decode($value);
            $data = array($data);
            $company = Company::find()->where(['id' => Yii::$app->user->identity->company_id])->one();
            if ($company) {
                $lang= $company->language;
            } else {
                $lang= Language::find()->where(['main' => Language::ACTIVE])->one()->iso_code;
            }
            foreach ($data as $list) {
                foreach ($list as $key => $item) {
                    $userProduct = UserProduct::findOne($key);
                    $products = $userProduct->productLanguage;
                    $productLang = '';
                    foreach ($products as $product) {
                        if ($product->lang == Yii::$app->language) {
                            $productLang = $product->product . ' - ' . $item . '<br>';
                            $result .= $productLang;
                        }
                    }
                    if (!$productLang) {
                        foreach ($products as $product) {
                            if ($product->lang == $lang) {
                                $productLang = $product->product . ' - ' . $item . '<br>';
                                $result .= $productLang;
                            }
                        }
                    }
                }
            }
        }
        return $result != '' ? $result : Yii::t('admin', 'not set');
    }
}