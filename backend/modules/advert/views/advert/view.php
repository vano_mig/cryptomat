<?php

use backend\modules\admin\formatter\CountryFormatter;
use backend\modules\admin\formatter\LanguageFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\advert\formatter\AdvertFormatter;
use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\advert\models\Advert */

$this->title = $model->advert_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('advert', 'Adverts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'MS Adverts')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-hubspot nav-icon"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?php echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'advert_name',
                            'unique_id',
                            [
                                'attribute' => 'country',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(CountryFormatter::class)->asCountryById($data->country);
                                },
                                'format' => 'raw',
                            ],
                            'city',
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(CompanyFormatter::class)->asCompany($data);
                                },
                                'format' => 'raw',
                                'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(AdvertFormatter::class)->asStatus($data->status);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status_error',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(AdvertFormatter::class)->asStatusError($data->status_error);
                                },
                                'format' => 'raw',
                            ],
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('support.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), Url::toRoute(['update', 'id' => $model->id]), ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can(User::ROLE_ADMIN) && $model->key): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('fridge', 'Generate new key'), Url::toRoute(['generate-key', 'id' => $model->id]), [
                                'class' => 'btn bg-gradient-success btn-block',
                                'data' => [
                                    'confirm' => Yii::t('advert', 'Are you sure you want to generate new key?'),
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('support.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), Url::toRoute(['delete', 'id' => $model->id]), [
                                'class' => 'btn  bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('support.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

