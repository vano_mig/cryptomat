<?php

use backend\modules\admin\formatter\CountryFormatter;
use backend\modules\admin\formatter\LanguageFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\advert\formatter\AdvertFormatter;
use backend\modules\user\formatter\CompanyFormatter;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\advert\models\AdvertSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Advert');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'MS Adverts')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fab fa-hubspot nav-icon"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('support.create')): ?>
                <?php echo Html::a(Yii::t('advert', 'Create Advert'), Url::toRoute(['create']), ['class' => 'btn bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered'],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'advert_name',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'unique_id',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'country',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CountryFormatter::class)->asCountryById($data->company);
                            },
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'city',
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(CompanyFormatter::class)->asCompany($data);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', Company::getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AdvertFormatter::class)->asStatus($data->status);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getStatuses(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                            'headerOptions' => ['class' => 'text-center  my-tw-9'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'attribute' => 'status_error',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AdvertFormatter::class)->asStatusError($data->status_error);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status_error', $searchModel->getProblemStatus(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center  my-tw-9'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => 'frontend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {video} {errors} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);

                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.delete', ['user' => $model]);
                                },
                                'errors' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.view', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>