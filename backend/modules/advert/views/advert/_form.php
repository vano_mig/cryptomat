<?php

use backend\modules\admin\models\Country;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\advert\models\Advert */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col mx-auto max-wr-30">
        <?php echo $form->field($model, 'advert_name')->textInput(['maxlength' => true]) ?>
        <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <?php echo $form->field($model, 'unique_id')->textInput(['maxlength' => true, 'placeholder' => Yii::t('advert', 'Input unique ID')]) ?>
            <?php //echo $form->field($model, 'model_id')->dropDownList((new Advert())->getAdvertlist(), ['prompt'=>Yii::t('advert', 'Select model')]);?>
        <?php } ?>
        <?php echo $form->field($model, 'country')->widget(Select2::class, [
            'data' => (new Country())->getList(),
            'options' => ['placeholder' => Yii::t('advert', 'Select country')],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme' => 'default',
        ]); ?>

        <?php echo $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => Yii::t('advert', 'Input city')]) ?>

        <?php echo $form->field($model, 'company_id')->dropDownList(Company::getList(),
            ['prompt' => Yii::t('advert', 'Select company')]); ?>

        <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => Yii::t('advert', 'Select status')]); ?>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?php echo $form->field($model, 'status_error')->dropDownList($model->getProblemStatus(), ['prompt' => Yii::t('fridge', 'Select status error')]); ?>
        <?php endif ?>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block ']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), \yii\helpers\Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
