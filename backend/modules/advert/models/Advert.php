<?php

namespace backend\modules\advert\models;

use common\components\Date;
use backend\modules\marketing\models\Advertisement;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Advert".
 *
 * @property int|null $id
 * @property string|null $advert_name
 * @property string|null $unique_id
 * @property string|null $country
 * @property string|null $city
 * @property int|null $company_id
 * @property string|null $status
 * @property string|null $status_error
 * @property string|null $key
 * @property int|null $key_active
 * @property int|null $ip_address
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Advert extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_RESERVE = 'reserve';
    const STATUS_FREE = 'free';
    const STATUS_NOT_ERROR = 'not error';
    const STATUS_CRITICAL_PROBLEM = 'critical problem';
    const STATUS_NON_CRITICAL_PROBLEM = 'non critical problem';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_CONNECTION_FAILED ='Connection failed';
    const KEY_INACTIVE = 0;
    const KEY_ACTIVE = 1;
    const STATUS_DELETED = 'deleted';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'advert_name', 'unique_id', 'country', 'city'], 'required'],
            ['advert_name', 'trim'],
            ['unique_id', 'trim'],
            [['unique_id'], 'string', 'length' => [6, 255]],
            [['unique_id', 'country', 'city', 'status', 'key', 'ip_address'], 'string', 'max' => 255],
            ['status', 'default', 'value' => Advert::STATUS_FREE],
            ['status', 'in', 'range' => [Advert::STATUS_ACTIVE, Advert::STATUS_DELETED,
                Advert::STATUS_INACTIVE, Advert::STATUS_RESERVE, Advert::STATUS_FREE]],
            ['status_error', 'default', 'value' => Advert::STATUS_NOT_ERROR],
            [['company_id', 'key_active'], 'integer'],
            ['status_error', 'in', 'range' => [Advert::STATUS_NOT_ERROR, Advert::STATUS_CRITICAL_PROBLEM, Advert::STATUS_NON_CRITICAL_PROBLEM,
                Advert::STATUS_BLOCKED, Advert::STATUS_CONNECTION_FAILED]],
            ['key', 'unique', 'targetClass' => '\frontend\modules\advert\models\Advert',
                'message' => Yii::t('advert', 'This key has already been 
                taken.'), 'on' => 'insert'],
            ['key', 'unique', 'filter' => ['!=', 'id', $this->id],
                'targetClass' => '\frontend\modules\advert\models\Advert',
                'message' => Yii::t('advert', 'This key has already been 
                taken.')],
            ['key_active', 'integer'],
            ['key_active', 'default', 'value' => Advert::KEY_INACTIVE],
            [['created_at', 'updated_at'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'advert_name' => Yii::t('advert', 'Advert Name'),
            'unique_id' => Yii::t('advert', 'Unique ID'),
            'country' => Yii::t('advert', 'Country'),
            'city' => Yii::t('advert', 'City'),
            'company_id' => Yii::t('advert', 'Company'),
            'status' => Yii::t('advert', 'Status'),
            'status_error' => Yii::t('advert', 'Status Error'),
            'key' => Yii::t('advert', 'Key'),
            'key_active' => Yii::t('advert', 'Key Active'),
            'ip_address' => Yii::t('advert', 'IP Address'),
            'created_at' => Yii::t('admin', 'Created'),
            'updated_at' => Yii::t('admin', 'Updated'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * Get relative Company list
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id'=>'company_id']);
    }

    /**
     * Get statuses list
     * @param null $id
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $list = [
            self::STATUS_FREE => Yii::t('advert', 'free'),
            self::STATUS_RESERVE => Yii::t('advert', 'reserve'),
            self::STATUS_INACTIVE => Yii::t('advert', 'inactive'),
            self::STATUS_ACTIVE => Yii::t('advert', 'active'),
            self::STATUS_DELETED => Yii::t('advert', 'deleted'),
        ];
        if (array_key_exists($id, $list))
            return $list[$id];
        return $list;
    }

    /**
     * Get problem status list
     * @return array
     */
    public function getProblemStatus(): array
    {
        return [
            self::STATUS_NOT_ERROR => Yii::t('advert', 'non errors'),
            self::STATUS_NON_CRITICAL_PROBLEM => Yii::t('advert', 'non critical problem'),
            self::STATUS_CRITICAL_PROBLEM => Yii::t('advert', 'critical problem'),
            self::STATUS_BLOCKED => Yii::t('advert', 'blocked'),
            self::STATUS_CONNECTION_FAILED => Yii::t('advert', 'connection failed'),
        ];
    }

    /**
     * Get key statuses list
     * @param null $id
     * @return array|mixed
     */
    public function getKeyStatuses($id = null)
    {
        $list = [
            self::KEY_ACTIVE => Yii::t('advert', 'Activated'),
            self::KEY_INACTIVE => Yii::t('advert', 'Not activate'),
        ];
        if (array_key_exists($id, $list))
            return $list[$id];
        return $list;
    }

    /**
     * Get list of advert modules
     * @return array
     */
    public function getList():array
    {
        $list = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $models = Advert::find()->all();
        } else {
            $models = Advert::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->all();
        }
        if(!empty($models))
        {
            foreach ($models as $model) {
                $list[$model->id] = $model->advert_name;
            }
        }
        return $list;
    }

    /**
     * Get relative AdvertVideo models
     * @return \yii\db\ActiveQuery
     */
    public function getTracks()
    {
        return $this->hasMany(AdvertVideo::class, ['advert_id'=>'id']);
    }

    /**
     * Send video to device
     * @param $track null|array
     * @return bool
     */
    public function sendVideo($track = null):bool
    {
        $result['tracks'] = [];
        $list = $this->tracks;
        $clips = [];
        $delete['deleted'] = [];
        if(!empty($list)) {
            foreach ($list as $item) {
                $clips[$item->advertisement_id] = $item;
            }
        }
        if(!empty($clips)) {
            if($track && !empty($track)) {
                foreach ($track as $item) {
                    if(array_key_exists($item, $clips)) {
                        unset($clips[$item]);
                    } else {
                        $delete['deleted'][] = $item;
                    }
                }
            }
        }
        if (!empty($clips)) {
            foreach ($clips as $video) {
                $data = Advertisement::find()->where(['id'=>$video->advertisement_id, 'status'=>Advertisement::STATUS_ACTIVE])->one();
                if(!$data) {
                    continue;
                }
                $hours = [];
                if($data->hours) {
                    $arrayHour = $data->getHours();
                    $value = json_decode($data->hours);
                    foreach ($value as $item) {
                        $hours[] = $arrayHour[$item];
                    }
                }
                $owner = $data->company_id ? 0 : 1;
                $result['tracks'][] = [
                    'marker'=>(int)$data->id,
                    'start_date'=>$data->start_date,
                    'end_date'=>$data->end_date,
                    'name'=>basename($data->video),
                    'length'=>(float)$data->length,
                    'repeat_per_hour'=>$data->repeat_per_hour,
                    'hours'=>$hours,
                    'owner'=> $owner
                ];
                exec('sudo sshpass -p ' . $this->key . ' scp /var/www/mayer/frontend/web' . $data->video . ' msstore@' . $this->ip_address . ':/home/msstore/FolderDataUpdate/Tracks/');
            }
            if (!empty($result)) {
                if(!empty($delete['deleted'])) {
                    array_merge($delete, $result);
                }
                $text = json_encode($result);
                $command = 'sudo sshpass -p ' . $this->key . ' ssh msstore@' . $this->ip_address . ' \'umask 0000 && echo "' . addslashes($text) . '" > /home/msstore/FolderDataUpdate/FileDescription.json\'';
                exec($command);
            }
        }
        return true;
    }

    /**
     * Generate key to connect
     */
    public function activeKey():void
    {
        $this->key = uniqid();
    }
}