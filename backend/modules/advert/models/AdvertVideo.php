<?php

namespace backend\modules\advert\models;

use backend\modules\marketing\models\Advertisement;
use Yii;

/**
 * This is the model class for table "advert_video".
 * @property int $id
 * @property int|null $advert_id
 * @property int|null $advertisement_id
 */
class AdvertVideo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advert_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['advert_id', 'advertisement_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('advert', 'ID'),
            'advert_id' => Yii::t('advert', 'Advert ID'),
            'advertisement_id' => Yii::t('advert', 'Advertisement ID'),
        ];
    }

    /**
     * Get relative Advert model
     * @return \yii\db\ActiveQuery
     */
    public function getAdvert()
    {
        return $this->hasOne(Advert::class, ['id'=>'advert_id']);

    }

    /**
     * Get relative Advertisement model
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisement()
    {
        return $this->hasOne(Advertisement::class, ['id'=>'advertisement_id']);
    }
}
