<?php

namespace backend\modules\advert;

use Yii;

/**
 * advert module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\advert\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['*'] = [
            'class' => 'yii\i18n\DbMessageSource',
            'forceTranslation' => true,
            'on missingTranslation' => [
                'backend\modules\advert\components\TranslationEventHandler',
                'handleMissingTranslation'
            ],
        ];
    }
}
