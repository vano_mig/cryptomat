<?php

namespace backend\modules\marketing\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\user\models\User;
use Yii;
use backend\modules\marketing\models\Sms;
use backend\modules\marketing\models\SmsSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmsController implements the CRUD actions for Sms model.
 */
class SmsController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('identification_profile.list');
        $searchModel = new SmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sms model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('identification_profile.list');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('identification_profile.list');
        $model = new Sms();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Send Sms
     * @return string|\yii\web\Response
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSend()
    {
        $this->accessRules('identification_profile.list');
        $model = new Sms();
        if(Yii::$app->request->post('Sms')) {
            $model->attributes = Yii::$app->request->post('Sms');
            if(!Yii::$app->user->can(User::ROLE_ADMIN)) {
                $model->company_id = Yii::$app->user->identity->company_id;
            }

            if($model->validate()) {
                $params = [
                    'to'=>$model->send_to,
                    'from'=>'MS Store 24',
                    'test'=>1,
                    'message'=>$model->message,
                    'format'=>'json',
                    'company_id'=>$model->company_id,
                    'date'=>$model->schedule_date
                ];
                if(($status = $model->send($params, true)) === 'success') {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'The message was send successfully!'));
                    return $this->redirect(Url::toRoute(['index']));
                } else {
                    $model->addError('message', $status);
                }
            }
        }
        return $this->render('send', ['model'=>$model]);
    }

    /**
     * Finds the Sms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sms::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
