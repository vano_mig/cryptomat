<?php

namespace backend\modules\marketing\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\admin\models\Currency;
use backend\modules\user\models\User;
use yii\helpers\Url;
use Yii;
use backend\modules\marketing\models\Advertisement;
use backend\modules\marketing\models\AdvertisementSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdvertisementController implements the CRUD actions for Advertisement model.
 */
class AdvertisementController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Advertisement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('identification_profile.list');
        $searchModel = new AdvertisementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advertisement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('identification_profile.list');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advertisement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('identification_profile.list');
        $model = new Advertisement(['scenario' => 'insert']);

        if (Yii::$app->request->post('Advertisement')) {
            $model->attributes = Yii::$app->request->post('Advertisement');
            if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
                $model->company_id = Yii::$app->user->identity->company_id;
            };
            $status = null;
            if ($_FILES['Advertisement']['size']['video'] != false) {
                $status = $model->saveVideo('video');
            }
            if ($model->hours) {
                $model->hours = json_encode($model->hours);
            }

            if ($model->validate() && $status && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            } else {
                if ($status && $status != '1') {
                    $model->clearErrors('video');
                    $model->addError('video', $status);
                }
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Advertisement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('identification_profile.list');
        $model = $this->findModel($id);
        $video = $model->video;
        if (Yii::$app->request->post('Advertisement')) {
            $model->attributes = Yii::$app->request->post('Advertisement');
            if (!Yii::$app->user->can(User::ROLE_ADMIN)) {
                $model->company_id = Yii::$app->user->identity->company_id;
            }

            $status = null;
            if ($_FILES['Advertisement']['size']['video'] != false) {
                $status = $model->saveVideo('video');
            }

            if (!$model->video) {
                $model->video = $video;
            }

            if ($model->hours) {
                $model->hours = json_encode($model->hours);
            }

            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            } else {
                if ($status) {
                    $model->clearErrors('video');
                    $model->addError('video', $status);
                }
            }
        }
        if ($model->hours) {
            $model->hours = json_decode($model->hours);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Advertisement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->accessRules('identification_profile.list');
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Advertisement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advertisement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $model = Advertisement::findOne($id);
        } else {
            $model = Advertisement::find()->where(['id'=>$id, 'company_owner_id'=>Yii::$app->user->identity->company_id]);
        }
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
