<?php

namespace backend\modules\marketing\controllers;

use backend\modules\admin\controllers\FrontendController;
use backend\modules\marketing\models\AddressList;
use backend\modules\marketing\models\EmailSearch;
use Yii;
use backend\modules\marketing\models\Email;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * EmailController implements the CRUD actions for Email model.
 */
class EmailController extends FrontendController
{
    /**
     * Lists all Email models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->accessRules('identification_profile.list');
        $searchModel = new EmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Email model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->accessRules('identification_profile.list');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Send Email
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSend()
    {
        $this->accessRules('identification_profile.list');
        $model = new Email();
        if(Yii::$app->request->post('Email')) {
            $model->attributes = Yii::$app->request->post('Email');
            if(!empty($model->adres_list)) {
                $address = AddressList::findOne($model->adres_list);
                $model->send_to = json_decode($address->user_id, true);
            }
            if ($_FILES['Email']['size']['file'] != false) {
                $model->saveFile('file');
            }
            if($model->validate()) {
                if($status = $model->sendEmail()) {
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'The message was send successfully!'));
                    return $this->redirect(Url::toRoute(['index']));
                }
            }
        }
        return $this->render('send', ['model'=>$model]);
    }

    /**
     * Finds the Email model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Email the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Email::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }


}
