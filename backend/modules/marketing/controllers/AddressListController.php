<?php

namespace backend\modules\marketing\controllers;

use Yii;
use backend\modules\marketing\models\AddressList;
use backend\modules\marketing\models\AddressListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * AddressListController implements the CRUD actions for AddressList model.
 */
class AddressListController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AddressList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddressListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AddressList model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->user_id = json_decode($model->user_id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new AddressList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AddressList();
        if ($model->user_id) {
            $model->user_id = json_decode($model->user_id);
        }
        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->user_id)) {
                $model->addError( Yii::t('user_id','Field can not be blank'));
            } else {
                $model->user_id = json_encode($model->user_id);
            }

            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AddressList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AddressList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AddressList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AddressList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AddressList::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('marketing', 'The requested page does not exist.'));
    }

    /**
     * Get recievers list
     * @return \yii\web\Response
     */
    public function actionRecievers()
    {
        $list['data'] = [];
        $marketing = new AddressList();
        if(Yii::$app->request->post('id')) {
            $model = new ActiveForm();
            $list['data'] = $this->renderAjax('_form_user', ['type'=>Yii::$app->request->post('type'), 'form' => $model, 'model'=>$marketing]);
        }
        return $this->asJson($list);
    }
}
