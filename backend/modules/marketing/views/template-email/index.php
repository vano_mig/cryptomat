<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('email', 'Email Templates');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-envelope-square"></i> <?php echo Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <?php echo  Html::a(Yii::t('template-email', 'Create template'), ['create'],
                ['class' => 'btn bg-gradient-success']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="table-responsive">
                <?php echo  GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5'],
                            'contentOptions' => ['class' => 'align-middle'],
                        ],
                        'name',
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company->name;
                            },
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Company())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'class' => 'frontend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return \Yii::$app->user->can('email.view', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>