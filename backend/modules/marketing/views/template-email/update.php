<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\EmailTemplate */

$this->title = Yii::t('email', 'Update Email Template: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('email', 'Email Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('email', 'Update');
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-envelope-square"></i> <?php echo $this->title; ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>