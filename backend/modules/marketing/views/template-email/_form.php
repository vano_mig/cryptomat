<?php

use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false,
    'enableAjaxValidation' => false]); ?>
    <div class="row">
        <div class="col mx-auto max-wr-30">

            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
            <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                <?php echo $form->field($model, 'company_id')->dropDownList((new Company())->getList(),
                    ['prompt' => Yii::t('email', 'Select company')]) ?>
            <?php endif ?>
        </div>
        <div class="col-12">
            <?php echo $form->field($model, 'content')->textarea(['class' => 'mceEdit']) ?>
        </div>
        <div class="col-12 mt-4 pt-4 border-top">
            <div class="row col-12 col-lg-8 mx-auto">
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
                </div>
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
$js = "
$(window).on('load', function () {
    tinymce.init({
        mode: 'specific_textareas',
        editor_selector: 'mceEdit',
        height: 500,
        contextmenu: false,
        menubar: true,
//        plugins: ['link preview code image'],
        plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink directionality code visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
        mobile: {
            plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode code visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker textpattern noneditable help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable'
        },
//        toolbar: 'undo redo | removeformat | bold underline italic strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview code',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample |code | a11ycheck ltr rtl',
//        branding: false,
///        image_advtab: true,
    });
    return false;
});    
";

$this->registerJs($js, yii\web\View::POS_END);