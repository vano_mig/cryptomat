<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\EmailTemplate */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('email', 'Email Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-envelope-square"></i> <?php echo Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?php echo  DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'template_id',
                            'name',
                            'description',
                            [
                                'attribute' => 'company_id',
                                'value' => function ($data) {
                                    return $data->company->name;
                                },
                                'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            ],
                            [
                                'attribute' => 'content',
                                'value' => function ($data) {
                                    return $data->getView();
                                },
                                'format' => 'raw',
                            ],
                            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('email.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>