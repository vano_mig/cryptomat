<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\marketing\models\EmailTemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo  $form->field($model, 'id') ?>

    <?php echo  $form->field($model, 'name') ?>

    <?php echo  $form->field($model, 'template_id') ?>

    <?php echo  $form->field($model, 'company_id') ?>

    <?php echo  $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?php echo  Html::submitButton(Yii::t('email', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo  Html::resetButton(Yii::t('email', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
