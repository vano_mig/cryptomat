<?php

use backend\modules\marketing\models\AddressList;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\Sms */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col mx-auto max-wr-30">
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?php echo  $form->field($model, 'company_id')->dropDownList(Company::getList(), ['prompt' => Yii::t('sms', 'Select company')]) ?>
        <?php endif ?>
        <?php echo  $form->field($model, 'address_list')->dropDownList((new AddressList())->getList(AddressList::TYPE_SMS), ['prompt' => Yii::t('email', 'Select adres list')]) ?>
        <?php echo  $form->field($model, 'send_to')->widget(Select2::class, [
            'data' => (new User())->getUserList(),
            'options' => ['placeholder' => Yii::t('fridge', 'Click for select receiver')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
            'theme' => 'default',
        ]); ?>

        <?php echo  $form->field($model, 'message')->textarea(['rows' => 6]) ?>

        <?php echo  $form->field($model, 'schedule_date')->widget(DateTimePicker::class, [
            'options' => ['placeholder' => Yii::t('product', 'Choose date:')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd H:i:00',
            ],]) ?>

    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

