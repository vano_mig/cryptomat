<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\marketing\formatter\AddressListFormatter;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\AddressList */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('marketing', 'Address Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-address-book nav-icon"></i><?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto max-wr-40">
                <div class="table-responsive">
                    <?php echo  DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'type',
                            'name',
                            'company_id',

                            [
                                'attribute' => 'user_id',
                                'value' => function ($data) {
                                    return Yii::$app->formatter->load(AddressListFormatter::class)->asUser($data->user_id);
                                },
                                'format' => 'html',
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('address_list.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('address_list.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('address_list.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), ['index'], ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>