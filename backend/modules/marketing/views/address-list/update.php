<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\AddressList */

$this->title = Yii::t('marketing', 'Update Address List: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('marketing', 'Address Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('marketing', 'Update');
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Address List')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-address-book nav-icon"></i><?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo $this->render('_form', [
                'model' => $model,

            ]) ?>
        </div>
    </div>
</div>
