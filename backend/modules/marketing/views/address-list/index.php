<?php

use backend\modules\admin\widgets\yii\GridView;
use backend\modules\marketing\formatter\AddressListFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\AddressListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('marketing', 'Address Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-address-book nav-icon"></i>
                <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php //if (Yii::$app->user->can('address_list.create')): ?>
                <?php echo Html::a(Yii::t('admin', 'Add Address Lists'), Url::toRoute(['create']),
                    ['class' => 'btn bg-gradient-success']) ?>
            <?php //endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AddressListFormatter::class)->asUser(json_decode($data->user_id));
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company->name;
                            },
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', (new Company())->getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'type',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AddressListFormatter::class)->asType($data);
                            },
                            'format' => 'html',
                        ],
                        [
                            'class' => 'frontend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                                ],

                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('email.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('email.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('email.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>