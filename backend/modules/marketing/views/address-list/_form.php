<?php

use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\AddressList */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="company-package-form">
    <div class="col mx-auto max-wr-40 list">
        <?php echo $form->field($model, 'name')->textInput([ 'maxlength' => true, 'placeholder' => Yii::t('address_list', 'Input name' )]); ?>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?php echo  $form->field($model, 'company_id')->dropDownList((new Company())->getList(), ['prompt' => Yii::t('email', 'Select company')]) ?>
        <?php endif ?>
        <?php echo $form->field($model, 'type')->dropDownList($model->getTypes(),
            ['prompt' => Yii::t('address_list', 'Select type')]); ?>
        <?php echo  $form->field($model, 'user_id')->widget(Select2::class, [
            'data' => (new User())->getUserListToEmail(),
            'options' => ['placeholder' => Yii::t('email', 'Select receiver')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
            'theme' => 'default',
        ]); ?>

    </div>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'),
                ['class' => 'btn bg-gradient-success btn-block']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php
$script = "
    $('#addresslist-type').on('change', function() {
        var value = $('option:selected', this).val();
        if(value != '' && value != null) {
             $.ajax({
            url: '" . Url::toRoute('/marketing/address-list/recievers') . "',
            type:'post',
            data:{id:'list', type:value},
            dataType:'json',
            success: function(res) {
                if(res) {
                console.log(res.data);
                    $('#addresslist-user_id').remove();
                    $('.field-addresslist-user_id').remove();
                    $('.list').append(res.data);
                } else {
                    console.log('no');
                }
            },
            error: function() {
                alert('Connection error');
            }
            });
        }
        return false;
    });
    ";
$this->registerJs($script, yii\web\View::POS_END);
?>