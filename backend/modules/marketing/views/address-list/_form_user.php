<?php

use backend\modules\user\models\User;
use kartik\select2\Select2;

echo $form->field($model, 'user_id')->widget(Select2::class, [
    'data' => $type == $model::TYPE_SMS ? (new User())->getUserList() : (new User())->getUserListToEmail(),
    'options' => ['placeholder' => Yii::t('email', 'Select receiver')],
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true
    ],
    'theme' => 'default',
]); ?>