<?php

/* @var $model \backend\modules\marketing\models\Advertisement */
/* @var $this \yii\web\View */

use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\marketing\formatter\AdvertisementFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Advertisements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-ad"></i> <?php echo \yii\bootstrap4\Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <div class="row col-12 m-0 p-0">
                <div class="col min-wr-18 max-wr-40 mx-auto p-0 m-0">
                    <?php if ($model->video): ?>
                        <video class="col p-0 m-0 max-wr-40 elevation-3 border border-r-5 border" controls="controls"
                               muted="" loop="loop">
                            <source src="<?php echo Yii::getAlias('@web') . $model->video ?>">
                        </video>
                    <?php endif ?>
                </div>
                <div class="col mx-auto max-wr-40">
                    <div class="table-responsive">
                        <?php echo  DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                [
                                    'attribute' => 'company_owner_id',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(AdvertisementFormatter::class)->asCompanyName($data);
                                    },
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'company_id',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(AdvertisementFormatter::class)->asCompanyName($data);
                                    },
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute'=>'currency_id',
                                    'value'=>function($id){
                                        return Yii::$app->formatter->load(CurrencyFormatter::class)->currencyGetNameById($id->currency_id);
                                    },
                                    'format'=>'html',
                                ],
                                'name',
                                [
                                    'attribute' => 'advert',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(AdvertisementFormatter::class)->asAdvert($data->advert);
                                    },
                                    'format' => 'html',
                                ],
                                'price',
                                'length',
                                'repeat_per_hour',
                                [
                                    'attribute' => 'hours',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(AdvertisementFormatter::class)->asHours($data);
                                    },
                                    'format'=>'html',
                                ],
                                'start_date',
                                'end_date',
                                [
                                    'attribute' => 'status',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->load(AdvertisementFormatter::class)->asStatus($data->status);
                                    },
                                    'format' => 'raw',
                                ],
                                'created_at',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto my-auto">
                    <?php if (Yii::$app->user->can('support.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'),
                                Url::toRoute(['update', 'id' => $model->id]),
                                ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('support.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                [
                                    'class' => 'btn bg-gradient-danger btn-block',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                        </div>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('support.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                                ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
