<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\Advertisement|yii\db\ActiveRecord */

use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\admin\models\Currency;
use backend\modules\advert\models\Advert;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'test',
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<div class="col mx-auto max-wr-30">
    <?php echo $form->field($model, 'name')->textInput(['placeholder' => Yii::t('advertisement', 'Input name')]) ?>

    <?php echo $form->field($model, 'price')->textInput(['placeholder' => Yii::t('advertisement', 'Input price')]) ?>

        <?php echo $form->field($model, 'currency_id')->dropDownList((new Currency())
            ->getCurrencyList()); ?>
        <?php echo $form->field($model, 'company_owner_id')->dropDownList(Company::getList(),
            ['prompt' => Yii::t('product', 'Select company - advert owner') . ":"]); ?>
    <?php echo $form->field($model, 'company_id')->dropDownList(Company::getList(),
        ['prompt' => Yii::t('advertisement', 'Select company-agent')]) ?>

    <?php echo $form->field($model, 'advert')->widget(Select2::class, [
        'data' => (new Advert())->getlist(),
        'options' => ['placeholder' => Yii::t('advertisement', 'Select advert'), 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => false,
        ],
        'theme' => 'default',
    ]); ?>

    <?php echo $form->field($model, 'repeat_per_hour')->textInput([
        'placeholder' => Yii::t('advertisement', 'Input count repeat')
    ]); ?>

    <?php echo $form->field($model, 'hours')->widget(Select2::class, [
        'data' => $model->getHours(),
        'options' => ['placeholder' => Yii::t('drinks', 'Select hours'), 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'theme' => 'default',
    ]); ?>

    <?php echo $form->field($model, 'start_date')->widget(DatePicker::class, [
        'options' => ['placeholder' => Yii::t('admin', 'Choose start date:')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]);
    ?>
    <?php echo $form->field($model, 'end_date')->widget(DatePicker::class, [
        'options' => ['placeholder' => Yii::t('admin', 'Choose end date:')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]);
    ?>
    <?php if (!$model->isNewRecord && $model->video): ?>
        <?php echo basename($model->video); ?>
    <?php endif ?>
    <?php echo $form->field($model, 'video')->fileInput() ?>

    <?php echo $form->field($model, 'status')->dropDownList($model->getStatuses(),
        ['prompt' => Yii::t('advertisement', 'Select status')]) ?>
</div>
<div class="col-12 mt-4 pt-4 border-top">
    <div class="row col-12 col-lg-8 mx-auto">
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::submitButton(Yii::t('admin', 'Save'),
                ['class' => 'btn bg-gradient-success btn-block']) ?>
        </div>
        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']),
                ['class' => 'btn bg-gradient-primary btn-block ']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

