<?php

/* @var $searchModel backend\modules\marketing\models\AdvertisementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */

use backend\modules\admin\formatter\CurrencyFormatter;
use backend\modules\admin\models\Currency;
use backend\modules\marketing\formatter\AdvertisementFormatter;
use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use yii\bootstrap4\Html;

$this->title = Yii::t('admin', 'Advertisements');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-ad"></i> <?php echo \yii\bootstrap4\Html::encode($this->title) ?>
            </h3>
        </div>
        <div class="card-body">
            <?php echo  Html::a(Yii::t('advertisement', 'Create advertisement'), ['create'],
                ['class' => 'btn bg-gradient-success']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="table-responsive">
                <?php echo  GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'company_owner_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AdvertisementFormatter::class)->asCompanyName($data->company_owner_id);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AdvertisementFormatter::class)->asCompanyName($data->company_id);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'currency_id',
                            'value' => function ($id) {
                                return Yii::$app->formatter->load(CurrencyFormatter::class)-> currencyGetNameById($id->currency_id);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'currency_id', (new Currency())->getCurrencyList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'html',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        'name',
                        'price',
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(AdvertisementFormatter::class)->asStatus($data->status);
                            },
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => 'frontend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{add} {view} {update} {delete}  ',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('support.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
