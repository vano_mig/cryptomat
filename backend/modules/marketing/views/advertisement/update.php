<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\Advertisement */

use backend\modules\admin\widgets\yii\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('admin','Update Caterer').': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Advertisements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-ad"></i>
                <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php echo  $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>