<?php

use backend\modules\admin\widgets\yii\Breadcrumbs;
use backend\modules\admin\widgets\yii\GridView;
use backend\modules\marketing\formatter\EmailFormatter;
use backend\modules\marketing\models\AddressList;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\EmailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('email', 'Email');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-header">
    <?php echo  Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Marketing')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-envelope-open-text"></i> <?php echo Yii::t('admin', 'Email'); ?>
            </h3>
        </div>
        <div class="card-body">
            <?php //if (Yii::$app->user->can('email.create')): ?>
                <?php echo Html::a(Yii::t('email', 'Send email'), Url::toRoute(['send']),
                    ['class' => 'btn  bg-gradient-success']) ?>
            <?php //endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => Yii::$app->user->can(User::ROLE_ADMIN) ? $searchModel : false,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['class' => 'my-tw-5']
                        ],
                        [
                            'attribute' => 'company_id',
                            'value' => function ($data) {
                                return $data->company->name;
                            },
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'filter' => Html::activeDropDownList($searchModel, 'company_id', Company::getList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'send_to',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(EmailFormatter::class)->asUser($data->send_to);
                            },
                            'format' => 'html',
                        ],
                        'subject',
                        [
                            'attribute' => 'address_list',
                            'value' => function ($data) {

                            },
                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN) ? true : false,
                            'filter' => Html::activeDropDownList($searchModel, 'address_list', (new AddressList())->getList(AddressList::TYPE_EMAIL),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return Yii::$app->formatter->load(EmailFormatter::class)->asStatus($data->status);
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'status', $searchModel->getStatusList(),
                                ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control']),
                            'format' => 'raw',
                            'headerOptions' => ['class' => 'text-center my-tw-8'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => 'frontend\modules\admin\widgets\yii\ActionColumn',
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('gii.view', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>