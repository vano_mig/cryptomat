<?php

use backend\modules\marketing\models\AddressList;
use backend\modules\marketing\models\EmailTemplate;
use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\Sms */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'], 'enableClientValidation' => false,
    'enableAjaxValidation' => false]); ?>
<div class="row">
    <div class="col mx-auto max-wr-30">
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?php echo  $form->field($model, 'company_id')->dropDownList(Company::getList(), ['prompt' => Yii::t('email', 'Select company')]) ?>
        <?php endif ?>

        <?php echo  $form->field($model, 'address_list')->dropDownList((new AddressList())->getList(AddressList::TYPE_EMAIL), ['prompt' => Yii::t('email', 'Select adres list')]) ?>

        <p><?php echo Yii::t('admin', 'OR input message')?></p>


        <?php echo  $form->field($model, 'send_to')->widget(Select2::class, [
            'data' => (new User())->getUserListToEmail(),
            'options' => ['placeholder' => Yii::t('email', 'Select receiver')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
            'theme' => 'default',
        ]); ?>

        <?php echo  $form->field($model, 'subject')->textInput(['placeholder'=>Yii::t('email', 'Input subject')]) ?>

        <?php echo  $form->field($model, 'file')->fileInput() ?>

        <?php echo  $form->field($model, 'template')->dropDownList((new EmailTemplate())->getList(), ['prompt'=>Yii::t('email', 'Select template')]);?>

    </div>
    <div class="col-12">
        <?php echo  $form->field($model, 'message')->textarea(['class' => 'mceEdit']) ?>
    </div>
    <div class="col-12 mt-4 pt-4 border-top">
        <div class="row col-12 col-lg-8 mx-auto">
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']) ?>
            </div>
            <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block ']); ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$js = "
$(window).on('load', function () {
    tinymce.init({
        mode: 'specific_textareas',
        editor_selector: 'mceEdit',
        height: 500,
        contextmenu: false,
        menubar: true,
//        plugins: ['link preview code image'],
        plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink directionality code visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
        mobile: {
            plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode code visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker textpattern noneditable help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable'
        },
//        toolbar: 'undo redo | removeformat | bold underline italic strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview code',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample |code | a11ycheck ltr rtl',
//        branding: false,
///        image_advtab: true,
    });
    return false;
});    
";

$this->registerJs($js, yii\web\View::POS_END);

