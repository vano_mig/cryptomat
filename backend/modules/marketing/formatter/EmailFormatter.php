<?php

namespace backend\modules\marketing\formatter;

use backend\components\BackendFormatter;
use backend\modules\marketing\models\Email;
use backend\modules\user\models\User;
use Yii;
use yii\bootstrap4\Html;

class EmailFormatter extends BackendFormatter
{
    /**
     * Get status
     * @param $value integer
     * @return string
     */
    public function asStatus($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new Email();
        $status = $model->getStatusList();
        if(array_key_exists($value, $status)) {
            if ($value == $model::STATUS_SEND) {
                $result =  Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
            } else {
                $result =  Html::tag('span', $status[$value], ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get list users
     * @param $array
     * @return string
     */
    public function asUser($value):string
    {
        $result = Yii::t('admin', 'not set');
        $array = explode(',', $value);
        if(!empty($array)) {
            foreach ($array as $item) {
                $item = (int)$item;
                if($item != 0  || $item != false) {
                    $user = User::findOne($item);
                    $result .= $user->email. ' '. $user->username. ' '. $user->last_name. '<br>';
                }
            }
        }
        return $result;
    }
}