<?php

namespace backend\modules\marketing\formatter;

use backend\components\BackendFormatter;
use backend\modules\advert\models\Advert;
use backend\modules\marketing\models\Advertisement;
use backend\modules\user\models\Company;
use Yii;
use yii\bootstrap4\Html;

class AdvertisementFormatter extends BackendFormatter
{
    /**
     * Get status
     * @param $value integer
     * @return string
     */
    public function asStatus($value): string
    {
        $result = Yii::t('admin', 'not set');
        $model = new Advertisement();
        $status = $model->getStatuses();
        if(array_key_exists($value, $status)) {
            if ($value == $model::STATUS_ACTIVE) {
                $result =  Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
            } else if ($value == $model::STATUS_INACTIVE) {
                $result =  Html::tag('span', $status[$value], ['class' => 'badge badge-secondary']);
            } else {
                $result =  Html::tag('span', $status[$value], ['class' => 'badge badge-danger']);
            }
        }
        return $result;
    }

    /**
     * Get company name
     * @param $id integer
     * @return string
     */
    public function asCompanyName($id): string
    {
        $model = Company::find()->where(['id' => $id])->one();
        if (!$model) {
            return Yii::t('marketing', 'Name did not set for this company');
        }
        return $model->name;
    }

    /**
     * Get list advert
     * @param $array
     * @return string
     */
    public function asAdvert($array):string
    {
        $result = Yii::t('admin', 'not set');
        if(!empty($array)) {
            $result = '';
            foreach ($array as $item){
                $advert = Advert::findOne($item);
                $result .= $advert->advert_name.'<br>';
            }
        }
        return $result;
    }

    /**
     * Get list hours
     * @param $data Advertisement
     * @return string
     */
    public function asHours($data):string
    {
        $result = Yii::t('admin', 'not set');
        $list = $data->getHours();
        if($data->hours) {
            $hours = json_decode($data->hours);
            $result = '';
            foreach ($hours as $item) {
                $result .= $list[$item]."<br>";
            }
        }
        return $result;
    }
}