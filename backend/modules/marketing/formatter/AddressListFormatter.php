<?php

namespace backend\modules\marketing\formatter;

use backend\components\BackendFormatter;
use backend\modules\marketing\models\AddressList;
use backend\modules\user\models\User;
use Yii;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/**
 * Class AddressListFormatter
 * @package frontend\modules\marketing\formatter
 */
class AddressListFormatter extends BackendFormatter
{
    /**
     * Get company name
     * @param $data
     * @return string
     */
    public function asCompany($data): string
    {
        if ($data->company) {
            $result = Html::a($data->company->name, Url::toRoute(['/user/company/view', 'id' => $data->company_id]));
        } else {
            $result = Yii::t('admin', 'not set');
        }
        return $result;
    }

    /**
     * Get currency status
     * @param $data AddressList
     * @return string
     */
    public function asType($data): string
    {
        $types = $data->getTypes();
        if ($data->type == $data::TYPE_SMS) {
            return Html::tag('span', $types[$data->type], ['class' => 'badge badge-success']);
        } elseif ($data->type == $data::TYPE_EMAIL) {
            return Html::tag('span', $types[$data->type], ['class' => 'badge badge-info']);
        }
        return Html::tag('span', $data->type, ['class' => 'badge badge-secondary']);
    }

    /**
     * Get list users
     * @param array $value
     * @return string
     */
    public function asUser($value): string
    {
        $result = '';
        foreach ($value as $item) {
            $user = User::findOne($item);
            $result .= $user->email . ' - ' . $user->username . ' - ' . $user->last_name . '<br>';
        }
        return $result;
    }
}