<?php

namespace backend\modules\marketing\models;

use backend\modules\admin\models\Equipment;
use backend\modules\admin\models\UploadEmail;
use backend\modules\user\models\Company;
use backend\modules\user\models\CompanyBalance;
use backend\modules\user\models\CompanyPackage;
use backend\modules\user\models\User;
use Yii;
use \Mailjet\Resources;
use \Mailjet\Client;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "email".
 *
 * @property int $id
 * @property int|null $company_id
 * @property string|null $send_to
 * @property string|null $address_list
 * @property string|null $from
 * @property string|null $subject
 * @property string|null $message
 * @property float|null $amount
 * @property string|null $status
 * @property string|null $error
 * @property string|null $file
 * @property string|null $created_at
 */
class Email extends \yii\db\ActiveRecord
{
    public const PUBLIC_KEY = '0aeeb26ffae5b58fc538ebf6354982fa';
    public const PRIVATE_KEY = 'ebc7fffd37153729401f383975120d1e';
    public const API_VERSION = 'v3.1';

    public const STATUS_SEND = 'success';
    public const STATUS_ERROR = 'error';

    public $template;
    public $address_list;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'subject'], 'required'],
            [['company_id', 'address_list', 'template'], 'integer'],
            ['message', 'required', 'when'=>function($data) {
                return $data->template == false;
            }],
            ['template', 'required', 'when'=>function($data) {
                return $data->message == false;
            }],
            ['send_to', 'required', 'when'=>function($data) {
                return $data->address_list == false;
            }],
            ['address_list', 'required', 'when'=>function($data) {
                return $data->send_to == false;
            }],
            [['message'], 'string'],
            [['amount'], 'number'],
            [['from', 'subject', 'status', 'error', 'file', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('email', 'Company'),
            'send_to' => Yii::t('email', 'Send To'),
            'address_list' => Yii::t('email', 'Adres List'),
            'from' => Yii::t('email', 'From'),
            'subject' => Yii::t('email', 'Subject'),
            'message' => Yii::t('email', 'Message'),
            'amount' => Yii::t('email', 'Amount'),
            'status' => Yii::t('email', 'Status'),
            'error' => Yii::t('email', 'Error'),
            'file' => Yii::t('email', 'File'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * Send Email
     * @return bool
     */
    public function sendEmail(): bool
    {
        if (Yii::$app->user->can(User::ROLE_ADMIN)) {
            $name = Company::findOne($this->company_id)->name;
        } else {
            $name = Yii::$app->user->identity->company->name;
        }
        $this->from = 'info@msstore24.de';
        $count = count($this->send_to);
        $mj = new Client(Email::PUBLIC_KEY, Email::PRIVATE_KEY, true, ['version' => Email::API_VERSION, 'call' => 'false']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $this->from,
                        'Name' => $name
                    ],
                    'To' => $this->sendTo(),
                    'Subject' => $this->subject,
//                    'TextPart' => $this->message,
                ]
            ]
        ];
        if($this->template) {
            $template = ['TemplateID' =>(integer)$this->template];
            $body['Messages'][0] = $body['Messages'][0] + $template;
            $this->message = 'Template ID '. $this->template;
        } else {
            $message = ['HTMLPart' => $this->message];
            $body['Messages'][0] = $body['Messages'][0] + $message;
        }
        if ($this->file) {
            $array = $this->attachment();
            $body['Messages'][0] = $body['Messages'][0] + $array;
        }
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        if ($response->success()) {
            $result = $response->getData();
            $this->status = $result['Messages'][0]['Status'];

//            $companyBalance = new CompanyBalance();
//            $package = CompanyPackage::find()->where([
//                'company_id' => $this->company_id,
//                'status' => CompanyPackage::STATUS_ACTIVE,
//            ])->all();
//            $cost_email = 0;
//            if ($package) {
//                foreach ($package as $item) {
//                    if (in_array($item->package->type,[Equipment::TYPE_FRIDGE, Equipment::TYPE_MAXI_STORE, Equipment::TYPE_BEVERAGE])) {
//                        $cost_email = $item->package->cost_email;
//                    }
//                    break;
//                }
//            }
//            $cost_email = 0.05;
//
//            $companyBalance->company_id = $this->company_id;
//            $companyBalance->service = CompanyBalance::SERVICE_EMAIL;
//            $companyBalance->cost = $cost_email;
//            $companyBalance->quantity = $count;
//            $companyBalance->message = Yii::t('email', 'Email order');
//            $companyBalance->save();
        } else {
            $this->status = self::STATUS_ERROR;
        }
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get sendTo addresses in array
     * @return array
     */
    public function sendTo():array
    {
        $sendTo = [];
        $send = '';
        foreach ($this->send_to as $item) {
            $user = User::findOne($item);
            $sendTo[] = ['Email' => $user->email, 'Name' => $user->username . ' ' . $user->last_name];
            $send .= $item . ', ';
        }
        $this->send_to = $send;
        return $sendTo;
    }

    /**
     * Upload file
     * @param $file string
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function saveFile($file)
    {
        if ($this->isNewRecord) {
            $id = Advertisement::find()->orderBy('id DESC')->one();
            if ($id === null) {
                $id = 1;
            } else {
                $id = $id->id + 1;
            }
        } else {
            $id = $this->id;
        }

        $path = Yii::getAlias('@backend') . '/web/email/' . $id . '/';
        $video = new UploadEmail;
        $video->videoFile = UploadedFile::getInstance($this, $file);
        $video->videoFile->name = preg_replace('/[#$%^&*()+=\-\[\]\'\s;,\/{}|":<>?~\\\\]/', '_', $video->videoFile->name);

        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                throw new NotFoundHttpException(sprintf('Directory "%s" was not created', $path));
            }
        }

        if (!file_exists($path . $video->videoFile->name)) {

            if ($video->videoFile->size > 104857600) {
                return Yii::t('admin', 'Video file size must be less then 100 MB');
            }
            if (!$this->$file = $video->upload($path)) {
                return $this->addError('admin', $video->getFirstError('videoFile'));
            }
            $this->$file = '/email/' . $id . '/' . $video->videoFile->name;
            return true;
        } elseif (file_exists($path . $video->videoFile->name)) {
            $this->$file = '/email/' . $id . '/' . $video->videoFile->name;
            return true;
        }
    }

    /**
     * Attached files to email
     * @return array
     */
    public function attachment():array
    {
        $name = basename($this->file);
        $path = Yii::$app->basePath . '/web' . $this->file;
        $array['Attachments'] = [
            [
                'ContentType' => "application/octet-stream",
                'Filename' => $name,
                'Base64Content' => base64_encode($path),
            ]
        ];
        return $array;
    }

    /**
     * Get relative Company model
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id'=>'company_id']);
    }

    /**
     * Get statuses list
     * @return array
     */
    public function getStatusList():array
    {
        return [
            self::STATUS_SEND => Yii::t('email', 'Send'),
            self::STATUS_ERROR => Yii::t('email', 'Error'),
        ];
    }
}