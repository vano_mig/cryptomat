<?php

namespace backend\modules\marketing\models;

use backend\modules\user\models\Company;
use backend\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "address_list".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $name
 * @property int|null $company_id
 * @property string|null $user_id
 *
 */
class AddressList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public const TYPE_SMS = 'sms';
    public const TYPE_EMAIL = 'email';

    public static function tableName()
    {
        return 'address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user_id','type', 'company_id'], 'required'],
            [['company_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['user_id', 'safe'],
            ['type', 'in', 'range' => [self::TYPE_SMS, self::TYPE_EMAIL,]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [


            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('address_list', 'Name'),
            'company_id' => Yii::t('address_list', 'Company'),
            'user_id' => Yii::t('address_list', 'User'),
            'type' => Yii::t('address_list', 'Type'),
        ];
    }

    /**
     * Get types list
     * @return array
     */
    public function getTypes(): array
    {
        return [
            AddressList::TYPE_SMS => Yii::t('address_list', 'SMS'),
            AddressList::TYPE_EMAIL => Yii::t('address_list', 'EMAIL'),
        ];
    }

    /**
     * Get relative Company model
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Get address_list list
     * @return array
     */
    public function getList($type): array
    {
        $list = [];
        if(Yii::$app->user->can(User::ROLE_ADMIN)) {
            $models = AddressList::find()->where(['type'=>$type])->all();
        } else {
            $models = AddressList::find()->where(['type'=>$type, 'company_id'=>Yii::$app->user->identity->company_id])->all();
        }
        if (!empty($models)) {
            foreach ($models as $model) {
                $list[$model->id] = $model->name;
            }
        }
        return $list;
    }
}
