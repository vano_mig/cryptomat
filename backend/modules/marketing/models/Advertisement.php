<?php

namespace backend\modules\marketing\models;

use common\components\Date;
use backend\modules\admin\models\UploadVideo;
use backend\modules\advert\models\Advert;
use backend\modules\advert\models\AdvertVideo;
use backend\modules\advert\models\queue\AdvertVideoSend;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "advertisement".
 *
 * @property int $id
 * @property int|null $company_owner_id
 * @property int|null $company_id
 * @property int|null $currency_id
 * @property string|null $price
 * @property string|null $status
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string|null $created_at
 * @property string|null $name
 * @property string|null $length
 * @property integer|null $repeat_per_hour
 * @property string|null $video
 * @property string|null $advert
 * @property string|null $hours
 */
class Advertisement extends ActiveRecord
{
    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE = 'active';
    const STATUS_ERROR = 'error';

    public $advert = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_owner_id', 'currency_id', 'repeat_per_hour', 'advert', 'name', 'start_date', 'end_date', 'price', 'hours'], 'required'],
            ['video', 'required', 'on'=>'insert'],
            [['company_owner_id', 'company_id', 'currency_id', 'repeat_per_hour'], 'integer'],
            [['price'], 'formatNumber'],
            ['status', 'default', 'value'=>self::STATUS_ACTIVE],
            ['status', 'in', 'range'=>[self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_ERROR]],
            [['name', 'length', 'video'], 'string', 'length' => [4,255]],
            [['start_date', 'end_date', 'created_at'], 'safe'],
            ['hours', 'checkHours']

        ];
    }

    /**
     * Validate$attribute to free time
     * @param $attribute
     */
    public function checkHours($attribute)
    {
        $array = [];
        $currentHours = json_decode($this->hours);
        if(!empty($this->adverts)) {
            foreach ($this->adverts as $advert) {
                $list = AdvertVideo::find()->where(['advert_id'=>$advert->advert_id])->andWhere(['!=', 'advertisement_id', $this->id])->all();
                if(empty($list)) {
                   continue;
                }
                foreach ($list as $item) {
                    $track = Advertisement::find()->where(['id'=>$item->advertisement_id, 'status'=>Advertisement::STATUS_ACTIVE])
                        ->andWhere(['<=', 'start_date', $this->end_date])->andWhere(['>=', 'end_date', $this->start_date])->one();
                    if($track && $track->hours) {
                        $hours = json_decode($track->hours);
                        foreach ($hours as $data) {
                            if(in_array($data, $currentHours)) {
                                array_key_exists($data, $array) ? $array[$data] += ($track->length * $track->repeat_per_hour) : $array[$data] = ($track->length * $track->repeat_per_hour);
                            }
                        }
                    }
                }
            }
        }
        if(!empty($array)) {
            $time = $this->getHours();
            foreach ($array as $key=>$item) {
                if ($item > 3600 || $item + ($this->length * $this->repeat_per_hour) > 3600) {
                    $this->addError($attribute, Yii::t('advertisement', 'This time "{n}" is already busy', ['n'=>$time[$key]]));
                }
            }
        }
    }

    /**
     * Validate $attribute to decimal
     * @param string $attribute
     */
    public function formatNumber($attribute)
    {
        $this->$attribute = (float)preg_replace('/,/', '.', $this->$attribute);
        $this->$attribute = number_format($this->$attribute, 2, '.', '');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_owner_id' => Yii::t('advertisement','Owner'),
            'company_id' => Yii::t('advertisement','Operator'),
            'currency_id' => Yii::t('advertisement','Currency'),
            'price' => Yii::t('advertisement','Price'),
            'name' => Yii::t('advertisement','Name'),
            'length' => Yii::t('advertisement','Length'),
            'repeat_per_hour' => Yii::t('advertisement','Repeat per hour'),
            'status' => Yii::t('advertisement','Status'),
            'video' => Yii::t('advertisement','Video File'),
            'start_date' => Yii::t('advertisement','Start Date'),
            'end_date' => Yii::t('advertisement','End Date'),
            'created_at' => Yii::t('admin','Created'),
            'advert' => Yii::t('advertisement','Advert'),
            'hours' => Yii::t('advertisement','Hours'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [ ActiveRecord::EVENT_BEFORE_INSERT => 'created_at'],
                'value' => (new Date())->now(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function beforeSave($insert)
//    {
//        $file = $path = realpath(Yii::$app->basePath.'/web/').$this->video;
//        exec('sudo ffmpeg -i '.$file.' 2>&1', $output);
//        foreach ($output as $s):
//            if(preg_match('/Duration: ([0-9:.]+)/',$s,$m))
//                $this->length = $m[1];
//        endforeach;
//        if($this->length) {
//            $this->convert();
//            $this->save();
//        }
//        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
//    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        AdvertVideo::deleteAll(['advertisement_id'=>$this->id]);
        if(!empty($this->advert)) {
            foreach ($this->advert as $item) {
                $data = new AdvertVideo();
                $data->advertisement_id = $this->id;
                $data->advert_id = $item;
                $data->save();
            }
        }
//        Yii::$app->queue->push(new AdvertVideoSend([
//            'array' => $this->advert,
//        ]));
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();
        $list = $this->adverts;
        if(!empty($list)) {
            foreach ($list as $item) {
                $this->advert[$item->advert_id] = $item->advert_id;
            }
        }

    }

    /**
     * Get list or advertisement statuses
     * @return array|mixed
     */
    public function getStatuses():array
    {
        $array = [
            self::STATUS_ACTIVE => Yii::t('advertisement', 'active'),
            self::STATUS_INACTIVE => Yii::t('advertisement', 'inactive'),
            self::STATUS_ERROR => Yii::t('advertisement', 'error')
        ];
        return $array;
    }

    /**
     * Upload file
     * @param $file string
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function saveVideo($file)
    {
        if ($this->isNewRecord) {
            $id = Advertisement::find()->orderBy('id DESC')->one();
            if ($id === null) {
                $id = 1;
            } else {
                $id = $id->id + 1;
            }
        } else {
            $id = $this->id;
        }

        $path = Yii::getAlias('@backend') . '/web/video/advertisement/' . $id . '/';
        $video = new UploadVideo;
        $video->videoFile = UploadedFile::getInstance($this, $file);
        $video->videoFile->name = preg_replace('/[#$%^&*()+=\-\[\]\'\s;,\/{}|":<>?~\\\\]/', '_', $video->videoFile->name);

        if (!is_dir($path)) {
            if (!mkdir($path, 0777) && !is_dir($path)) {
                throw new NotFoundHttpException(sprintf('Directory "%s" was not created', $path));
            }
        }

        if (!file_exists($path . $video->videoFile->name)) {

            if (!preg_match('/(mp4)$/ui', $video->videoFile->extension, $match)) {
                return Yii::t('admin', 'Unsupported format. Video file must have .mp4 format');
            } elseif ($video->videoFile->size > 104857600) {
                return Yii::t('admin', 'Video file size must be less then 100 MB');
            }
            if (!$this->$file = $video->upload($path)) {
                return $this->addError('admin', $video->getFirstError('videoFile'));
            }
            $this->$file = '/video/advertisement/' . $id . '/' . $video->videoFile->name;
            return true;
        } elseif (file_exists($path . $video->videoFile->name)) {
            $this->$file = '/video/advertisement/' . $id . '/' . $video->videoFile->name;
            return true;
        }
    }

    /**
     * Get Advertisement models list
     * @return array
     */
    public function getList():array
    {
        $list = [];
        $models = Advert::find()->all();
        if(!empty($models))
       {
            foreach ($models as $model) {
                $list[$model->id] = $model->advert;
            }
        }
        return $list;
    }

    /**
     * Get relative AdvertVideo model
     * @return \yii\db\ActiveQuery
     */
    public function getAdverts()
    {
        return $this->hasMany(AdvertVideo::class, ['advertisement_id'=>'id']);
    }

    /**
     * Convert time to seconds
     * @return float|int
     */
    public function convert():bool
    {
        $time = 0;
        $array = explode(':', $this->length);
        foreach ($array as $key=>$value) {
            if($key == 0 && $value != '00') {
             $time += (int)$value * 3600;
            }
            if($key == 1 && $value != '00') {
                $time += (int)$value * 60;

            }
            if($key == 2 && $value != '00') {
                $time += $value;
            }
        }
        $this->length = $time;
        return true;
    }

    /**
     * Ger hours list
     * @return array
     */
    public function getHours():array
    {
        return[
            '0'=>'00:00-01:00',
            '1'=>'01:00-02:00',
            '2'=>'02:00-03:00',
            '3'=>'03:00-04:00',
            '4'=>'04:00-05:00',
            '5'=>'05:00-06:00',
            '6'=>'06:00-07:00',
            '7'=>'07:00-08:00',
            '8'=>'08:00-09:00',
            '9'=>'09:00-10:00',
            '10'=>'10:00-11:00',
            '11'=>'11:00-12:00',
            '12'=>'12:00-13:00',
            '13'=>'13:00-14:00',
            '14'=>'14:00-15:00',
            '15'=>'15:00-16:00',
            '16'=>'16:00-17:00',
            '17'=>'17:00-18:00',
            '18'=>'18:00-19:00',
            '19'=>'19:00-20:00',
            '20'=>'20:00-21:00',
            '21'=>'21:00-22:00',
            '22'=>'22:00-23:00',
            '23'=>'23:00-00:00',
        ];
    }
}
