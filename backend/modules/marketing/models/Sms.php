<?php

namespace backend\modules\marketing\models;

use backend\modules\user\models\Company;
use backend\modules\user\models\CompanyBalance;
use backend\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property int $id
 * @property int|null $company_id
 * @property string|null $send_to
 * @property string|null $address_list
 * @property string|null $message
 * @property float|null $amount
 * @property string|null $sms_id
 * @property string|null $status
 * @property string|null $error
 * @property string|null $schedule_date
 * @property string|null $created_at
 */
class Sms extends \yii\db\ActiveRecord
{
    public const TOKEN = 'I8YutB7pfi0LvocMFysphykW7nFQNDudORVhA2rz';
    public const URL_BACKUP = 'https://api2.smsapi.com/sms.do';
    public const URL = 'sms_url';

    public const STATUS_SUCCESS = 'success';
    public const STATUS_ERROR = 'error';

    public $address_list;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'message', 'send_to'], 'required'],
            ['company_id', 'default', 'value'=>3],
            [['company_id',  'address_list', ], 'integer'],
            [['message', 'schedule_date'], 'string'],
            [['status', 'error', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('sms', 'Company'),
            'send_to' => Yii::t('sms', 'Receiver'),
            'address_list' => Yii::t('sms', 'Adres List'),
            'message' => Yii::t('sms', 'Message'),
            'status' => Yii::t('sms', 'Status'),
            'error' => Yii::t('sms', 'Error'),
            'schedule_date' => Yii::t('sms', 'Schedule date'),
            'created_at' => Yii::t('admin', 'Created'),
        ];
    }

    /**
     * Send sms
     * @param $params array
     * @param bool $backup
     * @return string
     */
    public function send($params, $backup = false):string
    {
        $content = null;

        if ($backup == true) {
            $url = Sms::URL_BACKUP;
        } else {
            $url = Sms::URL;
        }
        $array = $params;
        if($array['date'] != false) {
            $array['date'] = strtotime($array['date']);
        } else {
            unset($array['date']);
        }
        $sendTo = $this->getSendTo($array['to']);
        $send = implode(',', $sendTo);
        $array['to'] = $send;
        unset($array['company_id']);
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $array);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer ".Sms::TOKEN
        ));

        $content = curl_exec($c);
        $http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);
        if ($http_status != 200 && $backup == false) {
            $backup = true;
            $this->send($params, $backup);
        }
        $result = (array)json_decode($content, true);

        if(array_key_exists('error', $result)) {
            $model = new Sms();
            $model->status = self::STATUS_ERROR;
            $model->error = $result['error'].', '.$result['message'];
            $model->send_to = $sendTo[0];
            $model->company_id = $params['company_id'];
            $model->message = $params['message'];
            $model->schedule_date = $params['date'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            $response = $result['message'];
        } else {
            $quantity = 0;
            $cost = 0;

            foreach ($result['list'] as $item) {
                $model = new Sms();
                $model->status = self::STATUS_SUCCESS;
                $model->amount = $item['points'];
                $model->send_to = $item['number'];
                $model->sms_id = $item['id'];
                $model->company_id = $params['company_id'];
                $model->message = $params['message'];
                $model->schedule_date = $params['date'];
                $model->created_at = date('Y-m-d H:i:s');
                $model->save();
                $quantity +=1;
                $cost += $item['points'];
            }
//            $invoice = new CompanyBalance();
//            $invoice->company_id = $model->company_id;
//            $invoice->status = CompanyBalance::STATUS_WAIT;
//            $invoice->service = CompanyBalance::SERVICE_SMS;
//            $invoice->quantity = $quantity;
//            $invoice->cost = $cost;
//            $invoice->message = $params['message'];
//            $invoice->save();

            $response = 'success';
        }

        return $response;
    }

    /**
     * Get relative Company model
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id'=>'company_id']);
    }

    /**
     * @pGet Phone numbers
     * @return array
     */
    public function getSendTo($array) {
        $sendTo = [];
        foreach ($array as $item) {
            $user = User::findOne($item);
            $sendTo[] = str_replace('+', '', $user->userProfile->phone_number);
        }
        return $sendTo;
    }
}
