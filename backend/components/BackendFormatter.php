<?php

namespace backend\components;

use yii\i18n\Formatter;

class BackendFormatter extends Formatter
{
    public function load($formatClass)
    {
        return new $formatClass;
    }
}