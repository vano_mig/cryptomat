<?php


namespace backend\models;

use Yii;

/**
 * Class User
 * @package backend\models
 */
class User extends \common\models\User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],

            ['last_name', 'trim'],

            ['birthday', 'trim'],
            ['birthday', 'date', 'format'=>'yyy-mm-dd'],

            ['email', 'trim'],
            ['email', 'email'],

            ['password_hash', 'trim'],
            [['password_hash'], 'string', 'min' => 6],

            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_SEMI_ACTIVE]],

            ['role', 'default', 'value' => self::ROLE_USER],
 //           ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_CUSTOMER, self::ROLE_ENGINEER, self::ROLE_MANAGER, self::ROLE_ADMIN, self::ROLE_FILLING, self::ROLE_SERVICE]],

            ['image', 'string'],

            ['agent_id', 'integer'],

            [['company_id', 'company_role'], 'safe']

        ];
    }
}