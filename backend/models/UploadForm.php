<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package backend\models\form
 * @property mixed|null $imageFile
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, giff', 'maxFiles' => 4],
        ];
    }

    public function upload($path)
    {
        $result = true;
        if ($this->validate()) {
            foreach ($this->imageFile as $file) {
                if(!$file->saveAs($path . $file->baseName . '.' . $file->extension)) {
                    $result = false;
                }
            }
        }
        return $result;
    }
}