<?php

namespace backend\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use Yii;

class VerifyEmailForm extends Model
{
    /**
     * @var string
     */
    public $token;
//
//    /**
//     * @var User
//     */
//    private $_user;
//
//
//    /**
//     * Creates a form model with given token.
//     *
//     * @param string $token
//     * @param array $config name-value pairs that will be used to initialize the object properties
//     * @throws InvalidArgumentException if token is empty or not valid
//     */
//    public function __construct($token, array $config = [])
//    {
//        if (empty($token) || !is_string($token)) {
//            throw new InvalidArgumentException('Verify email token cannot be blank.');
//        }
//        $this->_user = User::findByVerificationToken($token);
//        if (!$this->_user) {
//            throw new InvalidArgumentException('Wrong verify email token.');
//        }
//        parent::__construct($config);
//    }
//
//    /**
//     * Verify email
//     *
//     * @return User|null the saved model or null if saving fails
//     */
//    public function verifyEmail()
//    {
//        $user = $this->_user;
//        $user->status = User::STATUS_ACTIVE;
//        return $user->save(false) ? $user : null;
//    }

    public function rules()
    {
        return [
            ['token', 'trim'],
            ['token', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'token' => Yii::t('verify email', 'Token'),
        ];
    }

    public function userVsToken($token)
    {
        $userVsToken = User::find()->where(['verification_token' => $token])->one();
        if ($userVsToken) {
            return $userVsToken;
        } else {
            return false;
        }
    }

    public function verify($token)
    {
        $user = $this->userVsToken($token);

        if ($user) {
            if ($user->status == User::STATUS_ACTIVE ) {
                return ['status' => $user->status];
            } else {
                $user->status = User::STATUS_SEMI_ACTIVE;
                $user->save();
                return ['email' => $user->email, 'password_hash' => $user->password_hash, 'status' => User::STATUS_SEMI_ACTIVE];
            }
        } else {
            return false;
        }
    }
}
