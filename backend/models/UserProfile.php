<?php
namespace backend\models;

use Yii;

/**
 * Class UserProfile
 * @package backend\models
 */
class UserProfile extends \common\models\UserProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['phone_number', 'trim'],
            ['phone_number', 'string', 'min' => 1, 'max' => 255],

            ['country', 'trim'],
            ['country', 'string', 'min' => 1, 'max' => 255],

            ['city', 'trim'],
            ['city', 'string', 'min' => 1, 'max' => 255],

            ['street_house', 'trim'],
            ['street_house', 'string', 'min' => 1, 'max' => 255],

            ['post_code', 'trim'],
            ['post_code', 'string', 'min' => 1, 'max' => 255],

            ['client_from', 'trim'],
            ['client_from', 'string', 'min' => 1, 'max' => 255],

            ['mailing', 'default', 'value' => false],
            ['mailing', 'boolean'],

            ['privacy_policy', 'default', 'value' => false],
            ['privacy_policy', 'boolean'],
            ['privacy_policy', 'in', 'range' => [true], 'message' => Yii::t('site', '
This field is a mandatory field.')],

            ['terms_of_service', 'default', 'value' => false],
            ['terms_of_service', 'boolean'],
            ['terms_of_service', 'in', 'range' => [true], 'message' => Yii::t('site', '
This field is a mandatory field.')],

        ];
    }
}