<?php


namespace backend\models;

/**
 * Class LoginAfterVerifyEmail
 * @package backend\models
 */
class LoginAfterVerifyEmail extends LoginForm
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['email', 'validateEmail'],
//            ['password', 'validatePassword'],
        ];
    }
}