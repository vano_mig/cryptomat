<?php

namespace frontend\formatter;

use frontend\components\FrontendFormatter;
use yii\bootstrap4\Html;

class LanguageFormatter extends FrontendFormatter
{
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public function asLangFlag($data): string
    {

        $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $data->iso_code]);
        if ($data->iso_code == 'uk') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']);
        }
        if ($data->iso_code == 'en') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']);
        }
        return $res;
    }

}
