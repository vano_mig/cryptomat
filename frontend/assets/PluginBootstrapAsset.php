<?php
namespace frontend\assets;

use yii\bootstrap4\BootstrapPluginAsset;
use yii\web\AssetBundle;

class PluginBootstrapAsset extends AssetBundle
{
    public $sourcePath = '@npm/bootstrap/dist';
    public $css = [
    ];
    public $js = [
        'js/bootstrap.bundle.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}