<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PluginChartJSAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'plugins/chart.js/Chart.js',
    ];

    public $depends = [];
}