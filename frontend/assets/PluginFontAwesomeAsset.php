<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PluginFontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
        'plugins/fontawesome-free/css/all.min.css',
    ];

    public $js = [];

    public $depends = [];
}