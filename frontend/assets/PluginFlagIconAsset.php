<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PluginFlagIconAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
        'plugins/flagIcon/css/flag-icon.css',
    ];

    public $js = [];

    public $depends = [];
}