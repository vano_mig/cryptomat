<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PluginTinyMceAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [];

    public $js = [
        'plugins/tinymce/js/tinymce/tinymce.min.js',
    ];

    public $depends = [];
}