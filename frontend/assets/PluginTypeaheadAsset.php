<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PluginTypeaheadAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'plugins/typeahead/typeahead-TODO.js',
    ];

    public $depends = [];
}