<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PluginMatchHeightAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'plugins/matchHeight/jquery.matchHeight.js'
    ];

    public $depends = [];
}
