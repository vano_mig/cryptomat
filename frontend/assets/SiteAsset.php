<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SiteAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'/css/styles.min.css',
		'/css/style.css',
	];

	public $js = [
		'/js/scripts.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		'frontend\assets\FontAsset',
		'frontend\assets\PluginAdminLteAsset',
		'frontend\assets\PluginBootstrapAsset',
//        'frontend\assets\TemplateAdminLteAsset',
		'frontend\assets\PluginFontAwesomeAsset',
		'frontend\assets\PluginTinyMceAsset',
		'frontend\assets\PluginChartJSAsset',
		'frontend\assets\PluginTypeaheadAsset',
		'frontend\assets\PluginFlagIconAsset',
		'frontend\assets\PluginMatchHeightAsset',
		'frontend\assets\PluginSpectrumAsset'
	];
}
