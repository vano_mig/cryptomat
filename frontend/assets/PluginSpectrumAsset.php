<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PluginSpectrumAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
        'plugins/spectrum/spectrum.css',
    ];

    public $js = [
        'plugins/spectrum/spectrum.js',
    ];

    public $depends = [];
}