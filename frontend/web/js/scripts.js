$(document).ready(function () {
//----------------------------------------------------------------------------------------------------------------------- Global variable //

//---------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------------------------------------------------------------- Hamburger menu  //

    $(".main-sidebar , .my-hamburger-nav").on('mouseover', function () {

        var a;
        var b;
        var c;

        $(".main-sidebar").hover(
            function () {
                if ($('body').is('.sidebar-collapse')) {
                    a = true;
                }
            },
            function () {
                if ($('body').is('.sidebar-collapse')) {
                    a = true;
                    setTimeout(
                        function () {
                            a = false;
                        },
                        500);
                }
            });

        $(".my-hamburger-nav").hover(
            function () {
                if (a || b) {
                    b = false;
                    c = true;
                    $('.main-sidebar').addClass('sidebar-focused');
                }
            },
            function () {
                if (c) {
                    b = true;
                    c = false;
                    setTimeout(
                        function () {
                            if (b) {
                                $('.main-sidebar').removeClass('sidebar-focused');
                                b = false;
                            }
                        },
                        350);
                }
            });
    });

//---------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------- Alert //
    if ($(".my-alert")) {
        setTimeout(
            function () {
                $(".my-alert").alert('close')
            },
            10000);
    }
//---------------------------------------------------------------------------------------------------------------------//
})
;