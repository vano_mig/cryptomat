<?php
/* @var $this yii\web\View */

/* @var $content mixed */

use frontend\assets\SiteAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use frontend\models\Language;
use frontend\formatter\LanguageFormatter;

SiteAsset::register($this);
Yii::t('site', 'Logout');
$languages = Language::findAll(['status' => Language::ACTIVE]);
$liLanguages = [];
if ($languages) {
    foreach ($languages as $language) {
        if ($language->iso_code != Yii::$app->language) {
            $liLanguages[] = [
                'label' => Yii::$app->formatter->load(LanguageFormatter::class)->asLangFlag($language) . Yii::t('language', $language->name),
                'url' => '/' . $language->iso_code . Yii::$app->getRequest()->getLangUrl(),
                'encode' => false,
            ];
        } else {
            $lanName = $language;
        }
    }
}
$menuLeft[] = '';
$menuRight[] = '';

//if (!Yii::$app->user->isGuest) {
//    $menuLeft[] = ['label' => Yii::t('admin', 'Admin panel'), 'url' => Url::toRoute(['/admin/default']),
//        'linkOptions' => ['class' => 'btn btn-outline-light mr-0 mr-md-2 mb-2 mb-md-0 my-p-nav'],
//        'options' => ['class' => 'my-lang-li-nav']];
//}

$menuRight[] = ['label' => Yii::$app->formatter->load(LanguageFormatter::class)->asLangFlag($lanName) . Yii::t('language', $lanName->name), 'items' => $liLanguages,
    'linkOptions' => ['class' => 'btn my-btn-outline-light dropdown-toggle my-leng-nav'],
    'options' => ['class' => 'mr-0 mr-md-2 mb-2 mb-md-0 my-lang-li-nav'],
    'encode' => false,
];

if (Yii::$app->user->isGuest) {
    $menuRight[] =
        '<li class="nav-item">'
        . Html::a(Yii::t('site', 'Signup'), Url::toRoute(['/site/registration']), ['class' => 'btn btn-outline-light mr-0 mr-md-2 mb-2 mb-md-0'])
        . '</li>';
//    $menuRight[] =
//        '<li class="nav-item">'
//        . Html::a(Yii::t('site', 'Login') . ' <i class="fas fa-sign-in-alt"></i>', Url::toRoute(['/site/login']), ['class' => 'btn btn-outline-light'])
//        . '</li>';
} else {
//    $menuRight[] = ['label' => ' ' . Html::encode(Yii::$app->user->identity->email), 'url' => Url::toRoute(['/admin/default']),
//        'linkOptions' => ['class' => 'btn btn-outline-light mr-0 mr-md-2 mb-2 mb-md-0 my-p-nav'],
//        'options' => ['class' => 'my-lang-li-nav']];
    $menuRight[] =
        '<li class="nav-item">'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(' <i class="fas fa-sign-out-alt"></i>', ['class' => 'btn btn-outline-light'])
        . Html::endForm()
        . '</li>';
}

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="min-vh-100" style="" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head();?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?=Url::to('/favicon/apple-icon-57x57.png');?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=Url::to('/favicon/apple-icon-60x60.png');?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=Url::to('/favicon/apple-icon-72x72.png');?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=Url::to('/favicon/apple-icon-76x76.png');?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=Url::to('/favicon/apple-icon-114x114.png');?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=Url::to('/favicon/apple-icon-120x120.png');?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=Url::to('/favicon/apple-icon-144x144.png');?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=Url::to('/favicon/apple-icon-152x152.png');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=Url::to('/favicon/apple-icon-180x180.png');?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=Url::to('/favicon/android-icon-192x192.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=Url::to('/favicon/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=Url::to('/favicon/favicon-96x96.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=Url::to('/favicon/favicon-16x16.png');?>">
    <link rel="manifest" href="<?=Url::to('/favicon/manifest.json');?>">
</head>
<body class="min-vh-100">
<?php $this->beginBody() ?>
<div class="min-vh-100 d-flex flex-column justify-content-between" style="position: relative;">
    <header class="mb-2">

		<?php
		NavBar::begin([
			'brandLabel' => Yii::t('site', 'CRYPTOMATICATM'),
			'brandUrl' => Url::toRoute('/'),
			'options' => [
				'name' => Yii::t('site', 'CRYPTOMATICATM'),
				'class' => 'main-header navbar navbar-expand navbar-white navbar-dark my-navbar-purple border-0',
				'style' => 'margin-left: unset'
			],
		]);

		echo Nav::widget([
			'options' => ['class' => 'navbar-nav mr-auto my-navbar-nav'],
			'items' => $menuLeft,
		]);
		echo Nav::widget([
			'options' => ['class' => 'navbar-nav ml-auto my-navbar-nav'],
			'items' => $menuRight,
		]);

		NavBar::end(); ?>
    </header>

    <div id="particles-js"></div>

    <main class="mt-2 container m-auto p-0 main-index" style="min-height: 100% !important;">
		<?php if ($alert = Alert::widget()) { ?>
            <div class="col-12 pt-4">
				<?= $alert ?>
            </div>
		<?php } ?>

		<?= $content ?>
    </main>
    <footer class="main-footer py-3" style="margin-left: unset">
        <div class="container">
            <p class="m-0 footer-center">
                &copy;<?= ' ' . '2019' . ' - ' . date('Y') . ', «' . Yii::t('site', 'CRYPTOMATICATM') . '»' ?>
            </p>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
