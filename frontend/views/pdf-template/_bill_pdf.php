<?php

use frontend\modules\admin\formatter\ProductFridgeFormatter;

/**
 * @var $this yii\web\View
 * @var $user frontend\models\User;
 * @var $transaction frontend\models\Transaction
 * @var $company frontend\models\Company
 * @var $productInfo array
 */

?>

<table class="width-100">
    <tr>
        <td colspan="2" class="big-font">
            <?php echo Yii::t('fridge', 'Invoice'), " #", $transaction->res_file_ref; ?>
        </td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('fridge', 'Date of invoice'), ": "; ?>
            <span class="common-font">
                <?php
                $time = strtotime($transaction->created_at);
                echo date('H:i:s d.m.y', $time); ?></span>
        </td>
        <td class="bold-font">
            <?php echo Yii::t('fridge', 'Created'), ": "; ?>
            <span class='common-font'>
                <?php
                $time = strtotime($transaction->created_at);
                echo date('H:i:s d.m.y', $time); ?></span>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="bold-font">
            <?php echo Yii::t('fridge', 'Customer'), ": "; ?>
            <span class='common-font'>
                <?php echo $user->username, ' ', $user->last_name; ?>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="bold-font">
            <?php echo Yii::t('fridge', 'Seller'), ": "; ?>
            <span class='common-font'>
                <?php echo $company->name; ?>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br/>
        </td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('admin', 'Date'), ": "; ?>
        </td>
        <td class="text-align-right">
            <?php
            $time = strtotime($transaction->created_at);
            echo date('H:i:s d.m.y', $time);
            ?>
        </td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('admin', 'Order created'), ": "; ?>
        </td>
        <td class="text-align-right">
            <?php
            $time = strtotime($transaction->created_at);
            echo date('H:i:s d.m.y', $time); ?>
        </td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('finance', 'Transaction'), " #"; ?>
        </td>

        <td class="text-align-right">
            <?php echo $transaction->res_file_ref; ?>
        </td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('finance', 'Transaction type'), ": "; ?>
        </td>
        <td class="text-align-right">
            <?php echo Yii::t('finance', 'Credit card'); ?>
        </td>
    </tr>
</table>
<br/>
<table class="width-100">
    <tr class="text-align-center">
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Item number'), " #"; ?>
        </td>
        <td class="font-bold">
            <?php echo Yii::t('finance', 'Title'), ": "; ?>
        </td>
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Vat'), "%"; ?>
        </td>
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Discount'), "%"; ?>
        </td>
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Amount'), ": "; ?>
        </td>
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Item price, €'), ": "; ?>
        </td>
        <td class="width80 font-bold">
            <?php echo Yii::t('finance', 'Total bill, €'), ": "; ?>
        </td>
    </tr>
    <?php
    $count = 0;
    $net_count = 0;
    $vat_19 = 0;
    $vat_7 = 0;
    $count_7 = 0;
    $count_19 = 0;
    $total_discount = 0;
    $discount = [];
    $total_weight = 0;
    $card_discount = 0;
    $one_position_discount = 0;
    $returnPack = [];
    ?>

    <?php if (!empty($productInfo)) : ?>
        <?php foreach ($productInfo as $one_position) : ?>
            <tr>
                <td class="text-align-center"><?php echo $one_position['id']; ?></td>
                <td>
                    <?php echo Yii::$app->formatter->load(ProductFridgeFormatter::class)->asName($one_position['product_id']); ?>
                </td>
                <td class="text-align-center"><?php echo $one_position['vat_raw']; ?></td>
                <td class="text-align-center"><?php echo $one_position['discount_raw']; ?></td>
                <td class="text-align-center"><?php echo $one_position['position_amount']; ?></td>
                <td class="text-align-center"><?php echo $one_position['net_price']; ?></td>
                <td class="text-align-center">
                    <?php if ($one_position['discount_part'] != 0) {
                        $one_position_discount = number_format(
                            round($one_position['net_price'] * $one_position['position_amount'], 2, 1),
                            2);
                        if ($one_position['discount_by_card'] == true) {
                            if (array_key_exists('card_discount', $discount)) {
                                $discount['card_discount']['amount'] += $one_position_discount;
                            } else {
                                $discount['card_discount']['amount'] = $one_position_discount;
                                $discount['card_discount']['discount'] = $one_position['discount_raw'];
                            }
                        } else {
                            if (array_key_exists($one_position['discount_raw'], $discount['discount'])) {
                                $discount['discount'][$one_position['discount_raw']]['amount'] += $one_position_discount;
                            } else {
                                $discount['discount'][$one_position['discount_raw']]['amount'] = $one_position_discount;
                                $discount['discount'][$one_position['discount_raw']]['discount'] = $one_position['discount_raw'];
                            }
                        }
                    }
                    if ($one_position['vat_raw'] == 19) {
                        $count_19 += round($one_position['net_price_including_discount'] * $one_position['position_amount'], 2, 1);
                    } else {
                        $count_7 += round($one_position['net_price_including_discount'] * $one_position['position_amount'], 2, 1);
                    }
                    if(array_key_exists($one_position['id'], $returnPack)) {
                        $returnPack[$one_position['id']]['count'] += 1;
                    } else {
                        $returnPack[$one_position['id']]['count'] = 1;
                        $returnPack[$one_position['id']]['price'] = $one_position['compensated_sum'];
                        $returnPack[$one_position['id']]['vat'] = $one_position['vat_raw'];
                    }
                    echo number_format(
                        round($one_position['net_price'] * $one_position['position_amount'], 2, 1),
                        2);
                    $net_count += round(($one_position['net_price']) * $one_position['position_amount'], 2, 1);
                    $count += round($one_position['net_price'] * $one_position['position_amount'], 2, 1);
                    ?>
                    <?php if (!$one_position['weight'] == 0): ?>
                        <?php $total_weight += $one_position['weight']; ?>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php endif ?>
</table>

<br/>
<?php
$vat_7 = round($count_7 * 7 / 100, 2, 1);
$vat_19 = round($count_19 * 19 / 100, 2, 1);
if (array_key_exists('card_discount', $discount)) {
    $card_discount = round($discount['card_discount']['amount'] * $discount['card_discount']['discount'] / 100, 2, 1);
} else {
    $card_discount = 0;
}

$product_discount = 0;
if (array_key_exists('discount', $discount)) {
    foreach ($discount['discount'] as $item) {
        $product_discount += round($item['amount'] * $item['discount'] / 100, 2, 1);
    }
}

$total_discount = $product_discount + $card_discount;
$minusPriceTare = 0;
if(!empty($returnPack)) {
    foreach ($returnPack as $data) {
        $minusPriceTare += round($data['price'] * $data['count'] * $data['vat']/100,2 ,1) + $data['price'];
    }
}

?>
<table class="width-100">
    <tr>
        <td class="bold-font font-size-22">
            <?php echo Yii::t('finance', 'Items cost, €'), ": "; ?>
        </td>
        <td class="text-align-right font-size-22"><?php echo number_format($net_count, 2) ?> <br/><br/></td>
    </tr>
    <tr>
        <td colspan="2"><br/></td>
    </tr>
    <tr>
        <td class="bold-font"><?php echo Yii::t('finance', 'VAT 19%:'); ?></td>
        <td class="text-align-right"><?php echo number_format($vat_19, 2); ?><br/><br/></td>
    </tr>
    <tr>
        <td class="bold-font"><?php echo Yii::t('finance', 'VAT 7%:'); ?></td>
        <td class="text-align-right"><?php echo number_format($vat_7, 2); ?> <br/><br/></td>
    </tr>
    <tr>
        <td class="bold-font"><?php echo Yii::t('finance', 'Card discount, €:'); ?></td>
        <td class="text-align-right">
            <?php echo number_format($card_discount, 2); ?><br/><br/></td>
    </tr>
    <tr>
        <td class="bold-font"><?php echo Yii::t('finance', 'Discounts, €:'); ?></td>
        <td class="text-align-right"><?php echo number_format($total_discount, 2); ?><br/><br/></td>
    </tr>
    <tr>
        <td class="bold-font">
            <?php echo Yii::t('finance', 'Total mass of weight product items, g:'); ?>
        </td>
        <td class="text-align-right"><?php echo $total_weight; ?><br/><br/></td>
    </tr>
    <?php if ($minusPriceTare != 0): ?>
        <tr>
            <td class="bold-font">
                <?php echo Yii::t('finance', 'Sum, compensated for returned tare:'); ?>
            </td>
            <td class="text-align-right"><?php echo number_format($minusPriceTare, 2); ?><br/><br/></td>
        </tr>
    <?php endif; ?>
    <tr>
        <td class="bold-font"><?php echo Yii::t('finance', 'Total VAT'), ": "; ?></td>
        <td class="text-align-right">
            <?php echo number_format($vat_7 + $vat_19, 2); ?><br/><br/>
        </td>
    </tr>
    <tr>
        <td class="bold-font font-size-26">
            <?php echo Yii::t('finance', 'Total price with discounts and VAT'), ": "; ?>
        </td>
        <td class="text-align-right font-size-26"><?php echo number_format($count + (number_format($vat_7 + $vat_19, 2)) - $total_discount - $minusPriceTare, 2); ?>
            <br/><br/></td>
    </tr>
</table>