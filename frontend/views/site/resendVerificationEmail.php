<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model frontend\models\ResetPasswordForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Resend verification email';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">

            <p><?php echo Yii::t('site', 'Please fill out your email. A verification email will be sent there.') ?></p>


            <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <div class="row form-group">
                <?= Html::submitButton(Yii::t('password reset', 'Send'), ['class' => 'btn bg-gradient-primary  mx-auto']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>