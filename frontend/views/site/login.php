<?php

/* @var $this yii\web\View */

/* @var $model frontend\models\LoginForm */


use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('site', 'Login form');

?>
<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <p class="col-12"><?php echo Yii::t('site', 'Please fill out the following fields to login:') ?></p>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                <?php echo Yii::t('site', 'If you forgot your password you can') ?> <?= Html::a(Yii::t('If you forgot your password you can', 'reset it'), ['site/request-password-reset']) ?>
                .
            </div>

            <div class="row form-group">
                <?= Html::submitButton(Yii::t('site', 'Login'), ['class' => 'btn bg-gradient-primary mx-auto', 'name' => 'login-button']) ?>
            </div>


            <?php ActiveForm::end(); ?>


        </div>
    </div>
</div>