<?php

/* @var $alert bool */
/* @var $passId null|string */
/* @var $androidUrl null|string */
/* @var $this yii\web\View */
/* @var $modelIdentificationCard frontend\models\IdentificationCard */
/* @var $modelPass frontend\models\PasscreatorService */
/* @var $iosUrl null|string */

/* @var $passTemplate bool */
/* @var $modelPasss array|\frontend\modules\pkpass\models\PkpassPass|null|\yii\db\ActiveRecord */

/* @var $writer \BaconQrCode\Writer */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('site', 'Payment Method');
$url = urlencode(Yii::getAlias('@wallet') . '/pkpass/temp/' . $modelPasss->serial_number . '_pass.pkpass');
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <div class="col-12 p-0 m-0">
                <h4 class="col-12" style="text-align: center; font-size: 2.5rem;"><?= Yii::t('site', 'QR-code') ?></h4>
                <ul class="nav nav-tabs d-flex justify-content-center" id="custom-content-above-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active"
                           id="custom-content-above-home-tab"
                           data-toggle="pill"
                           href="#custom-content-above-home"
                           role="tab"
                           aria-controls="custom-content-above-home"
                           aria-selected="true">
                            <i class="fab fa-apple"></i> <span> <?= Yii::t('registration', 'Iphone') ?></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           id="custom-content-above-profile-tab"
                           data-toggle="pill"
                           href="#custom-content-above-profile"
                           role="tab"
                           aria-controls="custom-content-above-profile"
                           aria-selected="false">
                            <i class="fab fa-android"></i> <?= Yii::t('registration', 'Android') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="custom-content-above-tabContent">
                    <div class="tab-pane fade active show"
                         id="custom-content-above-home"
                         role="tabpanel"
                         aria-labelledby="custom-content-above-home-tab"
                    >
                        <div style="padding-top: 1rem; margin-bottom: 1rem;">
                            <div class="col-xs-12" style="margin-bottom: 1rem;">
                                <div>
                                    <p style="text-align: center;"> <?= Yii::t('site', 'Scan QR-code:') ?></p>
                                    <div class="col-8 mx-auto my-svg">
                                        <?php echo $writer->writeString(Url::toRoute([Yii::getAlias('@wallet').'/get-pass-ios', 'serialNumber' => $modelPasss->serial_number], 'https')); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12"
                                     style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 1rem;">
                                    <p style="margin: 0.5rem 0; text-align: center;">
                                        - <?= Yii::t('Wallet passes', 'or Download') . ':' ?> -</p>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-3">
                                        <?php echo Html::a('<i class="fab fa-apple"></i> ' . Yii::t('site', 'Download'),
                                            Url::toRoute([Yii::getAlias('@wallet').'/get-pass-ios', 'serialNumber' => $modelPasss->serial_number], 'https'),
                                            ['class' => 'btn bg-gradient-secondary mx-auto']) ?>

                                    </div>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-3">
                                        <a href="/"
                                           class="btn bg-gradient-primary mx-auto">
                                            <i class="fab fa-android"
                                               style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Complete') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade"
                         id="custom-content-above-profile"
                         role="tabpanel"
                         aria-labelledby="custom-content-above-profile-tab">
                        <div style="padding-top: 1rem; margin-bottom: 1rem;">
                            <div class="col-xs-12" style="margin-bottom: 1rem;">
                                <div>
                                    <p style="text-align: center;"> <?= Yii::t('site', 'Scan QR-code:') ?></p>
                                    <div class="col-8 mx-auto my-svg">
                                        <?php echo $writer->writeString('https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12"
                                     style="border-bottom: 1px solid #eeeeee; border-top: 1px solid #eeeeee; margin-top: 1rem;">
                                    <p style="margin: 0.5rem 0; text-align: center;">
                                        - <?= Yii::t('Wallet passes', 'or Download') . ':' ?> -</p>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-3">
                                        <a href="<?= 'https://passwallet.page.link/?apn=com.attidomobile.passwallet&link=' . $url ?>"
                                           class="btn bg-gradient-success mx-auto">
                                            <i class="fab fa-android"
                                               style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Download') ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="my-download-buttom col-xs-12 mb-4" style="padding-top: 1rem;">
                                    <div class="my-android-buttom row mt-3">
                                        <a href="/"
                                           class="btn bg-gradient-primary mx-auto">
                                            <i class="fab fa-android"
                                               style="font-family: 'Font Awesome 5 Brands';"></i> <?= Yii::t('site', 'Complete') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>