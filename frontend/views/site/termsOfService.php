<?php
/* @var $this yii\web\View */

use common\models\StaticContent;

$this->title = Yii::t('site', 'Terms of Service');

?>

<div class="content col max-wr-49 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <?php
            echo StaticContent::Content('1');
            ?>
        </div>
    </div>
</div>