<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model frontend\models\PasswordResetRequestForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('password reset', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
                <p class=""
                   style="text-align: center; margin-top: 1rem;"><?php echo Yii::t('password reset', 'Please fill out your email. A link to reset password will be sent there.') ?></p>

                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="col-12" style="display: inline-flex; justify-content: center">
                    <div class="row form-group">
                        <?= Html::submitButton(Yii::t('password reset', 'Send'), ['class' => 'btn bg-gradient-primary mx-auto']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>