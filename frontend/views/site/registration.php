<?php

/* @var $birthdaySelect array */
/* @var $model frontend\models\RegistrationForm  */
/* @var $agentItem array */

/* @var $this yii\web\View */

use frontend\models\Company;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('site', 'Registration');
?>

    <div class="content col max-wr-33 mx-auto">
        <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
            <div class="card-header row">
                <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
            </div>
            <div class="card-body">

                <?php $form = ActiveForm::begin(['id' => 'form-registration']); ?>

                <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'last_name') ?>

                <div class="form-group required p-0 w-100 mb-3">
                    <label style="width: 100%;"><?= Yii::t('site', 'Birthday') ?></label>
                    <div class="my-birthday d-inline-flex pt-2 w-100 border-r-10" style="background-color: #fcfcfc;">

                        <?= $form->field($model, 'birthdayD', ['options' => ['style' => 'width:22%; margin-left:2%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionD'], ['class' => 'form-control my-birthday-d','style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionDCss']])->label(false); ?>
                        <p style="margin:0; padding:5px 0 0 0; font-size: 18px;">/</p>
                        <?= $form->field($model, 'birthdayM', ['options' => ['style' => 'width:28%; margin-left:3%; margin-right:3%; position: relative;']])->dropDownList($birthdaySelect['optionM'], ['class' => 'form-control my-birthday-m','style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionMCss']])->label(false); ?>
                        <p style="margin:0; padding:5px 0 0 0; font-size: 18px;">/</p>
                        <?= $form->field($model, 'birthdayY', ['options' => ['style' => 'width:32%; margin-left:3%; margin-right:2%; position: relative;']])->dropDownList($birthdaySelect['optionY'], ['class' => 'form-control my-birthday-y','style' => "color:gray; margin: 0; width:100% !important; padding:0 0 3px 2px;", "onchange" => "this.style.color='black'", 'options' => $birthdaySelect['optionYCss']])->label(false); ?>

                    </div>
                    <?= $form->field($model, 'birthday', ['template' => "{input}{error}", 'options' => ['class' => 'form-group my-birthday-e','style' => 'margin-top: 0;']])->input('text',['class' => 'form-control d-none my-birthday'])->label(false) ?>
                </div>



                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'phone_number') ?>

                <?= $form->field($model, 'password_hash', ['options' => ['class' => 'mb-0 form-group']])->passwordInput(['style' => 'font-size: 18px;']) ?>

                <?= $form->field($model, 'password_check')->passwordInput(['style' => 'font-size: 18px;'])->label(false) ?>

                <?= $form->field($model, 'country') ?>
                <?= $form->field($model, 'city') ?>
                <?= $form->field($model, 'street_house') ?>
                <?= $form->field($model, 'company_id')->dropDownList(Company::getList(), ['prompt'=>Yii::t('company', 'Select company')])?>
                <br>
                <div class="col-12"
                     style="border-top: 1px solid #bfbfbf; display: inline-flex; justify-content: center;">
                    <div class="row form-group" style="margin-top: 1rem">
                        <?= Html::submitButton(Yii::t('site', 'Registration'), ['class' => 'btn bg-gradient-primary mx-auto my-reg-btn', 'name' => 'Registration-button',]) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php
$alet = json_encode(Yii::t('site', 'You must be over 18 years old'));
$script = '
$( document ).ready(function() {
    function validBir() {
        $(".my-birthday-d").removeClass("is-valid");
        $(".my-birthday-d").addClass("is-invalid");
        $(".my-birthday-m").removeClass("is-valid");
        $(".my-birthday-m").addClass("is-invalid");
        $(".my-birthday-y").removeClass("is-valid");
        $(".my-birthday-y").addClass("is-invalid");
    }
    if ($(".field-registrationform-birthday input").hasClass("is-invalid")) {
        validBir();
        setTimeout( validBir, 350);
    }
    $(".my-reg-btn").on("click touchend keyup", function(event) {          
        if ($(".field-registrationform-birthday input").hasClass("is-invalid")) {
            validBir();
            setTimeout( validBir, 350);
        }
    });
    
    $(".my-birthday").on("click touchend keyup", function(event) {
    
        var day = $(".my-birthday-d").val();
        var mounth = $(".my-birthday-m").val();
        var year = $(".my-birthday-y").val();
        
        function agee(birthDateString) {
            var today = new Date();
            var birthDate = new Date(birthDateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }
        
        if (day && mounth && year) {
        $(".my-birthday").val(year + "-" + mounth + "-" + day);
            if(agee(mounth + "/" + day + "/" + year) < 18) {
               
                validBir();
                setTimeout( validBir, 210);
                 
                $(".my-birthday-e").addClass("validating");
                $(".invalid-feedback").removeClass("d-none");
                $(".my-birthday-e .invalid-feedback").addClass("d-block"); 
                $(".my-birthday-e .invalid-feedback").text('."$alet".'); 
            } else {
                $(".my-birthday-d").removeClass("is-invalid");
                $(".my-birthday-m").removeClass("is-invalid");
                $(".my-birthday-y").removeClass("is-invalid");
                $(".my-birthday-d").addClass("is-valid");
                $(".my-birthday-m").addClass("is-valid");
                $(".my-birthday-y").addClass("is-valid");
                $(".invalid-feedback").removeClass("d-block");
                $(".invalid-feedback").addClass("d-none");
            }
        }
    });
    
});
';
$this->registerJs($script, yii\web\View::POS_END);
