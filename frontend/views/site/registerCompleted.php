<?php
/* @var $this yii\web\View */

use common\models\StaticContent;

$this->title = Yii::t('site', 'Registration completed');

?>


<div class="content col max-wr-49 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-body">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
    </div>
</div>
