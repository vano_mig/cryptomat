<?php

/* @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model frontend\models\ResetPasswordForm
 */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('password reset', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content col max-wr-33 mx-auto">
    <div class="card card-secondary card-outline mt-5" style="border-radius: 7px">
        <div class="card-header row">
            <h3 class="card-title mx-auto"><?php echo $this->title ?></h3>
        </div>
        <div class="card-body">
            <p class="login-box-msg"
               style="text-align: center; margin-top: 1rem;"><?php echo Yii::t('password reset', 'Please choose your new password:') ?></p>

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

            <div class="row form-group">
                <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn bg-gradient-primary my-2 mx-auto']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
