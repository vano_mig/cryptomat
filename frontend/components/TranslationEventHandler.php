<?php

namespace frontend\components;

use yii\i18n\MissingTranslationEvent;
use frontend\models\Language;
use frontend\models\SourceMessage;
use frontend\models\Message;

/**
 * Class TranslationEventHandler
 * @package frontend\components
 */
class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $languages = (new Language())->getList();
        $message = SourceMessage::find()->where(['category'=>$event->category, 'message'=>$event->message])->one();
        if(!$message) {
            $model = new SourceMessage();
            $model->category = $event->category;
            $model->message = $event->message;
            $model->save();
            foreach ($languages as $language) {
                $translate = new Message();
                $translate->id = $model->id;
                $translate->language = $language;
                $translate->save();
            }
        }

//        $event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
    }
}