<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-frontend',
    'name' => 'CRYPTOMATIC',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        '@admin_lte' => '@frontend/theme/AdminLTE',
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => 'asdfgbkdjgbjgfdkbsdgbfvfzkbdvsdg',
            'class' => 'frontend\components\LanguageRequest'
        ],
        'assetManager' => [
            'linkAssets' => true,
            'bundles' => [
                'yii\bootstrap4\BootstrapPluginAsset' => false,
                'yii\bootstrap4\BootstrapAsset' => false,
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'class' => frontend\components\FrontendFormatter::class,
            'defaultTimeZone' => 'Europe/Kiev',
            'timeZone' => 'Europe/Kiev',
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                '*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'frontend\components\LanguageUrlManager',
            'rules' => [
                '<action>' => 'site/<action>',
                'GET,POST pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>/<pass:.+>' => '/pkpass/pkpass-web-service/registration',
                'GET,POST pkpass/pkpass-web-service/v1/devices/<device:[^\s]+>/registrations_attido/<type:[^\s]+>/<pass:[^\s]+>' => 'pkpass/pkpass-web-service/registration-attido',
                'GET pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>' => 'pkpass/pkpass-web-service/passes',
                'GET pkpass/pkpass-web-service/v1/passes/<type:.+>/<pass:.+>' => 'pkpass/pkpass-web-service/update',
                'DELETE pkpass/pkpass-web-service/v1/devices/<device:.+>/registrations/<type:.+>/<pass:.+>' => 'pkpass/pkpass-web-service/delete',
                'GET,POST pkpass/pkpass-web-service/v1/log' => 'pkpass/pkpass-web-service/log',
            ],
        ],
    ],
//    'modules' => [
//        'admin' => [
//            'class' => frontend\modules\admin\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
////            'view' => [
////                'theme' => [
////                    'basePath' => '@frontend/modules/admin/themes/lte',
////                    'baseUrl' => '@web/modules/admin/themes/lte',
//////                    'pathMap' => [
//////                        '@app/views' => '@frontend/modules/admin/themes/lte/views',
//////                        '@app/widgets' => '@frontend/modules/admin/themes/lte/views/widgets',
//////                    ],
////                ],
////            ],
//        ],
//
//        'connection' => [
//            'class' => frontend\modules\connection\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
//        ],
//        'fridge' => [
//            'class' => frontend\modules\fridge\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
//        ],
//        'user' => [
//            'class' => frontend\modules\user\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
//        ],
//        'pkpass' => [
//            'class' => frontend\modules\pkpass\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
//        ],
//        'virtual-virtual_terminal' => [
//            'class' => frontend\modules\virtual_terminal\Module::class,
//            'layout' => '@frontend/modules/admin/views/layouts/main.php',
//        ]
//    ],
    'container' => [
        'definitions' => [
            \yii\widgets\LinkPager::class => \frontend\modules\admin\widgets\yii\LinkPager::class,
            \yii\bootstrap4\ActiveField::class => \frontend\modules\admin\widgets\yii\ActiveField::class,
        ],
    ],
    'params' => $params,

];

// if (!YII_ENV_PROD) {
//     // configuration adjustments for 'dev' environment
//     $config['bootstrap'][] = 'debug';
//     $config['modules']['debug'] = [
//         'class' => 'yii\debug\Module',
//     ];

//     $config['bootstrap'][] = 'gii';
//     $config['modules']['gii'] = [
//         'class' => 'yii\gii\Module',
//     ];
// }

return $config;
