<?php

namespace frontend\controllers;


use backend\modules\pkpass\models\PkpassPass;
use backend\modules\pkpass\models\PkpassTemplate;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use frontend\models\CorrectPersonalData;
use frontend\models\RegistrationThirdParty;
use frontend\models\UploadForm;
use frontend\models\UserPassport;
use Yii;
use frontend\models\LoginAfterVerifyEmail;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\User;
use frontend\models\UserProfile;
use frontend\models\VerifyEmailForm;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\RegistrationForm;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 *
 *  "($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'])"
 *
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->role == User::ROLE_USER) {
                $modelUserProfile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);
                if ($modelUserProfile->pkpass_identifier != null || $modelUserProfile->pkpass_identifier != '' || !empty(IdentificationCard::findOne(['user_id' => Yii::$app->user->id]))) {
                    return $this->redirect(Url::toRoute('/admin/default'));
                } else {
                    return $this->redirect(Url::toRoute('add-card'));
                }
            } else {
                return $this->redirect(Url::toRoute('/admin/default'));
            }
        } else {
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Registration user.
     *
     * @return mixed
     */
    public function actionRegistration()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->redirect('https://office.cryptomatic-atm.org');
        }
        $model = new RegistrationForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && ($user = $model->registration())) {
                Yii::$app->mailer->compose('emailVerify-html', ['user' => $user])->setFrom([Yii::$app->params['registerEmail'] => Yii::$app->params['senderName']])->setTo([$user->email])->setSubject(Yii::t('email name', 'Verify Email for CRYPTOMATICATM'))->send();
                return $this->redirect(Url::toRoute('verify-email'));
            }
        }

        if (Yii::$app->getUser()->isGuest) {
            $birthdaySelect = $model->rBirthdaySelect($model->birthdayY, $model->birthdayD, $model->birthdayY);
            return $this->render('registration', [
                'model' => $model,
                'birthdaySelect' => $birthdaySelect,
            ]);
        } else {
            return $this->redirect(Url::toRoute('attach-passport'));
        }
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail()
    {
        $model = new VerifyEmailForm();
        $get = null;

        if ($model->load(Yii::$app->request->post()) || $get = Yii::$app->request->get()) {
            if ($get) {
                $model->token = $get['token'];
            }
            $verifyToken = $model->verify($model->token);
            if (!$verifyToken) {
                Yii::$app->session->setFlash('danger', 'Wrong key');
            } else {
                if ($verifyToken['status'] == User::STATUS_ACTIVE) {
                    return $this->redirect(Url::toRoute('attach-passport'));
                } else {
                    $login = new LoginAfterVerifyEmail;
                    $login->email = $verifyToken['email'];
                    $login->password = $verifyToken['password_hash'];
                    if ($login->validate() && $login->login()) {
                        return $this->redirect(Url::toRoute('attach-passport'));
                    } else {
                        Yii::$app->session->setFlash('danger', 'Authorization problem');
                    }
                }
            }
        }
        if (Yii::$app->getUser()->isGuest) {
            return $this->render('verifyEmail', [
                'model' => $model,
            ]);
        } elseif (!Yii::$app->user->can('card.create')) {
            return $this->redirect(Url::toRoute('payment-method'));
        } else {
            return $this->redirect(Url::toRoute('attach-passport'));
        }
    }

    public function actionAttachPassport()
    {
        $user = Yii::$app->user->id;
        $files = new UploadForm();

        if (Yii::$app->request->isPost) {
            $files->imageFile = UploadedFile::getInstances($files, 'imageFile');
            if($files->validate()) {
                $model = new UserPassport();
                if($model->saveImages($files)) {
                    User::updateAll(['status'=>User::STATUS_ACTIVE], ['id'=>Yii::$app->user->id]);
                    return $this->redirect(Url::toRoute('payment-method'));
                }
            }

        }
        return $this->render('attach_passport', ['file'=>$files]);
    }

    /**
     * Add card iframe.
     *
     * @return mixed
     */
    public function actionAddCard($edit = false)
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
        if (!Yii::$app->user->can('card.create')) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }

        $modelProfile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);

        //---------------------------------- Нет платежной системы

        $modelProfile->first_name_card = 'first_name_card_DEFAULT';
        $modelProfile->last_name_card = 'last_name_card_DEFAULT';
        $modelProfile->ccno = 'ccno_DEFAULT';
        $modelProfile->ref_card_no = 'ref_card_no_DEFAULT';
        $modelProfile->card_type = 'VI';
        $modelProfile->expiry_date = 'expiry_date_DEFAULT';
        $modelProfile->card_status = $modelProfile::STATUS_CARD_INACTIVE;

        $modelProfile->save();

        return $this->redirect(Url::toRoute('registration-third-party'));

//------------------------


//        if ($edit == true) {
//            return $this->render('addCard');
//        } elseif ($modelProfile->ref_card_no == '' || $modelProfile->ref_card_no == null) {
//            return $this->render('addCard');
//        } else {
//            return $this->redirect(Url::toRoute('registration-third-party'));
//        }
    }

    /**
     * Add card.
     *
     * @param $xmlresponse
     * @return mixed
     */
    public function actionCard($xmlresponse = null)
    {
        $vals = simplexml_load_string($xmlresponse)->RESPONSE;

        if (isset($vals->REFCARDNO)) {

            $userProfile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);

            $data = [];
            $data += ['ref_card_no' => (string)$vals->REFCARDNO];
            $data += ['ccno' => (string)$vals->CCNO];
            $data += ['card_type' => (string)$vals->CARDTYPE];
            $data += ['expiry_date' => (string)$vals->EXPIRYDATE];
            $data += ['first_name' => (string)$vals->FIRSTNAME];
            $data += ['last_name' => (string)$vals->LASTNAME];
            $data += ['errorCode' => (string)$vals->ERRORCODE];
            $data += ['errorText' => (string)$vals->ERRORTEXT];
            $data += ['action' => (string)$vals->ACTION];

            $userProfile->first_name_card = $data['first_name'];
            $userProfile->last_name_card = $data['last_name'];
            $userProfile->ccno = $data['ccno'];
            $userProfile->ref_card_no = $data['ref_card_no'];
            $userProfile->card_type = $data['card_type'];
            $userProfile->expiry_date = $data['expiry_date'];
            $userProfile->card_status = $userProfile::STATUS_CARD_INACTIVE;

            if ($userProfile->save()) {
                return $this->renderPartial('card', [
                    'data' => $data,
                ]);
            } else {
                return $this->redirect(Url::toRoute('add-card'));
            }
        } else {
            return $this->redirect(Url::toRoute('add-card'));
        }
    }

    public function actionRegistrationThirdParty()
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
//        if (!Yii::$app->user->can('card.create')) {
//            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
//        }

        $userId = Yii::$app->user->id;
        $model = new RegistrationThirdParty;
        $modelUser = User::findOne(['id' => $userId]);
        $modelUserProfile = $modelUser->userProfile;

        if (Yii::$app->user->identity->role == $modelUser::ROLE_ADMIN) {
            return $this->redirect(Url::toRoute('/user/profile/attached-card'));
        }

        if ($modelUserProfile->reg_person) {
            return $this->redirect(Url::toRoute('payment-method'));
        }

        if (strtoupper($modelUser->username) == strtoupper($modelUserProfile->first_name_card) && strtoupper($modelUser->last_name) == strtoupper($modelUserProfile->last_name_card)) {
            $modelUserProfile->reg_person = 'person';
            $modelUserProfile->card_status = $modelUserProfile::STATUS_CARD_ACTIVE;
            $modelUserProfile->save();
            return $this->redirect(Url::toRoute('payment-method'));
        } else {
            if (Yii::$app->request->post('continue') && $model->load(Yii::$app->request->post()) && $model->validate()) {
                $modelUserProfile->reg_person = 'third_party';
                $modelUserProfile->card_status = $modelUserProfile::STATUS_CARD_ACTIVE;
                $modelUserProfile->save();
                return $this->redirect(Url::toRoute('payment-method'));
            }
            if (Yii::$app->request->post('correct')) {
                return $this->redirect(Url::toRoute('correct-personal-data'));
            }
            if (Yii::$app->request->post('change')) {
                return $this->actionAddCard(true);
            }

            return $this->render('registrationThirdParty', [
                'model' => $model,
                'modelUser' => $modelUser,
                'modelUserProfile' => $modelUserProfile,
            ]);
        }

    }

    /**
     * Correct personal data
     */
    public function actionCorrectPersonalData()
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
        if (!Yii::$app->user->can('card.create')) {
            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
        }

        $userId = Yii::$app->user->id;
        $model = new CorrectPersonalData();
        $models = $model->toGetModels($userId);
        $oldEmail = $models->modelUser->email;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($oldEmail != $model->email) {
                $model->addChanges($models->modelUser, $models->modelUserProfile, true);
                Yii::$app->mailer->compose('emailVerify-html', ['user' => $models->modelUser])->setFrom([Yii::$app->params['registerEmail'] => Yii::$app->params['senderName']])->setTo([$models->modelUser->email])->setSubject('Verify Email')->send();
                return $this->redirect(Url::toRoute('verify-email'));
            } else {
                $model->addChanges($models->modelUser, $models->modelUserProfile);
                return $this->redirect(Url::toRoute('registration-third-party'));
            }
        }

        return $this->render('correctPersonalData', [
            'model' => $model,
        ]);
    }

    /**
     * Add identification card or passcreator service
     */
    public function actionPaymentMethod()
    {
        if (Yii::$app->getUser()->isGuest) {
            return $this->redirect(Url::toRoute('login'));
        }
//        if (!Yii::$app->user->can('card.create')) {
//            throw new ForbiddenHttpException(Yii::t('admin', 'Access denied'));
//        }
        $userId = Yii::$app->user->id;
        $modelUser = User::find()->where(['id' => $userId])->one();
        $modelUserProfile = $modelUser->userProfile;

        if (empty(($modelPasss = PkpassPass::find()->where(['user_id' => $userId])->one()))) {
            $modelPasss = new PkpassPass();
            $modelPasss->serial_number = uniqid('', true);
            $modelPasss->authentication_token = uniqid('token_', true);
            $modelPasss->user_id = $userId;
//            $modelPasss->voided = false;
            $pkpassTemplate = PkpassTemplate::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'status' => PkpassTemplate::STATUS_ACTIVE])->one();
            if(!$pkpassTemplate) {
                $pkpassTemplate = PkpassTemplate::find()->where(['status' => PkpassTemplate::STATUS_ACTIVE])->one();
            }
            $modelPasss->template_id = $pkpassTemplate->id;
            $modelPasss->save();
        }
        if (empty($modelUserProfile->qr_key) || $modelUserProfile->pkpass_status != $modelUserProfile::STATUS_PASS_ACTIVE) {
            if (empty($modelUserProfile->qr_key)) {
                $modelUserProfile->qr_key = $modelUserProfile->qr_key();
            }
            if ($modelUserProfile->pkpass_status != $modelUserProfile::STATUS_PASS_ACTIVE) {
                $modelUserProfile->pkpass_status = $modelUserProfile::STATUS_PASS_ACTIVE;
            }
            $modelUserProfile->pkpass_identifier= '1234567890_DEFAULT';
            $modelUserProfile->save();
        }
        if ($modelUser->status != User::STATUS_ACTIVE) {
            $modelUser->status = User::STATUS_ACTIVE;
            $modelUser->save();
        }

        if (($modelPasss = PkpassPass::find()->where(['user_id' => $userId])->one()) == null) {
            $modelPasss = new PkpassPass();
            $modelPasss->serial_number = uniqid('', true);
            $modelPasss->authentication_token = uniqid('token_', true);
            $modelPasss->user_id = $userId;
            $modelPasss->template_id = (PkpassTemplate::find()->where(['status' => PkpassTemplate::STATUS_ACTIVE])->one())->id;
            $modelPasss->pass_type_identifier = PkpassTemplate::PASS_TYPE_IDENTIFIER;
            $modelPasss->save();
        }
        $modelPkpassTemplate = $modelPasss->pkpassTemplate;

        $modelPkpassTemplate->createPass($modelPasss, false);

        $renderer = new ImageRenderer(
            new RendererStyle(700),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);


//        $modelIdentificationCard = new IdentificationCard();
//        $modelPass = new PasscreatorService();
//        if (empty(PasscreatorTemplate::findOne(['template_status' => PasscreatorTemplate::STATUS_ACTIVE]))) {
//            $passTemplate = false;
//        } else {
//            $passTemplate = true;
//        }
//
//        if ($modelUserProfile->pkpass_identifier != null || $modelUserProfile->pkpass_identifier != '') {
//            $pkpass = true;
//            $passId = $modelUserProfile->pkpass_identifier;
//            $iosUrl = $modelUserProfile->ios_pass;
//            $androidUrl = $modelUserProfile->android_pass;
//        } else {
//            $pkpass = false;
//            $passId = null;
//            $iosUrl = null;
//            $androidUrl = null;
//        }

//        if (!empty($model = IdentificationCard::findOne(['user_id' => $userId]))) {
//            $IdentificationCard = true;
//            $modelIdentificationCard = $model;
//        } else {
//            $IdentificationCard = false;
//        }
//
//        if (Yii::$app->request->post()) {
//
//            if (Yii::$app->request->post('continue')) {
//                if ($IdentificationCard || $pkpass) {
//                    return $this->redirect(Url::toRoute('/admin/default'));
//                } else {
//                    return $this->render('paymentMethod', [
//                        'modelIdentificationCard' => $modelIdentificationCard,
//                        'modelPass' => $modelPass,
//                        'passId' => $passId,
//                        'iosUrl' => $iosUrl,
//                        'androidUrl' => $androidUrl,
//                        'alert' => true,
//                        'passTemplate' => $passTemplate,
//                        'modelPasss' => $modelPasss,
//                        'writer' => $writer
//                    ]);
//                }
//            }

//            if (Yii::$app->request->post('save') && $modelIdentificationCard->load(Yii::$app->request->post()) && $modelIdentificationCard->validate()) {
//                if (!empty($modelCard = IdentificationCard::find()->where(['card_id' => $modelIdentificationCard->card_id, 'type' => IdentificationCard::TYPE_CLIENT])->one())) {
//                    $modelCard->user_id = $userId;
//                    $modelCard->card_status = IdentificationCard::STATUS_ACTIVE;
//                    $modelCard->save();
//                    $modelUser->status = User::STATUS_ACTIVE;
//                    $modelUser->save();
//                    return $this->render('paymentMethod', [
//                        'modelIdentificationCard' => $modelIdentificationCard,
//                        'modelPass' => $modelPass,
//                        'passId' => $passId,
//                        'iosUrl' => $iosUrl,
//                        'androidUrl' => $androidUrl,
//                        'alert' => false,
//                        'passTemplate' => $passTemplate,
//                        'modelPasss' => $modelPasss,
//                        'writer' => $writer
//                    ]);
//                }
//            }

//            if (Yii::$app->request->post('pkpass') && $modelPass->load(Yii::$app->request->post()) && $modelPass->validate() && !$pkpass) {
//                $pkpass = $modelPass->passcreatorCreatePass($userId);
//                $passId = $pkpass->identifier;
//                $iosUrl = $pkpass->iPhoneUri;
//                $androidUrl = $pkpass->AndroidUri;
//                $modelUser->status = User::STATUS_ACTIVE;
//                $modelUser->save();
//
//                return $this->render('paymentMethod', [
//                    'modelIdentificationCard' => $modelIdentificationCard,
//                    'modelPass' => $modelPass,
//                    'passId' => $passId,
//                    'iosUrl' => $iosUrl,
//                    'androidUrl' => $androidUrl,
//                    'alert' => false,
//                    'passTemplate' => $passTemplate,
//                    'modelPasss' => $modelPasss,
//                    'writer' => $writer
//                ]);
//            }
//        }

//        if ($IdentificationCard || $pkpass) {
//            return $this->redirect(Url::toRoute('/admin/default'));
//        } else {
            return $this->render('paymentMethod', [

                'alert' => false,
                'modelPasss' => $modelPasss,
                'writer' => $writer
            ]);
//        }
    }


    /**
     * Terms of Service
     */
    public function actionTermsOfService()
    {
        return $this->render('termsOfService');
    }

    /**
     * Terms of Use
     */
    public function actionTermsOfUse()
    {
        return $this->render('termsOfUse');
    }

    /**
     * Privacy Policy
     */
    public function actionPrivacyPolicy()
    {
        return $this->render('privacyPolicy');
    }

    /**
     * Mailing
     */
    public function actionMailing()
    {
        return $this->render('mailing');
    }

    /**
     * Test form.
     *
     * @return mixed
     */
    public function actionTestForm()
    {
        return $this->renderPartial('testForm');
    }
}
