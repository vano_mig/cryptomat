<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * Class RegistrationForm, use User & UserProfile class
 * @package frontend\models
 * @property string $username;
 * @property string $last_name;
 * @property string $email;
 * @property string $password_hash;
 * @property string $password_check;
 * @property string $image;
 * @property string $role;
 * @property integer $status;
 * @property string $phone_number;
 * @property string $country;
 * @property string $city;
 * @property string $street_house;
 * @property integer $post_code;
 * @property string $qr_key;
 * @property string $client_from;
 * @property string $mailing;
 * @property string $verification_token;
 * @property string $terms_of_service;
 * @property string $privacy_policy;
 * @property string $birthdayD;
 * @property string $birthdayM;
 * @property string $birthdayY;
 * @property string $agent_id;
 * @property integer $company_id;
 */
class RegistrationForm extends Model
{

    public $username;
    public $last_name;
    public $birthdayD;
    public $birthdayM;
    public $birthdayY;
    public $email;
    public $password_hash;
    public $password_check;
    public $image;
    public $role;
    public $status;
    public $phone_number;
    public $country;
    public $city;
    public $street_house;
    public $post_code;
    public $qr_key;
    public $client_from;
    public $mailing;
    public $verification_token;
    public $terms_of_service;
    public $privacy_policy;
    public $birthday;
    public $agent_id;
    public $company_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],

            ['last_name', 'trim'],
            ['last_name', 'required'],

            ['birthdayD', 'trim'],
            ['birthdayD', 'required', 'message' => Yii::t('site', 'Day')],
            ['birthdayM', 'trim'],
            ['birthdayM', 'required', 'message' => Yii::t('site', 'Month')],
            ['birthdayY', 'trim'],
            ['birthdayY', 'required', 'message' => Yii::t('site', 'Year')],


            ['birthday', 'filter', 'filter' => function ($value) {
                $value = $this->birthdayY . '-' . $this->birthdayM . '-' . $this->birthdayD;
                return $value;
            }],
            ['birthday', 'compare', 'compareValue' => date('Y-m-d', strtotime('-18 year')), 'operator' => '<=', 'type' => 'date', 'message' => Yii::t('site', 'You must be over 18 years old')],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('site', 'This email address has already been taken.')],

            ['password_hash', 'trim'],
            ['password_hash', 'required'],
            [['password_hash'], 'string', 'min' => 6],

            ['password_check', 'trim'],
            ['password_check', 'required'],
            ['password_check', 'compare', 'compareAttribute' => 'password_hash'],

            ['company_id', 'trim'],
            ['company_id', 'required'],

            ['status', 'default', 'value' => User::STATUS_INACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_INACTIVE]],

            ['role', 'default', 'value' => User::ROLE_USER],
            ['role', 'in', 'range' => [User::ROLE_USER, User::ROLE_CUSTOMER, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER, User::ROLE_ADMIN]],

            ['image', 'default', 'value' => User::PATH_DEFAULT_LOGO],

            ['phone_number', 'trim'],
            ['phone_number', 'required'],
            ['phone_number', 'string', 'min' => 1, 'max' => 255],

            ['country', 'trim'],
            ['country', 'required'],
            ['country', 'string', 'min' => 1, 'max' => 255],

            ['city', 'trim'],
            ['city', 'required'],
            ['city', 'string', 'min' => 1, 'max' => 255],

            ['street_house', 'trim'],
            ['street_house', 'required'],
            ['street_house', 'string', 'min' => 1, 'max' => 255],

            ['post_code', 'trim'],
            ['post_code', 'string', 'min' => 1, 'max' => 255],

            ['agent_id', 'integer'],

            ['mailing', 'default', 'value' => false],
            ['mailing', 'boolean'],

            ['privacy_policy', 'default', 'value' => false],
            ['privacy_policy', 'boolean'],

            ['terms_of_service', 'default', 'value' => false],
            ['terms_of_service', 'boolean'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('site', 'First Name'),
            'last_name' => Yii::t('site', 'Last Name'),
            'birthday' => Yii::t('site', 'Birthday'),
            'birthdayD' => Yii::t('site', 'Day'),
            'birthdayM' => Yii::t('site', 'Month'),
            'birthdayY' => Yii::t('site', 'Year'),
            'email' => Yii::t('site', 'Email'),
            'password_hash' => Yii::t('site', 'Password'),
            'password_check' => Yii::t('site', 'Check Password'),
            'first_name_card' => Yii::t('site', 'First Name'),
            'last_name_card' => Yii::t('site', 'Last Name'),
            'phone_number' => Yii::t('site', 'Phone Number'),
            'country' => Yii::t('site', 'Country'),
            'city' => Yii::t('site', 'City'),
            'street_house' => Yii::t('site', 'Street & House Number'),
            'post_code' => Yii::t('site', 'POST/ZIP Code'),
            'ref_card_no' => Yii::t('site', 'RefCardNo'),
            'ccno' => Yii::t('site', 'Card Number'),
            'card_type' => Yii::t('site', 'Card Type'),
            'expiry_date' => Yii::t('site', 'Expiry Date'),
            'client_from' => Yii::t('site', 'How do you know about us:'),
            'mailing' => Yii::t('site', 'Mailing'),
            'privacy_policy' => Yii::t('site', 'Privacy Policy'),
            'terms_of_service' => Yii::t('site', 'Terms of Service'),
            'agent_id' => Yii::t('site', 'Agent'),
            'company_id' => Yii::t('site', 'Company'),
        ];
    }

    /**
     * Verification token.
     *
     * @return string
     * @throws mixed
     */
    public function verify_token()
    {
        $verification_token = Yii::$app->security->generateRandomString();
        if (User::find()->where(['verification_token' => $verification_token])->one()) {
            $this->verify_token();
        } else {
            return $verification_token;
        }
    }


    /**
     * Birthday dropDown form options
     * @return array
     */
    public function rBirthdaySelect($birthdayDD = null, $birthdayMM = null, $birthdayYY = null)
    {
        $user = new User();
        return $user->birthdaySelect($birthdayDD, $birthdayMM, $birthdayYY);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function registration()
    {
        $user = new User();
        $user->username = $this->username;
        $user->last_name = $this->last_name;
        $user->birthday = $this->birthdayY . '-' . $this->birthdayM . '-' . $this->birthdayD;
        $user->email = $this->email;
        $user->agent_id = (int)$this->agent_id;
        $user->setPassword($this->password_hash);
        $user->generateAuthKey();
        $user->role = User::ROLE_USER;
        $user->status = User::STATUS_INACTIVE;
        $user->image = '';
        $user->verification_token = $this->verify_token();
        $user->company_id = $this->company_id;

        $userProfile = new UserProfile();
        $userProfile->phone_number = $this->phone_number;
        $userProfile->country = $this->country;
        $userProfile->city = $this->city;
        $userProfile->street_house = $this->street_house;
        $userProfile->post_code = $this->post_code;
        $userProfile->qr_key = $userProfile->qr_key();
        $userProfile->mailing = $this->mailing;
        $userProfile->privacy_policy = $this->privacy_policy;
        $userProfile->terms_of_service = $this->terms_of_service;
        $userProfile->pkpass_identifier= '1234567890_DEFAULT';

        if ($user->validate() && $userProfile->validate() && $user->save() && ($userProfile->user_id = $user->id) && $userProfile->save()) {
            return $user;
        } else {
            return false;
        }
    }
}