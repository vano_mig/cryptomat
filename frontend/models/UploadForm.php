<?php

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package frontend\models\form
 * @property mixed|null $imageFile
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, giff', 'maxFiles' => 4],
        ];
    }

    public function upload($path):bool
    {
        $result = true;
        if ($this->validate()) {
            foreach ($this->imageFile as $file) {
                if(!$file->saveAs($path . $file->baseName . '.' . $file->extension)) {
                    $result = false;
                }
            }
        }
        return $result;
    }
}