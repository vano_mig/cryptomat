<?php

namespace frontend\models;

use Yii;

/**
 * Class Company
 * @package frontend\models
 */
class Company extends \common\models\Company
{
    public static function getList()
    {
        $result = [];
        $list = Company::find()->where(['status'=>Company::STATUS_ACTIVE])->all();
        if(!empty($list)) {
            foreach ($list as $item) {
                $result[$item->id] = $item->name;
            }
        }
        return $result;
    }
}
