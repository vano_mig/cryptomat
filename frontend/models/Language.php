<?php

namespace frontend\models;

/**
 * Class Language
 * @package frontend\models
 */
class Language extends \common\models\Language
{
    /**
     * Get list active languages
     * @return $array
     */
    public static function getList():array
    {
        $array = [];
        $list = Language::find()->where(['status'=>'active'])->asArray()->all();
        if(!empty($list)) {
            foreach ($list as $item) {
                $array[$item['iso_code']] = $item['iso_code'];
            }
        }
        return $array;
    }
}