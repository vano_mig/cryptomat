<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * Class CorrectPersonalData, use User & UserProfile class
 * @package frontend\models
 * @property string $username;
 * @property string $last_name;
 * @property string $email;
 * @property integer $status;
 * @property string $phone_number;
 * @property string $country;
 * @property string $city;
 * @property string $street_house;
 * @property integer $post_code;
 * @property string $mailing;
 * @property string $verification_token;
 */
class CorrectPersonalData extends Model
{

    public $username;
    public $last_name;
    public $email;
    public $phone_number;
    public $country;
    public $city;
    public $street_house;
    public $post_code;
    public $mailing;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [['username'], 'string', 'min' => 2],

            ['last_name', 'trim'],
            ['last_name', 'required'],
            [['last_name'], 'string', 'min' => 2],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'validateEmail'],

            ['phone_number', 'trim'],
            ['phone_number', 'required'],
            ['phone_number', 'string', 'min' => 1, 'max' => 255],

            ['country', 'trim'],
            ['country', 'required'],
            ['country', 'string', 'min' => 1, 'max' => 255],

            ['city', 'trim'],
            ['city', 'required'],
            ['city', 'string', 'min' => 1, 'max' => 255],

            ['street_house', 'trim'],
            ['street_house', 'required'],
            ['street_house', 'string', 'min' => 1, 'max' => 255],

            ['post_code', 'trim'],
            ['post_code', 'required'],
            ['post_code', 'string', 'min' => 1, 'max' => 255],

            ['mailing', 'default', 'value' => false],
            ['mailing', 'boolean'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validateEmail($attribute, $params)
    {
        $model = User::findOne(['id' => Yii::$app->user->id]);
        if ($this->$attribute != $model->email) {
            if (!empty(User::findOne(['email' => $this->$attribute]))) {
                $this->addError($attribute, Yii::t('site', 'This email address has already been taken.'));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('site', 'First Name'),
            'last_name' => Yii::t('site', 'Last Name'),
            'email' => Yii::t('site', 'Email'),
            'phone_number' => Yii::t('site', 'Phone Number'),
            'country' => Yii::t('site', 'Country'),
            'city' => Yii::t('site', 'City'),
            'street_house' => Yii::t('site', 'Street & House Number'),
            'post_code' => Yii::t('site', 'POST/ZIP Code'),
            'mailing' => Yii::t('site', 'Mailing'),
        ];
    }

    /**
     * Verification token.
     *
     * @return string
     * @throws mixed
     */
    public function verify_token()
    {
        $verification_token = Yii::$app->security->generateRandomString();
        if (User::find()->where(['verification_token' => $verification_token])->one()) {
            $this->verify_token();
        } else {
            return $verification_token;
        }
    }

    /**
     * Get models User & UserProfile
     *
     * @param integer $id User id
     * @return object|bool
     */
    public function toGetModels($id)
    {
        $modelUser = User::findOne(['id' => $id]);
        $modelUserProfile = $modelUser->userProfile;
        $this->username = $modelUser->username;
        $this->last_name = $modelUser->last_name;
        $this->email =$modelUser->email;
        $this->phone_number = $modelUserProfile->phone_number;
        $this->country = $modelUserProfile->country;
        $this->city = $modelUserProfile->city;
        $this->street_house = $modelUserProfile->street_house;
        $this->post_code = $modelUserProfile->post_code;
        $this->mailing = $modelUserProfile->mailing;

        if ($modelUser) {
            return (object)['modelUser' => $modelUser, 'modelUserProfile' => $modelUserProfile];
        } else {
            return false;
        }
    }


    /**
     * Add changes in User & UserProfile
     *
     * @param User $modelUser
     * @param UserProfile $modelUserProfile
     * @param bool $recheckEmail
     * @return object | boolean('modelUser' => User, 'modelUserProfile' => UserProfile)
     */
    public function addChanges($modelUser, $modelUserProfile, $recheckEmail = false)
    {
        $modelUser->username = $this->username;
        $modelUser->last_name = $this->last_name;
        $modelUser->email = $this->email;

        if ($recheckEmail) {
            $modelUser->status = User::STATUS_INACTIVE;
            $modelUser->verification_token = $this->verify_token();
        }

        $modelUserProfile->phone_number = $this->phone_number;
        $modelUserProfile->country = $this->country;
        $modelUserProfile->city = $this->city;
        $modelUserProfile->street_house = $this->street_house;
        $modelUserProfile->post_code = $this->post_code;
        $modelUserProfile->mailing = $this->mailing;

        if ($modelUser->save() && $modelUserProfile->save()) {
            return (object)['modelUser' => $modelUser, 'modelUserProfile' => $modelUserProfile];
        } else {
            return false;
        }
    }

}