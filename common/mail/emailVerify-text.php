<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME']. '/verify-email?token='. $user->verification_token;
?>
Hello <?= $user->username ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>

