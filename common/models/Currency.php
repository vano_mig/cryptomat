<?php

namespace common\models;

use phpDocumentor\Reflection\Types\This;
use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $system_currency
 * @property string $status
 */
class Currency extends \yii\db\ActiveRecord
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_INACTIVE = 'inactive';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'system_currency', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('currency', 'Name'),
            'code' => Yii::t('currency', 'Code'),
            'system_currency' => Yii::t('currency', 'System Currency'),
            'status' => Yii::t('currency', 'Status'),
        ];
    }
}
