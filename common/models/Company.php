<?php

namespace common\models;

use backend\modules\admin\models\ThirdPartyApi;
use frontend\modules\user\models\CompanyReferal;
use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $vat_number
 * @property string|null $street
 * @property string|null $city
 * @property string|null $country_id
 * @property string|null $zip_code
 * @property int|null $user_id
 * @property string|null $image
 * @property string|null $reg_number
 * @property string|null $bank_name
 * @property string|null $iban
 * @property string|null $bic
 * @property string|null $uid
 * @property string|null $sixt_number
 * @property string|null $status
 * @property string|null $service_email
 */
class Company extends \yii\db\ActiveRecord
{
    public const PATH_DEFAULT_LOGO = '/img/system/logo/Cryptomatic_Logo.svg';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETED = 'deleted';

    public $owners = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'vat_number', 'zip_code', 'country_id', 'name', 'street', 'city'], 'required'],
            [['user_id', 'zip_code', 'country_id'], 'integer'],
            ['service_email', 'email'],
            ['vat_number', 'string', 'length' => [4, 50]],
            [['name', 'image', 'street', 'city', 'uid', 'reg_number', 'bank_name', 'iban', 'bic', 'image', 'status', 'contract_number'], 'string', 'length' => [4, 255]],
            ['image', 'default', 'value' => self::PATH_DEFAULT_LOGO],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uid' => Yii::t('company', 'Uniqid'),
            'id' => Yii::t('company', 'ID'),
            'name' => Yii::t('company', 'Company Name'),
            'vat_number' => Yii::t('company', 'Vat Number'),
            'street' => Yii::t('company', 'Address'),
            'city' => Yii::t('company', 'City'),
            'country_id' => Yii::t('company', 'Country'),
            'zip_code' => Yii::t('company', 'Zip Code'),
            'user_id' => Yii::t('company', 'User'),
            'image' => Yii::t('company', 'Logo'),
            'reg_number' => Yii::t('company', 'Registration number'),
            'bank_name' => Yii::t('company', 'Bank name'),
            'iban' => Yii::t('company', 'IBAN'),
            'contract_number' => Yii::t('company', 'Contract number'),
            'bic' => Yii::t('company', 'BIC'),
            'status' => Yii::t('company', 'Status'),
            'owners' => Yii::t('company', 'Owners'),
            'service_email' => Yii::t('company', 'Service email'),
        ];
    }

    /**
     * get User model
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * get Country model
     *
     * @return mixed
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * get Country model
     *
     * @return mixed
     */
    public function getThirdParty()
    {
        return $this->hasOne(ThirdPartyApi::class, ['company_id' => 'id']);
    }

//    public function getReferal()
//    {
//        return $this->hasMany(CompanyReferal::class, ['company_id'=>'id']);
//    }

    /**
     * Get list Sub agents
     * @return array
     */
    public function getSubAgents():array
    {
        $list = $this->referal;
        $company = [];
        if(!empty($list)) {
            foreach ($list as $item) {
                $company[] = $item->company_referral_id;
            }
        }
        $users = [];
        if(!empty($company)) {
            foreach ($company as $item) {
                $users = User::find()->where(['role'=>User::ROLE_CUSTOMER, 'company_id'=>$item, 'status'=>User::STATUS_ACTIVE])->all();
            }
        }
        return $users;
    }

    /**
     * Get relative CompanyPaysystem model
     * @return \yii\db\ActiveQuery
     */
    public function getPaysystem()
    {
        return $this->hasOne(CompanyPaysystem::class, ['company_id'=>'id']);
    }
}
