<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paysystem".
 *
 * @property int $id
 * @property string $name
 * @property string $vpn_address
 * @property string $domain
 * @property string $host
 * @property int $port
 */
class Paysystem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paysystem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['port'], 'integer'],
            [['name', 'vpn_address', 'identificator', 'host'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('paysystem', 'ID'),
            'name' => Yii::t('paysystem', 'Name'),
            'vpn_address' => Yii::t('paysystem', 'Vpn Address'),
            'identificator' => Yii::t('paysystem', 'Identificator'),
            'host' => Yii::t('paysystem', 'Host'),
            'port' => Yii::t('paysystem', 'Port'),
        ];
    }
}
