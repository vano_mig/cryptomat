<?php

namespace common\models;

use yii\base\Model;
use Yii;

/**
 * Class RegistrationThirdParty
 * @package frontend\models
 * @property boolean $agree;
 */
class RegistrationThirdParty extends Model
{
    public $agree;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['agree', 'default', 'value' => false],
            ['agree', 'boolean'],
            ['agree', 'in', 'range' => [true], 'message' => Yii::t('site', '
This field is a mandatory field.')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'agree' => Yii::t('site', 'Agree'),
        ];
    }
}