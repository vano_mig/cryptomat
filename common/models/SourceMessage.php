<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property int $id
 * @property string $category
 * @property string $message
 * @property string $comment
 * @property string $status
 * @property Message $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{

    const  STATUS_OK = 1;
    const  STATUS_EDIT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'category'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['message', 'category'], 'unique', 'targetAttribute' => ['message', 'category']],
            [['comment', 'status'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'category' => Yii::t('translation', 'Category'),
            'message' => Yii::t('translation', 'Message'),
            'comment' => Yii::t('translation', 'Comment'),
            'status' => Yii::t('site', 'Status'),
            'islanguage' => Yii::t('translation', 'Language'),
        ];
    }
    /**
     * Get list user statuses
     * @param null $id string
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $array = [
            self::STATUS_OK => Yii::t('users', 'Ok'),
            self::STATUS_EDIT => Yii::t('users', 'Edit'),
        ];
        if ($id)
            return $array[$id];
        return $array;
    }
    /**
     * @return \yii\db\ActiveQuery
     */

    public function getMessages()
    {
        $modelLanguage = new Language();
        $lang=$modelLanguage->activeCodeLanguages;
        return $this->hasMany(Message::class, ['id' => 'id'])->where(['language'=> $lang]);
    }


}
