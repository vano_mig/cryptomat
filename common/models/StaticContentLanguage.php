<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "static_content_language".
 *
 * @property int $id
 * @property int $content_id
 * @property string $language
 * @property string $content
 * @property StaticContent $staticContent
 */
class StaticContentLanguage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'static_content_language';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 255],
        ];
    }

    /**
     * get Static Content Name
     *
     * @return yii\db\ActiveQuery
     */
    public function getStaticContentName()
    {
        return $this->hasOne(StaticContent::class, ['id' => 'content_id']);
    }
}
