<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_referral".
 *
 * @property int $id
 * @property int $company_id
 * @property int $company_referral_id
 * @property int $level
 */
class CompanyReferal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_referral';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'company_referral_id', 'level'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('company', 'Company'),
            'company_referral_id' => Yii::t('company', 'Company Referral'),
            'level' => Yii::t('company', 'Level'),
        ];
    }
}
