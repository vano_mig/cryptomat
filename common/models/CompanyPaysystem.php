<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_paysystem".
 *
 * @property int $id
 * @property int|null $company_id
 * @property int|null $paysystem_id
 * @property string|null $token
 * @property string|null $key
 */
class CompanyPaysystem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_paysystem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'paysystem_id'], 'integer'],
            [['token', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'company_id' => Yii::t('administration', 'Company ID'),
            'paysystem_id' => Yii::t('administration', 'Paysystem ID'),
            'token' => Yii::t('administration', 'Token'),
            'key' => Yii::t('administration', 'Key'),
        ];
    }

    /**
     * Get relative Paysystem model
     * @return \yii\db\ActiveQuery
     */
    public function getPaysystem()
    {
        return $this->hasOne(Paysystem::class, ['id'=>'paysystem_id']);
    }
}
