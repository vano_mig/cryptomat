<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_passport".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $file
 */
class UserPassport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_passport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['file'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'user_id' => Yii::t('users', 'User'),
            'file' => Yii::t('users', 'File'),
        ];
    }
}