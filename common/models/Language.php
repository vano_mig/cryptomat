<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property int $id
 * @property string $name
 * @property string $iso_code
 * @property string $main
 * @property string $status
 * @property array $activeLanguages
 * @property array $activeCodeLanguages
 */
class Language extends \yii\db\ActiveRecord
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'iso_code', 'main', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('country', 'Name'),
            'iso_code' => Yii::t('country', 'Iso Code'),
            'main' => Yii::t('country', 'Main'),
            'status' => Yii::t('country', 'Status'),
        ];
    }

    /**
     * Get list active Languages
     * @param array $langs array
     * @return array
     */
    static function getListLanguages()
    {
        $list = Language::find()->where(['status'=>self::ACTIVE])->all();
        $array = [];
        if($list) {
            foreach($list as $item) {
                $array[$item->iso_code] = $item->iso_code;
            }
        }
        return $array;
    }

    static $current = null;

    //Получение текущего объекта языка
    static function getCurrent()
    {
        if (self::$current === null) {
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

//Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->iso_code;
    }

//Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return Language::find()->where('`main` = :default', [':default' => 'active'])->one();
    }

//Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = Language::find()->where('iso_code = :url', [':url' => $url])->one();
            if ($language === null) {
                return null;
            } else {
                return $language;
            }
        }
    }

    /**
     * Get list of all active Languages
     * @param array $langs array
     * @return array
     */
    public function getActiveLanguages()
    {
        $list = Language::find()->where(['status' => self::ACTIVE])->all();
        $array = [];
        if ($list) {
            foreach ($list as $item) {
                    $array[$item->iso_code] = $item->name;
            }
        }
        return $array;
    }
    /**
     * Get list of all active Languages
     * @param array $langs array
     * @return array
     */
    public function getActiveCodeLanguages()
    {
        $list = Language::find()->where(['status' => self::ACTIVE])->all();
        $array = [];
        if ($list) {
            foreach ($list as $item) {
                $array[] = $item->iso_code;
            }
        }
        return $array;
    }

}
