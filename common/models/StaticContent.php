<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "static_content".
 *
 * @property int $id
 * @property string $name
 * @property staticContentLanguage $staticContentLanguage
 */
class StaticContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%static_content}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }


    /**
     * Content by id
     * looking for german by default
     *
     * @param string $name Сontent id
     * @return string
     */
    public static function Content($id)
    {
        if (!empty($model = self::findOne(['id' => $id]))) {
            $lang = Yii::$app->language;
            foreach ($model->staticContentLanguage as $item) {
                if ($item->language == $lang) {
                    $modelContent = $item;
                    break;
                } elseif ($item->language == "de") {
                    $modelContentDe = $item;
                } else {
                    $modelContentRandom = $item;
                }
            }
            if (isset($modelContent)) {
                return $modelContent->content;
            } elseif (isset($modelContentDe)) {
                return $modelContentDe->content;
            } elseif (isset($modelContentRandom)) {
                return $modelContentRandom->content;
            } else {
                return false;
            }
        } else {
            throw new NotFoundHttpException(Yii::t('site', 'Content Not Found'));
        }
    }

    public function CreateContentTranslate($name, $language)
    {
        $model = StaticContent::findOne(['name' => $name]);
        $modelContent = new StaticContentLanguage();
        $modelContent->language = $language;
        $modelContent->content = '';
        $modelContent->content_id = $model->id;
        $modelContent->save();
    }

    /**
     * get Static Content Translation
     *
     * @return yii\db\ActiveQuery
     */
    public function getStaticContentLanguage()
    {
        return $this->hasMany(StaticContentLanguage::class, ['content_id' => 'id']);
    }
}
