<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name_card
 * @property string $last_name_card
 * @property string $phone_number
 * @property string $country
 * @property string $city
 * @property string $street_house
 * @property integer $post_code
 * @property string $qr_key
 * @property string $ref_card_no
 * @property string $ccno
 * @property string $card_type
 * @property string $expiry_date
 * @property string $client_from
 * @property string $mailing
 * @property string $privacy_policy
 * @property string $terms_of_service
 * @property string $pkpass_identifier
 * @property string $pkpass_qr_url
 * @property string $ios_pass
 * @property string $android_pass
 * @property string $reg_person
 * @property string $pkpass_status
 * @property string $card_status
 * @property User $user
 */
class UserProfile extends ActiveRecord
{

    public const STATUS_PASS_ACTIVE = '1';
    public const STATUS_PASS_INACTIVE = '0';

    public const STATUS_CARD_ACTIVE = '1';
    public const STATUS_CARD_INACTIVE = '0';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['user_id', 'trim'],
            ['user_id', 'integer'],

            ['first_name_card', 'trim'],
            ['first_name_card', 'string', 'min' => 1, 'max' => 255],

            ['last_name_card', 'trim'],
            ['last_name_card', 'string', 'min' => 1, 'max' => 255],

            ['phone_number', 'trim'],
            ['phone_number', 'string', 'min' => 1, 'max' => 255],

            ['country', 'trim'],
            ['country', 'string', 'min' => 1, 'max' => 255],

            ['city', 'trim'],
            ['city', 'string', 'min' => 1, 'max' => 255],

            ['street_house', 'trim'],
            ['street_house', 'string', 'min' => 1, 'max' => 255],

            ['post_code', 'trim'],
            ['post_code', 'integer'],

            ['ref_card_no', 'trim'],
            ['ref_card_no', 'string', 'min' => 1, 'max' => 20],

            ['ccno', 'trim'],
            ['ccno', 'string', 'min' => 1, 'max' => 255],

            ['card_type', 'trim'],
            ['card_type', 'string', 'min' => 1, 'max' => 255],

            ['expiry_date', 'trim'],
            ['expiry_date', 'string', 'min' => 1, 'max' => 255],

            ['client_from', 'trim'],
            ['client_from', 'string', 'min' => 1, 'max' => 255],

            ['mailing', 'default', 'value' => false],
            ['mailing', 'boolean'],

            ['privacy_policy', 'default', 'value' => false],
            ['privacy_policy', 'boolean'],

            ['terms_of_service', 'default', 'value' => false],
            ['terms_of_service', 'boolean'],

            ['pkpass_status', 'safe'],

            ['card_status', 'safe'],

            ['reg_person', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'first_name_card' => Yii::t('site', 'First Name'),
            'last_name_card' => Yii::t('site', 'Last Name'),
            'phone_number' => Yii::t('site', 'Phone Number'),
            'country' => Yii::t('site', 'Country'),
            'city' => Yii::t('site', 'City'),
//            'street_house' => Yii::t('site', 'Street & House Number'),
            'street_house' => Yii::t('site', 'Address'),
            'post_code' => Yii::t('site', 'POST/ZIP Code'),
            'ref_card_no' => Yii::t('site', 'RefCardNo'),
            'ccno' => Yii::t('site', 'Card Number'),
            'card_type' => Yii::t('site', 'Card Type'),
            'expiry_date' => Yii::t('site', 'Expiry Date'),
            'client_from' => Yii::t('site', 'How do you know about us:'),
            'mailing' => Yii::t('site', 'Mailing'),
            'privacy_policy' => Yii::t('site', 'Privacy Policy'),
            'terms_of_service' => Yii::t('site', 'Terms of Service'),
        ];
    }

    /**
     * Delete card
     *
     * @param $id -> User ID
     * @return bool
     */
    public function deleteCard($id)
    {
        $userProfile = self::find()->where(['user_id' => $id])->one();
        $userProfile->ref_card_no = null;
        $userProfile->ccno = null;
        $userProfile->card_type = null;
        $userProfile->expiry_date = null;
        $userProfile->first_name_card = null;
        $userProfile->last_name_card = null;
        $userProfile->reg_person = null;
        if ($userProfile->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get User model
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * QR key
     *
     * @return string
     * @throws mixed
     */
    public function qr_key()
    {
        $qr_key = Yii::$app->security->generateRandomString(19);
        if (self::find()->where(['qr_key' => $qr_key])->one()) {
            $this->qr_key();
        } else {
            return $qr_key;
        }
    }

}
