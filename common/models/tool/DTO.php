<?php

namespace common\models\tool;
/**
 * Class Data Transfer Object
 * @package common\models\tool
 */
class DTO extends \stdClass
{

    public function __set($key, $data): void
    {
        $this->{$key} = $data;
    }

    public function __get($key)
    {
        return $this->{$key} ?? null;
    }

    public function __isset($key): bool
    {
        return isset($this->{$key});
    }

}
