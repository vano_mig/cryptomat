<?php

namespace common\models;

use backend\modules\virtual_terminal\models\Atm;
use common\components\Date;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int|null $terminal_id
 * @property int|null $widget_id
 * @property int|null $type
 * @property float|null $amount_receive
 * @property string|null $currency_receive
 * @property float|null $amount_sent
 * @property string|null $currency_sent
 * @property string|null $transaction_id
 * @property int|null $status
 * @property string|null $address
 * @property string|null $address_coin
 * @property string|null $phone_number
 * @property int|null $return_payment
 * @property int|null $coinmarketcap_id
 * @property int|null $coinmarketcap_id_2
 * @property int|null $transaction_begin
 * @property string|null $endtime
 * @property int|null $confirm_needed
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $api_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    public const TYPE_BUY_CASH = Atm::SERIES_CASH;
    public const TYPE_BUY_CRYPTO = Atm::SERIES_CRYPTO;

    public const STATUS_USED = 3;
    public const STATUS_SUCCESS = 2;
    public const STATUS_RECEIVE_CRYPTO = 7;
    public const STATUS_WAIT = 1;
    public const AWAITING_CONFIRM = 0;
    public const STATUS_SEND_CRYPTO = 5;
    public const STATUS_SEND_CRYPTO_SUCCESS = 8;
    public const STATUS_SEND_CASH_SUCCESS = 9;
    public const STATUS_CANCELED = -1;
    public const STATUS_ERROR = 6;
    public const STATUS_NOT_BALANCE = 4;

    public const RETURN_NO = 0;
    public const RETURN_YES = 1;
    public const RETURN_SUCCESS = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['terminal_id', 'type', 'status', 'confirm_needed', 'coinmarketcap_id', 'coinmarketcap_id_2', 'widget_id', 'transaction_begin', 'api_id'], 'integer'],
            [['endtime'], 'safe'],
            [['amount_receive', 'amount_sent'], 'number'],
            [['address', 'address_coin', 'phone_number', 'currency_receive', 'currency_sent'], 'string'],
            ['return_payment', 'default', 'value'=>self::RETURN_NO],
            ['return_payment', 'in', 'range'=>[self::RETURN_NO, self::RETURN_YES, self::RETURN_SUCCESS]],
            [['created_at', 'updated_at'], 'safe'],
            [['transaction_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('api', 'ID'),
            'terminal_id' => Yii::t('api', 'Terminal ID'),
            'widget_id' => Yii::t('api', 'Widget'),
            'type' => Yii::t('api', 'Type'),
            'amount_receive' => Yii::t('api', 'Amount ReCeived'),
            'amount_sent' => Yii::t('api', 'Amount Sent'),
            'currency_receive' => Yii::t('api', 'Currency Received'),
            'currency_sent' => Yii::t('api', 'Currency Sent'),
            'transaction_id' => Yii::t('api', 'Transaction ID'),
            'phone_number' => Yii::t('api', 'Phone Number'),
            'status' => Yii::t('api', 'Status'),
            'address'=>Yii::t('api', 'Wallet'),
            'address_coin'=>Yii::t('api', 'Address'),
            'transaction_begin'=>Yii::t('api', 'First transaction'),
            'created_at' => Yii::t('api', 'Created At'),
            'updated_at' => Yii::t('api', 'Updated At'),
            'api_id' => Yii::t('api', 'Api Name'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    static  public function getType(): array
    {
        return [
            self::TYPE_BUY_CASH => Yii::t('transaction', 'CASH'),
            self::TYPE_BUY_CRYPTO => Yii::t('transaction', 'CRYPTO'),
        ];
    }

    static public function getStatus(): array
    {
        return [
            self::STATUS_CANCELED =>Yii::t('transaction', 'Canceled'),
            self::AWAITING_CONFIRM =>Yii::t('transaction', 'Waiting confirm'),
            self::STATUS_WAIT =>Yii::t('transaction', 'Waiting payment'),
            self::STATUS_SUCCESS =>Yii::t('transaction', 'Success payment, done'),
            self::STATUS_USED =>Yii::t('transaction', 'Success payment, issue'),
            self::STATUS_SEND_CRYPTO =>Yii::t('transaction', 'Send crypto'),
            self::STATUS_SEND_CRYPTO_SUCCESS =>Yii::t('transaction', 'Success send crypto'),
            self::STATUS_SEND_CASH_SUCCESS =>Yii::t('transaction', 'Success send cash'),
            self::STATUS_RECEIVE_CRYPTO =>Yii::t('transaction', 'Success receive crypto'),
            self::STATUS_NOT_BALANCE =>Yii::t('transaction', 'No Balance'),
            self::STATUS_ERROR =>Yii::t('transaction', 'Error'),
        ];
    }

    static public function getTypeById($id)
    {
        $array = Transaction::getType();
        if ($array[$id] != null)
            return $array[$id];
        return null;
    }

    static public function getTypeByName($name)
    {
        return array_search($name, Transaction::getType());
    }
}
