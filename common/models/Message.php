<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language','translation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'language' => Yii::t('translation', 'Language'),
            'translation' => Yii::t('translation', 'Translation'),
        ];
    }


//    28.02.2020
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getId0()
//    {
//        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
//    }

}
