<?php

namespace common\models;

use common\components\Date;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "support".
 *
 * @property int $id
 * @property integer $ticket_id
 * @property string $status
 * @property string $client_id
 * @property string $answerer_id
 * @property string $category
 * @property string $title
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Support $supportAnswers
 */
class Support extends \yii\db\ActiveRecord
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SOLVED = 'solved';
    public const STATUS_STOPPED = 'stopped';
    public const STATUS_ANSWERED = 'answered';

    public const CATEGORY_TECHNICAL = 'technical';
    public const CATEGORY_ORGANIZATION = 'organization';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%support}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at','updated_at', 'status', 'client_id', 'category', 'title', 'text', 'answerer_id'], 'safe'],
            ['text', 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site','ID'),
            'ticket_id' => Yii::t('support','Ticket'),
            'client_id' => Yii::t('support','Client'),
            'answerer_id' => Yii::t('support','Answerer'),
            'category' => Yii::t('support','Category'),
            'title' => Yii::t('support','Title'),
            'text' =>Yii::t('support', 'Text'),
            'status' => Yii::t('support','Status'),
            'created_at' =>Yii::t('site', 'Created At'),
            'updated_at' =>Yii::t('site', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * Get user
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'answerer_id']);
    }

    /**
     * Get Support answers
     * @return yii\db\ActiveQuery
     */
    public function getSupportAnswers()
    {
        return $this->hasMany(Support::class, ['ticket_id' => 'id']);
    }
}
