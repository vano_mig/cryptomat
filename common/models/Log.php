<?php

namespace common\models;

use yii\base\Model;

class Log extends Model
{
	/**
	 * @param string $note
	 * @param string $log
	 * @return string
	 */
	public static function generateText($note = '', $log = ''): string
	{
		$dateTime = date("Y-m-d H:i:s");
		return ("\n\n[{$dateTime}] => [" . $note . "]\n" .
			($log != '' ? "=============================================================================================================================\n" : '').
			print_r($log, true) . "\n" .
			($log != '' ? "=============================================================================================================================\n" : ''));
	}

    /**
     * Main log ATM
     * @param mixed $log
     * @param string $note
     */
    public static function logAtm($note = '', $log = '')
    {

        error_log(Log::generateText($note, $log), 3, "/var/www/cryptomat/backend/runtime/logs/atm/atm.log");
    }

    /**
     * Main log ATM
     * @param mixed $log
     * @param string $note
     */
    public static function logCurl($note = '', $log = '')
    {
        error_log(Log::generateText($note, $log), 3, "/var/www/cryptomat/backend/runtime/logs/atm/curlV2.log");
    }

    /**
     * Main log WIDGET
     * @param mixed $log
     * @param string $note
     */
    public static function logWidget($note = '', $log = '')
    {
		error_log(Log::generateText($note, $log), 3, "/var/www/cryptomat/backend/runtime/logs/atm/widget.log");
    }

    /**
     * Main log WIDGET
     * @param mixed $log
     * @param string $note
     */
    public static function log($note = '', $log = '')
    {
		error_log(Log::generateText($note, $log), 3, "/var/www/cryptomat/backend/runtime/logs/mApp.log");
    }
}

