<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\rbac\DbManager;
use common\components\Date;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $last_name
 * @property string $birthday;
 * @property string $image
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $company_role
 * @property string $role
 * @property string $password write-only password
 * @property UserProfile $userProfile
 * @property string $birthdayD;
 * @property string $birthdayM;
 * @property string $birthdayY;
 * @property string $password_check;
 * @property int $agent_id
 */
class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED_PREFIX_EMAIL = 'DELETED_';

    const STATUS_DELETED = 1;
    const STATUS_INACTIVE = 5;
    const STATUS_SEMI_ACTIVE = 7;
    const STATUS_ACTIVE = 10;

    const ROLE_ADMIN = 'administrator';
    const ROLE_MANAGER = 'manager';
    const ROLE_ACCOUNTANT = 'accountant';
    const ROLE_CUSTOMER = 'customer';
    const ROLE_SERVICE = 'service';
    const ROLE_FILLING = 'filling';
    const ROLE_USER = 'user';

    const COMPANY_ROLE_OWNER = 'owner';
    const COMPANY_ROLE_SERVICE = 'service';
    const COMPANY_ROLE_FILLING = 'filling';

    const PATH_DEFAULT_LOGO = '/img/system/logo/Cryptomatic_Logo.png';

    public const KYC_ACTIVE = 2;
    public const KYS_BLOCKED = 1;

    public $password_check;
    public $birthdayD;
    public $birthdayM;
    public $birthdayY;

    public function attributeLabels()
    {
        return [
            'agent_id' => Yii::t('site', 'Agent'),
            'birthday' => Yii::t('site', 'Birthday'),
            'id' => Yii::t('site', 'ID'),
            'created_at' => Yii::t('site', 'Created'),
            'updated_at' => Yii::t('site', 'Updated'),
            'username' => Yii::t('site', 'First Name'),
            'last_name' => Yii::t('site', 'Last Name'),
            'password_hash' => Yii::t('site', 'Password'),
            'email' => Yii::t('site', 'Email'),
            'role' => Yii::t('site', 'Role'),
            'status' => Yii::t('site', 'Status'),
            'active' => Yii::t('site', 'Active'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
//                'value' => new Expression('NOW()'),
                'value' => (new Date())->now(),
            ]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_ACCOUNTANT, self::ROLE_CUSTOMER, self::ROLE_MANAGER, self::ROLE_ADMIN, self::ROLE_FILLING, self::ROLE_SERVICE]],
            ['image', 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE, self::STATUS_SEMI_ACTIVE]]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface|null
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param $password
     * @throws mixed
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        $this->password_check = $this->password_hash;
    }

    /**
     * Generates "remember me" authentication key
     *
     * @throws mixed
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     *
     * @throws mixed
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws mixed
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->generateAuthKey();
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Add user role to rbac
     *
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     * @throws mixed
     */
    public function afterSave($insert, $changedAttributes): bool
    {
        parent::afterSave($insert, $changedAttributes);
        $rbac = new DbManager;
        if ($insert) {
            if ($rbac->assign($rbac->getRole($this->role), $this->id)) {
                return true;
            }
        } else {
            if (isset($changedAttributes['role']) &&
                ($rbac->revokeAll($this->id) &&
                    $rbac->assign($rbac->getRole($this->role), $this->id))) {
                return true;
            }
        }
        return false;
    }

    /**
     * delete user role
     * @params mixed
     * @return bool
     */
    public function beforeDelete(): bool
    {
        parent::beforeDelete();
        $this->trigger(self::EVENT_BEFORE_DELETE);

        $rbac = new DbManager;
        if ($rbac->revokeAll($this->id)) {
            return true;
        }
        return false;
    }

    /**
     * Get User Profile
     *
     * @return yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::class, ['user_id' => 'id']);
    }

    /**
     * Get User company
     *
     * @return yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Birthday dropDown form options
     * @param int $birthdayD
     * @param int $birthdayM
     * @param int $birthdayY
     * @param string $birthday $model->birthday
     * @return array
     */

    public function birthdaySelect($birthdayDD = null, $birthdayMM = null, $birthdayYY = null, $birthday = null)
    {
        if (isset($birthday)) {
            $birthdayD = date('d', strtotime($birthday));
            $birthdayM = date('m', strtotime($birthday));
            $birthdayY = date('Y', strtotime($birthday));
            $promt = false;
        } else if ($birthdayDD && $birthdayMM && $birthdayYY) {
            $birthdayD = $birthdayDD;
            $birthdayM = $birthdayMM;
            $birthdayY = $birthdayYY;
            $promt = false;
        } else {
            $birthdayD = null;
            $birthdayM = null;
            $birthdayY = null;
            $promt = true;
        }

        $day = 1;
        $optionD = [];
        $optionDCss = [];
        $month = 1;
        $optionM = [];
        $optionMCss = [];
        $dataY = date('Y');
        $optionY = [];
        $optionYCss = [];
        for ($i = 1; $i <= 31; $i++) {
            $day = sprintf('%02d', $day);
            $optionD += [$day => $day];
            if ($day == $birthdayD) {
                $optionDCss += [$day => ['Selected' => !$promt, 'style' => "color:black"]];
            } else {
                $optionDCss += [$day => ['style' => "color:black"]];
            }
            $day = $day + 1;
        }
        $optionD += ['' => Yii::t('site', 'Day')];
        $optionDCss += ['' => ['disabled' => 'disabled', 'Selected' => $promt, 'hidden' => 'hidden']];
        for ($i = 1; $i <= 12; $i++) {
            $month = sprintf('%02d', $month);
            $optionM += [$month => $month];
            if ($month == $birthdayM) {
                $optionMCss += [$month => ['Selected' => !$promt, 'style' => "color:black"]];
            } else {
                $optionMCss += [$month => ['style' => "color:black"]];
            }
            $month = $month + 1;
        }
        $optionM += ['' => Yii::t('site', 'Month')];
        $optionMCss += ['' => ['disabled' => 'disabled', 'Selected' => $promt, 'hidden' => 'hidden']];
        for ($i = 1; $i <= 120; $i++) {
            $optionY += [$dataY => $dataY];
            if ($dataY == $birthdayY) {
                $optionYCss += [$dataY => ['Selected' => !$promt, 'style' => "color:black"]];
            } else {
                $optionYCss += [$dataY => ['style' => "color:black"]];
            }
            $dataY = $dataY - 1;
        }
        $optionY += ['' => Yii::t('site', 'Year')];
        $optionYCss += ['' => ['disabled' => 'disabled', 'Selected' => $promt, 'hidden' => 'hidden']];

        $birthdaySelect = ['optionD' => $optionD, 'optionDCss' => $optionDCss, 'optionM' => $optionM, 'optionMCss' => $optionMCss, 'optionY' => $optionY, 'optionYCss' => $optionYCss];
        return $birthdaySelect;
    }
}
