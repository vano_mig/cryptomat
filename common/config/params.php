<?php
return [
    'supportEmail' => 'service@cryptomatic-atm.org',
    'senderEmail' => 'service@cryptomatic-atm.org',
    'registerEmail' => 'service@cryptomatic-atm.org',
    'paymentEmail' => 'service@cryptomatic-atm.org',
    'patrickEmail' => 'p.doepfner@my-smart-store24.de',
    'ivanEmail' => 'vano.mig@gmail.com',
    'senderName' => 'CRYPTOMATICATM',
    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '4.x',
    'verifyEmail'=>'mariam@cryptomatic-atm.com'
];
