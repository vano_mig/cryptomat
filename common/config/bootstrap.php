<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
$url = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : null;
if (YII_ENV == 'dev') {
	Yii::setAlias('@domain', "http://{$url}");
} else {
	Yii::setAlias('@domain', "https://{$url}");
}
Yii::setAlias('@wallet', "https://office.cryptomatic-atm.org");
Yii::setAlias('@widget', "https://office.cryptomatic-atm.org");
